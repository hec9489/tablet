// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef UTILE_SOCKET_H_
#define UTILE_SOCKET_H_

#include <sys/stat.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <sys/socket.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>


#define TIMEOUT_DEFAULT_MS 		(10000)


int  bindEx(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
int  listenEx(int sockfd, int backlog);
int  acceptEx(int sockfd, struct sockaddr *cliaddr, socklen_t *addrlen, int nTimeout_msec);
int  connectEx(int sockfd, const struct sockaddr *serv_addr, socklen_t addrlen, int nTimeout_msec);

int  recvfromEx(int sockfd, void *buf, int len, int flags, struct sockaddr *from, socklen_t *fromlen, int nTimeout_msec);
int  sendtoEx(int  sockfd,  const  void *buf, int len, int flags, const struct sockaddr *to, socklen_t tolen, int nTimeout_msec);
int	 recvEx(int sockfd, char *buffer, int buffer_length, int flags, int nTimeout_msec);
int	 recvDaemon(int sockfd, char *buffer, int buffer_length, int flags, int nTimeout_msec);
int	 sendEx(int sockfd, char *buffer, int buffer_length, int flags, int nTimeout_msec);
int  shutdownEx(int sockfd);



#endif /* UTILE_SOCKET_H_ */
