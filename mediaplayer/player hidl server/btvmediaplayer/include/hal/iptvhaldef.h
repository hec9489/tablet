// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef _IPTV_HAL_DEF_
#define _IPTV_HAL_DEF_


typedef struct {
	char multicast_addr[256];	// address
	int multicast_port;			// port
	int video_pid;				// video PID
	int audio_pid;				// audio PID
	int pcr_pid;				// pcr PID
	int video_stream;			// Video Codec type
	int audio_stream;			// Audio Codec type 
	int ca_systemid;			// CA system ID 0: not exist
	int ca_pid;					// ecm ID 0: not exist	
	int channel_num;			// channel number
	int res;					// SD/HD
	int audioenable;			// audio enable
	int enableLastFrame;		// lastframe
	int multiviewType;			// jung2604 : multiview type : 2x2, 1x3
	int isMainChannelSame;      // jung2604 : main의 채널과 같다는 Flag // realtek에서 pip mode일때 main에서 play중인 iptv채널일경우 별도의 함수를 호출해줘야한다. dvb_locator_t에 값을 전달한다
} HTuner_IPTV_Info;

typedef struct {
	char szPath[256];		// FilePath or RTSP URL
	char szMetaInfo[256];	// MetaInfo used in ADV
	char szContentID[256];  // content ID used in DRM
	char szOTPID[256];		// one time password ID
	char szOTPPasswd[256];	// one time password
	int nPlayMode;			// file or RTSP
	int nPlayType;			// adv or not adv
	int nDRMType;			// DRM None or DRM Used
	int nDurationSec;		// Duratioin Sec
	int nResumeSec;			// Resume Sec
	int video_pid;				// video PID
	int audio_pid;				// audio PID
	int pcr_pid;				// pcr PID
	int video_stream;			// Video Codec type
	int audio_stream;			// Audio Codec type
	int enableLastFrame;			// lastframe
} HPlayer_IPTV_Info;

typedef enum
{
	PLAYER_MODE_NONE = 0,
	PLAYER_MODE_NORMAL_FILE,
	PLAYER_MODE_RTSP,
	PLAYER_MODE_IPTV,
	PLAYER_MODE_UNKNOWN
} BTV_PLAYER_MODE;

typedef enum
{
	PLAYER_STATUS_NONE = 0,
	PLAYER_STATUS_TUNE,
	PLAYER_STATUS_PLAY = 1,
	PLAYER_STATUS_STOP,
	PLAYER_STATUS_PAUSE,
	PLAYER_STATUS_2XFF,
	PLAYER_STATUS_4XFF,
	PLAYER_STATUS_8XFF,
	PLAYER_STATUS_16XFF,
	PLAYER_STATUS_2XBW,
	PLAYER_STATUS_4XBW,
	PLAYER_STATUS_8XBW,
	PLAYER_STATUS_16XBW,
} BTV_PLAYER_STATUS;

typedef enum
{
	DEVICE_STATUS_CLOSE = 0,
	DEVICE_STATUS_OPEN
} BTV_DEVICE_STATUS;

typedef enum
{
	TYPE_FILE_ADV = 0,
	TYPE_FILE_NORMAL
} BTV_FILE_PLAY_TYPE;

typedef enum
{
	STATUS_FILE_START = 0,
	STATUS_FILE_PLAY,
	STATUS_FILE_STOP
} BTV_FILE_PLAY_STATUS;

typedef enum
{
	TYPE_RTSP_ADV = 0,
	TYPE_RTSP_NORMAL
} BTV_RTSP_PLAY_TYPE;

typedef enum
{
	STATUS_RTSP_START = 0,
	STATUS_RTSP_PLAY,
	STATUS_RTSP_STOP,
	STATUS_RTSP_PAUSE,
	STATUS_RTSP_EOS
} BTV_RTSP_PLAY_STATUS;
	
// error value defined

#define     IPTV_ERR_SUCCESS		  0
#define     IPTV_ERR_ERROR			 -1
#define     IPTV_ERR_BADFILENAME	 -2
#define     IPTV_ERR_INPROGRESS		 -3
#define     IPTV_ERR_INUSE			 -4
#define     IPTV_ERR_INVALID		 -5
#define     IPTV_ERR_NOTCONN		 -6
#define     IPTV_ERR_NOTSUP			 -7
#define     IPTV_ERR_TIMEOUT		 -8
#define     IPTV_ERR_NOTEXIST		 -9
#define     IPTV_ERR_INVALIDSTATUS	 -10
#define     IPTV_ERR_INVALIDFORMAT	 -11
#define     IPTV_ERR_UNKNOWN		 -12

#endif /* _IPTV_HAL_DEF_ */
