// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef ___IPTV_SOC_PLAYER_H__
#define ___IPTV_SOC_PLAYER_H__

#ifdef FEATURE_SOC_AMLOGIC
//#include <gui/Surface.h>
//#include <android/native_window.h>
#endif

#include <iptvmedia/BtvMediaPlayerInterface.h>
#include <iptvmedia/BtvMediaPlayerServiceInterface.h>
#include "IptvTimedEventQueue.h"
#include "iptvhaldef.h"

#include <dbg.h>
#include "dvb_primitive.h"

#include "iptvmedia/util_logmgr.h"

#include <binder/Parcel.h>


#define CMD_RUN    0x001
#define CMD_ZAPPING     0x010

#define QUALITY_THRESHOLD_KEY			"vendor.skb.btv.mediaerrorrecovery"
#define REFERENCE_QUALITY_KEY			"vendor.skb.btv.referencequality"
#define ALLOW_REFERENCE_KEY			"vendor.skb.btv.allowreference"

#define QUALITY_THRESHOLD_DEFAULT_STR	"0"
#define REFERENCE_QUALITY_DEFAULT_STR	"0"
#define ALLOW_REFERENCE_DEFAULT_STR	"1"	// 1 : enable(default),	0 : disable

// jung2604 : multiview 화면 갯수
#define MULTIVIEW_MAX_NUMBER 3

namespace android
{
    class IptvBtvEvent;

    class IptvSocPlayer : public BtvMediaPlayerInterface
    {
    public:
        IptvSocPlayer();
        virtual								~IptvSocPlayer();

        virtual	status_t					initCheck(BtvMediaPlayerServiceInterface *pOwnerService, int deviceID);
        virtual int							getAuthChecked();
        virtual	status_t					CertCheck();
        virtual status_t					open(const char* dataXML);
        virtual status_t					tuneTV(const char* dataXML);
        virtual status_t					closeTV();
        virtual status_t					bindingFilter(const char* dataXML);
        virtual status_t					releaseFilter(const char* dataXML);
        virtual status_t					changeAudioChannel(const char* dataXML);
        virtual status_t					changeAudioOnOff(const char* dataXML);
        virtual status_t					getPlayerMode();
        virtual status_t					getPlayerStatus();
        virtual status_t					getCurrentPosition();
        virtual status_t					setWindowSize(const char* dataXML);
        virtual status_t					play();
        virtual status_t					pause();
        virtual status_t					resume();
        virtual status_t					seek(const char* dataXML, int flag);
        virtual status_t					pauseAt(const char* dataXML);
        virtual status_t					trick(const char* dataXML);
        virtual status_t					stop();
        virtual status_t					close();
        virtual status_t					invoke(const Parcel& request, Parcel *reply);
        virtual status_t					reset();
        void 						notifyListener_l(int32_t msg, int32_t ext1, int32_t ext2, const char* strData);
        void 						dataListener_l(int32_t msg, int32_t ext1, int32_t ext2, const char* strData);		
        void 						dsmccListener_l(int32_t dsmccEventType, char* dsmccRootPath, SignalEvent* event);		
		void 						audioprogramListener_l(char* filePath, int updateType, int audioPid);

    public:
        friend 	class 						IptvBtvEvent;

        IptvSocPlayer(const IptvSocPlayer &);
        IptvSocPlayer			&operator=(const IptvSocPlayer &);

        mutable Mutex 						mLock;
        mutable Mutex 						mLockStopFile;
        mutable Mutex 						mLockRTSPGetTimeStamp;

        IptvTimedEventQueue			mQueue;
        bool						mQueueStarted;
        IptvTimedEventQueue			mQueueSecond;
        bool						mQueueSecondStarted;
        IptvTimedEventQueue			mQueueTimeCheck;
        bool						mQueueTimeCheckStarted;

        sp<IptvTimedEventQueue::Event> 	mSocketBufferCheckEvent;
        sp<IptvTimedEventQueue::Event>  mFilePlayTimeCheckEvent;
        sp<IptvTimedEventQueue::Event> 	mFileCloseTimeCheckEvent;
        sp<IptvTimedEventQueue::Event> 	mRTSPPlayTimeCheckEvent;
        sp<IptvTimedEventQueue::Event> 	mAVPTSTimeCheckEvent;
        sp<IptvTimedEventQueue::Event> 	mAVEventCheckEvent;
        sp<IptvTimedEventQueue::Event> 	mConstructCurrentTimeEvent;
        sp<IptvTimedEventQueue::Event> 	mChangeAudioLanguageCheckEvent;

        bool							mSocketBufferCheckEventStop;

        void						onSocketBufferCheckEvent();
        void						onFilePlayTimeCheckEvent();
        void						onFileCloseTimeCheckEvent();
        void						onRTSPPlayTimeCheckEvent();
        void						onRecvDtvCCData(int what, int arg1, int arg2, const char* strData);
        void						onRecvDsmccData(int event, char* path, void *data);		
        void						onAVPTSTimeCheckEvent();
        void						onAVEventCheckEvent();
        void						onConstructCurrentTimeEvent();

    public:
        status_t					open_l(HPlayer_IPTV_Info stPlayInfo);
        status_t					open_rtsp_l(HPlayer_IPTV_Info stPlayInfo);
        status_t					open_file_l(HPlayer_IPTV_Info stPlayInfo);
        status_t					play_rtsp_l(int nEnforcePlay);
        status_t					play_file_l();
        status_t					tuneTV_l(HTuner_IPTV_Info stTuneInfo);
        status_t					tuneTV_Multiview_l(HTuner_IPTV_Info *ptTuneInfo);
        status_t					bindingFilter_l(int nIndex, int nPID, int nTableID, int nTimeOut);
        status_t					releaseFilter_l(int nIndex);
        status_t					changeAudioChannel_l(int nAPID, int nACodecID);
        status_t					changeAudioOnOff_l(int audioenable);
        status_t					setPlayerSurfaceView_l(int deviceID, uint32_t nativWindows);
#ifdef FEATURE_SOC_AMLOGIC
        //status_t					setPlayerSurface_l(int deviceID, const sp<Surface> &surface);
#endif
        status_t					setWindowSize_l(int nX, int nY, int nWidth, int nHeight);
        status_t					setVideoDelay_l(int delay);
        status_t					changeGlobalAudioMute_l();
        status_t					changeSettingDolby();
        status_t					changeAspectRatioMode();
        status_t					stop_rtsp_l();
        status_t					stop_file_l(int isEOF);
        status_t					stop_tv_l();
        status_t					close_rtsp_l(int nStopFlag);
        status_t					close_file_l();
        status_t					seek_l(long nSec, int puaseFlag);
        status_t					pauseAt_l(long nMs);
		status_t					keepLastFrame(int flag);
        status_t					trick_l(int nTrick);
        status_t					reset_l();
        status_t					setLastFrame(int enableLastFrame);
        status_t					startDmxFilter(int32_t pid, int32_t tid);
        status_t					stopDmxFilter(int32_t pid, int32_t tid);

        // enum replace const static
        const static int					mBTV_DEVICE_MAIN = 0;
        const static int 					mBTV_DEVICE_PIP1 = 1;

    private:
        void						postFilePlayTimeCheckEvent_l(int64_t delayUs);
        void						postFileCloseTimeCheckEvent_l(int64_t delayUs);
        void						postSocketBufferCheckEvent_l(int64_t delayUs);
        void						postRTSPPlayTimeCheckEvent_l(int64_t delayUs);
        void						postAVPTSTimeCheckEvent_l(int64_t delayUs);
        void						postAVEventCheckEvent_l(int64_t delayUs);
        void						postConstructCurrentTimeEvent_l(int64_t delayUs);


        //    linuxdvb implement all... default private value
    public: dvb_player_t player;
        dvb_locator_t locator;

        dvb_locator_t locator_create(HTuner_IPTV_Info stTuneInfo);
        dvb_locator_t locator_create(HPlayer_IPTV_Info stPlayInfo);
        int _TuneChannel(HTuner_IPTV_Info stTuneInfo);
        int _TuneMultiviewChannel(HTuner_IPTV_Info *ptTuneInfo);
        int _ChangeChannel(HTuner_IPTV_Info stTuneInfo);

    public:
        /////////////////////////////////////////////////////////////////////////
        // player status api
        /////////////////////////////////////////////////////////////////////////

        int	InGetDeviceOpenStatus();
        void					InSetDeviceOpenStatus(BTV_DEVICE_STATUS eStatus);
        int	InGetPlayerStatus();
        void					InSetPlayerStatus(BTV_PLAYER_STATUS eStatus);
        int		InGetPlayerMode();
        void					InSetPlayerMode(BTV_PLAYER_MODE eMode);

        int SendFilePlayStatus(int nMilisec, BTV_FILE_PLAY_STATUS eStatus, int isEOF = 0);
        int SendRTSPPlayStatus(int nMilisec, BTV_RTSP_PLAY_STATUS eStatus);
        int SendRTSPSeekCompleteEvent(); //juing2604 : 추가..살이있는 동아에서 Seek 완료후 메시지 받기를 원한..
        int SendRTSPErrorStatus(int nErrorCode, char *szIp, int nPort);

        long long  GetRTSPTimeStamp(int *nTimeChanged);
        int WaitTimeMatch(long long nCorrectTimeMS, int nMinusIntervalMS, int nPlusIntervalMS, int nRetryCount, int nChangeCheck, int nFirstFrameCheck);

    public:
        /////////////////////////////////////////////////////////////////////////
        // player ?? status?? ?????????? ???o? flag
        /////////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////
        // common ????

        // (2013.06.04)
        // device open/close ????
        // int ?? ?????? ??? ??????? 1byte ???????? ??? module ???? sync ?? ??´? ?????? ?????? ???????.
        int		eDeviceOpenStatus;

        // (2013.06.04)
        // ???? play status ?? ???????.
        // int ?? ?????? ??? ??????? 1byte ???????? ??? module ???? sync ?? ??´? ?????? ?????? ???????.
        int		ePlayerStatus;

        // (2013.06.04)
        // ???? play status ?? ???????.
        // int ?? ?????? ??? ??????? 1byte ???????? ??? module ???? sync ?? ??´? ?????? ?????? ???????.
        int			ePlayerMode;

        /////////////////////////////////////////////////////////////////////////
        // tune ????

        // (2013.06.04)
        // tune ???? ?????? ???????. bad->good , bad->bad, good->bad ?????
        // event ?? app ?? ?????? ??μ? ??? u???? ????? ??????.
        int nLastSignalStatus;

        /////////////////////////////////////////////////////////////////////////
        // tune ????

        // u??° frame ?? ?????????? u????.
        int nFirstFrameChanged;

        /////////////////////////////////////////////////////////////////////////
        // file play ????

        // (2013.06.04)
        // file play ?? ???????? ???? ??? ???????? u????.
        // ???? ????? ??? ?????? ??????-> ??? ?????? playstatus (???? play time ????) ??
        // 1????? ??? ?÷???? ???? ?????? ??쿡?? playstatus ?? (start->impl->stop) ?????? ???????.
        // int ?? ?????? ??? ??????? 1byte ???????? ??? module ???? sync ?? ??´? ?????? ?????? ???????.
        int		eFilePlayType;

        // file adv play ?? IMP?? ???´??? ????´??? ???????.
        int		nSendFILEADVIMP; // 0 ????? : 1 ????

        // file time check count ?? ??????.
        int		nCheckFileTimeCount;

        // (2013.06.04)
        // file/RTSP play ?? ??? ?????? ???/URL .. ????? ts ?? ???? ???.(btv ???? ts ??? play ?o??????? ????????..)
        char szMediaInfo[256];

        // (2013.06.04)
        // file/RTSP ADV play ?? ??? ?????? ??? ????.. play status ??????? ???? ???????.
        char szMetaInfo[256];

        //////////////////////////////////////////////////////////////////////////
        // RTSP ????

        // RTSP session ?? ??????? KEY ID
        int nRTSPSessionID;

        // rtsp play ?? ???????? ???? ??? ???????? u????.
        // ???? ????? ??? ?????? ??????-> ??? ?????? playstatus (???? play time ????) ??
        // 1????? ??? ?÷???? ???? ?????? ??쿡?? playstatus ?? (start->impl->stop) ?????? ???????.
        // int ?? ?????? ??? ??????? 1byte ???????? ??? module ???? sync ?? ??´? ?????? ?????? ???????.
        int		eRTSPPlayType;

        // rtsp adv play ?? IMP?? ???´??? ????´??? ???????.
        int		nSendRTSPADVIMP; // 0 ????? : 1 ????

        // rtsp/drm play ?? contentID ????
        char szContentID[256];

        // RTSP drm ???? ?????? OTP ID
        char szOTPID[256];

        // RTSP drm ???? ?????? OTP pwd
        char szOTPPASSWD[256];

        // RTSP Trick ?? ?????? base time
        int mRTSPBaseTime;

        // RTSP Trick ?? ?????? EOF time
        long long mRTSPEOFTime;

        // RTSP Play ?? ?????? start time (sec)
        int mRTSPStartTime;

        // RTSP Pause ?? ?????? pause time, trick -> play -> pause ???? ??????? ??? play ?? ???? ?ð??? ????? ????
        // ?????? ???????.
        long mRTSPPauseTime;

        // ????? ???? time stamp ??
        long long  mLastGetTimeStamp;

        // rtsp play status notify cancel flag
        int mJumpRtspStatusSend;

        // rtsp play ???? play time millisec
        // play/trick/pause ???? ???? ??? ???? ?????.
        long long mRTSPCurrentPlayTimePTS;

        long long mLastSendRTSPStatusPlayTimeMs; //jung2604 : 20190518 : UI에게 마지막으로 던져준 play time 값..

        // rtsp time check  ???? ???? 1: check 0: check done
        int mRTSPCalculateTimeCheck;

        // rtsp time check wait ????? ???? ???? 1: check 0: check done
        int mRTSPCalculateTimeCheckExceptWait;

        // rtsp time ??? ?ð?
        long long mRTSPWannaBeGonePTS;

        // rtsp time ??? u? flag
        int mRTSPWaitTimeChanged;

        void convertCodecInfo(dvb_locator_t &locator);

    public:
        static int videoDelayTimeMs;
        static int enableGlobalMute;
        static AVP_AudioOutputMode enableSettingsDolby;
        static AVP_AspectRatioMode settingsAspectRatioMode;

    public:
        status_t enableCasInfo(int enable);

    private:
        int checkRtspUncontrollableState(); // jung2604 : 20190520 : vod를 제어할수 없을때(EOF를 받았을때) 명령을 무시하도록 한다.
        status_t pause_impl(); //jung2604 : 실제 pause동작을 별도로 구현, puase()는 클라이언트로 부터 받기용

#ifdef FEATURE_SOC_AMLOGIC
	private:
		//sp<ANativeWindow> mAnativeWindow;
#endif
    };

}	// namespace android



#endif // ___IPTV_SOC_PLAYER_H__ 
