#ifndef __PIX_HEVC_MERGE_EXPORT_H_2014_08_25__
#define __PIX_HEVC_MERGE_EXPORT_H_2014_08_25__

#ifdef __cplusplus
extern "C" {
#endif

#define PT_HEVC_ENC_MAX_TARGET_BITRATE        (10*1024*1024)

#include "pix_type.h"

typedef struct _pt_hevc_au_s
{
  pix_uint64 ulDts;
  pix_uint64 ulPts;

  pix_int32 iStreamLength;
  pix_uint8 pStream[PT_HEVC_ENC_MAX_TARGET_BITRATE>>3];
}pt_hevc_au_t, *p_pt_hevc_au_t;

void PIX_GetVersionHevcMerge(pix_uint32* pMajor, pix_uint32* pMinor, pix_uint32* pDetail);
h_pt_merge_hevc PIX_CreateMergeHevcEs(pix_int32 iMode);
void PIX_DestroyMergeHevcEs(h_pt_merge_hevc handle);
pix_int32 PIX_PutMergeHevcEs(h_pt_merge_hevc handle, pix_int32 iChId, struct _pt_hevc_au_s* psEs);
pix_int32 PIX_GetMergeHevcEs(h_pt_merge_hevc handle, struct _pt_hevc_au_s* psEs);
pix_int32 PIX_SetTimeStampId(h_pt_merge_hevc handle, pix_int32 iId);
void PIX_ResetMergeHevc(h_pt_merge_hevc handle);

typedef void (*Fn_GetVersionHevcMerge)(pix_uint32* pMajor, pix_uint32* pMinor, pix_uint32* pDetail);
typedef h_pt_merge_hevc (*Fn_PIX_CreateMergeHevcEs)(pix_int32 iMode);
typedef void (*Fn_PIX_DestroyMergeHevcEs)(h_pt_merge_hevc handle);
typedef pix_int32 (*Fn_PIX_PutMergeHevcEs)(h_pt_merge_hevc handle, pix_int32 iChId, struct _pt_hevc_au_s* psAu);
typedef pix_int32 (*Fn_PIX_GetMergeHevcEs)(h_pt_merge_hevc handle, struct _pt_hevc_au_s* psAu);
typedef pix_int32 (*Fn_PIX_SetTimeStampId)(h_pt_merge_hevc handle, pix_int32 iId);
typedef void (*Fn_PIX_ResetMergeHevc)(h_pt_merge_hevc handle);

#ifdef __cplusplus
} //extern "C"
#endif

#endif
