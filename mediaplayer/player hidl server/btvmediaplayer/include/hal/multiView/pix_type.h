#ifndef __PIX_TYPE_H_2011_06_24__
#define __PIX_TYPE_H_2011_06_24__
#ifdef WIN32
#define __PT_OS_WINDOWS__                     (1)
#define __PT_OS_LINUX__                       (0)
#else
#define __PT_OS_WINDOWS__                     (0)
#define __PT_OS_LINUX__                       (1)
#endif
#define OS_ADDRESS_ALIGN_SIZE                 (8)
#define OS_ADDRESS_ALIGN_SHIFT_BIT            (3)
#define OS_ADDRESS_ALIGN_STUFFING             (OS_ADDRESS_ALIGN_SIZE - 1)
#define OS_ADDRESS_ALIGN_MASKING              (~OS_ADDRESS_ALIGN_STUFFING)

#define MEM_ALIGN_SIZE                        (64)
#define MEM_ALIGN_STUFFING                    (MEM_ALIGN_SIZE - 1)
#define MEM_ALIGN_MASKING                     (~MEM_ALIGN_STUFFING)

#define PIX_NULL                              (NULL)

#define PIX_SUCCESS                           (0)
#define PIX_FAIL                              (-1)

#define PIX_TRUE                              (1)
#define PIX_FALSE                             (0)

#define PIX_MAX_STRING_LENGTH                 (256)

typedef signed char                           pix_int8;
typedef unsigned char                         pix_uint8;
typedef signed short                          pix_int16;
typedef unsigned short                        pix_uint16;
typedef signed int                            pix_int32;
typedef unsigned int                          pix_uint32;
typedef float                                 pix_float32;
typedef double                                pix_double64;
typedef long long                             pix_int64;
typedef unsigned long long                    pix_uint64;
typedef void*                                 h_pt_merge_hevc;


#ifdef __PT_OS_WINDOWS__
#define INLINE            __inline
#else
#define INLINE            inline
#endif

#define PIX_ERROR_NOT_INITIALIZE              (-10)
#define PIX_ERROR_NOT_ENOUGH_MEMORY           (-11)
#define PIX_ERROR_NOT_FOUND                   (-12)
#define PIX_ERROR_NOT_SUPPORTED               (-13)
#define PIX_ERROR_NOT_AVAILABLE_DPB           (-14)

#define PIX_ERROR_INVALID_PARAMETER           (-20)
#define PIX_ERROR_INVALID_VALUE               (-21)
#define PIX_ERROR_INVALID_OPERATION           (-22)
#define PIX_ERROR_INVALID_HANDLE              (-23)
#define PIX_ERROR_INVALID_OUTPUT_DATA         (-24)
#define PIX_ERROR_INVALID_VPS                 (-25)
#define PIX_ERROR_INVALID_SPS                 (-26)
#define PIX_ERROR_INVALID_PPS                 (-27)
#define PIX_ERROR_INVALID_SH                  (-28)
#define PIX_ERROR_NOT_AVAILABLE_DEC_PIC       (-29)

#define PIX_ERROR_ALREADY_INITIALIZE          (-30)
#define PIX_ERROR_OPERATION_TIME_OUT          (-31)
#define PIX_ERROR_SIZE_IS_TOO_BIG             (-32)
#define PIX_ERROR_TIME_OUT                    (-33)
#define PIX_ERROR_SOCKET                      (-34)
#define PIX_ERROR_WAIT_IDR_NAL_UNIT           (-35)
#define PIX_ERROR_DECODING_TIMEOUT            (-36)
#define PIX_ERROR_NO_REF_PICTURE              (-37)
#define PIX_ERROR_NOT_ENOUGHT_DATA            (-38)
#define PIX_ERROR_NOT_FIRST_SLICE             (-39)


#if __PT_OS_ANDROID__
//#define LOG_NDEBUG 0
#define LOG_TAG "PIX-HEVC"
#include <utils/Log.h>

//#define PIX_TRACE(...) ALOGV(__VA_ARGS__)
#define PIX_TRACE(...) ALOGI(__VA_ARGS__)

#else
#if __PT_OS_LINUX__
  #define PIX_TRACE(...) printf(__VA_ARGS__)
#endif
#endif

#if __PT_OS_WINDOWS__
  #define PIX_TRACE(...) printf(__VA_ARGS__)
#endif

#endif

