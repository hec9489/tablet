// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef QI_CALCULATOR_H
#define QI_CALCULATOR_H

#ifdef __cplusplus
extern "C" {
#endif/*__cplusplus*/

extern void	qi_calculator_init();
extern void     qi_calculator_init_2(short i);
extern void	qi_calculator_term(void);

extern void	qi_calculator_register_mlr_functiion(void *MRLHandle);

extern void	qi_calculator_scan(
	const unsigned char* stream_buf,
	unsigned int buf_size,
	unsigned int packet_count, unsigned  int channel_ip);
    


/* join trial event processing */
extern void	qi_calculator_on_join_requested(
	unsigned long ip,
	unsigned short port);

extern void	qi_calculator_on_leave_requested(
	unsigned long ip,
	unsigned short port);





extern void	qi_calculator_player_start(unsigned long * channel_ip, unsigned short count,unsigned char bmulti)  ;
extern void	qi_calculator_player_stop(unsigned long * channel_ip, unsigned short count,unsigned char bmulti)   ;


#ifdef __cplusplus
}/*extern "C"*/
#endif/*__cplusplus*/

#endif/*QI_CALCULATOR_H*/
