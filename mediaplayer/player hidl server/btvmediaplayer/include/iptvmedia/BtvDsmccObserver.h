// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef ANDROID_BTVDSMCCOBSERVER_H
#define ANDROID_BTVDSMCCOBSERVER_H

#ifdef __cplusplus

#include <utils/Errors.h>
#include <utils/RefBase.h>
#include <utils/threads.h>
#include <utils/Vector.h>

namespace android
{

typedef struct {
	int32_t appType;
	int32_t organizationId;
	int32_t applicationId;
	int32_t dsmccPid;	 
	bool	isServiceBound;
	char	name[32];
	int32_t controlCode;
	int32_t priority;
	char	initialPath[32];
} ApplicationInfo;

typedef struct {
    int32_t                    eventType;
    int32_t                    serviceId;
    int32_t                    demux;
    char                       uri[128];
    Vector<ApplicationInfo>    appLists;
} SignalEvent;


class BtvDsmccObserver: public RefBase
{
public:
	BtvDsmccObserver() {}
	virtual			~BtvDsmccObserver() {}
	virtual void	dsmccSignalEvent(int32_t dsmccEventType, char* dsmccRootPath, SignalEvent* event) = 0;

private:
	Mutex				mNotifyLock;
};

};	// namespace android

#endif // __cplusplus

#endif // ANDROID_BTVDSMCCOBSERVER_H

