// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef ANDROID_BTVMEDIAPLAYEROBSERVER_H
#define ANDROID_BTVMEDIAPLAYEROBSERVER_H

#ifdef __cplusplus

#include <utils/Errors.h>
#include <utils/RefBase.h>
#include <utils/threads.h>


namespace android
{

class BtvMediaPlayerObserver: public RefBase
{
public:
	BtvMediaPlayerObserver() : mDeviceID(0) {}
	virtual			~BtvMediaPlayerObserver() {}
	virtual void	notifyCallback(int32_t msg, int32_t ext1, int32_t ext2, const int8_t* obj) = 0;
	virtual void	dataCallback(int32_t msg, int32_t ext1, int32_t ext2, const int8_t* dataPtr) = 0;

	void setDeviceID(int deviceID)
	{
		mDeviceID = deviceID;
	}

	int getDeviceID()
	{
		return mDeviceID;
	}

private:
	Mutex				mNotifyLock;
	int 				mDeviceID;
};

};	// namespace android

#endif // __cplusplus

#endif // ANDROID_BTVMEDIAPLAYERINTERFACE_H

