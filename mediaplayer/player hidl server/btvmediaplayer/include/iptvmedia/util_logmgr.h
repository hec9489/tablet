// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#include <time.h>


#ifndef UTIL_LOGMGR_H_
#define UTIL_LOGMGR_H_

#ifdef __cplusplus
extern "C" {
#endif


#define MODULE_TAG_MEDIA		"MEDIA_MGR"
#define MODULE_TAG_SERVICE		"SVC_MGR"
#define MODULE_TAG_SYSTEM		"SYS_MGR"
#define MODULE_TAG_WMS			"WMS_MGR"
#define MODULE_TAG_APPMGR		"APP_MGR"
#define MODULE_TAG_WINMGR		"WIN_MGR"
#define MODULE_TAG_SKAF			"SKAF_MODULE"
#define MODULE_TAG_UPDATE		"UPDATE_MGR"
#define MODULE_TAG_SVCAGENT	"SVC_AGENT"
#define LOG_FILE_DIR		"/btv_home/log/"

// log option ?�정 �?
#define LOG_OPT_ALL		(0xFFFF)
#define LOG_OPT_STDOUT	(0x0001<<0)		/* printf out */
#define LOG_OPT_FILEOUT	(0x0001<<1)		/* file out*/
#define LOG_OPT_LOGCATOUT	(0x0001<<2)		/* logcat out*/

// 로그 ?�일 ?�션
#define LOG_FILE_START_PATH		"/DATA/SKAF/skaf_home/run/"
#define LOG_FILE_START_NAME 	"_started"

typedef enum
{
	e_LOGFILETYPE_TIME = 0,
	e_LOGFILETYPE_BYTES,
	e_LOGFILETYPE_MAX
} eLOGFILETYPE_Option;



// systemp level ?�정 �?
#define LOG_SLEVELALL	(0xFFFF)
#define LOG_SLEVEL00	(0x0001<<0)		/* system is unusable */
#define LOG_SLEVEL01	(0x0001<<1)		/* action must be taken immediately */
#define LOG_SLEVEL02	(0x0001<<2)		/* critical conditions */
#define LOG_SLEVEL03	(0x0001<<3)		/* error conditions */
#define LOG_SLEVEL04	(0x0001<<4)		/* warning conditions */
#define LOG_SLEVEL05	(0x0001<<5)		/* normal but significant condition */
#define LOG_SLEVEL06	(0x0001<<6)		/* informational */
#define LOG_SLEVEL07	(0x0001<<7)		/* debug-level messages */
#define LOG_SLEVEL08	(0x0001<<8)
#define LOG_SLEVEL09	(0x0001<<9)
#define LOG_SLEVEL10	(0x0001<<10)
#define LOG_SLEVEL11	(0x0001<<11)
#define LOG_SLEVEL12	(0x0001<<12)
#define LOG_SLEVEL13	(0x0001<<13)
#define LOG_SLEVEL14	(0x0001<<14)
#define LOG_SLEVEL15	(0x0001<<15)

// module level ?�정 �?
#define LOG_MLEVELALL	(0xFFFF)
#define LOG_MLEVEL00	(0x0001<<0)
#define LOG_MLEVEL01	(0x0001<<1)
#define LOG_MLEVEL02	(0x0001<<2)
#define LOG_MLEVEL03	(0x0001<<3)
#define LOG_MLEVEL04	(0x0001<<4)
#define LOG_MLEVEL05	(0x0001<<5)
#define LOG_MLEVEL06	(0x0001<<6)
#define LOG_MLEVEL07	(0x0001<<7)
#define LOG_MLEVEL08	(0x0001<<8)
#define LOG_MLEVEL09	(0x0001<<9)
#define LOG_MLEVEL10	(0x0001<<10)
#define LOG_MLEVEL11	(0x0001<<11)
#define LOG_MLEVEL12	(0x0001<<12)
#define LOG_MLEVEL13	(0x0001<<13)
#define LOG_MLEVEL14	(0x0001<<14)
#define LOG_MLEVEL15	(0x0001<<15)


//#undef 	LOG
#undef 	LOGL
#undef  LOGEX
#undef  LOGEXL
#undef  LOGSRC
#undef  LOGSRCL
#undef  LOGDUMP


#define __FILENAME__ (strrchr(__FILE__,'/')+1) 

#ifdef FEATURE_USED_LOG
//#define LOG(args...)						log_Print(LOG_SLEVELALL, LOG_MLEVELALL, ##args)
#define LOGL(SLevel, MLevel, args...)		log_Print(SLevel, MLevel, ##args)

#define LOGE(args...)						log_PrintEx(LOG_SLEVELALL, LOG_MLEVELALL, ##args)//jung2604 add
#define LOGW(args...)						log_PrintEx(LOG_SLEVELALL, LOG_MLEVELALL, ##args)//jung2604 add
#define LOGV(args...)						log_PrintEx(LOG_SLEVELALL, LOG_MLEVELALL, ##args)//jung2604 add
#define LOGI(args...)						log_PrintEx(LOG_SLEVELALL, LOG_MLEVELALL, ##args)//jung2604 add
#define LOGD(args...)						log_PrintEx(LOG_SLEVELALL, LOG_MLEVELALL, ##args)//jung2604 add
#define LOGEX(args...)						log_PrintEx(LOG_SLEVELALL, LOG_MLEVELALL, ##args)
#define LOGEXL(SLevel, MLevel, args...)		log_PrintEx(SLevel, MLevel, ##args)
#define LOGSRC(args...)						log_Printsrc(LOG_SLEVELALL, LOG_MLEVELALL, __FILENAME__, __LINE__, ##args)
#define LOGSRCL(SLevel, MLevel, args...)	log_Printsrc(SLevel, MLevel, __FILENAME__, __LINE__, ##args)
#define LOGSRCEX(args...)					log_PrintsrcEx(LOG_SLEVELALL, LOG_MLEVELALL, __FILENAME__, __LINE__, ##args)
#define LOGSRCLEX(SLevel, MLevel, args...)	log_PrintsrcEx(SLevel, MLevel, __FILENAME__, __LINE__, ##args)
#define LOGDUMP(pData, size)				log_PrintDump(pData, size)
#else
//#define LOG(args...)							// printf ?� ?�일?�게 출력
#define LOGL(SLevel, MLevel, arg123)
#define LOGE(args...) //jung2604 add
#define LOGW(args...) //jung2604 add
#define LOGV(args...) //jung2604 add
#define LOGI(args...) //jung2604 add
#define LOGD(args...) //jung2604 add
#define LOGEX(args...)							// 로그 "\n" ?�함?�여 출력
#define LOGEXL(SLevel, MLevel, args...)
#define LOGSRC(args...)							// 로그???�스?�일�??�인 출력
#define LOGSRCL(SLevel, MLevel, args...)
#define LOGSRCEX(args...)
#define LOGSRCLEX(SLevel, MLevel, args...)
#define LOGDUMP(pData, size)
#endif


#define LOGFILE(strfilePath, args...)		log_PrintLogFile(strfilePath,  __FILE__, __LINE__, ##args)
#define LOGFILEEEX(strfilePath, args...)	log_PrintLogFileEx(strfilePath,  __FILE__, __LINE__, ##args)

// Thread ?�버�????�용
#define LOG_THREAD(args...) LOGEXL(LOG_SLEVEL00, LOG_MLEVELALL,  ##args)


// Time ?�버�??�용
#define USED_TIME_LOG    1
#ifdef USED_TIME_LOG
#define LOG_TIME_LOG(args...) LOGEXL(LOG_SLEVEL06, LOG_MLEVELALL,  ##args)
#define LOG_TIME(time, args...) log_PrintTime(LOG_SLEVEL06, LOG_MLEVELALL, __FILE__, __LINE__, (int)time, ##args)

#define	LOG_TIME_STARTV(args...)	\
{ \
	struct timeval time; \
	long lStart, lEnd; \
	gettimeofday(&time, NULL);\
	LOG_TIME((-1), ##args); \
	lStart = time.tv_sec*1000 + time.tv_usec/1000;

#define	LOG_TIME_START()	\
{ \
	struct timeval time; \
	long lStart, lEnd; \
	gettimeofday(&time, NULL);\
	lStart = time.tv_sec*1000 + time.tv_usec/1000;

#define LOG_TIME_END(args...)	\
	gettimeofday(&time, NULL);\
	lEnd = time.tv_sec*1000 + time.tv_usec/1000;\
	LOG_TIME((lEnd -lStart), ##args); \
}
#else //#ifdef USED_TIME_LOG
#define LOG_TIME_LOG(args...)
#define LOG_TIME(time, args...)
#define	LOG_TIME_START()
#define	LOG_TIME_START(args...)
#define LOG_TIME_END(args...)
#endif //#ifdef USED_TIME_LOG


#define USED_LOG_INI    0


int 	log_Initialize(const char *strModuleName);
void 	log_uninitialize();

int 	log_LoadOption(void);
int 	log_SaveOption(void);
void 	log_FileOption(eLOGFILETYPE_Option eLogFileType, int nValue);

// control
long 	log_GetOption();
int 	log_SetOption(long uOption);

long 	log_GetSystemLevel();
int 	log_SetSystemLevel(long uSystemLevel);

long 	log_GetModuleLevel();
int 	log_SetModuleLevel(long uModuleLevel);

// print log
void 	log_Print(long uSystemLevel, long uModuleLevel, const char *format, ...);
void 	log_PrintM(long uSystemLevel, long uModuleLevel, const char *format, ...);
void 	log_PrintEx(long uSystemLevel, long uModuleLevel, const char *format, ...);
void 	log_Printsrc(long uSystemLevel, long uModuleLevel, const char *strFilename, int nline, const char *format, ...);
void 	log_PrintsrcEx(long uSystemLevel, long uModuleLevel, const char *strFilename, int nline, const char *format, ...);
void 	log_PrintTime(long uSystemLevel, long uModuleLevel, const char *strFilename, int nline, int nUsedTimeMS, const char *format, ...);
void 	log_PrintDump(const void* pData, long dwSize);

void 	log_PrintLogFile(const char *strLogFilePath, const char *strFilename, int nline, const char *format, ...);
void 	log_PrintLogFileEx(const char *strLogFilePath, const char *strFilename, int nline, const char *format, ...);

#ifdef __cplusplus
}
#endif
#endif /* UTIL_LOGMGR_H_ */
