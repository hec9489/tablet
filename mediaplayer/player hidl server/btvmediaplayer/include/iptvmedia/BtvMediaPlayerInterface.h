// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef ANDROID_BTVMEDIAPLAYERINTERFACE_H
#define ANDROID_BTVMEDIAPLAYERINTERFACE_H

#ifdef __cplusplus

#include <utils/Errors.h>
#include <utils/RefBase.h>
#include <utils/threads.h>

#include "BtvDsmccObserver.h"

namespace android
{

class Parcel;
class IMemory;

// callback mechanism for passing messages to IptvMediaPlayer object
typedef void (*notify_callback_f)(void* cookie, int msg, int ext1, int ext2, const int8_t* obj);
typedef void (*data_callback_f)(void* cookie, int msg, int ext1, int ext2, const int8_t* dataPtr);
typedef void (*dsmcc_callback_f)(void* cookie, int32_t dsmccEventType, char* dsmccRootPath, SignalEvent* event);
typedef void (*audio_programupdate_callback_f)(void* cookie, char* filepath, int updateType, int audioPid);

// abstract base class - use BtvMediaPlayerInterface
class IptvMediaPlayerBase: public RefBase
{
public:
									IptvMediaPlayerBase() : mCookie(0), mNotifyCallback(0), mDataCallback(0), mDsmccCallback(0), mAudioProgramUpdateCallback(0){}
	virtual							~IptvMediaPlayerBase() {}

    virtual status_t				open(const char* dataXML) = 0;
	virtual status_t 				tuneTV(const char* dataXML) = 0;
	virtual status_t				closeTV() = 0;
	virtual status_t				bindingFilter(const char *dataXML) = 0;
	virtual status_t				releaseFilter(const char *dataXML) = 0;
	virtual status_t 				changeAudioChannel(const char* dataXML) = 0;
	virtual status_t 				changeAudioOnOff(const char* dataXML) = 0;
	virtual status_t				getPlayerMode() = 0;
	virtual status_t				getPlayerStatus() = 0;
	virtual status_t				getCurrentPosition() = 0;
	virtual status_t 				setWindowSize(const char* dataXML) = 0;
	virtual status_t				play() = 0;
	virtual status_t				pause() = 0;
	virtual status_t 				resume() = 0;
	virtual status_t 				seek(const char* dataXML, int flag) = 0;
	virtual status_t 				pauseAt(const char* dataXML) = 0;
	virtual status_t 				trick(const char* dataXML) = 0;
	virtual status_t 				stop() = 0;
	virtual status_t 				close() = 0;
	virtual status_t				invoke(const Parcel& request, Parcel *reply) = 0;
	virtual	status_t				reset() = 0;

	// Invoke a generic method on the player by using opaque parcels
	// for the request and reply.
	//
	// @param request Parcel that is positioned at the start of the
	//				  data sent by the java layer.
	// @param[out] reply Parcel to hold the reply data. Cannot be null.
	// @return OK if the call was successful.
	//virtual status_t 		invoke(const Parcel& request, Parcel *reply) = 0;

	void setCallbacks(void* cookie, notify_callback_f notifyFunc, data_callback_f dataFunc)
	{
		Mutex::Autolock autoLock(mNotifyLock);
		mCookie = cookie;
		mNotifyCallback = notifyFunc;
		mDataCallback = dataFunc;
	}

	void setDsmccCallbacks(void* cookie, dsmcc_callback_f dsmccFunc)
	{
		Mutex::Autolock autoLock(mNotifyLock);
		mCookie = cookie;
		mDsmccCallback = dsmccFunc;
	}

	void setAudioProgramUpdateCallbacks(void* cookie, audio_programupdate_callback_f audio_updateFunc)
	{
		Mutex::Autolock autoLock(mNotifyLock);
		mCookie = cookie;
		mAudioProgramUpdateCallback = audio_updateFunc;
	}

	void sendEvent(int32_t msg, int32_t ext1=0, int32_t ext2=0, const int8_t* obj = NULL)
	{
		Mutex::Autolock autoLock(mNotifyLock);
		if (mNotifyCallback)
			mNotifyCallback(mCookie, msg, ext1, ext2, obj);
	}

	void sendDataEvent(int32_t msg, int32_t ext1, int32_t ext2, const int8_t* dataPtr)
	{
		Mutex::Autolock autoLock(mNotifyLock);
		if (mDataCallback)
			mDataCallback(mCookie, msg, ext1, ext2, dataPtr);
	}

	void sendDsmccEvent(int32_t dsmccEventType, char* dsmccRootPath, SignalEvent* event)
	{
		Mutex::Autolock autoLock(mNotifyLock);
		if (mDsmccCallback)
			mDsmccCallback(mCookie, dsmccEventType, dsmccRootPath, event);
	}

	void sendAudioProgramUpdateEvent(char* filepath, int updateType, int audioPid)
	{
		Mutex::Autolock autoLock(mNotifyLock);
		if (mAudioProgramUpdateCallback)
			mAudioProgramUpdateCallback(mCookie, filepath, updateType, audioPid);
	}

	void setDeviceID(int deviceID)
	{
		mDeviceID = deviceID;
	}

	int getDeviceID()
	{
		return mDeviceID;
	}

private:
	friend class BtvMediaPlayerService;

	Mutex				mNotifyLock;
	void*				mCookie;
	notify_callback_f	mNotifyCallback;
	data_callback_f 	mDataCallback;
	dsmcc_callback_f 	mDsmccCallback;	
	audio_programupdate_callback_f 	mAudioProgramUpdateCallback;		
	int 				mDeviceID;


};

// Implement this class for iptv media players
class BtvMediaPlayerInterface : public IptvMediaPlayerBase
{
public:
	virtual						~BtvMediaPlayerInterface() { }
};

};	// namespace android

#endif // __cplusplus

#endif // ANDROID_BTVMEDIAPLAYERINTERFACE_H

