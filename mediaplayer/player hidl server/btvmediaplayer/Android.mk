# Copyright 2019 SK Broadband Co., LTD.
# Author: Kwon Soon Chan (sysc0507@sk.com)

LOCAL_PATH:= $(call my-dir)

##############################
include $(CLEAR_VARS)
LOCAL_MODULE := libdrm2
LOCAL_SRC_FILES := ./libs/casdrmImpl/lib/drm/release/libdrm2.a
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_SUFFIX := .a
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

##############################
include $(CLEAR_VARS)
LOCAL_MODULE := libDCSDB
LOCAL_SRC_FILES := ./libs/casdrmImpl/lib/drm/release/libDCSDB.a
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_SUFFIX := .a
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

##############################
include $(CLEAR_VARS)
LOCAL_MODULE := libDRMCrypto
LOCAL_SRC_FILES := ./libs/casdrmImpl/lib/drm/CALibrary/libDRMCrypto.a
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_SUFFIX := .a
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

##############################
#include $(CLEAR_VARS)
#LOCAL_MODULE := libzoovod
#LOCAL_SRC_FILES := ./libs/zooinnetrtspclient/lib/libzoovod.so
#LOCAL_MODULE_TAGS := optional
#LOCAL_MODULE_SUFFIX := .so
#LOCAL_MODULE_CLASS := SHARED_LIBRARIES
#LOCAL_PROPRIETARY_MODULE := true
#include $(BUILD_PREBUILT)

##############################
include $(CLEAR_VARS)
LOCAL_MODULE := libCASClient
LOCAL_SRC_FILES := ./libs/casdrmImpl/lib/cas/libCASClient.so
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

##############################
#include $(CLEAR_VARS)
#LOCAL_MODULE := libqi_calculator
#LOCAL_SRC_FILES := ./libs/casdrmImpl/lib/cas/libqi_calculator.so
#LOCAL_MODULE_TAGS := optional
#LOCAL_MODULE_SUFFIX := .so
#LOCAL_MODULE_CLASS := SHARED_LIBRARIES
#LOCAL_PROPRIETARY_MODULE := true
#include $(BUILD_PREBUILT)

##############################
#include $(CLEAR_VARS)
#LOCAL_MODULE := libtvs_hevc_stream_merge
#LOCAL_SRC_FILES := ./libs/multiView/lib/libtvs_hevc_stream_merge.so
#LOCAL_MODULE_TAGS := optional
#LOCAL_MODULE_SUFFIX := .so
#LOCAL_MODULE_CLASS := SHARED_LIBRARIES
#LOCAL_PROPRIETARY_MODULE := true
#include $(BUILD_PREBUILT)

##############################
#include $(CLEAR_VARS)
#LOCAL_MODULE := libc++_shared
#LOCAL_SRC_FILES := ./libs/mlr_client/lib/libc++_shared.so
#LOCAL_MODULE_TAGS := optional
#LOCAL_MODULE_SUFFIX := .so
#LOCAL_MODULE_CLASS := SHARED_LIBRARIES
#LOCAL_PROPRIETARY_MODULE := true
#include $(BUILD_PREBUILT)

##############################
#include $(CLEAR_VARS)
#LOCAL_MODULE := libPixMLRClient
#LOCAL_SRC_FILES := ./libs/mlr_client/lib/libPixMLRClient.so
#LOCAL_MODULE_TAGS := optional
#LOCAL_MODULE_SUFFIX := .so
#LOCAL_MODULE_CLASS := SHARED_LIBRARIES
#LOCAL_PROPRIETARY_MODULE := true
#include $(BUILD_PREBUILT)

##############################
#include $(CLEAR_VARS)
#LOCAL_MODULE := libpxMlrIsqms
#LOCAL_SRC_FILES := ./libs/mlr_client/lib/libpxMlrIsqms.so
#LOCAL_MODULE_TAGS := optional
#LOCAL_MODULE_SUFFIX := .so
#LOCAL_MODULE_CLASS := SHARED_LIBRARIES
#LOCAL_PROPRIETARY_MODULE := true
#include $(BUILD_PREBUILT)

###########################################################################################
# libskcasdrm
###########################################################################################
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_CFLAGS:= -DFEATURE_USED_LOG -DEXIT_SUPPORT -DSPTEK_EXTENSION

LOCAL_SRC_FILES:= \
	libs/casdrmImpl/utile_socket.c \
	libs/casdrmImpl/drmp_port_impl.c \
	libs/casdrmImpl/wise_port_impl.c \
	libs/casdrmImpl/drm_crypto_impl.c \
	libs/casdrmImpl/mcas-port-impl.c \
	libs/casdrmImpl/encBlowFish.c \
	libs/casdrmImpl/wise_skdrm.c \
	libs/casdrmImpl/scs-trigger-handler.c \
	libs/casdrmImpl/jsmn.c \
	libs/casdrmImpl/utile_wscs.c \
	libs/casdrmImpl/json.c \
    iptvmedia/libbtvmediaplayerservice/util_logmgr.c
    

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/include \
	$(LOCAL_PATH)/include/hal \
	$(LOCAL_PATH)/include/iptvmedia \
	$(LOCAL_PATH)/libs/casdrmImpl	\
	$(LOCAL_PATH)/libs/casdrmImpl/include

LOCAL_CPP_INCLUDES := \
	$(LOCAL_PATH)/include \
	$(LOCAL_PATH)/include/hal

LOCAL_SHARED_LIBRARIES := \
	libutils \
        libsystemproperty \
	libcurl 
	
LOCAL_WHOLE_STATIC_LIBRARIES := \
        libdrm2 \
	libDCSDB \
	libDRMCrypto


LOCAL_LDLIBS := -llog

LOCAL_MODULE := libskcasdrm

LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_SHARED_LIBRARY)

###########################################################################################
# libiconv.so
###########################################################################################
#include $(CLEAR_VARS)

#LOCAL_MODULE := libiconv

#LOCAL_CFLAGS := \
#  -Wno-multichar \
#  -DANDROID \
#  -DLIBDIR=\"c\" \
#  -DBUILDING_LIBICONV \
#  -DIN_LIBRARY

#LOCAL_SRC_FILES := \
#	libs/iconv/libcharset/lib/localcharset.c \
#	libs/iconv/lib/iconv.c \
#	libs/iconv/lib/relocatable.c

#LOCAL_C_INCLUDES += \
#  $(LOCAL_PATH)/libs/iconv/include \
#  $(LOCAL_PATH)/libs/iconv/libcharset \
#  $(LOCAL_PATH)/libs/iconv/lib \
#  $(LOCAL_PATH)/libs/iconv/libcharset/include \
#  $(LOCAL_PATH)/libs/iconv/srclib

#LOCAL_MODULE_TAGS := optional    
#LOCAL_PROPRIETARY_MODULE := true
#include $(BUILD_SHARED_LIBRARY)

###########################################################################################
# libdsmcc.so
###########################################################################################
include $(CLEAR_VARS)

LOCAL_MODULE := libdsmcc

LOCAL_CFLAGS += -Wall -Wunused-parameter -Wmismatched-tags
LOCAL_CFLAGS += -D__LINUX__
LOCAL_LDLIBS := -llog

LOCAL_SRC_FILES := \
  libs/libdsmcc/dsmcc.c \
  libs/libdsmcc/dsmcc-receiver.c \
  libs/libdsmcc/dsmcc-descriptor.c \
  libs/libdsmcc/dsmcc-carousel.c \
  libs/libdsmcc/dsmcc-cache.c \
  libs/libdsmcc/dsmcc-biop.c \
  libs/libdsmcc/dsmcc-util.c

LOCAL_C_INCLUDES :=  \
  $(LOCAL_PATH)/libs/libdsmcc \
  external/zlib \

LOCAL_SHARED_LIBRARIES += libz \
  
LOCAL_MODULE_TAGS := optional    
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_SHARED_LIBRARY)

###########################################################################################
# libswdmx.so
###########################################################################################
include $(CLEAR_VARS)

LOCAL_MODULE := libswdmx

LOCAL_CFLAGS += -Wall -O2 -Wunused-parameter -Wmismatched-tags

LOCAL_SRC_FILES := \
  libs/libdmx/demux.c \
  libs/libdmx/demux_section.c \
  libs/libdmx/demux_pes.c

LOCAL_C_INCLUDES :=  \
  $(LOCAL_PATH)/libs/libdmx
 
LOCAL_SHARED_LIBRARIES := liblog

LOCAL_MODULE_TAGS := optional    
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_SHARED_LIBRARY)


###########################################################################################
# libdbg.so
###########################################################################################
include $(CLEAR_VARS)

LOCAL_MODULE := libdbg

LOCAL_CFLAGS += -Wall -Wunused-parameter -Wmismatched-tags
LOCAL_LDLIBS := -llog

LOCAL_SRC_FILES := \
  libs/libmediaplayer_primitive/dbg/dbg.c

LOCAL_C_INCLUDES :=  \
  $(LOCAL_PATH)/libs/libmediaplayer_primitive/dbg/ 

LOCAL_MODULE_TAGS := optional    
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_SHARED_LIBRARY)

###########################################################################################
# librtputils.so
###########################################################################################
include $(CLEAR_VARS)

LOCAL_MODULE := librtputils

LOCAL_LDLIBS := -llog

LOCAL_CFLAGS += -Wall -Wunused-parameter -Wmismatched-tags 
LOCAL_CFLAGS += -DSPTEK_EXTENSION

LOCAL_SRC_FILES := \
  libs/libmediaplayer_primitive/utils/RtpUtils.c

LOCAL_C_INCLUDES :=  \
				$(LOCAL_PATH)/include \
				$(LOCAL_PATH)/libs/libmediaplayer_primitive/dbg \
                $(LOCAL_PATH)/libs/pix_ads \
				$(LOCAL_PATH)/libs/mlr_client

LOCAL_SHARED_LIBRARIES += libutils 
	
LOCAL_MODULE_TAGS := optional    
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_SHARED_LIBRARY)


###########################################################################################
# librtp.so
###########################################################################################
include $(CLEAR_VARS)

LOCAL_MODULE := librtp

LOCAL_LDLIBS := -llog
LOCAL_SHARED_LIBRARIES += libbtvhalmgr \
			libswdmx \
			librtputils \
	    libPixADS

LOCAL_CFLAGS += -Wall -Wunused-parameter -Wmismatched-tags
LOCAL_CFLAGS += -DSPTEK_EXTENSION

LOCAL_SRC_FILES := \
  libs/libmediaplayer_primitive/rtp/rtp.c

LOCAL_C_INCLUDES :=  \
				$(LOCAL_PATH)/include \
				$(LOCAL_PATH)/libs/libmediaplayer_primitive \
				$(LOCAL_PATH)/libs/libmediaplayer_primitive/dbg \
				$(LOCAL_PATH)/libs/libmediaplayer_primitive/utils \
				$(LOCAL_PATH)/libs/mlr_client \
                $(LOCAL_PATH)/libs/pix_ads \
				$(LOCAL_PATH)/libs/libdmx \
				$(LOCAL_PATH)/libs/libdsmcc \
				$(LOCAL_PATH)/../btvhalmanager \
				$(TOP)/vendor/skb/framework/hal/interface

			
LOCAL_MODULE_TAGS := optional    
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_SHARED_LIBRARY)


###########################################################################################
# libmultiview
###########################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_CERTIFICATE := platform

LOCAL_MODULE := libmultiview

LOCAL_LDLIBS := -llog

LOCAL_CFLAGS := -g -O2 -DSAT_BAND_EU -g -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -D_GNU_SOURCE $(LOCAL_LOG_FLAG)
LOCAL_CFLAGS += -Wall -Wunused-parameter -Wmismatched-tags
LOCAL_CFLAGS += -D__LINUX__ -DFEATURE_USED_LOG
LOCAL_CFLAGS += -DSPTEK_EXTENSION
LDFLAGS = -nodefaultlibs -lc -lm -ldl
			
LOCAL_SRC_FILES:= \
				libs/libmediaplayer_primitive/multiview/multiViewRtp.c \
				libs/libmediaplayer_primitive/multiview/source/multiview_demuxer.c \
				libs/libmediaplayer_primitive/multiview/source/multiview_muxer.c \
				libs/libmediaplayer_primitive/multiview/source/multiview_util.c

LOCAL_LDLIBS := -llog \
			-lavcodec \
			-lavformat \
			-lavutil 
			
LOCAL_SHARED_LIBRARIES += libcutils \
            libtvs_hevc_stream_merge \
			libbtvhalmgr \
			librtputils
		
LOCAL_C_INCLUDES :=  \
				$(LOCAL_PATH)/include \
				$(LOCAL_PATH)/libs/libmediaplayer_primitive \
				$(LOCAL_PATH)/libs/libmediaplayer_primitive/dbg \
				$(LOCAL_PATH)/libs/libmediaplayer_primitive/utils \
				$(LOCAL_PATH)/libs/mlr_client \
                $(LOCAL_PATH)/libs/pix_ads \
				$(LOCAL_PATH)/libs/libdmx \
				$(LOCAL_PATH)/libs/libdsmcc \
				$(LOCAL_PATH)/../btvhalmanager \
				$(TOP)/vendor/skb/framework/hal/interface \
				$(VENDOR_SDK_INCLUDES)

LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_SHARED_LIBRARY)


###########################################################################################
# libmediaplayer_primitive
###########################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_CERTIFICATE := platform

LOCAL_MODULE := libmediaplayer_primitive
	
LOCAL_LOG_FLAG := -DLOG_VERBOSE -DLOG_DEBUG -DMEASURE_ZAPPING	
	
LOCAL_LDLIBS := -llog \
			-lavcodec \
			-lavformat \
			-lavutil 

LOCAL_CFLAGS := -g -O2 -DSAT_BAND_EU -g -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -D_GNU_SOURCE $(LOCAL_LOG_FLAG)
LOCAL_CFLAGS += -Wall -Wunused-parameter -Wmismatched-tags
LOCAL_CFLAGS += -D__LINUX__ -DFEATURE_USED_LOG
LOCAL_CFLAGS += -DSPTEK_EXTENSION

LDFLAGS = -nodefaultlibs -lc -lm -ldl

LOCAL_SRC_FILES:= \
				libs/libmediaplayer_primitive/dvb_primitive.c \
				libs/libmediaplayer_primitive/dvb_iptuner.c \
				libs/libmediaplayer_primitive/dvb_filetuner.c \
				libs/libmediaplayer_primitive/dvb_sectionfilter.c \
				libs/libmediaplayer_primitive/IptvImplement.c \
				libs/libmediaplayer_primitive/dtvcc.c

LOCAL_SHARED_LIBRARIES += libcutils \
            libtvs_hevc_stream_merge \
            libsystemproperty \
            libiconv \
            libdsmcc \
			libqi_calculator \
			libPixMLRClient \
			libbtvhal_common \
			libswdmx \
			libbtvhalmgr \
			libdbg \
			librtputils \
			librtp \
			libmultiview \
			libPixADS

LOCAL_C_INCLUDES :=  \
				$(LOCAL_PATH)/include \
				$(LOCAL_PATH)/libs/iconv/include \
				$(LOCAL_PATH)/libs/libdsmcc \
				$(LOCAL_PATH)/libs/mlr_client \
                $(LOCAL_PATH)/libs/pix_ads \
				$(LOCAL_PATH)/libs/libmediaplayer_primitive \
				$(LOCAL_PATH)/libs/libmediaplayer_primitive/dbg \
				$(LOCAL_PATH)/libs/libmediaplayer_primitive/rtp \
				$(LOCAL_PATH)/libs/libdmx \
				$(LOCAL_PATH)/libs/libdsmcc \
				$(TOP)/vendor/skb/framework/hal/interface \
				$(LOCAL_PATH)/../btvhalmanager \
				system/core/libutils/include \
				$(VENDOR_SDK_INCLUDES)

LOCAL_CPP_INCLUDES :=  \
				$(LOCAL_PATH)/include \
				$(LOCAL_PATH)/libs/libmediaplayer_primitive \
				$(LOCAL_PATH)/libs/libmediaplayer_primitive/dbg \
				$(LOCAL_PATH)/libs/libmediaplayer_primitive/rtp \
				$(LOCAL_PATH)/libs/libdmx \
				$(TOP)/vendor/skb/framework/hal/interface \
                $(LOCAL_PATH)/libs/pix_ads \
				$(LOCAL_PATH)/../btvhalmanager

ifeq ($(VENDOR_SOC), amlogic)

#LOCAL_CFLAGS += -DFEATURE_SOC_AMLOGIC

#LOCAL_SHARED_LIBRARIES += libgui_vendor libnativewindow

#LOCAL_C_INCLUDES += $(TOP)/frameworks/native/libs/nativewindow/include

#LOCAL_CPP_INCLUDES += $(TOP)/frameworks/native/libs/nativewindow/include

endif
				
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_SHARED_LIBRARY)

###########################################################################################
# libiptvmediahal
###########################################################################################
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_CERTIFICATE := platform

LOCAL_CFLAGS:= -DFEATURE_USED_LOG -DSPTEK_EXTENSION -fPIC -D_POSIX_SOURCE
LOCAL_CFLAGS += -Wall -Wunused-parameter -Wmismatched-tags
LOCAL_CFLAGS += -D__LINUX__

LOCAL_SRC_FILES:= \
	libs/libiptvmediahal/IptvSocPlayer.cpp \
	libs/libiptvmediahal/IptvTimedEventQueue.cpp \
	libs/zooinnetrtspclient/play_rtsp.c \
	libs/casdrmImpl/mcas-reg-dummy-impl.c 
	
LOCAL_SHARED_LIBRARIES := \
	libmediaplayer_primitive \
	libutils \
	libbinder \
	libsystemproperty \
	libskcasdrm \
	libcutils \
	libzoovod \
	libCASClient \
	libcurl \
	libdsmcc \
	libqi_calculator\
	libbtvhal_common \
	libswdmx \
	libbtvhalmgr

LOCAL_LDLIBS := -llog

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/include \
	$(LOCAL_PATH)/include/hal \
	$(LOCAL_PATH)/libs/casdrmImpl	\
	$(LOCAL_PATH)/libs/casdrmImpl/include \
	$(LOCAL_PATH)/include/iptvmedia \
	$(LOCAL_PATH)/libs/libmediaplayer_primitive \
	$(LOCAL_PATH)/libs/libdsmcc \
	$(LOCAL_PATH)/libs/libmediaplayer_primitive/dbg \
	$(TOP)/vendor/skb/framework/hal/interface \
	$(LOCAL_PATH)/../btvhalmanager \
	$(LOCAL_PATH)/libs/libdmx \
	system/core/base/include \
	frameworks/native/include \
	libnativehelper/include_jni \
	$(LOCAL_PATH)/libs/mlr_client \
                $(LOCAL_PATH)/libs/pix_ads \
	vendor/synaptics/aistb2/sdk/include \
	$(VENDOR_SDK_INCLUDES)

LOCAL_CPP_INCLUDES := \
	$(LOCAL_PATH)/include \
	$(LOCAL_PATH)/include/hal \
	$(LOCAL_PATH)/include/iptvmedia \
	$(TOP)/vendor/skb/framework/hal/interface \
	$(LOCAL_PATH)/../btvhalmanager \
	$(LOCAL_PATH)/libs/libmediaplayer_primitive \
	$(LOCAL_PATH)/libs/libdmx

ifeq ($(VENDOR_SOC), amlogic)

#LOCAL_CFLAGS += -DFEATURE_SOC_AMLOGIC

#LOCAL_SHARED_LIBRARIES += libgui_vendor libnativewindow

#LOCAL_C_INCLUDES += $(TOP)/frameworks/native/libs/nativewindow/include

#LOCAL_CPP_INCLUDES += $(TOP)/frameworks/native/libs/nativewindow/include

endif

LOCAL_MODULE := libiptvmediahal


LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_SHARED_LIBRARY)


###########################################################################################
# libbtvmediaplayerservice
###########################################################################################

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_CERTIFICATE := platform

LOCAL_CFLAGS:= -DFEATURE_USED_LOG -DSPTEK_EXTENSION
LOCAL_CFLAGS += -Wall -Wunused-parameter -Wmismatched-tags
LOCAL_CFLAGS += -D__LINUX__

LOCAL_SRC_FILES:=               \
    iptvmedia/libbtvmediaplayerservice/BtvMediaPlayerService.cpp \
    iptvmedia/libbtvmediaplayerservice/util_logmgr.c

LOCAL_SHARED_LIBRARIES :=     		\
	libcutils             			\
	libutils              			\
	libbinder             			\
	libiptvmediahal \
	libsystemproperty \
	libti2-audiomirror-proxy-native \
	libbtvhalmgr


LOCAL_LDLIBS := -llog

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/include \
	$(LOCAL_PATH)/libs/libmediaplayer_primitive \
	$(LOCAL_PATH)/libs/mlr_client \
                $(LOCAL_PATH)/libs/pix_ads \
	$(LOCAL_PATH)/libs/libdsmcc \
	$(LOCAL_PATH)/libs/libdmx \
	$(LOCAL_PATH)/libs/libmediaplayer_primitive/dbg \
	$(TOP)/vendor/skb/framework/hal/interface \
	$(LOCAL_PATH)/../btvhalmanager \
	vendor/synaptics/aistb2/sdk/include \
	$(VENDOR_SDK_INCLUDES)
##$(LOCAL_PATH)/../tvservice/hal/control \

LOCAL_CPP_INCLUDES := \
	$(LOCAL_PATH)/include \
	$(TOP)/vendor/skb/framework/hal/interface \
	$(LOCAL_PATH)/../btvhalmanager \
	$(LOCAL_PATH)/libs/libmediaplayer_primitive

ifeq ($(VENDOR_SOC), amlogic)

LOCAL_CFLAGS += -DFEATURE_SOC_AMLOGIC

#LOCAL_LDLIBS += -lEGL

#LOCAL_SHARED_LIBRARIES += libgui_vendor libui libnativewindow

#LOCAL_C_INCLUDES += $(TOP)/frameworks/native/libs/nativewindow/include

#LOCAL_CPP_INCLUDES += $(TOP)/frameworks/native/libs/nativewindow/include

endif


LOCAL_MODULE:= libbtvmediaplayerservice

LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_SHARED_LIBRARY)

###########################################################################################
# btf_media_jni.btf
###########################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libbtf_media_jni.btf
LOCAL_C_INCLUDES := \
    $(JNI_H_INCLUDE) \
    frameworks/native/libs/gui/include \
    $(TOP)/frameworks/native/libs/nativewindow/include \
    frameworks/base/core/jni/include \
    frameworks/native/include \
    $(VENDOR_SDK_INCLUDES)

LOCAL_CFLAGS += -Wall 
LOCAL_CFLAGS += -DFEATURE_USED_LOG -D_ANDROID_ -D__LINUX__
LOCAL_LDLIBS    := -llog -landroid -landroid_runtime -lEGL
LOCAL_SRC_FILES := jni/btf_media_jni.cpp

LOCAL_SHARED_LIBRARIES := \
    com.skb.btvservice@1.0 \
    android.hardware.graphics.bufferqueue@1.0 \
    libcutils \
    liblog \
    libhidlbase\
    libhidltransport\
    libutils \
    libui \
    libgui \
    libbinder

ifeq ($(VENDOR_SOC), amlogic)

LOCAL_CFLAGS += -DFEATURE_SOC_AMLOGIC
LOCAL_SHARED_LIBRARIES += libamgralloc_ext@2

LOCAL_C_INCLUDES += \
    hardware/amlogic/gralloc/amlogic

else ifeq ($(VENDOR_SOC), synaptics)

LOCAL_C_INCLUDES += \
    vendor/synaptics/common/libsideband

LOCAL_CFLAGS += -DFEATURE_SOC_SYNAPTICS

LOCAL_SHARED_LIBRARIES += \
    libsideband

endif

LOCAL_MODULE_TAGS := optional    
#LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_SHARED_LIBRARY)




