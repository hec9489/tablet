// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef ANDROID_BTVMEDIAPLAYERSERVICE_H
#define ANDROID_BTVMEDIAPLAYERSERVICE_H

#include <utils/Log.h>
#include <utils/threads.h>
#include <utils/Errors.h>
#include <utils/KeyedVector.h>
#include <utils/Vector.h>
#include <utils/RefBase.h>

#ifdef FEATURE_SOC_AMLOGIC
//#include <gui/Surface.h>
#endif

#include <iptvmedia/BtvMediaPlayerInterface.h>
#include <iptvmedia/BtvMediaPlayerServiceInterface.h>

#include <hal/IptvSocPlayer.h>
#include <iptvmedia/util_logmgr.h>
#include <iptvmedia/BtvMediaPlayerObserver.h>
#include <iptvmedia/BtvDsmccObserver.h>
#include <iptvmedia/BtvMediaCommunicationObserver.h>
#include <iptvmedia/BtvAudioProgramUpdateObserver.h>


// callback???BtvMediaPlayerService??서 받아??main callback client???
// observer 쪽으??분배??는 방식??로 ??다. 
// callback???모든 Player??static??게 ??러????????도????다.
namespace android
{

class BtvMediaPlayerService: public RefBase, BtvMediaPlayerServiceInterface
{
//	class Client;

public:
    static sp<BtvMediaPlayerService> getInstance();	
	IptvSocPlayer*					selectIptvSocPlayer(int deviceID);
//	static 	void					  instantiate();

	virtual	status_t				connect(sp<BtvMediaPlayerObserver>& client, int deviceID);

	void							removeClient(wp<BtvMediaPlayerObserver> client);
	void 							setClient(const wp<BtvMediaPlayerObserver> client);
	sp<BtvMediaPlayerObserver> 	getClientByDeviceID(int deviceID);
	sp<BtvDsmccObserver> 			getDsmccObserver();
	virtual status_t 				setDsmccClient(sp<BtvDsmccObserver>& client);	
	sp<BtvMediaCommunicationObserver> 			getMediaCommunicationObserver();
	virtual status_t 				setMediaCommunicationListenerClient(sp<BtvMediaCommunicationObserver>& client);
	status_t						enableCasInfo(bool enable);
	sp<BtvAudioProgramUpdateObserver> 			getAudioProgramUpdateObserver();	
	virtual status_t 				setAudioProgramUpdateClient(sp<BtvAudioProgramUpdateObserver>& client);		

/*
			void 					removeObserverClient(wp<Client> client);
			// client manipulating
			void 					setClient(const wp<Client> client);
			void 					setObserverClient(const wp<Client> client);
			sp<Client> 				getClientByDeviceID(int deviceID);
			sp<Client> 				getClientByPid(pid_t pid);
*/			
	static	void 					notifyCallbackMain(void* cookie, int msg, 
									   int ext1, int ext2, const int8_t* obj);
	static	void 					dataCallbackMain(void* cookie, int msg, int ext1, int ext2, 
									   const int8_t* dataPtr);
	static	void 					notifyCallbackPip1(void* cookie, int msg, 
									   int ext1, int ext2, const int8_t* obj);
	static 	void 					dataCallbackPip1(void* cookie, int msg, int ext1, int ext2, 
									   const int8_t* dataPtr);
	static  void 					dsmccCallback(void *cookie, int32_t dsmccEventType, 
										char* dsmccRootPath, SignalEvent* event);
	static  void 					familyStateCallback(void *cookie, int32_t state);
	static  void 					audioProgramUpdateCallback(void *cookie, char* filepath, int32_t updateType, 
										int32_t audioPid);

// Observer??device????러????을 ????으므??	
//			SortedVector< wp<Client> >	mObserverClients;

	virtual	void					disconnect(int deviceID);
	virtual status_t				open(int deviceID, const char* dataXML);
	virtual status_t				tuneTV(int deviceID, const char* dataXML);
	virtual status_t				parseMultiTuneTVInfo(HTuner_IPTV_Info *ptTuneInfo, const char* dataXML);
	virtual HTuner_IPTV_Info		parseTuneTVInfo(const char* dataXML);
	virtual HPlayer_IPTV_Info		parsePlayerInfo(const char* dataXML);
	virtual void					parseAudioChangeInfo(const char* dataXML, int *nAID, int *nACodecID);
	virtual void					printTuneTVInfo(HTuner_IPTV_Info stTuneInfo);
	virtual status_t				closeTV(int deviceID);
	virtual status_t				bindingFilter(int deviceID, const char* dataXML);
	virtual status_t				releaseFilter(int deviceID, const char* dataXML);
	virtual status_t				changeAudioChannel(int deviceID, const char* dataXML);
	virtual status_t				changeAudioOnOff(int deviceID, const char* dataXML);
	virtual status_t				getPlayerMode(int deviceID);
	virtual status_t				getPlayerStatus(int deviceID);
	virtual status_t				getCurrentPosition(int deviceID);
	virtual status_t				setWindowSize(int deviceID, const char* dataXML);
	virtual status_t				play(int deviceID );
	virtual status_t				pause(int deviceID );
	virtual status_t				resume(int deviceID );
	virtual status_t				seek(int deviceID, const char* dataXML, bool pause);
	virtual status_t				pauseAt(int deviceID, const char* dataXML);
	virtual status_t				keepLastFrame(int deviceID, bool flag);	
	virtual status_t				trick(int deviceID, const char* dataXM);
	virtual status_t				stop(int deviceID);
	virtual status_t				close(int deviceID);
	virtual status_t				invoke(int deviceID, const Parcel& request, Parcel* reply);
	virtual status_t				setDummys(int deviceID, const char* dummys);
	virtual status_t				setPlayerSize(int deviceID, int nLeft, int nTop, int nWidth, int nHeight);
	virtual status_t 				setPlayerSurfaceView(int deviceID, uint32_t nativWindows);
	
#ifdef FEATURE_SOC_AMLOGIC
	//virtual status_t 				setPlayerSurface(int deviceID, const sp<Surface> &surface);
#endif

	virtual status_t 				startDmxFilter(int deviceID, int32_t pid, int32_t tid);
	virtual status_t 				stopDmxFilter(int deviceID, int32_t pid, int32_t tid);	
	virtual void 					setBtvMediaPlayer(int deviceID);
	virtual IptvSocPlayer*			getBtvPlayer();

private:
									BtvMediaPlayerService();
	virtual							~BtvMediaPlayerService();

	mutable		Mutex						mLock;
	mutable		Mutex						mServiceLock;

//				int32_t						mNextConnId;
				SortedVector< wp<BtvMediaPlayerObserver> >	mClients;

				wp<BtvDsmccObserver>   mDsmccClient;
				wp<BtvMediaCommunicationObserver>  mMediaCommunicationClient;
				wp<BtvAudioProgramUpdateObserver>  mAudioProgramUpdateClient;				
				
				IptvSocPlayer 			mIptvSocPlayerMain;
				IptvSocPlayer 			mIptvSocPlayerPip1;
				IptvSocPlayer*			mpIptvSocPlayer;
	static      sp<BtvMediaPlayerService> instance_;
	
	
		// echwang : 2020.05.08 : + S	
		static Vector<int> mSupportedResList ;		
		static std::string getDisplaySupportedResolution();
		static BTF_DISPLAY_Resolution getBtfDisplayResolution(int resolution); 
		// echwang : 2020.05.08 : + E			

public:
		//jung2604 : 20190412 : 온가족 이어폰용..Thread
		static pthread_t threadEarPhone;
		static int isPlayingFamilyEarPhoneThread;
		static int isPlugFamilyEarPhone;
		static void familyEarPhoneThread(void *ThreadParam);
		void audioSnoop(int *pIsPlayingEarPhoneThread);
		
		//화면 Delay관련..
		static int videoDelayTimeMs;
		static void externFunctionStateChangeCheckThread(void *ThreadParam);
		static void externFunctionStateChangeCheckForGlobalMuteThread(void *ThreadParam); // 아리야 전용..

		//전체 mute 관련
		static int enableGlobalMute;

		//설정 -> 돌비 설정관련
		static AVP_AudioOutputMode enableSettingsDolby;

        //설정 -> 화면 비율유지 관련
        static AVP_AspectRatioMode settingsAspectRatioMode;

        static void attSednToPtsThread(void *ThreadParam);
        void attSendToPts();
    public:
		virtual void onCasInfoEvent(int32_t type, std::string casInfo);

};

}; // namespace android


#endif // ANDROID_BTVMEDIAPLAYERSERVICE_H
