// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

//#define LONG_NDEBUG 0
#define LOG_TAG "BtvMediaPlayerService"
#include <utils/Log.h>

#include <cutils/atomic.h>
#include <cutils/properties.h>
/*
#include <binder/IPCThreadState.h>
#include <binder/IServiceManager.h>
#include <binder/MemoryHeapBase.h>
#include <binder/MemoryBase.h>
*/
#include <utils/Errors.h>	// for status_t

#include <dvb_primitive.h>
#include <hal/iptvhaldef.h>

#include "BtvMediaPlayerService.h"

//jung2604 : 20190402 : 온가족 이어폰
#include <auto_mirror/audioMirrorProxyNative.h>
#include <linux/un.h>
#include <hal/systemproperty.h>
#include <stddefs.h>
#include <unistd.h>
#include <pwd.h>


#include <btv_hal.h>

//#include <SocInterface.h>
// display resolution : TODO : 위의 SocInterface.h에 정의 되어있다..일단..직접 define해준다.
#define		BTF_UI_DISPLAY_MODE_720P				1
#define		BTF_UI_DISPLAY_MODE_1080I				3
#define		BTF_UI_DISPLAY_MODE_1080P				5
#define		BTF_UI_DISPLAY_MODE_3840x2160P30HZ		9
#define		BTF_UI_DISPLAY_MODE_3840x2160P60HZ		10
#define		BTF_UI_DISPLAY_MODE_AUTO				11

#define VIDEO_DELAY_TIME_BT_EARPHONE 200
#define VIDEO_DELAY_TIME_FAMILY_EARPHONE 250

//#define NONOLOG

#ifdef NONOLOG
#define LOGSRC(...)
#endif

namespace android
{
void string_version() {
	char stVersion[256] ;
	sprintf(stVersion, "sptek__version__libiptvmediaplayerservice : 0.5.97");
}

sp<BtvMediaPlayerService> BtvMediaPlayerService::instance_ = NULL;
Vector<int> BtvMediaPlayerService::mSupportedResList = {};

sp<BtvMediaPlayerService> BtvMediaPlayerService::getInstance() {
    if (instance_ != NULL) {
        return instance_;
    }

     instance_ = new BtvMediaPlayerService();
     return instance_;
}

BtvMediaPlayerService::BtvMediaPlayerService()
{
	LOGSRC("[%s] called", __FUNCTION__);

	char 	retBuf[128] = {0};
	int	 	retLen = 0;

	retLen = property_get(QUALITY_THRESHOLD_KEY, retBuf, "-1");

	if(retLen < 0 || strcmp(retBuf, "-1")==0){
		LOGSRC("+============================+\n");
		LOGSRC("|  property_set : %s\n", QUALITY_THRESHOLD_KEY);
		LOGSRC("+============================+\n" );
		property_set(QUALITY_THRESHOLD_KEY, QUALITY_THRESHOLD_DEFAULT_STR);
	}

	retLen = property_get(REFERENCE_QUALITY_KEY, retBuf, "-1");

	if(retLen < 0 || strcmp(retBuf, "-1")==0){
		LOGSRC("+============================+\n");
		LOGSRC("|  property_set : %s\n", REFERENCE_QUALITY_KEY);
		LOGSRC("+============================+\n" );
		property_set(REFERENCE_QUALITY_KEY, REFERENCE_QUALITY_DEFAULT_STR);
	}

	retLen = property_get(ALLOW_REFERENCE_KEY, retBuf, "-1");

	if(retLen < 0 || strcmp(retBuf, "-1")==0){
		LOGSRC("+============================+\n");
		LOGSRC("|  property_set : %s\n", ALLOW_REFERENCE_KEY);
		LOGSRC("+============================+\n" );
		property_set(ALLOW_REFERENCE_KEY, ALLOW_REFERENCE_DEFAULT_STR);
	}

	// musespy : IptvSocPlayer initiates in here
	mIptvSocPlayerMain.initCheck(this, IptvSocPlayer::mBTV_DEVICE_MAIN);
	mIptvSocPlayerPip1.initCheck(this, IptvSocPlayer::mBTV_DEVICE_PIP1);

	mpIptvSocPlayer = NULL;

	// callback registering
	mIptvSocPlayerMain.setCallbacks(this, notifyCallbackMain, dataCallbackMain);
	mIptvSocPlayerPip1.setCallbacks(this, notifyCallbackPip1, dataCallbackPip1);

	// dsmcc callback registering
	mIptvSocPlayerMain.setDsmccCallbacks(this, dsmccCallback);

	// audio program update callback registering
	mIptvSocPlayerMain.setAudioProgramUpdateCallbacks(this, audioProgramUpdateCallback);

    //jung2604 : 20190412 : 온가족 이어폰용..Thread
    if(threadEarPhone == NULL) {
        int ret = TI2_open();
        LOGSRC("[%s] TI2_open : ret = %d", __FUNCTION__, ret);

        if(ret == 0) {
            isPlayingFamilyEarPhoneThread = TRUE;
            if (pthread_create(&threadEarPhone, NULL,
                               reinterpret_cast<void *(*)(void *)>(&BtvMediaPlayerService::familyEarPhoneThread), (void *)this) == 0) {
				LOGSRC("[%s] earphone pthread_create ", __FUNCTION__);
            } else isPlayingFamilyEarPhoneThread = FALSE;
        }
    }

    {
        //systemproperty 라이브러리 로드용..
        char propertyBuf[10];
        get_systemproperty("PROPERTY__ETHERNET_MAC", propertyBuf, 10);
    }

    //화면 Delay관련..
    pthread_t threadChangeVideoDelayTimeCheck;
    if (pthread_create(&threadChangeVideoDelayTimeCheck, NULL,
                       reinterpret_cast<void *(*)(void *)>(&BtvMediaPlayerService::externFunctionStateChangeCheckThread), (void *)this) != 0) {
        LOGSRC("[%s] threadChangeVideoDelayTimeCheck pthread_create fail!!", __FUNCTION__);
    }

	pthread_t threadChangeVideoDelayTimeCheckForGlobalMute;
	if (pthread_create(&threadChangeVideoDelayTimeCheckForGlobalMute, NULL,
					   reinterpret_cast<void *(*)(void *)>(&BtvMediaPlayerService::externFunctionStateChangeCheckForGlobalMuteThread), (void *)this) != 0) {
		LOGSRC("[%s] externFunctionStateChangeCheckForGlobalMuteThread pthread_create fail!!", __FUNCTION__);
	}

    pthread_t threadATTSendToPts;
    if (pthread_create(&threadATTSendToPts, NULL,
                       reinterpret_cast<void *(*)(void *)>(&BtvMediaPlayerService::attSednToPtsThread), (void *)this) == 0) {
		pthread_detach(threadATTSendToPts);
    } else LOGSRC("[%s] ATT pthread_create fail!!", __FUNCTION__);

}


// static
void BtvMediaPlayerService::notifyCallbackMain(void *cookie, int msg, int ext1, int ext2, 
											   const int8_t* obj)
{
	// ext1 is deviceID
	// search for client
	BtvMediaPlayerService* p = static_cast<BtvMediaPlayerService*>(cookie);

	// Normal Client Callback
	sp<BtvMediaPlayerObserver> client = p->getClientByDeviceID(ext1);
	if (client != 0)
	{
		LOGSRC("[%s][%d] notify (%d, %d, %d)", __FUNCTION__, __LINE__, 
			msg, ext1, ext2);
		client->notifyCallback(msg, ext1, ext2, obj);
	}
}

// static
void BtvMediaPlayerService::dataCallbackMain(void *cookie, int msg, int ext1, int ext2, 
											  const int8_t* dataPtr)
{
	// ext1 is deviceID
	// search for client
	BtvMediaPlayerService* p = static_cast<BtvMediaPlayerService*>(cookie);

	// Normal Client Callback
	sp<BtvMediaPlayerObserver> client = p->getClientByDeviceID(ext1);
	if (client != 0)
	{
		LOGSRC("[%s][%d] dataCallback (%d, %d, %d)", __FUNCTION__, __LINE__, 
			msg, ext1, ext2);
		client->dataCallback(msg, ext1, ext2, dataPtr);	 

	}
}

// static
void BtvMediaPlayerService::notifyCallbackPip1(void *cookie, int msg, int ext1, int ext2, 
											   const int8_t* obj)
{
	// ext1 is deviceID
	// search for client
	BtvMediaPlayerService* p = static_cast<BtvMediaPlayerService*>(cookie);

	// Normal Client Callback
	sp<BtvMediaPlayerObserver> client = p->getClientByDeviceID(ext1);
	if (client != 0)
	{
		LOGSRC("[%s][%d] notify (%d, %d, %d)", __FUNCTION__, __LINE__, 
			msg, ext1, ext2);
		client->notifyCallback(msg, ext1, ext2, obj);
	}
}

// static
void BtvMediaPlayerService::dataCallbackPip1(void *cookie, int msg, int ext1, int ext2, 
											  const int8_t* dataPtr)
{
	// ext1 is deviceID
	// search for client
	BtvMediaPlayerService* p = static_cast<BtvMediaPlayerService*>(cookie);

	// Normal Client Callback
	sp<BtvMediaPlayerObserver> client = p->getClientByDeviceID(ext1);
	if (client != 0)
	{
		LOGSRC("[%s][%d] dataCallback (%d, %d, %d)", __FUNCTION__, __LINE__, 
			msg, ext1, ext2);
		client->dataCallback(msg, ext1, ext2, dataPtr);	 

	}
}

// static
void BtvMediaPlayerService::dsmccCallback(void *cookie, int32_t dsmccEventType, char* dsmccRootPath, SignalEvent* event)
{
	LOGSRC("[%s][%d] dsmccCallback (%d, %s)", __FUNCTION__, __LINE__, dsmccEventType, dsmccRootPath);

	BtvMediaPlayerService* p = static_cast<BtvMediaPlayerService*>(cookie);
	sp<BtvDsmccObserver> client = p->getDsmccObserver();
	if (client != 0)
	{
		LOGSRC("[%s][%d] client find... dsmccCallback (%d, %s)", __FUNCTION__, __LINE__, dsmccEventType, dsmccRootPath);
		client->dsmccSignalEvent(dsmccEventType, dsmccRootPath, event);	 
	}
}

void BtvMediaPlayerService::familyStateCallback(void *cookie, int32_t state)
{
	LOGSRC("[%s][%d] familyStateCallback (%d)", __FUNCTION__, __LINE__, state);
	BtvMediaPlayerService* p = static_cast<BtvMediaPlayerService*>(cookie);
	sp<BtvMediaCommunicationObserver> client = p->getMediaCommunicationObserver();
	if (client != 0)
	{
		LOGSRC("[%s][%d] client find... familyStateCallback (%d)", __FUNCTION__, __LINE__, state);
		client->familyStateEvent(state);
	}
}
	
// static
void BtvMediaPlayerService::audioProgramUpdateCallback(void *cookie, char* filepath, int32_t updateType, int32_t audioPid)
{
	LOGSRC("[%s][%d] audioProgramUpdateCallback (%s, %d, %d)", __FUNCTION__, __LINE__, filepath, updateType, audioPid);

	BtvMediaPlayerService* p = static_cast<BtvMediaPlayerService*>(cookie);
	sp<BtvAudioProgramUpdateObserver> client = p->getAudioProgramUpdateObserver();
	if (client != 0)
	{
		LOGSRC("[%s][%d] client find... audioProgramUpdateCallback (%s, %d, %d)", __FUNCTION__, __LINE__, filepath, updateType, audioPid);
		client->audioProgramUpdate(filepath, updateType, audioPid);	 
	}
}

BtvMediaPlayerService::~BtvMediaPlayerService()
{
	LOGSRC("[%s] called", __FUNCTION__);

	//jung2604 : 20190412 : 온가족 이어폰용..Thread
	if(threadEarPhone) {
		LOGSRC("[%s]  earphone pthread..join", __FUNCTION__);
		isPlayingFamilyEarPhoneThread = FALSE;
		pthread_join(threadEarPhone, NULL);
		threadEarPhone = NULL;
	}
	TI2_close();
	LOGSRC("[%s] TI2_close", __FUNCTION__);

}


sp<BtvMediaPlayerObserver> BtvMediaPlayerService::getClientByDeviceID(int deviceID)
{
    Mutex::Autolock lock(mLock);
    LOGSRC("[%s][%d] xxxx 1", __FUNCTION__, __LINE__);
    for (int i = 0, n = mClients.size(); i < n; ++i) 
    {
        LOGSRC("[%s][%d] n=%d i=%d", __FUNCTION__, __LINE__, n, i);
        sp<BtvMediaPlayerObserver> c = mClients[i].promote();

        if (c != 0)
        {
        	int id = c->getDeviceID();
            LOGSRC("[%s][%d] getDeviceID=%d deviceID=%d", __FUNCTION__, __LINE__, id, deviceID);			
        	if (id == deviceID)
        		return c;
        }
    }

    LOGSRC("[%s][%d] xxxx 2", __FUNCTION__, __LINE__);
    return NULL;
}

sp<BtvDsmccObserver> BtvMediaPlayerService::getDsmccObserver()
{
    Mutex::Autolock lock(mLock);
    LOGSRC("[%s][%d] xxxx 1", __FUNCTION__, __LINE__);
    sp<BtvDsmccObserver> c = mDsmccClient.promote();	
    return c;	
}

sp<BtvMediaCommunicationObserver> BtvMediaPlayerService::getMediaCommunicationObserver()
{
    Mutex::Autolock lock(mLock);
    LOGSRC("[%s][%d] xxxx 1", __FUNCTION__, __LINE__);
    sp<BtvMediaCommunicationObserver> c = mMediaCommunicationClient.promote();
    return c;	
}

sp<BtvAudioProgramUpdateObserver> BtvMediaPlayerService::getAudioProgramUpdateObserver()
{
    Mutex::Autolock lock(mLock);
    LOGSRC("[%s][%d] xxxx 1", __FUNCTION__, __LINE__);
    sp<BtvAudioProgramUpdateObserver> c = mAudioProgramUpdateClient.promote();	
    return c;	
}

void BtvMediaPlayerService::setClient(wp<BtvMediaPlayerObserver> client)
{
	mClients.add(client);
}

void BtvMediaPlayerService::removeClient(wp<BtvMediaPlayerObserver> client)
{
	LOGSRC("[%s] called", __FUNCTION__);

	Mutex::Autolock lock(mLock);
	LOGSRC("[%s][%d] ...", __FUNCTION__, __LINE__);
	mClients.remove(client);
	LOGSRC("[%s][%d] ...", __FUNCTION__, __LINE__);
}

IptvSocPlayer* BtvMediaPlayerService::selectIptvSocPlayer(int deviceID)
{
	switch (deviceID)
	{
	case IptvSocPlayer::mBTV_DEVICE_MAIN:
	{
		return &mIptvSocPlayerMain;
	}	
	break;
	case IptvSocPlayer::mBTV_DEVICE_PIP1:
	{
		return &mIptvSocPlayerPip1;
	}	
	break;
	default:
	{
		return &mIptvSocPlayerMain;
	}	
	break;
	}
}

status_t BtvMediaPlayerService::setDsmccClient(sp<BtvDsmccObserver>& client)
{
	int nRet = 0;
	wp<BtvDsmccObserver> w = client;
	mDsmccClient = w;
	return nRet;	
}

status_t BtvMediaPlayerService::setMediaCommunicationListenerClient(sp<BtvMediaCommunicationObserver>& client)
{
	int nRet = 0;
	wp<BtvMediaCommunicationObserver> w = client;
	mMediaCommunicationClient = w;
	return nRet;	
}

status_t BtvMediaPlayerService::enableCasInfo(bool enable)
{
	IptvSocPlayer* mainIptvPlayer = selectIptvSocPlayer(0);
	if(mainIptvPlayer != NULL /*&& mainIptvPlayer->InGetPlayerMode() == PLAYER_MODE_IPTV && mainIptvPlayer->InGetPlayerStatus() != PLAYER_STATUS_STOP && mainIptvPlayer->player.isMultiview == false*/) {
		return mainIptvPlayer->enableCasInfo(enable);
	}

	return -1;
}


status_t BtvMediaPlayerService::setAudioProgramUpdateClient(sp<BtvAudioProgramUpdateObserver>& client)
{
	int nRet = 0;
	wp<BtvAudioProgramUpdateObserver> w = client;
	mAudioProgramUpdateClient = w;
	return nRet;	
}


status_t BtvMediaPlayerService::connect(sp<BtvMediaPlayerObserver>& client, int deviceID)
{
	int nRet = 0;
	LOGSRC("[%s] called, deviceID=%d", __FUNCTION__, deviceID);
	if (deviceID < 0)
	{
		LOGSRC("[%s][%d]rejected (invalid deviceID %d).", __FUNCTION__, __LINE__, 
			deviceID);
		return NO_INIT;
	}

	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	bool pass = false;
#if 0 //remove ELP
#ifndef FEATURE_SOC_AMLOGIC //TODO : amlogic 용 임시 제거

	{ // jung2604 : first media에서 미디어를 재생하였는지 체크
		char retBuf[128] = {0};
		int retLen = 0;

		retLen = property_get("vendor.skb.btv.boot.early.liveplay", retBuf, "");

		if (retLen > 0 && strcmp(retBuf, "1") == 0) {
			LOGSRC("+============================+");
			LOGSRC("|  EarlyLivePlayback : vendor.skb.btv.boot.early.liveplay");
			LOGSRC("+============================+");

			retLen = property_get("sys.boot_completed", retBuf, "0");

			pass = (retLen > 0 && strcmp(retBuf, "1") == 0);
		}
	}
	
#endif
#endif
	
	if (mpIptvSocPlayer)
	{
		if(!pass) {
			LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);
			if (mpIptvSocPlayer->getPlayerStatus() != PLAYER_STATUS_NONE)
			{
				LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);
				if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_IPTV)
				{
					LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);
					mpIptvSocPlayer->closeTV();
				}

				if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_RTSP)
				{
					LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);
					mpIptvSocPlayer->close();
				}

				if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_NORMAL_FILE)
				{
					LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);
					mpIptvSocPlayer->close();
				}
				mpIptvSocPlayer->sendEvent(7, deviceID, 1);
			}
		}
	}
	
	if (deviceID == 0)
	{
		IptvSocPlayer *pPlayer = selectIptvSocPlayer(1);
		if (pPlayer)
		{
			if(!pass) {
				LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);
				if (pPlayer->getPlayerStatus() != PLAYER_STATUS_NONE)
				{
					LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);
					if (pPlayer->getPlayerMode() == PLAYER_MODE_IPTV)
					{
						LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);
						pPlayer->closeTV();
					}

					if (pPlayer->getPlayerMode() == PLAYER_MODE_RTSP)
					{
						LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);
						pPlayer->close();
					}

					if (pPlayer->getPlayerMode() == PLAYER_MODE_NORMAL_FILE)
					{
						LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);
						pPlayer->close();
					}
					pPlayer->sendEvent(7, 1, 1);
				}
			}
		}
	}

	if(deviceID == 0 && mpIptvSocPlayer != NULL) {
		if (mpIptvSocPlayer->getAuthChecked() == 0) {
			mpIptvSocPlayer->CertCheck();
		}
	}

	sp<BtvMediaPlayerObserver> cli;
	cli = getClientByDeviceID(deviceID);
	if(cli == NULL){
		wp<BtvMediaPlayerObserver> w = client;
		setClient(w);
	}

	return nRet;
}

void BtvMediaPlayerService::disconnect(int deviceID)
{
	LOGSRC("[%s] called", __FUNCTION__);
	
	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);
	
	LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);
	if (mpIptvSocPlayer)
	{
		LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);
		if (mpIptvSocPlayer->getPlayerStatus() != PLAYER_STATUS_NONE)
		{
			LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);
			if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_IPTV)
			{
				LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);
				mpIptvSocPlayer->closeTV();
			}
	
			if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_RTSP)
			{
				LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);
				mpIptvSocPlayer->close();
			}
	
			if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_NORMAL_FILE)
			{
				LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);
				mpIptvSocPlayer->close();
			}
		}
	}
/*
	sp<BtvMediaPlayerObserver> client;
	client = getClientByDeviceID(deviceID);
	if(client){
		wp<BtvMediaPlayerObserver> w = client;
		removeClient(w);
	}
*/
	LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);

}

void BtvMediaPlayerService::setBtvMediaPlayer(int deviceID)
{
	mpIptvSocPlayer = selectIptvSocPlayer(deviceID);
}

IptvSocPlayer* BtvMediaPlayerService::getBtvPlayer()
{
	return mpIptvSocPlayer;
}

status_t BtvMediaPlayerService::setPlayerSize(int deviceID, int nLeft, int nTop, int nWidth, int nHeight)
{
	setBtvMediaPlayer(deviceID);

	LOGSRC("[%s] called", __FUNCTION__);
	LOGSRC("[%s] left=%d, nTop=%d, nWidth=%d, nHeight=%d", __FUNCTION__, nLeft, nTop, nWidth, nHeight);

	int callingPid = 0;//getCallingPid();

	mpIptvSocPlayer->setWindowSize_l(nLeft, nTop, nWidth, nHeight);

	return OK;
}
 
status_t BtvMediaPlayerService::setPlayerSurfaceView(int deviceID, uint32_t nativWindows)
{
	LOGI("%s %d", __FUNCTION__, __LINE__);
	LOGI("%s %d", __FUNCTION__, __LINE__);
	LOGSRC("BtvMediaPlayerService::setPlayerSurfaceView ");

	setBtvMediaPlayer(deviceID);

	mpIptvSocPlayer->setPlayerSurfaceView_l(deviceID, nativWindows);
	return OK;
}

#ifdef FEATURE_SOC_AMLOGIC
#if 0
status_t BtvMediaPlayerService::setPlayerSurface(int deviceID, const sp<android::Surface> &surface) {
	LOGI("%s %d", __FUNCTION__, __LINE__);

	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	mpIptvSocPlayer->setPlayerSurface_l(deviceID, surface);
	return OK;
}
#endif
#endif

status_t BtvMediaPlayerService::open(int deviceID, const char* dataXML)
{
	int nRet;
	LOGSRC("[%s] called", __FUNCTION__);

	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	LOGSRC("[%s][%d] dataXML=%s", __FUNCTION__, __LINE__, dataXML);
	// native service coding start....
	if (!mpIptvSocPlayer)
		return NO_INIT;

	if (mpIptvSocPlayer)
	{
		if (mpIptvSocPlayer->getPlayerStatus() != PLAYER_STATUS_NONE)
		{
			if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_IPTV)
			{
				LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);
				mpIptvSocPlayer->closeTV();
			}
		}
	}
	
	HPlayer_IPTV_Info stPlayInfo;
	LOGSRC("%s %d", __FUNCTION__, __LINE__);
	stPlayInfo = parsePlayerInfo(dataXML);
	LOGSRC("%s %d", __FUNCTION__, __LINE__);

	nRet = mpIptvSocPlayer->open_l(stPlayInfo);
//	if (nRet == 0)
//		mpIptvSocPlayer->mCallingPointer = (void*)this;

	// TODO : jung2604 : PIP에서 소리가 나오고 있을때 main에서는 소리가 나오면 안된다..세팅값을 넘기는게 제일 좋으나 일단 mute 처리를 해준다.
    if (mpIptvSocPlayer->mDeviceID == 0) {
        IptvSocPlayer *pipIptvPlayer = selectIptvSocPlayer(1);
        if (pipIptvPlayer->InGetPlayerStatus() != PLAYER_STATUS_NONE && pipIptvPlayer->InGetPlayerStatus() != PLAYER_STATUS_STOP) {
            if (pipIptvPlayer->player.locator_head != NULL && !pipIptvPlayer->player.save_mute_enable) {
                mpIptvSocPlayer->changeAudioOnOff_l(0);
            }
        }
    }

	LOGSRC("%s %d", __FUNCTION__, __LINE__);	
	return nRet;
}

int BtvMediaPlayerService::parseMultiTuneTVInfo(HTuner_IPTV_Info *ptTuneInfo, const char* dataXML)
{
	char *word;
	char *buffer;
	char *brkt = NULL;
	char *sep="\n";
	char szBuffer[4096];

	LOGI("%s %d", __FUNCTION__, __LINE__);

	strcpy(szBuffer, dataXML);
	/* get url field */

	buffer = (char*) &szBuffer[0];

	int i;
	for(i = 0 ; i < MULTIVIEW_MAX_NUMBER ; i++) {
		word = strtok_r(buffer, sep, &brkt);
		buffer = NULL;
		if(word != NULL) {
			ptTuneInfo[i] = parseTuneTVInfo(word);
		} else {
			LOGI("++ data is NULL, count : %d", i);
			return i;
		}
	}

	return i;
}

HTuner_IPTV_Info BtvMediaPlayerService::parseTuneTVInfo(const char* dataXML)
{
	HTuner_IPTV_Info stTuneInfo;
	char *word;
	char *buffer;
	char *brkt = NULL;
	char *sep=";";
	char szBuffer[4096];

	LOGSRC("%s %d", __FUNCTION__, __LINE__);

	strcpy(szBuffer, dataXML);
	/* get url field */

	buffer = (char*) &szBuffer[0];

	LOGSRC("%s %d %s", __FUNCTION__, __LINE__, buffer);
	word = strtok_r(buffer, sep, &brkt);
	strcpy(stTuneInfo.multicast_addr, word);

	/* get port field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.multicast_port = atoi(word);
	LOGSRC("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.multicast_port);	
	/* get vpid field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.video_pid = atoi(word);
	LOGSRC("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.video_pid);
	/* get apid field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.audio_pid = atoi(word);
	LOGSRC("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.audio_pid);
	/* get pcrpid field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.pcr_pid = atoi(word);
	LOGSRC("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.pcr_pid);
	/* get video stream field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.video_stream = atoi(word);
	LOGSRC("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.video_stream);
	/* get audio stream field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.audio_stream = atoi(word);
	LOGSRC("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.audio_stream);
	/* get ca pid field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.ca_pid = atoi(word);
	LOGSRC("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.ca_pid);
	/* get ca systemid field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.ca_systemid = atoi(word);
	LOGSRC("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.ca_systemid);
	/* get channel number field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.channel_num = atoi(word);
	LOGSRC("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.channel_num);
	/* get video resolution field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.res= atoi(word);
	LOGSRC("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.res);
	
	/* get audio mute in playfield */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.audioenable= atoi(word);
	LOGSRC("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.audioenable);

	/* last frame ??정 */
	word = strtok_r(NULL, sep, &brkt);
	if(word && strlen(word) == 1) stTuneInfo.enableLastFrame = atoi(word);
	else stTuneInfo.enableLastFrame = 1;

	/* multiview type : 2x2, 1x3 */
	word = strtok_r(NULL, sep, &brkt);
	if(word && strlen(word) == 1) stTuneInfo.multiviewType = atoi(word);
	else stTuneInfo.multiviewType = 0;

	stTuneInfo.isMainChannelSame = 0; // jung2604 : main??pip??시??같?? 채널??볼때 처리??주????한 Flag

	return stTuneInfo;
}

void BtvMediaPlayerService::parseAudioChangeInfo(const char* dataXML, int *nAID, int *nACodecID)
{
	char *word;
	char *buffer;
	char *brkt = NULL;
	char *sep=";";
	char szBuffer[4096];

	LOGSRC("%s %d", __FUNCTION__, __LINE__);

	strcpy(szBuffer, dataXML);

	buffer = (char*) &szBuffer[0];

	LOGSRC("%s %d %s", __FUNCTION__, __LINE__, buffer);
	word = strtok_r(buffer, sep, &brkt);
	*nAID = atoi(word);

	word = strtok_r(NULL, sep, &brkt);
	*nACodecID = atoi(word);
}


HPlayer_IPTV_Info BtvMediaPlayerService::parsePlayerInfo(const char* dataXML)
{
	HPlayer_IPTV_Info stPlayInfo;
	char *word;
	char *buffer;
	char *sep=";";
	char szBuffer[4096];

	LOGSRC("%s %d", __FUNCTION__, __LINE__);

	strcpy(szBuffer, dataXML);

	buffer = (char*) &szBuffer[0];

	/* get playMode field */
	word = strsep(&buffer, sep);
	stPlayInfo.nPlayMode = atoi(word);

	/* get playtype field */
	word = strsep(&buffer, sep);
	stPlayInfo.nPlayType = atoi(word);
	
	/* get Drm Type field */
	word = strsep(&buffer, sep);
	stPlayInfo.nDRMType = atoi(word);

	/* get Duration field */
	word = strsep(&buffer, sep);
	stPlayInfo.nDurationSec = atoi(word);

	/* get resume sec field */
	word = strsep(&buffer, sep);
	stPlayInfo.nResumeSec = atoi(word);

	/* get path field */
	word = strsep(&buffer, sep);
	strcpy(stPlayInfo.szPath, word);

	/* get meta info */
	word = strsep(&buffer, sep);
		strcpy(stPlayInfo.szMetaInfo, word);

	/* get contentid field */
	word = strsep(&buffer, sep);
		strcpy(stPlayInfo.szContentID, word);

	/* get OTP ID field */
	word = strsep(&buffer, sep);
		strcpy(stPlayInfo.szOTPID, word);

	/* get OTP PWD field */
	word = strsep(&buffer, sep);
		strcpy(stPlayInfo.szOTPPasswd, word);

	/* last frame ??정 */
	word = strsep(&buffer, sep);
	if(word && strlen(word) == 1) stPlayInfo.enableLastFrame = atoi(word);
	else stPlayInfo.enableLastFrame = 1;

	return stPlayInfo;
}

void BtvMediaPlayerService::printTuneTVInfo(HTuner_IPTV_Info stTuneInfo)
{

}


status_t BtvMediaPlayerService::tuneTV(int deviceID, const char* dataXML)
{
	int nRet;
	LOGSRC("[%s][%d] called",  __FUNCTION__, deviceID);

	Mutex::Autolock lock(mServiceLock);
	
	setBtvMediaPlayer(deviceID);

	int callingPid = 0; //getCallingPid();

	LOGSRC("[%s][%d] device[%d], dataXML=%s", __FUNCTION__, __LINE__, deviceID, dataXML);
	// native service coding start.....
	if (!mpIptvSocPlayer)
		return NO_INIT;

	if (mpIptvSocPlayer)
	{
		if (mpIptvSocPlayer->getPlayerStatus() != PLAYER_STATUS_NONE)
		{
			if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_RTSP
			|| mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_NORMAL_FILE)
			{
				LOGSRC("[%s] CLIENT CLOSED", __FUNCTION__);
				mpIptvSocPlayer->close();
			}
		}
	}

	// fldgo note : 2014.10.10 A Connect -> B Connect -> B Tune in progress -> A disconnect -> B tune done -> A close case check. 
	// A close must be discard
//	mpIptvSocPlayer->mCallingPointer = (void*)this;

	HTuner_IPTV_Info ptTuneInfo[MULTIVIEW_MAX_NUMBER];
	int channelCount = parseMultiTuneTVInfo(ptTuneInfo, dataXML);

	LOGSRC("%s %d, device[%d], channel count : %d", __FUNCTION__, __LINE__, deviceID, channelCount);

	LOGSRC("%s %d device[%d], multicast_addr : %s", __FUNCTION__, __LINE__, deviceID, ptTuneInfo[0].multicast_addr);
	LOGSRC("%s %d device[%d], multicast_port : %d", __FUNCTION__, __LINE__, deviceID, ptTuneInfo[0].multicast_port);
	LOGSRC("%s %d device[%d], video_pid : %d", __FUNCTION__, __LINE__, deviceID, ptTuneInfo[0].video_pid);
	LOGSRC("%s %d device[%d], audio_pid : %d", __FUNCTION__, __LINE__, deviceID, ptTuneInfo[0].audio_pid);
	LOGSRC("%s %d device[%d], pcr_pid : %d", __FUNCTION__, __LINE__, deviceID, ptTuneInfo[0].pcr_pid);
	LOGSRC("%s %d device[%d], video_stream : %d", __FUNCTION__, __LINE__, deviceID, ptTuneInfo[0].video_stream);
	LOGSRC("%s %d device[%d], audio_stream : %d", __FUNCTION__, __LINE__, deviceID, ptTuneInfo[0].audio_stream);
    LOGSRC("%s %d device[%d], enableLastFrame : %d", __FUNCTION__, __LINE__, deviceID, ptTuneInfo[0].enableLastFrame);
    LOGSRC("%s %d device[%d], audioenable : %d", __FUNCTION__, __LINE__, deviceID, ptTuneInfo[0].audioenable);

	if(channelCount <= 1) {
		//jung2604 : PIP??때 IPTV가 같?? 채널??경우 별도??처리가 ??요??다..
		if(mpIptvSocPlayer->mDeviceID != 0) {
			IptvSocPlayer* mainIptvPlayer = selectIptvSocPlayer(0);//mIptvPlayerService->selectIptvSocPlayer(0);
			if(mainIptvPlayer != NULL && mainIptvPlayer->InGetPlayerMode() == PLAYER_MODE_IPTV && mainIptvPlayer->InGetPlayerStatus() != PLAYER_STATUS_STOP && mainIptvPlayer->player.isMultiview == false) {
				if(mainIptvPlayer->player.locator_head != NULL) {
					//LOGSRC("%s main--:%s current:%s", __FUNCTION__, mainIptvPlayer->player.locator_head->tune_param.ip.ip, ptTuneInfo[0].multicast_addr);
					if(!memcmp(mainIptvPlayer->player.locator_head->tune_param.ip.ip, ptTuneInfo[0].multicast_addr, strlen(mainIptvPlayer->player.locator_head->tune_param.ip.ip))) {
						ptTuneInfo[0].isMainChannelSame = 1; // jung2604 : main??pip??시??같?? 채널??볼때 처리??주????한 Flag
						LOGSRC("%s main-- channel is same!!!", __FUNCTION__);
					}
				}
			}

			//pip 의 Audio가 Enable 이면...Main을 Mute 해준다.
            if(ptTuneInfo[0].audioenable && mainIptvPlayer != NULL) {
                mainIptvPlayer->changeAudioOnOff_l(FALSE);
            }
		}

		nRet = mpIptvSocPlayer->tuneTV_l(ptTuneInfo[0]);

		// TODO : jung2604 : PIP에서 소리가 나오고 있을때 main에서는 소리가 나오면 안된다..세팅값을 넘기는게 제일 좋으나 일단 mute 처리를 해준다.
		if (mpIptvSocPlayer->mDeviceID == 0) {
			IptvSocPlayer *pipIptvPlayer = selectIptvSocPlayer(1);
			if (pipIptvPlayer->InGetPlayerStatus() != PLAYER_STATUS_NONE && pipIptvPlayer->InGetPlayerStatus() != PLAYER_STATUS_STOP) {
				if (pipIptvPlayer->player.locator_head != NULL && !pipIptvPlayer->player.save_mute_enable) {
					mpIptvSocPlayer->changeAudioOnOff_l(0);
				}
			}
		}

	} else {
		nRet = mpIptvSocPlayer->tuneTV_Multiview_l(ptTuneInfo);
	}
	LOGSRC("[%s][%d] out",  __FUNCTION__, deviceID);
	
/*	if (nRet == 0)
		mpIptvSocPlayer->mCallingPointer = (void*)this;*/

	return nRet;
}

status_t BtvMediaPlayerService::closeTV(int deviceID)
{
	int nRet;

	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);
	LOGSRC("[%s][%d] called",  __FUNCTION__, deviceID);
	
	if (!mpIptvSocPlayer)
		return NO_INIT;
	/*
	if (this != mpIptvSocPlayer->mCallingPointer)
	{
		LOGSRC("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");
		LOGSRC("[%s] called [%x] [%x]", __FUNCTION__, mpIptvSocPlayer->mCallingPointer, this);
		return NO_ERROR;
	}*/

	nRet = mpIptvSocPlayer->closeTV();

	//mpIptvSocPlayer->mCallingPointer = NULL;

	return nRet;
}

status_t BtvMediaPlayerService::bindingFilter(int deviceID, const char* dataXML)
{
	int nRet;
	LOGSRC("[%s] called",  __FUNCTION__);
	LOGSRC("[%s][%d] dataXML=%s", __FUNCTION__, __LINE__, dataXML);

	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);
	
	// native service coding start.....
	if (!mpIptvSocPlayer)
		return NO_INIT;

	nRet = mpIptvSocPlayer->bindingFilter_l(0, 0, 0, 0);
	return nRet;
}

status_t BtvMediaPlayerService::releaseFilter(int deviceID, const char* dataXML)
{
	int nRet;
	LOGSRC("[%s] called",  __FUNCTION__);
	LOGSRC("[%s][%d] dataXML=%s", __FUNCTION__, __LINE__, dataXML);

	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);
	
	// native service coding start.....
	if (!mpIptvSocPlayer)
		return NO_INIT;

	nRet = mpIptvSocPlayer->releaseFilter_l(0);
	return nRet;
}

status_t BtvMediaPlayerService::changeAudioChannel(int deviceID, const char* dataXML)
{
	int nAid, nACodecID, nRet;
	LOGSRC("[%s] called",  __FUNCTION__);
	LOGSRC("[%s][%d] dataXML=%s", __FUNCTION__, __LINE__, dataXML);

	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);
	
	// native service coding start.....
	if (!mpIptvSocPlayer)
		return NO_INIT;


	parseAudioChangeInfo(dataXML, &nAid, &nACodecID);
	nRet = mpIptvSocPlayer->changeAudioChannel_l(nAid, nACodecID);
	LOGSRC("%s %d", __FUNCTION__, __LINE__);	

	return nRet;
}

status_t BtvMediaPlayerService::changeAudioOnOff(int deviceID, const char* dataXML)
{
	char *word;
	char *buffer;
	char *brkt = NULL;
	char *sep=";";
	char szBuffer[4096];

	int nOnOff;
	
	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	LOGSRC("[%s] called", __FUNCTION__);
	if (!mpIptvSocPlayer)
		return NO_INIT;

	strcpy(szBuffer, dataXML);

	buffer = &szBuffer[0];
	word = strtok_r(buffer, sep, &brkt);
	nOnOff = atoi(word);

#if 1   //멀티뷰 상황에서 mute정보가 올라오면..
	if(nOnOff == 1) {
        if (mpIptvSocPlayer->mDeviceID != 0) {
            IptvSocPlayer *mainIptvPlayer = selectIptvSocPlayer(0);
            if (mainIptvPlayer != NULL && mainIptvPlayer->InGetPlayerStatus() != PLAYER_STATUS_STOP &&
                mainIptvPlayer->player.isMultiview == true) {
                if (mainIptvPlayer->player.locator_head != NULL) {
                    mainIptvPlayer->changeAudioOnOff_l(0);
                }
            }

            //jung2604 : vod 배속 재생 + 멀티뷰 일때는 항상 소리가 나면 안된다
            IptvSocPlayer *pipIptvPlayer = selectIptvSocPlayer(1);
            if (pipIptvPlayer != NULL && pipIptvPlayer->InGetPlayerStatus() >= PLAYER_STATUS_2XFF &&
                pipIptvPlayer->InGetPlayerStatus() <= PLAYER_STATUS_16XBW) {
                nOnOff = 0;
            }
        } else {
            IptvSocPlayer *pipIptvPlayer = selectIptvSocPlayer(1);
            if (mpIptvSocPlayer->InGetPlayerStatus() != PLAYER_STATUS_STOP &&
                mpIptvSocPlayer->player.isMultiview == true) {
                if (pipIptvPlayer->player.locator_head != NULL) {
                    pipIptvPlayer->changeAudioOnOff_l(0);
                }
            }
        }
    }
#endif

	return mpIptvSocPlayer->changeAudioOnOff_l(nOnOff);
}

status_t BtvMediaPlayerService::getPlayerMode(int deviceID)
{
	LOGSRC("[%s] called", __FUNCTION__);

	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);
	
	if (!mpIptvSocPlayer)
		return NO_INIT;
	
	return mpIptvSocPlayer->getPlayerMode();
}

status_t BtvMediaPlayerService::getPlayerStatus(int deviceID)
{
	LOGSRC("[%s] called", __FUNCTION__);

	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);
	
	if (!mpIptvSocPlayer)
		return NO_INIT;

	return 	mpIptvSocPlayer->getPlayerStatus();
}

status_t BtvMediaPlayerService::getCurrentPosition(int deviceID)
{
	LOGSRC("[%s] called", __FUNCTION__);
	
	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);
	
	if (!mpIptvSocPlayer)
		return NO_INIT;

	return 	mpIptvSocPlayer->getCurrentPosition();
}


status_t BtvMediaPlayerService::setWindowSize(int deviceID, const char* dataXML)
{
	char *word;
	char *buffer;
	char *brkt = NULL;
	char *sep=";";
	char szBuffer[4096];

	int nX, nY, nWidth, nHeight;

	LOGSRC("[%s] called", __FUNCTION__);
	setBtvMediaPlayer(deviceID);
	
	if (!mpIptvSocPlayer)
		return NO_INIT;

	strcpy(szBuffer, dataXML);

	buffer = &szBuffer[0];
	word = strtok_r(buffer, sep, &brkt);
	nX = atoi(word);

	word = strtok_r(NULL, sep, &brkt);
	nY = atoi(word);

	word = strtok_r(NULL, sep, &brkt);
	nWidth = atoi(word);

	word = strtok_r(NULL, sep, &brkt);
	nHeight = atoi(word); 
	
	return mpIptvSocPlayer->setWindowSize_l(nX, nY, nWidth, nHeight);;
}

status_t BtvMediaPlayerService::play(int deviceID)
{
	int nRet;
	LOGSRC("[%s] called", __FUNCTION__);
	
	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	// native service coding start.....
	if (!mpIptvSocPlayer)
		return NO_INIT;

	nRet = mpIptvSocPlayer->play();

	return nRet;
}

status_t BtvMediaPlayerService::pause(int deviceID)
{
	LOGSRC("[%s] called", __FUNCTION__);
	
	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	// native service coding start.....
	if (!mpIptvSocPlayer)
		return NO_INIT;

	return mpIptvSocPlayer->pause();
}

status_t BtvMediaPlayerService::resume(int deviceID)
{
	LOGSRC("[%s] called", __FUNCTION__);
	
	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	// native service coding start.....
	if (!mpIptvSocPlayer)
		return NO_INIT;

	return mpIptvSocPlayer->resume();
}

status_t BtvMediaPlayerService::seek(int deviceID, const char* dataXML, bool pause)
{
	char *word;
	char *buffer;
	char *brkt = NULL;
	char *sep=";";
	char szBuffer[4096];

	long nSec;

	LOGSRC("[%s] called", __FUNCTION__);

	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);
	
	if (!mpIptvSocPlayer)
		return NO_INIT;

	strcpy(szBuffer, dataXML);

	buffer = &szBuffer[0];
	word = strtok_r(buffer, sep, &brkt);
	nSec = atol(word); //atoi(word);

	return mpIptvSocPlayer->seek_l(nSec, pause);
}

status_t BtvMediaPlayerService::pauseAt(int deviceID, const char* dataXML) {
    char *word;
    char *buffer;
    char *brkt = NULL;
    char *sep=";";
    char szBuffer[4096];

    long nMs = 0;

    LOGSRC("[%s] called", __FUNCTION__);

    Mutex::Autolock lock(mServiceLock);

    setBtvMediaPlayer(deviceID);

    strcpy(szBuffer, dataXML);

    buffer = &szBuffer[0];
    word = strtok_r(buffer, sep, &brkt);
	nMs = atol(word); //atoi(word);

	return mpIptvSocPlayer->pauseAt_l(nMs);
}

status_t BtvMediaPlayerService::trick(int deviceID, const char* dataXML)
{
	char *word;
	char *buffer;
	char *brkt = NULL;
	char *sep=";";
	char szBuffer[4096];

	int nTrick;

	LOGSRC("[%s] called", __FUNCTION__);

	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);
	
	if (!mpIptvSocPlayer)
		return NO_INIT;

	strcpy(szBuffer, dataXML);

	buffer = &szBuffer[0];
	word = strtok_r(buffer, sep, &brkt);
	nTrick = atoi(word);

	return mpIptvSocPlayer->trick_l(nTrick);
}

status_t BtvMediaPlayerService::keepLastFrame(int deviceID, bool flag) {
	LOGSRC("[%s] called", __FUNCTION__);

	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);
	
	if (!mpIptvSocPlayer)
		return NO_INIT;

	return mpIptvSocPlayer->keepLastFrame(flag);
}

status_t BtvMediaPlayerService::stop(int deviceID)
{
	LOGSRC("[%s] called", __FUNCTION__);
	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	// native service coding start.....
	if (!mpIptvSocPlayer)
		return NO_INIT;

	// fldgo note : 2015.01.20 A connect -> B connect -> B Tune -> A Stop case check. A Stop Must be discard

	int callingPid = 0;//getCallingPid();

	return mpIptvSocPlayer->stop();
}

status_t BtvMediaPlayerService::close(int deviceID)
{
	int nRet;
	LOGSRC("[%s] called",  __FUNCTION__);
	
	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	// native service coding start.....
	if (!mpIptvSocPlayer)
		return NO_INIT;

	/*
	if (this != mpIptvSocPlayer->mCallingPointer)
	{
		LOGSRC("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
		LOGSRC("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
		LOGSRC("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
		LOGSRC("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
		LOGSRC("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
		LOGSRC("[%s] called [%x] [%x]", __FUNCTION__, mpIptvSocPlayer->mCallingPointer, this);
		return NO_ERROR;
	}*/

	nRet = mpIptvSocPlayer->close();

	mpIptvSocPlayer->reset();

	//mpIptvSocPlayer->mCallingPointer = NULL;

	return nRet;
}

status_t BtvMediaPlayerService::invoke(int deviceID, const Parcel& request, Parcel* reply)
{
	int nRet;
	LOGSRC("[%s] called",  __FUNCTION__);
	
	//Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	// native service coding start.....
	if (!mpIptvSocPlayer)
		return NO_INIT;

	nRet = mpIptvSocPlayer->invoke(request, reply);
	return nRet;
}

status_t BtvMediaPlayerService::startDmxFilter(int deviceID, int32_t pid, int32_t tid)
{
	LOGSRC("[%s] (pid:%d, tid:%d) called",  __FUNCTION__, pid, tid);
	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	mpIptvSocPlayer->startDmxFilter(pid, tid);
	return OK;
}

status_t BtvMediaPlayerService::stopDmxFilter(int deviceID, int32_t pid, int32_t tid)
{
	LOGSRC("[%s] (pid:%d, tid:%d) called",  __FUNCTION__, pid, tid);
	Mutex::Autolock lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	mpIptvSocPlayer->stopDmxFilter(pid, tid);
	return OK;
}


status_t BtvMediaPlayerService::setDummys(int deviceID, const char* dummys)
{
	//setBtvMediaPlayer(deviceID);
	//return mpIptvSocPlayer->setDummys_l(dummys);
	return IPTV_ERR_SUCCESS;
}

//jung2604 : 20190412 : 온가족 이어폰용..Thread
    pthread_t BtvMediaPlayerService::threadEarPhone = NULL;
    int BtvMediaPlayerService::isPlayingFamilyEarPhoneThread = FALSE;
    int BtvMediaPlayerService::isPlugFamilyEarPhone = FALSE;

//jung2604 : 20190412 : 온가족 이어폰용..Thread
    void BtvMediaPlayerService::familyEarPhoneThread(void *ThreadParam) {
		LOGSRC("[%s] Call", __FUNCTION__);
		BtvMediaPlayerService *pThis = (BtvMediaPlayerService*) ThreadParam;

        int ret = 0;
		while(isPlayingFamilyEarPhoneThread) {
			ret = TI2_checkStatus();
			/*
			if(ret == AM_STATUS_F_PAIRED) LOGSRC("++TEST : TI2_checkStatus AM_STATUS_F_PAIRED");
			else if(ret == AM_STATUS_F_TVAUDIO) LOGSRC("++TEST : TI2_checkStatus AM_STATUS_F_PAIRED");
			else LOGSRC("++TEST : TI2_checkStatus unknown");
			*/
			if(ret == AM_STATUS_F_PAIRED) {
				//온가족 이어폰 연결됨
                isPlugFamilyEarPhone = TRUE;
                familyStateCallback(pThis, 1);
                pThis->audioSnoop(&isPlayingFamilyEarPhoneThread);
                familyStateCallback(pThis, 0);
                isPlugFamilyEarPhone = FALSE;
			} else if(isPlugFamilyEarPhone) isPlugFamilyEarPhone = FALSE;

			usleep(300000); //300ms
		}

        LOGSRC("[%s] familyEarPhoneThread EXIT, isPlayingFamilyEarPhoneThread : %d", __FUNCTION__, isPlayingFamilyEarPhoneThread);
    }

#define SND_CHUNK_BUFSIZE (4096*8)

    typedef struct snd_data_chunk_meta_st {
        unsigned int uiSampleNum;
		unsigned int uiChanNum;
		unsigned int uiBitDepth;
		unsigned int uiSampleRate;
    } SND_DATA_CHUNK_META;

    typedef union  snd_chunk_data_st {
        int32_t i32[0];
        int16_t i16[0];
        int8_t  i8[SND_CHUNK_BUFSIZE];
    } SND_CHUNK_DATA;

    typedef struct snoop_chunk_st {
        SND_DATA_CHUNK_META chunkMeta;
        SND_CHUNK_DATA chunkData;
    } SNOOP_CHUNK;

    void BtvMediaPlayerService::audioSnoop(int *pIsPlayingEarPhoneThread)
    {
        struct sockaddr_un servaddr;
        socklen_t addr_len;
        int fd = socket(PF_UNIX, SOCK_STREAM, 0), rv, recvbytes;
		int ret;
        SNOOP_CHUNK snoopChunk;
        char *buf;
        size_t len, count, chunk_size = 0;


#ifdef FEATURE_SOC_AMLOGIC
        char buffer[SND_CHUNK_BUFSIZE];
        buf = buffer;
#endif

		HalBtv_SetAudioSnoop(true);

        memset(&servaddr, 0, sizeof(servaddr));
        servaddr.sun_family = AF_UNIX;
#ifdef FEATURE_SOC_AMLOGIC
        snprintf(servaddr.sun_path, sizeof(servaddr.sun_path) - 1, "/dev/socket/audioserver/snd_snoop");
#else
		snprintf(&servaddr.sun_path[1], sizeof(servaddr.sun_path) - 1, "/dev/socket/snd_snoop");
		servaddr.sun_path[0] = '\0';
#endif
        addr_len = sizeof(servaddr.sun_family) + strlen(servaddr.sun_path) + 1;

        bool isStop = false;
        while (*pIsPlayingEarPhoneThread && !isStop) {
			LOGSRC("[%s] about to connect to %s", __FUNCTION__, servaddr.sun_path);
			fd = socket(PF_UNIX, SOCK_STREAM, 0);
			rv = ::connect(fd, (struct sockaddr *)&servaddr, addr_len);
			if (rv < 0) {
				LOGSRC("[%s] connect error (%s)", __FUNCTION__, strerror(errno));
				::close(fd);

				ret = TI2_checkStatus();

				if(ret != AM_STATUS_F_PAIRED) {
					//온가족 이어폰 연결해제됨
					isStop = true;
					break;
				}
				usleep(300000); //300ms
				continue;
			}

			LOGSRC("[%s] connect success %s", __FUNCTION__, servaddr.sun_path);

			fd_set fds;
			struct timeval tv;

			while (*pIsPlayingEarPhoneThread) {
				FD_ZERO(&fds);

				FD_SET(fd, &fds);

#ifdef FEATURE_SOC_AMLOGIC
				tv.tv_sec = 0;
				tv.tv_usec = 250000;

				ret = select(fd + 1, &fds, NULL, NULL, &tv);

				if (ret > 0 && FD_ISSET(fd, &fds)) {
					if ((recvbytes = (int)recv(fd, (void*)(buf + chunk_size), SND_CHUNK_BUFSIZE, 0)) == -1) {
						LOGSRC("[%s] recv header fail.. recvbytes=%d", __FUNCTION__, recvbytes);
						break;
					}
					chunk_size += recvbytes;
					//LOGSRC("[%s] recv recvbytes=%d, total : %d", __FUNCTION__, recvbytes, chunk_size);

					if(chunk_size > 4096) {
						ret = TI2_writePCM((unsigned char *)buf, chunk_size);
						chunk_size = 0;
						LOGSRC("[%s] TI2_writePCM %d", __FUNCTION__, ret);
					}

				}

#else
				tv.tv_sec = 1;  //
				tv.tv_usec = 0;

				ret = select(fd + 1, &fds, NULL, NULL, &tv);

				if (ret > 0 && FD_ISSET(fd, &fds)) {
					buf = (char *)&snoopChunk;
					len = sizeof(SNOOP_CHUNK);
					/* read header */
					if ((recvbytes = (int)recv(fd, (void*)buf, sizeof(SND_DATA_CHUNK_META), 0)) == -1) {
						LOGSRC("[%s] recv header fail.. recvbytes=%d", __FUNCTION__, recvbytes);
						break;
					}

					if(recvbytes != sizeof(SND_DATA_CHUNK_META)) {
						LOGSRC("[%s] recv header err.. recvbytes=%d", __FUNCTION__, recvbytes);
						break;
					}

					chunk_size = snoopChunk.chunkMeta.uiSampleNum*snoopChunk.chunkMeta.uiChanNum* \
				(snoopChunk.chunkMeta.uiBitDepth/8);
					//LOGSRC("[%s]  chunk size = %d", __FUNCTION__, chunk_size);

					/* read data */
					buf = (char *)&snoopChunk.chunkData;
					count = 0;
					recvbytes = 0;
					while(count < chunk_size) {
						if ((recvbytes = (int)recv(fd, (void*)(buf + count), (chunk_size - count), 0)) == -1) {
							LOGSRC("[%s]  recv data err.. recvbytes=%d", __FUNCTION__, recvbytes);
							break;
						}
						count += recvbytes;
					}
					if(count != chunk_size) {
						LOGSRC("[%s]  recv header err.. count=%d, chunk_size=%d", __FUNCTION__, count, chunk_size);
						break;
					}

					ret = TI2_writePCM((unsigned char *)snoopChunk.chunkData.i8, chunk_size);
					LOGSRC("[%s] TI2_writePCM %d", __FUNCTION__, ret);
				}
#endif

				ret = TI2_checkStatus();

				if(ret != AM_STATUS_F_PAIRED) {
					//온가족 이어폰 연결해제됨
					isStop = true;
					break;
				}
			}
			::close(fd);
			LOGSRC("[%s] Close socket fd %d", __FUNCTION__, fd);
        }

		HalBtv_SetAudioSnoop(false);

		LOGSRC("[%s] out", __FUNCTION__);
    }

	//화면 Delay관련..
    int BtvMediaPlayerService::videoDelayTimeMs = 0;
    int BtvMediaPlayerService::enableGlobalMute = 0;
	AVP_AudioOutputMode BtvMediaPlayerService::enableSettingsDolby = AVP_AUDIO_PCM;
	AVP_AspectRatioMode BtvMediaPlayerService::settingsAspectRatioMode = AVP_ASPECT_RATIO_ORIGINAL_SCREEN;

    bool getBtEarPhoneState() {
        char propertyBuf[10];
        if(get_systemproperty("SETTINGS.A2DP.PLUGGED", propertyBuf, 10) == 0) {
            if(strcmp(propertyBuf, "1") == 0) return true;
        }

        return false;
    }

    bool getDolbySettings() {
        char propertyBuf[10];
        if(get_settings_property("SET_HDMIAUDIOMODE", propertyBuf, 10) == 0) {
            if(strcmp(propertyBuf, "0") == 0) return true;
        }

        return false;
    }

	AVP_AspectRatioMode getVideoScalingModeSettings() {
        char propertyBuf[10];
        if(get_systemproperty("SCALING_MODE", propertyBuf, 10) == 0) {
            if(strcmp(propertyBuf, "1") == 0) return AVP_ASPECT_RATIO_FULL_SCREEN;
        }

        return AVP_ASPECT_RATIO_ORIGINAL_SCREEN;
    }

    int getDeleyTime() {
		int delay = VIDEO_DELAY_TIME_FAMILY_EARPHONE;
		struct stat fileStat;
		if (lstat("/btv_home/run/delay.txt", &fileStat) == 0) {
			FILE *fp;
			fp = fopen("/btv_home/run/delay.txt", "rt");
			if(fp) {
				int ret = fscanf(fp, "%d", &delay);
				LOGSRC("[%s][TEST] ENABLE FAMILY EARPHONE file ret : %d, delay : %d", __FUNCTION__, ret, delay);
				fclose(fp);
			} else LOGSRC("[%s][TEST] ENABLE FAMILY EARPHONE file open error", __FUNCTION__);
		} else
			LOGSRC("[%s][TEST] ENABLE FAMILY EARPHONE file not found", __FUNCTION__);

		return delay;
    }
	
	// echwang : 2020.05.08 : + S
	#define BTF_HAL_RESOLUTION_DELIMETER "|" // "|"
	
	std::string BtvMediaPlayerService::getDisplaySupportedResolution()
	{
		LOGSRC("[%s] called", __FUNCTION__);
		std::string retValue = "";
		Vector<int> list ;
		char resolution_list[128];
		if(HalBtv_GetSupportedResolutionList(resolution_list) != 0) 
			return retValue;

		if(strlen(resolution_list) == 0)
			return retValue;
		
	//	strcpy(resolution_list, "0 1 2 4 5 8 9 13 14 15 16");		// test code
		
		LOGSRC("[%s] resolution_list=(%s)", __FUNCTION__, resolution_list);
		
		bool is720P = false, is1080I = false, is1080P = false, is2160P30 = false, is2160P60 = false;  
		
		char *ptr = strtok(resolution_list, BTF_HAL_RESOLUTION_DELIMETER);
		while (ptr != NULL)
		{
			list.push_back(atoi(ptr));
			
			LOGSRC("[%s] atoi(ptr)=(%d)", __FUNCTION__, atoi(ptr));
			
			ptr = strtok(NULL, BTF_HAL_RESOLUTION_DELIMETER);
		}
		
		mSupportedResList.clear();
		int listSize = list.size();
		for(int i = 0; i < listSize; i++)
		{
			mSupportedResList.push_back(list[i]);
		}


		LOGSRC("[%s] end resolution_list:(%s) mSupportedResList.size:(%d)", __FUNCTION__, retValue.c_str(), mSupportedResList.size());
		return retValue;
	}
	
	BTF_DISPLAY_Resolution BtvMediaPlayerService::getBtfDisplayResolution(int resolution)
	{
		BTF_DISPLAY_Resolution retResolution = static_cast<BTF_DISPLAY_Resolution>(0);
		int tempResolution = 0;
		switch(resolution)
		{
			case BTF_UI_DISPLAY_MODE_AUTO:
				tempResolution = 0;
			break;
			case BTF_UI_DISPLAY_MODE_720P:
			{
				bool is60 = false, is5994 = false, is50 = false, is3dou = false; 
				int listSize = mSupportedResList.size();
				for(int i = 0; i < listSize; i++)
				{
					switch(mSupportedResList[i])
					{
						case 2: is5994 = true; break;
						case 3: is60 = true; break;
						case 1: is50 = true; break;
						case 10: is3dou = true; break;
					}
				}
				
				if(is5994) tempResolution = 2;				
				else if(is60)	tempResolution = 3;		
				else if(is50)	tempResolution = 1;	
				else if(is3dou)	tempResolution = 10;			
			}
			break;
			case BTF_UI_DISPLAY_MODE_1080I:
			{
				bool is60 = false, is5994 = false, is50 = false; 
				int listSize = mSupportedResList.size();
				for(int i = 0; i < listSize; i++)
				{
					switch(mSupportedResList[i])
					{
						case 5: is5994 = true; break;
						case 6: is60 = true; break;
						case 4: is50 = true; break;
					}
				}	

				if(is5994) tempResolution = 5;				
				else if(is60)	tempResolution = 6;		
				else if(is50)	tempResolution = 4;	
			}			
			break;
			case BTF_UI_DISPLAY_MODE_1080P:
			{
				bool is60 = false, is5994 = false, is50 = false, is24 = false, is24_3dou = false; 
				int listSize = mSupportedResList.size();
				for(int i = 0; i < listSize; i++)
				{
					switch(mSupportedResList[i])
					{
						case 8: is5994 = true; break;
						case 9: is60 = true; break;
						case 7: is50 = true; break;
						case 11: is24 = true; break;
						case 12: is24_3dou = true; break;
					}
				}	

				if(is5994) tempResolution = 8;				
				else if(is60)	tempResolution = 9;		
				else if(is50)	tempResolution = 7;				
				else if(is24)	tempResolution = 11;		
				else if(is24_3dou)	tempResolution = 12;	
			}			
			break;
			case BTF_UI_DISPLAY_MODE_3840x2160P30HZ:
			case BTF_UI_DISPLAY_MODE_3840x2160P60HZ:
			{
				bool is60 = false, is5994 = false, is30 = false, is2997 = false; 
				int listSize = mSupportedResList.size();
				for(int i = 0; i < listSize; i++)
				{
					switch(mSupportedResList[i])
					{
						case 15: is5994 = true; break;
						case 16: is60 = true; break;
						case 13: is2997 = true; break;
						case 14: is30 = true; break;
					}
				}	

				if(is5994) tempResolution = 15;				
				else if(is60)	tempResolution = 16;		
				else if(is2997)	tempResolution = 13;				
				else if(is30)	tempResolution = 14;
			}			
			break;	
		}
		
		retResolution = static_cast<BTF_DISPLAY_Resolution>(tempResolution);
		
		LOGSRC("[%s] result retResolution(%d)", __FUNCTION__, retResolution);

		return retResolution;
	}
	// echwang : 2020.05.08 : + E

	void BtvMediaPlayerService::externFunctionStateChangeCheckThread(void *ThreadParam) {
		LOGSRC("[%s] Call ", __FUNCTION__);
		BtvMediaPlayerService *pThis = (BtvMediaPlayerService*) ThreadParam;

		IptvSocPlayer* playerMain = pThis->selectIptvSocPlayer(IptvSocPlayer::mBTV_DEVICE_MAIN); //pThis->mIptvSocPlayerMain
        IptvSocPlayer* playerPip = pThis->selectIptvSocPlayer(IptvSocPlayer::mBTV_DEVICE_PIP1); //pThis->mIptvSocPlayerPip1;

        struct stat fileStat;
        bool isExit = false;
#if 0 //remove ELP
        bool isELPlayerPlayingCheck = true;
        bool isELPlyaerSetResolutionComplete = false;
        bool isELPlyaerSetVolumeComplete = false;
        int lastVolume = 0;
        {
            char retBuf[1024] = { 0 };
            if (get_systemproperty("PROPERTY_LAST_AUDIO_VOLUME", retBuf, 10) == 0) {
                LOGSRC("ELP PROPERTY_LAST_AUDIO_VOLUME [%s]!!!!", retBuf);
                lastVolume = atoi(retBuf);
            }

            if(lastVolume > 32) lastVolume = 32;
            else if(lastVolume < 0) lastVolume = 0;
        }
#endif
		while(!isExit) {
#if 0 //remove ELP
			if(isELPlayerPlayingCheck) {
#ifdef FEATURE_SOC_AMLOGIC //TODO : amlogic 용 임시 제거
                isELPlayerPlayingCheck = false;
#else

				char retBuf[1024] = { 0 };
				int retLen = property_get("sys.boot_completed", retBuf, "0");

				if(retLen <= 0 || strcmp(retBuf, "1") != 0) { //안드로이드 부팅이 안되었을경우
                    // jung2604 : first media에서 미디어를 재생하였는지 체크
                    retLen = property_get("vendor.skb.btv.boot.early.liveplay", retBuf, "");

                    if (retLen > 0 && strcmp(retBuf, "1") == 0) {
                        LOGSRC("+============================+");
                        LOGSRC("|  EarlyLivePlayback : vendor.skb.btv.boot.early.liveplay");
                        LOGSRC("+============================+");

                        isELPlayerPlayingCheck = false;

                        if(!isELPlyaerSetResolutionComplete) {
                        	int readResolution = BTF_UI_DISPLAY_MODE_AUTO;
                             if (get_systemproperty("DISPLAY_RESOLUTION", retBuf, 10) == 0) {
                                LOGSRC("ELP PROPERTY DISPLAY_RESOLUTION [%s]!!!!", retBuf);
                                readResolution = atoi(retBuf);
								
								getDisplaySupportedResolution();

                                //if(resolution != BTF_UI_DISPLAY_MODE_AUTO) {
                                {
                                    DISPLAY_Resolution resolution = static_cast<DISPLAY_Resolution>(readResolution);
									BTF_DISPLAY_Resolution resol = getBtfDisplayResolution(readResolution);
                                    if(HalBtv_SetResolution(resol) != 0) {
									//if(HalBtv_SetResolution(resolution) != 0) {
                                        LOGSRC("[%s] ELP SET RESOLUTION return false", __FUNCTION__);
                                    } else {
                                        LOGSRC("[%s] ELP SET RESOLUTION complete, resolution : %d[%d]", __FUNCTION__, resolution, readResolution);
                                        isELPlyaerSetResolutionComplete = true;
                                        property_set("vendor.skb.hdmi.is_set", "1");
                                    }
                                }
                             }
                        }

                        if(!isELPlyaerSetVolumeComplete) {
                            int currentVolume = 0.0;

                            if(HalBtv_SetMasterAudioVolume(lastVolume) == 0) {
                                LOGSRC("ELP set volume[%d] OK  !!!!!!", lastVolume);
                            } else {
                                LOGSRC("ELP set volume[%d] fail!!!!!", lastVolume);
                            }

                            if(HalBtv_GetMasterAudioVolume(&currentVolume) == 0) {
                                if(currentVolume == lastVolume) {
                                    LOGSRC("ELP change volume check OK %d, %d", currentVolume, lastVolume);
                                    isELPlyaerSetVolumeComplete = true;
                                } else LOGSRC("ELP change volume check fail %d, %d", currentVolume, lastVolume);
                            } else {
                                LOGSRC("ELP change volume check fail!!!!!,  %d", lastVolume);
                            }
                        }

                        isELPlayerPlayingCheck = !(isELPlyaerSetResolutionComplete && isELPlyaerSetVolumeComplete);
                    }
				} else isELPlayerPlayingCheck = false; //부팅이 완료 되었기 때문에..더이상 체크하지 않는다.
#endif  //TODO : amlogic 용 임시 제거
			}
#endif

		    //try {
                //if (lstat("/btv_home/run/test.txt", &fileStat) == 0) {
                if(isPlugFamilyEarPhone || lstat("/btv_home/run/test.txt", &fileStat) == 0) { //FIXME : jung2604 : test.txt를 이용한 테스트임
                    if (videoDelayTimeMs == 0) {
						videoDelayTimeMs = VIDEO_DELAY_TIME_FAMILY_EARPHONE;
						videoDelayTimeMs = getDeleyTime();
                        LOGSRC("[%s] ENABLE FAMILY EARPHONE gChangeVideoDelayTimeMs = %d", __FUNCTION__, videoDelayTimeMs);
                        IptvSocPlayer::videoDelayTimeMs = videoDelayTimeMs;
                        playerMain->setVideoDelay_l(videoDelayTimeMs);
                        playerPip->setVideoDelay_l(videoDelayTimeMs);
                    }
                } else if(getBtEarPhoneState()) {
                    if (videoDelayTimeMs != VIDEO_DELAY_TIME_BT_EARPHONE) {
                        LOGSRC("[%s] ENABLE BT EARPHONE gChangeVideoDelayTimeMs = %d", __FUNCTION__, VIDEO_DELAY_TIME_BT_EARPHONE);
                        videoDelayTimeMs = VIDEO_DELAY_TIME_BT_EARPHONE;
                        IptvSocPlayer::videoDelayTimeMs = videoDelayTimeMs;
                        playerMain->setVideoDelay_l(videoDelayTimeMs);
                        playerPip->setVideoDelay_l(videoDelayTimeMs);
                    }
                } else if (videoDelayTimeMs != 0) {
                    LOGSRC("[%s] gChangeVideoDelayTimeMs = 0", __FUNCTION__);
                    videoDelayTimeMs = 0;
                    IptvSocPlayer::videoDelayTimeMs = videoDelayTimeMs;
                    playerMain->setVideoDelay_l(videoDelayTimeMs);
                    playerPip->setVideoDelay_l(videoDelayTimeMs);
                }
//            } catch (int e) {
//                LOGSRC("[%s] gChangeVideoDelayTimeMs exception : %d", __FUNCTION__, e);
//		    }

            //check dolby settings
            if(getDolbySettings()) {
				if(enableSettingsDolby != AVP_AUDIO_PASSTHROUGH) {
					enableSettingsDolby = AVP_AUDIO_PASSTHROUGH;
					IptvSocPlayer::enableSettingsDolby = enableSettingsDolby;
					playerMain->changeSettingDolby();
					playerPip->player.enableSettingsDolby = enableSettingsDolby; //pip에서는 값만 변경해주자.
					//playerPip->changeSettingDolby();
				}

            } else if(enableSettingsDolby != AVP_AUDIO_PCM) {
				enableSettingsDolby = AVP_AUDIO_PCM;
				IptvSocPlayer::enableSettingsDolby = enableSettingsDolby;
				playerMain->changeSettingDolby();
				playerPip->player.enableSettingsDolby = enableSettingsDolby; //pip에서는 값만 변경해주자.
				//playerPip->changeSettingDolby();
            }

            if(getVideoScalingModeSettings() != settingsAspectRatioMode){
                settingsAspectRatioMode = getVideoScalingModeSettings();
                IptvSocPlayer::settingsAspectRatioMode = settingsAspectRatioMode;
				playerMain->changeAspectRatioMode();
				playerPip->changeAspectRatioMode();
            }

			usleep(100000); //100ms -> 5ms
		}
    }

#include <sys/inotify.h>
#define EVENT_SIZE (sizeof(struct inotify_event))
#define BUF_LEN 1024*(EVENT_SIZE+16)
#define GLOBAL_MUTE_FLAG_DIR "/btv_home/tmp/audio_route"
#define GLOBAL_MUTE_FLAG_FILENAME "btv_muted"
#define GLOBAL_MUTE_FLAG_PATH "/btv_home/tmp/audio_route/btv_muted"
#define GLOBAL_PRIORITY_MUTE_FLAG_PATH "/btv_home/tmp/audio_route/priority_btv_muted"

    bool getCheckGlobalMute() {
        struct stat fileStat;
        //global mute
        bool global_mute = false;
        bool btv_mute = (lstat(GLOBAL_MUTE_FLAG_PATH, &fileStat) == 0);
        bool priority_btv_mute = (lstat(GLOBAL_PRIORITY_MUTE_FLAG_PATH, &fileStat) == 0); 
        if(priority_btv_mute){
            global_mute = true;
        }else{
            if(btv_mute){
                global_mute = true;
            }
        }
        return global_mute;
    }

	void BtvMediaPlayerService::externFunctionStateChangeCheckForGlobalMuteThread(void *ThreadParam) {
		LOGSRC("[%s] Call ", __FUNCTION__);
		char propertyBuf[10];
		BtvMediaPlayerService *pThis = (BtvMediaPlayerService*) ThreadParam;

		IptvSocPlayer* playerMain = pThis->selectIptvSocPlayer(IptvSocPlayer::mBTV_DEVICE_MAIN); //pThis->mIptvSocPlayerMain
		IptvSocPlayer* playerPip = pThis->selectIptvSocPlayer(IptvSocPlayer::mBTV_DEVICE_PIP1); //pThis->mIptvSocPlayerPip1;
#if 1
		char buffer[BUF_LEN];
		int fd;
		int wd;
		int offset;
        int state;

		fd = inotify_init();
		if(fd < 0) {
			LOGSRC("[%s] inotify_init fail", __FUNCTION__);
			return;
		}

        wd = inotify_add_watch(fd, GLOBAL_MUTE_FLAG_DIR, IN_CREATE | IN_DELETE);

		bool isRun = true;
		do {
            if (getCheckGlobalMute()) {
                if (enableGlobalMute != 1) {
                    enableGlobalMute = 1;
                    IptvSocPlayer::enableGlobalMute = enableGlobalMute;
					playerMain->changeGlobalAudioMute_l();
					playerPip->changeGlobalAudioMute_l();
                }
            } else if (enableGlobalMute == 1) {
                enableGlobalMute = 0;
                IptvSocPlayer::enableGlobalMute = enableGlobalMute;

				playerMain->changeGlobalAudioMute_l();
				playerPip->changeGlobalAudioMute_l();
            }
#if 0  //time out을 걸고서 다른 작업을 할려고.... 폴더가 없을수도 있으니..다시 처음부터..체크를..하도록...
            struct timeval tv;
            fd_set readfds;
            FD_ZERO(&readfds);
            FD_SET(fd, &readfds);
            tv.tv_sec = 10;
            tv.tv_usec = 0;

            LOGSRC("[%s] select", __FUNCTION__);
            state = select(fd + 1, &readfds, NULL, NULL, &tv);
            if(state == -1) {
                //error
                //inotify_rm_watch(fd, wd);
                LOGSRC("[%s] select error %d", __FUNCTION__, state);
                continue;
            } else if(state == 0) {
                LOGSRC("[%s] select time out , state : %d", __FUNCTION__, state);
                continue;
            }
#endif
			LOGSRC("[%s] read", __FUNCTION__);
			int length = (int) read(fd, buffer, BUF_LEN);
            if (length < (int)sizeof(struct inotify_event))  {
				LOGSRC("[%s] length < 0", __FUNCTION__);
				usleep(1000);
				continue;
			}

            offset = 0;
            while(offset < length) {
                struct inotify_event *event = (struct inotify_event *) &buffer[offset];

                LOGSRC("[%s] event->name, %s", __FUNCTION__, event->name);
#if 1 //이벤트에서 직접처리 하지 않겠다. //로그용으로 남겨놓자..
                if (event->len) {
                    if (event->mask & IN_CREATE)  LOGSRC("[%s] IN_CREATE", __FUNCTION__);
                    else if (event->mask & IN_DELETE)  LOGSRC("[%s] IN_DELETE", __FUNCTION__);
                }
#endif
                offset += sizeof(struct inotify_event) + event->len;
            }
		} while(isRun);

		inotify_rm_watch(fd, wd);
		//close(fd);

#else

		struct stat fileStat;
		//global mute
		bool isExit = false;
		while(!isExit) {
			if (lstat("/btv_home/tmp/audio_route/btv_muted", &fileStat) == 0) {
				if (enableGlobalMute != 1) {
					enableGlobalMute = 1;
					IptvSocPlayer::enableGlobalMute = enableGlobalMute;
					playerMain->changeGlobalAudioMute_l();
					playerPip->changeGlobalAudioMute_l();
				}
			} else if (enableGlobalMute == 1) {
				enableGlobalMute = 0;
				IptvSocPlayer::enableGlobalMute = enableGlobalMute;
				playerMain->changeGlobalAudioMute_l();
				playerPip->changeGlobalAudioMute_l();
			}
			usleep(10000); //100ms -> 5ms
		}
#endif
	}

	void BtvMediaPlayerService::onCasInfoEvent(int32_t type, std::string casInfo) {
		LOGSRC("[casinfo][%s] onCasInfoEvent!!!!!  : %s", __FUNCTION__, casInfo.c_str());

		sp<BtvMediaCommunicationObserver> client = getMediaCommunicationObserver();
		if (client != 0) {
			LOGSRC("[%s][%d] client find... casinfo : %s", __FUNCTION__, __LINE__, casInfo.c_str());
			client->onCasInfoEvent(type, casInfo);
		} else
			LOGSRC("[%s][%d] client not found!!", __FUNCTION__, __LINE__);
    }

// ---------------------------------------------------------------------------------------------------

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/un.h>

#define  ATT_FIFO_PATH "/data/btv_home/run/att_fifo0"

    void BtvMediaPlayerService::attSednToPtsThread(void *ThreadParam) {
        LOGSRC("[%s] Call ", __FUNCTION__);
        BtvMediaPlayerService *pThis = (BtvMediaPlayerService*) ThreadParam;
        while(1) {
			pThis->attSendToPts();
            usleep(60000000);
		}
		LOGSRC("[%s] end ", __FUNCTION__);
    }

	void BtvMediaPlayerService::attSendToPts() {
		LOGSRC("[ATT] [%s] Call ", __FUNCTION__);

		char buffer[50] = { 0 };

		int fifo = 0;
		unlink(ATT_FIFO_PATH);
		if(mkfifo(ATT_FIFO_PATH, 0666) == -1)
		{
			LOGSRC("[ATT] [%s] mkfifo fail", __FUNCTION__);
			usleep(1000000);
			return;
		}

		if(chmod(ATT_FIFO_PATH, 0644) != 0)
			LOGSRC("[ATT] chmod 644 fail");

		// fifo 를 연다.
		// O_NONBLOCK 플래그를 설정하면, 데이터가 없는 동안에 다른 작업을 할 수 있다.
		fifo = ::open(ATT_FIFO_PATH, O_WRONLY);
		if(fifo < 0) {
			LOGSRC("[ATT] [%s] open fail", __FUNCTION__);
			usleep(1000000);
			return;
		}

		long long pts = 0;
		int getPtsResult = 0;
		while(fifo != -1) {
			if (0 < fifo) {
				sleep(1);

				IptvSocPlayer* playerMain = selectIptvSocPlayer(IptvSocPlayer::mBTV_DEVICE_MAIN);
				IptvSocPlayer* playerPip = selectIptvSocPlayer(IptvSocPlayer::mBTV_DEVICE_PIP1);

				if(playerMain != NULL && playerMain->InGetPlayerStatus() != PLAYER_STATUS_NONE && playerMain->InGetPlayerStatus() != PLAYER_STATUS_STOP) { /*&& playerMain->InGetPlayerMode() == PLAYER_MODE_IPTV */
					getPtsResult = HalBtv_GetVideoPTS(0, &pts);
				} else if(playerPip != NULL && playerPip->InGetPlayerStatus() == PLAYER_STATUS_NONE && playerPip->InGetPlayerStatus() == PLAYER_STATUS_STOP) {
					getPtsResult = HalBtv_GetVideoPTS(1, &pts);
				} else getPtsResult = 0; //continue;

				if(-9223372036854775808 == pts) pts = 0;
				//LOGSRC( "[ATT] **_** pts %lld, ms : %d", pts, (int)((pts / 90) - 10000));
				sprintf(buffer, "%lld\r\n", pts);
				if(write(fifo, (void *) buffer, strlen(buffer)) <= 0) { // +1: NULL까지 포함해서 전송
					::close(fifo);
					fifo = -1;
					LOGSRC( "[ATT] write error, close socket");
					break;
				}
			}
		}

		// 정리
		if(fifo != -1) ::close(fifo);
		unlink(ATT_FIFO_PATH);

        LOGSRC("[ATT] [%s] end ", __FUNCTION__);
    }

};	// namespace android
