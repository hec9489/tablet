// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

// namsj, namsj
//#define LOG_NDEBUG 0
#define LOG_TAG "IptvMediaPlayer-JNI"
#include "utils/Log.h"

#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include <unistd.h>
#include <utils/threads.h>
#include <jni.h>

#include <utils/Errors.h> 	// for status_t

//#include "util_logmgr.h"

#include <android/native_window_jni.h>
#include <android/native_window.h>
#include <gui/Surface.h>

#ifdef FEATURE_SOC_AMLOGIC
#include <cutils/properties.h>
#include <android_runtime/android_view_Surface.h>
#include <gui/bufferqueue/1.0/WGraphicBufferProducer.h>

#include <am_gralloc_ext.h>

#endif //FEATURE_SOC_AMLOGIC


#ifdef FEATURE_SOC_SYNAPTICS
#include <SidebandNativeHandle.h>
#endif

#include <com/skb/btvservice/1.0/IBtvMediaPlayer.h>
#include <com/skb/btvservice/1.0/IBtvMediaPlayerListener.h>


#include <binder/Parcel.h>

namespace android {

}

// ------------------------------------------------------------------------------------------------

using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::hardware::hidl_death_recipient;
using ::android::hidl::base::V1_0::IBase;

using ::com::skb::btvservice::V1_0::IBtvMediaPlayer;
using ::com::skb::btvservice::V1_0::IBtvMediaPlayerListener;
using ::com::skb::btvservice::V1_0::Status;
using ::com::skb::btvservice::V1_0::ParcelData;

#define BTV_MAIN_SERVICE "btv_main"
#define BTV_PIP_SERVICE  "btv_pip"

#define	BTV_DEVICE_MAIN  0
#define BTV_DEVICE_PIP   1

using namespace android;

static const char *classIptvMediaPlayer = "com/skb/btv/framework/media/core/IptvMediaPlayer";

enum ValueType {
    TYPE_INT    = 0,
    TYPE_LONG   = 1,
    TYPE_STRING = 2
};

//-------------------------------------------------------------------------------------------------

static JavaVM* gJavaVM = NULL;
static JNIEnv* AndroidRuntime_getJNIEnv()
{
    JNIEnv* env;
    assert(gJavaVM != NULL);

    if (gJavaVM->GetEnv((void**) &env, JNI_VERSION_1_6) != JNI_OK)
        return NULL;
    return env;
}

static void log_PrintEx(const char *format, ...)
{
    va_list 	va;
    char szDebugTemp[4096] = {0,};

    va_start(va, format);
    vsprintf(szDebugTemp, format, va);
    va_end(va);

    __android_log_print(ANDROID_LOG_DEBUG  , "[btf_media_jni]", "%s", szDebugTemp);
}  

static struct _parcelFields
{
    jclass clazz;
    jfieldID mNativePtr;
    jmethodID obtain;
    jmethodID recycle;
} parcelFields;

static Parcel* parcelForJavaObject(JNIEnv* env, jobject obj)
{
    if (obj) {
        Parcel* p = (Parcel*)env->GetLongField(obj, parcelFields.mNativePtr);
        if (p != NULL) {
            return p;
        }
    }
    return NULL;
}

#if 0
// Uncomment if necessary
static jobject createJavaParcelObject(JNIEnv* env)
{
    return env->CallStaticObjectMethod(parcelFields.clazz, parcelFields.obtain);
}

static void recycleJavaParcelObject(JNIEnv* env, jobject parcelObj)
{
    env->CallVoidMethod(parcelObj, parcelFields.recycle);
}
#endif

static void register_android_os_Parcel(JNIEnv* env)
{
    static const char* const classParcel = "android/os/Parcel";
    jclass parcelClazz = env->FindClass(classParcel);
    if (parcelClazz != NULL)
    {
        parcelFields.clazz = (jclass) env->NewGlobalRef(parcelClazz);
        parcelFields.mNativePtr = env->GetFieldID(parcelClazz, "mNativePtr", "J");
        parcelFields.obtain = env->GetStaticMethodID(parcelClazz, "obtain", "()Landroid/os/Parcel;");
        parcelFields.recycle = env->GetMethodID(parcelClazz, "recycle", "()V");
    }
}



class IptvMediaJniThreadAttach {
public:
    IptvMediaJniThreadAttach() {
        /*
         * attachResult will also be JNI_OK if the thead was already attached to
         * JNI before the call to AttachCurrentThread().
         */
        jint attachResult = gJavaVM->AttachCurrentThread(&mEnv, nullptr);
        if (attachResult != JNI_OK) {
            log_PrintEx("[%s][%d] Unable to attach thread. Error %d", __FUNCTION__, __LINE__, attachResult);        			
        }
    }
    ~IptvMediaJniThreadAttach() {
        jint detachResult = gJavaVM->DetachCurrentThread();
        /*
         * Return if the thread was already detached. Log error for any other
         * failure.
         */
        if (detachResult == JNI_EDETACHED) {
            return;
        }
        if (detachResult != JNI_OK) {
            log_PrintEx("[%s][%d] Unable to detach thread. Error %d", __FUNCTION__, __LINE__, detachResult);
        }
    }
    JNIEnv* getEnv() {
        /*
         * Checking validity of mEnv in case the thread was detached elsewhere.
         */
        if(AndroidRuntime_getJNIEnv() != mEnv){
            log_PrintEx("[%s][%d] AndroidRuntime_getJNIEnv != mEnv", __FUNCTION__, __LINE__);
        }
        return mEnv;
    }
private:
    JNIEnv* mEnv = nullptr;
};

thread_local std::unique_ptr<IptvMediaJniThreadAttach> tJniThreadAttacher;

static JNIEnv* getJniEnv() {
    JNIEnv* env = AndroidRuntime_getJNIEnv();
    /*
     * If env is nullptr, the thread is not already attached to
     * JNI. It is attached below and the destructor for IptvMediaJniThreadAttach
     * will detach it on thread exit.
     */
    if (env == nullptr) {
        tJniThreadAttacher.reset(new IptvMediaJniThreadAttach());
        env = tJniThreadAttacher->getEnv();
    }
    return env;
}

struct fields_t
{
    jfieldID  context;
    jfieldID  surface_texture;
    jfieldID  device;
    jfieldID  handle;
    jfieldID  listener;

    jmethodID post_event;
};

static fields_t Playerfields;

static Mutex sPlayerLock;

enum iptv_media_action_type
{
    IPTV_ACTIONTYPE_NORMAL = 0, 
    IPTV_ACTIONTYPE_OBSERVER, 
};

// static
static void checkAndClearExceptionFromCallback(JNIEnv* env, const char* methodName) {
    if (env->ExceptionCheck()) {
        log_PrintEx("An exception was thrown by callback '%s'.", methodName);
        env->ExceptionClear();
    }
}

// ------------------------------------------------------------------------------------------------
// ref-counted object for IptvMediaPlayer callbacks
class JNIIptvMediaPlayerListener: public IBtvMediaPlayerListener, public hidl_death_recipient
{
public:
    JNIIptvMediaPlayerListener(JNIEnv* env, jobject thiz, jobject weak_thiz, int actionType);
    ~JNIIptvMediaPlayerListener();

    Return<void> notifyCallback(int32_t msg, int32_t ext1, int32_t ext2, const hidl_vec<int8_t>& data) override;
    Return<void> dataCallback(int32_t msg, int32_t ext1, int32_t ext2, const hidl_vec<int8_t>& data) override;

    void         serviceDied(uint64_t cookie __unused, const wp<IBase>& who __unused) override;

private:
    JNIIptvMediaPlayerListener();
    jclass     mClass;        // Reference to IptvMediaPlayer class
    jobject    mObject;       // Weak ref to IptvMediaPlayer Java object to call on
#ifdef ANDROID_4_1_CLASS
    jobject    mParcel;
#endif
    Mutex      mLock;
    int        mActionType;
};

JNIIptvMediaPlayerListener::JNIIptvMediaPlayerListener(JNIEnv* env, jobject thiz, jobject weak_thiz, int actionType)
{

    // Hold onto the IptvMediaPlayer class for use in calling the static method
    // that posts events to the application thread.
    log_PrintEx("[%s] called", __FUNCTION__);
    jclass clazz = env->GetObjectClass(thiz);
    if (clazz == NULL)
    {
        log_PrintEx("Can't find %s", classIptvMediaPlayer);
        //jniThrowException(env, "java/lang/Exception", NULL);
        return;
    }
    mClass = (jclass)env->NewGlobalRef(clazz);

    // We use a weak reference so the IptvMediaPlayer object can be garbage collected.
    // The reference is only used as a proxy for callbacks.
    mObject = env->NewGlobalRef(weak_thiz);
#ifdef ANDROID_4_1_CLASS
    mParcel = env->NewGlobalRef(createJavaParcelObject(env));
#endif 

    mActionType = actionType;
}

JNIIptvMediaPlayerListener::~JNIIptvMediaPlayerListener()
{
    // remote global references
    log_PrintEx("[%s] called", __FUNCTION__);
    JNIEnv *env = getJniEnv();
    env->DeleteGlobalRef(mObject);
    env->DeleteGlobalRef(mClass);

#ifdef ANDROID_4_1_CLASS
    recycleJavaParcelObject(env, mParcel);
    env->DeleteGlobalRef(mParcel);
#endif
}

Return<void> JNIIptvMediaPlayerListener::notifyCallback(int32_t msg, int32_t ext1, int32_t ext2, const hidl_vec<int8_t>& data)
{
    log_PrintEx("[%s] message received msg=%d, ext1=%d, ext2=%d", __FUNCTION__, msg, ext1, ext2);	
    JNIEnv *env = getJniEnv();
    if(env == NULL)
    {
        log_PrintEx("[%s][%d] env is NULL error! No ActionType", __FUNCTION__, __LINE__);
        return Void(); 
    }

    if (data.size() > 0)
    {
        size_t dataLength = data.size();

        std::vector<int8_t> callbackData = data;
        const int8_t *ptr = &callbackData[0];
        const jbyte* jdata = reinterpret_cast<const jbyte*>(ptr);

        jbyteArray array = env->NewByteArray(dataLength);
        env->SetByteArrayRegion(array, 0, dataLength, jdata);
        if (mActionType == IPTV_ACTIONTYPE_NORMAL)
            env->CallStaticVoidMethod(mClass, Playerfields.post_event, mObject, 
                    msg, ext1, ext2, array);
        else
        {
            log_PrintEx("[%s][%d] error! No ActionType", __FUNCTION__, __LINE__);
        }
        env->DeleteLocalRef(array);

    }else{
        if (mActionType == IPTV_ACTIONTYPE_NORMAL)
            env->CallStaticVoidMethod(mClass, Playerfields.post_event, mObject,
                    msg, ext1, ext2, NULL);
        else
        {
            log_PrintEx("[%s][%d] error! No ActionType", __FUNCTION__, __LINE__);
        }

    }
    checkAndClearExceptionFromCallback(env, __FUNCTION__);
    return Void();
}

Return<void> JNIIptvMediaPlayerListener::dataCallback(int32_t msg, int32_t ext1, int32_t ext2, const hidl_vec<int8_t>& data)
{
    Mutex::Autolock _l(mLock);
    JNIEnv *env = getJniEnv();
    if(env == NULL)
    {
        log_PrintEx("[%s][%d] env is NULL error! No ActionType", __FUNCTION__, __LINE__);
        return Void(); 
    }

    if (data.size() > 0)
    {
        size_t dataLength = data.size();

        std::vector<int8_t> callbackData = data;
        const int8_t *ptr = &callbackData[0];
        const jbyte* jdata = reinterpret_cast<const jbyte*>(ptr);

        jbyteArray array = env->NewByteArray(dataLength);
        env->SetByteArrayRegion(array, 0, dataLength, jdata);
        if (mActionType == IPTV_ACTIONTYPE_NORMAL)
            env->CallStaticVoidMethod(mClass, Playerfields.post_event, mObject, 
                msg, ext1, ext2, array);
        else
        {
            log_PrintEx("[%s][%d] error! No ActionType", __FUNCTION__, __LINE__);
        }
        env->DeleteLocalRef(array);

    }
    checkAndClearExceptionFromCallback(env, __FUNCTION__);
    return Void();

}

void JNIIptvMediaPlayerListener::serviceDied(uint64_t cookie __unused, const wp<IBase>& who __unused)
{
    log_PrintEx("[%s] IptvMediaPlayer Service died unexpectedly", __FUNCTION__);
    JNIEnv *env = getJniEnv();
    if(env == NULL)
    {
        log_PrintEx("[%s][%d] env is NULL error!", __FUNCTION__, __LINE__);
        return; 
    }

    log_PrintEx("[%s] CallStaticVoidMethod disconnected service", __FUNCTION__);	

    env->CallStaticVoidMethod(mClass, Playerfields.post_event, mObject,
            7, 0, 0, NULL);

    log_PrintEx("[%s] CallStaticVoidMethod disconnected service end", __FUNCTION__);	
    checkAndClearExceptionFromCallback(env, __FUNCTION__);		
}


// ------------------------------------------------------------------------------------------------

static sp<IBtvMediaPlayer> getIptvMediaPlayer(JNIEnv* env, jobject thiz)
{
    Mutex::Autolock l(sPlayerLock);
    IBtvMediaPlayer *const p = (IBtvMediaPlayer*)env->GetIntField(thiz, Playerfields.context);

    //log_PrintEx("[%s] player:%p called", __FUNCTION__, p);

    return sp<IBtvMediaPlayer>(p);
}

static sp<IBtvMediaPlayer> setIptvMediaPlayer(JNIEnv* env, jobject thiz, const sp<IBtvMediaPlayer>& iptvPlayer)
{
    Mutex::Autolock l(sPlayerLock);
    sp<IBtvMediaPlayer> old = (IBtvMediaPlayer*)env->GetIntField(thiz, Playerfields.context);
    if (iptvPlayer.get())
    {
        iptvPlayer->incStrong(thiz);
    }
    if (old != 0)
    {
        old->decStrong(thiz);
    }
    env->SetIntField(thiz, Playerfields.context, (int)iptvPlayer.get());
    log_PrintEx("[%s] player:%p called", __FUNCTION__, iptvPlayer.get());	
    return old;
}

static sp<JNIIptvMediaPlayerListener> getIptvMediaPlayerListener(JNIEnv* env, jobject thiz)
{
    Mutex::Autolock l(sPlayerLock);
    JNIIptvMediaPlayerListener *const p = (JNIIptvMediaPlayerListener*)env->GetIntField(thiz, Playerfields.listener);

    log_PrintEx("[%s] listener:%p called", __FUNCTION__, p);

    return sp<JNIIptvMediaPlayerListener>(p);
}

static sp<JNIIptvMediaPlayerListener> setIptvMediaPlayerListener(JNIEnv* env, jobject thiz, const sp<JNIIptvMediaPlayerListener>& listener)
{
    Mutex::Autolock l(sPlayerLock);
    sp<JNIIptvMediaPlayerListener> old = (JNIIptvMediaPlayerListener*)env->GetIntField(thiz, Playerfields.listener);
    if (listener.get())
    {
        listener->incStrong(thiz);
    }
    if (old != 0)
    {
        old->decStrong(thiz);
    }
    env->SetIntField(thiz, Playerfields.listener, (int)listener.get());
    log_PrintEx("[%s] listener:%p called", __FUNCTION__, listener.get());	
    return old;
}


static void IptvMediaPlayerGetInstance(JNIEnv *env, jobject thiz, jint deviceID)
{
    log_PrintEx("[%s] called", __FUNCTION__);		

    if (deviceID < 0)
    {
        log_PrintEx("[%s][%d] Failed to connect to IptvMedia Service", __FUNCTION__, __LINE__);
        //jniThrowRuntimeException(env, "deviceID < 0");
        return;
    }

    sp<IBtvMediaPlayer> iptv_mp = NULL;

    if(deviceID == BTV_DEVICE_MAIN){
        iptv_mp = IBtvMediaPlayer::getService(BTV_MAIN_SERVICE);
        log_PrintEx("[%s][%d] connect to MAIN IptvMedia Service", __FUNCTION__, __LINE__);
    }else{
        iptv_mp = IBtvMediaPlayer::getService(BTV_PIP_SERVICE);	
        log_PrintEx("[%s][%d] connect to PIP IptvMedia Service", __FUNCTION__, __LINE__);
    }

    if (iptv_mp == NULL)
    {
        log_PrintEx("[%s][%d] Failed to connect to IptvMedia Service", __FUNCTION__, __LINE__);
        setIptvMediaPlayer(env, thiz, NULL);
        //jniThrowRuntimeException(env, "Fail to connect to IptvMedia Service : Already Inused other object");
        return;
    }

    auto opConStatus = iptv_mp->connect(deviceID);
    if (!opConStatus.isOk() && opConStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of connect() fail %s", __FUNCTION__, opConStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        return;
    }
    
    setIptvMediaPlayer(env, thiz, iptv_mp);

    sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
    if (listener != NULL)
    {

        auto opStatus = iptv_mp->linkToDeath(listener, 0x451F /* cookie */);
        if (!opStatus.isOk()) {
            log_PrintEx("[%s] calling of linkToDeath() fail", __FUNCTION__);
        }

        auto opSetStatus = iptv_mp->setListener(listener);
        if (!opSetStatus.isOk()) {
            log_PrintEx("[%s] calling of setListener() fail %s", __FUNCTION__, opSetStatus.description().c_str());
        }
    }
	
    log_PrintEx("[%s] called end", __FUNCTION__);	
}


// This function gets som field IDs, which in turn causes class initialization.
// It is called from a static block in IptvMediaPlayer, which won't run until the
// first time an instance of this class is used.
static void
media_IptvMediaPlayer_native_init(JNIEnv *env)
{
    log_PrintEx("[%s] called", __FUNCTION__);
    jclass clazz;

    clazz = env->FindClass(classIptvMediaPlayer);
    if (clazz == NULL)
    {
        log_PrintEx("[%s][%d] clazz == NULL ", __FUNCTION__, __LINE__);
        return;
    }

    Playerfields.context = env->GetFieldID(clazz, "mNativeContext", "I");
    if (Playerfields.context == NULL)
    {
        log_PrintEx("[%s][%d] Playerfields.context == NULL", __FUNCTION__, __LINE__);
        return;
    }

    Playerfields.post_event = env->GetStaticMethodID(clazz, "postEventFromNative",
                                                "(Ljava/lang/Object;IIILjava/lang/Object;)V");
    if (Playerfields.post_event == NULL)
    {
        log_PrintEx("[%s][%d] Playerfields.post_event == NULL", __FUNCTION__, __LINE__);
        return;
    }

    Playerfields.surface_texture = env->GetFieldID(clazz, "mNativeSurfaceTexture", "I");	
    if (Playerfields.surface_texture == NULL)	
    {
        log_PrintEx("[%s][%d] Playerfields.surface_texture == NULL", __FUNCTION__, __LINE__);
        return;
    }

    Playerfields.device = env->GetFieldID(clazz, "mDeviceId", "I");
    if (Playerfields.device == NULL)
    {
        log_PrintEx("[%s][%d] Playerfields.device == NULL", __FUNCTION__, __LINE__);
        return;
    }

    Playerfields.handle = env->GetFieldID(clazz, "mNativeHandle", "I");
    if (Playerfields.handle == NULL)
    {
        log_PrintEx("[%s][%d] Playerfields.handle == NULL", __FUNCTION__, __LINE__);
        return;
    }

    Playerfields.listener = env->GetFieldID(clazz, "mListener", "I");
    if (Playerfields.listener == NULL)
    {
        log_PrintEx("[%s][%d] Playerfields.listener == NULL", __FUNCTION__, __LINE__);
        return;
    }

}

static void
media_IptvMediaPlayer_native_setup(JNIEnv *env, jobject thiz, jobject weak_this, jint deviceID)
{
    log_PrintEx("[%s] called", __FUNCTION__);		

    if (deviceID < 0)
    {
        log_PrintEx("[%s][%d] Failed to connect to IptvMedia Service", __FUNCTION__, __LINE__);
        //jniThrowRuntimeException(env, "deviceID < 0");
        return;
    }

    sp<IBtvMediaPlayer> iptv_mp = NULL;

    if(deviceID == BTV_DEVICE_MAIN){
        iptv_mp = IBtvMediaPlayer::getService(BTV_MAIN_SERVICE);
        log_PrintEx("[%s][%d] connect to MAIN IptvMedia Service", __FUNCTION__, __LINE__);
    }else{
        iptv_mp = IBtvMediaPlayer::getService(BTV_PIP_SERVICE);	
        log_PrintEx("[%s][%d] connect to PIP IptvMedia Service", __FUNCTION__, __LINE__);
    }

    if (iptv_mp == NULL)
    {
        log_PrintEx("[%s][%d] Failed to connect to IptvMedia Service", __FUNCTION__, __LINE__);
        setIptvMediaPlayer(env, thiz, NULL);
        //jniThrowRuntimeException(env, "Fail to connect to IptvMedia Service : Already Inused other object");
        return;
    }

    auto opConStatus = iptv_mp->connect(deviceID);
    if (!opConStatus.isOk() && opConStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of connect() fail %s", __FUNCTION__, opConStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        return;
    }
    
    // Stow out new C++ IptvMediaPlayer in an opaque field in the Java object.
    setIptvMediaPlayer(env, thiz, iptv_mp);

    // create new listener and give it to IptvMediaPlayer
    sp<JNIIptvMediaPlayerListener> listener = new JNIIptvMediaPlayerListener(env, thiz, weak_this, IPTV_ACTIONTYPE_NORMAL);
    if (listener != NULL)
    {
        setIptvMediaPlayerListener(env, thiz, listener);

        auto opStatus = iptv_mp->linkToDeath(listener, 0x451F /* cookie */);
        if (!opStatus.isOk()) {
            log_PrintEx("[%s] calling of linkToDeath() fail", __FUNCTION__);
        }
    }

    log_PrintEx("[%s] called end", __FUNCTION__);	
}

// jung2604
static jint
media_IptvMediaPlayer_filePlay(
        JNIEnv *env, jobject thiz, jobject jsurface, jstring path, jint vpid, jint vcodec, jint apid, jint acodec)
{
    log_PrintEx("[%s] called", __FUNCTION__);
    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL)
    {
        log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
        //jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return -1;
    }

    if (path == NULL)
    {
        log_PrintEx("[%s][%d] IllegalArgumentException", __FUNCTION__, __LINE__);
        //jniThrowException(env, "java/lang/IllegalArgumentException", NULL);
        return -1;
    }

    const char *tmp = env->GetStringUTFChars(path, NULL);
    if (tmp == NULL) 	// Out of memory
    {
        return -1;
    }
    log_PrintEx("[%s][%d] open: \n		dataXML => %s", __FUNCTION__, __LINE__, tmp);

    hidl_string pathStr = hidl_string(tmp);

    env->ReleaseStringUTFChars(path, tmp);
    tmp = NULL;

    status_t opStatus = 0;//iptv_mp->filePlay(new_st, pathStr, vpid, vcodec, apid, acodec);
    return (int)opStatus;
}

static jint
media_IptvMediaPlayer_open(
        JNIEnv *env, jobject thiz, jstring dataXML)
{
    log_PrintEx("[%s] called", __FUNCTION__);
    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    if (dataXML == NULL)
    {
        log_PrintEx("[%s][%d] IllegalArgumentException", __FUNCTION__, __LINE__);
        //jniThrowException(env, "java/lang/IllegalArgumentException", NULL);
        return -1;
    }

    const char *tmp = env->GetStringUTFChars(dataXML, NULL);
    if (tmp == NULL) 	// Out of memory
    {
        return -1;
    }

    log_PrintEx("[%s][%d] open: \n		dataXML => %s", __FUNCTION__, __LINE__, tmp);

    hidl_string dataXMLStr = hidl_string(tmp);

    env->ReleaseStringUTFChars(dataXML, tmp);
    tmp = NULL;

    log_PrintEx("[%s] player:%p called", __FUNCTION__, iptv_mp.get());

    auto opStatus = iptv_mp->open(dataXMLStr);
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of open() fail %s", __FUNCTION__, opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);		
        if(listener != NULL) listener->serviceDied(0, nullptr);		
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);    
        return 0;
    }

}

static jint 
media_IptvMediaPlayer_tuneTV(
    JNIEnv *env, jobject thiz, jstring dataXML)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    if (dataXML == NULL)
    {
        log_PrintEx("[%s][%d] IllegalArgumentException", __FUNCTION__, __LINE__);
        //jniThrowException(env, "java/lang/IllegalArgumentException", NULL);
        return -1;
    }

    const char *tmp = env->GetStringUTFChars(dataXML, NULL);
    if (tmp == NULL)
    {
        return -1;
    }
    log_PrintEx("[%s][%d] tuneTV: \n 			dataXML => %s", __FUNCTION__, __LINE__, tmp);

    hidl_string dataXMLStr = hidl_string(tmp);

    env->ReleaseStringUTFChars(dataXML, tmp);
    tmp = NULL;

    log_PrintEx("[%s] player:%p called", __FUNCTION__, iptv_mp.get());	
    auto opStatus = iptv_mp->tuneTV(dataXMLStr);
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of tuneTV() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);		
		if(listener != NULL) listener->serviceDied(0, nullptr);
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint 
media_IptvMediaPlayer_closeTV(
    JNIEnv *env, jobject thiz)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    log_PrintEx("[%s] player:%p called", __FUNCTION__, iptv_mp.get());
    auto opStatus = iptv_mp->closeTV();
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of closeTV() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint 
media_IptvMediaPlayer_bindingFilter(
    JNIEnv *env, jobject thiz, jstring dataXML)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    if (dataXML == NULL)
    {
        log_PrintEx("[%s][%d] IllegalArgumentException", __FUNCTION__, __LINE__);
        //jniThrowException(env, "java/lang/IllegalArgumentException", NULL);
        return -1;
    }

    const char *tmp = env->GetStringUTFChars(dataXML, NULL);
    if (tmp == NULL)
    {
        return -1;
    }
    log_PrintEx("[%s][%d] bindingFilter: \n 			dataXML => %s", __FUNCTION__, __LINE__, tmp);

    hidl_string dataXMLStr = hidl_string(tmp);
    env->ReleaseStringUTFChars(dataXML, tmp);
    tmp = NULL;

    auto opStatus = iptv_mp->bindingFilter(dataXMLStr);
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of bindingFilter() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint 
media_IptvMediaPlayer_releaseFilter(
    JNIEnv *env, jobject thiz, jstring dataXML)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    if (dataXML == NULL)
    {
        log_PrintEx("[%s][%d] IllegalArgumentException", __FUNCTION__, __LINE__);
        //jniThrowException(env, "java/lang/IllegalArgumentException", NULL);
        return -1;
    }

    const char *tmp = env->GetStringUTFChars(dataXML, NULL);
    if (tmp == NULL)
    {
        return -1;
    }
    log_PrintEx("[%s][%d] releaseFilter: \n 			dataXML => %s", __FUNCTION__, __LINE__, tmp);

    hidl_string dataXMLStr = hidl_string(tmp);
    env->ReleaseStringUTFChars(dataXML, tmp);
    tmp = NULL;

    auto opStatus = iptv_mp->releaseFilter(dataXMLStr);
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of releaseFilter() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint 
media_IptvMediaPlayer_changeAudioChannel(
    JNIEnv *env, jobject thiz, jstring dataXML)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    if (dataXML == NULL)
    {
        log_PrintEx("[%s][%d] IllegalArgumentException", __FUNCTION__, __LINE__);
        //jniThrowException(env, "java/lang/IllegalArgumentException", NULL);
        return -1;
    }

    const char *tmp = env->GetStringUTFChars(dataXML, NULL);
    if (tmp == NULL)
    {
        return -1;
    }
    log_PrintEx("[%s][%d] changeAudioChannel: \n 			dataXML => %s", __FUNCTION__, __LINE__, tmp);

    hidl_string dataXMLStr = hidl_string(tmp);
    env->ReleaseStringUTFChars(dataXML, tmp);
    tmp = NULL;

    auto opStatus = iptv_mp->changeAudioChannel(dataXMLStr);
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of changeAudioChannel() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);		
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint 
media_IptvMediaPlayer_changeAudioOnOff(
    JNIEnv *env, jobject thiz, jstring dataXML)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    if (dataXML == NULL)
    {
        log_PrintEx("[%s][%d] IllegalArgumentException", __FUNCTION__, __LINE__);
        //jniThrowException(env, "java/lang/IllegalArgumentException", NULL);
        return -1;
    }

    const char *tmp = env->GetStringUTFChars(dataXML, NULL);
    if (tmp == NULL)
    {
        return -1;
    }
    log_PrintEx("[%s][%d] changeAudioOnOff: \n 			dataXML => %s", __FUNCTION__, __LINE__, tmp);

    hidl_string dataXMLStr = hidl_string(tmp);
    env->ReleaseStringUTFChars(dataXML, tmp);
    tmp = NULL;

    auto opStatus = iptv_mp->changeAudioOnOff(dataXMLStr);
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of changeAudioOnOff() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }
}


static jint 
media_IptvMediaPlayer_getPlayerMode(
    JNIEnv *env, jobject thiz)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    int cur_mode = 0;
    auto opStatus = iptv_mp->getPlayerMode([&](Status retval, int mode) {
        cur_mode = mode;
        if (retval != Status::OK) {
            log_PrintEx("[%s][%d] Current PlayerMode: null", __FUNCTION__, __LINE__);
            return;
        }
        log_PrintEx("[%s][%d] Current PlayerMode: %d", __FUNCTION__, __LINE__, mode);
    });
    
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of getPlayerMode() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    }

    return cur_mode;
}

static jint 
media_IptvMediaPlayer_getPlayerStatus(
    JNIEnv *env, jobject thiz)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    int cur_status = 0;
    auto opStatus = iptv_mp->getPlayerStatus([&](Status retval, int status) {
        cur_status = status;
        if (retval != Status::OK) {
            log_PrintEx("[%s][%d] Current PlayerStatus: null", __FUNCTION__, __LINE__);			
            return;
        }
        log_PrintEx("[%s][%d] Current PlayerStatus: %d", __FUNCTION__, __LINE__, status);
    });
    
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of getPlayerStatus() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    }

    return cur_status;
}



static jint 
media_IptvMediaPlayer_getCurrentPositionInSec(
    JNIEnv *env, jobject thiz)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }


    int cur_pos = 0;
    auto opStatus = iptv_mp->getCurrentPosition([&](Status retval, int pos) {
        cur_pos = pos;
        if (retval != Status::OK) {
            log_PrintEx("[%s][%d] Current PlayerCurrentPosition: null", __FUNCTION__, __LINE__);						
            return;
        }
        log_PrintEx("[%s][%d] Current PlayerCurrentPosition: %d", __FUNCTION__, __LINE__, pos);
    });
    
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of getCurrentPosition() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    }

    return cur_pos;
}


static jint 
media_IptvMediaPlayer_setWindowSize(
    JNIEnv *env, jobject thiz, jstring dataXML)
{
    log_PrintEx("[%s] called", __FUNCTION__);


    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    if (dataXML == NULL)
    {
        log_PrintEx("[%s][%d] IllegalArgumentException", __FUNCTION__, __LINE__);
        //jniThrowException(env, "java/lang/IllegalArgumentException", NULL);
        return -1;
    }

    const char *tmp = env->GetStringUTFChars(dataXML, NULL);
    if (tmp == NULL)
    {
        return -1;
    }
    log_PrintEx("[%s][%d] setWindowSize: \n	dataXML => %s", __FUNCTION__, __LINE__, tmp);

    hidl_string dataXMLStr = hidl_string(tmp);
    env->ReleaseStringUTFChars(dataXML, tmp);
    tmp = NULL;

    auto opStatus = iptv_mp->setWindowSize(dataXMLStr);
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of setWindowSize() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }

}

static jint
media_IptvMediaPlayer_play(JNIEnv *env, jobject thiz)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    log_PrintEx("[%s] player:%p called", __FUNCTION__, iptv_mp.get());
    auto opStatus = iptv_mp->play();
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of play() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint 
media_IptvMediaPlayer_pause(JNIEnv *env, jobject thiz)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    auto opStatus = iptv_mp->pause();
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of pause() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint 
media_IptvMediaPlayer_resume(JNIEnv *env, jobject thiz)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    auto opStatus = iptv_mp->resume();
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of resume() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }

}


static jint 
media_IptvMediaPlayer_seek(JNIEnv *env, jobject thiz, jstring dataXML, jboolean pause)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    if (dataXML == NULL)
    {
        log_PrintEx("[%s][%d] IllegalArgumentException", __FUNCTION__, __LINE__);
        //jniThrowException(env, "java/lang/IllegalArgumentException", NULL);
        return -1;
    }

    const char *tmp = env->GetStringUTFChars(dataXML, NULL);
    if (tmp == NULL) // Out of memory
    {
        return -1;
    }
    log_PrintEx("[%s][%d] seek: \n		dataXML => %s, pause:%s", __FUNCTION__, __LINE__, tmp, pause? "true":"false");

    hidl_string dataXMLStr = hidl_string(tmp);
    env->ReleaseStringUTFChars(dataXML, tmp);
    tmp = NULL;

    auto opStatus = iptv_mp->seek(dataXMLStr, pause);
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of seek() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint 
media_IptvMediaPlayer_trick(JNIEnv *env, jobject thiz, jstring dataXML)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    if (dataXML == NULL)
    {
        log_PrintEx("[%s][%d] IllegalArgumentException", __FUNCTION__, __LINE__);
        //jniThrowException(env, "java/lang/IllegalArgumentException", NULL);
        return -1;
    }

    const char *tmp = env->GetStringUTFChars(dataXML, NULL);
    if (tmp == NULL) 	// Out of memory
    {
        return -1;
    }
    log_PrintEx("[%s][%d] trick: \n		dataXML => %s", __FUNCTION__, __LINE__, tmp);

    hidl_string dataXMLStr = hidl_string(tmp);
    env->ReleaseStringUTFChars(dataXML, tmp);
    tmp = NULL;

    auto opStatus = iptv_mp->trick(dataXMLStr);
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of trick() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint 
media_IptvMediaPlayer_stop(JNIEnv *env, jobject thiz)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    auto opStatus = iptv_mp->stop();
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of stop() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint 
media_IptvMediaPlayer_close(JNIEnv *env, jobject thiz)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    log_PrintEx("[%s] player:%p called", __FUNCTION__, iptv_mp.get());
    auto opStatus = iptv_mp->close();
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of close() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }

}

static sp<ANativeWindow> getIptvVideoSurfaceTexture(JNIEnv* env, jobject thiz)
{
    ANativeWindow* const p = (ANativeWindow*)env->GetIntField(thiz, Playerfields.surface_texture);
    return sp<ANativeWindow>(p);
}

static void decIptvVideoSurfaceRef(JNIEnv *env, jobject thiz)
{
    sp<ANativeWindow> old_st = getIptvVideoSurfaceTexture(env, thiz);
    if (old_st != NULL)
    {
        log_PrintEx("[%s] Releasing window", __FUNCTION__);
        ANativeWindow_release(old_st.get());
        env->SetIntField(thiz, Playerfields.surface_texture, 0);
    }
}


static void 
media_IptvMediaPlayer_release(JNIEnv *env, jobject thiz)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp != NULL)
    {
        log_PrintEx("[%s] player:%p called", __FUNCTION__, iptv_mp.get());
		
#if 0
        auto opStatus = iptv_mp->disconnect();
        if (!opStatus.isOk() && opStatus.isDeadObject()) {
            log_PrintEx("[%s] calling of disconnect() fail %s", __FUNCTION__ , opStatus.description().c_str());
        }
#endif
        setIptvMediaPlayer(env, thiz, NULL);
    }

    sp<ANativeWindow> old_st = getIptvVideoSurfaceTexture(env, thiz);
    if (old_st != NULL)
    {
        log_PrintEx("[%s] Releasing window", __FUNCTION__);
        native_window_set_sideband_stream(old_st.get(), NULL);
        ANativeWindow_release(old_st.get());
    }

#ifdef FEATURE_SOC_AMLOGIC
    native_handle_t* nativeHandle = (native_handle_t*)env->GetIntField(thiz, Playerfields.handle);
    if (nativeHandle != NULL) {
        delete nativeHandle;
        nativeHandle = NULL;
        log_PrintEx("[%s] delete nativeHandle", __FUNCTION__);
    }
#endif

#ifdef FEATURE_SOC_SYNAPTICS
    SidebandNativeHandle* nativeHandle = (SidebandNativeHandle*)env->GetIntField(thiz, Playerfields.handle);
    if (nativeHandle != NULL) {
        delete nativeHandle;
        nativeHandle = NULL;
        log_PrintEx("[%s] delete nativeHandle", __FUNCTION__);
    }
#endif
    // cleanup
    env->SetIntField(thiz, Playerfields.surface_texture, 0);
    env->SetIntField(thiz, Playerfields.handle, 0);
    env->SetIntField(thiz, Playerfields.context, 0);

    sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
    if (listener != NULL)
    {
        setIptvMediaPlayerListener(env, thiz, NULL);
    }

/*
    log_PrintEx("[%s] player:%p called", __FUNCTION__, iptv_mp.get());	

    iptv_mp->disconnect();

    iptv_mp->decStrong(thiz);
*/	
    log_PrintEx("[%s] called end", __FUNCTION__);	
}


static void
media_IptvMediaPlayer_setIptvVideoSurface(JNIEnv *env, jobject thiz, jobject jsurface)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return;
        }
    }

    log_PrintEx("[%s] player:%p called", __FUNCTION__, iptv_mp.get());

    sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
    if (listener != NULL)
    {
        auto opSetStatus  = iptv_mp->setListener(listener);
        if (!opSetStatus.isOk() && opSetStatus.isDeadObject()) {
            log_PrintEx("[%s] calling of setListener() fail %s", __FUNCTION__, opSetStatus.description().c_str());
            setIptvMediaPlayer(env, thiz, NULL);
			if(listener != NULL) listener->serviceDied(0, nullptr); 		
            return;
        }
    }


    decIptvVideoSurfaceRef(env, thiz);

    ANativeWindow *window = ANativeWindow_fromSurface(env, jsurface);
    if(window)
    {
        int width = ANativeWindow_getWidth(window);
        int height = ANativeWindow_getHeight(window);
        log_PrintEx("Got window %p [%d x %d]", window, width, height);
    } else {
        log_PrintEx("[%s] java/lang/IllegalArgumentException , The surface has been released", __FUNCTION__);
        //jniThrowException(env, "java/lang/IllegalArgumentException", "The surface has been released");
        return;
    }

    sp<ANativeWindow> nativeWindow(window);
    env->SetIntField(thiz, Playerfields.surface_texture, (int)nativeWindow.get());

    int device = (int)env->GetIntField(thiz, Playerfields.device);

#ifdef FEATURE_SOC_AMLOGIC
#if 0
    int videotunnel = 1;

    {
        char value[128] = {0};
        if (property_get("persist.vendor.media.skb.videotunnel", value, "1")) {
            if(strcmp(value, "0")==0) {
                videotunnel = 0;
            } else log_PrintEx("set sideband");
        } else log_PrintEx("default sideband");
    }

    if(videotunnel == 1) {
#endif
        native_handle_t* nativeHandle = (native_handle_t*)env->GetIntField(thiz, Playerfields.handle);
        if (nativeHandle != NULL) {
            delete nativeHandle;
            nativeHandle = NULL;
        }

        if(device == BTV_DEVICE_MAIN){
            nativeHandle = am_gralloc_create_sideband_handle(AM_FIXED_TUNNEL, AM_VIDEO_DEFAULT);
            log_PrintEx("native_window_set_sideband_stream BTV_DEVICE_MAIN");
        } else if(device == BTV_DEVICE_PIP){
            nativeHandle = am_gralloc_create_sideband_handle(AM_FIXED_TUNNEL, AM_VIDEO_EXTERNAL);
            log_PrintEx("native_window_set_sideband_stream BTV_DEVICE_PIP");
        }

        native_window_set_sideband_stream(nativeWindow.get(), (native_handle_t*)nativeHandle);

        env->SetIntField(thiz, Playerfields.handle, (int)nativeHandle);
#if 0
    } else {
        sp<Surface> surface;
        if (jsurface) {
            surface = android_view_Surface_getSurface(env, jsurface);
            if (surface != NULL) {
                using TWGraphicBufferProducer = ::android::TWGraphicBufferProducer<::android::hardware::graphics::bufferqueue::V1_0::IGraphicBufferProducer>;
                sp<TWGraphicBufferProducer> tProducer = new TWGraphicBufferProducer(surface->getIGraphicBufferProducer());

                auto opStatus = iptv_mp->setPlayerSurface(tProducer);
                if (!opStatus.isOk() && opStatus.isDeadObject()) {
                    log_PrintEx("[%s] calling of setPlayerSurface() fail %s", __FUNCTION__ , opStatus.description().c_str());
                    setIptvMediaPlayer(env, thiz, NULL);
                    sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
                    if(listener != NULL) listener->serviceDied(0, nullptr);
                } else {
                    log_PrintEx("[%s] called end", __FUNCTION__);
                }
            }
        }
    }
#endif
#endif

#ifdef FEATURE_SOC_SYNAPTICS
    SidebandNativeHandle* nativeHandle = (SidebandNativeHandle*)env->GetIntField(thiz, Playerfields.handle);
    if (nativeHandle != NULL) {
        delete nativeHandle;
        nativeHandle = NULL;
    }

    int videoId = SidebandNativeHandle::SIDEBAND_VIDEO_PLANE_MAIN;
    if(device == BTV_DEVICE_MAIN){
        videoId = SidebandNativeHandle::SIDEBAND_VIDEO_PLANE_MAIN;
        log_PrintEx("native_window_set_sideband_stream BTV_DEVICE_MAIN");
    }else if(device == BTV_DEVICE_PIP){
        videoId = SidebandNativeHandle::SIDEBAND_VIDEO_PLANE_PIP;
        log_PrintEx("native_window_set_sideband_stream BTV_DEVICE_PIP");
    }

    nativeHandle = new SidebandNativeHandle;
    nativeHandle->setVideoPlaneId(videoId);
    native_window_set_sideband_stream(nativeWindow.get(), (native_handle_t*)nativeHandle);		

    env->SetIntField(thiz, Playerfields.handle, (int)nativeHandle);
#endif

    //log_PrintEx("[%s] call setIptvVideoSurfaceTexture", __FUNCTION__);
    //iptv_mp->setIptvVideoSurfaceTexture(new_st);
    //log_PrintEx("[%s] end setIptvVideoSurfaceTexture", __FUNCTION__);
    log_PrintEx("[%s] called end", __FUNCTION__);
}

static jint
media_IptvMediaPlayer_setPlayerSize(JNIEnv *env, jobject thiz,
        jint nLeft, jint nTop, jint nWidth, jint nHeight)
{
    log_PrintEx("[%s] called", __FUNCTION__);		

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    if ((nLeft < 0) || (nTop < 0) || (nWidth < 0) || (nHeight < 0))
    {
        log_PrintEx("[%s][%d] IllegalArgumentException", __FUNCTION__, __LINE__);
        //jniThrowException(env, "java/lang/IllegalArgumentException", NULL);
        return -1;
    }

    auto opStatus = iptv_mp->setPlayerSize(nLeft, nTop, nWidth, nHeight);
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of setPlayerSize() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint 
media_IptvMediaPlayer_startDmxFilter(JNIEnv *env, jobject thiz, jint pid, jint tid)
{
    log_PrintEx("[%s] pid:%d, tid:%d called", __FUNCTION__, pid, tid);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    auto opStatus = iptv_mp->startDmxFilter(pid, tid);
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of startDmxFilter() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint 
media_IptvMediaPlayer_stopDmxFilter(JNIEnv *env, jobject thiz, jint pid, jint tid)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    auto opStatus = iptv_mp->stopDmxFilter(pid, tid);
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of stopDmxFilter() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }
}


static jint
media_IptvMediaPlayer_setDummys(JNIEnv *env, jobject thiz, jstring dummys)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    if (dummys == NULL)
    {
        log_PrintEx("[%s][%d] IllegalArgumentException", __FUNCTION__, __LINE__);
        //jniThrowException(env, "java/lang/IllegalArgumentException", NULL);
        return -1;
    }

    const char *tmp = env->GetStringUTFChars(dummys, NULL);
    if (tmp == NULL) 	// Out of memory
    {
        return -1;
    }
    //log_PrintEx("[%s][%d] setDummys: \n		dummys => %s", __FUNCTION__, __LINE__, tmp);

    hidl_string dummyStr = hidl_string(tmp);

    env->ReleaseStringUTFChars(dummys, tmp);
    tmp = NULL;

    auto opStatus = iptv_mp->setDummys(dummyStr);
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of setDummys() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }
}


static void messageToParcel(const std::vector<ParcelData>& msg, Parcel& parcel)
{
    size_t countEntries = msg.size();
    log_PrintEx("[%s][%d] callback message Data size(%d)", __FUNCTION__, __LINE__, msg.size());

    for (size_t i = 0; i < countEntries; i++) {
        int32_t type = msg[i].parcelType; // type

        log_PrintEx("[%s][%d] callback message Data[%d] Type(%d)", __FUNCTION__, __LINE__, i, msg[i].parcelType);

        switch (type) {
            case TYPE_INT : {
                int32_t val; 
                val = msg[i].parcelInt; 
                log_PrintEx("[%s][%d] callback message Data[%d] int Value(%d)", __FUNCTION__, __LINE__, i, val);
                parcel.writeInt32(val);
                break;
            }
            case TYPE_LONG : {
                int64_t val; 
                val = msg[i].parcelLong; 
                log_PrintEx("[%s][%d] callback message Data[%d] long Value(%d)", __FUNCTION__, __LINE__, i, val);
                parcel.writeInt64(val);
                break;
            }
            case TYPE_STRING : {
                hidl_string val; 
                val = msg[i].parcelString; 
                log_PrintEx("[%s][%d] callback message Data[%d] string Value(%s)", __FUNCTION__, __LINE__, i, val.c_str());				
                parcel.writeUtf8AsUtf16(val.c_str());
                break;
            }
        }
    }
}

static void parcelToMessage(const Parcel& parcel, std::vector<ParcelData>& msg)
{
    parcel.setDataPosition(0);
    int32_t what = parcel.readInt32();
    ParcelData data;
    data.parcelType = TYPE_INT;
    data.parcelInt = what;
    msg.push_back(data);
}

static jint
media_IptvMediaPlayer_native_invoke(JNIEnv *env, jobject thiz,
                                 jobject java_request, jobject java_reply)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    Parcel *request = NULL;
    Parcel *reply = NULL;
    std::vector<ParcelData> Datas;

    if (java_request != NULL) {
        request = parcelForJavaObject(env, java_request);
        parcelToMessage(*request, Datas);
    }

    if (java_reply != NULL) {
        reply = parcelForJavaObject(env, java_reply);
    }

    //log_PrintEx("[%s][%d] invoke Data size(%d), type(%d), value(%d)", __FUNCTION__, __LINE__,
    //Datas.size(), Datas[0].parcelType, Datas[0].parcelInt);

    std::vector<ParcelData> callbackData;
    auto opStatus = iptv_mp->invoke(Datas, [&](Status retval, const hidl_vec<ParcelData>& response) {
        callbackData = response;
        if (retval != Status::OK) {
            log_PrintEx("[%s][%d] invoke Error", __FUNCTION__, __LINE__);
            return;
        }
        //log_PrintEx("[%s][%d] invoke Success", __FUNCTION__, __LINE__);
    });

    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of invoke() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    }

    messageToParcel(callbackData, *reply);
    return 0;
}

static jint 
media_IptvMediaPlayer_pauseAt(JNIEnv *env, jobject thiz, jstring dataXML)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    if (dataXML == NULL)
    {
        log_PrintEx("[%s][%d] IllegalArgumentException", __FUNCTION__, __LINE__);
        //jniThrowException(env, "java/lang/IllegalArgumentException", NULL);
        return -1;
    }

    const char *tmp = env->GetStringUTFChars(dataXML, NULL);
    if (tmp == NULL) // Out of memory
    {
        return -1;
    }
    log_PrintEx("[%s][%d] pauseAt: \n		dataXML => %s", __FUNCTION__, __LINE__, tmp);

    hidl_string dataXMLStr = hidl_string(tmp);
    env->ReleaseStringUTFChars(dataXML, tmp);
    tmp = NULL;

    auto opStatus = iptv_mp->pauseAt(dataXMLStr);
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of pauseAt() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint 
media_IptvMediaPlayer_keepLastFrame(JNIEnv *env, jobject thiz, jboolean flag)
{
    log_PrintEx("[%s] called", __FUNCTION__);

    sp<IBtvMediaPlayer> iptv_mp = getIptvMediaPlayer(env, thiz);
    if (iptv_mp == NULL || (iptv_mp != NULL && !iptv_mp->ping().isOk()))
    {
        int device = (int)env->GetIntField(thiz, Playerfields.device);
        IptvMediaPlayerGetInstance(env, thiz, device);
        iptv_mp = getIptvMediaPlayer(env, thiz);
        if(iptv_mp == NULL){
            log_PrintEx("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
            return -1;
        }
    }

    log_PrintEx("[%s][%d] keepLastFrame: \n		flag => %s", __FUNCTION__, __LINE__, flag? "true" : "false");

    auto opStatus = iptv_mp->keepLastFrame(flag);
    if (!opStatus.isOk() && opStatus.isDeadObject()) {
        log_PrintEx("[%s] calling of pauseAt() fail %s", __FUNCTION__ , opStatus.description().c_str());
        setIptvMediaPlayer(env, thiz, NULL);
        sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
	    if(listener != NULL) listener->serviceDied(0, nullptr);			
        return -1;
    } else {
        log_PrintEx("[%s] called end", __FUNCTION__);
        return 0;
    }
}


// ------------------------------------------------------------------------------------------------

static JNINativeMethod gMethods[] = {
        {"native_init", "()V", (void*)media_IptvMediaPlayer_native_init},
        {"native_setup", "(Ljava/lang/Object;I)V", (void*)media_IptvMediaPlayer_native_setup},
        {"_filePlay", "(Landroid/view/Surface;Ljava/lang/String;IIII)I", (void *)media_IptvMediaPlayer_filePlay},
        {"_open", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_open},
        {"_tuneTV", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_tuneTV},
        {"_closeTV", "()I", (void *)media_IptvMediaPlayer_closeTV},
        {"_bindingFilter", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_bindingFilter},
        {"_releaseFilter", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_releaseFilter},
        {"_changeAudioChannel", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_changeAudioChannel},
        {"_changeAudioOnOff", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_changeAudioOnOff},
        {"_getPlayerMode", "()I", (void *)media_IptvMediaPlayer_getPlayerMode},
        {"_getPlayerStatus", "()I", (void *)media_IptvMediaPlayer_getPlayerStatus},
        {"_getCurrentPositionInSec", "()I", (void *)media_IptvMediaPlayer_getCurrentPositionInSec},
        {"_setWindowSize", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_setWindowSize},
        {"_play", "()I", (void *)media_IptvMediaPlayer_play},
        {"_pause", "()I", (void *)media_IptvMediaPlayer_pause},
        {"_resume", "()I", (void *)media_IptvMediaPlayer_resume},
        {"_seek", "(Ljava/lang/String;Z)I", (void *)media_IptvMediaPlayer_seek},
        {"_trick", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_trick},
        {"_stop", "()I", (void *)media_IptvMediaPlayer_stop},
        {"_close", "()I", (void *)media_IptvMediaPlayer_close},
        {"_setIptvVideoSurface", "(Landroid/view/Surface;)V", (void *)media_IptvMediaPlayer_setIptvVideoSurface},
        {"_setPlayerSize", "(IIII)I", (void *)media_IptvMediaPlayer_setPlayerSize},
        {"_startDmxFilter", "(II)I", (void *)media_IptvMediaPlayer_startDmxFilter},
        {"_stopDmxFilter", "(II)I", (void *)media_IptvMediaPlayer_stopDmxFilter},		 
        {"_setDummys", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_setDummys},
        {"native_release", "()V", (void *)media_IptvMediaPlayer_release},
        {"native_invoke", "(Landroid/os/Parcel;Landroid/os/Parcel;)I", (void *)media_IptvMediaPlayer_native_invoke},
        {"_pauseAt", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_pauseAt},
        {"_keepLastFrame", "(Z)I", (void *)media_IptvMediaPlayer_keepLastFrame},        
};

jint JNI_OnLoad(JavaVM* vm, void* /* reserved */)
{
    log_PrintEx("[%s] called", __func__);
    JNIEnv* env = NULL;
    jint result = -1;

    if (vm->GetEnv((void**)&env, JNI_VERSION_1_6) != JNI_OK)
    {
        log_PrintEx("[%s][%d] GetEnv falied", __func__, __LINE__);
        return JNI_FALSE;
    }

    jclass clazz = env->FindClass(classIptvMediaPlayer);
    if (clazz == NULL) {
        log_PrintEx("[%s][%d] Failed to find %s", __func__, __LINE__, classIptvMediaPlayer);
        return JNI_FALSE;
    }

    if (env->RegisterNatives(clazz, gMethods, NELEM(gMethods)) < 0) {
        log_PrintEx("[%s][%d] Failed to IptvMediaPlayer native registration", __func__, __LINE__);
        return JNI_FALSE;
    }

    register_android_os_Parcel(env);

    gJavaVM = vm;

    result = JNI_VERSION_1_6;
    return result;
}
