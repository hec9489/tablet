# Copyright 2019 SK Broadband Co., LTD.
# Author: Kwon Soon Chan (sysc0507@sk.com)

LOCAL_PATH := $(call my-dir)

###########################################################################################
# com.skb.btvservice@1.0-service
###########################################################################################

include $(CLEAR_VARS)
LOCAL_MODULE := com.skb.btvservice@1.0-service
LOCAL_INIT_RC := com.skb.btvservice@1.0-service.rc
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/btvmediaplayer/iptvmedia/libbtvmediaplayerservice \
    $(LOCAL_PATH)/btvmediaplayer/include/iptvmedia \
    $(LOCAL_PATH)/btvmediaplayer/include \
    $(LOCAL_PATH)/btvmediaplayer/include/hal \
    $(LOCAL_PATH)/btvmediaplayer/libs/libmediaplayer_primitive \
    $(LOCAL_PATH)/btvmediaplayer/libs/libmediaplayer_primitive/dbg \
    $(LOCAL_PATH)/btvmediaplayer/libs/casdrmImpl	\
    $(LOCAL_PATH)/btvmediaplayer/libs/casdrmImpl/include \
    $(LOCAL_PATH)/btvmediaplayer/libs/psi_parser \
    $(LOCAL_PATH)/btvmediaplayer/libs/mlr_client \
    $(LOCAL_PATH)/btvmediaplayer/libs/libdsmcc \
    $(LOCAL_PATH)/btvmediaplayer/libs/pix_ads \
    $(TOP)/vendor/skb/framework/hal/interface \
	$(TOP)/external/tinyxml2 \
    $(LOCAL_PATH)/btvhalmanager \
    $(LOCAL_PATH)/btvmediaplayer/libs/libdmx \
    $(LOCAL_PATH)/tvservice/hal/include \
    $(LOCAL_PATH)/tvservice/hal/device \
    $(LOCAL_PATH)/tvservice/hal/control \
    system/core/base/include \
    frameworks/native/include \
    libnativehelper/include_jni \
    vendor/synaptics/aistb2/sdk/include \
    $(VENDOR_SDK_INCLUDES)


LOCAL_CFLAGS += -Wall -Wunused-parameter -Wmismatched-tags
LOCAL_CFLAGS += -D__LINUX__
LOCAL_LDFLAGS += -nodefaultlibs -lc -lm -ldl

LOCAL_SRC_FILES := \
    service.cpp \
    BtvMediaPlayer.cpp \
    TVService.cpp

LOCAL_SHARED_LIBRARIES := \
        com.skb.btvservice@1.0 \
        libutils\
        liblog\
        libhidlbase\
        libhidltransport\
        libbinder\
        libbtvmediaplayerservice\
        libsystemproperty\
        libtvcorehal\
        libtvcorehdmihal

ifeq ($(VENDOR_SOC), amlogic)

//LOCAL_CFLAGS += -DFEATURE_SOC_AMLOGIC

//LOCAL_C_INCLUDES += frameworks/native/libs/nativewindow/include \
//        frameworks/native/libs/arect/include \
//        frameworks/native/libs/nativebase/include \
//        system/libhidl/transport/token/1.0/utils/include \
//        hardware/libhardware/include \

//LOCAL_SHARED_LIBRARIES += android.hardware.graphics.bufferqueue@1.0 libui libEGL libgui_vendor

endif

LOCAL_MODULE_TAGS := optional
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_EXECUTABLE)

include $(call all-makefiles-under,$(LOCAL_PATH))
