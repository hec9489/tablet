# Copyright 2019 SK Broadband Co., LTD.
# Author: Kwon Soon Chan (sysc0507@sk.com)

LOCAL_PATH:= $(call my-dir)

###########################################################################################
# libbtvhalmgr
###########################################################################################
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_CERTIFICATE := platform

LOCAL_C_INCLUDES := system/core/libutils/include \
                    $(VENDOR_SDK_INCLUDES)

LOCAL_C_INCLUDES += $(TOP)/vendor/skb/framework/hal/interface

LOCAL_CFLAGS += -Wall -Wunused-parameter -Wmismatched-tags
LOCAL_CFLAGS += -D__LINUX__ \
                -DDEFINE_PRODUCT_MODEL=\"$(PRODUCT_MODEL)\"

ifeq ($(PRODUCT_MODEL), BFX-UA300)
$(info "================================ BFX-UA300 =================================")
$(info "==============[ OCAL_CFLAGS += -DUSE_AVP_FOR_VOLUME_PATH ]==================")
LOCAL_CFLAGS += -DUSE_AVP_FOR_VOLUME_PATH
endif

LOCAL_SRC_FILES:= \
	btv_hal_mgr.cpp

LOCAL_SHARED_LIBRARIES := \
        libutils\
        liblog\
	libbtvhal_common \
	libbtvhal_setting \
	libbtvhal_oem

LOCAL_MODULE := libbtvhalmgr

LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_SHARED_LIBRARY)




