LOCAL_PATH:= $(call my-dir)

# btvmedia.jar library
# ============================================================
include $(CLEAR_VARS)

LOCAL_MODULE := btvmedia

LOCAL_MODULE_TAGS := optional

LOCAL_STATIC_JAVA_LIBRARIES := android-support-annotations

aidl_files := \
	src/main/aidl/com/skplanet/hlsplayer/HLSServiceInterface.aidl \
	src/main/aidl/com/skplanet/hlsplayer/IWebVTTCallback.aidl \
	src/main/aidl/com/skplanet/hlsplayer/IPlayerDebugCallback.aidl \
	src/main/aidl/com/skplanet/hlsplayer/IOnInfoCallback.aidl \
	src/main/aidl/com/skplanet/hlsplayer/IOnErrorCallback.aidl \
	src/main/aidl/com/skplanet/hlsplayer/IOnSeekProcessedCallback.aidl \
	src/main/aidl/com/skplanet/hlsplayer/IOnRenderedFirstFrameCallback.aidl \
	src/main/aidl/com/skplanet/hlsplayer/IOnCompletionCallback.aidl \

LOCAL_SRC_FILES := $(call all-java-files-under, src/main/java)
LOCAL_SRC_FILES += $(aidl_files)

$(info aidl_files =  $(aidl_files))

LOCAL_AIDL_INCLUDES += $(FRAMEWORKS_BASE_JAVA_SRC_DIRS)
LOCAL_AIDL_INCLUDES += frameworks/native/aidl/gui
LOCAL_AIDL_INCLUDES += $(LOCAL_PATH)/src/main/aidl

LOCAL_JAVA_VERSION := 1.8

LOCAL_REQUIRED_MODULES := btvmedia.xml

# This will install the file in /system/framework
LOCAL_MODULE_PATH := $(TARGET_OUT_JAVA_LIBRARIES)

LOCAL_DEX_PREOPT := false

include $(BUILD_JAVA_LIBRARY)

# ====  permissions ========================
include $(CLEAR_VARS)

LOCAL_MODULE := btvmedia.xml

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE_CLASS := ETC

# This will install the file in /system/etc/permissions
LOCAL_MODULE_PATH := $(TARGET_OUT_ETC)/permissions

LOCAL_SRC_FILES := $(LOCAL_MODULE)

include $(BUILD_PREBUILT)

