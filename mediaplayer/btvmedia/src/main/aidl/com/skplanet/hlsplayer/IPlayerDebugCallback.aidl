// IPlayerDebugCallback.aidl
package com.skplanet.hlsplayer;

// Declare any non-default types here with import statements
import android.os.Bundle;

interface IPlayerDebugCallback {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
	void onUpdatePlayerDebug(in Bundle bundle);
}
