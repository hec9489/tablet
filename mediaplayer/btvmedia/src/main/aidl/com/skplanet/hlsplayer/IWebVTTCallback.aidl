// IWebVTTCallback.aidl
package com.skplanet.hlsplayer;

// Declare any non-default types here with import statements
import android.os.Bundle;

interface IWebVTTCallback {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void setWebVTTInfoList(in Bundle bundle);
}
