package com.skb.btv.framework.media.core;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.RemoteException;
import android.text.TextUtils;

import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.TextureView;

import com.skb.btv.framework.media.DmxFilter;
import com.skb.btv.framework.media.WebVTT;
import com.skplanet.hlsplayer.HLSServiceInterface;

import com.skb.btv.framework.media.SignalEvent;
import com.skplanet.hlsplayer.IOnCompletionCallback;
import com.skplanet.hlsplayer.IOnErrorCallback;
import com.skplanet.hlsplayer.IOnInfoCallback;
import com.skplanet.hlsplayer.IOnRenderedFirstFrameCallback;
import com.skplanet.hlsplayer.IOnSeekProcessedCallback;
import com.skplanet.hlsplayer.IWebVTTCallback;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SPTek on 2017-09-13.
 * Copyright © 2017년 SPTek. All rights reserved.
 */

public class MediaPlayer {

    private final static String STB_MEDIA_PLAYER_VER = "MediaPlayer.1.0.006";

    private final static String STB_ON_SLEEP = "STB_ON_SLEEP";
    private final static String STB_ON_WAKE_UP = "STB_ON_WAKE_UP";

    private final static Boolean HLS_TUNNEL = true;

    public final static int     IPTV_VOD_TRICK_1XFF		= 0;
    public final static int     IPTV_VOD_TRICK_0_8XFF		= 1;
    public final static int     IPTV_VOD_TRICK_1_2XFF		= 2;
    public final static int     IPTV_VOD_TRICK_1_5XFF		= 3;
    public final static int     IPTV_VOD_TRICK_2XFF		= 4;
    public final static int     IPTV_VOD_TRICK_4XFF		= 5;
    public final static int     IPTV_VOD_TRICK_8XFF		= 6;
    public final static int     IPTV_VOD_TRICK_16XFF	= 7;
    public final static int     IPTV_VOD_TRICK_2XBW		= 8;
    public final static int     IPTV_VOD_TRICK_4XBW		= 9;
    public final static int     IPTV_VOD_TRICK_8XBW		= 10;
    public final static int     IPTV_VOD_TRICK_16XBW	= 11;

    public interface OnPreparedListener {
        void onPrepared(MediaPlayer mp);
    }

    public interface OnInfoListener {
        boolean onInfo(MediaPlayer mp, int what, int extra);
    }

    public interface OnErrorListener {
        boolean onError(MediaPlayer mp, int what, int extra);
    }

    public interface OnCompletionListener {
        void onCompletion(MediaPlayer mp);
    }

    public interface OnSeekCompletionListener {
        void onSeekCompletion(MediaPlayer mp);
    }

    public interface OnSurfacePrepareListener {
        void onUpdated(SurfaceHolder surfaceHolder);
    }

    public interface OnWebVTTUpdateListener {
        void onWebVTTUpdated(MediaPlayer mp, List<WebVTT> webVTTList);
    }

    public interface OnDsmccListener    {
        void onDsmccEvent(MediaPlayer mp, String path, SignalEvent sigevent);
    }

    public interface OnDmxFilterListener  {
        void onDmxFilter(MediaPlayer mp,  byte[] sectiondata);
    }

    public class TrackInfo {
        public static final int MEDIA_TRACK_TYPE_UNKNOWN = 0;
        public static final int MEDIA_TRACK_TYPE_VIDEO = 1;
        public static final int MEDIA_TRACK_TYPE_AUDIO = 2;
        public static final int MEDIA_TRACK_TYPE_TIMEDTEXT = 3;
        public static final int MEDIA_TRACK_TYPE_SUBTITLE = 4;
        public static final int MEDIA_TRACK_TYPE_METADATA = 5;

        int trackType;

        public int getTrackType() {
            return trackType;
        }

        TrackInfo() {
            trackType = MEDIA_TRACK_TYPE_AUDIO;
        }
    }


    public enum TRICK_SPEED {
        BX160(IPTV_VOD_TRICK_16XBW, -16.0),
        BX080(IPTV_VOD_TRICK_8XBW, -8.0),
        BX040(IPTV_VOD_TRICK_4XBW, -4.0),
        BX020(IPTV_VOD_TRICK_2XBW, -2.0),
        X010(IPTV_VOD_TRICK_1XFF, 1.0),
        X008(IPTV_VOD_TRICK_0_8XFF, 0.8),
        X012(IPTV_VOD_TRICK_1_2XFF, 1.2),
        X015(IPTV_VOD_TRICK_1_5XFF, 1.5),
        X020(IPTV_VOD_TRICK_2XFF, 2.0),
        X040(IPTV_VOD_TRICK_4XFF, 4.0),
        X080(IPTV_VOD_TRICK_8XFF, 8.0),
        X160(IPTV_VOD_TRICK_16XFF, 16.0),;

        int speedId;
        double speedRatio;

        TRICK_SPEED(int i, double ratio) {
            speedId = i;
            speedRatio = ratio;
        }

        public int getSpeedId() {
            return speedId;
        }

        public TRICK_SPEED next() {
            return values()[Math.min(this.ordinal() + 1, values().length - 1)];
        }

        public TRICK_SPEED prev() {
            return values()[Math.max(this.ordinal() - 1, 0)];
        }

        public double getSpeedRatio() {
            return speedRatio;
        }
    }

    private static HLSServiceInterface mHLSPlayer = null;
    private boolean hlsPlayer_init;
    private static boolean mForceQuit = false;
    private IptvMediaPlayer.OnDsmccCallbackListener iptvMediaPlayerDsmccListener;
    private IptvMediaPlayer legacyPlayer;
    private MediaPlayer.OnPreparedListener onPreparedListener;
    private MediaPlayer.OnInfoListener onInfoListener;
    private MediaPlayer.OnErrorListener onErrorListener;
    private MediaPlayer.OnCompletionListener onCompletionListener;
    private MediaPlayer.OnSeekCompletionListener onSeekCompletionListener;
    private MediaPlayer.OnWebVTTUpdateListener onWebVTTUpdateListener;
    private MediaPlayer.OnDsmccListener onDsmccListener;
    private MediaPlayer.OnDmxFilterListener  onDmxFilterListener;
    private SurfaceHolder surfaceHolder;
//    private SurfaceHolder ccSurfaceHolder;
    private TextureView ccTextureView;

    private IptvPlayInfo playInfo;
    private IptvTuneInfo tuneInfo;
    private ArrayList<IptvInfo> mvTuneInfo;
    private int mvMode = 0;
    private int playerId;

    private List<WebVTT> mWebVTTInfoList;
    private boolean mLastMVAudioOnOff= false;
    private IptvSectionInfo sectionInfo;
    private boolean isPlaying = false, isPaused = false;
    private boolean isConnected = false;
    private Handler mHandler;
    private long duration;
    private long currntTime;
    private boolean bShowFirstFrame;
    private long retryTimeOffset;
    private Context mContext;
    private BroadcastReceiver mSleepBroadcastReceiver;

    public MediaPlayer(Context context) {
        mContext = context;
        legacyPlayer = IptvMediaPlayer.connect(IptvMediaPlayer.IPTV_DEVICE_MAIN);
        mForceQuit = false;
        bindHLSPlayerConnection();
        playerId = IptvMediaPlayer.IPTV_DEVICE_MAIN;
        initialize();
        isConnected = true;
        // Inform disconnected message to Tvsystem
        //Intent intent = new Intent("com.skbb.tv.MEDIA_CONNECT");
        //mContext.sendBroadcast(intent);
    }

    public MediaPlayer(Context context, int pipId) {
        mContext = context;
        legacyPlayer = IptvMediaPlayer.connect(pipId);
        mForceQuit = false;
        bindHLSPlayerConnection();
        playerId = pipId;
        initialize();
        isConnected = true;
        if(playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN) {
            // Inform disconnected message to Tvsystem
            //Intent intent = new Intent("com.skbb.tv.MEDIA_CONNECT");
            //mContext.sendBroadcast(intent);
        }
    }

    public String getVersion() {
        SLog.i("IPTV", "Get Version: " + STB_MEDIA_PLAYER_VER);
        return STB_MEDIA_PLAYER_VER;
    }

    private void initialize() {

        onPreparedListener = null;
        onInfoListener = null;
        onErrorListener = null;
        onCompletionListener = null;
        onSeekCompletionListener = null;
        onWebVTTUpdateListener = null;
        onDsmccListener = null;
        onDmxFilterListener = null;
        hlsPlayer_init = false;

        legacyPlayer.setOnDisconnectedListener(new IptvMediaPlayer.OnDisconnectedListener() {
            @Override
            public void onDisconnected(IptvMediaPlayer iptv_mp, int nDevice, int nReason) {
                SLog.d("IPTV_CALLBACK", "onDisconnected - nDevice : " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP") + nDevice + " nReason : " + nReason);
                isConnected = false;
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        SLog.d("IPTV_CALLBACK", "onErrorListener - onError(what:100, extra:0) before" + ", this->" + MediaPlayer.this);
                        if (onErrorListener != null) {
                            SLog.d("IPTV_CALLBACK", "onErrorListener - onError(what:100, extra:0)" + ", this->" + MediaPlayer.this);
                            onErrorListener.onError(MediaPlayer.this, 100 , 0);
                        }
/*
                        if(playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN) {
                            // only IPTV_DEVICE_MAIN player reconnect => PIP is UI error handling
                            legacyPlayer = IptvMediaPlayer.connect(playerId);
                            initialize();
                            isConnected = true;
                            // Inform disconnected message to Tvsystem
                            //Intent intent = new Intent("com.skbb.tv.MEDIA_CONNECT");
                            //mContext.sendBroadcast(intent);
                            //onErrorListener.onError(MediaPlayer.this, 10000 , 9999);
                            if (onErrorListener != null) {
                                onErrorListener.onError(MediaPlayer.this, 100 , 0);
                            }
                        }
 */
                    }
                }, 700);
            }
        });

        legacyPlayer.setOnPlayStatusListener(new IptvMediaPlayer.OnPlayStatusListener() {
            @Override
            public void onPlayStatus(IptvMediaPlayer iptv_mp, int nDevice, String playStatus) {
                SLog.d("IPTV_CALLBACK", "onPlayStatus - nDevice : " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP") + nDevice + " playStatus : " + playStatus);

                Status status = Status.parse(playStatus);

                if (status instanceof Status.StatusPlay) {
                    if (((Status.StatusPlay) status).getPlaybackStatus().equals("PLAY")) {
                        if (onInfoListener != null) {
                            if (((Status.StatusPlay) status).getTime() == 0L && currntTime > 0) {
                                onInfoListener.onInfo(MediaPlayer.this, 10000, 20001);
                                SLog.i("IPTV", "Send Play Start Info(20001)");
                            }
                        }
                    } else if (((Status.StatusPlay) status).getPlaybackStatus().equals("STOP_EOF")) {
                        if (onCompletionListener != null) {
                            onCompletionListener.onCompletion(MediaPlayer.this);
                        }
                    } else if (((Status.StatusPlay) status).getPlaybackStatus().equals("RTSP_EOS")) {
                        if (onInfoListener != null) {
                            onInfoListener.onInfo(MediaPlayer.this, 10000, 20006);
                            SLog.i("IPTV", "Send RTSP_EOS Info(20006)");
                        }
                    }

                    duration = ((Status.StatusPlay) status).getDuration();
                    currntTime = ((Status.StatusPlay) status).getTime();
                    SLog.i("IPTV", "Set Current Position : " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main " : "PIP ") + currntTime);
                } else if (onErrorListener != null) {
                    if (status instanceof Status.StatusError) {
                        if(((Status.StatusError) status).getErrorCode() == 1001) {
                            onInfoListener.onInfo(MediaPlayer.this, 10000, ((Status.StatusError) status).getErrorCode());
                        } else {
                            onErrorListener.onError(MediaPlayer.this, 10000, ((Status.StatusError) status).getErrorCode());
                        }
                    }
                }
            }
        });

        legacyPlayer.setOnSectionFilterListener(new IptvMediaPlayer.OnSectionFilterListener() {
            @Override
            public void onSectionFilter(IptvMediaPlayer iptv_mp, byte[] sectionData) {
                StringBuilder log = new StringBuilder();
                for (byte b : sectionData) {
                    log.append(String.format("0x%02X ", b));
                }
                SLog.i("IPTV_CALLBACK", "onSectionFilter - " + " sectionData : " + log.toString());
                if (onDmxFilterListener != null) {
                    onDmxFilterListener.onDmxFilter(MediaPlayer.this, sectionData);
                }
            }
        });

        legacyPlayer.setOnPidChangedListener(new IptvMediaPlayer.OnPidChangedListener() {
            @Override
            public void onPidChagned(IptvMediaPlayer iptv_mp, int event, String pid) {
                SLog.d("IPTV_CALLBACK", "onPidChagned - event : " + event + " pid : " + pid);
                if (onInfoListener != null){
                    int what = 7001; // Video pid Changed
                    if(event == 1) what = 7002; // Audio pid changed
                    if(event == 2) what = 7003; // num of Audio pid changed
                    int ProcessId = Integer.parseInt(pid);
                    onInfoListener.onInfo(MediaPlayer.this, what, ProcessId);
                }
            }
        });

        legacyPlayer.setOnSeekCompletionListener(new IptvMediaPlayer.OnSeekCompletionListener() {
            @Override
            public void onSeekCompletion(IptvMediaPlayer iptv_mp) {
                SLog.d("IPTV_CALLBACK", "onSeekCompletion");
                if (onSeekCompletionListener != null){
                    onSeekCompletionListener.onSeekCompletion(MediaPlayer.this);
                }
            }
        });

        legacyPlayer.setOnTuneEventListener(new IptvMediaPlayer.OnTuneEventListener() {
            @Override
            public void onTuneEvent(IptvMediaPlayer iptv_mp, int nDevice, int nStatus) {
                SLog.d("IPTV_CALLBACK", "onTuneEvent - nDevice : " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP") + nDevice + " nStatus : " + nStatus);
                if (onInfoListener != null) {
                    switch (nStatus) {
                        case 0:
                            if (!bShowFirstFrame) {
                                if(mvTuneInfo != null){
                                    // Inform firtst iframe event to hlsplayer
                                    Intent intent = new Intent("com.skplanet.hlsplayer.action.PIP_ENTER");
                                    mContext.sendBroadcast(intent);
                                }
                                SLog.d("IPTV_CALLBACK", "before onInfo ");
                                onInfoListener.onInfo(MediaPlayer.this, 10000, 20008);
                                SLog.d("IPTV_CALLBACK", "after onInfo ");
                                bShowFirstFrame = true;
                            }else{
                                SLog.d("IPTV_CALLBACK", "bShowFirstFrame == true");
                            }
                            break;
                        case 1:
                            if (System.currentTimeMillis() - retryTimeOffset > 2000) {
                                onInfoListener.onInfo(MediaPlayer.this, 10000, 1001);
                            }
                            break;
                    }
                }else{
                    SLog.d("IPTV_CALLBACK", "onInfoListener is null ");
                }
            }
        });

        if(mContext != null) {
            mSleepBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if(intent.getAction().equalsIgnoreCase(STB_ON_SLEEP)) {
                        SLog.d("IPTV_CALLBACK", "sptek:STB_ON_SLEEP receive " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP"));
                        reset();
                    } else if(intent.getAction().equalsIgnoreCase(STB_ON_WAKE_UP)) {
                        //TODO do nothing
                        SLog.d("IPTV_CALLBACK", "sptek:STB_ON_WAKE_UP receive " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP"));
                    }
                }
            };

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(STB_ON_SLEEP);
            intentFilter.addAction(STB_ON_WAKE_UP);

            mContext.registerReceiver(mSleepBroadcastReceiver, intentFilter);
        }

        mHandler = new Handler(Looper.getMainLooper());
    }


    private void bindHLSPlayerConnection() {
        SLog.d("IPTV_HLS", "IN|" + " bindHLSPlayerConnection");
        Intent i = new Intent();
        i.setComponent(new ComponentName("com.skplanet.hlsplayer", "com.skplanet.hlsplayer.HLSPlayerService"));
        boolean bindResult = mContext.bindService(i, mHLSconnection, Context.BIND_AUTO_CREATE);
        SLog.d("IPTV_HLS", "OUT|" + " bindService : " + bindResult);
    }

    private void retryHLSPlayerConnection() {
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                if(mHLSPlayer == null) {
                    bindHLSPlayerConnection();
                }
            }
        }, 1000);
    }

    private void unbindHLSPlayerConnection() {
        SLog.d("IPTV_HLS", "IN|" + " unbindHLSPlayerConnection");
        mForceQuit = true;
        if (mHLSPlayer != null) {
            mContext.unbindService(mHLSconnection);
        }
        SLog.d("IPTV_HLS", "OUT|" + " unbindHLSPlayerConnection");
    }

    IOnInfoCallback mOnInfoCallback = new IOnInfoCallback.Stub() {
        @Override
        public void onInfo(Message what) throws RemoteException {
            Bundle bundle = what.getData();
            if (bundle.containsKey("PLAYER_STATE")) {
                SLog.i("IPTV_CALLBACK", "onInfo : " + " PLAYER_STATE : " + bundle.getInt("PLAYER_STATE"));
            }
            if (bundle.containsKey("RENDER_FIRSTFRAME")) {
                SLog.i("IPTV_CALLBACK", "onInfo : " + " RENDER_FIRSTFRAME : " + bundle.getInt("RENDER_FIRSTFRAME"));
            }
/*
            if (onInfoListener != null) {
                if (bundle.containsKey("RENDER_FIRSTFRAME")) {
                    if (bundle.getInt("RENDER_FIRSTFRAME") == 1) {
                        if (mHLSPlayer.getCurrentPosition() == 0L) {
                            TRICK_SPEED trickSpeed = TRICK_SPEED.X010;
                            mHLSPlayer.playTrick((float)trickSpeed.getSpeedRatio());
                            onInfoListener.onInfo(MediaPlayer.this, 10000, 20001);
                            SLog.i("IPTV_CALLBACK", "Send Play Start Info(20001)");
                        }
                        if (!bShowFirstFrame) {
                            onInfoListener.onInfo(MediaPlayer.this, 10000, 20008);
                            SLog.i("IPTV_CALLBACK", "Send Play Start Info(20008)");
                            bShowFirstFrame = true;
                        }
                    }
                }
            }
 */
        }
    };

    IOnErrorCallback mOnErrorCallback = new IOnErrorCallback.Stub() {
        @Override
        public void onError(int errorCode, long detailErrorCode, String msg) throws RemoteException {
            SLog.i("IPTV_CALLBACK", "HLS onError : " + "error : " + errorCode + ", detail error : " + detailErrorCode + ", msg : " + msg);
            if(errorCode == 1001){
                if (onInfoListener != null) {
                    onInfoListener.onInfo(MediaPlayer.this, 10000, errorCode);
                }
            }else{
                if (onErrorListener != null) {
                    onErrorListener.onError(MediaPlayer.this, 10000, errorCode);
                }
            }
        }
    };

    IOnCompletionCallback mOnCompletionCallback = new IOnCompletionCallback.Stub() {
        @Override
        public void onCompletion() throws RemoteException {
            SLog.i("IPTV_CALLBACK", "HLS onCompletion");
            if (onCompletionListener != null) {
                onCompletionListener.onCompletion(MediaPlayer.this);
            }
        }
    };

    IOnSeekProcessedCallback mOnSeekCompletionCallback = new IOnSeekProcessedCallback.Stub() {
        @Override
        public void onSeekProcessed() throws RemoteException {
            SLog.i("IPTV_CALLBACK", "HLS onSeekProcessed");
            if (onSeekCompletionListener != null){
                SLog.i("IPTV_CALLBACK", "Send onSeekCompletion");
                onSeekCompletionListener.onSeekCompletion(MediaPlayer.this);
            }
        }
    };

    IOnRenderedFirstFrameCallback mOnRenderedFirstFrameCallback = new IOnRenderedFirstFrameCallback.Stub() {
        @Override
        public void onRenderedFirstFrame() throws RemoteException {
            SLog.i("IPTV_CALLBACK", "HLS onRenderedFirstFrame");
            if (onInfoListener != null) {
                if (mHLSPlayer.getCurrentPosition() == 0L) {
                    TRICK_SPEED trickSpeed = TRICK_SPEED.X010;
                    mHLSPlayer.playTrick((float)trickSpeed.getSpeedRatio());
                    onInfoListener.onInfo(MediaPlayer.this, 10000, 20001);
                    SLog.i("IPTV_CALLBACK", "Send Play Start Info(20001)");
                }
                if (!bShowFirstFrame) {
                    onInfoListener.onInfo(MediaPlayer.this, 10000, 20008);
                    SLog.i("IPTV_CALLBACK", "Send Play Start Info(20008)");
                    bShowFirstFrame = true;
                }
            }
        }
    };

    IWebVTTCallback mFirstWebVTTCallback = new IWebVTTCallback.Stub() {
        @Override
        public void setWebVTTInfoList(Bundle bundle) throws RemoteException {
            SLog.i("IPTV_CALLBACK", "HLS setWebVTTInfoList");
            if (onWebVTTUpdateListener != null) {
                mWebVTTInfoList.clear();

                long[] startTimeList = bundle.getLongArray("START_TIME");
                long[] endTimeList = bundle.getLongArray("END_TIME");

                ArrayList<CharSequence>
                        textList= bundle.getCharSequenceArrayList("TEXT");

                if(textList != null) {
                    long startTime = startTimeList[0];
                    long endTime = endTimeList[0];
                    CharSequence text = textList.get(0);
                    SLog.i("IPTV_CALLBACK", "start time:" + startTime + ", end time:" + endTime + ", text:" + text);
                    mWebVTTInfoList.add(new WebVTT(startTime, endTime, text));
                    onWebVTTUpdateListener.onWebVTTUpdated(MediaPlayer.this, mWebVTTInfoList);
                }
            }
        }
    };

    private ServiceConnection mHLSconnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            SLog.d("IPTV_HLS", "IN|" + "onServiceConnected : Class = " + name.getClassName() + ", Package = " + name.getPackageName());
            if (mHLSPlayer == null) {
                try {
                    mHLSPlayer = HLSServiceInterface.Stub.asInterface(service);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            SLog.d("IPTV_HLS", "OUT|" + "onServiceConnected ");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            SLog.d("IPTV_HLS", "IN|" + "connection mHLSPlayer " + "onServiceDisConnected : " + name.getClassName());
            mHLSPlayer = null;
            SLog.d("IPTV_CALLBACK", "onErrorListener - onError(what:100, extra:0) before" + ", this->" + MediaPlayer.this);
            if (onErrorListener != null) {
                SLog.d("IPTV_CALLBACK", "onErrorListener - onError(what:100, extra:0)" + ", this->" + MediaPlayer.this);
                onErrorListener.onError(MediaPlayer.this, 100 , 0);
            }
/*
            if(mForceQuit == false){
                retryHLSPlayerConnection();
            }
 */
            SLog.d("IPTV_HLS", "OUT|" + "connection mHLSPlayer " + "onServiceDisConnected : ");
        }
    };

    private void hls_player_initialize(){
        if(mHLSPlayer != null) {
            try {
                SLog.i("IPTV_HLS", "before hlsPlayer init");
                if(hlsPlayer_init == false){
                    if(HLS_TUNNEL) {
                        SLog.i("IPTV_HLS", "hlsPlayer init tunnel mode");
                        mHLSPlayer.init(true);
                    }else{
                        SLog.i("IPTV_HLS", "hlsPlayer init non-tunnel mode");
                        mHLSPlayer.init(false);
                    }
                    mWebVTTInfoList = new ArrayList();
                    hlsPlayer_init = true;
                }

                Surface surface;
                if (surfaceHolder != null)
                {
                    surface = surfaceHolder.getSurface();
                }
                else
                {
                    surface = null;
                }
                if(surface != null) {
                    mHLSPlayer.setSurface(surface);
                }

                mHLSPlayer.registerOnInfoCallback(mOnInfoCallback);
                mHLSPlayer.registerOnErrorCallback(mOnErrorCallback);
                mHLSPlayer.registerOnCompletionCallback(mOnCompletionCallback);
                mHLSPlayer.registerFirstWebVTTCallback(mFirstWebVTTCallback);
                mHLSPlayer.registerOnSeekProcessedCallback(mOnSeekCompletionCallback);
                mHLSPlayer.registerOnRenderedFirstFrameCallback(mOnRenderedFirstFrameCallback);
                //mHLSPlayer.registerSecondWebVTTCallback(mSecondWebVTTCallback);
                //mHLSPlayer.registerPlayerDebugCallback(mPlayerDebugCallback);
                SLog.i("IPTV_HLS", "after hlsPlayer init");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void hls_player_release(){
        if(hlsPlayer_init) {
            try {
                SLog.d("IPTV_HLS", "before hls_player release");
                mWebVTTInfoList.clear();
                SLog.d("IPTV_HLS", "before hls_player stop");
                mHLSPlayer.stop(true);
                SLog.d("IPTV_HLS", "after hls_player stop");
                mHLSPlayer.unregisterOnInfoCallback(mOnInfoCallback);
                mHLSPlayer.unregisterOnErrorCallback(mOnErrorCallback);
                mHLSPlayer.unregisterOnCompletionCallback(mOnCompletionCallback);
                mHLSPlayer.unregisterFirstWebVTTCallback(mFirstWebVTTCallback);
                mHLSPlayer.unregisterOnSeekProcessedCallback(mOnSeekCompletionCallback);
                mHLSPlayer.unregisterOnRenderedFirstFrameCallback(mOnRenderedFirstFrameCallback);
                mHLSPlayer.release();
                mWebVTTInfoList = null;
                hlsPlayer_init = false;
                SLog.d("IPTV_HLS", "after hls_player release");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    //Register a callback to be invoked when the media source is ready for playback.
    public void setOnPreparedListener(MediaPlayer.OnPreparedListener listener) {
        onPreparedListener = listener;
        SLog.d("IPTV",  "IN|listener = " + listener + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
    }


    //Register a callback to be invoked when an info/warning is available.
    public void setOnInfoListener(MediaPlayer.OnInfoListener listener) {
        onInfoListener = listener;
        SLog.d("IPTV",  "IN|listener = " + listener + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
    }

    //Register a callback to be invoked when an error has happened during an asynchronous operation.
    public void setOnErrorListener(MediaPlayer.OnErrorListener listener) {
        onErrorListener = listener;
        SLog.d("IPTV",  "IN|listener = " + listener + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
    }

    //Register a callback to be invoked when the end of a media source has been reached during playback.
    public void setOnCompletionListener(MediaPlayer.OnCompletionListener listener) {
        onCompletionListener = listener;
        SLog.d("IPTV",  "IN|listener = " + listener + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
    }

    //Register a callback to be invoked when seek completed
    protected void setOnSeekCompletionListener(MediaPlayer.OnSeekCompletionListener listener) {
        onSeekCompletionListener = listener;
        SLog.d("IPTV",  "IN|listener = " + listener + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
    }

    //Register a callback to be invoked when WebVTT Updated
    protected void setOnWebVTTUpdateListener(MediaPlayer.OnWebVTTUpdateListener listener) {
        onWebVTTUpdateListener = listener;
        SLog.d("IPTV",  "IN|listener = " + listener + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
/*
        if(listener != null) {
            if(hlsplayerWebVTTComponent == null){
                hlsplayerWebVTTComponent = new HlsPlayer.WebVTTComponent() {
                    @Override
                    public void setWebVTTInfoList(List<WebVTTInfo> list) {
                        //SLog.i("IPTV_CALLBACK", "setWebVTTInfoList");
                        // Print the name from the list....
                        if (onWebVTTUpdateListener != null) {
                            mWebVTTInfoList.clear();
                            for(WebVTTInfo info : list) {
                                SLog.i("IPTV_CALLBACK", "start time:" + info.startTime + ", end time:" + info.endTime + ", text:" + info.text);
                                mWebVTTInfoList.add(new WebVTT(info.startTime, info.endTime, info.text));
                            }
                            onWebVTTUpdateListener.onWebVTTUpdated(MediaPlayer.this, mWebVTTInfoList);
                        }
                    }
                };
            }
            hlsPlayer.setWebVTTComponent(hlsplayerWebVTTComponent);
        }

 */
    }

    //Register a callback to be invoked when demux data received
    protected void setDmxFilterListener(MediaPlayer.OnDmxFilterListener  listener) {
        onDmxFilterListener = listener;
    }

    //Register a callback to be invoked when dsmcc data received
    protected void setOnDsmccListener(MediaPlayer.OnDsmccListener listener) {
        onDsmccListener = listener;
        SLog.d("IPTV",  "IN|listener = " + listener + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        if(listener != null) {
            if(iptvMediaPlayerDsmccListener == null){
                iptvMediaPlayerDsmccListener = new IptvMediaPlayer.OnDsmccCallbackListener() {
                    @Override
                    public void onDsmccEventUpdated(String path, SignalEvent event) {
                        SLog.d("IPTV_CALLBACK", "onDsmccEventUpdated - " + " path : " + path);
                        if (onDsmccListener != null) {
                            onDsmccListener.onDsmccEvent(MediaPlayer.this, path, event);
                        }
                    }
                };
            }
            legacyPlayer.setOnDsmccCallbackListener(iptvMediaPlayerDsmccListener);
        }
    }

    //Sets the SurfaceHolder to use for displaying the video portion of the media.

    //Need to check surface created. shall you use setDisplay(SurfaceHolder sh, final OnSurfacePrepareListener listener)?
    public void setDisplay(SurfaceHolder sh) {
        surfaceHolder = sh;
        if(sh == null) {
            SLog.d("IPTV",  "IN|SurfaceHolder = null" + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        }else {
            SLog.d("IPTV",  "IN|SurfaceHolder = " + sh + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
            if(playerId != IptvMediaPlayer.IPTV_DEVICE_MAIN){
                try {
                    Thread.sleep(5);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
            SLog.d("IPTV",  "setIptvDisplay = " + surfaceHolder + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
            legacyPlayer.setIptvDisplay(surfaceHolder);
        }
    }

    //Sets the data source (file-path or http/rtsp URL) to use.
    public void setDataSource(String strUri) {
        SLog.d("IPTV",  "IN|strUri = " + strUri + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            Uri uri = Uri.parse(strUri);
            String scheme = uri.getScheme();

            if (scheme.equalsIgnoreCase("skbiptv")) {         //IPTV
                if (tuneInfo != null) {
                    tuneInfo = null;
                }
                reset();
                tuneInfo = getTuneInfo(uri);
//                SLog.d("IPTV",  "setIptvDisplay = " + surfaceHolder + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
//                legacyPlayer.setIptvDisplay(surfaceHolder);
            } else if (scheme.equalsIgnoreCase("skbvod")) {   //VOD
                reset();
                playInfo = getVodPlayInfo(uri);
//                SLog.d("IPTV",  "setIptvDisplay = " + surfaceHolder + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
//                legacyPlayer.setIptvDisplay(surfaceHolder);
            } else if (scheme.equalsIgnoreCase("skbfile")) {   //FILE
                reset();
                playInfo = getFilePlayInfo(uri);
//                SLog.d("IPTV",  "setIptvDisplay = " + surfaceHolder + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
//                legacyPlayer.setIptvDisplay(surfaceHolder);
            } else if (scheme.equalsIgnoreCase("skbsmv")) {   //멀티 뷰
                if (mvTuneInfo != null) {
                    mvTuneInfo = null;
                }
                reset();
                mvTuneInfo = getMVTuneInfo(uri);
//                SLog.d("IPTV",  "setIptvDisplay = " + surfaceHolder + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
//                legacyPlayer.setIptvDisplay(surfaceHolder);
            } else if ((scheme.equalsIgnoreCase("skbhttp") || scheme.equalsIgnoreCase("skbhttps"))) {
                SLog.d("IPTV-HLS","setDataSource HLS ");
                reset();
                playInfo = getHLSPlayInfo(uri);
//                SLog.d("IPTV",  "setIptvDisplay = " + surfaceHolder + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
//                if(playerId != IptvMediaPlayer.IPTV_DEVICE_MAIN){
//                    try {
//                        Thread.sleep(5);
//                    }catch (InterruptedException e){
//                        e.printStackTrace();
//                    }
//                }
//                legacyPlayer.setIptvDisplay(surfaceHolder);
                SLog.d("IPTV-HLS","GetContentsURL: " + playInfo.GetContentsURL());
                hls_player_initialize();
                if(!TextUtils.isEmpty(playInfo.GetLicenseURL())){
                    SLog.d("IPTV-HLS","setDrmLicenseUrl: " + playInfo.GetLicenseURL());
                    mHLSPlayer.setDataSource(playInfo.GetContentsURL(), playInfo.GetLicenseURL());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (onErrorListener != null)
                onErrorListener.onError(this, 0, 0);
        }
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
    }

    private IptvTuneInfo getTuneInfo(Uri uri) throws Exception {
        IptvTuneInfo info = new IptvTuneInfo();

        info.SetChNum(uri.getQueryParameter("ch") == null ? 0 : Integer.decode(uri.getQueryParameter("ch")));
        info.SetURL(uri.getHost());
        info.SetPort(uri.getPort());
        info.SetVideoPID(uri.getQueryParameter("vp") == null ? 0 : Integer.decode(uri.getQueryParameter("vp")));
        info.SetAudioPID(uri.getQueryParameter("ap") == null ? 0 : Integer.decode(uri.getQueryParameter("ap")));
        info.SetAudioPID1(uri.getQueryParameter("ap1") == null ? 0 : Integer.decode(uri.getQueryParameter("ap1")));
        info.SetPCRPID(uri.getQueryParameter("pp") == null ? 0 : Integer.decode(uri.getQueryParameter("pp")));
        info.SetCAPID(uri.getQueryParameter("cp") == null ? 0 : Integer.decode(uri.getQueryParameter("cp")));
        info.SetCASystemID(uri.getQueryParameter("ci") == null ? 0 : Integer.decode(uri.getQueryParameter("ci")));
        info.SetVideoStream(uri.getQueryParameter("vc") == null ? 0 : Integer.decode(uri.getQueryParameter("vc")));
        info.SetAudioStream(uri.getQueryParameter("ac") == null ? 0 : Integer.decode(uri.getQueryParameter("ac")));
        info.SetAudioStream1(uri.getQueryParameter("ac1") == null ? 0 : Integer.decode(uri.getQueryParameter("ac1")));
        info.setLastFrame(uri.getQueryParameter("lf") == null ? 0 : Integer.decode(uri.getQueryParameter("lf")));
        info.setAuioEnable(uri.getQueryParameter("ae") == null ? 0 : Integer.decode(uri.getQueryParameter("ae")));
        info.setPip(uri.getQueryParameter("p"));
        info.SetResolution(IptvTuneInfo.IPTV_TUNEINFO_RES_HD);

        SLog.d("IPTV", "getTuneInfo " + info.GetURL());

        return info;
    }

    private IptvPlayInfo getVodPlayInfo(Uri uri) throws Exception {
        IptvPlayInfo info = new IptvPlayInfo();
        info.setUri(uri);

        info.SetContentID(uri.getQueryParameter("ci") == null ? "" : uri.getQueryParameter("ci"));
        info.SetOTPID(uri.getQueryParameter("oi") == null ? "" : uri.getQueryParameter("oi")); // one time password
        info.SetOTPPasswd(uri.getQueryParameter("op") == null ? "" : uri.getQueryParameter("op")); // one time password
        info.SetPath("rtsp://" + uri.getHost() + (uri.getPort() == 0 ? "" : ":" + uri.getPort()) + uri.getPath());
        info.SetResumeSec(uri.getQueryParameter("rp") == null ? 0 : Integer.decode(uri.getQueryParameter("rp")));
        info.setLastFrame(uri.getQueryParameter("lf") == null ? 0 : Integer.decode(uri.getQueryParameter("lf")));
        info.SetPlayMode(IptvPlayInfo.IPTV_MODE_VOD);
        info.SetPlayType(IptvPlayInfo.IPTV_PLAY_TYPE_NORMAL);

        SLog.d("IPTV", "getVodPlayInfo " + info.getUri());

        return info;
    }

    private IptvPlayInfo getFilePlayInfo(Uri uri) throws Exception {
        IptvPlayInfo info = new IptvPlayInfo();
        info.setUri(uri);

        info.SetContentID(uri.getQueryParameter("ci") == null ? "" : uri.getQueryParameter("ci"));
        info.SetOTPID(uri.getQueryParameter("oi") == null ? "" : uri.getQueryParameter("oi")); // one time password
        info.SetOTPPasswd(uri.getQueryParameter("op") == null ? "" : uri.getQueryParameter("op")); // one time password
        info.SetPath(uri.getPath());
        info.SetResumeSec(uri.getQueryParameter("rp") == null ? 0 : Integer.decode(uri.getQueryParameter("rp")));
        info.setLastFrame(uri.getQueryParameter("lf") == null ? 0 : Integer.decode(uri.getQueryParameter("lf")));
        info.SetPlayMode(IptvPlayInfo.IPTV_MODE_FILE);
        info.SetPlayType(IptvPlayInfo.IPTV_PLAY_TYPE_ADV);

        SLog.d("IPTV", "getFilePlayInfo " + info.getUri());

        return info;
    }

    private IptvPlayInfo getHLSPlayInfo(Uri uri) throws Exception {
        IptvPlayInfo info = new IptvPlayInfo();
        info.setUri(uri);

        String strUri = uri.toString();
        if (strUri.contains("contents=")) {
            int endPos = strUri.length();
            if (strUri.contains("&license=")) {
                endPos = strUri.indexOf("&license=");
                info.SetLicenseURL(strUri.substring(strUri.indexOf("&license=") + 9, strUri.length()));
            }
            String contentsUri = strUri.substring(strUri.indexOf("contents=") + 9, endPos);
            info.SetContentsURL(contentsUri);
            //URI contextUri = new URI(strUri.substring(strUri.indexOf("contents=") + 9, endPos));
            //info.SetContentsURL(contextUri.getScheme()+"://" + contextUri.getAuthority() + contextUri.getRawPath());
        }else{
            int endPos = strUri.length();
            URI contextUri = new URI(strUri.substring(strUri.indexOf("skb") + 3, endPos));
            info.SetContentsURL(contextUri.getScheme()+"://" + contextUri.getAuthority() + contextUri.getRawPath());
        }

        info.SetContentID(uri.getQueryParameter("ci") == null ? "" : uri.getQueryParameter("ci"));
        info.SetOTPID(uri.getQueryParameter("oi") == null ? "" : uri.getQueryParameter("oi")); // one time password
        info.SetOTPPasswd(uri.getQueryParameter("op") == null ? "" : uri.getQueryParameter("op")); // one time password
        info.SetPath(uri.getPath());
        info.SetResumeSec(uri.getQueryParameter("rp") == null ? 0 : Integer.decode(uri.getQueryParameter("rp")));
        info.setLastFrame(uri.getQueryParameter("lf") == null ? 0 : Integer.decode(uri.getQueryParameter("lf")));

        info.SetPlayMode(IptvPlayInfo.IPTV_MODE_HLS);
        info.SetPlayType(IptvPlayInfo.IPTV_PLAY_TYPE_NORMAL);
        //SLog.d("IPTV", "getHLSPlayInfo " + info.getUri());

        return info;
    }

    private ArrayList<IptvInfo> getMVTuneInfo(Uri uris) throws Exception {
        if (uris == null) {
            return null;
        }

        ArrayList<IptvInfo> tuneInfos = new ArrayList<>();
        ArrayList<Uri> uriList = new ArrayList<>();
        //FIXME Parse List
        //
        String strUri = uris.toString();
        if (strUri.contains("view1=")) {
            int endPos = strUri.length();
            if (strUri.contains("&view2=")) {
                endPos = strUri.indexOf("&view2=");
            }
            uriList.add(Uri.parse(strUri.substring(strUri.indexOf("view1=") + 6, endPos)));
        }
        if (strUri.contains("view2=")) {
            int endPos = strUri.length();
            if (strUri.contains("&view3=")) {
                endPos = strUri.indexOf("&view3=");
            }
            uriList.add(Uri.parse(strUri.substring(strUri.indexOf("view2=") + 6, endPos)));
        }
        if (strUri.contains("view3=")) {
            int endPos = strUri.length();
            if (strUri.contains("&viewmode=")) {
                endPos = strUri.indexOf("&viewmode=");
            }
            uriList.add(Uri.parse(strUri.substring(strUri.indexOf("view3=") + 6, endPos)));
        }
        for (Uri uri : uriList) {
            IptvInfo iptvInfo = null;
            if(uri.getScheme().contains("skbiptv")) {
                iptvInfo = getTuneInfo(uri);
            } else if(uri.getScheme().contains("skbfile")) {
                iptvInfo = getFilePlayInfo(uri);
            } else  if(uri.getScheme().contains("skbvod")) {
                iptvInfo = getVodPlayInfo(uri);
            }
            if (iptvInfo != null) {
                tuneInfos.add(iptvInfo);
            }
        }
        if(strUri.contains("viewmode=")) {
            mvMode = Integer.parseInt(strUri.substring(strUri.indexOf("viewmode=") + 9, strUri.length()));
        }

        return tuneInfos;
    }

    public void prepare() {
        SLog.d("IPTV",  "IN|" + "nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            if (playInfo != null) {
                //SLog.d("IPTV", "prepare " + playInfo.getUri());
                if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                    try {
                        mHLSPlayer.prepare();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    if (isConnected) legacyPlayer.open(playInfo);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
    }

    public void prepareAsync() {
        SLog.d("IPTV",  "IN|player = " + MediaPlayer.this);
        Thread prepareThread = new Thread(new Runnable(){
            @Override
            public void run() {
                if (onPreparedListener != null) {
                    try {
                        if (playInfo != null) {
                            SLog.d("IPTV", "IN|" + "prepareAsync " + playInfo.getUri() + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
                            if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS) {
                                try {
                                    mHLSPlayer.prepare();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                if (isConnected) legacyPlayer.open(playInfo);
                            }
                        }
                        SLog.d("IPTV",  "onPrepared " + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
                        onPreparedListener.onPrepared(MediaPlayer.this);
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        prepareThread.setPriority(Thread.MAX_PRIORITY);
        prepareThread.start();
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
    }

    //Starts or resumes playback.
    public void start() {
        SLog.d("IPTV",  "IN|start" + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);

        duration = 0;
        currntTime = 0;
        bShowFirstFrame = false;
        retryTimeOffset = System.currentTimeMillis();

        try {
            if (tuneInfo != null) {
                SLog.d("IPTV", "Request tuneTV");
                if(isConnected) {
                    legacyPlayer.tuneTV(tuneInfo, playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? IptvMediaPlayer.IPTV_AUDIO_ON : IptvMediaPlayer.IPTV_AUDIO_OFF);
                }
            } else if (mvTuneInfo != null) {
                SLog.d("IPTV", "Request MultiView tuneTV");
                if(isConnected) {
                    legacyPlayer.tuneMultiViewTV(mvTuneInfo, mLastMVAudioOnOff ? IptvMediaPlayer.IPTV_AUDIO_ON : IptvMediaPlayer.IPTV_AUDIO_OFF, mvMode);
                }
            } else if (playInfo != null) {
                if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                    if (!isPaused){
                        SLog.d("IPTV", "Request HLS Play");
                        try {
                            mHLSPlayer.start(playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN? true:false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        SLog.d("IPTV", "Request HLS resume");
                        try {
                            mHLSPlayer.resume();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }else{
                    if (!isPaused) {
                        SLog.d("IPTV", "Request play");
                        if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_VOD) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    InetAddress address = null;
                                    try {
                                        address = InetAddress.getByName(new URL("http://" + playInfo.getUri().getHost()).getHost());
                                        final byte[] ipAddress = address.getAddress();

                                        mHandler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                int result = 0;
                                                for (byte b : ipAddress) {
                                                    result = result << 8 | (b & 0xFF);
                                                }

                                                if(onInfoListener != null) {
                                                    onInfoListener.onInfo(MediaPlayer.this, 10001, result);
                                                    onInfoListener.onInfo(MediaPlayer.this, 10002, playInfo.getUri().getPort());
                                                }
                                            }
                                        });
                                    } catch (UnknownHostException e) {
                                        e.printStackTrace();
                                    } catch (MalformedURLException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();
                        }
                        SLog.d("IPTV", "play before");
                        if(isConnected) legacyPlayer.play();
                        SLog.d("IPTV", "play done");
                    } else {
                        SLog.d("IPTV", "Request resume");
                        if(isConnected) legacyPlayer.resume();
                    }
                }
            }else{
                SLog.d("IPTV", "playinfo is empty");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        SLog.d("IPTV",  "OUT|start done player = " + MediaPlayer.this);
        isPlaying = true;
        isPaused = false;
    }

    //Checks whether the MediaPlayer is playing.
    public boolean isPlaying() {
        SLog.d("IPTV",  "IN|" + "nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
/*
        if(playInfo != null && mLooper != null) {
            if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS) {
                return hlsPlayer.isPlaying();
            }
        }

 */
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
        return isPlaying && !isPaused;
    }

    //Seeks to specified time position.
    public void seekTo(long msec) {
        SLog.d("IPTV",  "IN|seekTo:" + msec + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            currntTime = msec;
            if(playInfo != null && playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                try {
                    mHLSPlayer.seekTo(msec);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                if(isConnected) legacyPlayer.seek(msec);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
    }

    //Pauses playback.
    public void pause() {
        try {
            SLog.d("IPTV",  "IN|" + "nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
            if (playInfo != null && playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                try {
                    mHLSPlayer.pause();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                if(isConnected) legacyPlayer.pause();
            }
            isPaused = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Gets the duration of the file.
    public int getDuration() {
        SLog.d("IPTV",  "IN|" + "nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        if (playInfo != null && playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
            try {
                SLog.d("IPTV",  "OUT|duration = " + (int) mHLSPlayer.getDuration() + ", player = " + MediaPlayer.this);
                return (int) mHLSPlayer.getDuration();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        SLog.i("IPTV", "Get Duration from " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP") + " " + duration);
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
        return (int) duration;
    }

    //Gets the current playback position.
    public int getCurrentPosition() {
        SLog.d("IPTV",  "IN|" + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        //SLog.i("IPTV", "Get Current Position from " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP") + " " + currntTime);
        //return (int) currntTime;
        if (playInfo != null && playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
            try {
                SLog.d("IPTV",  "OUT|Current Position = " + (int) mHLSPlayer.getCurrentPosition() + ", player = " + MediaPlayer.this);
                return (int) mHLSPlayer.getCurrentPosition();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(!isConnected) return 0;

        Parcel request = newRequest();
        Parcel reply = Parcel.obtain();
        request.writeInt(1105);
        invoke(request, reply);
        long currentPts = reply.readLong() * 2 / 90;
        SLog.i("IPTV", "Get Current Position from " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP") + " " + currentPts);
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
        return (int) currentPts;
    }

    //Resets the MediaPlayer to its uninitialized state.
    public void reset() {
        SLog.d("IPTV",  "IN|" + "nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            if (tuneInfo != null) {
                SLog.d("IPTV", "closeTV");

                if(isConnected) {
                    if(playerId != IptvMediaPlayer.IPTV_DEVICE_MAIN){
                        try {
                            Thread.sleep(5);
                        }catch (InterruptedException e){
                            e.printStackTrace();
                        }
                    }
                    legacyPlayer.closeTV();
                }
                tuneInfo = null;
            }

            if (playInfo != null) {
                if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                    SLog.d("IPTV-HLS", "close");
                    hls_player_release();
                }else{
                    SLog.d("IPTV", "close");
                    if(isConnected) {
                        if(playerId != IptvMediaPlayer.IPTV_DEVICE_MAIN){
                            try {
                                Thread.sleep(5);
                            }catch (InterruptedException e){
                                e.printStackTrace();
                            }
                        }
                        legacyPlayer.close();
                    }
                }
                playInfo = null;
            }

            if (mvTuneInfo != null) {
                SLog.d("MutiView IPTV", "close");
                if(isConnected) legacyPlayer.closeTV();
                mvTuneInfo = null;
            }

            isPlaying = false;
            isPaused = false;

        } catch (IOException e) {
            e.printStackTrace();
        }

        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
    }

    //Releases resources associated with this MediaPlayer object.
    public void release() {
        SLog.d("IPTV",  "IN|" + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            if (tuneInfo != null) {
                SLog.d("IPTV", "closeTV");
                if(isConnected) legacyPlayer.closeTV();
                tuneInfo = null;
            }

            if (playInfo != null) {
                if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                    SLog.d("IPTV-HLS", "close");
                    hls_player_release();
                }else{
                    SLog.d("IPTV", "close");
                    if(isConnected) legacyPlayer.close();
                }
                playInfo = null;
            }

            if (mvTuneInfo != null) {
                SLog.d("MultiView IPTV", "close");
                if(isConnected) legacyPlayer.closeTV();
                mvTuneInfo = null;
            }

            isPlaying = false;
            isPaused = false;

            legacyPlayer.setOnTuneEventListener(null);
            legacyPlayer.setOnDisconnectedListener(null);
            legacyPlayer.setOnPlayStatusListener(null);
            legacyPlayer.setOnSectionFilterListener(null);
            legacyPlayer.setOnPidChangedListener(null);
            legacyPlayer.setOnSeekCompletionListener(null);
            legacyPlayer.disconnect();

        } catch (IOException e) {
            e.printStackTrace();
        }

//        unbindHLSPlayerConnection();

        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);

        if(mContext != null && mSleepBroadcastReceiver != null) {
            mContext.unregisterReceiver(mSleepBroadcastReceiver);
            mSleepBroadcastReceiver = null;
        }
    }

//    //Returns an array of track information.
    public TrackInfo[] getTrackInfo () {
        SLog.d("IPTV",  "IN|" + "nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        TrackInfo[] trackInfos = null;

        if(mvTuneInfo != null) {
            trackInfos = new TrackInfo[mvTuneInfo.size()];

            for(int i = 0; i < mvTuneInfo.size(); i++) {
                trackInfos[i] = new TrackInfo();
                trackInfos[i].trackType = TrackInfo.MEDIA_TRACK_TYPE_AUDIO;
            }
        }
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
        return trackInfos;
    }

    //Selects a track. 멀티뷰의 각 채널별 audio focus를 설정할 때 사용하는 API
    public void selectTrack(int index) {
        SLog.d("IPTV",  "IN|selectTrack = " + index + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        if(mvTuneInfo != null && index < mvTuneInfo.size()) {
            try {
                if(mvTuneInfo.get(index) instanceof IptvTuneInfo) {
                    IptvTuneInfo iptvTuneInfo = (IptvTuneInfo) mvTuneInfo.get(index);
                    if(isConnected) legacyPlayer.changeAudioChannel(iptvTuneInfo.GetAudioPID(), iptvTuneInfo.GetAudioStream());
                }
             } catch (IOException e) {
                e.printStackTrace();
            }
        }
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
    }

    //별도로 구현된 API이며 멀티뷰의 경우 2개 player (주/부)를 사용하므로 각각의 볼륨을 제어할 수 있는 API임
    public void setVolume(float volume) {
        SLog.d("IPTV",  "IN|setVolume = " + volume + "," + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        mute(volume == 0);
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
    }

    public Parcel newRequest() {
        return legacyPlayer.newRequest();
    }

    public void invoke(Parcel request, Parcel reply) {
        if(isConnected) legacyPlayer.invoke(request, reply);
    }

    public void mute(boolean isMute) {
        SLog.d("IPTV",  "IN|mute = " + isMute + "," + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            if (playInfo != null && playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                try {
                    mHLSPlayer.muteAudio(isMute);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if(isConnected) legacyPlayer.changeAudioOnOff(isMute ? IptvMediaPlayer.IPTV_AUDIO_OFF : IptvMediaPlayer.IPTV_AUDIO_ON);
            mLastMVAudioOnOff = isMute;

        } catch (IOException e) {
            e.printStackTrace();
        }
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
    }

    public int trick(TRICK_SPEED eTrick) {
        SLog.d("IPTV",  "IN|trick = " + (float)eTrick.getSpeedRatio() + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            if (playInfo != null && playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                try {
                    mHLSPlayer.playTrick((float)eTrick.getSpeedRatio());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                if(isConnected) return legacyPlayer.trick(eTrick.getSpeedId());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
        return -1;
    }

    public boolean isDualAudio() {
        if (tuneInfo != null) {
            return tuneInfo.isDualChannelAudio();
        }
        return false;
    }

    public boolean isMainAudio() {
        if (tuneInfo != null) {
            return tuneInfo.isMainAudio();
        }
        return true;
    }

    public void switchAudio() {
        if (tuneInfo != null) {
            try {
                if (tuneInfo.isDualChannelAudio()) {
                    if (tuneInfo.isMainAudio()) {
                        if(isConnected) legacyPlayer.changeAudioChannel(tuneInfo.GetAudioPID1(), tuneInfo.GetAudioStream1());

                    } else {
                        if(isConnected) legacyPlayer.changeAudioChannel(tuneInfo.GetAudioPID(), tuneInfo.GetAudioStream());
                    }

                    tuneInfo.setMainAudio(!tuneInfo.isMainAudio());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void changeAudio(int index) {
        if (tuneInfo != null) {
            try {
                if (tuneInfo.isDualChannelAudio()) {
                    if (index != 0) {
                        if(isConnected) legacyPlayer.changeAudioChannel(tuneInfo.GetAudioPID1(), tuneInfo.GetAudioStream1());

                    } else {
                        if(isConnected) legacyPlayer.changeAudioChannel(tuneInfo.GetAudioPID(), tuneInfo.GetAudioStream());
                    }

                    tuneInfo.setMainAudio(!tuneInfo.isMainAudio());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setCCDisplay(TextureView textureView) {
        SLog.d("IPTV",  "IN|textureView = " + textureView + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        ccTextureView = textureView;
        legacyPlayer.setCCDisplay(ccTextureView);
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
    }

    public int setWindowSize(int nLeft, int nTop, int nWidth, int nHeight) {
        try {
            if(isConnected) legacyPlayer.setWindowSize(nLeft, nTop, nWidth, nHeight);
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    protected void startDmxFilter(DmxFilter filter)
    {
        SLog.d("IPTV",  "IN|pid = " + filter.pid + ", tid = " + filter.tid + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        legacyPlayer.startDmxFilter(filter);
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
    }

    protected void stopDmxFilter(DmxFilter filter){
        SLog.d("IPTV",  "IN|pid = " + filter.pid + ", tid = " + filter.tid + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        legacyPlayer.stopDmxFilter(filter);
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
    }

    protected ArrayList<String> getLanguageList(int index) {
        SLog.d("IPTV",  "IN|index = " + index + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            if (mHLSPlayer !=null){
                return (ArrayList<String>)mHLSPlayer.getLanguageList(index);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
        return null;
    }

    protected void setTrackLanguage(int index, String language){
        SLog.d("IPTV",  "IN|index = " + index + ", language = " + language + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            if (mHLSPlayer !=null){
                mHLSPlayer.setTrackLanguage(index, language);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
    }

    protected void setTrackDisable(int index, boolean flag){
        SLog.d("IPTV",  "IN|index = " + index + ", flag = " + flag + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            if (mHLSPlayer !=null){
                mHLSPlayer.setTrackDisable(index, flag);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
    }

    protected void adjustSubtitleOffset(long offset){
        SLog.d("IPTV",  "IN|offset = " + offset + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            if (mHLSPlayer !=null){
                mHLSPlayer.adjustSubtitleOffset(offset);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
    }

    protected void setLopper(Looper looper) {
        SLog.d("IPTV",  "IN|Looper = " + looper + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        SLog.d("IPTV",  "OUT|player = " + MediaPlayer.this);
    }
}
