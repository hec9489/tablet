package com.skb.btv.framework.media.core;

import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.TextureView;
import android.view.View;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;

import android.graphics.Rect;

import com.skb.btv.framework.media.SignalEvent;
import com.skb.btv.framework.media.DmxFilter;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;


/**
 * 
 * @author musespy
 *
 */

class IptvMediaPlayer
{
    static 
    {
        System.loadLibrary("btf_media_jni.btf");
        native_init();
    }
    
    private final static String TAG = "IptvMediaPlayer";
    
    // Name of the remote interface for the IPTV media player. Must be kept
    // in sync with the 2nd parameter of the IMPLEMENT_META_INTERFACE
    // macro invocation in IIPTVMediaPlayer.cpp
    private final static String IIPTV_MEDIA_PLAYER = "com.skb.btv.framework.media.IIPTVMediaPlayer";
    
    // IptvMediaPlayer Error
    public final static int     IPTV_ERR_SUCCESS		= 0;
    public final static int     IPTV_ERR_ERROR			= -1;
    public final static int     IPTV_ERR_BADFILENAME	= -2;
    public final static int     IPTV_ERR_INPROGRESS		= -3;
    public final static int     IPTV_ERR_INUSE			= -4;
    public final static int     IPTV_ERR_INVALID		= -5;
    public final static int     IPTV_ERR_NOTCONN		= -6;
    public final static int     IPTV_ERR_NOTSUP			= -7;
    public final static int     IPTV_ERR_TIMEOUT		= -8;
    public final static int     IPTV_ERR_NOTEXIST		= -9;
    public final static int     IPTV_ERR_INVALIDSTATUS	= -10;
    public final static int     IPTV_ERR_INVALIDFORMAT	= -11;
    public final static int     IPTV_ERR_UNKNOWN		= -12;
        
    // IptvMediaPlayer ID
    public final static int     IPTV_DEVICE_MAIN            = 0;
    public final static int     IPTV_DEVICE_PIP1            = 1;
    public final static int     IPTV_DEVICE_PIP2            = 2;
    public final static int     IPTV_DEVICE_PIP3            = 3;
    
	// IptvMediaPlayer Mode
	public final static int     IPTV_MODE_NONE		= 0;
    public final static int     IPTV_MODE_FILE		= 1;
    public final static int     IPTV_MODE_VOD		= 2;
    public final static int     IPTV_MODE_TUNE		= 3;
    

	// IptvMediaPlayer Status
	public final static int     IPTV_STATUS_NONE		= 0;
	public final static int     IPTV_STATUS_TUNE		= 1;
	public final static int     IPTV_STATUS_PLAY		= 1;
	public final static int     IPTV_STATUS_STOP		= 2;
	public final static int     IPTV_STATUS_PAUSE		= 3;
	public final static int     IPTV_STATUS_2XFF		= 4;
	public final static int     IPTV_STATUS_4XFF		= 5;
	public final static int     IPTV_STATUS_8XFF		= 6;
	public final static int     IPTV_STATUS_16XFF		= 7;
	public final static int     IPTV_STATUS_2XBW		= 8;
	public final static int     IPTV_STATUS_4XBW		= 9;
	public final static int     IPTV_STATUS_8XBW		= 10;
	public final static int     IPTV_STATUS_16XBW		= 11;

	// IptvMediaPlayer Status
    public final static int     IPTV_VOD_TRICK_1XFF		= 0;
    public final static int     IPTV_VOD_TRICK_0_8XFF		= 1;
    public final static int     IPTV_VOD_TRICK_1_2XFF		= 2;
    public final static int     IPTV_VOD_TRICK_1_5XFF		= 3;
	public final static int     IPTV_VOD_TRICK_2XFF		= 4;
	public final static int     IPTV_VOD_TRICK_4XFF		= 5;
	public final static int     IPTV_VOD_TRICK_8XFF		= 6;
	public final static int     IPTV_VOD_TRICK_16XFF	= 7;
	public final static int     IPTV_VOD_TRICK_2XBW		= 8;
	public final static int     IPTV_VOD_TRICK_4XBW		= 9;
	public final static int     IPTV_VOD_TRICK_8XBW		= 10;
	public final static int     IPTV_VOD_TRICK_16XBW	= 11;

	// IptvMediaPlayer audio 
	public final static int     IPTV_AUDIO_OFF			= 0;
	public final static int     IPTV_AUDIO_ON			= 1;

	// IptvMediaPlayer Disconnect
	public final static int     IPTV_DISCONNECT_BY_SERVICE_CRASHED	= 0;
	public final static int     IPTV_DISCONNECT_BY_OTHER_OPEN		= 1;

    private int mNativeContext;
    private int mListener;
    private int mDeviceId;
    private int mNativeSurfaceTexture;
    private int mNativeHandle;
    private int mDsmccListener;
    private SurfaceHolder mSurfaceHolder;

    private int mReqeustTrick;
    private int mCurrentTrick;
    private boolean isTrickOnExecute = false;

    private long mReqeustSeek;
    private long mCurrentSeek;
    private boolean isSeekOnExecute = false;

    private EventHandler mEventHandler;

    private CloseCaption closeCaption;
    
    // native method defines
    private native final void native_setup(Object iptv_mediaplayer_this, int deviceID);
    private native final void native_release();
    private static native final void native_init();
    
    /*
     * @param request Parcel destinated to the iptv meida player. The
     * 				  Interface token must be set to the IIptvMediaPlayer
     * 				  one to be routed correctly through the system.
     * @param reply[out] Parcel that will contain the reply.
     * @return The status code.
     */
    private native final int native_invoke(Parcel request, Parcel reply);
    
	private int mX = 0, mY = 0, mWidth = 0, mHeight = 0;

    public static IptvMediaPlayer connect(int deviceID)
    {
        return new IptvMediaPlayer(deviceID);
    }

    public static IptvMediaPlayer connect()
    {
        return new IptvMediaPlayer(IPTV_DEVICE_MAIN);
    }

	public void disconnect()
    {
        Log.v(TAG, "disconnect");
        if(closeCaption != null) {
            closeCaption.release();
            closeCaption = null;
        }
        native_release();
    }


    /**
     * constructor.
     */
    IptvMediaPlayer(int playerId)
    {
    	Looper looper;
    	if ((looper = Looper.myLooper()) != null)
        {
            mEventHandler = new EventHandler(this, looper);
        }
    	else if ((looper = Looper.getMainLooper()) != null)
    	{
    		mEventHandler = new EventHandler(this, looper);
    	}
    	else
    	{
    		mEventHandler = null;
    	}

        mDeviceId = playerId;
        isTrickOnExecute = false;
        isSeekOnExecute = false;

    	/*
    	 * Native setup requires a weak reference to our object.
    	 * It's easier to create it here than in C++.
    	 */
    	native_setup(new WeakReference<IptvMediaPlayer>(this), playerId);
    }
 
    protected void finalize() {
        Log.v(TAG, "finalize");
//        native_release();
    }
   
    public Parcel newRequest()
    {
    	Parcel parcel = Parcel.obtain();
    	//parcel.writeInterfaceToken(IIPTV_MEDIA_PLAYER);
    	return parcel;
    }
    
    public void invoke(Parcel request, Parcel reply)
    {
    	int retcode = native_invoke(request, reply);
        if(isSeekOnExecute){
            reply.setDataPosition(0);
            reply.writeLong(((mReqeustSeek) * 90) / 2);
            Log.v(TAG, "invoke replace pts=" + ((mReqeustSeek) * 90) / 2);
        }
    	reply.setDataPosition(0);
    	if (retcode != 0)
    	{
    		throw new RuntimeException("failure code: " + retcode);
    	}
    }
    
    private native int _vodOpen(String dataXML)
    	throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    
    public int vodOpen(String dataXML)
    	throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {
    	return _vodOpen(dataXML);
    }





///////////////////////////////////////////////////////////////////////////////////////
//  control api
    private native int _control(String strCommand, int arg0, String arg1);

	public int control(String strCommand, int arg0, String arg1)
	{
		return _control(strCommand, arg0, arg1);
	}

///////////////////////////////////////////////////////////////////////////////////////
//setPlayerSize api
	private native int _setPlayerSize(int nLeft, int nTop, int nWidth, int nHeight)
	        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

	public int setPlayerSize(View playerView)
	        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
	{
		int left=0, top=0, width=0, height=0;
		int [] location = new int[2];
		playerView.getLocationOnScreen(location);
		left = location[0];
		top = location[1];
		width = playerView.getWidth();
		height = playerView.getHeight();

	    //custom Log
	    Log.v("PlayerSize", "left=" + left + " top=" + top + " width=" + width + " height=" + height);
	    
	    return _setPlayerSize(left, top, width, height);
	}

///////////////////////////////////////////////////////////////////////////////////////
//  setWindowSize api
    private native int _setWindowSize(String dataXML)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    public int setWindowSize(int nLeft, int nTop, int nWidth, int nHeight)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {
		String dataXML;
/*		LayoutParams params;
					
		if (mGLIPTVSurfaceView == null)
		{
			return -1;
		}
		params = mGLIPTVSurfaceView.getLayoutParams();

		mGLIPTVSurfaceView.setX(nLeft);
		mGLIPTVSurfaceView.setY(nTop);
		params.height = nHeight;
		params.width = nWidth;
	  
		mGLIPTVSurfaceView.setLayoutParams(params);
*/		
		dataXML = String.format("%d;%d;%d;%d;", nLeft, nTop, nWidth, nHeight);

/*		mX = nLeft;
		mY = nTop;
		mWidth = nWidth;
		mHeight = nHeight;

		mGLIPTVSurfaceView.setVisibility(View.GONE);
*/
        return _setWindowSize(dataXML);
	}




///////////////////////////////////////////////////////////////////////////////////////
//  getWindowSize api
    public Rect getWindowSize()
    {
		Rect rWindowRect = new Rect(mX, mY, mX + mWidth, mY + mHeight);	
		return rWindowRect;
	}

///////////////////////////////////////////////////////////////////////////////////////
//  getCurrentPostion api
    private native int _getCurrentPositionInSec();

    public int getCurrentPositionInSec()
    {
		return _getCurrentPositionInSec();
	}


///////////////////////////////////////////////////////////////////////////////////////
//  tunerTuneTV api
    private native int _tuneTV(String dataXML)
        throws Exception; //IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    public int tuneTV(IptvTuneInfo tuneInfo, int nAudioOnOff)
        throws Exception //IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {
		String dataXML;

		if (nAudioOnOff < IPTV_AUDIO_OFF || nAudioOnOff > IPTV_AUDIO_ON)
			return IPTV_ERR_INVALID;

		int defaultAudioOnOff = nAudioOnOff;

		if(tuneInfo.getIsPip()) defaultAudioOnOff = tuneInfo.getAuioEnable();
		
		dataXML = String.format("%s;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;",
                tuneInfo.GetURL(),
                tuneInfo.GetPort(),
                tuneInfo.GetVideoPID(),
                tuneInfo.GetAudioPID(),
                tuneInfo.GetPCRPID(),
                tuneInfo.GetVideoStream(),
                tuneInfo.GetAudioStream(),
                tuneInfo.GetCAPID(),
                tuneInfo.GetCASystemID(),
                tuneInfo.GetChNum(),
                tuneInfo.GetResolution(),
                defaultAudioOnOff,
                tuneInfo.getLastFrame()
		);
		

	/*	if (mGLIPTVSurfaceView != null)
		{
			mGLIPTVSurfaceView.setVisibility(View.VISIBLE);
		}
	*/	

        return _tuneTV(dataXML);
	}

    public int tuneMultiViewTV(ArrayList<IptvInfo> tuneInfos, int nAudioOnOff, int viewMode)
            throws Exception //IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {
        if(tuneInfos == null || tuneInfos.size() == 0) {
            return -1;
        }

        String dataXML = "";

        if (nAudioOnOff < IPTV_AUDIO_OFF || nAudioOnOff > IPTV_AUDIO_ON)
            return IPTV_ERR_INVALID;

        for (IptvInfo info : tuneInfos) {
            if(info instanceof IptvTuneInfo) {
                IptvTuneInfo iptvTuneInfo = (IptvTuneInfo)info;
                dataXML += String.format("%s;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;\n",
                        iptvTuneInfo.GetURL(),
                        iptvTuneInfo.GetPort(),
                        iptvTuneInfo.GetVideoPID(),
                        iptvTuneInfo.GetAudioPID(),
                        iptvTuneInfo.GetPCRPID(),
                        iptvTuneInfo.GetVideoStream(),
                        iptvTuneInfo.GetAudioStream(),
                        iptvTuneInfo.GetCAPID(),
                        iptvTuneInfo.GetCASystemID(),
                        iptvTuneInfo.GetChNum(),
                        iptvTuneInfo.GetResolution(),
                        nAudioOnOff,
                        iptvTuneInfo.getLastFrame(),
                        viewMode
                );
            } else if(info instanceof IptvPlayInfo) {
                IptvPlayInfo playInfo = (IptvPlayInfo)info;
                dataXML = String.format("%d;%d;%d;%d;%d;%s;%s;%s;%s;%s;%d;",
                        playInfo.GetPlayMode(),
                        playInfo.GetPlayType(),
                        playInfo.getDRMType(),
                        playInfo.GetDurationSec(),
                        playInfo.GetResumeSec(),
                        playInfo.GetPath(),
                        playInfo.GetMetaInfo(),
                        playInfo.GetContentID(),
                        playInfo.GetOTPID(),
                        playInfo.GetOTPPasswd(),
                        playInfo.getLastFrame()
                );
            }
        }


	/*	if (mGLIPTVSurfaceView != null)
		{
			mGLIPTVSurfaceView.setVisibility(View.VISIBLE);
		}
	*/

        return _tuneTV(dataXML);
    }

///////////////////////////////////////////////////////////////////////////////////////
//  tunerCloseTV api
    private native int _closeTV()
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    public int closeTV()
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {	

	//	mGLIPTVSurfaceView.setVisibility(View.GONE);
        return _closeTV();
	}




///////////////////////////////////////////////////////////////////////////////////////
//  changeAudioChannel api
    private native int _changeAudioChannel(String dataXML)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    public int changeAudioChannel(int nAudioPid, int nAudioDecoder)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {
		String dataXML;
		dataXML = String.format("%d;%d;", nAudioPid, nAudioDecoder);
	
        return _changeAudioChannel(dataXML);
	}

///////////////////////////////////////////////////////////////////////////////////////
//_changeAudioOnOff api
    private native int _changeAudioOnOff(String dataXML)
    	throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    public int changeAudioOnOff(int nAudioOnOff)
    	throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {
    	if (nAudioOnOff < IPTV_AUDIO_OFF || nAudioOnOff > IPTV_AUDIO_ON)
			return IPTV_ERR_INVALID;

    	String dataXML;
    	dataXML = String.format("%d;", nAudioOnOff);

    	return _changeAudioOnOff(dataXML);
    }

///////////////////////////////////////////////////////////////////////////////////////
//  bindingFilter api
    private native int _bindingFilter(String dataXML)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    public int bindingFilter(IptvSectionInfo sectionInfo)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {	
    	String dataXML;
    	dataXML = String.format("%d;%d;%d;%d;", sectionInfo.mIndex, sectionInfo.mPID, sectionInfo.mTableID, sectionInfo.mTimeOut);

        return _bindingFilter(dataXML);
	}

///////////////////////////////////////////////////////////////////////////////////////
//  releaseFilter api
    private native int _releaseFilter(String dataXML)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

//    public int releaseFilter(int nIndex)
    public int releaseFilter(int nIndex)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {	
    	String dataXML;
    	dataXML = String.format("%d;", nIndex);

    	return _releaseFilter(dataXML);
	}

	// jung2604
    public native int _filePlay(Surface surface, String path, int vpid, int vcodec, int apid, int acodec)
            throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    public int filePlay(SurfaceHolder sh,  String path, int vpid, int vcodec, int apid, int acodec)
            throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {
        mSurfaceHolder = sh;
        Surface surface;
        if (sh != null)
        {
            surface = sh.getSurface();
        }
        else
        {
            surface = null;
        }


	/*	if (mGLIPTVSurfaceView != null)
		{
			mGLIPTVSurfaceView.setVisibility(View.VISIBLE);
		}
	*/
        return _filePlay(surface, path, vpid, vcodec, apid, acodec);
    }
///////////////////////////////////////////////////////////////////////////////////////
//  open VOD / FILE api
    private native int _open(String dataXML)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    public int open(IptvPlayInfo playInfo)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {
	    String dataXML;
		dataXML = String.format("%d;%d;%d;%d;%d;%s;%s;%s;%s;%s;%d;",
                playInfo.GetPlayMode(),
                playInfo.GetPlayType(),
                playInfo.getDRMType(),
                playInfo.GetDurationSec(),
                playInfo.GetResumeSec(),
                playInfo.GetPath(),
                playInfo.GetMetaInfo(),
                playInfo.GetContentID(),
                playInfo.GetOTPID(),
                playInfo.GetOTPPasswd(),
                playInfo.getLastFrame()
		);
	

	/*	if (mGLIPTVSurfaceView != null)
		{
			mGLIPTVSurfaceView.setVisibility(View.VISIBLE);
		}
	*/	
        return _open(dataXML);
	}

///////////////////////////////////////////////////////////////////////////////////////
//  playVOD api
    private native int _play()
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    public int play()
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {	
		return _play();
	}


///////////////////////////////////////////////////////////////////////////////////////
//  stopVOD api
    private native int _stop()
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    public int stop()
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {	
		return _stop();
	}





///////////////////////////////////////////////////////////////////////////////////////
//  closeVOD api
    private native int _close()
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    public int close()
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {	
		/*if (mGLIPTVSurfaceView != null)
		{
			mGLIPTVSurfaceView.setVisibility(View.GONE);
		}*/

		return _close();
	}





///////////////////////////////////////////////////////////////////////////////////////
//  pauseVOD api
    private native int _pause()
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    public int pause()
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {	
		return _pause();
	}

///////////////////////////////////////////////////////////////////////////////////////
//  resumeVOD api
    private native int _resume()
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    public int resume()
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {	
		return _resume();
	}

///////////////////////////////////////////////////////////////////////////////////////
//  seekVOD api
    private native int _seek(String dataXML, boolean pause)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    public int seek(long mSec)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {
        Log.e(TAG, "seek start :" + mSec);
        if(isSeekOnExecute){
            mReqeustSeek = mSec;
            Log.e(TAG, "OnSeekOnExecute seek :" + mReqeustSeek);
            return 0;
        }
        mReqeustSeek = mSec;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    isSeekOnExecute = true;
                    do {
                        mCurrentSeek = mReqeustSeek;
                        Log.e(TAG, "Request seek :" + mCurrentSeek);
                        String dataXML;
                        dataXML = String.format("%d;", mCurrentSeek);
                        _seek(dataXML, false);
                        Log.e(TAG, "Request seek end" );
                    } while(mCurrentSeek != mReqeustSeek);
                    isSeekOnExecute = false;
                    Log.e(TAG, "Request seek execute end" );
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
/*
		String dataXML;
		dataXML = String.format("%d;", nSec);
	
        return _seek(dataXML);
*/
        return 0;
	}

///////////////////////////////////////////////////////////////////////////////////////
//trickVOD api
    private native int _trick(String dataXML)
    	throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    public int trick(int nTrick)
    	throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {	
      	if (nTrick < IPTV_VOD_TRICK_1XFF || nTrick > IPTV_VOD_TRICK_16XBW)
   			return IPTV_ERR_INVALID;

        Log.e(TAG, "trick start :" + nTrick);
        if(isTrickOnExecute){
            mReqeustTrick = nTrick;
            Log.e(TAG, "OnTrickOnExecute trick :" + mReqeustTrick);
            return 0;
        }
        mReqeustTrick = nTrick;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    isTrickOnExecute = true;
                    do {
                        mCurrentTrick = mReqeustTrick;
                        Log.e(TAG, "Request trick :" + mCurrentTrick);
                        String dataXML;
                        dataXML = String.format("%d;", mCurrentTrick);
                        _trick(dataXML);
                        Log.e(TAG, "Request trick end" );
                    } while(mCurrentTrick != mReqeustTrick);
                    isTrickOnExecute = false;
                    Log.e(TAG, "Request trick execute end" );
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
/*
    	String dataXML;
    	dataXML = String.format("%d;", nTrick);

    	return _trick(dataXML);
*/
        return 0;
    }

///////////////////////////////////////////////////////////////////////////////////////
//  pauseAT VOD api
private native int _pauseAt(String dataXML)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    public int pauseAt(String dataXML)
            throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {
        Log.e(TAG, "pauseAt start :" + dataXML);
        return _pauseAt(dataXML);
    }

///////////////////////////////////////////////////////////////////////////////////////
//  setKeepLastFrame api
private native int _keepLastFrame(boolean flag)
        throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    public int keepLastFrame(boolean flag)
            throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {
        return _keepLastFrame(flag);
    }


///////////////////////////////////////////////////////////////////////////////////////
//  getPlayerMode api
    private native int _getPlayerMode();

    public int getPlayerMode()
    {	
		return _getPlayerMode();
	}


///////////////////////////////////////////////////////////////////////////////////////
//  getplayerStatus api
    private native int _getPlayerStatus();

    public int getPlayerStatus()
    {	
		return _getPlayerStatus();
	}

    public void setCCDisplay(TextureView textureView) {
        if(closeCaption == null) {
            closeCaption = new CloseCaption();
        }
        closeCaption.setCCDisplay(textureView);
    }

///////////////////////////////////////////////////////////////////////////////////////
//  event handler

    private class EventHandler extends Handler
    {
    	private IptvMediaPlayer mIptvMediaPlayer;
    	
    	public EventHandler(IptvMediaPlayer iptv_mp, Looper looper)
    	{
    		super(looper);
    		mIptvMediaPlayer = iptv_mp;
    	}
    	
    	@Override
    	public void handleMessage(Message msg)
    	{	
    		// 1 ~ 3 		: not used
    		// 4			: section filter
    		// 5		    : playstatus
    		// 6			: tuneEvent
    		// 7			: disconnected
    		// 8			: close caption data event
    		// 9			: pid changed event
            // 10			: seek completion event

    		//Log.d(TAG, "what=" + msg.what + "arg1=" + msg.arg1 + "arg2=" + msg.arg2);
    		switch (msg.what)
    		{
            case 1:
				if (msg.arg1 == 0 && msg.arg2 == 0)
				{
					/*if (mGLIPTVSurfaceView == null)
					{
						break;
					}*/

				//	mGLIPTVSurfaceView.setVisibility(View.VISIBLE);

				}
				break;
            case 2:
                break;
            case 3:
                break;
            case 4:
            	Log.d(TAG, "section what=" + msg.what + "arg1=" + msg.arg1 + "arg2=" + msg.arg2);
                if (mOnSectionFilterListener != null)    
                {
                	mOnSectionFilterListener.onSectionFilter(mIptvMediaPlayer, (byte[])msg.obj);
                }
                break;
            case 5:
            	Log.d(TAG, "playstatus what=" + msg.what + "arg1=" + msg.arg1 + "arg2=" + msg.arg2);
                if (mOnPlayStatusListener != null)    
                    mOnPlayStatusListener.onPlayStatus(mIptvMediaPlayer, msg.arg1, new String ((byte[])msg.obj));
                break;
            case 6:
                Log.d(TAG, "TuneEvent what=" + msg.what + "arg1=" + msg.arg1 + "arg2=" + msg.arg2);
                if (mOnTuneEventListener != null)
                    mOnTuneEventListener.onTuneEvent(mIptvMediaPlayer, msg.arg1, msg.arg2);
                break;
            case 7:
                if (mOnDisconnectedListener != null)
                    mOnDisconnectedListener.onDisconnected(mIptvMediaPlayer, msg.arg1, msg.arg2);
                break;
            case 8:
                if(closeCaption != null) {
                    closeCaption.onUpdateCC(msg);
                }
                break;
            case 9:
                Log.d(TAG, "PidChagned what=" + msg.what + "arg1=" + msg.arg1 + "arg2=" + msg.arg2);
                if (mOnPidChangedListener != null)
                    mOnPidChangedListener.onPidChagned(mIptvMediaPlayer, msg.arg2, new String ((byte[])msg.obj));
                break;
            case 10:
                Log.d(TAG, "SeekCompletion what=" + msg.what + "arg1=" + msg.arg1 + "arg2=" + msg.arg2);
                if (mOnSeekCompletionListener != null)
                    mOnSeekCompletionListener.onSeekCompletion(mIptvMediaPlayer);
                break;

    		default:
    			Log.e(TAG, "Unknown message type " + msg.what);
    			return;
    		}
    	}
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    // setDummys api
    private native int _setDummys(String dummys)
            throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;

    public int setDummys(String dummys)
    		throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {
    	return _setDummys(dummys);
    }    

    /*
     * Called from native code when an interesting event happens.  This method
     * just uses the EventHandler system to post the event back to the main app thread.
     * We use a weak reference to the original IptvMediaPlayer object so that the native 
     * code is safe from the object disapperaing from underneath it.  (This is
     * the cookie passed to native_setup().)
     */
    private static void postEventFromNative(Object iptvmediaplayer_ref, 
    										int what, int arg1, int arg2, Object obj)
    {
    	IptvMediaPlayer iptv_mp = (IptvMediaPlayer)((WeakReference)iptvmediaplayer_ref).get();
    	if (iptv_mp == null)
    	{
    		return;
    	}

    	if (iptv_mp.mEventHandler != null)
    	{
    		Message m = iptv_mp.mEventHandler.obtainMessage(what, arg1, arg2, obj);
    		iptv_mp.mEventHandler.sendMessage(m);
    	}
    }

    private static void postEventFromDsmcc(int event, String rootPath, SignalEvent dsmccSignalEvent)
    {
        Log.d(TAG, "postEventFromDsmcc");
        if(mOnDsmccCallbackListener != null) {
            mOnDsmccCallbackListener.onDsmccEventUpdated(rootPath, dsmccSignalEvent);
        }
    }

    // Section filter listener
    public interface OnSectionFilterListener
    {
        void onSectionFilter(IptvMediaPlayer iptv_mp, byte[] sectionData);
    }

    public void setOnSectionFilterListener(OnSectionFilterListener listener)
    {
    	mOnSectionFilterListener = listener;
    }

    private OnSectionFilterListener mOnSectionFilterListener;

    
    // Play status listener
    public interface OnPlayStatusListener
    {
        void onPlayStatus(IptvMediaPlayer iptv_mp, int nDevice, String playStatus);
    }

    public void setOnPlayStatusListener(OnPlayStatusListener listener)
    {
        mOnPlayStatusListener = listener;
    }

    private OnPlayStatusListener mOnPlayStatusListener;

    // tune listener
    public interface OnTuneEventListener
    {
        void onTuneEvent(IptvMediaPlayer iptv_mp, int nDevice, int nStatus);
    }

    public void setOnTuneEventListener(OnTuneEventListener listener)
    {
        mOnTuneEventListener = listener;
    }

    private OnTuneEventListener mOnTuneEventListener;

    // disconnected listener
    public interface OnDisconnectedListener
    {
        void onDisconnected(IptvMediaPlayer iptv_mp, int nDevice, int nReason);
    }

    public void setOnDisconnectedListener(OnDisconnectedListener listener)
    {
        mOnDisconnectedListener = listener;
    }

    private OnDisconnectedListener mOnDisconnectedListener;

    // onseekcompleted listener
    public interface OnSeekCompletionListener
    {
        void onSeekCompletion(IptvMediaPlayer iptv_mp);
    }

    public void setOnSeekCompletionListener(OnSeekCompletionListener listener)
    {
        mOnSeekCompletionListener = listener;
    }

    private OnSeekCompletionListener mOnSeekCompletionListener;

    // pidchanged listener
    public interface OnPidChangedListener
    {
        void onPidChagned(IptvMediaPlayer iptv_mp, int event, String pid);
    }

    public void setOnPidChangedListener(OnPidChangedListener listener)
    {
        mOnPidChangedListener = listener;
    }

    private OnPidChangedListener mOnPidChangedListener;

    public interface OnDsmccCallbackListener
    {
        void onDsmccEventUpdated(String path, SignalEvent event);
    }

    public void setOnDsmccCallbackListener(OnDsmccCallbackListener listener)
    {
        mOnDsmccCallbackListener = listener;
    }

    static private OnDsmccCallbackListener mOnDsmccCallbackListener;

    private native void _setIptvVideoSurface(Surface surface);
    public void setIptvDisplay(SurfaceHolder sh)
	{
    	mSurfaceHolder = sh;
    	Surface surface;
    	if (sh != null)
		{
    		surface = sh.getSurface();
        }
    	else
        {
    		surface = null;
        }
        if(surface != null) {
            _setIptvVideoSurface(surface);
        }
    }

    private native int _startDmxFilter(int pid, int tid);
    public void startDmxFilter(DmxFilter filter)
    {
        int pid = filter.pid;
        int tid = filter.tid;
        Log.e(TAG, "startDmxFilter pid:" + pid + ",tid:" + tid );
        _startDmxFilter(pid, tid);
    }

    private native int _stopDmxFilter(int pid, int tid);
    public void stopDmxFilter(DmxFilter filter)
    {
        int pid = filter.pid;
        int tid = filter.tid;
        Log.e(TAG, "stopDmxFilter pid:" + pid + ",tid:" + tid );
        _stopDmxFilter(pid, tid);
    }

}
