package com.skb.btv.framework.media;

import android.content.Context;
import android.util.Log;

import com.skb.btv.framework.media.core.MediaPlayer;


/**
 * Created by stragon on 2019-09-27.
 */

public class DvbServicePlayer extends MediaPlayer {

    public interface OnPreparedListener {
        void onPrepared(DvbServicePlayer mp);
    }

    public interface OnInfoListener {
        boolean onInfo(DvbServicePlayer mp, int what, int extra);
    }

    public interface OnErrorListener {
        boolean onError(DvbServicePlayer mp, int what, int extra);
    }

    public interface OnCompletionListener {
        void onCompletion(DvbServicePlayer mp);
    }

    public interface OnDsmccListener    {
        void onDsmccEvent(DvbServicePlayer mp, String path, SignalEvent sigevent);
    }

    public interface OnDmxFilterListener  {
        void onDmxFilter(DvbServicePlayer mp,  byte[] sectiondata);
    }

    private OnPreparedListener onPreparedListener;
    private OnInfoListener onInfoListener;
    private OnErrorListener onErrorListener;
    private OnCompletionListener onCompletionListener;
    private OnDsmccListener onDsmccListener;
    private OnDmxFilterListener  onDmxFilterListener;

    private MediaPlayer.OnPreparedListener onMediaPlayerPreparedListener;
    private MediaPlayer.OnInfoListener onMediaPlayerInfoListener;
    private MediaPlayer.OnErrorListener onMediaPlayerErrorListener;
    private MediaPlayer.OnCompletionListener onMediaPlayerCompletionListener;
    private MediaPlayer.OnDsmccListener onMediaPlayerDsmccListener;
    private MediaPlayer.OnDmxFilterListener  onMediaPlayerDmxFilterListener;

    public DvbServicePlayer(Context context) {
        super(context);
        initialize();
    }

    public DvbServicePlayer(Context context, int pipId) {
        super(context, pipId);
        initialize();
    }

    private void initialize(){
        onMediaPlayerPreparedListener = new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                Log.i("IPTV", "DvbServicePlayer onPrepared");
                if(onPreparedListener != null)  onPreparedListener.onPrepared(DvbServicePlayer.this);
            }
        };

        onMediaPlayerInfoListener = new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                Log.i("IPTV", "DvbServicePlayer onInfo");
                if(onInfoListener != null) onInfoListener.onInfo(DvbServicePlayer.this, what, extra);
                return false;
            }
        };

        onMediaPlayerErrorListener = new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.i("IPTV", "DvbServicePlayer onError");
                if(onErrorListener != null) onErrorListener.onError(DvbServicePlayer.this, what, extra);
                return false;
            }
        };

        onMediaPlayerCompletionListener = new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Log.i("IPTV", "DvbServicePlayer onCompletion");
                if(onCompletionListener != null) onCompletionListener.onCompletion(DvbServicePlayer.this);
            }
        };

        onMediaPlayerDsmccListener = new MediaPlayer.OnDsmccListener() {
            @Override
            public void onDsmccEvent(MediaPlayer mp, String path, SignalEvent sigevent) {
                Log.i("IPTV", "DvbServicePlayer onDsmccEvent");
                if(onDsmccListener != null) onDsmccListener.onDsmccEvent(DvbServicePlayer.this, path, sigevent);
            }
        };

        onMediaPlayerDmxFilterListener = new MediaPlayer.OnDmxFilterListener() {
            @Override
            public void onDmxFilter(MediaPlayer mp, byte[] sectiondata) {
                Log.i("IPTV", "DvbServicePlayer onDmxFilter");
                if(onDmxFilterListener != null) onDmxFilterListener.onDmxFilter(DvbServicePlayer.this, sectiondata);
            }
        };
    }

    public void tune(String strUri){
        super.setDataSource(strUri);
    }

    public void startDmxFilter(DmxFilter filter){
        super.startDmxFilter(filter);
    }

    public void stopDmxFilter(DmxFilter filter){
            super.stopDmxFilter(filter);
    }

    public void setOnDsmccListener(OnDsmccListener listener){
        onDsmccListener = listener;
        if(listener != null)
            super.setOnDsmccListener(onMediaPlayerDsmccListener);
        else
            super.setOnDsmccListener(null);
    }

    public void setDmxFilterListener(OnDmxFilterListener listener){
        onDmxFilterListener = listener;
        if(listener != null)
            super.setDmxFilterListener(onMediaPlayerDmxFilterListener);
        else
            super.setDmxFilterListener(null);
    }

    //Register a callback to be invoked when the media source is ready for playback.
    public void setOnPreparedListener(OnPreparedListener listener) {
        onPreparedListener = listener;
        if(listener != null)
            super.setOnPreparedListener(onMediaPlayerPreparedListener);
        else
            super.setOnPreparedListener(null);
    }

    //Register a callback to be invoked when an info/warning is available.
    public void setOnInfoListener(OnInfoListener listener) {
        onInfoListener = listener;
        if(listener != null)
            super.setOnInfoListener(onMediaPlayerInfoListener);
        else
            super.setOnInfoListener(null);
    }

    //Register a callback to be invoked when an error has happened during an asynchronous operation.
    public void setOnErrorListener(OnErrorListener listener) {
        onErrorListener = listener;
        if(listener != null)
            super.setOnErrorListener(onMediaPlayerErrorListener);
        else
            super.setOnErrorListener(null);
    }

    //Register a callback to be invoked when the end of a media source has been reached during playback.
    public void setOnCompletionListener(OnCompletionListener listener) {
        onCompletionListener = listener;
        if(listener != null)
            super.setOnCompletionListener(onMediaPlayerCompletionListener);
        else
            super.setOnCompletionListener(null);
    }

    public void release() {
        super.setOnCompletionListener(null);
        super.setOnErrorListener(null);
        super.setOnInfoListener(null);
        super.setOnPreparedListener(null);
        super.setDmxFilterListener(null);
        super.setOnDsmccListener(null);
        super.release();

        onMediaPlayerPreparedListener = null;
        onMediaPlayerInfoListener = null;
        onMediaPlayerErrorListener = null;
        onMediaPlayerCompletionListener = null;
        onMediaPlayerDsmccListener = null;
        onMediaPlayerDmxFilterListener = null;
    }
}
