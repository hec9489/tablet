package com.skb.btv.framework.media;

public class DmxFilter {
    public int pid;
    public int tid;

    public DmxFilter(int pid, int tid) {
        this.pid = pid;
        this.tid = tid;
    }
}
