package com.skb.btv.framework.media;

public class WebVTT {
    public long startTime;
    public long endTime;
    public CharSequence text;

    public WebVTT(long startTime, long endTime, CharSequence text) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.text = text;
    }

    public long getStartTime() {
        return this.startTime;
    }

    public long getEndTime() {
        return this.endTime;
    }
    public CharSequence getText() {
        return this.text;
    }
}
