package com.skb.btv.framework.media.core;

import android.net.Uri;

/**
 * 
 * @author musespy
 *
 */

class IptvPlayInfo extends IptvInfo
{
 // IptvMediaPlayer Play type
    public final static int     IPTV_MODE_FILE		= 1;
    public final static int     IPTV_MODE_VOD		= 2;
    public final static int     IPTV_MODE_HLS		= 3;

    public final static int     IPTV_PLAY_TYPE_ADV		= 0;
	public final static int     IPTV_PLAY_TYPE_NORMAL	= 1;

    public final static int     IPTV_DRM_NONE            = 0;
    public final static int     IPTV_DRM_USED		     = 1;

    private int mPlayMode;
    private int mPlayType;
    private int mDRMType;

    private int mDurationSec;
    private int mResumeSec;

    private String mStrPath = "";
    private String mStrMetaInfo = "";
    private String mStrContentID = "";

    private String mStrOTPID = "";
    private String mStrOTPPasswd = "";

    private Uri mUri;
    private int mLastFrame = 0;

    private String mContentsURL = "";
    private String mLicenseURL = "";

    public void SetContentsURL(String strURL)
    {
        mContentsURL = strURL;
    }

    public String GetContentsURL()
    {
        return mContentsURL;
    }

    public void SetLicenseURL(String strURL)
    {
        mLicenseURL = strURL;
    }

    public String GetLicenseURL()
    {
        return mLicenseURL;
    }

    public void SetPath(String strPath)
    {
        mStrPath = strPath;
    }

    public String GetPath()
    {
        return mStrPath;
    }

	public void SetMetaInfo(String strMetaInfo)
    {
        mStrMetaInfo = strMetaInfo;
    }

    public String GetMetaInfo()
    {
        return mStrMetaInfo;
    }

	public void SetContentID(String strContentID)
    {
        mStrContentID = strContentID;
    }

    public String GetContentID()
    {
        return mStrContentID;
    }

	public void SetPlayMode(int nPlayMode)
    {
        mPlayMode = nPlayMode;
    }

    public int GetPlayMode()
    {
        return mPlayMode;
    }

    public void SetPlayType(int nPlayType)
    {
        mPlayType = nPlayType;
    }

    public int GetPlayType()
    {
        return mPlayType;
    }

    public void SetDurationSec(int nDurationSec)
    {
        mDurationSec = nDurationSec;
    }

    public int GetDurationSec()
    {
        return mDurationSec;
    }

    public void SetResumeSec(int nResumeSec)
    {
        mResumeSec = nResumeSec;
    }

    public int GetResumeSec()
    {
        return mResumeSec;
    }

    public void SetOTPID(String strOTPID)
    {
        mStrOTPID = strOTPID;
    }

    public String GetOTPID()
    {
        return mStrOTPID;
    }

    public void SetOTPPasswd(String strOTPPasswd)
    {
        mStrOTPPasswd = strOTPPasswd;
    }

    public String GetOTPPasswd()
    {
        return mStrOTPPasswd;
    }

    public Uri getUri() {
        return mUri;
    }

    public void setUri(Uri uri) {
        this.mUri = uri;
    }

    public int getDRMType() {
        return mDRMType;
    }

    public void setmDRMType(int mDRMType) {
        this.mDRMType = mDRMType;
    }

    public int getLastFrame() {
        return mLastFrame;
    }

    public void setLastFrame(int mLastFrame) {
        this.mLastFrame = mLastFrame;
    }
}
    
