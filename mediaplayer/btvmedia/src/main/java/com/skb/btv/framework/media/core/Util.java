package com.skb.btv.framework.media.core;

/**
 * Created by hiizu on 2018-01-24.
 */

public class Util {
    public static String printStackTrace() {
        StringBuffer sb = new StringBuffer();
        StackTraceElement[] stackTraceElement = new Exception().getStackTrace();

        for( StackTraceElement element : stackTraceElement ){
            sb.append( element.toString() + "\n" );
        }

        return sb.toString();
    }
}
