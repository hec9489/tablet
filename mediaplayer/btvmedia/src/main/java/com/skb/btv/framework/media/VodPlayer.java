package com.skb.btv.framework.media;

import android.content.Context;
import android.os.Looper;
import android.util.Log;

import com.skb.btv.framework.media.core.MediaPlayer;

import java.util.ArrayList;
import java.util.List;

public class VodPlayer extends MediaPlayer {

    public interface OnPreparedListener {
        void onPrepared(VodPlayer mp);
    }

    public interface OnInfoListener {
        boolean onInfo(VodPlayer mp, int what, int extra);
    }

    public interface OnErrorListener {
        boolean onError(VodPlayer mp, int what, int extra);
    }

    public interface OnCompletionListener {
        void onCompletion(VodPlayer mp);
    }

    public interface OnSeekCompletionListener {
        void onSeekCompletion(VodPlayer mp);
    }

    public interface OnWebVTTUpdateListener {
        void onWebVTTUpdated(VodPlayer mp, List<WebVTT> webVTTList);
    }

    private OnPreparedListener onPreparedListener;
    private OnInfoListener onInfoListener;
    private OnErrorListener onErrorListener;
    private OnCompletionListener onCompletionListener;
    private OnSeekCompletionListener onSeekCompletionListener;
    private OnWebVTTUpdateListener  onWebVTTUpdateListener;

    private MediaPlayer.OnPreparedListener onMediaPlayerPreparedListener;
    private MediaPlayer.OnInfoListener onMediaPlayerInfoListener;
    private MediaPlayer.OnErrorListener onMediaPlayerErrorListener;
    private MediaPlayer.OnCompletionListener onMediaPlayerCompletionListener;
    private MediaPlayer.OnSeekCompletionListener onMediaPlayerSeekCompletionListener;
    private MediaPlayer.OnWebVTTUpdateListener  onMediaPlayerWebVTTUpdateListener;

    public VodPlayer(Context context) {
        super(context);
        initialize();
    }

    public VodPlayer(Context context, int pipId) {
        super(context, pipId);
        initialize();
    }

    private void initialize(){
        onMediaPlayerPreparedListener = new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                Log.i("IPTV", "VodPlayer onPrepared");
                if(onPreparedListener != null)  onPreparedListener.onPrepared(VodPlayer.this);
            }
        };

        onMediaPlayerInfoListener = new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                Log.i("IPTV", "VodPlayer onInfo");
                if(onInfoListener != null) onInfoListener.onInfo(VodPlayer.this, what, extra);
                return false;
            }
        };

        onMediaPlayerErrorListener = new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.i("IPTV", "VodPlayer onError");
                if(onErrorListener != null) onErrorListener.onError(VodPlayer.this, what, extra);
                return false;
            }
        };

        onMediaPlayerCompletionListener = new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Log.i("IPTV", "VodPlayer onCompletion");
                if(onCompletionListener != null) onCompletionListener.onCompletion(VodPlayer.this);
            }
        };

        onMediaPlayerSeekCompletionListener = new MediaPlayer.OnSeekCompletionListener() {
            @Override
            public void onSeekCompletion(MediaPlayer mp) {
                Log.i("IPTV", "VodPlayer onSeekCompletion");
                if(onSeekCompletionListener != null) onSeekCompletionListener.onSeekCompletion(VodPlayer.this);
            }
        };

        onMediaPlayerWebVTTUpdateListener = new MediaPlayer.OnWebVTTUpdateListener() {
            @Override
            public void onWebVTTUpdated(MediaPlayer mp, List<WebVTT> webVTTList) {
                if(onWebVTTUpdateListener != null) onWebVTTUpdateListener.onWebVTTUpdated(VodPlayer.this, webVTTList);
            }
        };
    }
    //Register a callback to be invoked when the media source is ready for playback.
    public void setOnPreparedListener(OnPreparedListener listener) {
        onPreparedListener = listener;
        if(listener != null)
            super.setOnPreparedListener(onMediaPlayerPreparedListener);
        else
            super.setOnPreparedListener(null);
    }

    //Register a callback to be invoked when an info/warning is available.
    public void setOnInfoListener(OnInfoListener listener) {
        onInfoListener = listener;
        if(listener != null)
            super.setOnInfoListener(onMediaPlayerInfoListener);
        else
            super.setOnInfoListener(null);
    }

    //Register a callback to be invoked when an error has happened during an asynchronous operation.
    public void setOnErrorListener(OnErrorListener listener) {
        onErrorListener = listener;
        if(listener != null)
            super.setOnErrorListener(onMediaPlayerErrorListener);
        else
            super.setOnErrorListener(null);
    }

    //Register a callback to be invoked when the end of a media source has been reached during playback.
    public void setOnCompletionListener(OnCompletionListener listener) {
        onCompletionListener = listener;
        if(listener != null)
            super.setOnCompletionListener(onMediaPlayerCompletionListener);
        else
            super.setOnCompletionListener(null);
    }

    public void setOnSeekCompletionListener(OnSeekCompletionListener listener) {
        onSeekCompletionListener = listener;
        if(listener != null)
            super.setOnSeekCompletionListener(onMediaPlayerSeekCompletionListener);
        else
            super.setOnSeekCompletionListener(null);
    }

    public void setOnWebVTTUpdateListener(OnWebVTTUpdateListener listener){
        Log.i("IPTV", "setOnWebVTTUpdateListener");
        onWebVTTUpdateListener = listener;
        if(listener != null)
            super.setOnWebVTTUpdateListener(onMediaPlayerWebVTTUpdateListener);
        else
            super.setOnWebVTTUpdateListener(null);
    }


    public ArrayList<String> getLanguageList(int index) {
        return super.getLanguageList(index);
    }

    public void setTrackLanguage(int index, String language){
        super.setTrackLanguage(index, language);
    }

    public void setTrackDisable(int index, boolean flag){
        super.setTrackDisable(index, flag);
    }

    public void adjustSubtitleOffset(long offset){
        super.adjustSubtitleOffset(offset);
    }

    public void setLooper(Looper looper) {
        super.setLopper(looper);
    }


    public void release() {
        super.setOnSeekCompletionListener(null);
        super.setOnCompletionListener(null);
        super.setOnErrorListener(null);
        super.setOnInfoListener(null);
        super.setOnPreparedListener(null);
        super.setOnWebVTTUpdateListener(null);
        super.release();
        super.setLopper(null);

        onMediaPlayerPreparedListener = null;
        onMediaPlayerInfoListener = null;
        onMediaPlayerErrorListener = null;
        onMediaPlayerCompletionListener = null;
        onMediaPlayerSeekCompletionListener = null;
        onMediaPlayerWebVTTUpdateListener = null;
    }
}
