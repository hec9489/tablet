
#define LOG_TAG "halMediaPlayer"

#include <ALog.h>
#include <string.h>

#include "btv_hal.h"
#include "hal_av_pipeline.h"

#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif

//aml_player_info gamlPlayerInfo[2];

int  AVP_Create(AVP_HANDLE *player, AVP_CreateConfig *config) {
    ALOGD("BTF|%s|%d|IN|\n", __FUNCTION__,__LINE__);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

int  AVP_Destroy(AVP_HANDLE player) {
    ALOGD("BTF|%s|%d|IN| player=%x\n", __FUNCTION__,__LINE__, player);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}

int  AVP_Start(AVP_HANDLE player, AVP_PlayerConfig *config) {
    ALOGD("BTF|%s|%d|IN| player=%x, vcodec=%d, acodec=%d, pcr_pid=%d, video_pid=%d, audio_pid=%d \n",
            __FUNCTION__,__LINE__, player, config->video_codec, config->audio_codec, config->pcr_pid, config->video_pid, config->audio_pid);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	
	return 0;
}

int  AVP_Stop(AVP_HANDLE player) {
    ALOGD("BTF|%s|%d|IN| player=%x\n", __FUNCTION__,__LINE__, player);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}

int  AVP_Pause(AVP_HANDLE player) {
    ALOGD("BTF|%s|%d|IN| player=%x\n", __FUNCTION__,__LINE__, player);


    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}


int  AVP_Resume(AVP_HANDLE player) {
    ALOGD("BTF|%s|%d|IN| player=%x\n", __FUNCTION__,__LINE__, player);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}

int  AVP_InjectTS(AVP_HANDLE player, char *buffer, int size) {

    //return aml_feedData((aml_player_info *)player, (uint8_t *)buffer, size);
	return size;
}

int  AVP_InjectCommand(AVP_HANDLE player, AVP_InjectCmdType command) {
    ALOGD("BTF|%s|%d|IN| player=%x, AVP_InjectCmdType:%d\n", __FUNCTION__,__LINE__, player, command);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}


int  AVP_GetVideoPTS(AVP_HANDLE player, long long *pts) {

	return 0;
}

int  AVP_FlushTS(AVP_HANDLE player) {
    ALOGD("BTF|%s|%d|IN| player=%x\n", __FUNCTION__,__LINE__, player);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}

int  AVP_SetPlaySpeed(AVP_HANDLE player, float speed) {
    ALOGD("BTF|%s|%d|IN| player=%x, speed:%d\n", __FUNCTION__,__LINE__, player, speed);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}

int  AVP_SetVideoPlaneSize(AVP_HANDLE player, int x, int y, int width, int height) {
    ALOGD("BTF|%s|%d|IN| player=%x, x=%d y=%d width=%d height=%d \n", __FUNCTION__,__LINE__, player, x, y, width, height);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

int  AVP_SetWindowSize(AVP_HANDLE player, int x, int y, int width, int height) {
    ALOGD("BTF|%s|%d|IN| player=%x, x=%d y=%d width=%d height=%d \n", __FUNCTION__,__LINE__, player, x, y, width, height);


    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return 0;
}


int  AVP_SetSurface(AVP_HANDLE player, void *nativeWidow){
    ALOGD("BTF|%s|%d|IN| player=%x, nativeWidow=%x \n", __FUNCTION__,__LINE__, player, nativeWidow);
	
    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

int  AVP_SetAVoffset(AVP_HANDLE player, AVP_AVoffset offset) {
    ALOGD("BTF|%s|%d|IN| player=%x, offset=%d \n", __FUNCTION__,__LINE__, player, offset);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	
	return 0;
}

int  AVP_SetVideoMute(AVP_HANDLE player, bool mute) {
    ALOGD("BTF|%s|%d|IN| player=%x, mute=%d \n", __FUNCTION__,__LINE__, player, mute);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}

int  AVP_SetAudioMute(AVP_HANDLE player, bool mute) {
    ALOGD("BTF|%s|%d|IN| player=%x, mute=%d \n", __FUNCTION__,__LINE__, player, mute);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}

int  AVP_GetAudioMute(AVP_HANDLE player, bool *mute_status) {
    ALOGD("BTF|%s|%d|IN| player=%x \n", __FUNCTION__,__LINE__, player);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return -1;
}

int  AVP_SetAudioStream(AVP_HANDLE player, AVP_AudioConfig *newAudio) {
    ALOGD("BTF|%s|%d|IN| player=%x, codec=%d, pid=%d \n", __FUNCTION__,__LINE__, player, newAudio->codec, newAudio->pid);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	
	return 0;

}

int  AVP_SetSectionFilter(AVP_HANDLE player, AVP_SectionFilterConfig *config) {
    ALOGD("BTF|%s|%d|IN| player=%x, pid=%d, tid=%d,  \n", __FUNCTION__,__LINE__, player, config->pid, config->tid);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}

int  AVP_ClearSectionFilter(AVP_HANDLE player) {
    ALOGD("BTF|%s|%d|IN| player=%x \n", __FUNCTION__,__LINE__, player);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}

int  AVP_SetCommand(AVP_HANDLE player, int request, void* in_param, void* out_param) {
    ALOGD("BTF|%s|%d|IN| player=%x, request=%d, in_param=%d \n", __FUNCTION__,__LINE__, player, request, (in_param == NULL) ? 0:*(bool*)in_param);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}

int AVP_VIDEO_SetErrorConcealment(AVP_HANDLE player, AVP_DecoderErrorCallback errorCallback, void *user_param) {
    ALOGD("BTF|%s|%d|IN| player=%x, errorCallback=%d, user_param=%d \n", __FUNCTION__,__LINE__, player, errorCallback, user_param);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

       return 0;
}

int  AVP_SetMediaAudioVolume(AVP_HANDLE player, int volume, float gain) {
    ALOGD("BTF|%s|%d|IN| player=%x \n", __FUNCTION__,__LINE__);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}


int  AVP_GetMediaAudioVolume(AVP_HANDLE player, int *volume){
    ALOGD("BTF|%s|%d|IN| player=%x \n", __FUNCTION__,__LINE__, player);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

#ifdef __cplusplus
} //extern "C"
#endif
