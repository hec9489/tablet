#ifndef _HAL_V_DISP_________H
#define _HAL_V_DISP_________H

#ifndef CHECK_RETURN
#define CHECK_RETURN(x) (x == 0? (void)0:(printf("%s returned error \n", #x)))
#endif
#ifndef UNUSED
#define UNUSED(x) (void)(x)
#endif

#define DISP_EVENT_HDMI_UNPLUGED  0
#define DISP_EVENT_HDMI_PLUGED    1

#define DISP_EVENT_CEC_POWER_OFF  0
#define DISP_EVENT_CEC_POWER_ON   1


#endif
