/*
 *  Filename : hal_v_disp.c 
 *  DISP APIs  
 *
 */
#define LOG_TAG "Display.20.04.06"
	 
#include <ALog.h>
 
#include "btv_hal.h"
#include "hal_v_disp.h"

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>



#ifdef __cplusplus
extern "C" {
#endif

#define STRINGIFY(x) case x: return #x

#define UI_SVD_BLOCK_LEN_MAX 31 // uiSvdBlockLen

#define DEFAULT_OUT_RES         AMP_DISP_OUT_RES_720P5994
#define DEFAULT_OUT_BIT_DEPTH   AMP_DISP_OUT_BIT_DPE_8
#define DEFAULT_OUT_COLOR_FMT   AMP_DISP_OUT_CLR_FMT_RGB888
#define DEFAULT_OUT_PIXEL_RPT   1

#define TMDS_MAX_CHAR_RATE_575MHZ 0x253  /* HF VSDB Max char rate block*/
#define TMDS_MAX_CHAR_RATE_371MHZ 0x172
#define TMDS_MAX_CLOCK            0x12C /*Vendor specific info bcl 1.4 TMDS clk*/
#define TMDS_1080P_DC_10BIT_FREQ  0x25
#define Is4KSupportedByYUV420DeepColor10bit(x) ((x)&0x01)
#define Is4KSupportedByYUV420DeepColor12bit(x) ((x>>1)&0x01)

#define VIC_LEN 16
#define VIC_4Kx2KP50 96
#define VIC_4Kx2KP5994_4Kx2KP60 97
#define VIC_4Kx2KP5994_4Kx2KP50 96

#define DEEPCOLOR_8BIT_444_SUPPORT    1<<0
#define DEEPCOLOR_10BIT_444_SUPPORT   1<<1
#define DEEPCOLOR_12BIT_444_SUPPORT   1<<2
#define DEEPCOLOR_444_SUPPORT         1<<3
#define DEEPCOLOR_10BIT_420_SUPPORT   1<<4
#define DEEPCOLOR_12BIT_420_SUPPORT   1<<5

#define Is8bitSupported(x)               ((x)&0x01)
#define Is10bitSupported(x)              ((x>>1)&0x01)
#define Is12bitSupported(x)              ((x>>2)&0x01)
#define IsDeepClrSupported(x)            ((x>>3)&0x01)
#define ISDeepClr420YUV10BitSupported(x) ((x>>4)&0x01)
#define ISDeepClr420YUV12BitSupported(x) ((x>>5)&0x01)

// add echwang : 2020.04.03 - S
#define PROPERTY_VALUE_MAX 256

int  DISPLAY_Create(DISPLAY_HANDLE *display) {
    ALOGD("BTF|%s|%d|IN|\n", __FUNCTION__,__LINE__);


    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}


int  DISPLAY_Destroy(DISPLAY_HANDLE display) {
    ALOGD("BTF|%s|%d|IN| display:%x \n", __FUNCTION__,__LINE__, display);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}

int  DISPLAY_GetSupportedResolutionList(DISPLAY_HANDLE display, char* resolution_list){
    ALOGD("BTF|%s|%d|IN| display:%x \n", __FUNCTION__,__LINE__, display);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
	
}

int  DISPLAY_SetResolution(DISPLAY_HANDLE display, BTF_DISPLAY_Resolution resolution) {
    ALOGD("BTF|%s|%d|IN| display:%x, resolution:%d \n", __FUNCTION__,__LINE__, display, resolution);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
}


int  DISPLAY_GetResolution(DISPLAY_HANDLE display, BTF_DISPLAY_Resolution *resolution) {
    ALOGD("BTF|%s|%d|IN| display:%x \n", __FUNCTION__,__LINE__, display);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

int	DISPLAY_CEC_SetEnable(DISPLAY_HANDLE display, bool enable) {
    ALOGD("BTF|%s|%d|IN| display:%x \n", __FUNCTION__,__LINE__, display);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}

int	DISPLAY_CEC_GetState(DISPLAY_HANDLE display, bool *state) {
    ALOGD("BTF|%s|%d|IN| display:%x \n", __FUNCTION__,__LINE__, display);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

int	DISPLAY_CEC_SetPowerOnOff(DISPLAY_HANDLE display, bool onoff) {
    ALOGD("BTF|%s|%d|IN| display:%x, onoff:%d \n", __FUNCTION__,__LINE__, display, onoff);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}


int	DISPLAY_HDR_SetEnable(DISPLAY_HANDLE display, bool enable) {
    ALOGD("BTF|%s|%d|IN| display:%x, enable:%d \n", __FUNCTION__,__LINE__, display, enable);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}


int	DISPLAY_HDR_GetState(DISPLAY_HANDLE display, bool *state) {
    ALOGD("BTF|%s|%d|IN| display:%x \n", __FUNCTION__,__LINE__, display);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

int	DISPLAY_HDMI_SetEnable(DISPLAY_HANDLE display, bool enable)
{
    ALOGD("BTF|%s|%d|IN| display:%x, enable:%d \n", __FUNCTION__,__LINE__, display, enable);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

}

int	DISPLAY_HDMI_CEC_ActiveSource(DISPLAY_HANDLE display, bool activeSource)
{
    ALOGD("BTF|%s|%d|IN| display:%x, activeSource:%d \n", __FUNCTION__,__LINE__, display, activeSource);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}


int  DISPLAY_GetHdmiEdidInfo(DISPLAY_HANDLE display, char* hdmi_edid_info)
{
    ALOGD("BTF|%s|%d|IN| display:%x \n", __FUNCTION__,__LINE__, display);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}


int	DISPLAY_HDR_GetSupported(DISPLAY_HANDLE display, bool *support) {
    ALOGD("BTF|%s|%d|IN| display:%x \n", __FUNCTION__,__LINE__, display);

	ALOGD("++%s : support(%d)", __FUNCTION__, *support);	
	return 0;
}


int  DISPLAY_SetHdmiHotpluggedListener(DISPLAY_HANDLE display, DISPLAY_HDMIHotPluggedCallback callback) {
    ALOGD("BTF|%s|%d|IN| display:%x \n", __FUNCTION__,__LINE__, display);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

int  DISPLAY_SetCECPowerStateListener(DISPLAY_HANDLE display, DISPLAY_CECPowerStateCallback callback) {
    ALOGD("BTF|%s|%d|IN| display:%x \n", __FUNCTION__,__LINE__, display);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}
//================================
int  DISPLAY_HDMI_SetEventListener(DISPLAY_HANDLE display, DISPLAY_HDMIEventCallback callback) {

    ALOGD("++%s ", __FUNCTION__);

	return 0;
}

int  DISPLAY_CEC_SetEventListener(DISPLAY_HANDLE display, DISPLAY_CECEventCallback callback) {

    ALOGD("++%s ", __FUNCTION__);

	return 0;
}

int  DISPLAY_HDMI_CEC_SetMessageListener(DISPLAY_HANDLE display, DISPLAY_HDMI_CECMessageCallback callback) {

    ALOGD("++%s ", __FUNCTION__);

	return 0;
}

int  DISPLAY_HDMI_CEC_SendMessage(DISPLAY_HANDLE display, int type, void* message) {

    ALOGD("++%s ", __FUNCTION__);

	// TO-DO 
	return 0;
}



#ifdef __cplusplus
} //extern "C"
#endif
