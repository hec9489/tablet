/*
 *  Filename :hal_a_mixer.c 
 *  Mixer control APIs  
 *
 */

#define LOG_TAG "AudioMixer"

#include <ALog.h>
 
#include "btv_hal.h"
#include "hal_a_mixer.h"

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

int  AMIXER_Create(AMIXER_HANDLE *amixer) {
	ALOGD("BTF|%s|%d|IN|\n", __FUNCTION__,__LINE__);

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return 0;
}


int  AMIXER_Destroy(AMIXER_HANDLE amixer) {
	ALOGD("BTF|%s|%d|IN| amixer=%x\n", __FUNCTION__,__LINE__, amixer);
	
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return 0;
}

int  AMIXER_SetMasterAudioMute(AMIXER_HANDLE amixer, bool mute) {
	ALOGD("BTF|%s|%d|IN| amixer=%x mute=%d\n", __FUNCTION__,__LINE__, amixer, mute);

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
    return 0;
}


int  AMIXER_SetMasterAudioVolume(AMIXER_HANDLE amixer, int volume) {
	ALOGD("BTF|%s|%d|IN| amixer=%x volume=%d\n", __FUNCTION__,__LINE__, amixer, volume);

	int rtn = 0;
	
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
    return rtn;
}

int  AMIXER_GetMasterAudioVolume(AMIXER_HANDLE amixer, int *volume) {
	ALOGD("BTF|%s|%d|IN| amixer=%x volume=%d\n", __FUNCTION__,__LINE__, amixer, *volume);

	int rtn = 0;

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return rtn;
}


int  AMIXER_SetAudioOutputMode(AMIXER_HANDLE amixer, AVP_AudioOutputMode mode) {
	ALOGD("BTF|%s|%d|IN| amixer=%x mode=%d\n", __FUNCTION__,__LINE__, amixer, mode);


	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

int  AMIXER_GetAudioOutputMode(AMIXER_HANDLE amixer, AVP_AudioOutputMode *mode) {
	ALOGD("BTF|%s|%d|IN| amixer=%x mode=%d\n", __FUNCTION__,__LINE__, amixer, *mode);


	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}

int AMIXER_SetAudioSnoop(bool enable) {
	ALOGD("BTF|%s|%d|IN|\n", __FUNCTION__,__LINE__);


	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}


int  AMIXER_GetSoundEffect(AMIXER_HANDLE amixer, AVP_SoundEffect *mode) {

    ALOGD("BTF|%s|%d|IN|\n", __FUNCTION__,__LINE__);


    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}


int  AMIXER_SetSoundEffect(AMIXER_HANDLE amixer, AVP_SoundEffect mode){

    ALOGD("BTF|%s|%d|IN|\n", __FUNCTION__,__LINE__);


    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}


int  AMIXER_GetAudioVolume(AMIXER_HANDLE amixer, int *volume){

    ALOGD("BTF|%s|%d|IN|\n", __FUNCTION__,__LINE__);


    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}


int  AMIXER_SetAudioVolume(AMIXER_HANDLE amixer, int volume, float gain){

    ALOGD("BTF|%s|%d|IN|\n", __FUNCTION__,__LINE__);


    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

#ifdef __cplusplus
} //extern "C"
#endif
