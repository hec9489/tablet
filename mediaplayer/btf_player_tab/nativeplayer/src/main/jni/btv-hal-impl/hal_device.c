/*
 *  Filename : hal_device.c 
 *  DEVICE APIs  
 *
 */
#define LOG_TAG "DEVICE"

#include <ALog.h>
 
#include "btv_oem_hal.h"
#include "hal_device.h"

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>



#ifdef __cplusplus
extern "C" {
#endif

int  DEVICE_Create(DEVICE_HANDLE *device){
	ALOGD("BTF|%s|%d|IN|\n", __FUNCTION__,__LINE__);

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

int  DEVICE_Destroy(DEVICE_HANDLE device){
	ALOGD("BTF|%s|%d|IN| device:%x\n", __FUNCTION__,__LINE__, device);

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}

int DEVICE_SetLedState(DEVICE_HANDLE device, int led_id, int state){
	ALOGD("BTF|%s|%d|IN| device:%x, led_id:%d, state:%d\n", __FUNCTION__,__LINE__, device, led_id, state);

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}


int DEVICE_SetLedColor(DEVICE_HANDLE device, int led_id, int color){
	ALOGD("BTF|%s|%d|IN| device:%x, led_id:%d, color:%d\n", __FUNCTION__,__LINE__, device, led_id, color);


	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}


int DEVICE_SetVolumeLedState(DEVICE_HANDLE device, int led_id, int state, int volume){
	ALOGD("BTF|%s|%d|IN| device:%x, led_id:%d, state:%d, volume:%d\n", __FUNCTION__,__LINE__, device, led_id, state, volume);


	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}


int DEVICE_SetBeamformingLedState(DEVICE_HANDLE device, int led_id, int state, int degree){
	ALOGD("BTF|%s|%d|IN| device:%x, led_id:%d, state:%d, degree:%d\n", __FUNCTION__,__LINE__, device, led_id, state, degree);

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}

int DEVICE_SetMicOnOff(DEVICE_HANDLE device, bool enable){
	ALOGD("BTF|%s|%d|IN| device:%x, enable:%d\n", __FUNCTION__,__LINE__, device, enable);

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}

int DEVICE_GetMicVolume(DEVICE_HANDLE device, int *volume_level){
	ALOGD("BTF|%s|%d|IN| device:%x \n", __FUNCTION__,__LINE__, device);

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}


int DEVICE_SetMicVolume(DEVICE_HANDLE device, int volume_level){
	ALOGD("BTF|%s|%d|IN| device:%x, volume_level:%d \n", __FUNCTION__,__LINE__, device, volume_level);

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}


int DEVICE_GetMicEchoLevel(DEVICE_HANDLE device, int *echo_level){
	ALOGD("BTF|%s|%d|IN| device:%x\n", __FUNCTION__,__LINE__, device);

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}


int DEVICE_SetMicEchoLevel(DEVICE_HANDLE device, int echo_level){
	ALOGD("BTF|%s|%d|IN| device:%x, echo_level:%d \n", __FUNCTION__,__LINE__, device, echo_level);

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return 0;
}

int DEVICE_MicPause(DEVICE_HANDLE device){
	ALOGD("BTF|%s|%d|IN| device:%x \n", __FUNCTION__,__LINE__, device);


	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}

int DEVICE_MicResume(DEVICE_HANDLE device){
	ALOGD("BTF|%s|%d|IN| device:%x \n", __FUNCTION__,__LINE__, device);

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return 0;
}


#ifdef __cplusplus
} //extern "C"
#endif
