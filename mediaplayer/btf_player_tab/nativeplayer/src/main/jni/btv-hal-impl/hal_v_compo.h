#ifndef _HAL_V_COMPO_________H
#define _HAL_V_COMPO_________H


#ifndef CHECK_RETURN
#define CHECK_RETURN(x) (x == 0? (void)0:(printf("%s returned error \n", #x)))
#endif
#ifndef UNUSED
#define UNUSED(x) (void)(x)
#endif

#endif
