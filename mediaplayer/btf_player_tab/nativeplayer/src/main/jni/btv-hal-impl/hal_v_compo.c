/*
 *  Filename : hal_v_compo.c 
 *  Video Compositor APIs  
 *
 */

#define LOG_TAG "VideoCompositor"

#include <ALog.h>
 
#ifdef __cplusplus
extern "C" {
#endif

#include "btv_hal.h"
#include "hal_v_compo.h"

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#define PLANE_ID_MASK    0xFFE00000
#define PLANE_ID_OFFSET 21
#define PARAM_MASK   0x1FFFFF
#define MULTIPLE_RANGE 5	// ui 0-100 => soc 0- 500

enum ENUM_PICTURE_CTRL
{
	PICTURE_CTRL_FISRT = 0,
	PICTURE_CTRL_BRIGHTNESS = 0,
	PICTURE_CTRL_CONTRAST = 1,
	PICTURE_CTRL_SATURATION = 2,
	PICTURE_CTRL_HUE = 3,
	PICTURE_CTRL_MAX  = 4,	
};

int  VCOMPO_Create(VCOMPO_HANDLE *vcompo) {
    ALOGD("BTF|%s|%d|IN|\n", __FUNCTION__,__LINE__);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

int  VCOMPO_Destroy(VCOMPO_HANDLE vcompo) {
    ALOGD("BTF|%s|%d|IN| vcompo:%x \n", __FUNCTION__,__LINE__, vcompo);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}


int  VCOMPO_SetZorder(VCOMPO_HANDLE vcompo, VIDEO_Zorder_Plane zorder1, VIDEO_Zorder_Plane zorder2,
                      VIDEO_Zorder_Plane zorder3, VIDEO_Zorder_Plane zorder4) {
    ALOGD("BTF|%s|%d|IN| vcompo:%x, zorder1:%d, zorder2:%d, zorder3:%d, zorder4:%d \n", __FUNCTION__,__LINE__, vcompo, zorder1, zorder2, zorder3, zorder4);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

static AVP_AspectRatioMode aspectRatioMode; //TODO: is it really necessary ?

int  VCOMPO_SetScaleRatioMode(VCOMPO_HANDLE vcompo, AVP_AspectRatioMode mode) {
    ALOGD("BTF|%s|%d|IN| vcompo:%x, mode:%d \n", __FUNCTION__,__LINE__, vcompo, mode);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

int  VCOMPO_SetVideoBrightness(VCOMPO_HANDLE vcompo, int brightness) {
    ALOGD("BTF|%s|%d|IN| vcompo:%x, brightness:%d \n", __FUNCTION__,__LINE__, vcompo, brightness);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

int  VCOMPO_GetVideoBrightness(VCOMPO_HANDLE vcompo, int *brightness) {
    ALOGD("BTF|%s|%d|IN| vcompo:%x \n", __FUNCTION__,__LINE__, vcompo);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

int  VCOMPO_SetVideoContrast(VCOMPO_HANDLE vcompo, int contrast) {
    ALOGD("BTF|%s|%d|IN| vcompo:%x, contrast:%d \n", __FUNCTION__,__LINE__, vcompo, contrast);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

int  VCOMPO_GetVideoContrast(VCOMPO_HANDLE vcompo, int *contrast) {
    ALOGD("BTF|%s|%d|IN| vcompo:%x \n", __FUNCTION__,__LINE__, vcompo);

    return 0;
}

int  VCOMPO_SetVideoHue(VCOMPO_HANDLE vcompo, int hue) {
    ALOGD("BTF|%s|%d|IN| vcompo:%x, hue:%d \n", __FUNCTION__,__LINE__, vcompo, hue);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}


int  VCOMPO_GetVideoHue(VCOMPO_HANDLE vcompo, int *hue) {
    ALOGD("BTF|%s|%d|IN| vcompo:%x \n", __FUNCTION__,__LINE__, vcompo);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

int  VCOMPO_SetVideoSaturation(VCOMPO_HANDLE vcompo, int saturation) {
    ALOGD("BTF|%s|%d|IN| vcompo:%x, saturation:%d \n", __FUNCTION__,__LINE__, vcompo, saturation);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}

int  VCOMPO_GetVideoSaturation(VCOMPO_HANDLE vcompo, int *saturation) {
    ALOGD("BTF|%s|%d|IN| vcompo:%x \n", __FUNCTION__,__LINE__, vcompo);

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return 0;
}



#ifdef __cplusplus
} //extern "C"
#endif
