// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#define LOG_TAG "com.skb.btvservice@1.0-service"

#include <android-base/logging.h>
#include <android/log.h>
#include <hidl/HidlSupport.h>
#include <hidl/HidlTransportSupport.h>
#include "TVService.h"
#include "BtvMediaPlayer.h"


#include "iptvmedia/util_logmgr.h"

#include <fcntl.h>

#define DEBUG_TAG_MODULE "com.skb.btvservice@1.0-service"

#define DEBUGMODE_STDOUT "/btf/btvmedia/log/iptvserver_stdout"
#define DEBUGMODE_LOGCAT "/btf/btvmedia/log/iptvserver_logcat"
#define DEBUGMODE_FILE "/btf/btvmedia/log/iptvserver_file"

#define BTV_MAIN "btv_main"
#define BTV_PIP  "btv_pip"

using android::hardware::configureRpcThreadpool;
using android::hardware::joinRpcThreadpool;

using android::sp;
using android::OK;
using android::int32_t;

using ::com::skb::btvservice::V1_0::IBtvMediaPlayer;
using ::com::skb::btvservice::V1_0::ITVService;
using ::com::skb::btvservice::V1_0::implementation::BtvMediaPlayer;
using ::com::skb::btvservice::V1_0::implementation::BTV_DEVICE_MAIN;
using ::com::skb::btvservice::V1_0::implementation::BTV_DEVICE_PIP;
using ::com::skb::btvservice::V1_0::implementation::TVService;

void execute_init()
{
	int ret = 0;
	char szSystemCall[256] = {0,};

	sprintf(szSystemCall, "alsa_amixer -Dhw:MIXER0 sset \"Emergency Mute\" \"Source-only\"");
	ret = system(szSystemCall);
   // YIWOO CHECK
	sprintf(szSystemCall, "mkdir -p /data/user/0/com.skb.framework.myapplication/btf/btvmedia/cas");
	ret = system(szSystemCall);

	sprintf(szSystemCall, "mkdir -p /data/user/0/com.skb.framework.myapplication/btf/btvmedia/tmp/iptv_cas");
	ret = system(szSystemCall);

	sprintf(szSystemCall, "mkdir /data/user/0/com.skb.framework.myapplication/btf/btvmedia/drm");
	ret = system(szSystemCall);

	sprintf(szSystemCall, "mkdir -p %s",LOG_FILE_DIR);
	ret = system(szSystemCall);
}

int main(int /*argc*/, char **/*argv*/) {

	execute_init();

	log_Initialize(DEBUG_TAG_MODULE);
	char debugmode_stdout[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, debugmode_stdout, 128);
	strcat(debugmode_stdout, DEBUGMODE_STDOUT);	
	if( 0 == access(debugmode_stdout, F_OK)){
		log_SetOption(log_GetOption()|LOG_OPT_STDOUT);
	}
	else{
		log_SetOption(log_GetOption()&~LOG_OPT_STDOUT);
	}
	char debugmode_logcat[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, debugmode_logcat, 128);
	strcat(debugmode_logcat, DEBUGMODE_LOGCAT);		
	if( 0 == access(debugmode_logcat, F_OK)){
		log_SetOption(log_GetOption()|LOG_OPT_LOGCATOUT);
	}
	else{
		log_SetOption(log_GetOption()&~LOG_OPT_LOGCATOUT);
	}

	/*
	char debugmode_file[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, debugmode_file, 128);
	strcat(debugmode_file, DEBUGMODE_FILE);	
	if( 0 == access(DEBUGMODE_FILE, F_OK)){		// DEBUGMODE_FILE ???? ???ο? ???? file log ???? ????.
		log_SetOption(log_GetOption()|LOG_OPT_FILEOUT);
	}
	else{
		log_SetOption(log_GetOption()&~LOG_OPT_FILEOUT);
	}
	*/

	log_SetOption(log_GetOption()|LOG_OPT_FILEOUT);	// file log ?? ?????? ????.

	LOGD("BtvService service starting.");

	android::sp<IBtvMediaPlayer> main_service = new BtvMediaPlayer(BTV_DEVICE_MAIN);
	if (main_service == nullptr) {
		LOGE("Can not create an instance of BtvMediaPlayer Main Iface, exiting.");
		return 1;
	}
	android::sp<IBtvMediaPlayer> pip_service = new BtvMediaPlayer(BTV_DEVICE_PIP);
	if (pip_service == nullptr) {
		LOGE("Can not create an instance of BtvMediaPlayer Pip Iface, exiting.");
		return 1;
	}

	android::sp<ITVService> tv_service = new TVService();
	if (tv_service == nullptr) {
		LOGE("Can not create an instance of TVService Iface, exiting.");
		return 1;
	}

	configureRpcThreadpool(4, true /*callerWillJoin*/);

	int32_t status = main_service->registerAsService(BTV_MAIN);
	if (status != OK) {
		LOGE("Could not register service for Main BtvMediaPlayer Iface (%d)", status);
		return 1;
	}
	status = pip_service->registerAsService(BTV_PIP);
	if (status != OK) {
		LOGE("Could not register service for Pip BtvMediaPlayer Iface (%d)", status);
		return 1;
	}

	status = tv_service->registerAsService(); 
	if (status != OK) {        
		LOGE("Could not register service for TVService HAL Iface (%d)", status);        
		return 1;    
	}	

	LOGD("BtvService service is ready.");

	joinRpcThreadpool();
	
	LOGD("BtvService service is shutting down.");
	return 0;	
}
