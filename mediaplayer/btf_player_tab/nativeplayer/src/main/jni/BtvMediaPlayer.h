// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef COM_SKB_BTVSERVICE_V1_0_BTVMEDIAPLAYER_H
#define COM_SKB_BTVSERVICE_V1_0_BTVMEDIAPLAYER_H

#include <com/skb/btvservice/1.0/IBtvMediaPlayer.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>
#include <utils/threads.h>
#include "BtvMediaPlayerService.h"
#include "BtvMediaPlayerObserver.h"
#include "BtvDsmccObserver.h"
#include "BtvMediaCommunicationObserver.h"

namespace com {
namespace skb {
namespace btvservice {
namespace V1_0 {
namespace implementation {

using ::com::skb::btvservice::V1_0::IBtvMediaPlayer;
using ::com::skb::btvservice::V1_0::IBtvMediaPlayerListener;
using ::com::skb::btvservice::V1_0::IBtvDsmccListener;
using ::com::skb::btvservice::V1_0::IBtvMediaCommunicationListener;
using ::com::skb::btvservice::V1_0::IBtvAudioProgramUpdateListener;
using ::com::skb::btvservice::V1_0::ParcelData;

#ifdef FEATURE_SOC_AMLOGIC
//using ::android::hardware::graphics::bufferqueue::V1_0::IGraphicBufferProducer;
#endif

using ::android::hardware::hidl_array;
using ::android::hardware::hidl_memory;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::Return;
using ::android::hardware::Void;

using ::android::sp;
using ::android::wp;
using ::android::Mutex;
using ::android::BtvMediaPlayerService;
using ::android::BtvMediaPlayerObserver;
using ::android::BtvDsmccObserver;
using ::android::SignalEvent;
using ::android::BtvMediaCommunicationObserver;
using ::android::BtvAudioProgramUpdateObserver;

enum Device {
    BTV_DEVICE_MAIN = 0,
    BTV_DEVICE_PIP  = 1
};

class BtvMediaPlayer : public IBtvMediaPlayer, public ::android::hardware::hidl_death_recipient {
  public:
    BtvMediaPlayer(int device);	
    ~BtvMediaPlayer();

    Return<Status> connect(int32_t device) override;
    Return<Status> disconnect() override;

    Return<Status> open(const hidl_string &dataXML) override;	
    Return<Status> tuneTV(const hidl_string &dataXML) override;	
    Return<Status> closeTV() override;	

    Return<Status> bindingFilter(const hidl_string &dataXML) override;	
    Return<Status> releaseFilter(const hidl_string &dataXML) override;	

    Return<Status> changeAudioChannel(const hidl_string &dataXML) override;	
    Return<Status> changeAudioOnOff(const hidl_string &dataXML) override;	

    Return<void>   getPlayerMode(getPlayerMode_cb _hidl_cb) override;	
    Return<void>   getPlayerStatus(getPlayerStatus_cb _hidl_cb) override;	
    Return<void>   getCurrentPosition(getCurrentPosition_cb _hidl_cb) override;	
  
    Return<Status> setWindowSize(const hidl_string &dataXML) override;	
	
    Return<Status> play() override;
    Return<Status> pause() override;
    Return<Status> resume() override;	
    Return<Status> stop() override;		
    Return<Status> close() override;			
	
    Return<Status> seek(const hidl_string &dataXML, bool pause) override;
    Return<Status> pauseAt(const hidl_string &dataXML) override;
    Return<Status> trick(const hidl_string &dataXML) override;			

    Return<Status> keepLastFrame(bool flag) override;
	
    Return<void>   invoke(const hidl_vec<ParcelData>& request, invoke_cb _hidl_cb) override;

    Return<Status> setPlayerSize(int32_t nLeft, int32_t nTop, int32_t nWidth, int32_t nHeight) override;

    Return<Status> setPlayerSurfaceView(int32_t deviceID, uint32_t nativWindows) override;
	
#ifdef FEATURE_SOC_AMLOGIC
    //Return<Status> setPlayerSurface(const sp<IGraphicBufferProducer>& surface) override;
#endif
	
    Return<Status> startDmxFilter(int32_t pid, int32_t tid) override;
    Return<Status> stopDmxFilter(int32_t pid, int32_t tid) override;
	
    Return<Status> setDummys(const hidl_string &dummys) override;

    Return<void>   setListener(const sp<IBtvMediaPlayerListener>& listener)  override;
    void           notifyCallback(int32_t msg, int32_t ext1, int32_t ext2, const int8_t* obj);
    void           dataCallback(int32_t msg, int32_t ext1, int32_t ext2, const int8_t* dataPtr);

    Return<void>   setDsmccListener(const sp<IBtvDsmccListener>& dsmcclistener)  override;
    void           dsmccSignalEvent(int32_t eventType, char* rootPath, SignalEvent* signalEvent);

    Return<void>   setMediaCommunicationListener(const sp<IBtvMediaCommunicationListener>& mediaCommunicationListener)  override;
    void           familyStateEvent(int32_t state);
    void           onCasInfoEvent(int32_t type, std::string casInfo);

    void           serviceDied(uint64_t cookie, const wp<IBase>& /* who */) override;

    Return<void>   setAudioProgramUpdateListener(const sp<IBtvAudioProgramUpdateListener>& audiolistener)  override;
    void           audioProgramUpdateEvent(char* filepath, int32_t updateType, int32_t audioPid);

    Return<Status>  enableCasInfo(bool enable) override;
	
  private:
    sp<IBtvMediaPlayerListener>  mListener;
    sp<IBtvDsmccListener>        mDsmccListener;
    sp<IBtvMediaCommunicationListener>  mMediaCommunicationListener;
    sp<BtvMediaPlayerService>    mBtvPlayerService;
    sp<IBtvAudioProgramUpdateListener>  mAudioProgramUpdateListener;
    Mutex                        mLock;
    Mutex                        mNotifyLock;
    int                          mDeviceID;	
    sp<BtvMediaPlayerObserver>   mObserver;	
    sp<BtvDsmccObserver>         mDsmccObserver;
    sp<BtvMediaCommunicationObserver>        mMediaCommunicationObserver;
    sp<BtvAudioProgramUpdateObserver>   mAudioProgramUpdateObserver;

};

}  // namespace implementation
}  // namespace V1_0
}  // namespace btvservice
}  // namespace skb
}  // namespace com

#endif  // COM_SKB_BTVSERVICE_V1_0_BTVMEDIAPLAYER_H
