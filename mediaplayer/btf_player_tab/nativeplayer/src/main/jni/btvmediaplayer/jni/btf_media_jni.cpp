#define LOG_TAG "BOXKEY-Btf-Media-JNI"
#include <ALog.h>
#include <jni.h>
#include <utils/RefBase.h>
#include <utils/Vector.h>
#include <utils/Mutex.h>

#include <BtvMediaPlayerService.h>
#include <assert.h>

#include <android/native_window_jni.h>
#include <android/native_window.h>

#include <iptvmedia/IBtvMediaPlayerListener.h>
#include <systemproperty.h>

//#include <android_runtime/android_view_Surface.h>
//#include <gui/IGraphicBufferProducer.h>
//#include <gui/Surface.h>

namespace android {
}

#define	BTV_DEVICE_MAIN  0
#define BTV_DEVICE_PIP   1

using namespace android;

static JavaVM* gJavaVM = NULL;
static JNIEnv* AndroidRuntime_getJNIEnv()
{
    JNIEnv* env;
    assert(gJavaVM != NULL);

    if (gJavaVM->GetEnv((void**) &env, JNI_VERSION_1_6) != JNI_OK)
        return NULL;
    return env;
}

class IptvMediaJniThreadAttach {
public:
    IptvMediaJniThreadAttach() {
        /*
         * attachResult will also be JNI_OK if the thead was already attached to
         * JNI before the call to AttachCurrentThread().
         */
        jint attachResult = gJavaVM->AttachCurrentThread(&mEnv, nullptr);
        if (attachResult != JNI_OK) {
            ALOGD("[%s][%d] Unable to attach thread. Error %d", __FUNCTION__, __LINE__, attachResult);
        }
    }
    ~IptvMediaJniThreadAttach() {
        jint detachResult = gJavaVM->DetachCurrentThread();
        /*
         * Return if the thread was already detached. Log error for any other
         * failure.
         */
        if (detachResult == JNI_EDETACHED) {
            return;
        }
        if (detachResult != JNI_OK) {
            ALOGD("[%s][%d] Unable to detach thread. Error %d", __FUNCTION__, __LINE__, detachResult);
        }
    }
    JNIEnv* getEnv() {
        /*
         * Checking validity of mEnv in case the thread was detached elsewhere.
         */
        if(AndroidRuntime_getJNIEnv() != mEnv){
            ALOGD("[%s][%d] AndroidRuntime_getJNIEnv != mEnv", __FUNCTION__, __LINE__);
        }
        return mEnv;
    }
private:
    JNIEnv* mEnv = nullptr;
};

thread_local std::unique_ptr<IptvMediaJniThreadAttach> tJniThreadAttacher;

static JNIEnv* getJniEnv() {
    JNIEnv* env = AndroidRuntime_getJNIEnv();
    /*
     * If env is nullptr, the thread is not already attached to
     * JNI. It is attached below and the destructor for IptvMediaJniThreadAttach
     * will detach it on thread exit.
     */
    if (env == nullptr) {
        tJniThreadAttacher.reset(new IptvMediaJniThreadAttach());
        env = tJniThreadAttacher->getEnv();
    }
    return env;
}

struct fields_t
{
    jfieldID  context;
    jfieldID  surface_texture;
    jfieldID  device;
    jfieldID  handle;
    jfieldID  listener;

    jmethodID post_event;
};

static fields_t Playerfields;
static const char *classJavaPackage = "com/skb/btv/framework/media/core/IptvMediaPlayer";

static Mutex sPlayerLock;

enum iptv_media_action_type
{
    IPTV_ACTIONTYPE_NORMAL = 0,
    IPTV_ACTIONTYPE_OBSERVER,
};

static void checkAndClearExceptionFromCallback(JNIEnv* env, const char* methodName) {
    if (env->ExceptionCheck()) {
        ALOGD("An exception was thrown by callback '%s'.", methodName);
        env->ExceptionClear();
    }
}

class JNIIptvMediaPlayerListener: public IBtvMediaPlayerListener
{
public:
    JNIIptvMediaPlayerListener(JNIEnv* env, jobject thiz, jobject weak_thiz, int actionType);
    ~JNIIptvMediaPlayerListener();

    void notifyCallback(int32_t msg, int32_t ext1, int32_t ext2, const int8_t* data) override;
    void dataCallback(int32_t msg, int32_t ext1, int32_t ext2, const int8_t* data) override;
    void serviceDied(uint64_t cookie __unused);
private:
    JNIIptvMediaPlayerListener();
    jclass     mClass;        // Reference to IptvMediaPlayer class
    jobject    mObject;       // Weak ref to IptvMediaPlayer Java object to call on
#ifdef ANDROID_4_1_CLASS
    jobject    mParcel;
#endif
    Mutex      mLock;
    int        mActionType;
};

JNIIptvMediaPlayerListener::JNIIptvMediaPlayerListener(JNIEnv* env, jobject thiz, jobject weak_thiz, int actionType)
{

    // Hold onto the IptvMediaPlayer class for use in calling the static method
    // that posts events to the application thread.
    ALOGD("[%s] called (%d)", __FUNCTION__,__LINE__);
    jclass clazz = env->GetObjectClass(thiz);
    if (clazz == NULL)
    {
        ALOGD("Can't find %s", classJavaPackage);
        //jniThrowException(env, "java/lang/Exception", NULL);
        return;
    }
    mClass = (jclass)env->NewGlobalRef(clazz);

    // We use a weak reference so the IptvMediaPlayer object can be garbage collected.
    // The reference is only used as a proxy for callbacks.
    mObject = env->NewGlobalRef(weak_thiz);
#ifdef ANDROID_4_1_CLASS
    mParcel = env->NewGlobalRef(createJavaParcelObject(env));
#endif

    mActionType = actionType;
}

JNIIptvMediaPlayerListener::~JNIIptvMediaPlayerListener()
{
    // remote global references
    ALOGD("[%s] called(%d)", __FUNCTION__,__LINE__);
    JNIEnv *env = getJniEnv();
    env->DeleteGlobalRef(mObject);
    env->DeleteGlobalRef(mClass);

#ifdef ANDROID_4_1_CLASS
    recycleJavaParcelObject(env, mParcel);
    env->DeleteGlobalRef(mParcel);
#endif
}

void JNIIptvMediaPlayerListener::notifyCallback(int32_t msg, int32_t ext1, int32_t ext2, const int8_t* data)
{
    //ALOGD("[%s] message received msg=%d, ext1=%d, ext2=%d", __FUNCTION__, msg, ext1, ext2);
    JNIEnv *env = getJniEnv();
    if(env == NULL)
    {
        ALOGD("[%s][%d] env is NULL error! No ActionType", __FUNCTION__, __LINE__);
        return;
    }

    if(data != NULL)
    {
        size_t dataLength = strlen((const char*)data);
        const jbyte* jdata = reinterpret_cast<const jbyte*>(data);

        jbyteArray array = env->NewByteArray(dataLength);
        env->SetByteArrayRegion(array, 0, dataLength, jdata);
        if (mActionType == IPTV_ACTIONTYPE_NORMAL) {
            env->CallStaticVoidMethod(mClass, Playerfields.post_event, mObject,
                                      msg, ext1, ext2, array);
        }
        else
        {
            ALOGD("[%s][%d] error! No ActionType", __FUNCTION__, __LINE__);
        }
        env->DeleteLocalRef(array);
    }
    else
    {
        if (mActionType == IPTV_ACTIONTYPE_NORMAL)
        {
            env->CallStaticVoidMethod(mClass, Playerfields.post_event, mObject,
                                      msg, ext1, ext2, NULL);
        }
        else
        {
            ALOGD("[%s][%d] error! No ActionType", __FUNCTION__, __LINE__);
        }
    }

    checkAndClearExceptionFromCallback(env, __FUNCTION__);

    return;
}

void JNIIptvMediaPlayerListener::dataCallback(int32_t msg, int32_t ext1, int32_t ext2, const int8_t* data)
{
    Mutex::Autolock _l(mLock);
    JNIEnv *env = getJniEnv();
    if(env == NULL)
    {
        ALOGD("[%s][%d] env is NULL error! No ActionType", __FUNCTION__, __LINE__);
        return;
    }
    ALOGD("[%s] message received msg=%d, ext1=%d, ext2=%d", __FUNCTION__, msg, ext1, ext2);

    if(data != NULL)
    {
        size_t dataLength = strlen((const char*)data);
        const jbyte* jdata = reinterpret_cast<const jbyte*>(data);

        jbyteArray array = env->NewByteArray(dataLength);
        env->SetByteArrayRegion(array, 0, dataLength, jdata);
        if (mActionType == IPTV_ACTIONTYPE_NORMAL)
        {
            env->CallStaticVoidMethod(mClass, Playerfields.post_event, mObject,
                                      msg, ext1, ext2, array);
        }
        else
        {
            ALOGD("[%s][%d] error! No ActionType", __FUNCTION__, __LINE__);
        }
        env->DeleteLocalRef(array);
    }

    checkAndClearExceptionFromCallback(env, __FUNCTION__);
    return ;

}

void JNIIptvMediaPlayerListener::serviceDied(uint64_t cookie __unused)
{
    ALOGD("[%s] IptvMediaPlayer Service died unexpectedly", __FUNCTION__);
    JNIEnv *env = getJniEnv();
    if(env == NULL)
    {
        ALOGD("[%s][%d] env is NULL error!", __FUNCTION__, __LINE__);
        return;
    }

    ALOGD("[%s] CallStaticVoidMethod disconnected service", __FUNCTION__);

    env->CallStaticVoidMethod(mClass, Playerfields.post_event, mObject, 7, 0, 0, NULL);

    ALOGD("[%s] CallStaticVoidMethod disconnected service end", __FUNCTION__);
    checkAndClearExceptionFromCallback(env, __FUNCTION__);
}

static sp<JNIIptvMediaPlayerListener> getIptvMediaPlayerListener(JNIEnv* env, jobject thiz)
{
    Mutex::Autolock l(sPlayerLock);
    JNIIptvMediaPlayerListener *const p = (JNIIptvMediaPlayerListener*)env->GetIntField(thiz, Playerfields.listener);

    ALOGD("[%s] listener:%p called", __FUNCTION__, p);

    return sp<JNIIptvMediaPlayerListener>(p);
}

static sp<JNIIptvMediaPlayerListener> setIptvMediaPlayerListener(JNIEnv* env, jobject thiz, const sp<JNIIptvMediaPlayerListener>& listener)
{
    Mutex::Autolock l(sPlayerLock);
    sp<JNIIptvMediaPlayerListener> old = (JNIIptvMediaPlayerListener*)env->GetIntField(thiz, Playerfields.listener);
#if 1
    if (listener.get())
    {
        listener->incStrong(thiz);
    }
    if (old != 0)
    {
        old->decStrong(thiz);
    }
    #ifdef _WIN64
    env->SetIntField(thiz, Playerfields.listener, (int64_t)listener.get()); //for 64bit build
    #else
    env->SetIntField(thiz, Playerfields.listener, (int)listener.get());
    #endif
#endif
    ALOGD("[%s] listener:%p called(%d)", __FUNCTION__, listener.get(),__LINE__);
    return old;
}


static jint tuneTv(JNIEnv *env, jobject thiz, jstring xml) {
    ALOGD("[%s] called", __FUNCTION__);

    if(xml == NULL) {
        ALOGD("invalid argument is NULL ");
        return -1;
    }

    const char *tmpXml = env->GetStringUTFChars(xml, NULL);
    if (tmpXml == NULL)
        return -1;

    auto btvmedia = BtvMediaPlayerService::getInstance();
    btvmedia->tuneTV(0, tmpXml);

    return 1;
}


static void media_IptvMediaPlayer_native_init(JNIEnv *env, jclass clazz)
{
    ALOGD("[%s] called", __FUNCTION__);

    Playerfields.context = env->GetFieldID(clazz, "mNativeContext", "I");
    if (Playerfields.context == NULL)
    {
        ALOGD("[%s][%d] Playerfields.context == NULL", __FUNCTION__, __LINE__);
        return;
    }

    Playerfields.post_event = env->GetStaticMethodID(clazz, "postEventFromNative",
                                                     "(Ljava/lang/Object;IIILjava/lang/Object;)V");
    if (Playerfields.post_event == NULL)
    {
        ALOGD("[%s][%d] Playerfields.post_event == NULL", __FUNCTION__, __LINE__);
        return;
    }

    Playerfields.surface_texture = env->GetFieldID(clazz, "mNativeSurfaceTexture", "I");
    if (Playerfields.surface_texture == NULL)
    {
        ALOGD("[%s][%d] Playerfields.surface_texture == NULL", __FUNCTION__, __LINE__);
        return;
    }

    Playerfields.device = env->GetFieldID(clazz, "mDeviceId", "I");
    if (Playerfields.device == NULL)
    {
        ALOGD("[%s][%d] Playerfields.device == NULL", __FUNCTION__, __LINE__);
        return;
    }

    Playerfields.handle = env->GetFieldID(clazz, "mNativeHandle", "I");
    if (Playerfields.handle == NULL)
    {
        ALOGD("[%s][%d] Playerfields.handle == NULL", __FUNCTION__, __LINE__);
        return;
    }

    Playerfields.listener = env->GetFieldID(clazz, "mListener", "I");
    if (Playerfields.listener == NULL)
    {
        ALOGD("[%s][%d] Playerfields.listener == NULL", __FUNCTION__, __LINE__);
        return;
    }

}

static void media_IptvMediaPlayer_native_setup(JNIEnv *env, jobject thiz, jobject weak_this, jint deviceID, jstring internalPath)
{
    ALOGD("[%s] called", __FUNCTION__);

    if (deviceID < 0)
    {
        ALOGD("[%s][%d] Failed to connect to IptvMedia Service", __FUNCTION__, __LINE__);
        return;
    }

    if (internalPath == NULL)
    {
        ALOGD("[%s][%d] IllegalArgumentException", __FUNCTION__, __LINE__);
        //jniThrowException(env, "java/lang/IllegalArgumentException", NULL);
        return;
    }

    const char *tmp = env->GetStringUTFChars(internalPath, NULL);
    if (tmp == NULL)
    {
        return;
    }
    ALOGD("[%s][%d] open: \n		internalPath => %s", __FUNCTION__, __LINE__, tmp);

    set_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, tmp, 64);
    env->ReleaseStringUTFChars(internalPath, tmp);
    tmp = NULL;
    int iptv_mp = 0;
    auto btvmedia = BtvMediaPlayerService::getInstance();

    if(btvmedia == NULL){
        ALOGD("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
        return ;
    }

    ALOGD("[%s] deviceID => [%d]", __FUNCTION__, deviceID);

    if(deviceID == BTV_DEVICE_MAIN) {
        iptv_mp = btvmedia->connect(BTV_DEVICE_MAIN);
        ALOGD("[%s][%d] connect to MAIN IptvMedia Service", __FUNCTION__, __LINE__);
    } else {
        iptv_mp = btvmedia->connect(BTV_DEVICE_PIP);
        ALOGD("[%s][%d] connect to PIP IptvMedia Service", __FUNCTION__, __LINE__);
    }

    sp<JNIIptvMediaPlayerListener> listener = new JNIIptvMediaPlayerListener(env, thiz, weak_this, IPTV_ACTIONTYPE_NORMAL);

    if (listener != NULL)
    {
        setIptvMediaPlayerListener(env, thiz, listener);
        btvmedia->setBtvMediaPlayerListener(listener);
    }
    ALOGD("[%s] called end", __FUNCTION__);
}

static jint media_IptvMediaPlayer_filePlay(JNIEnv *env, jobject thiz, jobject jsurface, jstring path, jint vpid, jint vcodec, jint apid, jint acodec)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if (btvmedia == NULL)
    {
        ALOGD("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
        //jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return -1;
    }

    if (path == NULL)
    {
        ALOGD("[%s][%d] IllegalArgumentException", __FUNCTION__, __LINE__);
        //jniThrowException(env, "java/lang/IllegalArgumentException", NULL);
        return -1;
    }

    const char *tmp = env->GetStringUTFChars(path, NULL);
    if (tmp == NULL)
    {
        return -1;
    }
    ALOGD("[%s][%d] open: \n		dataXML => %s", __FUNCTION__, __LINE__, tmp);

    env->ReleaseStringUTFChars(path, tmp);
    tmp = NULL;

    status_t opStatus = 0;//iptv_mp->filePlay(new_st, pathStr, vpid, vcodec, apid, acodec);
    return (int)opStatus;

    return 0;
}

static jint media_IptvMediaPlayer_open(JNIEnv *env, jobject thiz, jstring dataXML)
{
    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL){
        ALOGD("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
        return -1;
    }

    if (dataXML == NULL)
    {
        ALOGD("[%s][%d] IllegalArgumentException", __FUNCTION__, __LINE__);
        //jniThrowException(env, "java/lang/IllegalArgumentException", NULL);
        return -1;
    }

    const char *dataXMLStr = env->GetStringUTFChars(dataXML, NULL);

    if (dataXMLStr == NULL) 	// Out of memory
    {
        return -1;
    }

    auto opStatus = btvmedia->open(0, dataXMLStr);
    if (opStatus != 0) {
        ALOGD("[%s] calling of open() fail %d", __FUNCTION__, opStatus);
        env->ReleaseStringUTFChars(dataXML, dataXMLStr);
        dataXMLStr = NULL;
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        env->ReleaseStringUTFChars(dataXML, dataXMLStr);
        dataXMLStr = NULL;
        return 0;
    }

    return 0;
}

static jint media_IptvMediaPlayer_tuneTV(JNIEnv *env, jobject thiz, jstring dataXML)
{
    ALOGD("[%s] called", __FUNCTION__);

    if(dataXML == NULL) {
        ALOGD("invalid argument is NULL ");
        return -1;
    }

    const char *tmpXml = env->GetStringUTFChars(dataXML, NULL);
    if (tmpXml == NULL)
        return -1;
    ALOGD("[%s][%d] tuneTV: \n 			dataXML => %s", __FUNCTION__, __LINE__, tmpXml);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    auto opStatus = btvmedia->tuneTV(0, tmpXml);
    if(opStatus != 0) {
        ALOGD("[%s] calling of tuneTV() fail %d", __FUNCTION__ , opStatus);
        env->ReleaseStringUTFChars(dataXML, tmpXml);
		tmpXml = NULL;		
        //sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);		
		//if(listener != NULL) listener->serviceDied(0, nullptr);
        //return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        env->ReleaseStringUTFChars(dataXML, tmpXml);
		tmpXml = NULL;		
        //return 0;
    }

    return 1;
}

static jint media_IptvMediaPlayer_closeTV(JNIEnv *env, jobject thiz)
{
    ALOGD("[%s] called", __FUNCTION__);
    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL){
        ALOGD("[%s][%d] IllegalStateException", __FUNCTION__, __LINE__);
        return -1;
    }
    auto opStatus = btvmedia->closeTV(0);
    if (opStatus != 0) {
        ALOGD("[%s] calling of closeTV() fail %d", __FUNCTION__ , opStatus);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        return 0;
    }

    return 0;
}

static jint media_IptvMediaPlayer_bindingFilter(JNIEnv *env, jobject thiz, jstring dataXML)
{
    ALOGD("[%s] called", __FUNCTION__);

    if(dataXML == NULL) {
        ALOGD("invalid argument is NULL ");
        return -1;
    }

    const char *tmpXml = env->GetStringUTFChars(dataXML, NULL);
    if (tmpXml == NULL)
        return -1;

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }

    auto opStatus = btvmedia->bindingFilter(0, tmpXml);
    if(opStatus != 0) {
        ALOGD("[%s] calling of bindingFilter() fail %d", __FUNCTION__ , opStatus);
        env->ReleaseStringUTFChars(dataXML, tmpXml);
        tmpXml = NULL;
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        env->ReleaseStringUTFChars(dataXML, tmpXml);
        tmpXml = NULL;
        return 0;
    }
}

static jint media_IptvMediaPlayer_releaseFilter(JNIEnv *env, jobject thiz, jstring dataXML)
{
    ALOGD("[%s] called", __FUNCTION__);

    if(dataXML == NULL) {
        ALOGD("invalid argument is NULL ");
        return -1;
    }

    const char *tmpXml = env->GetStringUTFChars(dataXML, NULL);
    if (tmpXml == NULL)
        return -1;

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->releaseFilter(0, tmpXml);
    if(opStatus != 0) {
        ALOGD("[%s] calling of releaseFilter() fail %d", __FUNCTION__ , opStatus);
        env->ReleaseStringUTFChars(dataXML, tmpXml);
        tmpXml = NULL;
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        env->ReleaseStringUTFChars(dataXML, tmpXml);
        tmpXml = NULL;
        return 0;
    }

    return 1;
}

static jint media_IptvMediaPlayer_changeAudioChannel(JNIEnv *env, jobject thiz, jstring dataXML)
{
    ALOGD("[%s] called", __FUNCTION__);

    if(dataXML == NULL) {
        ALOGD("invalid argument is NULL ");
        return -1;
    }

    const char *tmpXml = env->GetStringUTFChars(dataXML, NULL);
    if (tmpXml == NULL)
        return -1;

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->changeAudioChannel(0, tmpXml);
    if(opStatus != 0) {
        ALOGD("[%s] calling of changeAudioChannel() fail %d", __FUNCTION__ , opStatus);
        env->ReleaseStringUTFChars(dataXML, tmpXml);
        tmpXml = NULL;
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        env->ReleaseStringUTFChars(dataXML, tmpXml);
        tmpXml = NULL;
        return 0;
    }

    return 1;
}

static jint media_IptvMediaPlayer_changeAudioOnOff(JNIEnv *env, jobject thiz, jstring dataXML)
{
    ALOGD("[%s] called", __FUNCTION__);

    if(dataXML == NULL) {
        ALOGD("invalid argument is NULL ");
        return -1;
    }

    const char *tmpXml = env->GetStringUTFChars(dataXML, NULL);
    if (tmpXml == NULL)
        return -1;

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }

    auto opStatus = btvmedia->changeAudioOnOff(0, tmpXml);
    if(opStatus != 0) {
        ALOGD("[%s] calling of changeAudioOnOff() fail %d", __FUNCTION__ , opStatus);
        env->ReleaseStringUTFChars(dataXML, tmpXml);
        tmpXml = NULL;
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        env->ReleaseStringUTFChars(dataXML, tmpXml);
        tmpXml = NULL;
        return 0;
    }

    return 1;
}

static jint media_IptvMediaPlayer_getPlayerMode(JNIEnv *env, jobject thiz)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    int cur_mode = 0;
    auto opStatus = btvmedia->getPlayerMode(0/*[&](Status retval, int mode) {
        cur_mode = mode;
        if (retval != Status::OK) {
            log_PrintEx("[%s][%d] Current PlayerMode: null", __FUNCTION__, __LINE__);
            return;
        }
        log_PrintEx("[%s][%d] Current PlayerMode: %d", __FUNCTION__, __LINE__, mode);
    }*/);
    if (opStatus != 0) {
        ALOGD("[%s] calling of getPlayerMode() fail %d", __FUNCTION__ , opStatus);
        //setIptvMediaPlayer(env, thiz, NULL);
        //sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
        //if(listener != NULL) listener->serviceDied(0, nullptr);
        return -1;
    }
    return cur_mode;

    return 1;
}

static jint media_IptvMediaPlayer_getPlayerStatus(JNIEnv *env, jobject thiz)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    int cur_status = 0;
    auto opStatus = btvmedia->getPlayerStatus(0/*[&](Status retval, int status) {
        cur_status = status;
        if (retval != Status::OK) {
            ALOGD("[%s][%d] Current PlayerStatus: null", __FUNCTION__, __LINE__);
            return;
        }
        ALOGD("[%s][%d] Current PlayerStatus: %d", __FUNCTION__, __LINE__, status);
    }*/);

    if (opStatus != 0) {
        ALOGD("[%s] calling of getPlayerStatus() fail %d", __FUNCTION__ , opStatus);
        //setIptvMediaPlayer(env, thiz, NULL);
        //sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
        //if(listener != NULL) listener->serviceDied(0, nullptr);
        return -1;
    }

    return cur_status;
}

static jint media_IptvMediaPlayer_getCurrentPositionInSec(JNIEnv *env, jobject thiz)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    int cur_pos = 0;
    auto opStatus = btvmedia->getCurrentPosition(0/*[&](Status retval, int pos) {
        cur_pos = pos;
        if (retval != Status::OK) {
            ALOGD("[%s][%d] Current PlayerCurrentPosition: null", __FUNCTION__, __LINE__);
            return;
        }
        ALOGD("[%s][%d] Current PlayerCurrentPosition: %d", __FUNCTION__, __LINE__, pos);
    }*/);

    if (opStatus != 0) {
        ALOGD("[%s] calling of getCurrentPosition() fail %d", __FUNCTION__ , opStatus);
        return -1;
    }

    return cur_pos;
}

static jint media_IptvMediaPlayer_setWindowSize(JNIEnv *env, jobject thiz, jstring dataXML)
{
    ALOGD("[%s] called", __FUNCTION__);

    if(dataXML == NULL) {
        ALOGD("invalid argument is NULL ");
        return -1;
    }

    const char *tmpXml = env->GetStringUTFChars(dataXML, NULL);
    if (tmpXml == NULL)
        return -1;

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }

    auto opStatus = btvmedia->setWindowSize(0, tmpXml);
    if(opStatus != 0) {
        ALOGD("[%s] calling of setWindowSize() fail %d", __FUNCTION__ , opStatus);
        env->ReleaseStringUTFChars(dataXML, tmpXml);
        tmpXml = NULL;
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        env->ReleaseStringUTFChars(dataXML, tmpXml);
        tmpXml = NULL;
        return 0;
    }
}

static jint media_IptvMediaPlayer_play(JNIEnv *env, jobject thiz)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->play(0);
    if (opStatus != 0) {
        ALOGD("[%s] calling of play() fail %d", __FUNCTION__ , opStatus);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint media_IptvMediaPlayer_pause(JNIEnv *env, jobject thiz)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->pause(0);
    if (opStatus != 0) {
        ALOGD("[%s] calling of pause() fail %d", __FUNCTION__ , opStatus);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint
media_IptvMediaPlayer_resume(JNIEnv *env, jobject thiz)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->resume(0);
    if (opStatus != 0) {
        ALOGD("[%s] calling of resume() fail %d", __FUNCTION__ , opStatus);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint media_IptvMediaPlayer_seek(JNIEnv *env, jobject thiz, jstring dataXML, jboolean pause)
{
    ALOGD("[%s] called", __FUNCTION__);

    if(dataXML == NULL) {
        ALOGD("invalid argument is NULL ");
        return -1;
    }

    const char *tmpXml = env->GetStringUTFChars(dataXML, NULL);
    if (tmpXml == NULL)
        return -1;

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->seek(0, tmpXml, pause);
    if (opStatus != 0) {
        ALOGD("[%s] calling of seek() fail %d", __FUNCTION__ , opStatus);
        env->ReleaseStringUTFChars(dataXML, tmpXml);
        tmpXml = NULL;
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        env->ReleaseStringUTFChars(dataXML, tmpXml);
        tmpXml = NULL;
        return 0;
    }
}

static jint media_IptvMediaPlayer_trick(JNIEnv *env, jobject thiz, jstring dataXML)
{
    ALOGD("[%s] called", __FUNCTION__);

    if(dataXML == NULL) {
        ALOGD("invalid argument is NULL ");
        return -1;
    }

    const char *tmpXml = env->GetStringUTFChars(dataXML, NULL);
    if (tmpXml == NULL)
        return -1;

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->trick(0, tmpXml);
    if (opStatus != 0) {
        ALOGD("[%s] calling of trick() fail %d", __FUNCTION__ , opStatus);
        env->ReleaseStringUTFChars(dataXML, tmpXml);
        tmpXml = NULL;
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        env->ReleaseStringUTFChars(dataXML, tmpXml);
        tmpXml = NULL;
        return 0;
    }
}

static jint media_IptvMediaPlayer_stop(JNIEnv *env, jobject thiz)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->stop(0);
    if (opStatus != 0) {
        ALOGD("[%s] calling of stop() fail %d", __FUNCTION__ , opStatus);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint media_IptvMediaPlayer_close(JNIEnv *env, jobject thiz)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->close(0);
    if (opStatus != 0) {
        ALOGD("[%s] calling of close() fail %d", __FUNCTION__, opStatus);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        return 0;
    }
}

#if 1
static ANativeWindow *getIptvVideoSurfaceTexture(JNIEnv* env, jobject thiz)
{
    ANativeWindow* p = (ANativeWindow*)env->GetIntField(thiz, Playerfields.surface_texture);
    return p;
}

static void decIptvVideoSurfaceRef(JNIEnv *env, jobject thiz)
{
    ANativeWindow *old_st = getIptvVideoSurfaceTexture(env, thiz);
    if (old_st != NULL)
    {
        ALOGD("[%s] Releasing window", __FUNCTION__);
        ANativeWindow_release(old_st);
        env->SetIntField(thiz, Playerfields.surface_texture, 0);
    }
    else
    {
        //ALOGD("[%s] Prev window NULL!!", __FUNCTION__);
    }
}
#endif

static void media_IptvMediaPlayer_setIptvVideoSurface(JNIEnv *env, jobject thiz, jobject jsurface)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        ALOGD("[%s][%d] getInstance fail", __FUNCTION__, __LINE__);
        return;
    }
    //sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz); //for 64bit build
    //if (listener != NULL)
    //{
        //auto opSetStatus  = btvmedia->setListener(listener);
        //if (!opSetStatus.isOk() && opSetStatus.isDeadObject()) {
            //ALOGD("[%s] calling of setListener() fail %s", __FUNCTION__, opSetStatus.description().c_str());
            //setIptvMediaPlayer(env, thiz, NULL);
			//if(listener != NULL) listener->serviceDied(0, nullptr); 		
         //   return;
        //}
    //}

    decIptvVideoSurfaceRef(env, thiz);

    ANativeWindow *window = ANativeWindow_fromSurface(env, jsurface);
    if(window)
    {
        int width = ANativeWindow_getWidth(window);
        int height = ANativeWindow_getHeight(window);
        ALOGD("Got window %p [%d x %d]", window, width, height);
    } else {
        ALOGD("[%s] java/lang/IllegalArgumentException , The surface has been released", __FUNCTION__);
        //jniThrowException(env, "java/lang/IllegalArgumentException", "The surface has been released");
        return;
    }

    //std::shared_ptr<ANativeWindow> nativeWindow(window);
    //sp<ANativeWindow> nativeWindow(window);
    //env->SetIntField(thiz, Playerfields.surface_texture, (int)nativeWindow.get());

    //int device = (int)env->GetIntField(thiz, Playerfields.device);


#if 0
    //sp<IGraphicBufferProducer> tProducer;
    sp<Surface> surface;
    if (jsurface) {
        surface = android_view_Surface_getSurface(env, jsurface);
        if (surface != NULL) {
            //using TWGraphicBufferProducer = ::android::TWGraphicBufferProducer<::android::hardware::graphics::bufferqueue::V1_0::IGraphicBufferProducer>;
            //sp<TWGraphicBufferProducer> tProducer = new TWGraphicBufferProducer(surface->getIGraphicBufferProducer());
            //tProducer = surface->getIGraphicBufferProducer());

            auto opStatus = 0;//btvmedia->setPlayerSurface(0, surface);
            if (opStatus != 0) {
                //ALOGD("[%s] calling of setPlayerSurface() fail %s", __FUNCTION__ , opStatus.description().c_str());
                //setIptvMediaPlayer(env, thiz, NULL);
                sp<JNIIptvMediaPlayerListener> listener = getIptvMediaPlayerListener(env, thiz);
                //if(listener != NULL) listener->serviceDied(0, nullptr);
            } else {
                ALOGD("[%s] called end", __FUNCTION__);
            }
        }
    }
#endif

    btvmedia->setPlayerSurfaceView(0, (uint64_t)window); //for 64bit build
    //btvmedia->setPlayerSurfaceView(0, (uint32_t)window);
    ALOGD("[%s] called end", __FUNCTION__);
}

static jint media_IptvMediaPlayer_setPlayerSize(JNIEnv *env, jobject thiz, jint nLeft, jint nTop, jint nWidth, jint nHeight)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->setPlayerSize(0, nLeft, nTop, nWidth, nHeight);
    if (opStatus != 0) {
        ALOGD("[%s] calling of setPlayerSize() fail %d", __FUNCTION__ , opStatus);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint media_IptvMediaPlayer_startDmxFilter(JNIEnv *env, jobject thiz, jint pid, jint tid)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->startDmxFilter(0, pid, tid);
    if (opStatus != 0) {
        ALOGD("[%s] calling of startDmxFilter() fail %d", __FUNCTION__ , opStatus);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint media_IptvMediaPlayer_stopDmxFilter(JNIEnv *env, jobject thiz, jint pid, jint tid)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->stopDmxFilter(0, pid, tid);
    if (opStatus != 0) {
        ALOGD("[%s] calling of stopDmxFilter() fail %d", __FUNCTION__ , opStatus);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint media_IptvMediaPlayer_setDummys(JNIEnv *env, jobject thiz, jstring dummys)
{
    ALOGD("[%s] called", __FUNCTION__);

    if(dummys == NULL) {
        ALOGD("invalid argument is NULL ");
        return -1;
    }

    const char *tmpXml = env->GetStringUTFChars(dummys, NULL);
    if (tmpXml == NULL)
        return -1;

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }

    auto opStatus = btvmedia->setDummys(0, tmpXml);
    if (opStatus != 0) {
        ALOGD("[%s] calling of setDummys() fail %d", __FUNCTION__ , opStatus);
        env->ReleaseStringUTFChars(dummys, tmpXml);
        tmpXml = NULL;
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        env->ReleaseStringUTFChars(dummys, tmpXml);
        tmpXml = NULL;
        return 0;
    }
}

static void media_IptvMediaPlayer_release(JNIEnv *env, jobject thiz)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return;
    }
    btvmedia->disconnect(BTV_DEVICE_MAIN);
    ALOGD("[%s] called end", __FUNCTION__);
}

static jint media_IptvMediaPlayer_native_invoke(JNIEnv *env, jobject thiz, jobject java_request, jobject java_reply)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }

    return 1;
}

static jint media_IptvMediaPlayer_pauseAt(JNIEnv *env, jobject thiz, jstring dataXML)
{
    ALOGD("[%s] called", __FUNCTION__);

    if(dataXML == NULL) {
        ALOGD("invalid argument is NULL ");
        return -1;
    }

    const char *tmpXml = env->GetStringUTFChars(dataXML, NULL);
    if (tmpXml == NULL)
        return -1;

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->pauseAt(0, tmpXml);
    if (opStatus != 0) {
        ALOGD("[%s] calling of pauseAt() fail %d", __FUNCTION__ , opStatus);
        env->ReleaseStringUTFChars(dataXML, tmpXml);
        tmpXml = NULL;
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        env->ReleaseStringUTFChars(dataXML, tmpXml);
        tmpXml = NULL;
        return 0;
    }
}

static jint media_IptvMediaPlayer_keepLastFrame(JNIEnv *env, jobject thiz, jboolean flag)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->keepLastFrame(0, flag);
    if (opStatus != 0) {
        ALOGD("[%s] calling of pauseAt() fail %d", __FUNCTION__ , opStatus);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint media_IptvMediaPlayer_setStbId(JNIEnv *env, jobject thiz, jstring stbId)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
//stbid : {69AF56BB-BA9A-11EA-AB15-C57A43B26EA7}
    if(stbId == NULL) {
        ALOGD("invalid argument is NULL ");
        return -1;
    }

    const char *tmpXml = env->GetStringUTFChars(stbId, NULL);
    if (tmpXml == NULL)
        return -1;
    auto opStatus = btvmedia->setStbId(tmpXml);
    if (opStatus != 0) {
        ALOGD("[%s] calling of setStbId() fail %d", __FUNCTION__ , opStatus);
        env->ReleaseStringUTFChars(stbId, tmpXml);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        env->ReleaseStringUTFChars(stbId, tmpXml);
        return 0;
    }
}

static jint media_IptvMediaPlayer_setMacAddress(JNIEnv *env, jobject thiz, jstring macAddr)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
//MAC : 30:EB:25:1D:56:24
    if(macAddr == NULL) {
        ALOGD("invalid argument is NULL ");
        return -1;
    }

    const char *tmpXml = env->GetStringUTFChars(macAddr, NULL);
    if (tmpXml == NULL)
        return -1;

    auto opStatus = btvmedia->setMacAddress(tmpXml);
    if (opStatus != 0) {
        ALOGD("[%s] calling of setMacAddress() fail %d", __FUNCTION__ , opStatus);
        env->ReleaseStringUTFChars(macAddr, tmpXml);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        env->ReleaseStringUTFChars(macAddr, tmpXml);
        return 0;
    }
}

static jint media_IptvMediaPlayer_setGwToken(JNIEnv *env, jobject thiz, jstring gwToken)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }

    if(gwToken == NULL) {
        ALOGD("invalid argument is NULL ");
        return -1;
    }

    const char *tmpXml = env->GetStringUTFChars(gwToken, NULL);
    if (tmpXml == NULL)
        return -1;

    auto opStatus = btvmedia->setGwToken(tmpXml);
    if (opStatus != 0) {
        ALOGD("[%s] calling of setGwToken() fail %d", __FUNCTION__ , opStatus);
        env->ReleaseStringUTFChars(gwToken, tmpXml);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        env->ReleaseStringUTFChars(gwToken, tmpXml);
        return 0;
    }
}

static jint media_IptvMediaPlayer_setVisionImpaired(JNIEnv *env, jobject thiz, jboolean visionImpaired)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->setVisionImpaired(visionImpaired);
    if (opStatus != 0) {
        ALOGD("[%s] calling of setVisionImpaired() fail %d", __FUNCTION__ , opStatus);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint media_IptvMediaPlayer_setHearingImpaired(JNIEnv *env, jobject thiz, jboolean hearingImpaired)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->setHearingImpaired(hearingImpaired);
    if (opStatus != 0) {
        ALOGD("[%s] calling of setHearingImpaired() fail %d", __FUNCTION__ , opStatus);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint media_IptvMediaPlayer_setVideoScalingMode(JNIEnv *env, jobject thiz, jint scalingMode)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->setVideoScalingMode(scalingMode);
    if (opStatus != 0) {
        ALOGD("[%s] calling of setVideoScalingMode() fail %d", __FUNCTION__ , opStatus);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jboolean media_IptvMediaPlayer_getMlrActivationEnable(JNIEnv *env, jobject thiz)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->getMlrActivationEnable();
    ALOGD("[%s] calling of getMlrActivationEnable() return %d", __FUNCTION__ , opStatus);
    return opStatus;
}

static jint media_IptvMediaPlayer_setMlrActivationEnable(JNIEnv *env, jobject thiz, jboolean mlractivationEnable)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->setMlrActivationEnable(mlractivationEnable);
    if (opStatus != 0) {
        ALOGD("[%s] calling of setMlrActivationEnable() fail %d", __FUNCTION__ , opStatus);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        return 0;
    }
}

static jint media_IptvMediaPlayer_setTtaTestOption(JNIEnv *env, jobject thiz, jstring key, jstring value)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }

    if(key == NULL || value == NULL) {
        ALOGD("invalid argument is NULL ");
        return -1;
    }

    const char *tmpKey = env->GetStringUTFChars(key, NULL);
    if (tmpKey == NULL)
        return -1;

    const char *tmpValue = env->GetStringUTFChars(value, NULL);
    if (tmpValue == NULL)
        return -1;

    auto opStatus = btvmedia->setTtaTestOption(tmpKey, tmpValue);
    if (opStatus != 0) {
        ALOGD("[%s] calling of setTtaTestOption() fail %d", __FUNCTION__ , opStatus);
        env->ReleaseStringUTFChars(key, tmpKey);
        env->ReleaseStringUTFChars(value, tmpValue);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        env->ReleaseStringUTFChars(key, tmpKey);
        env->ReleaseStringUTFChars(value, tmpValue);
        return 0;
    }
}

static jint media_IptvMediaPlayer_setAVOffset(JNIEnv *env, jobject thiz, jint type, jint nTimeMs)
{
    ALOGD("[%s] called", __FUNCTION__);

    auto btvmedia = BtvMediaPlayerService::getInstance();
    if(btvmedia == NULL) {
        return -1;
    }
    auto opStatus = btvmedia->setAVOffset(type, nTimeMs);
    if (opStatus != 0) {
        ALOGD("[%s] calling of stopDmxFilter() fail %d", __FUNCTION__ , opStatus);
        return -1;
    } else {
        ALOGD("[%s] called end", __FUNCTION__);
        return 0;
    }
}
// ------------------------------------------------------------------------------------------------


#define NELEM(x) ((int) (sizeof(x) / sizeof((x)[0])))

// FIMXME : 사용할 API 정의
static JNINativeMethod gMethods[] = {
        {"native_init", "()V", (void*)media_IptvMediaPlayer_native_init},
        {"native_setup", "(Ljava/lang/Object;ILjava/lang/String;)V", (void*)media_IptvMediaPlayer_native_setup},
        {"_filePlay", "(Landroid/view/Surface;Ljava/lang/String;IIII)I", (void *)media_IptvMediaPlayer_filePlay},
        {"_open", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_open},
        {"_tuneTV", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_tuneTV},
        {"_closeTV", "()I", (void *)media_IptvMediaPlayer_closeTV},
        {"_bindingFilter", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_bindingFilter},
        {"_releaseFilter", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_releaseFilter},
        {"_changeAudioChannel", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_changeAudioChannel},
        {"_changeAudioOnOff", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_changeAudioOnOff},
        {"_getPlayerMode", "()I", (void *)media_IptvMediaPlayer_getPlayerMode},
        {"_getPlayerStatus", "()I", (void *)media_IptvMediaPlayer_getPlayerStatus},
        {"_getCurrentPositionInSec", "()I", (void *)media_IptvMediaPlayer_getCurrentPositionInSec},
        {"_setWindowSize", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_setWindowSize},
        {"_play", "()I", (void *)media_IptvMediaPlayer_play},
        {"_pause", "()I", (void *)media_IptvMediaPlayer_pause},
        {"_resume", "()I", (void *)media_IptvMediaPlayer_resume},
        {"_seek", "(Ljava/lang/String;Z)I", (void *)media_IptvMediaPlayer_seek},
        {"_trick", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_trick},
        {"_stop", "()I", (void *)media_IptvMediaPlayer_stop},
        {"_close", "()I", (void *)media_IptvMediaPlayer_close},
        {"_setIptvVideoSurface", "(Landroid/view/Surface;)V", (void *)media_IptvMediaPlayer_setIptvVideoSurface},
        {"_setPlayerSize", "(IIII)I", (void *)media_IptvMediaPlayer_setPlayerSize},
        {"_startDmxFilter", "(II)I", (void *)media_IptvMediaPlayer_startDmxFilter},
        {"_stopDmxFilter", "(II)I", (void *)media_IptvMediaPlayer_stopDmxFilter},
        {"_setDummys", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_setDummys},
        {"native_release", "()V", (void *)media_IptvMediaPlayer_release},
        {"native_invoke", "(Landroid/os/Parcel;Landroid/os/Parcel;)I", (void *)media_IptvMediaPlayer_native_invoke},
        {"_pauseAt", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_pauseAt},
        {"_keepLastFrame", "(Z)I", (void *)media_IptvMediaPlayer_keepLastFrame},
        {"_setStbId", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_setStbId},
        {"_setMacAddress", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_setMacAddress},
        {"_setGwToken", "(Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_setGwToken},
        {"_setVisionImpaired", "(Z)I", (void *)media_IptvMediaPlayer_setVisionImpaired},
        {"_setHearingImpaired", "(Z)I", (void *)media_IptvMediaPlayer_setHearingImpaired},
        {"_setVideoScalingMode", "(I)I", (void *)media_IptvMediaPlayer_setVideoScalingMode},
        {"_getMlrActivationEnable", "()Z", (void *)media_IptvMediaPlayer_getMlrActivationEnable},
        {"_setMlrActivationEnable", "(Z)I", (void *)media_IptvMediaPlayer_setMlrActivationEnable},
        {"_setTtaTestOption", "(Ljava/lang/String;Ljava/lang/String;)I", (void *)media_IptvMediaPlayer_setTtaTestOption},
        {"_setAVOffset", "(II)I", (void *)media_IptvMediaPlayer_setAVOffset},
};

jint JNI_OnLoad(JavaVM* vm, void* /* reserved */)
{
    JNIEnv* env = NULL;
    jint result = -1;

    if (vm->GetEnv((void**)&env, JNI_VERSION_1_4) != JNI_OK)
    {
        ALOGD("[%s][%d] ERROR: GetEnv falied", __func__, __LINE__);
        return JNI_FALSE;
    }

    jclass clazz = env->FindClass(classJavaPackage);
    if (clazz == NULL) {
        ALOGD("[%s][%d] ERROR: FindClass failed", __func__, __LINE__);
        return JNI_FALSE;
    }

    if (env->RegisterNatives(clazz, gMethods, NELEM(gMethods)) < 0) {
        ALOGD("[%s][%d] ERROR: native registration failed", __func__, __LINE__);
        return JNI_FALSE;
    }
    gJavaVM = vm;
    result = JNI_VERSION_1_4;
    return result;
}