// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

//#define LONG_NDEBUG 0
//#define LOG_TAG "BtvMediaPlayerService"
#define LOG_TAG "BOXKEY-BtvMediaPlayerService"

#include <ALog.h>

#include <dvb_primitive.h>
#include <hal/iptvhaldef.h>

#include "BtvMediaPlayerService.h"
#include <IptvSocPlayer.h>
#include <iptvmedia/BtvMediaPlayerObserver.h>

#include <android/native_window_jni.h>
#include <android/native_window.h>
#include <systemproperty.h>

//#include <android_runtime/android_view_Surface.h>

static void signal_handler(int signal_value)
{
    ALOGE("[%s] called", __FUNCTION__);
    BtvMediaPlayerService::getInstance()->closeTV(0);
}

/**
 * @brief  Ctrl+C, or Kill signal handle setting
 * @return void
 */
static void catch_signals(void)
{
    ALOGE("[%s] called set signal_handler", __FUNCTION__);
    struct sigaction action;
    action.sa_handler = signal_handler;
    action.sa_flags = 0;
    sigemptyset (&action.sa_mask);
    sigaction (SIGINT, &action, NULL);
    sigaction (SIGTERM, &action, NULL);
}

std::shared_ptr<BtvMediaPlayerService> BtvMediaPlayerService::instance_ = NULL;

std::shared_ptr<BtvMediaPlayerService> BtvMediaPlayerService::getInstance() {
    if (instance_ != NULL) {
        return instance_;
    }
    std::shared_ptr<BtvMediaPlayerService> btvMediaPlayerService(new BtvMediaPlayerService);

    instance_ = btvMediaPlayerService;//std::make_shared<BtvMediaPlayerService>();
    return instance_;
}

BtvMediaPlayerService::BtvMediaPlayerService()
{
    ALOGE("BTV PLAYER Version [%s]",BTV_PLAYER_VERSION);
	ALOGD("[%s] called", __FUNCTION__);

	// musespy : IptvSocPlayer initiates in here
    mIptvSocPlayerMain = std::make_shared<IptvSocPlayer>();
    mIptvSocPlayerMain->initCheck(this, IptvSocPlayer::mBTV_DEVICE_MAIN);

    mIptvSocPlayerPip1 = std::make_shared<IptvSocPlayer>();
    mIptvSocPlayerPip1->initCheck(this, IptvSocPlayer::mBTV_DEVICE_PIP1);

    mpIptvSocPlayer = NULL;

    // TODO callback registering


    // callback registering
    mIptvSocPlayerMain->setCallbacks(this, notifyCallbackMain, dataCallbackMain);
	mIptvSocPlayerPip1->setCallbacks(this, notifyCallbackPip1, dataCallbackPip1);
#if 0

	// dsmcc callback registering
    mpIptvSocPlayer->setDsmccCallbacks(this, dsmccCallback);

	// audio program update callback registering
    mpIptvSocPlayer->setAudioProgramUpdateCallbacks(this, audioProgramUpdateCallback);
#endif
}

BtvMediaPlayerService::~BtvMediaPlayerService()
{
	ALOGD("[%s] called", __FUNCTION__);
}

// static
void BtvMediaPlayerService::notifyCallbackMain(void* cookie, int msg, int ext1, int ext2, const int8_t* obj)
{
    //ALOGD("[%s][%d] notify (%d, %d, %d)", __FUNCTION__, __LINE__, msg, ext1, ext2);

    BtvMediaPlayerService* p = static_cast<BtvMediaPlayerService*>(cookie);
    p->mBtvMediaPlayerListener->notifyCallback(msg, ext1, ext2, obj);
}

// static
void BtvMediaPlayerService::dataCallbackMain(void *cookie, int msg, int ext1, int ext2,
                                             const int8_t* dataPtr)
{
    ALOGD("[%s][%d] notify (%d, %d, %d)", __FUNCTION__, __LINE__, msg, ext1, ext2);

    BtvMediaPlayerService* p = static_cast<BtvMediaPlayerService*>(cookie);
    p->mBtvMediaPlayerListener->dataCallback(msg, ext1, ext2, dataPtr);
}

// static
void BtvMediaPlayerService::notifyCallbackPip1(void *cookie, int msg, int ext1, int ext2,
                                               const int8_t* obj)
{
    ALOGD("[%s][%d] notify (%d, %d, %d)", __FUNCTION__, __LINE__, msg, ext1, ext2);

    BtvMediaPlayerService* p = static_cast<BtvMediaPlayerService*>(cookie);
    p->mBtvMediaPlayerListener->notifyCallback(msg, ext1, ext2, obj);
}

// static
void BtvMediaPlayerService::dataCallbackPip1(void *cookie, int msg, int ext1, int ext2,
                                             const int8_t* dataPtr)
{
    ALOGD("[%s][%d] notify (%d, %d, %d)", __FUNCTION__, __LINE__, msg, ext1, ext2);

    BtvMediaPlayerService* p = static_cast<BtvMediaPlayerService*>(cookie);
    p->mBtvMediaPlayerListener->dataCallback(msg, ext1, ext2, dataPtr);
}


#if 0
// static
void BtvMediaPlayerService::dsmccCallback(void *cookie, int32_t dsmccEventType, char* dsmccRootPath, SignalEvent* event)
{
	ALOGD("[%s][%d] dsmccCallback (%d, %s)", __FUNCTION__, __LINE__, dsmccEventType, dsmccRootPath);

	BtvMediaPlayerService* p = static_cast<BtvMediaPlayerService*>(cookie);
	sp<BtvDsmccObserver> client = p->getDsmccObserver();
	if (client != 0)
	{
		ALOGD("[%s][%d] client find... dsmccCallback (%d, %s)", __FUNCTION__, __LINE__, dsmccEventType, dsmccRootPath);
		client->dsmccSignalEvent(dsmccEventType, dsmccRootPath, event);	 
	}
}

// static
void BtvMediaPlayerService::audioProgramUpdateCallback(void *cookie, char* filepath, int32_t updateType, int32_t audioPid)
{
	ALOGD("[%s][%d] audioProgramUpdateCallback (%s, %d, %d)", __FUNCTION__, __LINE__, filepath, updateType, audioPid);

	BtvMediaPlayerService* p = static_cast<BtvMediaPlayerService*>(cookie);
	//sp<BtvAudioProgramUpdateObserver> client = p->getAudioProgramUpdateObserver();
	static std::shared_ptr<BtvAudioProgramUpdateObserver> client = p->getAudioProgramUpdateObserver;
	if (client != 0)
	{
		ALOGD("[%s][%d] client find... audioProgramUpdateCallback (%s, %d, %d)", __FUNCTION__, __LINE__, filepath, updateType, audioPid);
		client->audioProgramUpdate(filepath, updateType, audioPid);	 
	}
}
#endif

shared_ptr<BtvMediaPlayerObserver> BtvMediaPlayerService::getClientByDeviceID(int deviceID)
{
    std::lock_guard<std::mutex> lock(mServiceLock);
    ALOGD("[%s][%d] xxxx 1", __FUNCTION__, __LINE__);
#if 0
    for (int i = 0, n = mClients.size(); i < n; ++i) 
    {
        ALOGD("[%s][%d] n=%d i=%d", __FUNCTION__, __LINE__, n, i);
        //sp<BtvMediaPlayerObserver> c = mClients[i].promote();

        if (c != 0)
        {
        	int id = c->getDeviceID();
            ALOGD("[%s][%d] getDeviceID=%d deviceID=%d", __FUNCTION__, __LINE__, id, deviceID);			
        	if (id == deviceID)
        		return c;
        }
    }
#endif
    ALOGD("[%s][%d] xxxx 2", __FUNCTION__, __LINE__);
    return NULL;
}

int32_t BtvMediaPlayerService::enableCasInfo(bool enable)
{
    shared_ptr<IptvSocPlayer> mainIptvPlayer = selectIptvSocPlayer(0);
    if(mainIptvPlayer != NULL /*&& mainIptvPlayer->InGetPlayerMode() == PLAYER_MODE_IPTV && mainIptvPlayer->InGetPlayerStatus() != PLAYER_STATUS_STOP && mainIptvPlayer->player.isMultiview == false*/) {
        return mainIptvPlayer->enableCasInfo(enable);
    }

    return -1;
}

#if 0
sp<BtvDsmccObserver> BtvMediaPlayerService::getDsmccObserver()
{
    std::lock_guard<std::mutex> lock(mLock);
    ALOGD("[%s][%d] xxxx 1", __FUNCTION__, __LINE__);
    sp<BtvDsmccObserver> c = mDsmccClient.promote();	
    return c;	
}

void BtvMediaPlayerService::setClient(wp<BtvMediaPlayerObserver> client)
{
	mClients.add(client);
}

void BtvMediaPlayerService::removeClient(wp<BtvMediaPlayerObserver> client)
{
	ALOGD("[%s] called", __FUNCTION__);

	std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[%s][%d] ...", __FUNCTION__, __LINE__);
	mClients.remove(client);
	ALOGD("[%s][%d] ...", __FUNCTION__, __LINE__);
}



int32_t BtvMediaPlayerService::connect(sp<BtvMediaPlayerObserver>& client, int deviceID)
{
	int nRet = 0;
	ALOGD("[%s] called, deviceID=%d", __FUNCTION__, deviceID);
	if (deviceID < 0)
	{
		ALOGD("[%s][%d]rejected (invalid deviceID %d).", __FUNCTION__, __LINE__, 
			deviceID);
		return NO_INIT;
	}

	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	bool pass = false;
	if (mpIptvSocPlayer)
	{
		if(!pass) {
			ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
			if (mpIptvSocPlayer->getPlayerStatus() != PLAYER_STATUS_NONE)
			{
				ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
				if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_IPTV)
				{
					ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
					mpIptvSocPlayer->closeTV();
				}

				if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_RTSP)
				{
					ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
					mpIptvSocPlayer->close();
				}

				if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_NORMAL_FILE)
				{
					ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
					mpIptvSocPlayer->close();
				}
				mpIptvSocPlayer->sendEvent(7, deviceID, 1);
			}
		}
	}
	
	if (deviceID == 0)
	{
		IptvSocPlayer *pPlayer = selectIptvSocPlayer(1);
		if (pPlayer)
		{
			if(!pass) {
				ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
				if (pPlayer->getPlayerStatus() != PLAYER_STATUS_NONE)
				{
					ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
					if (pPlayer->getPlayerMode() == PLAYER_MODE_IPTV)
					{
						ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
						pPlayer->closeTV();
					}

					if (pPlayer->getPlayerMode() == PLAYER_MODE_RTSP)
					{
						ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
						pPlayer->close();
					}

					if (pPlayer->getPlayerMode() == PLAYER_MODE_NORMAL_FILE)
					{
						ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
						pPlayer->close();
					}
					pPlayer->sendEvent(7, 1, 1);
				}
			}
		}
	}

	if(deviceID == 0 && mpIptvSocPlayer != NULL) {
		if (mpIptvSocPlayer->getAuthChecked() == 0) {
			mpIptvSocPlayer->CertCheck();
		}
	}

	sp<BtvMediaPlayerObserver> cli;
	cli = getClientByDeviceID(deviceID);
	if(cli == NULL){
		wp<BtvMediaPlayerObserver> w = client;
		setClient(w);
	}

	return nRet;
}

void BtvMediaPlayerService::disconnect(int deviceID)
{
	ALOGD("[%s] called", __FUNCTION__);
	
	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);
	
	ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
	if (mpIptvSocPlayer)
	{
		ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
		if (mpIptvSocPlayer->getPlayerStatus() != PLAYER_STATUS_NONE)
		{
			ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
			if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_IPTV)
			{
				ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
				mpIptvSocPlayer->closeTV();
			}
	
			if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_RTSP)
			{
				ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
				mpIptvSocPlayer->close();
			}
	
			if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_NORMAL_FILE)
			{
				ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
				mpIptvSocPlayer->close();
			}
		}
	}
/*
	sp<BtvMediaPlayerObserver> client;
	client = getClientByDeviceID(deviceID);
	if(client){
		wp<BtvMediaPlayerObserver> w = client;
		removeClient(w);
	}
*/
	ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);

}

void BtvMediaPlayerService::setBtvMediaPlayer(int deviceID)
{
	mpIptvSocPlayer = selectIptvSocPlayer(deviceID);
}

IptvSocPlayer* BtvMediaPlayerService::getBtvPlayer()
{
	return mpIptvSocPlayer;
}

int32_t BtvMediaPlayerService::setPlayerSize(int deviceID, int nLeft, int nTop, int nWidth, int nHeight)
{
	setBtvMediaPlayer(deviceID);

	ALOGD("[%s] called", __FUNCTION__);
	ALOGD("[%s] left=%d, nTop=%d, nWidth=%d, nHeight=%d", __FUNCTION__, nLeft, nTop, nWidth, nHeight);

	int callingPid = 0;//getCallingPid();

	mpIptvSocPlayer->setWindowSize_l(nLeft, nTop, nWidth, nHeight);

	return OK;
}
#endif

//int32_t BtvMediaPlayerService::setPlayerSurfaceView(int deviceID, uint32_t nativWindows)
int32_t BtvMediaPlayerService::setPlayerSurfaceView(int deviceID, uint64_t nativWindows)//for 64bit build
{
	ALOGI("%s %d", __FUNCTION__, __LINE__);
	ALOGD("BtvMediaPlayerService::setPlayerSurfaceView");
	setBtvMediaPlayer(deviceID);

	mpIptvSocPlayer->setPlayerSurfaceView_l(deviceID, nativWindows);
	return 0;
}

#if 0
int32_t BtvMediaPlayerService::setPlayerSurface(int deviceID, const std::shared_ptr<Surface> &surface) {
	LOGI("%s %d", __FUNCTION__, __LINE__);

	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	mpIptvSocPlayer->setPlayerSurface_l(deviceID, surface);
	return 0;//OK;

}


int32_t BtvMediaPlayerService::open(int deviceID, const char* dataXML)
{
	int nRet;
	ALOGD("[%s] called", __FUNCTION__);

	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	ALOGD("[%s][%d] dataXML=%s", __FUNCTION__, __LINE__, dataXML);
	// native service coding start....
	if (!mpIptvSocPlayer)
		return NO_INIT;

	if (mpIptvSocPlayer)
	{
		if (mpIptvSocPlayer->getPlayerStatus() != PLAYER_STATUS_NONE)
		{
			if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_IPTV)
			{
				ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
				mpIptvSocPlayer->closeTV();
			}
		}
	}
	
	HPlayer_IPTV_Info stPlayInfo;
	ALOGD("%s %d", __FUNCTION__, __LINE__);
	stPlayInfo = parsePlayerInfo(dataXML);
	ALOGD("%s %d", __FUNCTION__, __LINE__);

	nRet = mpIptvSocPlayer->open_l(stPlayInfo);

	// TODO : jung2604 : PIP에서 소리가 나오고 있을때 main에서는 소리가 나오면 안된다..세팅값을 넘기는게 제일 좋으나 일단 mute 처리를 해준다.
    if (mpIptvSocPlayer->mDeviceID == 0) {
        IptvSocPlayer *pipIptvPlayer = selectIptvSocPlayer(1);
        if (pipIptvPlayer->InGetPlayerStatus() != PLAYER_STATUS_NONE && pipIptvPlayer->InGetPlayerStatus() != PLAYER_STATUS_STOP) {
            if (pipIptvPlayer->player.locator_head != NULL && !pipIptvPlayer->player.save_mute_enable) {
                mpIptvSocPlayer->changeAudioOnOff_l(0);
            }
        }
    }

	ALOGD("%s %d", __FUNCTION__, __LINE__);	
	return nRet;
}

int BtvMediaPlayerService::parseMultiTuneTVInfo(HTuner_IPTV_Info *ptTuneInfo, const char* dataXML)
{
	char *word;
	char *buffer;
	char *brkt = NULL;
	char *sep="\n";
	char szBuffer[4096];

	LOGI("%s %d", __FUNCTION__, __LINE__);

	strcpy(szBuffer, dataXML);
	/* get url field */

	buffer = (char*) &szBuffer[0];

	int i;
	for(i = 0 ; i < MULTIVIEW_MAX_NUMBER ; i++) {
		word = strtok_r(buffer, sep, &brkt);
		buffer = NULL;
		if(word != NULL) {
			ptTuneInfo[i] = parseTuneTVInfo(word);
		} else {
			LOGI("++ data is NULL, count : %d", i);
			return i;
		}
	}

	return i;
}
#endif

HTuner_IPTV_Info parseTuneTVInfo(const char* dataXML)
{
	HTuner_IPTV_Info stTuneInfo;
	char *word;
	char *buffer;
	char *brkt = NULL;
	char *sep=";";
	char szBuffer[4096];

	ALOGD("%s %d", __FUNCTION__, __LINE__);

	strcpy(szBuffer, dataXML);
	/* get url field */

	buffer = (char*) &szBuffer[0];

	ALOGD("%s %d %s", __FUNCTION__, __LINE__, buffer);
	word = strtok_r(buffer, sep, &brkt);
	strcpy(stTuneInfo.multicast_addr, word);

	/* get port field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.multicast_port = atoi(word);
	ALOGD("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.multicast_port);	
	/* get vpid field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.video_pid = atoi(word);
	ALOGD("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.video_pid);
	/* get apid field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.audio_pid = atoi(word);
	ALOGD("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.audio_pid);
	/* get pcrpid field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.pcr_pid = atoi(word);
	ALOGD("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.pcr_pid);
	/* get video stream field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.video_stream = atoi(word);
	ALOGD("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.video_stream);
	/* get audio stream field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.audio_stream = atoi(word);
	ALOGD("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.audio_stream);
	/* get ca pid field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.ca_pid = atoi(word);
	ALOGD("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.ca_pid);
	/* get ca systemid field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.ca_systemid = atoi(word);
	ALOGD("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.ca_systemid);
	/* get channel number field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.channel_num = atoi(word);
	ALOGD("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.channel_num);
	/* get video resolution field */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.res= atoi(word);
	ALOGD("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.res);
	
	/* get audio mute in playfield */
	word = strtok_r(NULL, sep, &brkt);
	stTuneInfo.audioenable= atoi(word);
	ALOGD("%s %d %d", __FUNCTION__, __LINE__, stTuneInfo.audioenable);

	/* last frame ??정 */
	word = strtok_r(NULL, sep, &brkt);
	if(word && strlen(word) == 1) stTuneInfo.enableLastFrame = atoi(word);
	else stTuneInfo.enableLastFrame = 1;

	/* multiview type : 2x2, 1x3 */
	word = strtok_r(NULL, sep, &brkt);
	if(word && strlen(word) == 1) stTuneInfo.multiviewType = atoi(word);
	else stTuneInfo.multiviewType = 0;

	stTuneInfo.isMainChannelSame = 0; // jung2604 : main??pip??시??같?? 채널??볼때 처리??주????한 Flag

	return stTuneInfo;
}

HPlayer_IPTV_Info BtvMediaPlayerService::parsePlayerInfo(const char* dataXML)
{
	HPlayer_IPTV_Info stPlayInfo;
	char *word;
	char *buffer;
	char *sep=";";
	char szBuffer[4096];

	ALOGD("%s %d", __FUNCTION__, __LINE__);

	strcpy(szBuffer, dataXML);

	buffer = (char*) &szBuffer[0];

	/* get playMode field */
	word = strsep(&buffer, sep);
	stPlayInfo.nPlayMode = atoi(word);

	/* get playtype field */
	word = strsep(&buffer, sep);
	stPlayInfo.nPlayType = atoi(word);

	/* get Drm Type field */
	word = strsep(&buffer, sep);
	stPlayInfo.nDRMType = atoi(word);

	/* get Duration field */
	word = strsep(&buffer, sep);
	stPlayInfo.nDurationSec = atoi(word);

	/* get resume sec field */
	word = strsep(&buffer, sep);
	stPlayInfo.nResumeSec = atoi(word);

	/* get path field */
	word = strsep(&buffer, sep);
	strcpy(stPlayInfo.szPath, word);

	/* get meta info */
	word = strsep(&buffer, sep);
	strcpy(stPlayInfo.szMetaInfo, word);

	/* get contentid field */
	word = strsep(&buffer, sep);
	strcpy(stPlayInfo.szContentID, word);

	/* get OTP ID field */
	word = strsep(&buffer, sep);
	strcpy(stPlayInfo.szOTPID, word);

	/* get OTP PWD field */
	word = strsep(&buffer, sep);
	strcpy(stPlayInfo.szOTPPasswd, word);

	/* last frame ????*/
	word = strsep(&buffer, sep);
	if(word && strlen(word) == 1) stPlayInfo.enableLastFrame = atoi(word);
	else stPlayInfo.enableLastFrame = 1;

	return stPlayInfo;
}

void BtvMediaPlayerService::parseAudioChangeInfo(const char* dataXML, int *nAID, int *nACodecID)
{
    char *word;
    char *buffer;
    char *brkt = NULL;
    char *sep=";";
    char szBuffer[4096];

    ALOGD("%s %d", __FUNCTION__, __LINE__);

    strcpy(szBuffer, dataXML);

    buffer = (char*) &szBuffer[0];

    ALOGD("%s %d %s", __FUNCTION__, __LINE__, buffer);
    word = strtok_r(buffer, sep, &brkt);
    *nAID = atoi(word);

    word = strtok_r(NULL, sep, &brkt);
    *nACodecID = atoi(word);
}


shared_ptr<IptvSocPlayer> BtvMediaPlayerService::selectIptvSocPlayer(int deviceID)
{

	switch (deviceID)
	{
		case IptvSocPlayer::mBTV_DEVICE_MAIN:
		{
			return (shared_ptr<IptvSocPlayer>)mIptvSocPlayerMain;
		}
		case IptvSocPlayer::mBTV_DEVICE_PIP1:
		{
			return (shared_ptr<IptvSocPlayer>)mIptvSocPlayerPip1;
		}
		default:
		{
			return (shared_ptr<IptvSocPlayer>)mIptvSocPlayerMain;
		}
	}

}

void BtvMediaPlayerService::setBtvMediaPlayer(int deviceID)
{
	mpIptvSocPlayer = selectIptvSocPlayer(deviceID);
}

void BtvMediaPlayerService::setBtvMediaPlayerListener(const sp<IBtvMediaPlayerListener> &listener) {
    mBtvMediaPlayerListener = listener;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////

int32_t BtvMediaPlayerService::connect(int deviceID)
{
	int nRet = 0;
	ALOGD("[%s] called, deviceID=%d", __FUNCTION__, deviceID);
	if (deviceID < 0)
	{
		ALOGD("[%s][%d]rejected (invalid deviceID %d).", __FUNCTION__, __LINE__,
			   deviceID);
		return -1;
	}

	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	bool pass = false;

	if (mpIptvSocPlayer)
	{
		if(!pass) {
			ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
			if (mpIptvSocPlayer->getPlayerStatus() != PLAYER_STATUS_NONE)
			{
				ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
				if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_IPTV)
				{
					ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
					mpIptvSocPlayer->closeTV();
				}

				if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_RTSP)
				{
					ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
					mpIptvSocPlayer->close();
				}

				if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_NORMAL_FILE)
				{
					ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
					mpIptvSocPlayer->close();
				}
				mpIptvSocPlayer->sendEvent(DISCONNECT_EVENT, deviceID, 1);
			}
		}
	}

	if(deviceID == 0 && mpIptvSocPlayer != NULL) {
		if (mpIptvSocPlayer->getAuthChecked() == 0) {
			mpIptvSocPlayer->CertCheck();
		}
	}
/*
	std::shared_ptr<BtvMediaPlayerObserver> cli;
	cli = getClientByDeviceID(deviceID);
	if(cli == NULL){
		std::weak_ptr<BtvMediaPlayerObserver> w = client;
		setClient(w);
	}
*/
    catch_signals();
	return nRet;
}

int32_t BtvMediaPlayerService::open(int deviceID, const char* dataXML){
	int nRet;
	ALOGD("[%s] called", __FUNCTION__);

	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	ALOGD("[%s][%d] dataXML=%s", __FUNCTION__, __LINE__, dataXML);
	// native service coding start....
	if (!mpIptvSocPlayer)
		return -1;

	if (mpIptvSocPlayer)
	{
		if (mpIptvSocPlayer->getPlayerStatus() != PLAYER_STATUS_NONE)
		{
			if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_IPTV)
			{
				ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
				mpIptvSocPlayer->closeTV();
			}
		}
	}

	HPlayer_IPTV_Info stPlayInfo;
	ALOGD("%s %d", __FUNCTION__, __LINE__);
	stPlayInfo = parsePlayerInfo(dataXML);
	ALOGD("%s %d", __FUNCTION__, __LINE__);

	nRet = mpIptvSocPlayer->open_l(stPlayInfo);
//	if (nRet == 0)
//		mpIptvSocPlayer->mCallingPointer = (void*)this;

	// TODO : jung2604 : PIP?먯꽌 ?뚮━媛 ?섏삤怨??덉쓣??main?먯꽌???뚮━媛 ?섏삤硫??덈맂??.?명똿媛믪쓣 ?섍린?붽쾶 ?쒖씪 醫뗭쑝???쇰떒 mute 泥섎━瑜??댁???
/*
	if (mpIptvSocPlayer->mDeviceID == 0) {
		IptvSocPlayer *pipIptvPlayer = selectIptvSocPlayer(1);
		if (pipIptvPlayer->InGetPlayerStatus() != PLAYER_STATUS_NONE && pipIptvPlayer->InGetPlayerStatus() != PLAYER_STATUS_STOP) {
			if (pipIptvPlayer->player.locator_head != NULL && !pipIptvPlayer->player.save_mute_enable) {
				mpIptvSocPlayer->changeAudioOnOff_l(0);
			}
		}
	}
 */

	ALOGD("%s %d", __FUNCTION__, __LINE__);
	return nRet;
}

int32_t BtvMediaPlayerService::tuneTV(int deviceID, const char* dataXML)
{
	int nRet;
	ALOGD("[%s][%d] called",  __FUNCTION__, deviceID);

	std::lock_guard<std::mutex> lock(mServiceLock);
	
	setBtvMediaPlayer(deviceID);

	int callingPid = 0; //getCallingPid();

	ALOGD("[%s][%d] device[%d], dataXML=%s", __FUNCTION__, __LINE__, deviceID, dataXML);
	// native service coding start.....
	if (!mpIptvSocPlayer)
		return -1;


	if (mpIptvSocPlayer)
	{
		if (mpIptvSocPlayer->getPlayerStatus() != PLAYER_STATUS_NONE)
		{
			if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_RTSP
			|| mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_NORMAL_FILE)
			{
				ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
				mpIptvSocPlayer->close();
			}
		}
	}

	// fldgo note : 2014.10.10 A Connect -> B Connect -> B Tune in progress -> A disconnect -> B tune done -> A close case check. 
	// A close must be discard
//	mpIptvSocPlayer->mCallingPointer = (void*)this;

#ifdef USE_MULTIVIEW
	HTuner_IPTV_Info ptTuneInfo[MULTIVIEW_MAX_NUMBER];

	int channelCount = parseMultiTuneTVInfo(ptTuneInfo, dataXML);

	ALOGD("%s %d, device[%d], channel count : %d", __FUNCTION__, __LINE__, deviceID, channelCount);
#else
	HTuner_IPTV_Info ptTuneInfo[1];
	ptTuneInfo[0] = parseTuneTVInfo(dataXML);
#endif

	ALOGD("%s %d device[%d], multicast_addr : %s", __FUNCTION__, __LINE__, deviceID, ptTuneInfo[0].multicast_addr);
	ALOGD("%s %d device[%d], multicast_port : %d", __FUNCTION__, __LINE__, deviceID, ptTuneInfo[0].multicast_port);
	ALOGD("%s %d device[%d], video_pid : %d", __FUNCTION__, __LINE__, deviceID, ptTuneInfo[0].video_pid);
	ALOGD("%s %d device[%d], audio_pid : %d", __FUNCTION__, __LINE__, deviceID, ptTuneInfo[0].audio_pid);
	ALOGD("%s %d device[%d], pcr_pid : %d", __FUNCTION__, __LINE__, deviceID, ptTuneInfo[0].pcr_pid);
	ALOGD("%s %d device[%d], video_stream : %d", __FUNCTION__, __LINE__, deviceID, ptTuneInfo[0].video_stream);
	ALOGD("%s %d device[%d], audio_stream : %d", __FUNCTION__, __LINE__, deviceID, ptTuneInfo[0].audio_stream);
    ALOGD("%s %d device[%d], enableLastFrame : %d", __FUNCTION__, __LINE__, deviceID, ptTuneInfo[0].enableLastFrame);
    ALOGD("%s %d device[%d], audioenable : %d", __FUNCTION__, __LINE__, deviceID, ptTuneInfo[0].audioenable);

#ifdef USE_MULTIVIEW
	if(channelCount <= 1) {
		//jung2604 : PIP??때 IPTV가 같?? 채널??경우 별도??처리가 ??요??다..
		if(mpIptvSocPlayer->mDeviceID != 0) {
			IptvSocPlayer* mainIptvPlayer = selectIptvSocPlayer(0);//mIptvPlayerService->selectIptvSocPlayer(0);
			if(mainIptvPlayer != NULL && mainIptvPlayer->InGetPlayerMode() == PLAYER_MODE_IPTV && mainIptvPlayer->InGetPlayerStatus() != PLAYER_STATUS_STOP && mainIptvPlayer->player.isMultiview == false) {
				if(mainIptvPlayer->player.locator_head != NULL) {
					//ALOGD("%s main--:%s current:%s", __FUNCTION__, mainIptvPlayer->player.locator_head->tune_param.ip.ip, ptTuneInfo[0].multicast_addr);
					if(!memcmp(mainIptvPlayer->player.locator_head->tune_param.ip.ip, ptTuneInfo[0].multicast_addr, strlen(mainIptvPlayer->player.locator_head->tune_param.ip.ip))) {
						ptTuneInfo[0].isMainChannelSame = 1; // jung2604 : main??pip??시??같?? 채널??볼때 처리??주????한 Flag
						ALOGD("%s main-- channel is same!!!", __FUNCTION__);
					}
				}
			}

			//pip 의 Audio가 Enable 이면...Main을 Mute 해준다.
            if(ptTuneInfo[0].audioenable && mainIptvPlayer != NULL) {
                mainIptvPlayer->changeAudioOnOff_l(FALSE);
            }
		}

		nRet = mpIptvSocPlayer->tuneTV_l(ptTuneInfo[0]);

		// TODO : jung2604 : PIP에서 소리가 나오고 있을때 main에서는 소리가 나오면 안된다..세팅값을 넘기는게 제일 좋으나 일단 mute 처리를 해준다.
		if (mpIptvSocPlayer->mDeviceID == 0) {
			IptvSocPlayer *pipIptvPlayer = selectIptvSocPlayer(1);
			if (pipIptvPlayer->InGetPlayerStatus() != PLAYER_STATUS_NONE && pipIptvPlayer->InGetPlayerStatus() != PLAYER_STATUS_STOP) {
				if (pipIptvPlayer->player.locator_head != NULL && !pipIptvPlayer->player.save_mute_enable) {
					mpIptvSocPlayer->changeAudioOnOff_l(0);
				}
			}
		}
	}
	else {
		nRet = mpIptvSocPlayer->tuneTV_Multiview_l(ptTuneInfo);
	}
#else
	nRet = mpIptvSocPlayer->tuneTV_l(ptTuneInfo[0]);
#endif

	ALOGD("[%s][%d] out",  __FUNCTION__, deviceID);
	
/*	if (nRet == 0)
		mpIptvSocPlayer->mCallingPointer = (void*)this;*/

	return nRet;
}

int32_t BtvMediaPlayerService::closeTV(int deviceID)
{
	int nRet=0;

	std::lock_guard<std::mutex> lock(mServiceLock);

	ALOGD("[%s][%d] called",  __FUNCTION__, deviceID);

	if (!mpIptvSocPlayer)
		return -1;

	nRet = mpIptvSocPlayer->closeTV();

	return nRet;
}

int32_t BtvMediaPlayerService::bindingFilter(int deviceID, const char* dataXML)
{
	int nRet = 0;
	ALOGD("[%s] called",  __FUNCTION__);
	ALOGD("[%s][%d] dataXML=%s", __FUNCTION__, __LINE__, dataXML);

	std::lock_guard<std::mutex> lock(mServiceLock);

	if (!mpIptvSocPlayer)
		return -1;

	nRet = mpIptvSocPlayer->bindingFilter_l(0, 0, 0, 0);

	return nRet;
}

int32_t BtvMediaPlayerService::releaseFilter(int deviceID, const char* dataXML)
{
	int nRet;
	ALOGD("[%s] called",  __FUNCTION__);
	ALOGD("[%s][%d] dataXML=%s", __FUNCTION__, __LINE__, dataXML);

	if (!mpIptvSocPlayer)
		return -1;

	nRet = mpIptvSocPlayer->releaseFilter_l(0);
	return nRet;
}

int32_t BtvMediaPlayerService::changeAudioChannel(int deviceID, const char* dataXML) {
	int nAid, nACodecID, nRet;
	ALOGD("[%s] called", __FUNCTION__);
	ALOGD("[%s][%d] dataXML=%s", __FUNCTION__, __LINE__, dataXML);

	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	// native service coding start.....
	if (!mpIptvSocPlayer)
		return -1;


	parseAudioChangeInfo(dataXML, &nAid, &nACodecID);
	nRet = mpIptvSocPlayer->changeAudioChannel_l(nAid, nACodecID);
	ALOGD("%s %d", __FUNCTION__, __LINE__);

	return nRet;
}

int32_t BtvMediaPlayerService::changeAudioOnOff(int deviceID, const char* dataXML)
{
	char *word;
	char *buffer;
	char *brkt = NULL;
	char *sep = ";";
	char szBuffer[4096];

	int nOnOff;

	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	ALOGD("[%s] called", __FUNCTION__);
	if (!mpIptvSocPlayer)
		return -1;

	strcpy(szBuffer, dataXML);

	buffer = &szBuffer[0];
	word = strtok_r(buffer, sep, &brkt);
	nOnOff = atoi(word);

	return mpIptvSocPlayer->changeAudioOnOff_l(nOnOff);
}

int32_t BtvMediaPlayerService::getPlayerMode(int deviceID)
{
	ALOGD("[%s] called", __FUNCTION__);

	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	if (!mpIptvSocPlayer)
		return -1;

	return mpIptvSocPlayer->getPlayerMode();
}

int32_t BtvMediaPlayerService::getPlayerStatus(int deviceID)
{
	ALOGD("[%s] called", __FUNCTION__);

	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	if (!mpIptvSocPlayer)
		return -1;

	return 	mpIptvSocPlayer->getPlayerStatus();
}

int32_t BtvMediaPlayerService::getCurrentPosition(int deviceID)
{
	ALOGD("[%s] called", __FUNCTION__);

	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	if (!mpIptvSocPlayer)
		return -1;

	return 	mpIptvSocPlayer->getCurrentPosition();
}

int32_t BtvMediaPlayerService::setWindowSize(int deviceID, const char* dataXML)
{
	char *word;
	char *buffer;
	char *brkt = NULL;
	char *sep=";";
	char szBuffer[4096];

	int nX, nY, nWidth, nHeight;

	ALOGD("[%s] called", __FUNCTION__);
	setBtvMediaPlayer(deviceID);

	if (!mpIptvSocPlayer)
		return -1;

	strcpy(szBuffer, dataXML);

	buffer = &szBuffer[0];
	word = strtok_r(buffer, sep, &brkt);
	nX = atoi(word);

	word = strtok_r(NULL, sep, &brkt);
	nY = atoi(word);

	word = strtok_r(NULL, sep, &brkt);
	nWidth = atoi(word);

	word = strtok_r(NULL, sep, &brkt);
	nHeight = atoi(word);

	return mpIptvSocPlayer->setWindowSize_l(nX, nY, nWidth, nHeight);;
}

int32_t BtvMediaPlayerService::play(int deviceID)
{
	int nRet;
	ALOGD("[%s] called", __FUNCTION__);

	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	// native service coding start.....
	if (!mpIptvSocPlayer)
		return -1;

	nRet = mpIptvSocPlayer->play();

	return nRet;
}

int32_t BtvMediaPlayerService::pause(int deviceID)
{
	ALOGD("[%s] called", __FUNCTION__);

	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	// native service coding start.....
	if (!mpIptvSocPlayer)
		return -1;

	return mpIptvSocPlayer->pause();
}

int32_t BtvMediaPlayerService::resume(int deviceID)
{
	ALOGD("[%s] called", __FUNCTION__);

	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	// native service coding start.....
	if (!mpIptvSocPlayer)
		return -1;

	return mpIptvSocPlayer->resume();
}

int32_t BtvMediaPlayerService::seek(int deviceID, const char* dataXML, bool pause)
{
	char *word;
	char *buffer;
	char *brkt = NULL;
	char *sep=";";
	char szBuffer[4096];

	long nSec;

	ALOGD("[%s] called", __FUNCTION__);

	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	if (!mpIptvSocPlayer)
		return -1;

	strcpy(szBuffer, dataXML);

	buffer = &szBuffer[0];
	word = strtok_r(buffer, sep, &brkt);
	nSec = atol(word); //atoi(word);

	return mpIptvSocPlayer->seek_l(nSec, pause);
}

int32_t BtvMediaPlayerService::trick(int deviceID, const char* dataXML)
{
	char *word;
	char *buffer;
	char *brkt = NULL;
	char *sep=";";
	char szBuffer[4096];

	int nTrick;

	ALOGD("[%s] called", __FUNCTION__);

	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	if (!mpIptvSocPlayer)
		return -1;

	strcpy(szBuffer, dataXML);

	buffer = &szBuffer[0];
	word = strtok_r(buffer, sep, &brkt);
	nTrick = atoi(word);

	return mpIptvSocPlayer->trick_l(nTrick);
}

int32_t BtvMediaPlayerService::stop(int deviceID)
{
	ALOGD("[%s] called", __FUNCTION__);
	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	// native service coding start.....
	if (!mpIptvSocPlayer)
		return -1;

	// fldgo note : 2015.01.20 A connect -> B connect -> B Tune -> A Stop case check. A Stop Must be discard

	int callingPid = 0;//getCallingPid();

	return mpIptvSocPlayer->stop();
}

int32_t BtvMediaPlayerService::close(int deviceID)
{
	int nRet;
	ALOGD("[%s] called",  __FUNCTION__);

	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	// native service coding start.....
	if (!mpIptvSocPlayer)
		return -1;

	/*
	if (this != mpIptvSocPlayer->mCallingPointer)
	{
		LOGSRC("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
		LOGSRC("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
		LOGSRC("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
		LOGSRC("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
		LOGSRC("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
		LOGSRC("[%s] called [%x] [%x]", __FUNCTION__, mpIptvSocPlayer->mCallingPointer, this);
		return NO_ERROR;
	}*/

	nRet = mpIptvSocPlayer->close();

	mpIptvSocPlayer->reset();

	//mpIptvSocPlayer->mCallingPointer = NULL;

	return nRet;
}

int32_t BtvMediaPlayerService::setPlayerSize(int deviceID, int nLeft, int nTop, int nWidth, int nHeight)
{
	setBtvMediaPlayer(deviceID);

	ALOGD("[%s] called", __FUNCTION__);
	ALOGD("[%s] left=%d, nTop=%d, nWidth=%d, nHeight=%d", __FUNCTION__, nLeft, nTop, nWidth, nHeight);

	int callingPid = 0;//getCallingPid();

	mpIptvSocPlayer->setWindowSize_l(nLeft, nTop, nWidth, nHeight);

	return 0;
}

int32_t BtvMediaPlayerService::startDmxFilter(int deviceID, int32_t pid, int32_t tid)
{
	ALOGD("[%s] (pid:%d, tid:%d) called",  __FUNCTION__, pid, tid);
	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	mpIptvSocPlayer->startDmxFilter(pid, tid);
	return 0;
}

int32_t BtvMediaPlayerService::stopDmxFilter(int deviceID, int32_t pid, int32_t tid)
{
	ALOGD("[%s] (pid:%d, tid:%d) called",  __FUNCTION__, pid, tid);
	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	mpIptvSocPlayer->stopDmxFilter(pid, tid);
	return 0;
}


int32_t BtvMediaPlayerService::setDummys(int deviceID, const char* dummys)
{
	//setBtvMediaPlayer(deviceID);
	//return mpIptvSocPlayer->setDummys_l(dummys);
	return IPTV_ERR_SUCCESS;
}

void BtvMediaPlayerService::disconnect(int deviceID)
{
	ALOGD("[%s] called", __FUNCTION__);

	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
	if (mpIptvSocPlayer) {
		ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
		if (mpIptvSocPlayer->getPlayerStatus() != PLAYER_STATUS_NONE) {
			ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
			if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_IPTV) {
				ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
				mpIptvSocPlayer->closeTV();
			}

			if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_RTSP) {
				ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
				mpIptvSocPlayer->close();
			}

			if (mpIptvSocPlayer->getPlayerMode() == PLAYER_MODE_NORMAL_FILE) {
				ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);
				mpIptvSocPlayer->close();
			}
		}
	}
/*
	sp<BtvMediaPlayerObserver> client;
	client = getClientByDeviceID(deviceID);
	if(client){
		wp<BtvMediaPlayerObserver> w = client;
		removeClient(w);
	}
*/
	ALOGD("[%s] CLIENT CLOSED", __FUNCTION__);

}

	int32_t BtvMediaPlayerService::pauseAt(int deviceID, const char* dataXML)
{
	char *word;
	char *buffer;
	char *brkt = NULL;
	char *sep=";";
	char szBuffer[4096];

	long nMs = 0;

	ALOGD("[%s] called", __FUNCTION__);

	std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	strcpy(szBuffer, dataXML);

	buffer = &szBuffer[0];
	word = strtok_r(buffer, sep, &brkt);
	nMs = atol(word); //atoi(word);

	return mpIptvSocPlayer->pauseAt_l(nMs);
}

#if 0
int32_t BtvMediaPlayerService::invoke(int deviceID, const Parcel& request, Parcel* reply)
{
    int nRet;
    ALOGD("[%s] called",  __FUNCTION__);

    //std::lock_guard<std::mutex> lock(mServiceLock);

    setBtvMediaPlayer(deviceID);

    // native service coding start.....
    if (!mpIptvSocPlayer)
        return NO_INIT;

    //nRet = mpIptvSocPlayer->invoke(request, reply);
    return nRet;
}
#endif

int32_t BtvMediaPlayerService::keepLastFrame(int deviceID, bool flag)
{
	ALOGD("[%s] called", __FUNCTION__);

    std::lock_guard<std::mutex> lock(mServiceLock);

	setBtvMediaPlayer(deviceID);

	if (!mpIptvSocPlayer)
		return -1;

	return mpIptvSocPlayer->keepLastFrame(flag);
}


int32_t BtvMediaPlayerService::setStbId(const char* stbId)
{
	ALOGD("[%s] called stbId : %s", __FUNCTION__, stbId);
    set_systemproperty(PROPERTY__STB_ID, stbId, strlen(stbId));
    if(mpIptvSocPlayer != NULL && mpIptvSocPlayer->getAuthChecked() == 0){
		mpIptvSocPlayer->CertCheck();
    }
	return 0;
}

int32_t BtvMediaPlayerService::setMacAddress(const char* macAddr)
{
	ALOGD("[%s] called macAddr : %s", __FUNCTION__, macAddr);
    set_systemproperty(PROPERTY__ETHERNET_MAC, macAddr, strlen(macAddr));
	if(mpIptvSocPlayer != NULL && mpIptvSocPlayer->getAuthChecked() == 0){
		mpIptvSocPlayer->CertCheck();
	}
	return 0;
}
int32_t BtvMediaPlayerService::setGwToken(const char* gwToken)
{
	ALOGD("[%s] called gwToken : %s", __FUNCTION__, gwToken);
	set_systemproperty(PROPERTY__GW_TOKEN, gwToken, strlen(gwToken));
	if(mpIptvSocPlayer != NULL && mpIptvSocPlayer->getAuthChecked() == 0){
		mpIptvSocPlayer->CertCheck();
	}
	return 0;
}
int32_t BtvMediaPlayerService::setVisionImpaired(bool visionImpaired)
{
	ALOGD("[%s] called visionImpaired : %d", __FUNCTION__, visionImpaired);
	if(visionImpaired){
		set_systemproperty(PROPERTY__VISION_IMPAIRED, "true", 20);
	} else {
		set_systemproperty(PROPERTY__VISION_IMPAIRED, "false", 20);
	}

	return 0;
}
int32_t BtvMediaPlayerService::setHearingImpaired(bool hearingImpaired)
{
	ALOGD("[%s] called hearingImpaired : %d", __FUNCTION__, hearingImpaired);
	if(hearingImpaired){
		set_systemproperty(PROPERTY__HEARING_IMPAIRED, "true", 20);
	} else {
		set_systemproperty(PROPERTY__HEARING_IMPAIRED, "false", 20);
	}
	return 0;
}
int32_t BtvMediaPlayerService::setVideoScalingMode(int scalingMode)
{
	ALOGD("[%s] called scalingMode : %d", __FUNCTION__, scalingMode);
	//set_systemproperty(PROPERTY__SCALING_MODE, ""+scalingMode, 1);
	if(mpIptvSocPlayer != NULL ){
		if(scalingMode != mpIptvSocPlayer->getAspectRationMode()) {
			mpIptvSocPlayer->setAspectRationMode(scalingMode);
			mpIptvSocPlayer->changeAspectRatioMode();
		}
	}
	return 0;
}

bool BtvMediaPlayerService::getMlrActivationEnable()
{
	char value[20];
	char file_value[20];
    int ret;
    get_systemproperty(PROPERTY__MLR_ACTIVATION_ENABLE, value, 20);
    ALOGD("[%s] called getMlrActivationEnable %s", __FUNCTION__, value);
    memcpy(file_value, value, sizeof(value));
    #ifdef USE_MLR
    ret = mlr_conf_file_operation(0, file_value); //1st time??
    if((strcmp(value, "true") == 0) && (ret == 1)) {
        return true;
    }
    if((strcmp(value, "true") == 0) && (ret == 0))
    {
      set_systemproperty(PROPERTY__MLR_ACTIVATION_ENABLE, "false", 20);
    }
    #else
    if(strcmp(value, "true") == 0) {
        return true;
    }
    #endif
    return false;
}


int32_t BtvMediaPlayerService::setMlrActivationEnable(bool mlractivationEnable)
{
	char file_value[20];

	ALOGD("[%s] called setMlrActivationEnable : %d", __FUNCTION__, mlractivationEnable);
	if(mlractivationEnable){
		set_systemproperty(PROPERTY__MLR_ACTIVATION_ENABLE, "true", 20);
		sprintf(file_value, "%s", "true");
	} else {
		set_systemproperty(PROPERTY__MLR_ACTIVATION_ENABLE, "false", 20);
		sprintf(file_value, "%s", "false");
	}
	#ifdef USE_MLR
    mlr_conf_file_operation(1, file_value);
    #endif
	return 0;
}

int32_t BtvMediaPlayerService::setPackageInternalPath(const char* internalPath) {
    set_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, internalPath, 64);
    return 0;
}

int32_t BtvMediaPlayerService::setTtaTestOption(const char* key, const char* value)
{
	ALOGD("[%s] called key => %s,    value => %s", __FUNCTION__, key, value);
	set_systemproperty(key, value, 128);
	return 0;
}

int32_t BtvMediaPlayerService::setAVOffset(int type, int nTimeMs)
{
    int nRet;
	ALOGD("[%s] called type : %d, nTimeMs : %d",  __FUNCTION__, type, nTimeMs);

	if (!mpIptvSocPlayer)
		return -1;

    nRet = mpIptvSocPlayer->setAVOffset_l(type, nTimeMs);
	return nRet;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

