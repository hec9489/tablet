// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <sys/syscall.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <ctype.h>

#include <iptvmedia/util_logmgr.h>
#include <android/log.h>

#if USED_LOG_INI
#include "SKAF_INI.h"
#endif //USED_LOG_INI


#define MAX_LOG_BYTES	(4*1024*1024)


//#define PRINT printf chojjj android porting
//#define PRINT(...) __android_log_print(ANDROID_LOG_DEBUG  , "svcmanager", __VA_ARGS__)

static char g_strModuleName[128] 	= "UNKNOWN";
static long g_uOption				= 0L;
static long g_uSystemLevel			= -1L;
static long g_nLogCount 			= 0L;
static long g_uModuleLevel			= -1L;
static long g_nLogFD	 			= -1L;
static long g_nStandardFD	 		= -1L;

// 2011.10.17 namsj
static eLOGFILETYPE_Option s_eLogFileType = e_LOGFILETYPE_TIME;
static int s_nStartedFileLogTime	= 0;
static int s_nMaxBackupSec = 0;
static int s_nLogMaxBytes = MAX_LOG_BYTES;

// 2013.9.13 yohan
static char g_strLogFilePath[256] 	= "";



#define TEXT_COLOR_BLACK 		"\033[22;15m"
#define TEXT_COLOR_RED	 		"\033[22;31m"
#define TEXT_COLOR_GREEN 		"\033[22;32m"
#define TEXT_COLOR_BROWN 		"\033[22;33m"
#define TEXT_COLOR_BLUE 		"\033[22;34m"
#define TEXT_COLOR_MAGENTA 		"\033[22;35m"
#define TEXT_COLOR_CYAN 		"\033[22;36m"
#define TEXT_COLOR_GRAY 		"\033[22;37m"
#define TEXT_COLOR_DARKGRAY 	"\033[01;30m"
#define TEXT_COLOR_LIGHTRED 	"\033[01;31m"
#define TEXT_COLOR_LIGHTGREEN 	"\033[01;32m"
#define TEXT_COLOR_YELLOW 		"\033[01;33m"
#define TEXT_COLOR_LIGHTBLUE 	"\033[01;34m"
#define TEXT_COLOR_LIGHTMAGENTA "\033[01;35m"
#define TEXT_COLOR_LIGHTCYAN 	"\033[01;36m"
#define TEXT_COLOR_WHITE		"\033[01;37m"
#define TEXT_COLOR_WHITE_END	"\033[01;37m" // ommit \n for chojjj android porting



// log option ??�젙 �?
#define INI_SECTION_OPT			"LOGOPT"
#define INI_LOG_OPT_ALL			"ALL"
#define INI_LOG_OPT_STDOUT		"STDOUT"	/* printf out */
#define INI_LOG_OPT_FILEOUT		"FILEOUT"	/* file out*/

// systemp level ??�젙 �?
#define INI_SECTION_SYSTEMLEVEL	"LOGLEVEL_SYSTEM"
#define INI_LOG_SLEVELALL		"ALL"
#define INI_LOG_SLEVELXX		"LOG_SLEVEL%02d"

// module level ??�젙 �?
#define INI_SECTION_MODULELEVEL	"LOGLEVEL_MODULE"
#define INI_LOG_MLEVELALL		"ALL"
#define INI_LOG_MLEVELXX		"LOG_MLEVEL%02d"


char* g_aColoString[] = {
		TEXT_COLOR_GREEN//TEXT_COLOR_BLACK
		,TEXT_COLOR_RED
		,TEXT_COLOR_GREEN
		,TEXT_COLOR_BROWN
		,TEXT_COLOR_BLUE
		,TEXT_COLOR_MAGENTA
		,TEXT_COLOR_CYAN
		,TEXT_COLOR_GRAY
		,TEXT_COLOR_DARKGRAY
		,TEXT_COLOR_LIGHTRED
		,TEXT_COLOR_LIGHTGREEN
		,TEXT_COLOR_YELLOW
		,TEXT_COLOR_LIGHTBLUE
		,TEXT_COLOR_LIGHTMAGENTA
		,TEXT_COLOR_LIGHTCYAN
		,TEXT_COLOR_WHITE
};



#define MAX_BUF_DEBUG_TEMP 4096


// internal prototype
int 	_log_checkSystemLevel(long uSystemLevel);
int 	_log_checkModuleLevel(long uModuleLevel);
int 	_log_getLevelColor(long uSystemLevel);
int 	_log_FileOut(char* strLog, long uSystemLevel, char* strEnd);
int 	_log_StdOut(char* strLog, long uSystemLevel, char* strEnd);
int 	_log_CatOut(char* strLog, long uSystemLevel, char* strEnd);


int 	_log_ThreadID(void);

int 	_log_ThreadID(void)
{
	return (int)syscall(__NR_gettid/*SYS_gettid*/); // android porting chojjj 20130131*/
}



#if USED_LOG_INI
int 	_log_INI_GetInt_default(TDictionary * d, const TChar * key, TInt32 notfound);
#endif //#if USED_LOG_INI

int 	_log_StdOut(char* strLog, long uSystemLevel, char* strEnd)
{
	if(LOG_OPT_STDOUT&g_uOption){
		printf("%s\n", strLog);
	}
	else{
		return -1;
	}
	return 0;
}

int 	_log_CatOut(char* strLog, long uSystemLevel, char* strEnd)
{
	if(LOG_OPT_LOGCATOUT&g_uOption){
		__android_log_print(ANDROID_LOG_DEBUG  , "BOXKEY-MEDIA", "%s", strLog);
	}
	else{
		return -1;
	}
	return 0;
}



int 	_log_FileOut(char* strLog, long uSystemLevel, char* strEnd)
{

	struct stat statbuf;
	int nCurBytes=0;
	char szBackupFileName[256];

	if(LOG_OPT_FILEOUT&g_uOption){

		if(-1 == g_nLogFD){

			sprintf(g_strLogFilePath, "%s%s.log", LOG_FILE_DIR, g_strModuleName);

			g_nLogFD = open(g_strLogFilePath, O_CREAT|O_APPEND|O_WRONLY, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
			if(-1 == g_nLogFD){
				return -1;
			}
		}

		//write(g_nLogFD, g_aColoString[_log_getLevelColor(uSystemLevel)], strlen(g_aColoString[_log_getLevelColor(uSystemLevel)]));
		write(g_nLogFD, strLog, strlen(strLog));
		write(g_nLogFD, strEnd, strlen(strEnd));

		memset(&statbuf, 0x00, sizeof (struct stat));

		if (stat(g_strLogFilePath, &statbuf) < 0){
			fprintf(stderr, "stat error [%s]\n", g_strLogFilePath);
			return -1;
		}

		nCurBytes = ( int )statbuf.st_size ;
		//printf ("[%s][%d] s_nLogMaxBytes=%d, nCurBytes=%d\n\n", __func__, __LINE__, s_nLogMaxBytes, nCurBytes);

		if (nCurBytes > s_nLogMaxBytes){
			memset(szBackupFileName, 0x00, 256);
			strcpy(szBackupFileName, g_strLogFilePath);
			strcat(szBackupFileName, ".bak" );
			remove(szBackupFileName);

			rename( g_strLogFilePath, szBackupFileName);

			close(g_nLogFD);
			g_nLogFD = -1;
		}

		return 0;
	}
	else{
		return -1;
	}

	return 0;
}



int 	_log_checkSystemLevel(long uSystemLevel)
{
	if(g_uSystemLevel&uSystemLevel){
		return 0;
	}
	else{
		return -1;
	}
}

int 	_log_checkModuleLevel(long uModuleLevel)
{
	if(g_uModuleLevel&uModuleLevel){
		return 0;
	}
	else{
		return -1;
	}
}


int 	_log_getLevelColor(long uSystemLevel)
{

	int nColorIndex = 15;
	if(LOG_SLEVEL00 == uSystemLevel){
		nColorIndex = 0;
	}
	else if(LOG_SLEVEL01 == uSystemLevel){
		nColorIndex = 1;
	}
	else if(LOG_SLEVEL02 == uSystemLevel){
		nColorIndex = 2;
	}
	else if(LOG_SLEVEL03 == uSystemLevel){
		nColorIndex = 3;
	}
	else if(LOG_SLEVEL04 == uSystemLevel){
		nColorIndex = 4;
	}
	else if(LOG_SLEVEL05 == uSystemLevel){
		nColorIndex = 5;
	}
	else if(LOG_SLEVEL06 == uSystemLevel){
		nColorIndex = 6;
	}
	else if(LOG_SLEVEL07 == uSystemLevel){
		nColorIndex = 3;
	}
	else if(LOG_SLEVEL08 == uSystemLevel){
		nColorIndex = 8;
	}
	else if(LOG_SLEVEL09 == uSystemLevel){
		nColorIndex = 9;
	}
	else if(LOG_SLEVEL10 == uSystemLevel){
		nColorIndex = 10;
	}
	else if(LOG_SLEVEL11 == uSystemLevel){
		nColorIndex = 11;
	}
	else if(LOG_SLEVEL12 == uSystemLevel){
		nColorIndex = 12;
	}
	else if(LOG_SLEVEL13 == uSystemLevel){
		nColorIndex = 13;
	}
	else if(LOG_SLEVEL14 == uSystemLevel){
		nColorIndex = 14;
	}
	else if(LOG_SLEVEL15 == uSystemLevel){
		nColorIndex = 15;
	}
	else {
		nColorIndex = 3;
	}


	return nColorIndex;
}



#if USED_LOG_INI
int 	_log_INI_GetInt_default(TDictionary * d, const TChar * key, TInt32 notfound)
{


	int nRet = -1;

	nRet = TINI_GetInt(d, key, notfound);
	PRINT("INI_GetInt INI_GetInt key(%s) nRet(%d)\n\n ", key, nRet);
	if(notfound ==  nRet){
		TINI_SetStr(d, key, "0");
	}

	return nRet;
}
#endif //#if USED_LOG_INI

int 	log_LoadOption()
{

	char strTempFilePath[256];
	char strTemp[128];

	int nINIFD = -1;
	int i;

	if(0 == strcmp("UNKNOWN", g_strModuleName)){
		return -1;
	}


	sprintf(strTempFilePath, "%s%s.ini", LOG_FILE_DIR, g_strModuleName);
	if(0 != access(strTempFilePath,F_OK)){

		// 2012.06.26. namsj ???�� 沅뚰�???�젙
		nINIFD = open(strTempFilePath, O_CREAT|O_TRUNC|O_WRONLY, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
		if(0 < nINIFD){
			// log option ??�젙 �?
			sprintf(strTemp, "[%s]\n", INI_SECTION_OPT);
			write(nINIFD, strTemp, strlen(strTemp));
			sprintf(strTemp, "%s = 0\n", INI_LOG_OPT_ALL);
			write(nINIFD, strTemp, strlen(strTemp));
			sprintf(strTemp, "%s = 0\n", INI_LOG_OPT_STDOUT);
			write(nINIFD, strTemp, strlen(strTemp));
			sprintf(strTemp, "%s = 0\n", INI_LOG_OPT_FILEOUT);
			write(nINIFD, strTemp, strlen(strTemp));

			// systemp level ??�젙 �?
			sprintf(strTemp, "[%s]\n", INI_SECTION_SYSTEMLEVEL);
			write(nINIFD, strTemp, strlen(strTemp));
			sprintf(strTemp, "%s = 0\n", INI_LOG_SLEVELALL);
			write(nINIFD, strTemp, strlen(strTemp));
			for(i=0; i<16; i++){
				sprintf(strTemp,INI_LOG_SLEVELXX, i);
				strcat(strTemp, " = 0\n");
				write(nINIFD, strTemp, strlen(strTemp));
			}

			// module level ??�젙 �?
			sprintf(strTemp, "[%s]\n", INI_SECTION_MODULELEVEL);
			write(nINIFD, strTemp, strlen(strTemp));
			sprintf(strTemp, "%s = 0\n", INI_LOG_MLEVELALL);
			write(nINIFD, strTemp, strlen(strTemp));
			for(i=0; i<16; i++){
				sprintf(strTemp, INI_LOG_MLEVELXX, i);
				strcat(strTemp, " = 0\n");
				write(nINIFD, strTemp, strlen(strTemp));

			}

			close(nINIFD);
		}
	}


#if USED_LOG_INI
	TDictionary *d;
	d = TINI_LoadIni(strTempFilePath);
	if(d){
		// log option ??�젙 �?
		sprintf(strTemp, "%s:%s", INI_SECTION_OPT, INI_LOG_OPT_ALL);
		nRet = _log_INI_GetInt_default(d, strTemp, -1);
		g_uOption = (1 == nRet)? 0xFFFF:0x0000;

		if(0xFFFF != g_uOption){
			sprintf(strTemp, "%s:%s", INI_SECTION_OPT, INI_LOG_OPT_STDOUT);
			nRet = _log_INI_GetInt_default(d, strTemp, -1);
			g_uOption |= (1 == nRet)? 0x0001:0;

			sprintf(strTemp, "%s:%s", INI_SECTION_OPT, INI_LOG_OPT_FILEOUT);
			nRet = _log_INI_GetInt_default(d, strTemp, -1);
			g_uOption |= (1 == nRet)? (0x0001<<1):0;
		}


		// systemp level ??�젙 �?
		sprintf(strTemp, "%s:%s", INI_SECTION_SYSTEMLEVEL, INI_LOG_SLEVELALL);
		nRet = _log_INI_GetInt_default(d, strTemp, -1);
		g_uSystemLevel = (1 == nRet)? 0xFFFF:0x0000;

		if(0xFFFF != g_uSystemLevel){
			for(i=0; i<16; i++){

				sprintf(strTemp, "%s:", INI_SECTION_SYSTEMLEVEL);
				sprintf(strTemp1,INI_LOG_SLEVELXX, i);
				strcat(strTemp, strTemp1);
				nRet = _log_INI_GetInt_default(d, strTemp, -1);
				g_uSystemLevel |= (1 == nRet)? (0x0001<<i):0;
			}
		}



		// module level ??�젙 �?
		sprintf(strTemp, "%s:%s", INI_SECTION_MODULELEVEL, INI_LOG_MLEVELALL);
		nRet = _log_INI_GetInt_default(d, strTemp, -1);
		g_uModuleLevel = (1 == nRet)? 0xFFFF:0x0000;

		if(0xFFFF != g_uModuleLevel){
			for(i=0; i<16; i++){

				sprintf(strTemp, "%s:", INI_SECTION_MODULELEVEL);
				sprintf(strTemp1,INI_LOG_MLEVELXX, i);
				strcat(strTemp, strTemp1);
				nRet = _log_INI_GetInt_default(d, strTemp, -1);
				g_uModuleLevel |= (1 == nRet)? (0x0001<<i):0;
			}
		}




/*
 	 	// 2012.06.26. namsj ???�� 沅뚰�???�젙
		nINIFD = open(strTempFilePath, O_CREAT|O_TRUNC|O_WRONLY, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
		if(0 < nINIFD){
			TINI_SaveIni(d, nINIFD);
			close(nINIFD);
		}
*/
		TINI_FreeDict(d);
	}

	PRINT("g_uOption(0x%04x) g_uSystemLevel(0x%04x) g_uModuleLevel(0x%04x)\n\n", g_uOption, g_uSystemLevel, g_uModuleLevel);
#endif //


	return 0;
}

int 	log_SaveOption()
{
	return 0;
}

// initialize
int 	log_Initialize(const char *strModuleName)
{
	if(0 != strcmp("UNKNOWN", g_strModuleName)){
		return 1;
	}

	strcpy(g_strModuleName, strModuleName);

	g_uSystemLevel			= -1L;
	g_uModuleLevel			= -1L;
	g_uOption				= -1L;



	LOGEX("");
	LOGEX("");
	LOGEX("****************************************************************");
	LOGEX("%s start", g_strModuleName);
	LOGEX("****************************************************************");
	LOGEX("");

	g_uOption				= 0;

	log_LoadOption();

	return 0;
}
void 	log_uninitialize()
{
	g_uSystemLevel			= -1L;
	g_uModuleLevel			= -1L;
	g_uOption				= -1L;

	LOGEX("\n");
	LOGEX("\n");
	LOGEX("****************************************************************");
	LOGEX("%s end", g_strModuleName);
	LOGEX("****************************************************************");
	LOGEX("\n");


	if(-1 != g_nStandardFD){
		close(g_nStandardFD);
		g_nStandardFD = -1;
	}

	if(-1 != g_nLogFD){
		close(g_nLogFD);
		g_nLogFD = -1;
	}



	strcpy(g_strModuleName, "UNKNOWN");
	g_uOption				= LOG_OPT_STDOUT;
	g_uSystemLevel			= 0L;
	g_uModuleLevel			= 0L;
}

// control
long 	log_GetOption()
{
	return g_uOption;
}

int 	log_SetOption(long uOption)
{
	g_uOption = uOption;
	return 0;
}

long 	log_GetSystemLevel()
{
	return g_uSystemLevel;
}

int 	log_SetSystemLevel(long uSystemLevel)
{
	g_uSystemLevel = uSystemLevel;
	return 0;
}

long 	log_GetModuleLevel()
{
	return g_uModuleLevel;
}

int 	log_SetModuleLevel(long uModuleLevel)
{
	g_uModuleLevel = uModuleLevel;
	return 0;
}

// print log
void 	log_Print(long uSystemLevel, long uModuleLevel, const char *format, ...)
{
	char szDebugTemp[MAX_BUF_DEBUG_TEMP] = {0,};
	va_list va;


	// ??�뒪????�꺼 泥댄�?
	if(0 != _log_checkSystemLevel(uSystemLevel)){
		return;
	}

	// 紐⑤�???�꺼 泥댄�?
	if(0 != _log_checkModuleLevel(uModuleLevel)){
		return;
	}



	va_start(va, format);
	vsprintf(szDebugTemp, format, va);
	va_end(va);

	_log_FileOut(szDebugTemp, uSystemLevel, "\n");
	_log_StdOut(szDebugTemp, uSystemLevel, TEXT_COLOR_WHITE);
	_log_CatOut(szDebugTemp, uSystemLevel, TEXT_COLOR_WHITE);


}

void 	log_PrintEx(long uSystemLevel, long uModuleLevel, const char *format, ...)
{
	char szDebugTemp[MAX_BUF_DEBUG_TEMP] = {0,};

	int nHeaderLength;
	
	struct 		timeval val;
	struct tm 	*ptm = NULL;
	va_list 	va;


	// ??�뒪????�꺼 泥댄�?
	if(0 != _log_checkSystemLevel(uSystemLevel)){
		return;
	}

	// 紐⑤�???�꺼 泥댄�?
	if(0 != _log_checkModuleLevel(uModuleLevel)){
		return;
	}


	gettimeofday( &val, NULL );
	ptm = localtime( &val.tv_sec );
	sprintf(szDebugTemp, "[%s %04d %02d/%02d %02d:%02d:%02d:%03d][%d] ", g_strModuleName, g_nLogCount++, ptm->tm_mon + 1, ptm->tm_mday
			,ptm->tm_hour, ptm->tm_min, ptm->tm_sec, (int)val.tv_usec / 1000, _log_ThreadID());

	nHeaderLength = strlen(szDebugTemp);

	va_start(va, format);
	vsprintf(szDebugTemp+strlen(szDebugTemp), format, va);
	va_end(va);

	_log_FileOut(szDebugTemp, uSystemLevel, "\n");
	_log_StdOut(szDebugTemp, uSystemLevel, TEXT_COLOR_WHITE_END);
	_log_CatOut(&szDebugTemp[nHeaderLength], uSystemLevel, TEXT_COLOR_WHITE_END);

}

void 	log_Printsrc(long uSystemLevel, long uModuleLevel, const char *strFilename, int nline, const char *format, ...)
{
	char 	szDebugTemp[MAX_BUF_DEBUG_TEMP] = {0,};
	
	int nHeaderLength;

	struct 	timeval val;
	struct 	tm *ptm = NULL;
	va_list va;

#if 0 //jung2604
	// ??�뒪????�꺼 泥댄�?
	if(0 != _log_checkSystemLevel(uSystemLevel)){
		return;
	}

	// 紐⑤�???�꺼 泥댄�?
	if(0 != _log_checkModuleLevel(uModuleLevel)){
		return;
	}
#endif
	gettimeofday( &val, NULL );
	ptm = localtime( &val.tv_sec );
	sprintf(szDebugTemp, "[%s %04d %02d/%02d %02d:%02d:%02d:%03d][%d] ", g_strModuleName, g_nLogCount++, ptm->tm_mon + 1, ptm->tm_mday
			,ptm->tm_hour, ptm->tm_min, ptm->tm_sec, (int)val.tv_usec / 1000, _log_ThreadID());


	nHeaderLength = strlen(szDebugTemp);

	va_start(va, format);
	vsprintf(szDebugTemp+strlen(szDebugTemp), format, va);
	va_end(va);

	if(0 < strlen(szDebugTemp)){
		strcat(szDebugTemp, " ");
	}

	sprintf(szDebugTemp+strlen(szDebugTemp), "@@INFO:%s(%d)", strFilename, nline);
#if 0 //jung2604
	_log_FileOut(szDebugTemp, uSystemLevel, "\n");
	_log_StdOut(szDebugTemp, uSystemLevel, TEXT_COLOR_WHITE_END);
	_log_CatOut(&szDebugTemp[nHeaderLength], uSystemLevel, TEXT_COLOR_WHITE_END);
#else
	__android_log_print(ANDROID_LOG_DEBUG  , "BOXKEY-MEDIA", "%s", &szDebugTemp[nHeaderLength]);
#endif
}

void 	log_PrintsrcEx(long uSystemLevel, long uModuleLevel, const char *strFilename, int nline, const char *format, ...)
{
	char 	szDebugStart[128] = {0,};
	char 	szDebugTail[512] = {0,};
	struct 	timeval val;
	struct 	tm *ptm = NULL;
	va_list va;
	int nTotLen=0, nArgvLen, nStartLen=0, nTailLen=0;
	char *outp=NULL;


	// ??�뒪????�꺼 泥댄�?
	if(0 != _log_checkSystemLevel(uSystemLevel)){
		return;
	}

	// 紐⑤�???�꺼 泥댄�?
	if(0 != _log_checkModuleLevel(uModuleLevel)){
		return;
	}

	gettimeofday( &val, NULL );
	ptm = localtime( &val.tv_sec );
	sprintf(szDebugStart, "[%s %04d %02d/%02d %02d:%02d:%02d:%03d][%d] ", g_strModuleName, g_nLogCount++, ptm->tm_mon + 1, ptm->tm_mday
			,ptm->tm_hour, ptm->tm_min, ptm->tm_sec, (int)val.tv_usec / 1000, _log_ThreadID());
	nStartLen = strlen(szDebugStart);
	sprintf(szDebugTail, " @@INFO:%s(%d)", strFilename, nline);
	nTailLen = strlen(szDebugTail);

	va_start(va, format);

	nArgvLen = vsnprintf(NULL, 0, format, va);
	nTotLen = nArgvLen + nStartLen + nTailLen +1;
	outp = malloc(nTotLen);
	if (!outp)
	{
		va_end(va);
		return;
	}
	memset(outp, 0x00, nTotLen);

	strcpy(outp, szDebugStart);
	vsprintf(outp+nStartLen, format, va);
	strcat(outp, szDebugTail);
	va_end(va);

	_log_FileOut(outp, uSystemLevel, "\n");
	_log_StdOut(outp, uSystemLevel, TEXT_COLOR_WHITE_END);
	_log_CatOut(&outp[nStartLen], uSystemLevel, TEXT_COLOR_WHITE_END);

	if (outp)
	{
		free(outp);
		outp = NULL;
	}
}

void 	log_PrintTime(long uSystemLevel, long uModuleLevel, const char *strFilename, int nline,  int nUsedTimeMS, const char *format, ...)
{
	char 	szDebugTemp[MAX_BUF_DEBUG_TEMP] = {0,};
	struct 	timeval val;
	struct 	tm *ptm = NULL;
	va_list va;


	// ??�뒪????�꺼 泥댄�?
	if(0 != _log_checkSystemLevel(uSystemLevel)){
		return;
	}

	// 紐⑤�???�꺼 泥댄�?
	if(0 != _log_checkModuleLevel(uModuleLevel)){
		return;
	}


	gettimeofday( &val, NULL );
	ptm = localtime( &val.tv_sec );

	if(-1 == nUsedTimeMS){
		sprintf(szDebugTemp, "[%s %04d %02d/%02d %02d:%02d:%02d:%03d][%d] Used time =>    begin ", g_strModuleName, g_nLogCount++, ptm->tm_mon + 1, ptm->tm_mday
						,ptm->tm_hour, ptm->tm_min, ptm->tm_sec, (int)val.tv_usec / 1000, _log_ThreadID());
	}
	else{
		sprintf(szDebugTemp, "[%s %04d %02d/%02d %02d:%02d:%02d:%03d][%d] Used time => %6ldms ", g_strModuleName, g_nLogCount++, ptm->tm_mon + 1, ptm->tm_mday
				,ptm->tm_hour, ptm->tm_min, ptm->tm_sec, (int)val.tv_usec / 1000, _log_ThreadID(), nUsedTimeMS);
	}


	va_start(va, format);
	vsprintf(szDebugTemp+strlen(szDebugTemp), format, va);
	va_end(va);

	if(0 < strlen(szDebugTemp)){
		strcat(szDebugTemp, " ");
	}

	sprintf(szDebugTemp+strlen(szDebugTemp), "@@INFO:%s(%d)", strFilename, nline);

	_log_FileOut(szDebugTemp, uSystemLevel, "\n");
	_log_StdOut(szDebugTemp, uSystemLevel, TEXT_COLOR_WHITE_END);
	_log_CatOut(szDebugTemp, uSystemLevel, TEXT_COLOR_WHITE_END);
}


void log_PrintDump(const void* pData, long dwSize)
{
#define __BLOCK__        8
    int i;
    int j;
    int nLoop;
    long dwIndex = 0;
    long dwPos = 0;
    long dwAddress = 0;
    char byBlock1[__BLOCK__+1];
    char byBlock2[__BLOCK__+1];
    char szBlock1[__BLOCK__+1];
    char szBlock2[__BLOCK__+1];
    char szLog[512];
    char szTemp[512];
    char* pTemp = (char*)pData;



    snprintf(szLog, 512, (char*)("memory addess:0x%08x size:%d"), (int)pData, (int)dwSize);
    LOGEX(szLog);
    nLoop = dwSize/(__BLOCK__*2);

    if(dwSize%(__BLOCK__*2)){
    	nLoop += 1;
    }


    for(i=0; i<nLoop; i++){
        memset(byBlock1, 0x00, __BLOCK__);
        memset(byBlock2, 0x00, __BLOCK__);

        if(dwSize <= dwIndex+__BLOCK__){
            if(dwSize>dwIndex){
                memcpy(byBlock1, pTemp+dwIndex, dwSize-dwIndex);
                dwAddress = (long)(pTemp+dwIndex);
                dwIndex += __BLOCK__;
            }
        }
        else{
            memcpy(byBlock1, pTemp+dwIndex, __BLOCK__);
            dwAddress = (long)(pTemp+dwIndex);
            dwIndex += __BLOCK__;
        }

        if(dwSize <= dwIndex+__BLOCK__){
            if( dwSize>dwIndex){
                memcpy(byBlock2, pTemp+dwIndex, dwSize-dwIndex);
                dwIndex += __BLOCK__;
            }
        }
        else{
            memcpy(byBlock2, pTemp+dwIndex, __BLOCK__);
            dwIndex += __BLOCK__;
        }

        for(j=0; j<__BLOCK__; j++){
            szBlock1[j] = (char)byBlock1[j];
            if( '\t' == szBlock1[j] || '\r'== szBlock1[j] || '\n'== szBlock1[j] || 0 == isgraph(szBlock1[j])){
                szBlock1[j] = '.';
            }

            szBlock2[j] = (char)byBlock2[j];
            if( '\t' == szBlock2[j] || '\r'== szBlock2[j] || '\n'== szBlock2[j] || 0 == isgraph(szBlock2[j])){
                szBlock2[j] = '.';
            }
        }

        szBlock1[__BLOCK__] = '\0';
        szBlock2[__BLOCK__] = '\0';

        snprintf(szLog, 128, "0x%08x:", (int)dwAddress);
        for(j=0; j<__BLOCK__; j++){
            if(dwSize <= dwPos++){
                strcat(szLog, "   ");
            }
            else{
                snprintf(szTemp, 128, "%02X ", (unsigned	char)byBlock1[j]);
                strcat(szLog, szTemp);
            }
        }

        strcat(szLog, " ");
        for(j=0; j<__BLOCK__; j++){
            if(dwSize <= dwPos++){
                strcat(szLog, "   ");
            }
            else{
                snprintf(szTemp, 128, "%02X ", (unsigned	char)byBlock2[j]);
                strcat(szLog, szTemp);
            }
        }

        strcat(szLog, " \t\t");
        strcat(szLog, szBlock1);
        strcat(szLog, " ");
        strcat(szLog, szBlock2);

        LOGEX(szLog);
    }
}

void 	log_PrintLogFile(const char *strLogFilePath, const char *strFilename, int nline, const char *format, ...)
{
	char 	szDebugTemp[MAX_BUF_DEBUG_TEMP] = {0,};
	struct 	timeval val;
	struct 	tm *ptm = NULL;
	va_list va;

	long 	nLogFD	 			= -1L;

	gettimeofday( &val, NULL );
	ptm = localtime( &val.tv_sec );
	sprintf(szDebugTemp, "[%s %02d/%02d %02d:%02d:%02d:%03d][%d]", g_strModuleName,  ptm->tm_mon + 1, ptm->tm_mday
			,ptm->tm_hour, ptm->tm_min, ptm->tm_sec, (int)val.tv_usec / 1000, _log_ThreadID());


	va_start(va, format);
	vsprintf(szDebugTemp+strlen(szDebugTemp), format, va);
	va_end(va);

	if(0 < strlen(szDebugTemp)){
		strcat(szDebugTemp, " ");
	}

	sprintf(szDebugTemp+strlen(szDebugTemp), "@@INFO:%s(%d)\n", strFilename, nline);

	// 2012.06.26. namsj ???�� 沅뚰�???�젙
	nLogFD = open(strLogFilePath, O_CREAT|O_APPEND|O_WRONLY, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
	if(-1 == nLogFD){
		return;
	}

	write(nLogFD, szDebugTemp, strlen(szDebugTemp));
	close(nLogFD);
}

// ?????�?�???�� 濡쒓????�????�??�??
// 1. ?몄옄�???�뼱????�컙 留뚰�????��濡쒓?�瑜???��???�떎媛�??�컙??吏�?�㈃ ?꾩옱 ???��??
//    .bak ??�줈 ???�?�?湲곗??.bak ???��??吏�?�??
// 2. 濡쒓?�瑜?理쒖?�濡?湲곕�????�컙?�??�??��?�뱢�?starttime ??�줈 湲곕�???볤�?
//    ??�쾬??�줈 ??��?�瑜???�떎.
// bytes�???寃쎌?? 理쒕? int ?�?��吏��??�떎. long long????�젮????��???�졄�?
// ?�녹???꾩슂媛�??�쓣 ??..
void 	log_FileOption(eLOGFILETYPE_Option eLogFileType, int nValue)
{
	struct stat statbuf;
	char szStartTimePath[256];
	long nStartTimeFD = -1L;
	time_t 	curTime;
	char szCurTime[65];
	char szSavedStartTime[65];

	if ((eLogFileType < e_LOGFILETYPE_TIME) || (eLogFileType >= e_LOGFILETYPE_MAX))
	{
		return;
	}
	if (nValue <= 0)
	{
		return;
	}

	s_eLogFileType = eLogFileType;
	if (s_eLogFileType == e_LOGFILETYPE_TIME)
	{
		s_nMaxBackupSec = nValue * 3600;

		curTime = time(0);
		memset(szCurTime, 0x00, 65);
		sprintf(szCurTime, "%d", (int)curTime);

		memset(&statbuf, 0x00, sizeof(struct stat));
		memset(szStartTimePath, 0x00, 256);
		strcpy(szStartTimePath, LOG_FILE_START_PATH);
		strcat(szStartTimePath, g_strModuleName);
		strcat(szStartTimePath, LOG_FILE_START_NAME);

		// ??�컙??湲곕�?????��??李얜???
		if (stat(szStartTimePath, &statbuf) < 0)
		{
			// ???��????�떎�??꾩옱 ??�컙??湲곕�??�뿬 ?�ν븳??
			// 2012.06.26. namsj ???�� 沅뚰�???�젙
			nStartTimeFD = open(szStartTimePath, O_CREAT|O_WRONLY, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
			if(-1 == nStartTimeFD)
			{
				return;
			}

			write(nStartTimeFD, szCurTime, strlen(szCurTime));
			close(nStartTimeFD);

		}

		memset(szSavedStartTime, 0x00, 65);
		nStartTimeFD = open(szStartTimePath, O_RDONLY);
		if(-1 == nStartTimeFD)
		{
			return;
		}

		read(nStartTimeFD, szSavedStartTime, 64);
		close(nStartTimeFD);

		s_nStartedFileLogTime = atoi(szSavedStartTime);
		//printf("[%s][%d] szSavedStartTime=%s, s_nStartedFileLogTime=%d, s_nMaxBackupSec=%d\n\n", __func__, __LINE__,
		//		szSavedStartTime, s_nStartedFileLogTime, s_nMaxBackupSec);
	}
	else if (eLogFileType == e_LOGFILETYPE_BYTES)
	{
		// 濡쒓?????�� 理쒕? ??린瑜?硫붾?�由?�??�ν빐 ?볥뒗??
		s_nLogMaxBytes = nValue;
		printf("[%s][%d] s_nLogMaxBytes=%d\n\n", __func__, __LINE__, s_nLogMaxBytes);
	}
	else
	{
		return;
	}
}

int fileMove(char *srcPath, char *dstPath)
{
	int retVal;
	char buff[4096];
	FILE *in_fp, *out_fp;
	size_t n_size;

	// ?�?��???�� ??�린紐⑤뱶濡???�뵂
	in_fp = fopen(srcPath, "r");
	if (in_fp == NULL)
	{
		return -1;
	}

	// ?�쿊��??곌린紐⑤뱶濡???�뵂
	out_fp = fopen(dstPath, "w");
	if (out_fp == NULL)
	{
		fclose(in_fp);
		return -1;
	}

	// ?�?�� ???��???�쿊��?�줈 蹂듭�?
	while (0 < (n_size = fread(buff, 1, sizeof(buff), in_fp)))
	{
		fwrite(buff, 1, n_size, out_fp);
	}

	// ???��蹂듭�??꾨즺 ??fp close
	fclose(in_fp);
	fclose(out_fp);

	// 蹂듭�???�?��???�� ??�굅
	retVal = unlink(srcPath);
	if (retVal == -1)
	{
		return -1;
	}

	return 0;
}

// ??�??�뒗 濡쒓?????�� ?뺤콉???곕씪 濡쒓?????��??backup ??�궎嫄곕�?湲고? ??�옉????�궓??
void log_fileOperation(const char *strLogFilePath)
{
	int nCompareTime=0;
	int nCurTime;
	char szBackupFileName[256];
	char szStartTimePath[256];
	char szCurTime[65];
	long nStartTimeFD = -1L;

	if (s_eLogFileType == e_LOGFILETYPE_TIME)
	{
		// 硫붾?�由?�???�뒗 started time + s_nMaxBackupSec ????�쓣 ?�ы빐 ?볥뒗??
		nCompareTime = s_nStartedFileLogTime + s_nMaxBackupSec;
		nCurTime = (int)time(0);
		//printf("[%s][%d] nCompareTime=%d, nCurTime=%d\n\n", __func__, __LINE__, nCompareTime, nCurTime);
		if (nCurTime > nCompareTime)
		{
			// 湲곗??backup ???��??吏�곌�? ?꾩옱 濡쒓?????��??.bak??�줈 留뚮�??
			s_nStartedFileLogTime = nCurTime;

			memset(szBackupFileName, 0x00, 256);
			strcpy(szBackupFileName, strLogFilePath);
			strcat(szBackupFileName, ".bak");
			remove(szBackupFileName);
			//sync();

			rename(strLogFilePath, szBackupFileName);

			// backup ???��??留뚮�??�떎�?XXX_started????�뒗 ??�컙???꾩옱??�컙??�줈 媛깆???�빞 ??�떎.
			memset(szCurTime, 0x00, 65);
			sprintf(szCurTime, "%d", (int)nCurTime);

			memset(szStartTimePath, 0x00, 256);
			strcpy(szStartTimePath, LOG_FILE_START_PATH);
			strcat(szStartTimePath, g_strModuleName);
			strcat(szStartTimePath, LOG_FILE_START_NAME);
			printf("log file start name : %s \n", szStartTimePath);
			remove(szStartTimePath);

			// 2012.06.26. namsj ???�� 沅뚰�???�젙
			nStartTimeFD = open(szStartTimePath, O_CREAT|O_WRONLY, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
			if(-1 == nStartTimeFD)
			{
				return;
			}

			write(nStartTimeFD, szCurTime, strlen(szCurTime));
			close(nStartTimeFD);

		}
	}
	else if (s_eLogFileType == e_LOGFILETYPE_BYTES)
	{
		struct stat statbuf;
		int nCurBytes=0;

		memset(&statbuf, 0x00, sizeof(struct stat));

		// ?꾩옱 濡쒓?????��????린瑜??�ы븳??
		if (stat(strLogFilePath, &statbuf) < 0)
		{   
            fprintf(stderr, "stat error [%s]\n", strLogFilePath);
			return;
		}
		nCurBytes = (int)statbuf.st_size;
		//printf("[%s][%d] s_nLogMaxBytes=%d, nCurBytes=%d\n\n", __func__, __LINE__, s_nLogMaxBytes, nCurBytes);
		if (nCurBytes > s_nLogMaxBytes)
		{
			// 湲곗??backup ???��??吏�곌�? ?꾩옱 濡쒓?????��??.bak??�줈 留뚮�??
			memset(szBackupFileName, 0x00, 256);
			strcpy(szBackupFileName, strLogFilePath);
			strcat(szBackupFileName, ".bak");
			remove(szBackupFileName);
			//sync();

			rename(strLogFilePath, szBackupFileName);

		}
	}
	else
	{
		return;
	}
}

void 	log_PrintLogFileEx(const char *strLogFilePath, const char *strFilename, int nline, const char *format, ...)
{
	char 	szDebugStart[128] = {0,};
	char 	szDebugTail[512] = {0,};
	struct 	timeval val;
	struct 	tm *ptm = NULL;
	va_list va;
	int nTotLen=0, nArgvLen, nStartLen=0, nTailLen=0;
	char *outp=NULL;

	long 	nLogFD	 			= -1L;

	// 濡쒓?????�� ?뺤콉???곸슜??�떎.
	log_fileOperation(strLogFilePath);

	gettimeofday( &val, NULL );
	ptm = localtime( &val.tv_sec );
	sprintf(szDebugStart, "[%s %02d/%02d %02d:%02d:%02d:%03d][%d] ", g_strModuleName,  ptm->tm_mon + 1, ptm->tm_mday
			,ptm->tm_hour, ptm->tm_min, ptm->tm_sec, (int)val.tv_usec / 1000, _log_ThreadID());
	nStartLen = strlen(szDebugStart);
	sprintf(szDebugTail, " @@INFO:%s(%d)\n", strFilename, nline);
	nTailLen = strlen(szDebugTail);

	va_start(va, format);

	nArgvLen = vsnprintf(NULL, 0, format, va);
	nTotLen = nArgvLen + nStartLen + nTailLen +1;
	outp = malloc(nTotLen);
	if (!outp)
	{
		va_end(va);
		return;
	}
	memset(outp, 0x00, nTotLen);

	strcpy(outp, szDebugStart);
	vsprintf(outp+nStartLen, format, va);
	strcat(outp, szDebugTail);

	va_end(va);

	// 2012.06.26. namsj ???�� 沅뚰�???�젙
	nLogFD = open(strLogFilePath, O_CREAT|O_APPEND | O_WRONLY, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
	if(-1 == nLogFD){
		if (outp)
		{
			free(outp);
			outp = NULL;
		}
		return;
	}

	write(nLogFD, outp, strlen(outp));
	close(nLogFD);
	if (outp)
	{
		free(outp);
		outp = NULL;
	}

}

void 	log_PrintM(long uSystemLevel, long uModuleLevel, const char *format, ...)
{
	va_list va;
	int nTotLen=0, nArgvLen;
	char *outp=NULL;


	// ??�뒪????�꺼 泥댄�?
	if(0 != _log_checkSystemLevel(uSystemLevel)){
		return;
	}

	// 紐⑤�???�꺼 泥댄�?
	if(0 != _log_checkModuleLevel(uModuleLevel)){
		return;
	}

	va_start(va, format);

	nArgvLen = vsnprintf(NULL, 0, format, va);
	nTotLen = nArgvLen + 1;
	outp = malloc(nTotLen);
	if (!outp)
	{
		va_end(va);
		return;
	}
	memset(outp, 0x00, nTotLen);
	vsprintf(outp, format, va);
	va_end(va);

//	_log_FileOut(outp, uSystemLevel, "");
	_log_StdOut(outp, uSystemLevel, TEXT_COLOR_WHITE);

	if (outp)
	{
		free(outp);
		outp = NULL;
	}


}




