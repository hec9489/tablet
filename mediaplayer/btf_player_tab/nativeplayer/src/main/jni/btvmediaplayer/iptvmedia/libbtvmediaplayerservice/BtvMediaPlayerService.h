// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef ANDROID_BTVMEDIAPLAYERSERVICE_H
#define ANDROID_BTVMEDIAPLAYERSERVICE_H

//#include <ALog.h>
#include <utils/Errors.h>
#include <memory>
#include <mutex>
#include <iptvmedia/BtvMediaPlayerInterface.h>
#include <iptvmedia/BtvMediaPlayerServiceInterface.h>
#include <iptvmedia/IBtvMediaPlayerListener.h>
#include <iptvhaldef.h>

//#include <android_runtime/android_view_Surface.h>
#include <android/native_window_jni.h>
#include <android/native_window.h>

using namespace std;
using namespace android;

class IptvSocPlayer;
class BtvMediaPlayerObserver;
class Surface;

class BtvMediaPlayerService: public BtvMediaPlayerServiceInterface
{
	class Client;
public:
    static std::shared_ptr<BtvMediaPlayerService> getInstance();
	std::shared_ptr<IptvSocPlayer> selectIptvSocPlayer(int deviceID);
	void setBtvMediaPlayer(int deviceID);
	void setBtvMediaPlayerListener(const sp<IBtvMediaPlayerListener>& listener);



private:
    static std::shared_ptr<BtvMediaPlayerService> instance_;
	std::shared_ptr<IptvSocPlayer> mpIptvSocPlayer;
    std::shared_ptr<IptvSocPlayer>	mIptvSocPlayerMain;
    std::shared_ptr<IptvSocPlayer> mIptvSocPlayerPip1;
    std::shared_ptr<BtvMediaPlayerObserver> mBtvMediaPlayerObserver;
	std::shared_ptr<BtvMediaPlayerObserver> getClientByDeviceID(int deviceID);
	BtvMediaPlayerService();
	sp<IBtvMediaPlayerListener> mBtvMediaPlayerListener;

    //vector<std::weak_ptr<BtvMediaPlayerObserver>>	mClients; //SortedVector<wp<BtvMediaPlayerObserver>>

public:
	~BtvMediaPlayerService();

	static void notifyCallbackMain(void* cookie, int msg, int ext1, int ext2, const int8_t* obj);
	static void	dataCallbackMain(void* cookie, int msg, int ext1, int ext2, const int8_t* dataPtr);

	static void	notifyCallbackPip1(void* cookie, int msg, int ext1, int ext2, const int8_t* obj);
	static void	dataCallbackPip1(void* cookie, int msg, int ext1, int ext2, const int8_t* dataPtr);
													   
	static void audioProgramUpdateCallback(void *cookie, char* filepath, int32_t updateType,
										int32_t audioPid);

    int32_t connect(int deviceID);
	int32_t open(int deviceID, const char* dataXML);
	int32_t tuneTV(int deviceID, const char* dataXML);
	int32_t closeTV(int deviceID);
	int32_t bindingFilter(int deviceID, const char* dataXML);
	int32_t releaseFilter(int deviceID, const char* dataXML);
	int32_t changeAudioChannel(int deviceID, const char* dataXML);
	int32_t changeAudioOnOff(int deviceID, const char* dataXML);
	int32_t getPlayerMode(int deviceID);
	int32_t getPlayerStatus(int deviceID);
	int32_t getCurrentPosition(int deviceID);
	int32_t setWindowSize(int deviceID, const char* dataXML);
	int32_t play(int deviceID);
	int32_t pause(int deviceID);
	int32_t resume(int deviceID);
	int32_t seek(int deviceID, const char* dataXML, bool pause);
	int32_t trick(int deviceID, const char* dataXML);
	int32_t stop(int deviceID);
	int32_t close(int deviceID);
	int32_t setPlayerSize(int deviceID, int nLeft, int nTop, int nWidth, int nHeight);
	int32_t startDmxFilter(int deviceID, int32_t pid, int32_t tid);
	int32_t stopDmxFilter(int deviceID, int32_t pid, int32_t tid);
	int32_t setDummys(int deviceID, const char* dummys);
	void disconnect(int deviceID);
	int32_t pauseAt(int deviceID, const char* dataXML);
	int32_t keepLastFrame(int deviceID, bool flag);

	int32_t setStbId(const char* stbId);
	int32_t setMacAddress(const char* macAddr);
	int32_t setGwToken(const char* gwToken);
	int32_t setVisionImpaired(bool visionImpaired);
	int32_t setHearingImpaired(bool hearingImpaired);
	int32_t setVideoScalingMode(int scalingMode);
    bool getMlrActivationEnable();
	int32_t setMlrActivationEnable(bool mlractivationEnable);
	//int32_t setPlayerSurfaceView(int deviceID, uint64_t nativWindows);
	int32_t setPlayerSurfaceView(int deviceID, uint64_t nativWindows); //for 64bit build
	int32_t setPackageInternalPath(const char* internalPath);
	//int32_t setPlayerSurface(int deviceID, const std::shared_ptr<Surface> &surface);
    std::shared_ptr<BtvMediaPlayerObserver>   mObserver;
	void parseAudioChangeInfo(const char* dataXML, int *nAID, int *nACodecID);
    HPlayer_IPTV_Info parsePlayerInfo(const char* dataXML);
    int32_t enableCasInfo(bool enable);
	int32_t setTtaTestOption(const char* key, const char* value);
	int32_t setAVOffset(int type, int nTimeMs);

private:
	std::mutex mServiceLock;
};

#endif // ANDROID_BTVMEDIAPLAYERSERVICE_H
