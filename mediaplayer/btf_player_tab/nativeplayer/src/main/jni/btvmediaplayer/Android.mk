# Copyright 2019 SK Broadband Co., LTD.
# Author: Kwon Soon Chan (sysc0507@sk.com)

LOCAL_PATH:= $(call my-dir)

##############################
include $(CLEAR_VARS)
LOCAL_MODULE := libCASClient
LOCAL_SRC_FILES := ./libs/casdrmImpl/lib/cas/libCASClient.so
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_PROPRIETARY_MODULE := true
#include $(BUILD_PREBUILT)
include $(PREBUILT_SHARED_LIBRARY)

###########################################################################################
# libdsmcc.so
###########################################################################################
include $(CLEAR_VARS)

LOCAL_MODULE := libdsmcc

LOCAL_CFLAGS += -Wall -Wunused-parameter -Wmismatched-tags
LOCAL_CFLAGS += -D__LINUX__
LOCAL_LDLIBS := -llog

LOCAL_SRC_FILES := \
  libs/libdsmcc/dsmcc.c \
  libs/libdsmcc/dsmcc-receiver.c \
  libs/libdsmcc/dsmcc-descriptor.c \
  libs/libdsmcc/dsmcc-carousel.c \
  libs/libdsmcc/dsmcc-cache.c \
  libs/libdsmcc/dsmcc-biop.c \
  libs/libdsmcc/dsmcc-util.c

LOCAL_C_INCLUDES :=  \
  $(LOCAL_PATH)/libs/libdsmcc

LOCAL_LDLIBS := -llog -lz

LOCAL_MODULE_TAGS := optional
include $(BUILD_SHARED_LIBRARY)

###########################################################################################
# libswdmx.so
###########################################################################################
include $(CLEAR_VARS)

LOCAL_MODULE := libswdmx

LOCAL_CFLAGS += -Wall -O2 -Wunused-parameter -Wmismatched-tags

LOCAL_SRC_FILES := \
  libs/libdmx/demux.c \
  libs/libdmx/demux_section.c \
  libs/libdmx/demux_pes.c

LOCAL_C_INCLUDES :=  \
  $(LOCAL_PATH)/libs/libdmx

LOCAL_LDLIBS := -llog

LOCAL_MODULE_TAGS := optional
include $(BUILD_SHARED_LIBRARY)


###########################################################################################
# libdbg.so
###########################################################################################
include $(CLEAR_VARS)

LOCAL_MODULE := libdbg

LOCAL_CFLAGS += -Wall -Wunused-parameter -Wmismatched-tags
LOCAL_LDLIBS := -llog

LOCAL_SRC_FILES := \
  libs/libmediaplayer_primitive/dbg/dbg.c

LOCAL_C_INCLUDES :=  \
  $(LOCAL_PATH)/libs/libmediaplayer_primitive/dbg/

LOCAL_MODULE_TAGS := optional
include $(BUILD_SHARED_LIBRARY)
