// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef SECTIONFILTER_H_
#define SECTIONFILTER_H_
#include <stdio.h>
#include <stdlib.h>

class SectionFilter {
public:
	SectionFilter();
	virtual ~SectionFilter();
public:
	static SectionFilter* getInstance();

	int Initalize();
	int sectionfilter_test(const char* path);

private:
	static SectionFilter* m_pInstance;
};

#endif /* SECTIONFILTER_H_ */
