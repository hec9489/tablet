// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef ANDROID_BTV_MEDIA_PLAYER_SERVICE_INTERFACE_H
#define ANDROID_BTV_MEDIA_PLAYER_SERVICE_INTERFACE_H

#ifdef __cplusplus

#include <string>

class BtvMediaPlayerServiceInterface
{
public:
	virtual ~BtvMediaPlayerServiceInterface() { }
	virtual void onCasInfoEvent(int32_t type, std::string casInfo) { }
};

#endif // __cplusplus

#endif // ANDROID_BTV_MEDIA_PLAYER_SERVICE_INTERFACE_H

