//
// Created by parkjeongho on 2021-04-06.
//

#ifndef BTF_PLAYER_TAB_IBTVMEDIAPLAYERLISTENER_H
#define BTF_PLAYER_TAB_IBTVMEDIAPLAYERLISTENER_H

#ifdef __cplusplus

#include <utils/RefBase.h>
#include <utils/Vector.h>
#include <utils/Mutex.h>

namespace android {
	class IBtvMediaPlayerListener : public RefBase {
	public:
		IBtvMediaPlayerListener() {}
		virtual ~IBtvMediaPlayerListener() {}

		virtual void notifyCallback(int32_t msg, int32_t ext1, int32_t ext2, const int8_t *data) = 0;
		virtual void dataCallback(int32_t msg, int32_t ext1, int32_t ext2, const int8_t *data) = 0;

	private:

	};
}

#endif // __cplusplus

#endif //BTF_PLAYER_TAB_IBTVMEDIAPLAYERLISTENER_H
