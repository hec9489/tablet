// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef _IPTV_TIMED_EVENT_QUEUE_H_
#define _IPTV_TIMED_EVENT_QUEUE_H_

#include <pthread.h>

#include <list>
#include <mutex>
#include <threads.h>

//#include <iostream>
//namespace android {
//}
//
//using namespace android;
using namespace std;

struct IptvTimedEventQueue
{
	typedef int32_t event_id;

	struct Event
	{
		Event()
			: mEventID(0)
		{

		}
		virtual ~Event() {}

		event_id eventID()
		{
			return mEventID;
		}

	protected:
		virtual void fire(IptvTimedEventQueue *queue, int64_t now_us) = 0;

	private:
		friend class IptvTimedEventQueue;

		event_id mEventID;

		void setEventID(event_id id)
		{
			mEventID = id;
		}

		Event(const Event &);
		Event &operator=(const Event &);
	};

public:
	IptvTimedEventQueue();
	~IptvTimedEventQueue();

	// start executing the event loop.
	void start();

	// Stop executing the event loop, if flush is false, any pending
	// events are discarded, otherwise the queue will stop (and this call
	// return) once all pending events have been handled.
	void stop(bool flush = false);

	// Posts an event to the front of the queue (after all events that
	// have previously been posted to the front but before timed events).
	event_id postEvent(const std::shared_ptr<Event> &event);

	event_id postEventToBack(const std::shared_ptr<Event> &event);

	// It is an error to post an event with a negative delay.
	event_id postEventWithDelay(const std::shared_ptr<Event> &event, int64_t dealy_us);

	// If the event is to be posted at a time that has already passed, 
	// it will fire as soon as possible.
	event_id postTimedEvent(const std::shared_ptr<Event> &event, int64_t realtime_us);

	// Returns true if event is currently in the queue and has been
	// successfully canceled. In this case the event will have been 
	// removed from the queue and won't fire
	bool cancelEvent(event_id id);

	// Returns true if event is currently in the queue and has been
	// successfully canceled. In this case the event will have been 
	// removed from the queue and won't fire
	bool cancelAllEvent();

	// Cancel any pending event that satisfies the predicate.
	// if sotpAfterFirestMatch is true, only cancels the first event
	// satisfying the predicate (if any).
	void cancelEvents(
			bool (*predicate)(void *cookie, const std::shared_ptr<Event> &event), 
			void *cookie, 
			bool stopAfterFirstMatch = false);

	static int64_t getRealTimeUs();

private:
	struct QueueItem
	{
		std::shared_ptr<Event> event;
		int64_t realtime_us;
	};

	struct StopEvent : public IptvTimedEventQueue::Event
	{
		virtual void fire(IptvTimedEventQueue *queue, int64_t now_us)
		{
			queue->mStopped = true;
		}
	};

	pthread_t 				mThread;
	std::list<QueueItem> 		mQueue;
	std::mutex               mLock;
	std::condition_variable 				mQueueNotEmptyCondition;
	std::condition_variable  				mQueueHeadChangedCondition;
	event_id				mNextEventID;

	bool					mRunning;
	bool					mStopped;

	static void 			*ThreadWrapper(void *me);
	void					threadEntry();

	std::shared_ptr<Event>				removeEventFromQueue_l(event_id id);

	IptvTimedEventQueue(const IptvTimedEventQueue &);
	IptvTimedEventQueue &operator=(const IptvTimedEventQueue &);

};


#endif // _IPTV_TIMED_EVENT_QUEUE_H_