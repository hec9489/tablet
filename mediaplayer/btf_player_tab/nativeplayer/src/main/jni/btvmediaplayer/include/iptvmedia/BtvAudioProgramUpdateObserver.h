// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef ANDROID_BTVAUDIOPROGRAMUPDATEOBSERVER_H
#define ANDROID_BTVAUDIOPROGRAMUPDATEOBSERVER_H

#ifdef __cplusplus

#include <utils/Errors.h>
#include <utils/RefBase.h>
#include <utils/threads.h>
#include <utils/Vector.h>

namespace android
{

class BtvAudioProgramUpdateObserver: public RefBase
{
public:
	BtvAudioProgramUpdateObserver() {}
	virtual			~BtvAudioProgramUpdateObserver() {}
	virtual void	audioProgramUpdate(char* filepath, int32_t updateType, int32_t audioPid) = 0;

private:
	Mutex				mNotifyLock;
};

};	// namespace android

#endif // __cplusplus

#endif // ANDROID_BTVAUDIOPROGRAMUPDATEOBSERVER_H

