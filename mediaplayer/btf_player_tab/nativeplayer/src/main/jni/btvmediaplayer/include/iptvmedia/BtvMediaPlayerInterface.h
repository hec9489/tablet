// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef ANDROID_BTVMEDIAPLAYERINTERFACE_H
#define ANDROID_BTVMEDIAPLAYERINTERFACE_H

#ifdef __cplusplus

#include <threads.h>
#include <mutex>

#include "BtvDsmccObserver.h"

namespace android
{ }
using namespace std;

// callback mechanism for passing messages to IptvMediaPlayer object
typedef void (*notify_callback_f)(void* cookie, int msg, int ext1, int ext2, const int8_t* obj);
typedef void (*data_callback_f)(void* cookie, int msg, int ext1, int ext2, const int8_t* dataPtr);
typedef void (*dsmcc_callback_f)(void* cookie, int32_t dsmccEventType, char* dsmccRootPath, SignalEvent* event);
typedef void (*audio_programupdate_callback_f)(void* cookie, char* filepath, int updateType, int audioPid);

// abstract base class - use BtvMediaPlayerInterface
class IptvMediaPlayerBase
{
public:
									IptvMediaPlayerBase() : mCookie(0), mNotifyCallback(0), mDataCallback(0), mDsmccCallback(0), mAudioProgramUpdateCallback(0){}
	virtual							~IptvMediaPlayerBase() {}

    virtual int32_t				open(const char* dataXML) = 0;
	virtual int32_t 				tuneTV(const char* dataXML) = 0;
	virtual int32_t				closeTV() = 0;
	virtual int32_t				bindingFilter(const char *dataXML) = 0;
	virtual int32_t				releaseFilter(const char *dataXML) = 0;
	virtual int32_t 				changeAudioChannel(const char* dataXML) = 0;
	virtual int32_t 				changeAudioOnOff(const char* dataXML) = 0;
	virtual int32_t				getPlayerMode() = 0;
	virtual int32_t				getPlayerStatus() = 0;
	virtual int32_t				getCurrentPosition() = 0;
	virtual int32_t 				setWindowSize(const char* dataXML) = 0;
	virtual int32_t				play() = 0;
	virtual int32_t				pause() = 0;
	virtual int32_t 				resume() = 0;
	virtual int32_t 				seek(const char* dataXML, int flag) = 0;
	virtual int32_t 				pauseAt(const char* dataXML) = 0;
	virtual int32_t 				trick(const char* dataXML) = 0;
	virtual int32_t 				stop() = 0;
	virtual int32_t 				close() = 0;
//	virtual int32_t				invoke(const Parcel& request, Parcel *reply) = 0;
	virtual	int32_t				reset() = 0;

	// Invoke a generic method on the player by using opaque parcels
	// for the request and reply.
	//
	// @param request Parcel that is positioned at the start of the
	//				  data sent by the java layer.
	// @param[out] reply Parcel to hold the reply data. Cannot be null.
	// @return OK if the call was successful.
	//virtual int32_t 		invoke(const Parcel& request, Parcel *reply) = 0;

	void setCallbacks(void* cookie, notify_callback_f notifyFunc, data_callback_f dataFunc)
	{
		std::lock_guard<std::mutex> lock(mNotifyLock);
		mCookie = cookie;
		mNotifyCallback = notifyFunc;
		mDataCallback = dataFunc;
	}

	void setDsmccCallbacks(void* cookie, dsmcc_callback_f dsmccFunc)
	{
		std::lock_guard<std::mutex> lock(mNotifyLock);
		mCookie = cookie;
		mDsmccCallback = dsmccFunc;
	}

	void setAudioProgramUpdateCallbacks(void* cookie, audio_programupdate_callback_f audio_updateFunc)
	{
		std::lock_guard<std::mutex> lock(mNotifyLock);
		mCookie = cookie;
		mAudioProgramUpdateCallback = audio_updateFunc;
	}

	void sendEvent(int32_t msg, int32_t ext1=0, int32_t ext2=0, const int8_t* obj = NULL)
	{
		std::lock_guard<std::mutex> lock(mNotifyLock);
		if (mNotifyCallback)
			mNotifyCallback(mCookie, msg, ext1, ext2, obj);
	}

	void sendDataEvent(int32_t msg, int32_t ext1, int32_t ext2, const int8_t* dataPtr)
	{
		std::lock_guard<std::mutex> lock(mNotifyLock);
		if (mDataCallback)
			mDataCallback(mCookie, msg, ext1, ext2, dataPtr);
	}

	void sendDsmccEvent(int32_t dsmccEventType, char* dsmccRootPath, SignalEvent* event)
	{
		std::lock_guard<std::mutex> lock(mNotifyLock);
		if (mDsmccCallback)
			mDsmccCallback(mCookie, dsmccEventType, dsmccRootPath, event);
	}

	void sendAudioProgramUpdateEvent(char* filepath, int updateType, int audioPid)
	{
		std::lock_guard<std::mutex> lock(mNotifyLock);
		if (mAudioProgramUpdateCallback)
			mAudioProgramUpdateCallback(mCookie, filepath, updateType, audioPid);
	}

	void setDeviceID(int deviceID)
	{
		mDeviceID = deviceID;
	}

	int getDeviceID()
	{
		return mDeviceID;
	}

private:
	friend class BtvMediaPlayerService;

	std::mutex				mNotifyLock;
	void*				mCookie;
	notify_callback_f	mNotifyCallback;
	data_callback_f 	mDataCallback;
	dsmcc_callback_f 	mDsmccCallback;	
	audio_programupdate_callback_f 	mAudioProgramUpdateCallback;		
	int 				mDeviceID;


};

// Implement this class for iptv media players
class BtvMediaPlayerInterface : public IptvMediaPlayerBase
{
public:
	virtual						~BtvMediaPlayerInterface() { }
};


#endif // __cplusplus

#endif // ANDROID_BTVMEDIAPLAYERINTERFACE_H

