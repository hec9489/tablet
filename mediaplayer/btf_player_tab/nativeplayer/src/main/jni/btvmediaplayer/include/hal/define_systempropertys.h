// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef _DEFINE_SYSTEMPROPERTYS_H_
#define _DEFINE_SYSTEMPROPERTYS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <systemproperty.h>

#define PROPERTY__STB_ID						"STB_ID" // G/S, Secure
#define PROPERTY__USER_ID						"USER_ID" // G/S, Secure
#define PROPERTY__USER_NAME					"USER_NAME" // G/S, Secure
#define PROPERTY__USER_ADDR					"USER_ADDR" // G/S, Secure
#define PROPERTY__STB_PASSWORD					"STB_PASSWORD" // G/S, Secure

#define PROPERTY__ETHERNET_MAC					"ETHERNET_MAC" // G
#define PROPERTY__GW_TOKEN                   "GW_TOKEN"
#define PROPERTY__SERIAL_NUMBER				"SERIAL_NUMBER" // G
#define PROPERTY__MODEL_NAME					"MODEL_NAME" // G
#define PROPERTY__FIRMWARE_VERSION				"FIRMWARE_VERSION" // G
#define PROPERTY__FIRMWARE_BUILD_DATE			"FIRMWARE_BUILD_DATE" // G
#define PROPERTY__FIRMWARE_UPDATE_DATE			"FIRMWARE_UPDATE_DATE" // G
#define PROPERTY__PURCHASE_CERTIFICATION		"PURCHASE_CERTIFICATION" // G/S
#define PROPERTY__COMMON_ADULT_CLASS			"COMMON_ADULT_CLASS" // G/S
#define PROPERTY__PICTURE_RATIO				"PICTURE_RATIO" // G/S
#define PROPERTY__TV_TYPE						"TV_TYPE" // G/S
#define PROPERTY__BRIGHTNESS					"BRIGHTNESS" // G/S
#define PROPERTY__CONTRAST						"CONTRAST" // G/S
#define PROPERTY__SATURATION					"SATURATION" // G/S
#define PROPERTY__HUE							"HUE" // G/S
#define PROPERTY__SOUND						"SOUND" // G/S
//#define PROPERTY__MEDIA_VOLUME					"MEDIA_VOLUME" // G/S
//#define PROPERTY__BELL_VOLUME					"BELL_VOLUME" // G/S
//#define PROPERTY__CALL_VOLUME					"CALL_VOLUME" // G/S
#define PROPERTY__CONSECUTIVE_PLAY				"CONSECUTIVE_PLAY" // G/S
#define PROPERTY__AUTO_HOME					"AUTO_HOME" // G/S
#define PROPERTY__RESERVATION_TIME				"RESERVATION_TIME" // G/S
#define PROPERTY__CHILDREN_SEE_LIMIT			"CHILDREN_SEE_LIMIT" // G/S
#define PROPERTY__CHILDREN_SEE_LIMIT_TIME		"CHILDREN_SEE_LIMIT_TIME" // G/S
#define PROPERTY__ADULT_MENU					"ADULT_MENU" // G/S
#define PROPERTY__BTV_ADULT_CLASS				"BTV_ADULT_CLASS" // G/S
#define PROPERTY__CHILDREN_SEE_REMAIN_TIME		"CHILDREN_SEE_REMAIN_TIME" // G/S
#define PROPERTY__SLEEP_MODE					"SLEEP_MODE" // G/S
#define PROPERTY__ACTUAL_CUSTOMER				"ACTUAL_CUSTOMER" // G/S
#define PROPERTY__MINI_TV						"MINI_TV" // G/S

#define PROPERTY__STATE_RCU_SKT				"STATE_RCU_SKT" // G

#define PROPERTY__LASTFRAME			"LASTFRAME" // 1 / 0

#define PROPERTY__HEARING_IMPAIRED				"HEARING_IMPAIRED" // G/S
#define PROPERTY__VISION_IMPAIRED				"VISIONIMPAIRED" // G/S
/*

#define property_get(name, value, default_value) \
    do { \
        if(get_systemproperty(name, value, sizeof(value)) != 0)  \
            strcpy(value, default_value); \
        } \
    } while(0)
*/

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* _DEFINE_SYSTEMPROPERTYS_H_ */
