#ifndef _audioMirrorProxy_h_
#define _audioMirrorProxy_h_

#define AM_STATUS_F_PAIRED			0x00000001
#define AM_STATUS_F_TVAUDIO			0x00000002

#ifdef __cplusplus
extern "C" {
#endif

	int TI2_open(void);
	int TI2_close(void);
	
	int TI2_getVideoPTSOffsetMax(void);
	int TI2_setVideoPTSOffsetMax(int videoPTSOffsetMax);

	unsigned int TI2_checkStatus(void);
	
	int TI2_writePCM(unsigned char *data, size_t dataLength);
	int TI2_writeSilence(void);
	
#ifdef __cplusplus
}
#endif


#endif //_audioMirrorProxy_h_

