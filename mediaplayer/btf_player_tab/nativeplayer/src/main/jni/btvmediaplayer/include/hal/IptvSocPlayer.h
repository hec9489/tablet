// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef ___IPTV_SOC_PLAYER_H__
#define ___IPTV_SOC_PLAYER_H__

#ifdef FEATURE_SOC_AMLOGIC
//#include <gui/Surface.h>
//#include <android/native_window.h>
#endif

#include <iptvmedia/BtvMediaPlayerInterface.h>
#include <iptvmedia/BtvMediaPlayerServiceInterface.h>
#include "IptvTimedEventQueue.h"
#include "iptvhaldef.h"

//#include <dbg.h>
#include "dvb_primitive.h"

#include "iptvmedia/util_logmgr.h"

#include <mutex>
#include <android/native_window_jni.h>
#include <android/native_window.h>

//#include <parcel.h>

//namespace android { }

using namespace std;

    class IptvBtvEvent;
    class Surface;

    class IptvSocPlayer : public BtvMediaPlayerInterface
    {
    public:
        IptvSocPlayer();
        virtual								~IptvSocPlayer();

        virtual	int32_t					initCheck(BtvMediaPlayerServiceInterface *pOwnerService, int deviceID);
        virtual int32_t					getAuthChecked();
        virtual	int32_t					CertCheck();
        virtual int32_t					open(const char* dataXML);
        virtual int32_t					tuneTV(const char* dataXML);
        virtual int32_t					closeTV();
        virtual int32_t					bindingFilter(const char* dataXML);
        virtual int32_t					releaseFilter(const char* dataXML);
        virtual int32_t					changeAudioChannel(const char* dataXML);
        virtual int32_t					changeAudioOnOff(const char* dataXML);
        virtual int32_t					getPlayerMode();
        virtual int32_t					getPlayerStatus();
        virtual int32_t					getCurrentPosition();
        virtual int32_t					setWindowSize(const char* dataXML);
        virtual int32_t					play();
        virtual int32_t					pause();
        virtual int32_t					resume();
        virtual int32_t					seek(const char* dataXML, int flag);
        virtual int32_t					pauseAt(const char* dataXML);
        virtual int32_t					trick(const char* dataXML);
        virtual int32_t					stop();
        virtual int32_t					close();
//        virtual int32_t					invoke(const Parcel& request, Parcel *reply);
        virtual int32_t					reset();
        void 						notifyListener_l(int32_t msg, int32_t ext1, int32_t ext2, const char* strData);
        void 						dataListener_l(int32_t msg, int32_t ext1, int32_t ext2, const char* strData);		
        void 						dsmccListener_l(int32_t dsmccEventType, char* dsmccRootPath, SignalEvent* event);		
		void 						audioprogramListener_l(char* filePath, int updateType, int audioPid);

    public:
        friend 	class 						IptvBtvEvent;

        IptvSocPlayer(const IptvSocPlayer &);
        IptvSocPlayer			&operator=(const IptvSocPlayer &);

        mutable std::mutex 						mLock;
        mutable std::mutex 						mLockStopFile;
        mutable std::mutex 						mLockRTSPGetTimeStamp;

        IptvTimedEventQueue			mQueue;
        bool						mQueueStarted;
        IptvTimedEventQueue			mQueueSecond;
        bool						mQueueSecondStarted;
        IptvTimedEventQueue			mQueueTimeCheck;
        bool						mQueueTimeCheckStarted;

        std::shared_ptr<IptvTimedEventQueue::Event> 	mSocketBufferCheckEvent;
        std::shared_ptr<IptvTimedEventQueue::Event>  mFilePlayTimeCheckEvent;
        std::shared_ptr<IptvTimedEventQueue::Event> 	mFileCloseTimeCheckEvent;
#ifdef USE_RTSP
        std::shared_ptr<IptvTimedEventQueue::Event> 	mRTSPPlayTimeCheckEvent;
#endif
        std::shared_ptr<IptvTimedEventQueue::Event> 	mAVPTSTimeCheckEvent;
        std::shared_ptr<IptvTimedEventQueue::Event> 	mAVEventCheckEvent;
#ifdef USE_RTSP
        std::shared_ptr<IptvTimedEventQueue::Event> 	mConstructCurrentTimeEvent;
#endif
        std::shared_ptr<IptvTimedEventQueue::Event> 	mChangeAudioLanguageCheckEvent;

        bool							mSocketBufferCheckEventStop;

        void						onSocketBufferCheckEvent();
        void						onFilePlayTimeCheckEvent();
        void						onFileCloseTimeCheckEvent();
#ifdef USE_RTSP
        void						onRTSPPlayTimeCheckEvent();
#endif
        void						onRecvDtvCCData(int what, int arg1, int arg2, const char* strData);
        void						onRecvDsmccData(int event, char* path, void *data);		
        void						onAVPTSTimeCheckEvent();
        void						onAVEventCheckEvent();
        void						onConstructCurrentTimeEvent();

    public:
        int32_t					open_l(HPlayer_IPTV_Info stPlayInfo);
#ifdef USE_RTSP
        int32_t					open_rtsp_l(HPlayer_IPTV_Info stPlayInfo);
#endif
        int32_t					open_file_l(HPlayer_IPTV_Info stPlayInfo);

#ifdef USE_RTSP
        int32_t					play_rtsp_l(int nEnforcePlay);
#endif
        int32_t					play_file_l();
        int32_t					tuneTV_l(HTuner_IPTV_Info stTuneInfo);
#ifdef USE_MULTIVIEW
        int32_t					tuneTV_Multiview_l(HTuner_IPTV_Info *ptTuneInfo);
#endif
        int32_t					bindingFilter_l(int nIndex, int nPID, int nTableID, int nTimeOut);
        int32_t					releaseFilter_l(int nIndex);
        int32_t					changeAudioChannel_l(int nAPID, int nACodecID);
        int32_t					changeAudioOnOff_l(int audioenable);
        //int32_t					setPlayerSurfaceView_l(int deviceID, uint32_t nativWindows);
        int32_t					setPlayerSurfaceView_l(int deviceID, uint64_t nativWindows); //for 64bit build
#if 0//def FEATURE_SOC_AMLOGIC
        int32_t					setPlayerSurface_l(int deviceID, const std::shared_ptr<Surface> &surface); 
#endif
        int32_t					setWindowSize_l(int nX, int nY, int nWidth, int nHeight);
        int32_t					setVideoDelay_l(int delay);
        int32_t					changeGlobalAudioMute_l();
        int32_t					changeSettingDolby();
        int32_t					changeAspectRatioMode();
        AVP_AspectRatioMode     getAspectRationMode();
        void setAspectRationMode(int rationMode);
#ifdef USE_RTSP
        int32_t					stop_rtsp_l();
#endif
        int32_t					stop_file_l(int isEOF);
        int32_t					stop_tv_l();
#ifdef USE_RTSP
        int32_t					close_rtsp_l(int nStopFlag);
#endif
        int32_t					close_file_l();
        int32_t					seek_l(long nSec, int puaseFlag);
        int32_t					pauseAt_l(long nMs);
		int32_t					keepLastFrame(int flag);
        int32_t					trick_l(int nTrick);
        int32_t					reset_l();
        int32_t					setLastFrame(int enableLastFrame);
        int32_t					startDmxFilter(int32_t pid, int32_t tid);
        int32_t					stopDmxFilter(int32_t pid, int32_t tid);
        int32_t                 setAVOffset_l(int type, int time_ms);
        // enum replace const static
        const static int					mBTV_DEVICE_MAIN = 0;
        const static int 					mBTV_DEVICE_PIP1 = 1;

    private:
        void						postFilePlayTimeCheckEvent_l(int64_t delayUs);
        void						postFileCloseTimeCheckEvent_l(int64_t delayUs);
        void						postSocketBufferCheckEvent_l(int64_t delayUs);
#ifdef USE_RTSP
        void						postRTSPPlayTimeCheckEvent_l(int64_t delayUs);
#endif
        void						postAVPTSTimeCheckEvent_l(int64_t delayUs);
        void						postAVEventCheckEvent_l(int64_t delayUs);
#ifdef USE_RTSP
        void						postConstructCurrentTimeEvent_l(int64_t delayUs);
#endif


        //    linuxdvb implement all... default private value
    public: dvb_player_t player;
        dvb_locator_t locator;

        dvb_locator_t locator_create(HTuner_IPTV_Info stTuneInfo);
        dvb_locator_t locator_create(HPlayer_IPTV_Info stPlayInfo);
        int _TuneChannel(HTuner_IPTV_Info stTuneInfo);
        int _TuneMultiviewChannel(HTuner_IPTV_Info *ptTuneInfo);
        int _ChangeChannel(HTuner_IPTV_Info stTuneInfo);

    public:
        /////////////////////////////////////////////////////////////////////////
        // player status api
        /////////////////////////////////////////////////////////////////////////

        int	InGetDeviceOpenStatus();
        void					InSetDeviceOpenStatus(BTV_DEVICE_STATUS eStatus);
        int	InGetPlayerStatus();
        void					InSetPlayerStatus(BTV_PLAYER_STATUS eStatus);
        int		InGetPlayerMode();
        void					InSetPlayerMode(BTV_PLAYER_MODE eMode);

        int SendFilePlayStatus(int nMilisec, BTV_FILE_PLAY_STATUS eStatus, int isEOF = 0);

#ifdef USE_RTSP
        int SendRTSPPlayStatus(int nMilisec, BTV_RTSP_PLAY_STATUS eStatus);
        int SendRTSPSeekCompleteEvent(); //juing2604 : 추가..살이있는 동아에서 Seek 완료후 메시지 받기를 원한..
        int SendRTSPErrorStatus(int nErrorCode, char *szIp, int nPort);

        long long  GetRTSPTimeStamp(int *nTimeChanged);
        int WaitTimeMatch(long long nCorrectTimeMS, int nMinusIntervalMS, int nPlusIntervalMS, int nRetryCount, int nChangeCheck, int nFirstFrameCheck);
#endif
    public:
        /////////////////////////////////////////////////////////////////////////
        // player ?? status?? ?????????? ???o? flag
        /////////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////
        // common ????

        // (2013.06.04)
        // device open/close ????
        // int ?? ?????? ??? ??????? 1byte ???????? ??? module ???? sync ?? ??´? ?????? ?????? ???????.
        int		eDeviceOpenStatus;

        // (2013.06.04)
        // ???? play status ?? ???????.
        // int ?? ?????? ??? ??????? 1byte ???????? ??? module ???? sync ?? ??´? ?????? ?????? ???????.
        int		ePlayerStatus;

        // (2013.06.04)
        // ???? play status ?? ???????.
        // int ?? ?????? ??? ??????? 1byte ???????? ??? module ???? sync ?? ??´? ?????? ?????? ???????.
        int			ePlayerMode;

        /////////////////////////////////////////////////////////////////////////
        // tune ????

        // (2013.06.04)
        // tune ???? ?????? ???????. bad->good , bad->bad, good->bad ?????
        // event ?? app ?? ?????? ??μ? ??? u???? ????? ??????.
        int nLastSignalStatus;

        /////////////////////////////////////////////////////////////////////////
        // tune ????

        // u??° frame ?? ?????????? u????.
        int nFirstFrameChanged;

        /////////////////////////////////////////////////////////////////////////
        // file play ????

        // (2013.06.04)
        // file play ?? ???????? ???? ??? ???????? u????.
        // ???? ????? ??? ?????? ??????-> ??? ?????? playstatus (???? play time ????) ??
        // 1????? ??? ?÷???? ???? ?????? ??쿡?? playstatus ?? (start->impl->stop) ?????? ???????.
        // int ?? ?????? ??? ??????? 1byte ???????? ??? module ???? sync ?? ??´? ?????? ?????? ???????.
        int		eFilePlayType;

        // file adv play ?? IMP?? ???´??? ????´??? ???????.
        int		nSendFILEADVIMP; // 0 ????? : 1 ????

        // file time check count ?? ??????.
        int		nCheckFileTimeCount;

        // (2013.06.04)
        // file/RTSP play ?? ??? ?????? ???/URL .. ????? ts ?? ???? ???.(btv ???? ts ??? play ?o??????? ????????..)
        char szMediaInfo[256];

        // (2013.06.04)
        // file/RTSP ADV play ?? ??? ?????? ??? ????.. play status ??????? ???? ???????.
        char szMetaInfo[256];

#ifdef USE_RTSP
        //////////////////////////////////////////////////////////////////////////
        // RTSP ????

        // RTSP session ?? ??????? KEY ID
        int nRTSPSessionID;

        // rtsp play ?? ???????? ???? ??? ???????? u????.
        // ???? ????? ??? ?????? ??????-> ??? ?????? playstatus (???? play time ????) ??
        // 1????? ??? ?÷???? ???? ?????? ??쿡?? playstatus ?? (start->impl->stop) ?????? ???????.
        // int ?? ?????? ??? ??????? 1byte ???????? ??? module ???? sync ?? ??´? ?????? ?????? ???????.
        int		eRTSPPlayType;

        // rtsp adv play ?? IMP?? ???´??? ????´??? ???????.
        int		nSendRTSPADVIMP; // 0 ????? : 1 ????

        // rtsp/drm play ?? contentID ????
        char szContentID[256];

        // RTSP drm ???? ?????? OTP ID
        char szOTPID[256];

        // RTSP drm ???? ?????? OTP pwd
        char szOTPPASSWD[256];

        // RTSP Trick ?? ?????? base time
        int mRTSPBaseTime;

        // RTSP Trick ?? ?????? EOF time
        long long mRTSPEOFTime;

        // RTSP Play ?? ?????? start time (sec)
        int mRTSPStartTime;

        // RTSP Pause ?? ?????? pause time, trick -> play -> pause ???? ??????? ??? play ?? ???? ?ð??? ????? ????
        // ?????? ???????.
        long mRTSPPauseTime;

        // ????? ???? time stamp ??
        long long  mLastGetTimeStamp;

        // rtsp play status notify cancel flag
        int mJumpRtspStatusSend;

        // rtsp play ???? play time millisec
        // play/trick/pause ???? ???? ??? ???? ?????.
        long long mRTSPCurrentPlayTimePTS;

        long long mLastSendRTSPStatusPlayTimeMs; //jung2604 : 20190518 : UI에게 마지막으로 던져준 play time 값..

        // rtsp time check  ???? ???? 1: check 0: check done
        int mRTSPCalculateTimeCheck;

        // rtsp time check wait ????? ???? ???? 1: check 0: check done
        int mRTSPCalculateTimeCheckExceptWait;

        // rtsp time ??? ?ð?
        long long mRTSPWannaBeGonePTS;

        // rtsp time ??? u? flag
        int mRTSPWaitTimeChanged;
#endif
        void convertCodecInfo(dvb_locator_t &locator);

    public:
        static int videoDelayTimeMs;
        static int enableGlobalMute;
        static AVP_AudioOutputMode enableSettingsDolby;
        static AVP_AspectRatioMode settingsAspectRatioMode;

    public:
        int32_t enableCasInfo(int enable);

    private:
#ifdef USE_RTSP
        int checkRtspUncontrollableState(); // jung2604 : 20190520 : vod를 제어할수 없을때(EOF를 받았을때) 명령을 무시하도록 한다.
#endif
        int32_t pause_impl(); //jung2604 : 실제 pause동작을 별도로 구현, puase()는 클라이언트로 부터 받기용

#if 1//def FEATURE_SOC_AMLOGIC
	private:
		ANativeWindow *mAnativeWindow;
		//std::shared_ptr<ANativeWindow> mAnativeWindow;
#endif
    };




#endif // ___IPTV_SOC_PLAYER_H__ 
