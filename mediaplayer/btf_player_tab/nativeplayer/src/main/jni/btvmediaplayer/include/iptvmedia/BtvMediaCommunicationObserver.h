// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef ANDROID_BTVFAMILYOBSERVER_H
#define ANDROID_BTVFAMILYOBSERVER_H

#ifdef __cplusplus

#include <string>

#include <utils/Errors.h>
#include <utils/RefBase.h>
#include <utils/threads.h>
#include <utils/Vector.h>

namespace android
{

class BtvMediaCommunicationObserver: public RefBase
{
public:
	BtvMediaCommunicationObserver() {}
	virtual			~BtvMediaCommunicationObserver() {}
	virtual void	familyStateEvent(int32_t state) = 0;
	virtual void	onCasInfoEvent(int32_t type, std::string casInfo) = 0;

private:
	Mutex				mNotifyLock;
};

};	// namespace android

#endif // __cplusplus

#endif // ANDROID_BTVFAMILYOBSERVER_H

