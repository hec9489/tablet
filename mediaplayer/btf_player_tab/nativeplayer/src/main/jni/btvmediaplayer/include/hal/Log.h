// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef __ANDROID_LOG_H__
#define __ANDROID_LOG_H__

#ifdef FEATURE_USED_LOG
#include <android/log.h>
#define  LOG_TAG    "MiniPlayer"
#define  LOGUNK(...)  __android_log_print(ANDROID_LOG_UNKNOWN,LOG_TAG,__VA_ARGS__)
#define  LOGDEF(...)  __android_log_print(ANDROID_LOG_DEFAULT,LOG_TAG,__VA_ARGS__)
#define  LOGV(...)  __android_log_print(ANDROID_LOG_VERBOSE,LOG_TAG,__VA_ARGS__)
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define  LOGF(...)  __android_log_print(ANDROID_FATAL_ERROR,LOG_TAG,__VA_ARGS__)
#define  LOGS(...)  __android_log_print(ANDROID_SILENT_ERROR,LOG_TAG,__VA_ARGS__)
#define  LOG_FOOT	__android_log_print(ANDROID_LOG_ERROR, LOG_TAG, "[%s:%d:%s]", __FILE__, __LINE__, __FUNCTION__)
//#define  LOG_FUNC_START	__android_log_print(ANDROID_LOG_INFO, LOG_TAG, "[Function entry - %s:%d:%s]", __FILE__, __LINE__, __FUNCTION__)
//#define  LOG_FUNC_END	__android_log_print(ANDROID_LOG_INFO, LOG_TAG, "[Function out - %s:%d:%s]", __FILE__, __LINE__, __FUNCTION__)
#else
#define  LOGI(...)  printf(__VA_ARGS__)
#define  LOGE(...)  printf(__VA_ARGS__)
#endif

#endif /* __ANDROID_LOG_H__ */
