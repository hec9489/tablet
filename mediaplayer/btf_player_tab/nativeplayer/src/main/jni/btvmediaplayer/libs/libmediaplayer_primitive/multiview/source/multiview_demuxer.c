// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifdef __cplusplus
extern "C" {
#endif


#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <libavutil/timestamp.h>

#include "multiview_demuxer.h"
#include "multiview_error.h"
#include "multiview_util.h"
#include "multiview_muxer.h"

//static int STREAM_INDEX_ARRAY[MULTIVIEW_MUXER_CHANNEL] = {-1, };
//pthread_mutex_t  stream_demuxer_mutex = PTHREAD_MUTEX_INITIALIZER; // 쓰레드 초기화

static int64_t      g_pts_base;
static int64_t      g_dts_base;
static fd_set       g_fdChannelReady;           // demux 생성에 대한 체크
static fd_set       g_fdChannelSync;            // iframe sync 에 대한 체크
static fd_set       g_fdReCalcTimming;          // mod 값 계산 여부에 대한 체크

#define FD_CHANNEL_SYNC_CNT     3
#define DEMUX_BASE_CHANNEL_IDX  0           // sync 기준 채널
#define VIDEO_QUEUE_MAX_SIZE    128

pthread_mutex_t     g_demux_mutex = PTHREAD_MUTEX_INITIALIZER;


static int64_t *g_packetPts_demuxer;            // packet pts 계산을 위한 값 저장
static bool    *g_printPts_demuxer;             // packet pts 초기화 여부 저장
static int64_t *g_prev_pts;                     // pts 비교를 위한 저장
static int64_t *g_next_pts;                     // pts 비교를 위한 저장

/**
 * 내부 QUEUE 메모리 해제를 위한 Callback
 * @param msg
 */
void demuxerQueue_freeCB(void *msg)
{
	AVPacket* packet = (AVPacket*)msg;
	av_packet_unref(packet);
//	LOGI("message free...  [%s:%d]", __FUNCTION__, __LINE__);
}

/**
 * 내부 QUEUE 제거
 * @param ctx
 */
void demuxerQueue_final(ST_FFMPEG_DEMUX_CTX *ctx)
{
    LOGI("call %s:%d", __FUNCTION__, __LINE__);
	av_thread_message_flush(ctx->video_queue);
	av_thread_message_queue_free(&ctx->video_queue);
//	ctx->video_queue = NULL;

	return;
}

/**
 * 내부 QUEUE 초기화
 * @param ctx
 * @return
 */
int demuxerQueue_init(ST_FFMPEG_DEMUX_CTX *ctx)
{
	int result = 0;
	result = av_thread_message_queue_alloc(&ctx->video_queue,
	                                       VIDEO_QUEUE_MAX_SIZE,
	                                       sizeof(AVPacket));
	if(result < 0)
	{
		LOGE("av_thread_message_queue_alloc alloc failed.  error(%d)", result, av_err2str(result));
		return FFMPEG_ERR_NOT_ENOUGHT_MEMORY;
	}

	/**
	* Set the optional free message callback function which will be called if an
	* operation is removing messages from the queue.
	*/
	av_thread_message_queue_set_free_func(ctx->video_queue,
	                                      &demuxerQueue_freeCB);

	return result;
}


/**
 * 내부 QUEUE 에 Message 추가
 * @param ctx
 * @param packet
 * @return
 */
int demuxerQueue_push(ST_FFMPEG_DEMUX_CTX *ctx, AVPacket *packet)
{
	int result = av_thread_message_queue_send(ctx->video_queue, packet, AV_THREAD_MESSAGE_NONBLOCK);         // nonblocking 처리

	// retry 를 해야 하는 경우 전달 처리.
	if (result == AVERROR(EAGAIN)) {
		result = av_thread_message_queue_send(ctx->video_queue, packet, 0);        // blocking 처리
		LOGI("demuxerQueue_push blocking proc.  result(%d), msg(%s)",
		     result, av_err2str(result));
	} else if (result < 0){
		LOGE("demuxerQueue_push error(%d), message(%s)", result,
		     av_err2str(result));
		return FFMPEG_ERR_INTERNAL;
	}

	return result;
}

/**
 * 내부 QUEUE 에 Message Pop 요청
 * @param ctx
 * @param packet
 * @return
 */
int demuxerQueue_pop(ST_FFMPEG_DEMUX_CTX *ctx, AVPacket *packet)
{
	if(ctx == NULL || packet == NULL )
	{
		LOGE("demuxerQueue_pop invalid parameter.  ctx(0x%x)", (unsigned int)ctx);
		return FFMPEG_ERR_INVAIDDATA;
	}

	int result = av_thread_message_queue_recv(ctx->video_queue, packet, AV_THREAD_MESSAGE_NONBLOCK);        // nonblocking
	if(result < 0)
	{
		LOGE("demuxerQueue_pop failed. error(%d), message(%s)", result, av_err2str(result));
		return result;
	}

	return result;
}


/**
 * Demuxer 초기화
 * @return
 */
int multiview_demuxer_init()
{
	LOGI("multiview_demuxer_init start. (%s : %d)", __FUNCTION__, __LINE__);

	g_pts_base = 0;
	g_dts_base = 0;

	pthread_mutex_init(&g_demux_mutex, NULL);
//	g_demux_mutex = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;

	FD_ZERO(&g_fdChannelReady);
	FD_ZERO(&g_fdChannelSync);
	FD_ZERO(&g_fdReCalcTimming);

	int size = FD_CHANNEL_SYNC_CNT * 2;    // audio, video
	g_packetPts_demuxer = av_malloc(sizeof(int64_t) * size);
	g_printPts_demuxer = av_malloc(sizeof(bool) * size);

	g_prev_pts = av_malloc(sizeof(int64_t) * size);
	g_next_pts = av_malloc(sizeof(int64_t) * size);

	for(int i = 0; i < size ; i++) {
		g_packetPts_demuxer[i] = -1;
		g_printPts_demuxer[i] = false;
		g_prev_pts[i] = 0;
		g_next_pts[i] = 0;
	}

    return 0;
}

/**
 * Demuxer global variable 삭제
 * @return
 */
int multiview_demuxer_final()
{
	if(g_packetPts_demuxer != NULL) {
        av_free(g_packetPts_demuxer);
        g_packetPts_demuxer = NULL;
    }
    if(g_printPts_demuxer != NULL) {
        av_free(g_printPts_demuxer);
        g_printPts_demuxer = NULL;
    }


    if(g_prev_pts != NULL) {
        av_free(g_prev_pts);
        g_prev_pts = NULL;
    }
    if(g_next_pts != NULL){
        av_free(g_next_pts);
        g_next_pts = NULL;
    }

	return 0;
}

/**
 * 처음 생성한 packet에 대한 packet interval 확인.-- test용 코드
 * @param packet
 * @param channelIdx  0 : video, 1, 2, 3: audio
 */
void checkDemuxPacketTime(AVPacket *packet, int channelIdx)
{
	AVRational avRational;
	avRational.den = 90000;
	avRational.num = 1;

	int idx = 0;        // global variable index;
	int64_t interval = 0;

	// global vairable 의 index 계산
	idx = (channelIdx * 2) + packet->stream_index;

	if(g_packetPts_demuxer[idx] == -1)
	{
		g_packetPts_demuxer[idx] = packet->pts;
	}

	if( packet->stream_index == 0 )  // video 일 경우.
	{
		LOGI("DEMUXER -- channelIdx(%d) video pts (%lld) ", channelIdx, packet->pts);
		return;
	}

	// currentPacket - base video channel packet
	interval = g_packetPts_demuxer[idx] - g_packetPts_demuxer[0];

	if(g_packetPts_demuxer[0] != -1 && g_packetPts_demuxer[idx] != -1 && g_printPts_demuxer[idx] == false)
	{
		LOGI("DEMUXER -- channelIdx(%d) first video pts (%lld) first audio pts(%lld), intervalPts(%lld), intervalTime(%s)",
		     channelIdx, g_packetPts_demuxer[0], g_packetPts_demuxer[idx], interval, av_ts2timestr(interval, &avRational));
		g_printPts_demuxer[idx] = true;
	}
}



// 현재 채널에 IFrame이 입력되었음을 설정한다.
bool setFdForSync(int channelIdx)
{

	if(channelIdx < 0 || channelIdx > FD_CHANNEL_SYNC_CNT)
		return false;

	pthread_mutex_lock(&g_demux_mutex);
	FD_SET(channelIdx, &g_fdChannelSync);
	LOGI("setFdForSync fd channel(%d)", channelIdx);
	pthread_mutex_unlock(&g_demux_mutex);

	return true;
}

/**
 * 현재 채널의 Iframe을 찾아 sync 준비가 되었는지 확인
 * @param channelIdx : channel index
 * @return
 */
bool isSetFdForSync(int channelIdx)
{
	int state;
	bool isSet = false;
	struct timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = 100;

	pthread_mutex_lock(&g_demux_mutex);
#if 0 //select를 사용하면 g_fdChannelSync가 초기화 된다
	state = select(FD_CHANNEL_SYNC_CNT, &g_fdChannelSync, NULL, NULL, &timeout);
#endif
	isSet = FD_ISSET(channelIdx, &g_fdChannelSync);
	if (state < 0 || !isSet) {
		isSet = false;
	}
	pthread_mutex_unlock(&g_demux_mutex);

	return isSet;
}

/**
 * 모든 채널에 대해 Iframe 이 입력되었는지 확인한다.
 * @param
 * @return sync 여부
 */
bool checkAllFdForSync()
{
	int state;
	bool isReady = false;
	struct timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = 100;

	pthread_mutex_lock(&g_demux_mutex);
#if 0
	state = select(FD_CHANNEL_SYNC_CNT, &g_fdChannelSync, NULL, NULL, &timeout);
	if (state == FD_CHANNEL_SYNC_CNT) {
#else
	if(FD_ISSET(0, &g_fdChannelSync) && FD_ISSET(1, &g_fdChannelSync) && FD_ISSET(2, &g_fdChannelSync) ) {
#endif
		isReady = true;
	}
	pthread_mutex_unlock(&g_demux_mutex);

	return isReady;
}

// Sync를 다시 실행하기 위하여 fd를 Clear 처리함.
bool clearAllFdForSync()
{
	pthread_mutex_lock(&g_demux_mutex);
	for(int i = 0 ; i < MULTIVIEW_MUXER_CHANNEL ; i++)
	{
		// fd clear
		FD_CLR(0, &g_fdChannelSync);
	}
	pthread_mutex_unlock(&g_demux_mutex);

	return true;
}

void setTimeForFrame(ST_FFMPEG_DEMUX_CTX* ctx, AVPacket* packet)
{
	if(ctx == NULL || packet == NULL)
		return;

	// pts_iframe 이 0 인지 확인하는 것은 값을 설정한 적이 없는지 확인하는 것임.
	if(packet->flags == AV_PKT_FLAG_KEY) {
		ctx->pts_iframe = packet->pts;
		ctx->dts_iframe = packet->dts;
	}
}

// 현재 채널에 대한 Demuxer 상태를 설정하고, 모든 채널에 대해 Demuxer 가 생성되었는지 체크한다.
bool setReadyStatusDemuxer(int channelIdx, int* terminate)
{
	int state = 0;
	int retryCnt = 0;
	int RETRY_MAX_CNT = 10000;
	bool    isReady = false;
	struct timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = 1000;

	pthread_mutex_lock(&g_demux_mutex);
	FD_SET(channelIdx, &g_fdChannelReady);
	pthread_mutex_unlock(&g_demux_mutex);

	for(;;) {
		pthread_mutex_lock(&g_demux_mutex);
#if 0
		state = select(FD_CHANNEL_SYNC_CNT, &g_fdChannelReady, NULL, NULL, &timeout);
#else
		if(FD_ISSET(0, &g_fdChannelReady) && FD_ISSET(1, &g_fdChannelReady) && FD_ISSET(2, &g_fdChannelReady)) {
			state = FD_CHANNEL_SYNC_CNT;
		} else {
			state = 0;
			usleep(1000);
		}
#endif
		// 모든 채널의 Demux 가 Ready 상태 인지 체크
		if (state == FD_CHANNEL_SYNC_CNT) {
			isReady = true;
		}

		pthread_mutex_unlock(&g_demux_mutex);

		if(isReady || *terminate == 1)
			break;

		++retryCnt;
		if(retryCnt > RETRY_MAX_CNT)
		{
			LOGE("ready to all demux init failed.");
			return false;
		}

		usleep(MULTIVIEW_USLEEP_TIME);
	}

	return isReady;
}

// packet 에 대한 pts 정보 보정.
int modify_stream_time(ST_FFMPEG_DEMUX_CTX* ctx, AVPacket* packet)
{
    if(ctx == NULL || packet == NULL)
    {
        LOGE("parameter invalid.   ctx(0x%x), packet(0x%x)", (unsigned int)ctx, (unsigned int)packet);
        return FFMPEG_ERR_INTERNAL;
    }

    packet->pts = packet->pts + ctx->pts_mod;
    packet->dts = packet->dts + ctx->dts_mod;

//	LOGI("modify timestamp..   channel(%d), stream(%d), pts(%lld), dts(%lld)",
//	     ctx->channelIdx, packet->stream_index, packet->pts, packet->dts);

    return 0;
}

void setBaseStreamRescaleTime(ST_FFMPEG_DEMUX_CTX* ctx, AVPacket* packet)
{
    pthread_mutex_lock(&g_demux_mutex);

	if(ctx->channelIdx == DEMUX_BASE_CHANNEL_IDX) {

		g_pts_base = packet->pts;
		g_dts_base = packet->dts;

		LOGI("base g_video_pts(%lld), _video_dts(%lld)",
		     g_pts_base, g_dts_base);
	}

    pthread_mutex_unlock(&g_demux_mutex);

}

// 현재 채널에 대한 pts 보정값 계산
void rescaleChannelTime(ST_FFMPEG_DEMUX_CTX* ctx)
{
	ctx->pts_mod = g_pts_base - ctx->pts_iframe;
	ctx->dts_mod = g_dts_base - ctx->dts_iframe;


	LOGI("[%s:%d] channel(%d), pts_mod(%lld), dts_mod(%lld), g_pts_base(%lld)", __FUNCTION__, __LINE__, ctx->channelIdx,
	     ctx->pts_mod, ctx->dts_mod, g_pts_base);
	LOGI("[%s:%d] base channel(%d) ctx pts_iframe(%lld), ctx->dts_iframe(%lld)", __FUNCTION__, __LINE__,
	     ctx->channelIdx,
	     ctx->pts_iframe, ctx->dts_iframe);

}

void recalcTimming(ST_FFMPEG_DEMUX_CTX* ctx, AVPacket* packet)
{
	if(ctx == NULL || packet == NULL)
	{
		LOGE("parameter invalid. (%s : %d) ctx(0x%x), packet(0x%x)", __FUNCTION__, __LINE__, ctx, packet);
		return;
	}

	int state;
	struct timeval timeout;
	bool isSet;
	timeout.tv_sec = 0;
	timeout.tv_usec = 100;

	pthread_mutex_lock(&g_demux_mutex);
	state = select(FD_CHANNEL_SYNC_CNT, &g_fdReCalcTimming, NULL, NULL, &timeout);
	isSet = FD_ISSET(ctx->channelIdx, &g_fdReCalcTimming);
	if (state >= 0 && isSet) {
		// 해당 채널의 base 채널과 현재 채널간의 Gap timming 계산처리
		rescaleChannelTime(ctx);

		// fd clear
		FD_CLR(ctx->channelIdx, &g_fdReCalcTimming);

		LOGI("clear fd set. channelIdx(%d)", ctx->channelIdx);
	}
	pthread_mutex_unlock(&g_demux_mutex);

	// 여기서 packet timming 재계산.
	modify_stream_time(ctx, packet);

}

void setAllTimmingFd()
{
	int i;
	pthread_mutex_lock(&g_demux_mutex);
	for(i = 0; i < FD_CHANNEL_SYNC_CNT; i++)
	{
		FD_SET(i, &g_fdReCalcTimming);
	}
	pthread_mutex_unlock(&g_demux_mutex);
}

static int open_codec_context(int *stream_idx,
                              AVCodecContext **dec_ctx, AVFormatContext *fmt_ctx,
							  enum AVMediaType type, int* terminate) {
	int ret = -1;
	int stream_index = 0;
	AVStream *st;
	AVCodec *dec = NULL;
	AVDictionary *opts = NULL;

	while (*terminate == 0 && ret < 0) {
		ret = av_find_best_stream(fmt_ctx, type, -1, -1, NULL, 0);
		if (ret >= 0)
			break;

		LOGE("Could not find %s stream in input stream, streamIdx(%d)",
			 av_get_media_type_string(type), *stream_idx);
		usleep(MULTIVIEW_USLEEP_TIME);
	}

	if (ret < 0) {
		LOGE("Could not find %s stream in input stream, streamIdx(%d) return fail.",
			 av_get_media_type_string(type), *stream_idx);
		return ret;
	}

	stream_index = ret;
	st = fmt_ctx->streams[stream_index];

	/* find decoder for the stream */
	dec = avcodec_find_decoder(st->codecpar->codec_id);
	if (!dec) {
		LOGE("Failed to find %s codec", av_get_media_type_string(type));
		return FFMPEG_ERR_INVAIDDATA;
	}

	/* Allocate a codec context for the decoder */
	*dec_ctx = avcodec_alloc_context3(dec);
	if (!*dec_ctx) {
		LOGE("Failed to allocate the %s codec context", av_get_media_type_string(type));
		return FFMPEG_ERR_NOT_ENOUGHT_MEMORY;
	}

	/* Copy codec parameters from input stream to output codec context */
	if ((ret = avcodec_parameters_to_context(*dec_ctx, st->codecpar)) < 0) {
		LOGE("Failed to copy %s codec parameters to decoder context",
			 av_get_media_type_string(type));
		return ret;
	}

	/* Init the decoders, with or without reference counting */
	if ((ret = avcodec_open2(*dec_ctx, dec, &opts)) < 0) {
		LOGE("Failed to open %s codec",
			 av_get_media_type_string(type));
		return ret;
	}
	*stream_idx = stream_index;

	return 0;
}

int multiview_create_demuxer_stream(ST_FFMPEG_DEMUX_CTX** ctx, int channelIdx,
                                    int pid, int vid, int aid, void* opaque,
									READ_STREAM_CB cb, int* terminate)
{
    int result = 0;
    uint8_t *buffer = NULL;
    AVFormatContext *fmt_ctx = NULL;
    AVCodecContext *video_dec_ctx = NULL;
    AVCodecContext *audio_dec_ctx = NULL;

    int audio_stream_idx = -1;
    int video_stream_idx = -1;

	LOGI("multiview_create_demuxer_stream start. channel(%d), (%s : %d)", channelIdx, __FUNCTION__, __LINE__);

    if(ctx == NULL)
    {
	    LOGE("(%s : %d) ctx is null", __FUNCTION__, __LINE__);
	    return FFMPEG_ERR_INVAIDDATA;
    }

//    av_register_all();

    *ctx = (ST_FFMPEG_DEMUX_CTX*)av_malloc(sizeof(ST_FFMPEG_DEMUX_CTX));
    if(*ctx == NULL) {
	    LOGE("(%s : %d) av_malloc failed.", __FUNCTION__, __LINE__);
	    return FFMPEG_ERR_NOT_ENOUGHT_MEMORY;
    }

    fmt_ctx = avformat_alloc_context();
    if(fmt_ctx == NULL)
    {
        LOGE("fail to alloc avformat context");
		av_free(*ctx);
		*ctx = NULL;
		return FFMPEG_ERR_INTERNAL;
    }

    buffer = av_malloc(AVIO_BUFFER_SIZE);
    if(!buffer)
    {
        LOGE("fail to alloc av_buffer.");
		avformat_free_context(fmt_ctx);
		av_free(*ctx);
		*ctx = NULL;
        return FFMPEG_ERR_NOT_ENOUGHT_MEMORY;
    }

    AVIOContext* avioContext = avio_alloc_context(buffer, AVIO_BUFFER_SIZE, 0,
                                                  opaque, cb, NULL, NULL);
    if(avioContext == NULL)
    {
        LOGE("avio_alloc_context fail");
		av_free(buffer);
		avformat_close_input(fmt_ctx);
		av_free(*ctx);
		*ctx = NULL;
        return FFMPEG_ERR_INTERNAL;
    }

    fmt_ctx->pb = avioContext;
    fmt_ctx->flags = AVFMT_FLAG_CUSTOM_IO;
    fmt_ctx->iformat = av_find_input_format("mpegts");

    result = avformat_open_input(&fmt_ctx, NULL, NULL, NULL);
    if (result < 0) {
        LOGE("Could not open input");
		av_free(avioContext->buffer);
		av_free(avioContext);
		avformat_free_context(fmt_ctx);
		av_free(*ctx);
		*ctx = NULL;
        return FFMPEG_ERR_INTERNAL;
    }


    /* retrieve stream information */
    if (avformat_find_stream_info(fmt_ctx, NULL) < 0) {
        LOGE("Could not find stream information");
		av_free(avioContext->buffer);
		av_free(avioContext);
		avformat_free_context(fmt_ctx);
		av_free(*ctx);
		*ctx = NULL;
        return FFMPEG_ERR_INTERNAL;
    }

    // find audio / video stream index
    // codec 찾기
    if (open_codec_context(&video_stream_idx, &video_dec_ctx, fmt_ctx, AVMEDIA_TYPE_VIDEO, terminate) < 0) {
        LOGE("Could not find video stream information");
		av_free(avioContext->buffer);
		av_free(avioContext);
		avformat_free_context(fmt_ctx);
		av_free(*ctx);
		*ctx = NULL;
        return FFMPEG_ERR_INTERNAL;
    }

    if (open_codec_context(&audio_stream_idx, &audio_dec_ctx, fmt_ctx, AVMEDIA_TYPE_AUDIO, terminate) < 0) {
        LOGE("Could not find audio stream information");
		avcodec_close(video_dec_ctx);
		avcodec_free_context(&video_dec_ctx);
		av_free(avioContext->buffer);
		av_free(avioContext);
		avformat_free_context(fmt_ctx);
		av_free(*ctx);
		*ctx = NULL;
		return FFMPEG_ERR_INTERNAL;
    }

	result = mvQueue_getInstance(&(*ctx)->message_queue);
	if(result < 0)
	{
		LOGE("get queue instance failed.");
		avcodec_close(video_dec_ctx);
		avcodec_close(audio_dec_ctx);

		avcodec_free_context(&video_dec_ctx);
		avcodec_free_context(&audio_dec_ctx);
        av_free(avioContext->buffer);
		av_free(avioContext);
		avformat_free_context(fmt_ctx);
		av_free(*ctx);
		*ctx = NULL;
		return result;
	}

	// BASE Channel 이 아닌 모든 Channel 의 Demux가 Ready 상태인지 체크하는 것으로 변경
	if(!setReadyStatusDemuxer(channelIdx, terminate))
	{
		LOGE("setReadyStatusDemuxer failed.");
		avcodec_close(video_dec_ctx);
		avcodec_close(audio_dec_ctx);

		avcodec_free_context(&video_dec_ctx);
		avcodec_free_context(&audio_dec_ctx);
        av_free(avioContext->buffer);
		av_free(avioContext);
		avformat_free_context(fmt_ctx);
		mvQueue_unRefInstance(&(*ctx)->message_queue);
		av_free(*ctx);
		*ctx = NULL;
		return result;
	}

	demuxerQueue_init(*ctx);

    (*ctx)->fmt_ctx = fmt_ctx;
    (*ctx)->video_dec_ctx = video_dec_ctx;
    (*ctx)->audio_dec_ctx = audio_dec_ctx;
    (*ctx)->video_stream_idx = video_stream_idx;
    (*ctx)->audio_stream_idx = audio_stream_idx;
	(*ctx)->width = video_dec_ctx->width;
	(*ctx)->height = video_dec_ctx->height;
	(*ctx)->pix_fmt = video_dec_ctx->pix_fmt;
	(*ctx)->avio_ctx = avioContext;
	(*ctx)->channelIdx = channelIdx;
	(*ctx)->pid = pid;
    (*ctx)->vid = vid;
    (*ctx)->aid = aid;
//	(*ctx)->isFindIFrame = false;
	(*ctx)->pts_iframe = 0;
	(*ctx)->dts_iframe = 0;
	(*ctx)->pts_mod = 0;
	(*ctx)->dts_mod = 0;

	LOGI("multiview_create_demuxer_stream end. channel(%d), (%s : %d)", channelIdx, __FUNCTION__, __LINE__);

    return result;
}


void saveFileRawData(uint8_t* buf, size_t size){
    static FILE* fp = NULL;
    static int MAX_COUNT = 50000;
    static int currentIdx = 0;
    static bool isFinish = false;
    int result = 0;

    if(isFinish)
        return;

    if(!fp) {
        fp = fopen("/mnt/sdcard/h265/video.es", "wb");
    }

    if(currentIdx < MAX_COUNT){
        result = fwrite(buf, 1, size, fp);
        if(result < size)
        {
            LOGE("request buf(%d), write buf(%d)", size, result);
        }
        currentIdx++;
    }

    if(currentIdx >= MAX_COUNT)
    {
        fclose(fp);
        isFinish = true;
    }
}

// pts 정보를 재계산해야 하는 경우를 확인.
bool checkForModifyBasePts(ST_FFMPEG_DEMUX_CTX *ctx, AVPacket *pkt){
	int channelIdx = ctx->channelIdx;
    int idx = (channelIdx * 2) + pkt->stream_index;        // global variable index;
	int64_t next = pkt->pts + pkt->duration;
	int64_t pts_gap = pkt->pts - g_prev_pts[idx];
	AVRational* avrational = &ctx->fmt_ctx->streams[pkt->stream_index]->time_base;
	bool    result = false;

	// video의 iframe 에 대해서 값의 차이를 확인해 보자
	if(!pkt->flags || pkt->stream_index == 1 )   // 일단 VideoStream 만 확인.
		return result;

	// iframe 간의 간격이 1.2초가 넘어가는 경우에 대해 고려
	if(pts_gap > ( avrational->den * 1.2 ) ) {
		LOGE("[channelIdx : %d, streamIdx : %d] curPtsTime(%s), curPtsDuration(%lld), ptsgapTime(%s), gappts(%lld)",
		     channelIdx, pkt->stream_index,
		     av_ts2timestr(pkt->pts, avrational),
		     pkt->duration,
		     av_ts2timestr(pts_gap, avrational),
		     pts_gap);
		result = true;
	}

	// 마지막 iframe pts 정보를 재등록.  오차가 심하면 재계산하는 부분을 반영하도록 하자.
//	setTimeForFrame(ctx, pkt);

	g_prev_pts[idx] = pkt->pts;
	g_next_pts[idx] = next;
	return result;
}

void putMultiViewRecvData(h_pt_merge_hevc merge_hevc, p_pt_hevc_au_t ptHevc, int nDevice, AVPacket *pPacket) {

    if(merge_hevc == NULL || ptHevc == NULL || nDevice < 0 || pPacket == NULL)
    {
        LOGE("Input parameter invalid.");
        return;
    }

    int ret = 0;

	// 파일저장 부분 제거
//    saveFileRawData(pPacket->buf->data, (size_t)pPacket->buf->size);

    memset(ptHevc, 0, sizeof(pt_hevc_au_t));
    ptHevc->iStreamLength = pPacket->buf->size;
    ptHevc->ulDts = pPacket->dts;
    ptHevc->ulPts = pPacket->pts;

    memcpy((void *)ptHevc->pStream, (void *)pPacket->buf->data, pPacket->buf->size);

    ret = PIX_PutMergeHevcEs(merge_hevc, nDevice, ptHevc);
    if(ret < 0)
        LOGE("PIX_PutMergeHevcEs ++Device[%d] error : %d" , nDevice, ret);
//    else if(pPacket->flags)
//	    LOGW("Pix_putMergeHevcEs,  device(%d) pts(%lld), dts(%lld)", nDevice, ptHevc->ulPts, ptHevc->ulDts);
//    else
//	    LOGW("Pix_putMergeHevcex, +++ device(%d) pts(%lld), dts(%lld)", nDevice,
//	         ptHevc->ulPts, ptHevc->ulDts);

}


int multiview_demux_stream(ST_FFMPEG_DEMUX_CTX* ctx, h_pt_merge_hevc pt_ctx, int* terminate)
{
    AVPacket    packet;
    int         result = 0;
    pt_hevc_au_t *ptHevc = NULL;
	ST_MESSAGE  msg;
	bool        isReadySync = false;
	static int64_t  oldPos = 0;
	static int      packetSize = 0;
	static int64_t  oldPts = 0;
	static int64_t  oldDts = 0;

    if(ctx == NULL || pt_ctx == NULL || terminate == NULL)
    {
        LOGE("paramter is invalid.   ctx(0x%x), pt_ctx(0x%x), terminate(0x%x)",
             (unsigned int)ctx, (unsigned int)pt_ctx, (unsigned int)terminate);
        return FFMPEG_ERR_INVAIDDATA;
    }


    ptHevc = (pt_hevc_au_t*)av_malloc(sizeof(pt_hevc_au_t));

    while (*terminate == 0) {

	    av_init_packet(&packet);

        // read packet
        result = av_read_frame(ctx->fmt_ctx, &packet);
        if(result >= 0) {
	        // 모든 채널이 Sync가 완료되었는지 확인함.
	        isReadySync = checkAllFdForSync();

	        /*
	         * Video Stream 이고 iFrame 일 경우
	         * 모든 채널에 대해 IFrame을 찾었는지 확인한다.
	         */
	        if(!isReadySync) {
		        if (ctx->video_stream_idx == packet.stream_index &&
		            packet.flags == AV_PKT_FLAG_KEY) {
			        // base 설정값에 대한 보정이 필요하다.
			        setBaseStreamRescaleTime(ctx, &packet);

			        setTimeForFrame(ctx, &packet);

			        setFdForSync(ctx->channelIdx);
			        isReadySync = checkAllFdForSync();
			        if (isReadySync) {
				        // 각 demuxer는 pts_mod를 계산 하도록 설정
				        LOGI("call requesetRecalcTimming");
				        setAllTimmingFd();
			        }
//			        ctx->isFindIFrame = true;
		        }

		        if (!isReadySync) {
					// iframe 을 찾은 상태이면
					if(isSetFdForSync(ctx->channelIdx)) {
//						LOGI("packet push to queue... channel : %d", ctx->channelIdx);
						// 모든 채널의 정보가 Sync 되지 않으면 Queue 에 보관한다.
						if (demuxerQueue_push(ctx, &packet) < 0) {
							LOGE("packet push failed...");
							av_packet_unref(&packet);
						}
					}
                    else
                        av_packet_unref(&packet);

                    usleep(MULTIVIEW_USLEEP_TIME);
			        continue;
		        } else{
			        // 전체가 Sync 되어 마지막 frame 을 queue 에 넣는다.
//			        LOGI("packet push to queue... channel : %d", ctx->channelIdx);
			        // 모든 채널의 정보가 Sync 되지 않으면 Queue 에 보관한다.
			        if (demuxerQueue_push(ctx, &packet) < 0) {
				        LOGE("packet push failed...");
				        av_packet_unref(&packet);
			        }
		        }
	        } else {
//		        LOGI("packet push to queue... after sync.. channel : %d", ctx->channelIdx);
		        // 읽어온 packet 을 Queue 에 Push 한다.
		        if(demuxerQueue_push(ctx, &packet) < 0 )
		        {
			        LOGE("########## ERROR.......    packet push failed...");
			        av_packet_unref(&packet);
		        }
	        }

	        av_init_packet(&packet);
	        if(demuxerQueue_pop(ctx, &packet) < 0)
	        {
		        LOGE("####### ERROR.....   packet pop failed... channel(%d)", ctx->channelIdx);
		        usleep(MULTIVIEW_USLEEP_TIME);
		        continue;
	        }

	        // TODO pts 값을 재계산 해야 하는지 확인.  -- 테스트용 코드.
//	        checkForModifyBasePts(ctx, &packet);

			// PTS 보정 처리
	        recalcTimming(ctx, &packet);
//	        log_packet(ctx->fmt_ctx, &packet, ctx->channelIdx);

            if (ctx->video_stream_idx == packet.stream_index /*&& ctx->isFindIFrame*/)
	        {
//              LOGI("VIDEO stream index[%d]", packet.stream_index);
		        putMultiViewRecvData(pt_ctx, ptHevc, ctx->channelIdx, &packet);

	            // TODO for test..   시간 체크용...  배포시 삭제 필요
//		        checkDemuxPacketTime(&packet, ctx->channelIdx);

	            av_packet_unref(&packet);
            }
	        else if(ctx->audio_stream_idx == packet.stream_index &&
			        isReadySync)
		    {
			    // TODO for test..   시간 체크용...  배포시 삭제 필요
//			    checkDemuxPacketTime(&packet, ctx->channelIdx);

	            msg.streamId = ctx->channelIdx;
	            memcpy(&msg.time_base, &ctx->fmt_ctx->streams[ctx->audio_stream_idx]->time_base, sizeof(AVRational));
	            memcpy(&msg.packet, &packet, sizeof(AVPacket));
	            // push packet to queue
	            result = mvQueue_pushMessage(ctx->message_queue, &msg);
	            if(result < 0)
		            av_packet_unref(&packet);

//	            LOGI("demuxer audio device(%d), pts(%lld), dts(%lld)", msg.streamId, packet.pts, packet.dts);

            } else{
//	            LOGE("unknown stream index.  channel(%d), videoIdx(%d), audioIdx(%d), findKeyFrame(%d)",
//	                 ctx->channelIdx, ctx->video_stream_idx, ctx->audio_stream_idx, ctx->isFindIFrame);
	            av_packet_unref(&packet);
            }
        } else{
	        LOGE("av_read_frame fail. channel(%d) result(%d : %s)", ctx->channelIdx, result, av_err2str(result));
        }

	    usleep(MULTIVIEW_USLEEP_TIME);
    } //while (!*Terminate )

	LOGI("terminated demuxer thread. channel(%d)", ctx->channelIdx);

    av_free(ptHevc);

    return 0;
}

int multiview_destroy_demuxer(ST_FFMPEG_DEMUX_CTX** ctx)
{
    int result = 0;
	int idx = 0;

	LOGI("multiview_destroy_demuxer start. (%s : %d)", __FUNCTION__, __LINE__);

    if(ctx == NULL || *ctx == NULL){
        LOGE("invalid ctx.  ctx is invalid");
        return FFMPEG_ERR_INVAIDDATA;
    }

	idx = (*ctx)->channelIdx;

	avcodec_close((*ctx)->video_dec_ctx);
	avcodec_close((*ctx)->audio_dec_ctx);

    avcodec_free_context(&(*ctx)->video_dec_ctx);
    avcodec_free_context(&(*ctx)->audio_dec_ctx);

	av_free((*ctx)->avio_ctx->buffer);
	av_free((*ctx)->avio_ctx);
    avformat_free_context((*ctx)->fmt_ctx);
//    avformat_close_input(&(*ctx)->fmt_ctx);

	result = mvQueue_unRefInstance(&(*ctx)->message_queue);
//	LOGI("demuxer mvQueueRefCount(%d), (%s:%d)", result, __FUNCTION__, __LINE__);

	demuxerQueue_final(*ctx);


	av_free(*ctx);
    *ctx = NULL;

	LOGI("DemuxerContext destroy. streamIdx(%d) Queue RefCount(%d)", idx, result);

    return result;
}

#ifdef __cplusplus
} //extern "C"
#endif
