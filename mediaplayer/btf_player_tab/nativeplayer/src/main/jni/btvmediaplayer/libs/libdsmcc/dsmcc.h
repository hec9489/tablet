// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef __dsmcc_h__
#define __dsmcc_h__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <signal.h>
#include <fcntl.h>
#include <stdarg.h>
#include <errno.h>
#include <pthread.h>
#include <assert.h>
#define __STDC_FORMAT_MACROS
#include <unistd.h>
#define __STDC_LIMIT_MACROS
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "dsmcc-cache.h"

#define DSMCC_DEBUG_LOG_ENABLE 
//#define DSMCC_PRINT_DEBUG 
//#define DSMCC_MALLOC_PRINT_DEBUG 

#define DSMCC_DATA_BUFFER_CAPACITY	128
#include "Cust_Feature.h"
#define DSMCC_ROOTPATH "/btf/btvmedia/tmp/dsmcc"

#define DSMCC_MUSIC_APP_TYPE 0x5789

// event types
enum DSMCC_EVENTS
{
    DSMCC_EVENT_SECTION_FILTER_DATA = 0x8001,
    DSMCC_EVENT_APPLICATION_DATA	
};

enum DsmccEvent
{
    OC_DOWNLOAD_COMPLETE = 0,
    DC_DOWNLOAD_COMPLETE = 1,
    OC_OBJECT_UPDATED = 2,
    OME_CHANGE = 3,    
    DOWNLOAD_FAIL = 4,
    INFO_MUSIC_DOWNLOAD = 0x90,
    INFO_MUSIC_CACHE = 0x91	
};

enum DsmccSignalEvent
{
    APP_INFO_SIGNAL = 0,
    APP_INFO_CHANGED = 1
};

enum DsmccUpdate
{
    APP_NEW_DOWN = 0,
    APP_UPDATE_DOWN = 1
};

struct _dsmcc_event_list;
struct _dsmcc_event_head;

typedef struct _dsmcc_event_list 
{
    int event;
    int param;
    void *data;
    struct _dsmcc_event_list *next;
} dsmcc_event_list_t;

typedef struct _dsmcc_event_head 
{
    struct _dsmcc_event_list *volatile head;
    struct _dsmcc_event_list *last;
    pthread_cond_t cond;
    pthread_mutex_t lock;
} dsmcc_event_head_t;

typedef void (*dsmcc_callback)(void *handle, int event, char* path, void *data);
typedef void (*dsmcc_section_done_callback)(int done_section, void *section_user_param);

typedef struct _dsmcc_section_data
{
    void *section_user_param;			//	user section callback parameter
    void *section_data;					//	section data pointer. which have received section.
    unsigned int section_data_size;		//	section data size.
    dsmcc_section_done_callback callback;
} dsmcc_section_data;

typedef struct _dsmcc_application{
    int32_t appType;
    int32_t organizationId;
    int32_t applicationId;
    int32_t dsmccPid;	 
    int8_t  isServiceBound;
    char    name[32];
    int32_t controlCode;
    int32_t priority;
    int32_t channelNum;	 
    char    initialPath[32];
} dsmcc_application;

typedef struct _dsmcc_sigevent{
    int32_t           eventType;
    int32_t           serviceId;
    int32_t           demux;
    char              uri[128];
    dsmcc_application application;
} dsmcc_sigevent;

struct obj_carousel {
    dsmcc_application application;
    struct cache *filecache;
    struct cache_module_data *cache; 
    struct dsmcc_dsi *gate;
    unsigned long id;
    unsigned long flag;	
    struct obj_carousel *next;
};

typedef struct dsmcc_ctx
{
    struct obj_carousel *carousels;
    struct obj_carousel *active_carousel;

    char buffer[DSMCC_DATA_BUFFER_CAPACITY];
		
    // event
    dsmcc_event_head_t dsmcc_event;

    // thread stop request flag
    int stop_requested;
    pthread_t dsmcc_thread;

    dsmcc_callback callback;

    void* user_param;
    dsmcc_section_done_callback done_callback;
	
    void* handle;
} dsmcc_ctx;

void dsmcc_log_dump(const char* data, int lengths);
void dsmcc_log(const char *format, ...);

void dsmcc_process_send_event_toapp(dsmcc_ctx *ctx, int eventType, int cached);

void dsmcc_post_application_data(dsmcc_ctx *ctx, dsmcc_application *data);
void dsmcc_post_section_filter_data(dsmcc_ctx *ctx, void *data, int size, void *user_param, dsmcc_section_done_callback callback);

dsmcc_ctx *dsmcc_init(void *handle, dsmcc_callback callback);
void dsmcc_destroy(dsmcc_ctx *ctx);
void dsmcc_finish(dsmcc_ctx *ctx);


#ifdef __cplusplus
}
#endif

#endif //__dsmcc_h__
