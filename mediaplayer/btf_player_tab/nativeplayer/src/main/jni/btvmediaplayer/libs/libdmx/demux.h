// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef DEMUXER_H
#define DEMUXER_H

#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h> 
#include <sys/fcntl.h>
#include <assert.h>

#define ANDROID_LOG

#ifdef ANDROID_LOG
#include <android/log.h>
#endif

/******************************************************************************
 * Defines
 *****************************************************************************/
#define SYNC_BYTE                                      0x47
#define ONE_TS_MAX_SIZE                         208
#define TS_DEFAULT_PACKET_SIZE            188
#define BUFF_SIZE_FOR_CHECK_TS_SIZE  TS_DEFAULT_PACKET_SIZE*2

#define MAX_SECTIONS              10
#define MAX_SECTION_FILTERS 30
#define MAX_PROGRAM_COUNT 40
#define MAX_ES_COUNT              15
#define MAX_PMT_DESC_SIZE    1024

#define SECTION_LEN_MASK    0x0FFF

/* Filter types */
#define FILTER_NONE         0xff
#define SECTION_FILTER    0x0
#define VIDEO_FILTER        0x1
#define FILTER_TYPE_MAX   2

#define PES_START_HDR_LEN 6
#define PES_OPT_HDR_LEN     9
#define PES_HDR_BUF_LEN     (PES_OPT_HDR_LEN + 255)

#define STRM_TYPE_VIDEO 0


/* PID/TID ***************************/
#define NULL_PID    0x1fff
#define PAT_PID     0x00

#define PAT_TID     0x00
#define PMT_TID     0x02
#define AIT_TID      0x74
#define DSMCC1_TID     0x3A
#define DSMCC2_TID     0x3B /* DII TABLE */
#define DSMCC3_TID     0x3C /* DDB TABLE */
#define DSMCC4_TID     0x3D
#define DSMCC5_TID     0x3E
#define DSMCC6_TID     0x3F
/* **********************************/

#ifdef ANDROID_LOG
#define LOG(fmt, args...) __android_log_print(ANDROID_LOG_DEBUG, "DEMUX", fmt,   ##args);
#define LOGE(fmt, args...) __android_log_print(ANDROID_LOG_ERROR, "DEMUX", fmt,   ##args);
#else
#define LOG   printf
#define LOGE printf
#endif

#define CHECK_DEMUX_ALIVE(a)  if(a->alive == 0 ){\
                                                          LOGE("DEMUX : demux was destoryed.");\
                                                          pthread_mutex_unlock(&(a->section_table_mutex));\
                                                          return false;\
                                                       }

struct _event_list;
struct _event_head;

typedef struct _event_list 
{
	int event;
	int param;
	void *data;
	struct _event_list *next;
} event_list_t;

typedef struct _event_head 
{
	struct _event_list *volatile head;
	struct _event_list *last;
	pthread_cond_t cond;
	pthread_mutex_t lock;
} event_head_t;

enum
{
	DEMUX_EVENT_FEED_DATA = 0x5001,
	DEMUX_EVENT_EVENT_SECTION_FILTER_DATA	
};

/******************************************************************************
 * Structures
 *****************************************************************************/
/* TS Header */
typedef struct _ts_header {
    uint8_t  sync_byte;                    /*  8bit, 0x47 */
    uint8_t  transport_error_indicator;    /*  1bit */
    uint8_t  payload_unit_start_indicator; /*  1bit */
    uint8_t  transport_priority;           /*  1bit */
    uint16_t pid;                          /* 13bit, PacketID */
    uint8_t  scramble_control;             /*  2bit */
    uint8_t  adaptation_field_control;     /*  2bit */
    uint8_t  continuity_counter;           /*  4bit */
    uint8_t  adaptation_byte_length;       /*  8bit if exist */
} __attribute__((packed))ts_header;


/* adaptation field header */
typedef struct _adapt_field_header {
    uint8_t discontinuity_indicator;
    uint8_t random_access_indicator;
    uint8_t es_priority_indicator;
    uint8_t pcr_flag;
    uint8_t opcr_flag;
    uint8_t splicing_point_flag;
    uint8_t transport_private_data_flag;
    uint8_t adapt_field_extension_flag;
} __attribute__((packed))adapt_field_header;


/* Section header */
typedef struct _section_header {
    uint8_t  table_id;
    uint8_t  section_syntax_indicator;
    uint8_t  private_indicator;
    uint8_t  reserved;
    uint16_t section_length;
    uint16_t transport_stream_id;
    uint16_t version_number;
    uint8_t  current_next_indicator;
    uint8_t  section_number;
    uint8_t  last_section_number;
} __attribute__((packed))section_header;

typedef struct _local_buffer {
    uint8_t  *buffer;
    uint32_t length;
} local_buffer;

/* Section Buffer */
#define SECTION_HDR_LEN     8
#define SECTION_MAX_LEN     (4352)
typedef struct _section_buffer {
    uint32_t sect_len;
    uint32_t curr_len;
    uint8_t  buf[SECTION_MAX_LEN];
} section_buffer;

/* Section filter */
typedef void (*SectionFilterCallback)(
                        void *handle, 
                        uint16_t pid,
                        uint16_t tid,
                        local_buffer* buffer,
                        void  *user_param);

typedef void (*CheckIFrameCallback)(void );

typedef struct _section_filter {
	void             *user_param;
    SectionFilterCallback  callback;
    uint16_t           pid;
    uint16_t           tid;
    section_buffer   buffer;
} section_filter;

/* PES header */
typedef struct _pes_header {
    uint8_t  stream_id;
    uint32_t  pes_packet_length;
    uint8_t  pes_scrambling_control;
    uint8_t  pes_priority;
    uint8_t  data_alignment_indicator;
    uint8_t  copyright;
    uint8_t  original_or_copy;
    uint8_t  pts_dts_flags;
    uint8_t  escr_flag;
    uint8_t  es_rate_flag;
    uint8_t  dsm_trick_mode_flag;
    uint8_t  additional_copy_info_flag;
    uint8_t  pes_crc_flag;
    uint8_t  pes_extension_flag;
    int16_t  pes_header_data_length;
}  __attribute__((packed))pes_header;

/* PES filter */
typedef struct _es_buffer {
    uint32_t data_size;
} es_buffer;

typedef struct _pes_filter {
    uint16_t pid;
    uint16_t codec_type;
    int32_t  strm_type;

    int32_t  stream_id_extension;

    uint32_t state;
    pes_header header;
    int32_t  header_pos;
    uint8_t  header_buf[PES_HDR_BUF_LEN];

    es_buffer es_buf;
} pes_filter;

/* TS filter */
typedef struct _ts_filter {
    uint16_t pid;
    uint16_t tid;   
    int32_t last_cc;
    int64_t last_pcr;
    section_filter *sect;
    pes_filter *pes;
} ts_filter;


#define FILTERS_MAX    100
typedef struct _ts_filter_table {
    int32_t type; 
    int32_t max_cnt;
    int32_t used_cnt;
    ts_filter filters[FILTERS_MAX];
} ts_filter_table;

typedef struct _si_program {
    uint16_t program_number;
    uint16_t pid;
} si_program;

typedef struct _si_pmt_es {
    uint8_t  stream_type;
    uint16_t elementary_pid;
} si_pmt_es;

typedef struct _si_descriptor_info {
    uint8_t descriptor_tag;     /**< descriptor tag */
    uint8_t *start;             /**< pointer to 1st byte of the descriptor */
} si_descriptor_info;


/* Demuxer */
typedef struct _demux_context {

    int32_t           alive;
    int32_t           pcr_pid;
    ts_filter_table filter_table[FILTER_TYPE_MAX];
    int64_t           curr_pcr;
    uint32_t         ts_size;

    /* PMT or Network service info */   
    si_program     program[MAX_PROGRAM_COUNT];
    uint16_t         program_count; 
    uint16_t         pmt_count; 
    uint8_t           pmt_descriptor[MAX_PMT_DESC_SIZE];
    uint16_t         pmt_desc_len;
    uint16_t         es_count;	
    uint8_t           es_descriptor[MAX_ES_COUNT][MAX_PMT_DESC_SIZE];
    uint16_t         es_desc_len[MAX_ES_COUNT];     
    si_pmt_es       es_info[MAX_ES_COUNT];

    /* mutex */
    pthread_mutex_t section_table_mutex;

    /*codec for I freame */
    uint32_t video_codec;	

	// event	
	event_head_t demux_event;	

	// thread stop request flag	
	int stop_requested;	
	pthread_t demux_thread;	
	
} demux_context;

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 * GLOBAL FUNCTION
 *****************************************************************************/
/* demux.c */
bool       DEMUX_Init(demux_context ** handle, int ts_size) ;
void       DEMUX_Destroy(demux_context ** handle) ;
bool       DEMUX_InitSectionFilter(demux_context * handle);
int        DEMUX_GetTsSize(char* buffer, int size);
bool       DEMUX_FeedData(demux_context *handle, char *buf, uint32_t size);
bool       DEMUX_ProcFeedData(demux_context *handle, char *buf, uint32_t size);
bool       DEMUX_CheckIFrame(demux_context *handle, char *buf, uint32_t size, bool* pIsIframe);
bool       DEMUX_SetSectionFilter(demux_context *handle, section_filter *sect);
bool       DEMUX_SetPesFilter(demux_context *demux, int32_t ftype, int32_t pid, int32_t codec_type);
bool       DEMUX_ClearSectionFilter(demux_context *handle, section_filter *sect);
bool       DEMUX_ClearAllSectionFilter(demux_context *handle);
bool       DEMUX_ClearAllPesFilter(demux_context *handle);
uint32_t DEMUX_SectionAddData(demux_context *handle, ts_header *ts_hdr, 
                                uint8_t *data, uint32_t size, section_filter *filter);
uint32_t DEMUX_Pes_AddData(demux_context *handle, ts_header *ts_hdr, 
                                uint8_t *data, uint32_t size, pes_filter *filter, bool* pIsIframe);

/* demux_section.c */
void       DEMUX_PATCallback(void *handle, uint16_t pid, uint16_t tid,local_buffer* buffer, void* user_parma);
void       DEMUX_PMTCallback(void *handle, uint16_t pid, uint16_t tid,local_buffer* buffer, void* user_parma);
void       DEMUX_AITCallback(void *handle, uint16_t pid, uint16_t tid,local_buffer* buffer, void* user_parma);
void       DEMUX_DSMCCCallback(void *handle, uint16_t pid, uint16_t tid,local_buffer* buf, void* user_parma);

#ifdef __cplusplus
}
#endif

#endif /* DEMUXER_H */
