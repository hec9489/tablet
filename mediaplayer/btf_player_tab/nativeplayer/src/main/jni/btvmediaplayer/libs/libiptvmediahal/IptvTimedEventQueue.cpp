// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#undef __STRICT_ANSI__
#define __STDINT_LIMITS
#define __STDC_LIMIT_MACROS
//#include <stdint.h>

//#define LOG_NDEBUG 0
#define LOG_TAG	"IptvTimedEventQueue"
#include <ALog.h>
//#include <threads.h>

#include "IptvTimedEventQueue.h"

#include <sys/prctl.h>
#include <sys/time.h>

namespace android
{ }

using namespace std::chrono;

IptvTimedEventQueue::IptvTimedEventQueue()
	: mNextEventID(1), 
	  mRunning(false), 
	  mStopped(false)
{

}

IptvTimedEventQueue::~IptvTimedEventQueue()
{
	stop();
}

void IptvTimedEventQueue::start()
{
	if (mRunning)
	{
		return;
	}

	mStopped = false;

	pthread_attr_t attr;
	pthread_attr_init(&attr);
   	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	pthread_create(&mThread, &attr, ThreadWrapper, this);

	pthread_attr_destroy(&attr);

	mRunning = true;
}

void IptvTimedEventQueue::stop(bool flush)
{
	if (!mRunning)
	{
		return;
	}

	if (flush)
	{
		postEventToBack(std::make_shared<StopEvent>());
	}
	else
	{
		postTimedEvent(std::make_shared<StopEvent>(), INT64_MIN);
	}

	void *dummy;
	pthread_join(mThread, &dummy);

	mQueue.clear();

	mRunning = false;
}

IptvTimedEventQueue::event_id IptvTimedEventQueue::postEvent(const std::shared_ptr<Event> &event)
{
	// Reserve an earlier timeslot an INT64_MIN to be able to post
	// the StopEvent to the absolute head of the queue.
	return postTimedEvent(event, INT64_MIN + 1);
}

IptvTimedEventQueue::event_id IptvTimedEventQueue::postEventToBack(const std::shared_ptr<Event> &event)
{
	return postTimedEvent(event, INT64_MAX);
}

IptvTimedEventQueue::event_id IptvTimedEventQueue::postEventWithDelay(const std::shared_ptr<Event> &event, int64_t delay_us)
{
	//CHECK(delay_us >= 0);
	return postTimedEvent(event, getRealTimeUs() + delay_us);
}

IptvTimedEventQueue::event_id IptvTimedEventQueue::postTimedEvent(const std::shared_ptr<Event> &event, int64_t realtime_us)
{
	std::unique_lock<std::mutex> lock(mLock);

	event->setEventID(mNextEventID++);

	list<QueueItem>::iterator it = mQueue.begin();
	while (it != mQueue.end() && realtime_us >= (*it).realtime_us)
	{
		++it;
	}

	QueueItem item;
	item.event = event;
	item.realtime_us = realtime_us;

	if (it == mQueue.begin())
	{
		mQueueHeadChangedCondition.notify_one();
	}

	mQueue.insert(it, item);

	mQueueNotEmptyCondition.notify_one();

	return event->mEventID;
}

static bool MatchesEventID(void *cookie, const std::shared_ptr<IptvTimedEventQueue::Event> &event)
{
	IptvTimedEventQueue::event_id *id = static_cast<IptvTimedEventQueue::event_id *>(cookie);

	if (event->eventID() != *id)
	{
		return false;
	}

	*id = 0;

	return true;
}

bool IptvTimedEventQueue::cancelEvent(event_id id) {
    if (id == 0) {
        return false;
    }

    cancelEvents(&MatchesEventID, &id, true /* stopAfterFirstMatch */);

    // if MatchesEventID found a match, it will have set id to 0
    // (which is not a valid event_id).

    return id == 0;
}

bool IptvTimedEventQueue::cancelAllEvent() {
 
	std::unique_lock<std::mutex> lock(mLock);

	mQueue.clear();

	mQueueHeadChangedCondition.notify_one();
    
	return true;
}

void IptvTimedEventQueue::cancelEvents(
		bool (*predicate)(void *cookie, const std::shared_ptr<Event> &event), 
		void *cookie, 
		bool stopAfterFirstMatch)
{
	std::unique_lock<std::mutex> lock(mLock);

	list<QueueItem>::iterator it = mQueue.begin();
	while (it != mQueue.end())
	{
		if (!(*predicate)(cookie, (*it).event))
		{
			++it;
			continue;
		}

		if (it == mQueue.begin()) 
		{
			mQueueHeadChangedCondition.notify_one();
		}

		//LOGV("[%s] cancelling event %d\n[%s]", __FUNCTION__, (*it).event->eventID(), __FILE__);

		(*it).event->setEventID(0);
		it = mQueue.erase(it);

		if (stopAfterFirstMatch)
		{
			return;
		}
	}
}

// static
int64_t IptvTimedEventQueue::getRealTimeUs()
{
	struct timeval tv;

	gettimeofday(&tv, NULL);

	return (int64_t)tv.tv_sec * 1000000ll + tv.tv_usec;
}

// static
void *IptvTimedEventQueue::ThreadWrapper(void *me)
{
#ifdef ANDROID_SIMULATOR
	// The simulator runs everything as one process, so any
	// binder calls happen on this thread instead of a thread
	// in anothrer process. We therefore need to make sure that
	// this thread can do calls into interpreted code.
	// On the device this is not an issue because the remote
	// thread will already be set up correctly for this.
	JavaVM *vm;
	int numvms;
	JNI_GetCreatedJavaVMs(&vm, 1, &numvms);
	JNIEnv *env;
	vm->AttachCurrentThread(&env, NULL);
#endif

//	androidSetThreadPriority(0, ANDROID_PRIORITY_FOREGROUND);

	static_cast<IptvTimedEventQueue *>(me)->threadEntry();

#ifdef ANDROID_SIMULATOR
	vm->DetachCurrenThread();
#endif 
	return NULL;

}

void IptvTimedEventQueue::threadEntry()
{
	prctl(PR_SET_NAME, (unsigned long)"IptvTimedEventQueue", 0, 0, 0);

	for (;;)
	{
		int64_t now_us = 0;
		std::shared_ptr<Event> event;

		{
			std::unique_lock<std::mutex> lock(mLock);

			if (mStopped)
			{
				break;
			}

			while (mQueue.empty())
			{
				mQueueNotEmptyCondition.wait(lock);
			}

			event_id eventID = 0;
			for (;;)
			{
				if (mQueue.empty())
				{
					// The only event in the queue could have been cancelled
					// while we were waiting for its scheduled time.
					break;
				}

				list<QueueItem>::iterator it = mQueue.begin();
				eventID = (*it).event->eventID();

				now_us = getRealTimeUs();
				int64_t when_us = (*it).realtime_us;

				int64_t delay_us;
				if (when_us < 0 || when_us == INT64_MAX)
				{
					delay_us = 0;
				}
				else
				{
					delay_us = when_us - now_us;
				}
				if (delay_us <= 0)
				{
					break;
				}

				static int64_t kMaxTimeoutUs = 10000000ll; 	// 10 secs
				bool timeoutCapped = false;
				if (delay_us > kMaxTimeoutUs)
				{
					//LOGW("delay_us dxceeds max timeout: %lld us", delay_us);

					// We'll never block for more than 10 secs, instead
					// we will split up the full timeout into chunks of
					// 10 secs at a time. this will also avoid overflow
					// when converting from us to ns.
					delay_us = kMaxTimeoutUs;
					timeoutCapped = true;
				}

				auto status = mQueueHeadChangedCondition.wait_for(lock, milliseconds(delay_us / 1000ll), [] { return false;}); //{ return true;}
//				int32_t err = mQueueHeadChangedCondition.waitRelative(mLock, delay_us * 1000ll);

				if (!timeoutCapped && status)
				{
					// We finally hit the time this event is supposed to trigger.
					now_us = getRealTimeUs();
					break;
				}
			}

			// The event w/ this id may have been cancelled while we're
			// waiting for its trigger-time, in that case
			// removeEventFromQueue_l will return NULL.
			// Otherwise, the QueueItem will be removed
			// from the queue and the referenced event returned.
			event = removeEventFromQueue_l(eventID);
		}

		if (event != NULL)
		{
			// Fire event with the lock NOT held.
			event->fire(this, now_us);
		}
	}
}

std::shared_ptr<IptvTimedEventQueue::Event> IptvTimedEventQueue::removeEventFromQueue_l(event_id id)
{
	for (list<QueueItem>::iterator it = mQueue.begin(); it != mQueue.end(); ++it)
	{
		if ((*it).event->eventID() == id)
		{
			std::shared_ptr<Event> event = (*it).event;
			event->setEventID(0);

			mQueue.erase(it);

			return event;
		}
	}

	//LOGW("[%s] Event %d was not foundin the queue, already cancelled?\n[%s]", __FUNCTION__, id, __FILE__);

	return NULL;
}
