// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef __dtvcc_h__
#define __dtvcc_h__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <signal.h>
#include <fcntl.h>
#include <stdarg.h>
#include <errno.h>
#include <pthread.h>
#include <assert.h>
#define __STDC_FORMAT_MACROS
#include <unistd.h>
#define __STDC_LIMIT_MACROS
#include <inttypes.h>

#include <dsmcc.h>

/**************** TTA CERTIFICATION **************/
//20210517 yiwoo define DTVCC_TTA_CERT_MODE
//1st : kec_mode=1 when exist /storage/emulated/0/TTAchannel.json file
//2nd : Real TTA_CERT_MODE depends on kec_mode 1 or 0, 
#define DTVCC_TTA_CERT_MODE 

/**************** TTA CERTIFICATION **************/

#define DEBUG_LOG_ENABLE 
//#define DEBUG_CC_PACKETS 
//#define DTVCC_PRINT_DEBUG 
#define DTVCC_CC_PRINT_DEBUG 

//#define ENABLE_DSMCC_LOGIC 

#define CCX_WHAT_EVENT_CC 8
#define CCX_WHAT_EVENT_PID_CHAGNED 9

#define CCX_EVENT_VPID_CHAGNED 0
#define CCX_EVENT_APID_CHAGNED 1
#define CCX_EVENT_NUM_APID_CHAGNED 2

#define CCX_ITU_T35_CONTRY_CODE 0xB5
#define CCX_ITU_T35_PROVIDER_CODE 0x0031

#define CCX_USERDATA_STARTCODE_MPEG2 0x000001B2

#define CCX_USER_IDENTIFIER 0x47413934
#define CCX_USERDATA_TYPE_CODE 0x03

#define CCX_DTVCC_MAX_PACKET_LENGTH 128 //According to EIA-708B, part 5
#define CCX_DTVCC_NO_LAST_SEQUENCE -1

#define INITIAL_ENC_BUFFER_CAPACITY	512

#define CC_DATA_BUFFER_CAPACITY	2048

#define CCX_DTVCC_MAX_SERVICES 63

#define CCX_DTVCC_MAX_ROWS 15

/**
 * This value should be 32, but there were 16-bit encoded samples,
 * where RowCount calculated another way and equals 46 (23[8bit]*2)
 */
#define CCX_DTVCC_MAX_COLUMNS (32*2)

#define CCX_DTVCC_SCREENGRID_ROWS 75
#define CCX_DTVCC_SCREENGRID_COLUMNS 210

#define CCX_DTVCC_MAX_WINDOWS 8

#define CCX_DTVCC_MUSICAL_NOTE_UNICODE_CHAR 0x266C	// Unicode Character 'BEAMED SIXTEENTH NOTES'
#define CCX_DTVCC_MUSICAL_NOTE_EUCKR_CHAR 0xA2DD	// euc-kr Character 'BEAMED SIXTEENTH NOTES'

#define CCX_DTVCC_TIME_OUT_SEC 16	// Timeout 16 sec

#define CCX_DSMCC_TIME_OUT_SEC 60	// Timeout 1 minute

#define CCX_DTVCC_TIME_OUT_1_SEC 1	// Timeout 1 sec

#define CCX_DTVCC_CHECK_GAP_TIME_MS 10000
#define CCX_DTVCC_CHECK_TIME_MS 15000

#define CCX_DTVCC_ACTIVCE_CHECK_TIME_MS 1000

#define CCX_DTVCC_WINDOW_INIT 0x0F

// vi audio types
enum CCX_DTVCC_AUDIO_TYPES
{
	CCX_DTVCC_AUDIO_KCM 	= 0,
	CCX_DTVCC_AUDIO_KVI 	= 1,
	CCX_DTVCC_AUDIO_ECM 	= 2,
	CCX_DTVCC_AUDIO_EVI 	= 3
};

// expired_action
enum CCX_DTVCC_EXPIRED_ACTIONS
{
	CCX_DTVCC_EXPIRED_ACTION_CLEAR_WINDOW = 0x0001,
	CCX_DTVCC_EXPIRED_ACTION_DSMCC_REQUEST,
	CCX_DTVCC_EXPIRED_ACTION_KEC_PMT_REQUEST,
	CCX_DTVCC_EXPIRED_ACTION_AUDIO_CHECK_REQUEST
};

// event types
enum CCX_DTVCC_EVENTS
{
	CCX_DTVCC_EVENT_PICTURE_DATA = 0x9001,
	CCX_DTVCC_EVENT_SECTION_FILTER_DATA,
	CCX_DTVCC_EVENT_DSMCC_SECTION_FILTER_DATA	
};

enum CCX_DTVCC_TV_EVENTS
{
	CCX_DTVCC_TV_EVENT_CAPTION_DATA  	= 0x01,
	CCX_DTVCC_TV_EVENT_CLEAR_WINDOW  	= 0x02,
	CCX_DTVCC_TV_EVENT_WINDOW_COLOR  	= 0x03,
	CCX_DTVCC_TV_EVENT_WINDOW_ROW_COL	= 0x04,
	CCX_DTVCC_TV_EVENT_FONT_SIZE		= 0x05,
	CCX_DTVCC_TV_EVENT_WINDOW_POINT		= 0x06,	
	CCX_DTVCC_TV_EVENT_WINDOW_ANCHOR	= 0x07,
	CCX_DTVCC_TV_EVENT_WINDOW_POSITON	= 0x08,	
	CCX_DTVCC_TV_EVENT_WINDOW_JUSTIFY	= 0x09,	
	CCX_DTVCC_TV_EVENT_DELETE_WINDOW	= 0x0A, 
	CCX_DTVCC_TV_EVENT_TTA_CERT_MODE	= 0x0F
};

// cc types
enum CCX_DTVCC_TYPES
{
	CCX_NTSC_CC_FIELD_1 	= 0,
	CCX_NTSC_CC_FIELD_2 	= 1,
	CCX_DTVCC_PACKET_DATA 	= 2,
	CCX_DTVCC_PACKET_START 	= 3
};

enum CCX_DTVCC_COMMANDS_C0_CODES
{
	CCX_DTVCC_C0_NUL = 	0x00,
	CCX_DTVCC_C0_ETX = 	0x03,
	CCX_DTVCC_C0_BS = 	0x08,
	CCX_DTVCC_C0_FF = 	0x0c,
	CCX_DTVCC_C0_CR = 	0x0d,
	CCX_DTVCC_C0_HCR = 	0x0e,
	CCX_DTVCC_C0_EXT1 = 0x10,
	CCX_DTVCC_C0_P16 = 	0x18
};

enum CCX_DTVCC_COMMANDS_C1_CODES
{
	CCX_DTVCC_C1_CW0 = 0x80,
	CCX_DTVCC_C1_CW1 = 0x81,
	CCX_DTVCC_C1_CW2 = 0x82,
	CCX_DTVCC_C1_CW3 = 0x83,
	CCX_DTVCC_C1_CW4 = 0x84,
	CCX_DTVCC_C1_CW5 = 0x85,
	CCX_DTVCC_C1_CW6 = 0x86,
	CCX_DTVCC_C1_CW7 = 0x87,
	CCX_DTVCC_C1_CLW = 0x88,
	CCX_DTVCC_C1_DSW = 0x89,
	CCX_DTVCC_C1_HDW = 0x8A,
	CCX_DTVCC_C1_TGW = 0x8B,
	CCX_DTVCC_C1_DLW = 0x8C,
	CCX_DTVCC_C1_DLY = 0x8D,
	CCX_DTVCC_C1_DLC = 0x8E,
	CCX_DTVCC_C1_RST = 0x8F,
	CCX_DTVCC_C1_SPA = 0x90,
	CCX_DTVCC_C1_SPC = 0x91,
	CCX_DTVCC_C1_SPL = 0x92,
	CCX_DTVCC_C1_RSV93 = 0x93,
	CCX_DTVCC_C1_RSV94 = 0x94,
	CCX_DTVCC_C1_RSV95 = 0x95,
	CCX_DTVCC_C1_RSV96 = 0x96,
	CCX_DTVCC_C1_SWA = 0x97,
	CCX_DTVCC_C1_DF0 = 0x98,
	CCX_DTVCC_C1_DF1 = 0x99,
	CCX_DTVCC_C1_DF2 = 0x9A,
	CCX_DTVCC_C1_DF3 = 0x9B,
	CCX_DTVCC_C1_DF4 = 0x9C,
	CCX_DTVCC_C1_DF5 = 0x9D,
	CCX_DTVCC_C1_DF6 = 0x9E,
	CCX_DTVCC_C1_DF7 = 0x9F
};

struct CCX_DTVCC_S_COMMANDS_C1
{
	int code;
	const char *name;
	const char *description;
	int length;
};

enum ccx_dtvcc_window_justify
{
	CCX_DTVCC_WINDOW_JUSTIFY_LEFT	= 0,
	CCX_DTVCC_WINDOW_JUSTIFY_RIGHT	= 1,
	CCX_DTVCC_WINDOW_JUSTIFY_CENTER	= 2,
	CCX_DTVCC_WINDOW_JUSTIFY_FULL	= 3
};

enum ccx_dtvcc_window_pd //Print Direction
{
	CCX_DTVCC_WINDOW_PD_LEFT_RIGHT = 0, //left -> right
	CCX_DTVCC_WINDOW_PD_RIGHT_LEFT = 1,
	CCX_DTVCC_WINDOW_PD_TOP_BOTTOM = 2,
	CCX_DTVCC_WINDOW_PD_BOTTOM_TOP = 3
};

enum ccx_dtvcc_window_sd //Scroll Direction
{
	CCX_DTVCC_WINDOW_SD_LEFT_RIGHT = 0,
	CCX_DTVCC_WINDOW_SD_RIGHT_LEFT = 1,
	CCX_DTVCC_WINDOW_SD_TOP_BOTTOM = 2,
	CCX_DTVCC_WINDOW_SD_BOTTOM_TOP = 3
};

enum ccx_dtvcc_window_sde //Scroll Display Effect
{
	CCX_DTVCC_WINDOW_SDE_SNAP = 0,
	CCX_DTVCC_WINDOW_SDE_FADE = 1,
	CCX_DTVCC_WINDOW_SDE_WIPE = 2
};

enum ccx_dtvcc_window_ed //Effect Direction
{
	CCX_DTVCC_WINDOW_ED_LEFT_RIGHT = 0,
	CCX_DTVCC_WINDOW_ED_RIGHT_LEFT = 1,
	CCX_DTVCC_WINDOW_ED_TOP_BOTTOM = 2,
	CCX_DTVCC_WINDOW_ED_BOTTOM_TOP = 3
};

enum ccx_dtvcc_window_fo //Fill Opacity
{
	CCX_DTVCC_WINDOW_FO_SOLID		= 0,
	CCX_DTVCC_WINDOW_FO_FLASH		= 1,
	CCX_DTVCC_WINDOW_FO_TRANSLUCENT	= 2,
	CCX_DTVCC_WINDOW_FO_TRANSPARENT = 3
};

enum ccx_dtvcc_window_border
{
	CCX_DTVCC_WINDOW_BORDER_NONE			= 0,
	CCX_DTVCC_WINDOW_BORDER_RAISED			= 1,
	CCX_DTVCC_WINDOW_BORDER_DEPRESSED		= 2,
	CCX_DTVCC_WINDOW_BORDER_UNIFORM			= 3,
	CCX_DTVCC_WINDOW_BORDER_SHADOW_LEFT		= 4,
	CCX_DTVCC_WINDOW_BORDER_SHADOW_RIGHT	= 5
};

enum ccx_dtvcc_pen_size
{
	CCX_DTVCC_PEN_SIZE_SMALL 	= 0,
	CCX_DTVCC_PEN_SIZE_STANDART = 1,
	CCX_DTVCC_PEN_SIZE_LARGE	= 2
};

enum ccx_dtvcc_pen_font_style
{
	CCX_DTVCC_PEN_FONT_STYLE_DEFAULT_OR_UNDEFINED					= 0,
	CCX_DTVCC_PEN_FONT_STYLE_MONOSPACED_WITH_SERIFS					= 1,
	CCX_DTVCC_PEN_FONT_STYLE_PROPORTIONALLY_SPACED_WITH_SERIFS		= 2,
	CCX_DTVCC_PEN_FONT_STYLE_MONOSPACED_WITHOUT_SERIFS				= 3,
	CCX_DTVCC_PEN_FONT_STYLE_PROPORTIONALLY_SPACED_WITHOUT_SERIFS	= 4,
	CCX_DTVCC_PEN_FONT_STYLE_CASUAL_FONT_TYPE						= 5,
	CCX_DTVCC_PEN_FONT_STYLE_CURSIVE_FONT_TYPE						= 6,
	CCX_DTVCC_PEN_FONT_STYLE_SMALL_CAPITALS							= 7
};

enum ccx_dtvcc_pen_text_tag
{
	CCX_DTVCC_PEN_TEXT_TAG_DIALOG						= 0,
	CCX_DTVCC_PEN_TEXT_TAG_SOURCE_OR_SPEAKER_ID			= 1,
	CCX_DTVCC_PEN_TEXT_TAG_ELECTRONIC_VOICE				= 2,
	CCX_DTVCC_PEN_TEXT_TAG_FOREIGN_LANGUAGE				= 3,
	CCX_DTVCC_PEN_TEXT_TAG_VOICEOVER					= 4,
	CCX_DTVCC_PEN_TEXT_TAG_AUDIBLE_TRANSLATION			= 5,
	CCX_DTVCC_PEN_TEXT_TAG_SUBTITLE_TRANSLATION			= 6,
	CCX_DTVCC_PEN_TEXT_TAG_VOICE_QUALITY_DESCRIPTION	= 7,
	CCX_DTVCC_PEN_TEXT_TAG_SONG_LYRICS					= 8,
	CCX_DTVCC_PEN_TEXT_TAG_SOUND_EFFECT_DESCRIPTION		= 9,
	CCX_DTVCC_PEN_TEXT_TAG_MUSICAL_SCORE_DESCRIPTION	= 10,
	CCX_DTVCC_PEN_TEXT_TAG_EXPLETIVE					= 11,
	CCX_DTVCC_PEN_TEXT_TAG_UNDEFINED_12					= 12,
	CCX_DTVCC_PEN_TEXT_TAG_UNDEFINED_13					= 13,
	CCX_DTVCC_PEN_TEXT_TAG_UNDEFINED_14					= 14,
	CCX_DTVCC_PEN_TEXT_TAG_NOT_TO_BE_DISPLAYED			= 15
};

enum ccx_dtvcc_pen_offset
{
	CCX_DTVCC_PEN_OFFSET_SUBSCRIPT		= 0,
	CCX_DTVCC_PEN_OFFSET_NORMAL			= 1,
	CCX_DTVCC_PEN_OFFSET_SUPERSCRIPT	= 2
};

enum ccx_dtvcc_pen_edge
{
	CCX_DTVCC_PEN_EDGE_NONE					= 0,
	CCX_DTVCC_PEN_EDGE_RAISED				= 1,
	CCX_DTVCC_PEN_EDGE_DEPRESSED			= 2,
	CCX_DTVCC_PEN_EDGE_UNIFORM				= 3,
	CCX_DTVCC_PEN_EDGE_LEFT_DROP_SHADOW		= 4,
	CCX_DTVCC_PEN_EDGE_RIGHT_DROP_SHADOW	= 5
};

enum ccx_dtvcc_pen_anchor_point
{
	CCX_DTVCC_ANCHOR_POINT_TOP_LEFT 		= 0,
	CCX_DTVCC_ANCHOR_POINT_TOP_CENTER 		= 1,
	CCX_DTVCC_ANCHOR_POINT_TOP_RIGHT 		= 2,
	CCX_DTVCC_ANCHOR_POINT_MIDDLE_LEFT 		= 3,
	CCX_DTVCC_ANCHOR_POINT_MIDDLE_CENTER 	= 4,
	CCX_DTVCC_ANCHOR_POINT_MIDDLE_RIGHT 	= 5,
	CCX_DTVCC_ANCHOR_POINT_BOTTOM_LEFT 		= 6,
	CCX_DTVCC_ANCHOR_POINT_BOTTOM_CENTER 	= 7,
	CCX_DTVCC_ANCHOR_POINT_BOTTOM_RIGHT 	= 8
};

typedef struct ccx_dtvcc_pen_color
{
	int fg_color;
	int fg_opacity;
	int bg_color;
	int bg_opacity;
	int edge_color;
} ccx_dtvcc_pen_color;

typedef struct ccx_dtvcc_pen_attribs
{
	int pen_size;
	int offset;
	int text_tag;
	int font_tag;
	int edge_type;
	int underline;
	int italic;
} ccx_dtvcc_pen_attribs;

typedef struct ccx_dtvcc_window_attribs
{
	int justify;
	int print_direction;
	int scroll_direction;
	int word_wrap;
	int display_effect;
	int effect_direction;
	int effect_speed;
	int fill_color;
	int fill_opacity;
	int border_type;
	int border_color;
} ccx_dtvcc_window_attribs;

/**
 * Since 1-byte and 2-byte symbols could appear in captions and
 * since we have to keep symbols alignment and several windows could appear on a screen at one time,
 * we use special structure for holding symbols
 */
typedef struct ccx_dtvcc_symbol
{
	unsigned short sym; //symbol itself, at least 16 bit
	unsigned char init; //initialized or not. could be 0 or 1
} ccx_dtvcc_symbol;

#define CCX_DTVCC_SYM_SET(x, c) {x.init = 1; x.sym = c;}
#define CCX_DTVCC_SYM_SET_16(x, c1, c2) {x.init = 1; x.sym = (c1 << 8) | c2;}
#define CCX_DTVCC_SYM(x) ((unsigned char)(x.sym))
#define CCX_DTVCC_SYM_IS_EMPTY(x) (x.init == 0)
#define CCX_DTVCC_SYM_IS_SET(x) (x.init == 1)

typedef struct ccx_dtvcc_window
{
	int is_defined;
	int number;
	int priority;
	int col_lock;
	int row_lock;
	int visible;
	int anchor_vertical;
	int relative_pos;
	int anchor_horizontal;
	int row_count;
	int anchor_point;
	int col_count;
	int real_col_count;
	int pen_style;
	int win_style;
	unsigned char commands[6]; // Commands used to create this window
	ccx_dtvcc_window_attribs attribs;
	int pen_row;
	int pen_column;
	ccx_dtvcc_symbol *rows[CCX_DTVCC_MAX_ROWS];
	ccx_dtvcc_pen_color pen_colors[CCX_DTVCC_MAX_ROWS][CCX_DTVCC_SCREENGRID_COLUMNS];
	ccx_dtvcc_pen_attribs pen_attribs[CCX_DTVCC_MAX_ROWS][CCX_DTVCC_SCREENGRID_COLUMNS];
	ccx_dtvcc_pen_color pen_color_pattern;
	ccx_dtvcc_pen_attribs pen_attribs_pattern;
	int memory_reserved;
	int is_empty;
} ccx_dtvcc_window;

typedef struct dtvcc_tv_screen
{
	ccx_dtvcc_symbol chars[CCX_DTVCC_SCREENGRID_ROWS][CCX_DTVCC_SCREENGRID_COLUMNS];
	ccx_dtvcc_pen_color pen_colors[CCX_DTVCC_SCREENGRID_ROWS][CCX_DTVCC_SCREENGRID_COLUMNS];
	ccx_dtvcc_pen_attribs pen_attribs[CCX_DTVCC_SCREENGRID_ROWS][CCX_DTVCC_SCREENGRID_COLUMNS];
	int service_number;
} dtvcc_tv_screen;


typedef struct ccx_dtvcc_service_decoder
{
	ccx_dtvcc_window *windows[CCX_DTVCC_MAX_WINDOWS];	
	int current_window;
	dtvcc_tv_screen *tv;
} ccx_dtvcc_service_decoder;

typedef struct ccx_dtvcc_service_descriptor
{
	char language[4];
	int  digital_cc;
	int  caption_service_number;
	int  easy_reader;
	int  wide_aspect_ratio;	
	int  korean_code;	
} ccx_dtvcc_service_descriptor;

typedef struct ccx_dtvcc_ac3_stream_descriptor
{
	uint8_t		stream_type;
	uint16_t	pid;
	char 		sample_rate_code;
	char 		bsid;
	char 		bit_rate_code;
	char 		surround_mode;	
	char 		bsmod;	
	char 		full_svc;	
	char 		langcod;
	char 		langcod2;	
	char 		mainid;	
	char 		priority;		
	char 		asvcflags;
	char 		text_code;		
	int  		num_channels;
	int  		langflag;
	char 		language[4];	
	char 		language2[4];		
	char 		text_desc[128];	
} ccx_dtvcc_ac3_stream_descriptor;

typedef struct ccx_dtvcc_iso_639_language_descriptor
{
	uint8_t		stream_type;
	uint16_t	pid;
	char 		audio_type;
	char 		language[4];	

} ccx_dtvcc_iso_639_language_descriptor;

struct _ccx_dtvcc_audio_service_descriptor;
typedef struct _ccx_dtvcc_audio_service_descriptor
{
	ccx_dtvcc_ac3_stream_descriptor ac3_stream_descriptor;	
	ccx_dtvcc_iso_639_language_descriptor language_descriptor;
	struct _ccx_dtvcc_audio_service_descriptor *next;
} ccx_dtvcc_audio_service_descriptor;

typedef struct ccx_dtvcc_application_signalling_descriptor
{
	uint16_t	application_type ;
	uint8_t		ait_version;
} ccx_dtvcc_application_signalling_descriptor;

typedef struct ccx_dtvcc_application_descriptor
{
	uint16_t 	applicaiton_profile;
	uint8_t 	major;
	uint8_t 	minor;
	uint8_t 	micro;
	uint8_t 	service_bound_flag;
	uint8_t 	visibility;
	uint8_t 	application_priority;
	uint8_t 	transport_protocol_label;
} ccx_dtvcc_application_descriptor;

typedef struct ccx_dtvcc_application_name_descriptor
{
	char language[4];
	char application_name[64];
} ccx_dtvcc_application_name_descriptor;

typedef struct ccx_dtvcc_dvbj_application_location_descriptor
{
	char base_directory[64];
	char classpath_extension[64];
	char initial_class[64];
} ccx_dtvcc_dvbj_application_location_descriptor;

typedef struct ccx_dtvcc_application_storage_descriptor
{
	uint8_t 	storage_property;
	uint8_t 	not_launchable_from_broadcast;
	uint8_t 	launchable_completely_from_cache;
	uint8_t 	is_launchable_with_older_version;
	uint32_t	version;
	uint8_t		priority;	
} ccx_dtvcc_application_storage_descriptor;

typedef struct ccx_dtvcc_application_location_descriptor
{
	char	initial_path_bytes[254];	
} ccx_dtvcc_application_location_descriptor;

typedef struct ccx_dtvcc_application_tranport_descriptor
{
	uint16_t	protocol_id ;
	uint8_t		transport_protocol_label;
	uint8_t		remote_connection;	
	uint8_t		component_tag;		
} ccx_dtvcc_application_tranport_descriptor;

typedef struct
{
	uint16_t	appication_type;
	uint32_t	organisation_id;
	uint16_t	application_id;
	uint8_t		application_control_code;
	uint16_t	application_descriptor_loop_length;	
} ccx_dtvcc_appication;

typedef struct ccx_dtvcc_application_info_table
{
	ccx_dtvcc_appication application;
	ccx_dtvcc_application_descriptor application_descriptor;
	ccx_dtvcc_application_name_descriptor name_descriptor;
	ccx_dtvcc_dvbj_application_location_descriptor dvbj_location_descriptor;
	ccx_dtvcc_application_storage_descriptor storage_descriptor;
	ccx_dtvcc_application_location_descriptor location_descriptor;	
	ccx_dtvcc_application_tranport_descriptor transport_descriptor;
} ccx_dtvcc_application_info_table;

typedef struct ccx_dtvcc_carousel_id_descriptor
{
	uint32_t	carousel_id;
	uint8_t		format_id;	
} ccx_dtvcc_carousel_id_descriptor;

typedef struct ccx_dtvcc_association_tag_descriptor
{
	uint16_t	association_tag;
	uint16_t	use;
	uint32_t	transaction_id;
	uint32_t	timeout;
} ccx_dtvcc_association_tag_descriptor;

typedef struct ccx_dtvcc_broadcast_id_descriptor
{
	uint16_t	data_broadcast_id;
} ccx_dtvcc_broadcast_id_descriptor;

typedef struct ccx_dtvcc_stream_id_descriptor
{
	uint8_t		component_tag;
} ccx_dtvcc_stream_id_descriptor;


typedef struct ccx_dtvcc_dsmcc_stream_service
{
	uint8_t		stream_type;
	uint16_t	pid;
	ccx_dtvcc_carousel_id_descriptor carousel_id_descriptor;	
	ccx_dtvcc_association_tag_descriptor association_tag_descriptor;	
	ccx_dtvcc_broadcast_id_descriptor broadcast_id_descriptor;	
	ccx_dtvcc_stream_id_descriptor stream_id_descriptor;		
} ccx_dtvcc_dsmcc_stream_service;

struct _ccx_dtvcc_dsmcc_stream;
typedef struct _ccx_dtvcc_dsmcc_stream {
	uint16_t pid;
	uint16_t assoc_tag;
	struct _ccx_dtvcc_dsmcc_stream *next;
} ccx_dtvcc_dsmcc_stream;

typedef struct ccx_dtvcc_ait_stream_service
{
	uint8_t		stream_type;
	uint16_t	pid;
	ccx_dtvcc_application_signalling_descriptor signalling_descriptor;
} ccx_dtvcc_ait_stream_service;


struct _ccx_event_list;
struct _ccx_event_head;

typedef struct _ccx_event_list 
{
	int event;
	int param;
	void *data;
	struct _ccx_event_list *next;
} ccx_event_list_t;

typedef struct _ccx_event_head 
{
	struct _ccx_event_list *volatile head;
	struct _ccx_event_list *last;
	pthread_cond_t cond;
	pthread_mutex_t lock;
} ccx_event_head_t;

typedef struct ccx_section_data
{					//	section filter handle
	void *section_user_param;			//	user section callback parameter
	void *section_data;					//	section data pointer. which have received section.
	unsigned int section_data_size;		//	section data size.
} ccx_section_data;

typedef struct ccx_dsmcc_section_data
{
	void *section_user_param;			//	user section callback parameter
	void *section_data;					//	section data pointer. which have received section.
	unsigned int section_data_size;		//	section data size.
} ccx_dsmcc_section_data;

typedef void (*cc_fcallback)(void* handle, int what, int arg1, int arg2, const char* data);

typedef struct ccx_dtvcc_ctx
{
	int active_service_num;

	int is_active;
	int is_audio_active;
	int is_audio_play;	

	int audio_language;
	int vi_audio_count;

	int kec_mode;

	uint16_t video_pid;
	uint16_t audio_pid;	
	uint8_t  num_audio_pids;

	uint16_t main_audio_pid;
	uint16_t main_audio_codec;

	int init_korean_code;
	int korean_code;	
	int check_character_cnt;
	int check_unicode_routine;	

	ccx_dtvcc_service_decoder *decoders[CCX_DTVCC_MAX_SERVICES];
	ccx_dtvcc_service_descriptor cc_descriptor;	
	
	ccx_dtvcc_audio_service_descriptor audio_descriptor;
	ccx_dtvcc_audio_service_descriptor *audio_descriptor_head;

	ccx_dtvcc_application_info_table application_info;

	ccx_dtvcc_dsmcc_stream_service dsmcc_stream;
	ccx_dtvcc_ait_stream_service ait_stream;
	
	ccx_dtvcc_dsmcc_stream *dsmcc_stream_head;
	uint16_t	dsmcc_pid;
	int on_dsmcc_request_section_filter;
	
	unsigned char current_packet[CCX_DTVCC_MAX_PACKET_LENGTH];
	int current_packet_length;
	int start_packet_recved;	

	int last_sequence;

	char buffer[INITIAL_ENC_BUFFER_CAPACITY];
	char encoded_buf[INITIAL_ENC_BUFFER_CAPACITY];
		
	int rollup_screen_update;

	int delay_usec;

	timer_t timer_id;
	int expired_action;

	// event
	ccx_event_head_t dtvcc_event;

	// thread stop request flag
	int stop_requested;
	pthread_t ccx_dtvcc_thread;

	unsigned int time_ms_prev;
	unsigned int time_ms_check;	

	// pid
	uint16_t	pid;
	int on_request_section_filter;
	
	// initial_internal
	int initial_internal;

	// iconv descriptor
	void* cd;	

	void* handle;
	cc_fcallback cc_callback;

	char cc_data[CC_DATA_BUFFER_CAPACITY];
	int cc_data_size;	

	int screen_content_changed;

} ccx_dtvcc_ctx;

void ccx_dtvcc_log_dump(const char* data, int lengths);
void ccx_dtvcc_log(const char *format, ...);

void ccx_dtvcc_callback_event(ccx_dtvcc_ctx *ctx, int cw, int event, void *data);
void ccx_dtvcc_pid_callback_event(ccx_dtvcc_ctx *ctx, int event, void *data);

void ccx_dtvcc_post_picture_data(ccx_dtvcc_ctx *ctx, void *data, int size);
void ccx_dtvcc_post_section_filter_data(ccx_dtvcc_ctx *ctx, void *data, int size, void *user_param);

void ccx_dtvcc_init_internal(ccx_dtvcc_ctx *ctx);
void ccx_dtvcc_finish_internal(ccx_dtvcc_ctx *ctx);

void ccx_dtvcc_init(void *handle);
void ccx_dtvcc_destroy(void *handle);
void ccx_dtvcc_finish(ccx_dtvcc_ctx *ctx);


#ifdef __cplusplus
}
#endif

#endif //__dtvcc_h__
