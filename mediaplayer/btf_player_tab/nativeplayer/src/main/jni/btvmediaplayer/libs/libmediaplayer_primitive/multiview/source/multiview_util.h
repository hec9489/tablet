// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifdef __cplusplus
extern "C" {
#endif

#ifndef WORK_MULTIVIEW_UTIL_H
#define WORK_MULTIVIEW_UTIL_H

#include <libavformat/avformat.h>
#include <libavutil/threadmessage.h>


typedef struct _ST_QUEUE_MESSAGE{
	AVThreadMessageQueue *msg_queue;
	int                   refCount;
}ST_QUEUE_MESSAGE;

typedef struct _ST_MESSAGE{
	AVPacket    packet;
	AVRational  time_base;
	int         streamId;          // channelIdx as deviceId for pixtree lib.
}ST_MESSAGE;

#define     MVQUEUE_MSG_FREEZING        -9999


int mvQueue_init();
int mvQueue_getInstance(ST_QUEUE_MESSAGE** instance);
int mvQueue_unRefInstance(ST_QUEUE_MESSAGE** queue);
int mvQueue_pushMessage(ST_QUEUE_MESSAGE* pQueue, ST_MESSAGE* msg);
int mvQueue_popMessage(ST_QUEUE_MESSAGE* pQueue, ST_MESSAGE* msg);
int mvQueue_freezing(ST_QUEUE_MESSAGE* pQueue);
int mvQueue_unfreezing(ST_QUEUE_MESSAGE* pQueue);

void log_packet(const AVFormatContext *fmt_ctx, const AVPacket *pkt);

#ifdef __cplusplus
} //extern "C"
#endif

#endif // WORK_MULTIVIEW_UTIL_H