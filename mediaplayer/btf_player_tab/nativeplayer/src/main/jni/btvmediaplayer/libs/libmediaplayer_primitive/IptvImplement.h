// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef __iptvimplement_h__
#define __iptvimplement_h__

#ifdef __cplusplus
extern "C" {
#endif

void ST_InitPlayer(dvb_player_t *player, int idx_player, int jitter, int non_tunneled);
void ST_SetWindowSize(dvb_player_t *player, int x, int y, int width, int height);
int ST_setPlayerSurfaceView(dvb_player_t *player, int deviceID, uint32_t nativWindows);
int ST_setPlayerSurface(dvb_player_t *player, int deviceID, const void *m_pAnative);
void ST_SetVideoDelay(dvb_player_t *player, AVP_AVoffset offset);
void ST_SetAVOffset(dvb_player_t *player, AVP_AVoffset offset);
int ST_PlayerGetBufferStatus (dvb_player_t *player);
int SPTEK_TuneMultiviewChannel(dvb_player_t *player, dvb_locator_t *locator_head, AVP_MediaCallback pSoc_player_callback);
int ST_TuneChannel(dvb_player_t *player, dvb_locator_t *locator_head, AVP_MediaCallback pSoc_player_callback);
int ST_ChangeChannel(dvb_player_t *player, dvb_locator_t *locator_head, AVP_MediaCallback pSoc_player_callback);
int ST_CloseChannel(dvb_player_t *player, dvb_locator_t *locator_head);
int ST_StopChannel(dvb_player_t *player, dvb_locator_t *locator_head);
int ST_StopChannelSilent(dvb_player_t *player, dvb_locator_t *locator_head);
int ST_OpenTsFile(dvb_player_t *player, dvb_locator_t *locator_head, int nWillOpenDevice,AVP_MediaCallback pSoc_player_callback);
int ST_OpenRTSP(dvb_player_t *player, dvb_locator_t *locator_head, int nWillOpenDevice, AVP_MediaCallback pSoc_player_callback);
int ST_PlayStDvbBuffer(dvb_player_t *player, dvb_locator_t *locator_head);
int ST_StopStDvbBuffer(dvb_player_t *player, dvb_locator_t *locator_head);
int ST_PauseBuffer(dvb_player_t *player);
int ST_ResumeBuffer(dvb_player_t *player);
int ST_FlushAVBuffer(dvb_player_t *player);
int ST_CloseDevice(dvb_player_t *player);
int ST_ChangeAudio(dvb_player_t *player, dvb_locator_t *locator);
int ST_IsFilePlayEOFReached(dvb_player_t *player);
long long ST_GetTimeStamp (dvb_player_t *player);
int ST_ChangeGlobalAudioMute(dvb_player_t *player);
int ST_AudioMute(dvb_player_t *player, int nOnOff);
int ST_ChangeDolbyMode(dvb_player_t *player, AVP_AudioOutputMode mode);
void ST_AspectRatioMode(dvb_player_t *player, AVP_AspectRatioMode mode);

int ST_GetClientEvent(dvb_player_t *player);
void ST_ReleasePlayer(dvb_player_t *player);

#ifdef __cplusplus
}
#endif

#endif //__iptvimplement_h__