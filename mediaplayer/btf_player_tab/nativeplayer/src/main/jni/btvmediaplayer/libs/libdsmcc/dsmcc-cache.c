// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include "dsmcc-cache.h"
#include "dsmcc-biop.h"
#include "dsmcc-receiver.h"
#include "dsmcc-descriptor.h"
#include "dsmcc.h"
#include <systemproperty.h>

/* TODO This should be stored in obj_carousel structure  */

void
dsmcc_cache_init(struct cache *filecache, const char *channel_name) {

	char dirbuf[256];

	/* TODO - load cache from disk into obj_carousel */

	filecache->gateway = filecache->dir_cache = NULL;
	filecache->file_cache = NULL; filecache->data_cache = NULL;

	filecache->num_files = filecache->num_dirs = filecache->total_files 
		= filecache->total_dirs = 0;

	filecache->files = NULL;

	/* Write contents under channel name */

	if(channel_name) {
		filecache->name = (char*)calloc(sizeof(unsigned char), strlen(channel_name)+1);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> cari->filecache->name len : %d", strlen(channel_name)+1);
#endif
		strcpy(filecache->name, channel_name);
	} else {
		filecache->name = (char*)calloc(sizeof(unsigned char), strlen("unknown")+1);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> cari->filecache->name len : %d", strlen("unknown")+1);
#endif
		strcpy(filecache->name, "unknown");
	}

	char dataPath[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
	sprintf(dirbuf, "%s%s/%s", dataPath, DSMCC_ROOTPATH, filecache->name);
	mkdir(dirbuf, 0777); 
	chmod(dirbuf, 0777);

	dsmcc_log("[dsmcc-cache] mkdir %s 0755\n", dirbuf);	
}

void
dsmcc_cache_free(struct cache *filecache) {
	struct cache_file *f, *fn;
	struct cache_dir *d, *dn;
	struct file_info *file, *nfile;

	/* Free unconnected files */
	f = filecache->file_cache;
	while(f!=NULL) {
		fn = f->next;
		if(f->key_len>0){
			free(f->key);
			f->key = NULL; f->key_len = 0;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> filecache->file_cache->key");
#endif
		}
		if(f->filename!=NULL){
			free(f->filename);
			f->filename = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> filecache->file_cache->filename");
#endif
		}
		if(f->data!=NULL){
			free(f->data);
			f->data = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> filecache->file_cache->data");
#endif
		}
		if(f->p_key_len>0){
			free(f->p_key);
			f->p_key = NULL; f->p_key_len = 0;			
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> filecache->file_cache->p_key");
#endif
		}
		free(f);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free   -> filecache->file_cache");
#endif
		f = fn;
	}

	/* Free cached data */
	f = filecache->data_cache;
	while(f!=NULL) {
		fn = f->next;
		if(f->key_len>0){
			free(f->key);
			f->key = NULL; f->key_len = 0;	
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> filecache->data_cache->key");
#endif
		}
		if(f->data!=NULL){
			free(f->data);
			f->data = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> filecache->data_cache->data");
#endif
		}
		free(f);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free   -> filecache->data_cache");
#endif
		f = fn;
	}

	/* Free unconnected dirs */
	d = filecache->dir_cache;
	while(d!=NULL) {
		dn = d->next;
		if(d->name!=NULL){
			free(d->name);
			d->name = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> filecache->dir_cache->name");
#endif
		}
		if(d->dirpath!=NULL){
			free(d->dirpath);
			d->dirpath = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> filecache->dir_cache->dirpath");
#endif
		}
		if(d->key_len>0){
			free(d->key);
			d->key = NULL; d->key_len = 0;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> filecache->dir_cache->key");
#endif
		}
		if(d->p_key_len>0){
			free(d->p_key);
			d->p_key = NULL; d->p_key_len = 0;			
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> filecache->dir_cache->p_key");
#endif
		}
		f = d->files;
		while(f!=NULL) {
			fn = f->next;
			if(f->key_len>0){
				free(f->key);
				f->key = NULL; f->key_len = 0;				
#ifdef DSMCC_MALLOC_PRINT_DEBUG
				dsmcc_log("[MEMINFO] free   -> filecache->dir_cache->files->key");
#endif
			}
			if(f->filename!=NULL){
				free(f->filename);
				f->filename = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
				dsmcc_log("[MEMINFO] free   -> filecache->dir_cache->files->filename");
#endif
			}
			if(f->data!=NULL) {
				free(f->data);
				f->data = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
				dsmcc_log("[MEMINFO] free   -> filecache->dir_cache->files->data");
#endif
			}
			if(f->p_key_len>0){
				free(f->p_key);
				f->p_key = NULL; f->p_key_len = 0;		
#ifdef DSMCC_MALLOC_PRINT_DEBUG
				dsmcc_log("[MEMINFO] free   -> filecache->dir_cache->files->p_key");
#endif
			}
			free(f);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> filecache->dir_cache->files");
#endif
			f = fn;
		}
		free(d);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free   -> filecache->dir_cache");
#endif
		d = dn;
	}

	/* Free gateway */
	if(filecache->gateway != NULL)
		dsmcc_cache_free_dir(filecache->gateway);

	/* Free file infos */
	file = filecache->files;
	while(file!=NULL) {
		nfile = file->next;
		if(file->filename){
			free(file->filename);
			file->filename = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> filecache->files->filename");
#endif
		}
		if(file->path){
			free(file->path);
			file->path = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> filecache->files->path");
#endif
		}
		free(file);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free   -> filecache->files");
#endif
		file = nfile;
	}

	filecache->file_cache = filecache->data_cache = NULL;
	filecache->gateway = filecache->dir_cache = NULL;
	filecache->files = NULL;

	filecache->num_files = filecache->num_dirs = filecache->total_files 
		= filecache->total_dirs = 0;

	if(filecache->name){
		free(filecache->name);
		filecache->name = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free   -> filecache->name");
#endif
	}
}

void
dsmcc_cache_free_dir(struct cache_dir *d) {
	struct cache_dir *dn, *dnn;
	struct cache_file *f, *fn;

	if(d->sub!=NULL) {
		dn = d->sub;
		while(dn!=NULL) {
			dnn = dn->next;
			dsmcc_cache_free_dir(dn);
			dn = dnn;
		}
	}

	if(d->name!=NULL){
		free(d->name);
		d->name = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free   -> cache_dir->name");
#endif
	}
	if(d->dirpath!=NULL){
		free(d->dirpath);
		d->dirpath = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free   -> cache_dir->dirpath");
#endif
	}
	if(d->key_len>0){
		free(d->key);
		d->key = NULL; d->key_len = 0;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free   -> cache_dir->key");
#endif
	}
	if(d->p_key_len>0){
		free(d->p_key);
		d->p_key = NULL; d->p_key_len = 0;		
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free   -> cache_dir->p_key");
#endif
	}
	f = d->files;
	while(f!=NULL) {
		fn = f->next;
		if(f->key_len>0){
			free(f->key);
			f->key = NULL; f->key_len = 0;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> cache_file->key");
#endif
		}
		if(f->filename!=NULL){
			free(f->filename);
			f->filename = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> cache_file->filename");
#endif
		}
		if(f->data!=NULL){
			free(f->data);
			f->data = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> cache_file->data");
#endif
		}
		if(f->p_key_len>0){
			free(f->p_key);
			f->p_key = NULL; f->p_key_len = 0;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> cache_file->p_key");
#endif
		}
		free(f);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free   -> cache_file");
#endif
		f = fn;
	}
	free(d);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
	dsmcc_log("[MEMINFO] free   -> cache_dir");
#endif

}

void
dsmcc_cache_module_data_free(struct cache_module_data *cache) {
	struct descriptor *desc, *last;
	struct dsmcc_ddb *dbb, *ndbb;

	if(cache->data) { 
		free(cache->data);
		cache->data = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free   -> cache_module_data->data");
#endif
	}
	
	if(cache->bstatus) {
		free(cache->bstatus);
		cache->bstatus = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free   -> cache_module_data->bstatus");
#endif
	}

	if(cache->blocks) {
		dbb = cache->blocks;
		while(dbb != NULL) {
			ndbb = dbb->next;
			if(dbb->blockdata){
				free(dbb->blockdata);
				dbb->blockdata = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
				dsmcc_log("[MEMINFO] free   -> cache_module_data->blocks->blockdata");
#endif
			}
			free(dbb);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> cache_module_data->blocks");
#endif
			dbb = ndbb;
		}
	}
	
	if(cache->descriptors) {
		desc = cache->descriptors;
		while(desc != NULL) {
			last = desc->next;
			dsmcc_desc_free(desc);
			desc = last;
		}
	}
	free(cache);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
	dsmcc_log("[MEMINFO] free   -> cache_module_data");
#endif
}


unsigned int
dsmcc_cache_key_cmp(char *str1, char *str2, unsigned int len1, unsigned int len2) {
	unsigned int i;

	/* Key Len must be equal */
	if(len1 != len2) return 0;
	
	for(i = 0; i < len1; i++) {
		if(str1[i] != str2[i]) {
			return 0;
		}
	}
	return 1;
}

struct cache_dir *
dsmcc_cache_scan_dir(struct cache_dir *dir, unsigned long car_id, unsigned short module_id, unsigned int key_len, char *key) {
	struct cache_dir *founddir, *subdir;

	if(dir == NULL) return NULL;

	if((dir->carousel_id == car_id) && (dir->module_id == module_id) &&
			dsmcc_cache_key_cmp(dir->key, key, dir->key_len, key_len)) 
	{
		return dir;
	}

	/* Search sub dirs */

	for(subdir = dir->sub; subdir != NULL; subdir=subdir->next) {
		founddir = dsmcc_cache_scan_dir(subdir, car_id, module_id, key_len,key);
		if(founddir != NULL) return founddir;
	}
	return NULL;
}

struct cache_dir *
dsmcc_cache_dir_find(struct cache *filecache, unsigned long car_id, unsigned short module_id, unsigned int key_len, char *key) {
	struct cache_dir *dir, *fdir;
	struct cache_file *file, *nf;
	
	dsmcc_log("[dsmcc-cache] Searching for dir %d/%d/(key)\n", module_id, key_len);

	/* Scan through known dirs and return details if known else NULL */

	if(module_id == 0 && key_len == 0) {
		/* Return gateway object. Create if not already */
		if(filecache->gateway == NULL) {
			dsmcc_log("[dsmcc-cache] filecache->gateway is NULL \n");
			filecache->gateway = (struct cache_dir *)calloc(sizeof(unsigned char), sizeof(struct cache_dir));
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] calloc -> filecache->gateway len : %d", sizeof(struct cache_dir));
#endif
			filecache->gateway->name = (char *)calloc(sizeof(unsigned char), 2);
			filecache->gateway->carousel_id = car_id;
			filecache->gateway->module_id = filecache->gateway->key_len = 
			filecache->gateway->p_key_len = 0;

			/*TODO argg! a hack to fix a bug caused by a hack.Need better linking */
			strcpy(filecache->gateway->name, "/");
			filecache->gateway->dirpath = (char *)calloc(sizeof(unsigned char), 2);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] calloc -> filecache->gateway->dirpath len : %d", 2);
#endif

			strcpy(filecache->gateway->dirpath, "/");
			filecache->gateway->sub = filecache->gateway->parent = NULL;
			filecache->gateway->prev = filecache->gateway->next = NULL;
			filecache->gateway->files = NULL;

			/* Attach any subdirs or files that arrived prev. */
			for(file=filecache->file_cache;	file!=NULL; file=nf) {
				nf=file->next;
				if((file->carousel_id == filecache->gateway->carousel_id) &&
 					(file->p_module_id == filecache->gateway->module_id) &&
 					dsmcc_cache_key_cmp(file->p_key, filecache->gateway->key,
					file->p_key_len,filecache->gateway->key_len)) 
				{
					dsmcc_cache_attach_file(filecache,filecache->gateway, file);
				}
			}

			for(fdir=filecache->dir_cache; fdir!=NULL; fdir=fdir->next) 
				dsmcc_cache_attach_dir(filecache, filecache->gateway, fdir);
		
			dsmcc_cache_write_dir(filecache, filecache->gateway);	
			/* Write files to filesystem */

			return filecache->gateway;
		} else {
			dsmcc_log("[dsmcc-cache] filecache->gateway is exist \n");
			return filecache->gateway;
		}
	}


	/* Find dir magic */
	dir = dsmcc_cache_scan_dir(filecache->gateway, car_id, module_id, key_len, key);


	if(dir == NULL) {	/* Try looking in unlinked dirs list */
		for(fdir=filecache->dir_cache;(dir==NULL)&&(fdir!=NULL); fdir = fdir->next) {
			dir = dsmcc_cache_scan_dir(fdir, car_id, module_id, key_len, key);
		}
	}

	/* TODO - Directory not known yet, cache it ? */
	return dir;
}

void
dsmcc_cache_attach_file(struct cache *filecache, struct cache_dir *root, struct cache_file *file) {
	struct cache_file *cfile;

	/* Search for any files that arrived previously in unknown files list*/
	if(root->files == NULL) {
		if(file->prev!=NULL) {
			file->prev->next = file->next;
			dsmcc_log("[dsmcc-cache] Set filecache prev to next file\n");
		} else {
			filecache->file_cache=file->next;
			dsmcc_log("[dsmcc-cache] Set filecache to next file\n");
		}

		if(file->next!=NULL)
			file->next->prev = file->prev; 

		root->files = file;
		root->files->next = root->files->prev = NULL;
		file->parent = root;
	} else {

		if(file->prev!=NULL) {
			file->prev->next = file->next;
			dsmcc_log("[dsmcc-cache] Set filecache (not start) prev to next file\n");
		} else {
			filecache->file_cache=file->next;
			dsmcc_log("[dsmcc-cache] Set filecache (not start) to next file\n");			
		}

		if(file->next!=NULL)
			file->next->prev = file->prev;

		for(cfile=root->files;cfile->next!=NULL;cfile=cfile->next)
		{
			;
		}

		cfile->next = file;
		file->prev = cfile;
		file->next = NULL;	/* TODO uurrgh */
		file->parent = root;
	}
}

void
dsmcc_cache_attach_dir(struct cache *filecache, struct cache_dir *root, struct cache_dir *dir) {
	struct cache_dir *last;
	
	if((dir->carousel_id == root->carousel_id) &&
		(dir->p_module_id == root->module_id) &&
		dsmcc_cache_key_cmp(dir->p_key,root->key,dir->p_key_len,root->key_len)) {

		if(root->sub == NULL) {
			if(dir->prev != NULL) { 
				dir->prev->next = dir->next; 
			} else {
				filecache->dir_cache = dir->next;
			}

			if(dir->next!=NULL) 
				dir->next->prev = dir->prev;

			root->sub = dir;
			root->sub->next = root->sub->prev = NULL;
			dir->parent = root;
		} else {
			if(dir->prev!=NULL) {
				dir->prev->next = dir->next;
			} else {
				filecache->dir_cache = dir->next;
			}

			if(dir->next!=NULL)
				dir->next->prev = dir->prev;

			for(last=root->sub;last->next!=NULL;last=last->next) 
			{ 
				; 
			}
			last->next = dir;
			dir->prev = last;
			dir->next = NULL;
			dir->parent = root;
		}
	}
}

struct cache_file *
dsmcc_cache_scan_file(struct cache_dir *dir, unsigned long car_id, unsigned int mod_id, unsigned int key_len, char *key) {
	struct cache_file *file;
	struct cache_dir *subdir;

	if(dir == NULL) { return NULL; }
	
	char logging[1024] = {0,};
	sprintf(logging, "[dsmcc-cache] Searching for file (%d) - ", mod_id);
	for(unsigned int i = 0; i < key_len; i++) {
		sprintf(logging+strlen(logging), "%02X", key[i]);
	}
	sprintf(logging+strlen(logging), "\n");
	dsmcc_log(logging);

	/* Search files in this dir */

	for(file = dir->files; file != NULL; file = file->next) {
		if((file->carousel_id == car_id) &&
			(file->module_id == mod_id) &&
			dsmcc_cache_key_cmp(file->key, key, file->key_len, key_len)) 
		{
			return file;
		}
	}

	/* Search sub dirs */

	for(subdir = dir->sub; subdir != NULL; subdir=subdir->next) {
		file = dsmcc_cache_scan_file(subdir, car_id, mod_id, key_len, key);
		if(file != NULL) {
			return file;
		}
	}

	return NULL;
}

struct cache_file *
dsmcc_cache_file_find(struct cache *filecache, unsigned long car_id, unsigned short module_id, unsigned int key_len, char *key)
{
	struct cache_file *file;

	/* Try looking in parent-less list */
	for(file = filecache->file_cache; file != NULL; file = file->next) {
		if((file->carousel_id == car_id) &&
			(file->module_id == module_id) &&
			dsmcc_cache_key_cmp(file->key, key, file->key_len, key_len)) 
		{
			return file;
		}
	}

	/* Scan through known files and return details if known else NULL */
	file = dsmcc_cache_scan_file(filecache->gateway, car_id, module_id, key_len, key);
	return file;
}

void
dsmcc_cache_dir_info(struct cache *filecache, unsigned short module_id, unsigned int objkey_len, char *objkey, struct biop_binding *bind) {
	struct cache_dir *dir, *last, *subdir;
	struct cache_file *file, *nf;

	dir = dsmcc_cache_dir_find(filecache,
			bind->ior.body.full.obj_loc.carousel_id,
			bind->ior.body.full.obj_loc.module_id,
			bind->ior.body.full.obj_loc.objkey_len,
			bind->ior.body.full.obj_loc.objkey);

	if(dir != NULL) return; /* Already got (check version TODO) */

	dir = (struct cache_dir *)calloc(sizeof(unsigned char), sizeof(struct cache_dir));
#ifdef DSMCC_MALLOC_PRINT_DEBUG
	dsmcc_log("[MEMINFO] calloc -> dir len : %d", sizeof(struct cache_dir));
#endif
	dir->name = (char *)calloc(sizeof(unsigned char), bind->name.comps[0].id_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
	dsmcc_log("[MEMINFO] calloc -> dir->name len : %d", bind->name.comps[0].id_len);
#endif

	memcpy(dir->name, bind->name.comps[0].id, bind->name.comps[0].id_len);

	dir->dirpath = NULL;
	dir->next = dir->prev = dir->sub = NULL;
	dir->files = NULL;
	dir->carousel_id = bind->ior.body.full.obj_loc.carousel_id;
	dir->module_id = bind->ior.body.full.obj_loc.module_id;
	dir->key_len = bind->ior.body.full.obj_loc.objkey_len;
	if(dir->key_len > 0){
		dir->key = (char *)calloc(sizeof(unsigned char), dir->key_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> dir->key len : %d", dir->key_len);
#endif
		memcpy(dir->key, bind->ior.body.full.obj_loc.objkey, dir->key_len);
	}
	
//	dir->p_carousel_id = carousel_id; Must be the same ?

	dir->p_module_id = module_id;
	dir->p_key_len = objkey_len;
	if(dir->p_key_len > 0){
		dir->p_key = (char *)calloc(sizeof(unsigned char), dir->p_key_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> dir->p_key len : %d", dir->p_key_len);
#endif
		memcpy(dir->p_key, objkey, dir->p_key_len);
	}

	dir->parent = dsmcc_cache_dir_find(filecache, dir->carousel_id, module_id, objkey_len, objkey);
	if(dir->p_key){
		dsmcc_log("[dsmcc-cache] Caching dir %s (with parent %d/%d/%02X%02X%02X)\n", dir->name, dir->p_module_id, dir->p_key_len, dir->p_key[0], dir->p_key[1], dir->p_key[2]);
	}else{
		dsmcc_log("[dsmcc-cache] Caching dir %s (with parent %d/%d)\n", dir->name, dir->p_module_id, dir->p_key_len);	
	}

	if(dir->parent == NULL) {
		if(filecache->dir_cache == NULL) {
			filecache->dir_cache = dir;
		} else {
			/* Directory not yet known. Add this to dirs list */
			for(last=filecache->dir_cache;last->next!=NULL;last=last->next){;}
			dsmcc_log("Added to Unknown list not empty\n");
			last->next = dir;
			dir->prev = last;
		}
	} else {
		dsmcc_log("[dsmcc-cache] Caching dir %s under parent %s\n", dir->name, dir->parent->name);
		/* Create under parent directory */
		if(dir->parent->sub == NULL) {
			dsmcc_log("Parent has no subdirs\n");
			dir->parent->sub = dir;
		} else {
			dsmcc_log("Parent has other subdirs\n");
			for(last=dir->parent->sub;last->next!=NULL;last=last->next) {;}
			last->next = dir;
			dir->prev = last;
			dsmcc_log("Added to Parent has other subdirs\n");
		}
	}

	/* Attach any files that arrived previously */

	for(file = filecache->file_cache; file != NULL; file = nf) {
		nf = file->next;
		if((file->carousel_id == dir->carousel_id) &&
 			(file->p_module_id == dir->module_id) &&
 			dsmcc_cache_key_cmp(file->p_key, dir->key, file->p_key_len, dir->key_len)) 
		{
			dsmcc_log("[dsmcc-cache] Attaching previously arrived file %s to newly created directory %s\n", file->filename, dir->name);
			dsmcc_cache_attach_file(filecache, dir, file);
		}
	}

	/* Attach any subdirs that arrived beforehand */
	for(subdir = filecache->dir_cache; subdir != NULL; subdir = subdir->next) {
		dsmcc_cache_attach_dir(filecache, dir, subdir);
	}
		
	if((dir->parent!=NULL)&&(dir->parent->dirpath!=NULL))
		dsmcc_cache_write_dir(filecache, dir);	/* Write dir/files to filesystem */

	filecache->num_dirs++; filecache->total_dirs++;
}

void
dsmcc_cache_write_dir(struct cache *filecache, struct cache_dir *dir) {
	struct cache_dir *subdir;
	struct cache_file *file;
	char dirbuf[256];

	if(dir->dirpath == NULL)  {
		dir->dirpath= (char*)calloc(sizeof(unsigned char), strlen(dir->parent->dirpath)+strlen(dir->name)+2);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> dir->dirpath len : %d", strlen(dir->parent->dirpath)+strlen(dir->name)+2);
#endif
		strcpy(dir->dirpath, dir->parent->dirpath);
		strcat(dir->dirpath, "/");
		strcat(dir->dirpath, dir->name);
	}
	
	char dataPath[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
	sprintf(dirbuf, "%s%s/%s", dataPath, DSMCC_ROOTPATH, filecache->name);
	sprintf(dirbuf, "%s/%s", dirbuf, dir->dirpath);
	dsmcc_log("[dsmcc-cache] Writing directory %s to filesystem\n", dirbuf);

	mkdir(dirbuf, 0777); 
	chmod(dirbuf, 0777);

	/* Write out files that had arrived before directory */

	for(file=dir->files;file!=NULL;file=file->next) {
		if(file->data != NULL) {
			dsmcc_log("[dsmcc-cache] Writing out file %s under new dir %s\n", file->filename, dir->dirpath);
			dsmcc_cache_write_file(filecache, file);
		}
	}

	/* Recurse thorugh child directories */

	for(subdir=dir->sub;subdir!=NULL;subdir=subdir->next) {
		dsmcc_cache_write_dir(filecache, subdir);
	}
}
	
void
dsmcc_cache_file(struct cache *filecache, struct biop_message *bm, struct cache_module_data *cachep) {
	struct cache_file *file;


	/* search for file info */

	file = dsmcc_cache_file_find(filecache, cachep->carousel_id, cachep->module_id, bm->hdr.objkey_len, bm->hdr.objkey);
	if(file == NULL) {

		dsmcc_log("[dsmcc-cache] Unknown file 0x%04X/%d/%d/%02X%02X%02X, caching data\n", cachep->carousel_id, cachep->module_id, bm->hdr.objkey_len, bm->hdr.objkey[0], bm->hdr.objkey[1], bm->hdr.objkey[2]);

		/* Not known yet. Save data */
		file = (struct cache_file *)calloc(sizeof(unsigned char), sizeof(struct cache_file));
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> file len : %d", sizeof(struct cache_file));
#endif
		file->data_len = bm->body.file.content_len;
		file->data = (char*)calloc(sizeof(unsigned char), file->data_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> file->data len : %d", file->data_len);
#endif
		memcpy(file->data, cachep->data+cachep->curp, file->data_len);
		file->carousel_id= cachep->carousel_id;
		file->module_id= cachep->module_id;
		file->key_len= bm->hdr.objkey_len;
		file->key= (char*)calloc(sizeof(unsigned char), file->key_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> file->key len : %d", file->key_len);
#endif
		memcpy(file->key, bm->hdr.objkey, file->key_len);
		file->next = file->prev = NULL;
		// Add to unknown data cache

		if(filecache->data_cache == NULL) {
			filecache->data_cache = file;
		} else {
			struct cache_file *last;
			for(last=filecache->data_cache;last->next!=NULL;last=last->next){;}
			last->next = file;
			file->prev = last;
		}

		filecache->num_files++; filecache->total_files++;

	} else {
		/* Save data. Save file if wanted  (TODO check versions ) */

		dsmcc_log("[dsmcc-cache] Data for file %s\n", file->filename);

		if(file->data == NULL) {
			file->data_len = bm->body.file.content_len;
			file->data = (char *)calloc(sizeof(unsigned char), file->data_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] calloc -> file->data len : %d", file->data_len);
#endif
			memcpy(file->data,cachep->data+cachep->curp, file->data_len);
			/* TODO this should be a config option */
			dsmcc_cache_write_file(filecache, file);
		} else {
			dsmcc_log("[dsmcc-cache] Data for file %s had already arrived\n", file->filename);
		}
	}
}

void
dsmcc_cache_write_file(struct cache *filecache, struct cache_file *file) {
	FILE *data_fd;
	char buf[128];
	struct file_info *filei, *files;

	/* TODO create directory structure rather than one big mess! */
	if((file->parent!=NULL) && (file->parent->dirpath != NULL)) {

		dsmcc_log("[dsmcc-cache] Writing file %s/%s (%d bytes)\n", file->parent->dirpath, file->filename, file->data_len);
		char dataPath[128];
		get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
		sprintf(buf, "%s%s/%s", dataPath, DSMCC_ROOTPATH, filecache->name);
		sprintf(buf,"%s/%s/%s", buf, file->parent->dirpath, file->filename);
		dsmcc_log("[dsmcc-cache] Writing file %s\n", buf);

		data_fd = fopen(buf, "wb");
		if(data_fd != NULL) {
			char c;
			fwrite(file->data, 1, file->data_len, data_fd);
			c = 0x0D; /* CR */
			fwrite(&c, 1, sizeof(char), data_fd); 
			c = 0x0A; /* LF */
			fwrite(&c, 1, sizeof(char), data_fd); 			
			fclose(data_fd);
			chmod(buf, 0777);
		} else {
			dsmcc_log("[dsmcc-cache] Writing file => Failed to open file (%s)\n", buf);
		}
		/* Free data as no longer needed */
		free(file->data);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free   -> file->data");
#endif
		file->data = NULL; file->data_len = 0;

		filecache->num_files--;

		/* Update information in file info */
		filei = (struct file_info *)calloc(sizeof(unsigned char), sizeof(struct file_info));
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> filei len : %d", sizeof(struct file_info));
#endif
		filei->filename = (char *)calloc(sizeof(unsigned char), strlen(file->filename)+1);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> filei->filename  len : %d", strlen(file->filename)+1);
#endif
		strcpy(filei->filename, file->filename);
		filei->path = (char *)calloc(sizeof(unsigned char), strlen(buf)+1);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> filei->path	len : %d", strlen(buf)+1);
#endif
		strcpy(filei->path, buf);
		filei->arrived = filei->written = 1;
		if(filecache->files == NULL) {
			filecache->files = filei;
		} else {
			for(files=filecache->files;files->next!=NULL;files=files->next){;}
			files->next = filei;
		}
		filei->next = NULL;
	} else {
		dsmcc_log("[dsmcc-cache] File %s Parent == %p Dirpath == %s\n", file->filename, file->parent, file->parent->dirpath);
	}
}

void
dsmcc_cache_unknown_dir_info(struct cache *filecache, struct cache_dir *newdir) {
	struct cache_dir *last;

	if(filecache->dir_cache == NULL) {
		filecache->dir_cache = newdir;
		newdir->next = newdir->prev = NULL;
	} else {
		for(last=filecache->dir_cache;last->next!=NULL;last=last->next) { ; }
		last->next = newdir;
		newdir->prev = last;
		newdir->next = NULL;
	}
}

void
dsmcc_cache_unknown_file_info(struct cache *filecache, struct cache_file *newfile) {
	struct cache_file *last;

/* TODO Check if already unknown file (i.e. data arrived twice before
 * dir/srg or missed dir/srg message, if so skip.
 */

	if(filecache->file_cache == NULL) {
		filecache->file_cache = newfile;
		filecache->file_cache->next = filecache->file_cache->prev = NULL;
	} else {
		for(last=filecache->file_cache;last->next!=NULL; last = last->next) { ; }
		last->next = newfile;
		newfile->prev = last;
		newfile->next = NULL;
	}
}

struct cache_file *
dsmcc_cache_file_find_data(struct cache *filecache, unsigned long car_id, unsigned short mod_id, unsigned int key_len, char *key) {

	struct cache_file *last;

	for(last=filecache->data_cache; last!=NULL; last = last->next) {
		if((last->carousel_id==car_id) && (last->module_id==mod_id)
			&& dsmcc_cache_key_cmp(key, last->key, key_len, last->key_len)) 
		{
			if(last->prev != NULL) {
				last->prev->next = last->next;
			} else {
				filecache->data_cache = last->next;
			}

			if(last->next != NULL)
				last->next->prev = last->prev;

			break;
		}
	}
	return last;
}

void
dsmcc_cache_file_info(struct cache *filecache, unsigned short mod_id, unsigned int key_len, char *key, struct biop_binding *bind) {
	struct cache_file *newfile, *last;
	struct cache_dir *dir;

	dsmcc_log("[dsmcc-cache] Caching file info\n");

	// Check we do not already have file (or file info) cached 
	if(dsmcc_cache_file_find(filecache,
		bind->ior.body.full.obj_loc.carousel_id,
		bind->ior.body.full.obj_loc.module_id,
		bind->ior.body.full.obj_loc.objkey_len,
		bind->ior.body.full.obj_loc.objkey) != NULL) 
	{
		return;
	}

	// See if the data had already arrived for the file 
	newfile = dsmcc_cache_file_find_data(filecache,
		bind->ior.body.full.obj_loc.carousel_id,
		bind->ior.body.full.obj_loc.module_id,
		bind->ior.body.full.obj_loc.objkey_len,
		bind->ior.body.full.obj_loc.objkey);
	
	if(newfile == NULL) {
		dsmcc_log("[dsmcc-cache] Data not arrived for file %s, caching\n", bind->name.comps[0].id);
		// Create the file from scratch
		newfile = (struct cache_file*)calloc(sizeof(unsigned char), sizeof(struct cache_file));
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> newfile	len : %d", sizeof(struct cache_file));
#endif
		newfile->carousel_id = bind->ior.body.full.obj_loc.carousel_id;
		newfile->module_id = bind->ior.body.full.obj_loc.module_id;
		newfile->key_len = bind->ior.body.full.obj_loc.objkey_len;
		if(newfile->key_len > 0){
			newfile->key= (char *)calloc(sizeof(unsigned char), newfile->key_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] calloc -> newfile->key	len : %d", newfile->key_len);
#endif
			memcpy(newfile->key, bind->ior.body.full.obj_loc.objkey, newfile->key_len);
		}
		newfile->data = NULL;
	} else {
		dsmcc_log("[dsmcc-cache] Data already arrived for file %s\n", bind->name.comps[0].id);
	}

	newfile->filename = (char*)calloc(sizeof(unsigned char), bind->name.comps[0].id_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
	dsmcc_log("[MEMINFO] calloc -> newfile->filename len : %d", bind->name.comps[0].id_len);
#endif
	memcpy(newfile->filename, bind->name.comps[0].id, bind->name.comps[0].id_len);
	newfile->next = NULL;

	dir = dsmcc_cache_dir_find(filecache, newfile->carousel_id, mod_id, key_len, key);

	filecache->num_files++; filecache->total_files++;

	if(dir == NULL) {
		/* Parent directory not yet known */
		newfile->p_module_id = mod_id;
		newfile->p_key_len = key_len;
		if(newfile->p_key_len > 0){
			newfile->p_key = (char *)calloc(sizeof(unsigned char), newfile->p_key_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] calloc -> newfile->p_key len : %d", newfile->p_key_len);
#endif
			memcpy(newfile->p_key, key, newfile->p_key_len);
		}
		newfile->parent = NULL;
		if(newfile->key){
			dsmcc_log("[dsmcc-cache] Caching info for file %s with unknown parent dir (file info - 0x%04X/%d/%d/%02X%02X%02X)\n", 
				newfile->filename, newfile->carousel_id, newfile->module_id, newfile->key_len, newfile->key[0], newfile->key[1], newfile->key[2]);
		}else{
			dsmcc_log("[dsmcc-cache] Caching info for file %s with unknown parent dir (file info - 0x%04X/%d/%d)\n", 
				newfile->filename, newfile->carousel_id, newfile->module_id, newfile->key_len);
		}
		dsmcc_cache_unknown_file_info(filecache, newfile);
	} else {
		/* TODO Check if already stored under directory (new version?)
		 *      Checking version info for a file is difficult,
		 *      the data should not be passed to us by dsmcc layer 
		 *      unless the version has changed. Need to remove old
		 *      and store new.
		*/
		/* If not append to list */

		newfile->p_key_len = dir->key_len;
		if(newfile->p_key_len > 0){
			newfile->p_key = (char *)calloc(sizeof(unsigned char), dir->key_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] calloc -> newfile->p_key len : %d", newfile->p_key_len);
#endif
			memcpy(newfile->p_key, dir->key, dir->key_len);
		}
		newfile->parent = dir;
		if(dir->files == NULL) {
			dir->files = newfile;
			newfile->prev = NULL;
		} else {
			for(last=dir->files;last->next!=NULL;last=last->next){;}
			last->next = newfile;
			newfile->prev = last;
		}
		if(newfile->key){
			dsmcc_log("[dsmcc-cache] Caching info for file %s with known parent dir (file info - 0x%04X/%d/%d/%02X%02X%02X%02X)\n", 
				newfile->filename, newfile->carousel_id, newfile->module_id, newfile->key_len, newfile->key[0], newfile->key[1], newfile->key[2]);
		}else{
			dsmcc_log("[dsmcc-cache] Caching info for file %s with known parent dir (file info - 0x%04X/%d/%d)\n", 
				newfile->filename, newfile->carousel_id, newfile->module_id, newfile->key_len);
		}

		if(newfile->data != NULL)
			dsmcc_cache_write_file(filecache, newfile);
	}
}
