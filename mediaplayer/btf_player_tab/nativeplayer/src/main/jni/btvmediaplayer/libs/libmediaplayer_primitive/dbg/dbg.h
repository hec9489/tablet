// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef __dbg_h__
#define __dbg_h__

#define LOG_TAG     "BOXKEY-STDVB"

#include <android/log.h>

#include <stdio.h>
#include <errno.h> 
#include <string.h>
#include <stdbool.h>

#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define  LOGF(...)  __android_log_print(ANDROID_FATAL_ERROR,LOG_TAG,__VA_ARGS__)
#define  LOGS(...)  __android_log_print(ANDROID_SILENT_ERROR,LOG_TAG,__VA_ARGS__)
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)

#define log_err      LOGI
#define log_dry_err  LOGE
#define log_warn    LOGW(
#define log_info      LOGI
#define debug         LOGD
#define verbose       LOGD

#define logLogCatKey_stdvb LOGD


#define check(A, M, ...)        if(!(A)) { log_err(M, ##__VA_ARGS__); errno=0; goto error; } 
#define dry_check(A, M, ...)    if(!(A)) { log_dry_err(M, ##__VA_ARGS__); errno=0; } 
#define check_fd(A, M, ...)     if((A)<0) { log_err(M, ##__VA_ARGS__); errno=0; goto error; } 
#define sentinel(M, ...)        { log_err(M, ##__VA_ARGS__); errno=0; goto error; } 
#define check_mem(A)            check((A), "Out of memory.")
#define check_debug(A, M, ...)  if(!(A)) { debug(M, ##__VA_ARGS__); errno=0; goto error; } 

#endif // __dbg_h__
