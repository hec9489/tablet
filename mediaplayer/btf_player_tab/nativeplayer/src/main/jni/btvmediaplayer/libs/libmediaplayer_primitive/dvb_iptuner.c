// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <sys/poll.h>

#include <stdio.h>
#include <pthread.h>

#include <sys/socket.h>
#include <arpa/inet.h>

#include <utils/RtpInfoDefine.h>
#include <rtp.h>

#include <pt_mlr_client.h>
#include <pt_ads.h>

#ifdef USE_QI_CALCULATOR
#include <hal/qi_calculator.h>
#endif

#include "dvb_primitive.h"
#include <dbg.h>

#include "multiview/multiViewRtp.h"
#ifdef USE_MLR
#include <systemproperty.h>
#include <dirent.h>
#endif

extern RtpInfo_t *gRtpInfo[];

// TODO : jung2604
extern void *iptv_media_player_feed_thread(void *ThreadParam);
extern void *iptv_media_player_recv_thread(void *ThreadParam);

void multiview_unlock(dvb_player_t *player);

int processMlr(dvb_player_t *player, char *buf, int *pLengthData);

int processAds(dvb_player_t *player, char *buf, int *pLengthData);

#ifdef USE_MLR
int first_start = 0;
#define MLR_SET_FILE "btf/btvmedia/mlr_client/mlrc.conf"
#define MLR_STORAGE_NAME "btf/btvmedia/mlr_client"
void set_first_start(int set)
{
  first_start = set;
}

int get_first_start(void)
{
  return first_start;
}

int mlr_conf_file_operation(int set, char *value)
{
	FILE *pfile = NULL;
	int result, val;
	int ret = 0;
	char read_buf[1];
    char mlr_value[1];
	char mlr_configdir[256];
	char mlr_configfile[128];

    logLogCatKey_stdvb("%s  %s",__FUNCTION__, (set==1)?"set":"get");

    get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, mlr_configdir, 128);
    sprintf(mlr_configfile, "%s/%s", mlr_configdir, MLR_SET_FILE);

    if(value != NULL)
    {
      if(memcmp(value, "t", 1)==0)
      {
        mlr_value[0] = '1';
      }
      else
      {
        mlr_value[0] = '0';
      }
      logLogCatKey_stdvb("mlr file %s  value %d->%d",mlr_configfile, mlr_value[0],atoi(mlr_value));
    }

    if(set == 1) //set
    {
      logLogCatKey_stdvb("mlr set : %d",mlr_value[0]);
      pfile = fopen( mlr_configfile, "r+");
      fseek(pfile, 1, SEEK_SET);
      fwrite(mlr_value, 1, sizeof(char), pfile);
      fclose(pfile);

      //verify
      //pfile = fopen(mlr_configfile, "r");
      //fseek(pfile, 1, SEEK_SET);
      //result = fread(read_buf, 1, sizeof(char), pfile);
      //logLogCatKey_stdvb("mlr set & read mlr config file : %d %d",atoi(read_buf), result);
      //fclose(pfile);
    }
    else //get
    {
      pfile = fopen(mlr_configfile, "r");
      if(pfile == NULL) //only 1st time
      {
        logLogCatKey_stdvb("Not found mlr conf file.. make file");
        pfile = fopen(mlr_configfile, "r+");
        fseek(pfile, 1, SEEK_SET);
        fwrite(mlr_value, 1, sizeof(char), pfile);
        fclose(pfile);

        //1st time verify
        pfile = fopen(mlr_configfile, "r");
        fseek(pfile, 1, SEEK_SET);
        result = fread(read_buf, 1, sizeof(char),pfile);
        val = atoi(read_buf);
        logLogCatKey_stdvb("1st make mlr file & read check: %d %d",val, result);
        fclose(pfile);
      }
      else
      {
        fseek(pfile, 1, SEEK_SET);
        result = fread(read_buf, 1, sizeof(char), pfile);
        val = atoi(read_buf);
        logLogCatKey_stdvb("Found mlr conf file read %d : %d",val,result);
        fclose(pfile);
      }
      ret = val;
	}
    return ret;
}

int MLR_set_path_and_Mac(void)
{
  int ret = 0, i = 0;
  DIR* dir = NULL;
  FILE *pf = NULL;
  char mlr_dirbuf[256];
  char mlr_dataPath[128];
  char mlr_configfile[128];
  char mac_buf[18];
  char mlr_Macbuf[6];
  char *token;
  char *buffer;
  char *next_ptr=NULL;
  char *sep = ":";
  char mlr_set[20];
  bool mlr_on;
  bool mlr_property;

  logLogCatKey_stdvb("%s ",__FUNCTION__);

  get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, mlr_dataPath, 128);
  sprintf(mlr_configfile, "%s/%s", mlr_dataPath, MLR_SET_FILE);
  
  get_systemproperty(PROPERTY__MLR_ACTIVATION_ENABLE, mlr_set, 20);
  mlr_property = (bool)(strcmp(mlr_set, "true") == 0);
  pf = fopen(mlr_configfile, "r");
  if(pf == NULL)
  {
    logLogCatKey_stdvb("No mlr file.. 1st time??");
    fclose(pf);
    mlr_on = mlr_conf_file_operation(0, mlr_set);
  }
  else
  {
    fclose(pf);
    mlr_on = mlr_conf_file_operation(0, NULL);
  }
  if((mlr_on == 0) && (mlr_property == 1))
  {
    logLogCatKey_stdvb("set mlr property false");
    set_systemproperty(PROPERTY__MLR_ACTIVATION_ENABLE, "false", 20);
  }
  logLogCatKey_stdvb("Set MLR ==> %s",(mlr_on==1)?"On":"Off");

  //if(mlr_on)
  //{
    sprintf(mlr_dirbuf, "%s/%s", mlr_dataPath, MLR_STORAGE_NAME);
  //  dir = opendir(mlr_dirbuf);
  //  if(dir == NULL)
  //  { 
  //    logLogCatKey_stdvb("%s folder not exist.. mkdir ");
  //    mkdir(mlr_dirbuf, 0700); 
  //    chmod(mlr_dirbuf, 0700);
  //  }
  //}
  //else
  //{
  //  sprintf(mlr_dirbuf, "%s", "NULL");
  //}

  if(get_systemproperty(PROPERTY__ETHERNET_MAC, mac_buf, 18) != 0 || strlen(mac_buf) == 0) 
  {
     logLogCatKey_stdvb("MAC from property NULL !!");
     memset(mlr_Macbuf, 0x11, 6); //dummy mac set
  }
  else
  {
    logLogCatKey_stdvb("MAC from property %s",mac_buf);

    buffer = (char*)&mac_buf[0];

    token = strtok_r(buffer, sep, &next_ptr); 
    mlr_Macbuf[0] = strtol(token, NULL, 16);
    logLogCatKey_stdvb("MLR MAC  %s %x",token, mlr_Macbuf[0]);

    for(i=1;i<6;i++)
    {
      token = strtok_r(NULL, sep, &next_ptr); 
      mlr_Macbuf[i] = strtol(token, NULL, 16);
      logLogCatKey_stdvb("MLR MAC  %s %x",token, mlr_Macbuf[i]);
    }
  }

  ret = PIX_MLR_AirSetup(mlr_dirbuf, mlr_Macbuf);
  logLogCatKey_stdvb("%s end %d",__FUNCTION__,ret);

  return ret;
}
#endif

int dvb_iptuner_unlock(dvb_player_t *player) {
    player->TerminateTuner =1;

    return 0;
}

int dvb_wait_iptuner_unlock(dvb_player_t *player) {
    logLogCatKey_stdvb("[devices %d] %s in", player->idx_player, __FUNCTION__);
    RtpInfo_t *pRtpInfo=gRtpInfo[player->idx_player];
    pthread_yield();
#ifdef USE_MULTIVIEW
    if(player->isMultiview) {
        logLogCatKey_stdvb(" dvb_wait_iptuner_unlock call  multiview_unlock");
        multiview_unlock(player);
    } else {
#else
    {
#endif
        logLogCatKey_stdvb(" dvb_wait_iptuner_unlock call  single_unlock");
        pthread_join(pRtpInfo->ThreadReader,NULL);
        if(pRtpInfo->ThreadReaderFeed != 0) pthread_join(pRtpInfo->ThreadReaderFeed,NULL);
        //close(pRtpInfo->socketIn);
        cMulticast_leave(&pRtpInfo->socketIn, &pRtpInfo->blub);

        if(player->idx_player == 0) {
#ifdef USE_QI_CALCULATOR
            qi_calculator_player_stop(&pRtpInfo->ipAddr,1, 0);
#endif

#ifdef USE_MLR
            // Uninitialize MLR Client
            if(player->hMlrClient != NULL) {
                logLogCatKey_stdvb("+++MLR PIX_MLR_Uninitialize ");
                PIX_MLR_Uninitialize(player->hMlrClient);
            }
#endif

#ifdef USE_PIX_ADS
            // Uninitialize MLR Client
            if(player->hAdsClient != NULL) {
                logLogCatKey_stdvb("[AddrAD] PIX_ADS_Uninitialize ");
                PIX_ADS_Uninitialize(player->hAdsClient);
            }
#endif

            if(player->demuxHandle != NULL) {
                DEMUX_Destroy(&player->demuxHandle);
                player->demuxHandle = NULL;						
            }
        }
        player->hMlrClient = NULL;

        player->hAdsClient = NULL;
    }

    player->isMultiview = false;
#ifdef STB_BMT_LEAVE_10MS_JOIN_TEST_BULLSHIT_NOSENSEINDOING
	pRtpInfo->socketBeforeIn = -1;
#endif
    logLogCatKey_stdvb("[devices %d] %s out", player->idx_player, __FUNCTION__);
    return 0;
}

#ifdef USE_MULTIVIEW
void multiview_unlock(dvb_player_t *player) {
    logLogCatKey_stdvb("multiview_unlock in");

    RtpInfo_t *pRtpInfo = gRtpInfo[player->idx_player];
    if(pRtpInfo->ThreadReaderFeed != 0) pthread_join(pRtpInfo->ThreadReaderFeed,NULL);

    if(player->isMultiview) {
        for(int i = 0 ;i < 3 ; i++) {
            t_MultiviewInfo *ptMultiviewInfo = &player->pt_MultiviewInfo[i];
            RtpInfo_t *pMultiviewRtpInfo = gRtpInfo[ptMultiviewInfo->idx_player_for_multiview];
            //pthread_join(pMultiviewRtpInfo->ThreadReader,NULL);

            logLogCatKey_stdvb("[Multiview %d] TEST in ", ptMultiviewInfo->idx_player_for_multiview);
            if(!ptMultiviewInfo->isTerminateComplete) {
                struct	timespec timeout;
                clock_gettime(CLOCK_REALTIME, &timeout);
                timeout.tv_sec += 2;
                timeout.tv_nsec += (500 * 1000000);
                if (timeout.tv_nsec >= 1000000000) {
                    int addSec = timeout.tv_nsec / 1000000000;
                    timeout.tv_sec += timeout.tv_nsec / 1000000000;
                    timeout.tv_nsec %= 1000000000;
                    logLogCatKey_stdvb("[Multiview %d] add sec : %d", ptMultiviewInfo->idx_player_for_multiview, addSec);
                }

                pthread_mutex_lock(&ptMultiviewInfo->multiview_thread_lock);
                int e = pthread_cond_timedwait(&ptMultiviewInfo->multiview_thread_cond, &ptMultiviewInfo->multiview_thread_lock, &timeout);
                pthread_mutex_unlock(&ptMultiviewInfo->multiview_thread_lock);
                if (e == ETIMEDOUT) {
                    logLogCatKey_stdvb("[Multiview %d] timed out waiting for thread to exit", ptMultiviewInfo->idx_player_for_multiview);
                    int status = 0;
                    if ( (status = pthread_kill(pMultiviewRtpInfo->ThreadReader, SIGUSR1)) != 0) {
                        logLogCatKey_stdvb("[MULTIVIEW %d] Error cancelling thread, error = %d", ptMultiviewInfo->idx_player_for_multiview, status);
                        pthread_join(pMultiviewRtpInfo->ThreadReader,NULL);
                    } else {
                        logLogCatKey_stdvb("[MULTIVIEW %d] pthread_detach", ptMultiviewInfo->idx_player_for_multiview);
                        pthread_detach(pMultiviewRtpInfo->ThreadReader);
                    }
                } else pthread_join(pMultiviewRtpInfo->ThreadReader,NULL);
            } else pthread_join(pMultiviewRtpInfo->ThreadReader,NULL);

            //close(pMultiviewRtpInfo->socketIn);
            cMulticast_leave(&pMultiviewRtpInfo->socketIn, &pMultiviewRtpInfo->blub);

            ptMultiviewInfo->isTerminateComplete = true;
            pthread_cond_destroy(&ptMultiviewInfo->multiview_thread_cond);
            pthread_mutex_destroy(&ptMultiviewInfo->multiview_thread_lock);
            logLogCatKey_stdvb(" [Multiview %d] TEST out ", ptMultiviewInfo->idx_player_for_multiview);
        }

        if(player->hMultiView != NULL) PIX_DestroyMergeHevcEs(player->hMultiView);
        player->hMultiView = NULL;
        multiview_final();
    }
    logLogCatKey_stdvb(" multiview_unlock out");
}
#endif

#ifdef STB_BMT_LEAVE_10MS_JOIN_TEST_BULLSHIT_NOSENSEINDOING
int dvb_iptuner_unlock_silent(dvb_player_t *player) {
    RtpInfo_t *pRtpInfo=gRtpInfo[player->idx_player];
    player->TerminateTuner =1;
    pRtpInfo->socketBeforeIn = pRtpInfo->socketIn;
    
    return 0;
}

int dvb_wait_iptuner_unlock_silent(dvb_player_t *player) {
	RtpInfo_t *pRtpInfo=gRtpInfo[player->idx_player];
    pthread_yield();
//    pthread_join(pRtpInfo->ThreadReader,NULL);
//    if(pRtpInfo->ThreadReaderFeed != 0) pthread_join(pRtpInfo->ThreadReaderFeed,NULL);

    if(player->isMultiview) {
        logLogCatKey_stdvb(" dvb_wait_iptuner_unlock_silent call  multiview_unlock");
        multiview_unlock(player);
    } else {
        pthread_join(pRtpInfo->ThreadReader,NULL);
        if(pRtpInfo->ThreadReaderFeed != 0) pthread_join(pRtpInfo->ThreadReaderFeed,NULL);
        //close(pRtpInfo->socketIn);
        //cMulticast_leave(&pRtpInfo->socketIn, &pRtpInfo->blub);

        if(player->idx_player == 0) {
#ifdef USE_QI_CALCULATOR
            qi_calculator_player_stop(&pRtpInfo->ipAddr,1, 0);
#endif

#ifdef USE_MLR
            // Uninitialize MLR Client
            if(player->hMlrClient != NULL) {
                logLogCatKey_stdvb("+++MLR PIX_MLR_Uninitialize ");
                PIX_MLR_Uninitialize(player->hMlrClient);
            }
#endif

#ifdef USE_PIX_ADS
            // Uninitialize MLR Client
            if(player->hAdsClient != NULL) {
                logLogCatKey_stdvb("[AddrAD] PIX_ADS_Uninitialize ");
                PIX_ADS_Uninitialize(player->hAdsClient);
            }
#endif

            if(player->demuxHandle != NULL) {
                DEMUX_Destroy(&player->demuxHandle);
                player->demuxHandle = NULL;				
            }
        }
        player->hMlrClient = NULL;
        player->hAdsClient = NULL;
    }
    return 0;
}

int dvb_close_silent_byerror(dvb_player_t *player)
{
    RtpInfo_t *pRtpInfo=gRtpInfo[player->idx_player];
    if (pRtpInfo->socketBeforeIn >= 0) {
        //close(pRtpInfo->socketBeforeIn);
        cMulticast_leave(&pRtpInfo->socketBeforeIn, &pRtpInfo->blub);
    }

    pRtpInfo->socketBeforeIn = -1;
    return 0;
}

#endif

int dvb_iptuner_lock_one_channel(dvb_player_t *player, dvb_locator_t *locator) {
    struct sockaddr_in si;
//#ifdef USE_MLR
//    static uint first_mlr = 0;
//#endif
	RtpInfo_t *pRtpInfo=gRtpInfo[player->idx_player];

    if (pRtpInfo==NULL) {
        pRtpInfo = (RtpInfo_t *) malloc(sizeof(RtpInfo_t));
        
        if (pRtpInfo == NULL) {
            printf("%s: Unable to create RTP context\n",__FUNCTION__ );
            return -1;
        }
#ifdef STB_BMT_LEAVE_10MS_JOIN_TEST_BULLSHIT_NOSENSEINDOING
		pRtpInfo->socketBeforeIn = -1;
#endif
        gRtpInfo[player->idx_player] = pRtpInfo;
    }

    {   
        //clear  
        memset(pRtpInfo, 0, sizeof(pRtpInfo));

        pRtpInfo->buf_readptr = &pRtpInfo->buffer[0];
        pRtpInfo->buf_writeptr = &pRtpInfo->buffer[0];
        // recv data 확인용 flag
        pRtpInfo->buf_readOneOrMoreData = 0; // clear
        pRtpInfo->buf_max = &pRtpInfo->buffer[SIZE];
        pRtpInfo->buf_min = &pRtpInfo->buffer[0];
        pRtpInfo->connected = 0;

		//open
        { // TODO : jung2604 : 20190429 : zapping time check용..
            if ( 0 == clock_gettime( CLOCK_REALTIME, &player->tTempTimeVal)) {
                player->tempTimeMs = (player->tTempTimeVal.tv_sec * 1000) + (player->tTempTimeVal.tv_nsec / 1000000);
                player->timeCheckSocketBeforTime = player->tempTimeMs;
                logLogCatKey_stdvb("**_** IGMP member ship join, ip : %s, before : %ld ,start ~ now : %d ms ", locator->tune_param.ip.ip, player->tempTimeMs, player->tempTimeMs - player->timeCheckStartTime);
            }
        }
        pRtpInfo->socketIn = stdvbplayer_makeclientsocket(locator->tune_param.ip.ip,locator->tune_param.ip.port, 2, &si, &pRtpInfo->blub, player->idx_player);
        //printf ("%s: sock-fd=%d\n", __FUNCTION__, pRtpInfo->socketIn);

        //open
        { // TODO : jung2604 : 20190429 : zapping time check용..
            if ( 0 == clock_gettime( CLOCK_REALTIME, &player->tTempTimeVal)) {
                player->tempTimeMs = (player->tTempTimeVal.tv_sec * 1000) + (player->tTempTimeVal.tv_nsec / 1000000);
                player->timeCheckSocketAfterTime = player->tempTimeMs;
                logLogCatKey_stdvb("**_** IGMP member ship join, ip : %s, after : %ld ,start ~ now : %d ms ", locator->tune_param.ip.ip, player->tempTimeMs, player->tempTimeMs - player->timeCheckStartTime);
            }
        }

        if (pRtpInfo->socketIn <= 0) return -1;
        pRtpInfo->connected = 1;
    }

    player->TerminateTuner = 0;

#ifdef USE_MLR
    //MLR client
    if(player->idx_player == 0) { //jung2604 : 20190228 : MAIN만 실행
        char server_port[10];
        sprintf(server_port, "%9d", locator->tune_param.ip.port);
        server_port[9] = 0;
        //logLogCatKey_stdvb("++Device[%d] PIX_MLR_Initialize : , ip : %s, port : %s", player->idx_player, /*player->hMlrClient ,*/ locator->tune_param.ip, server_port);

        player->isMlrClientHangup = false;

        if(player->enable_video) { //음악채널은 MLR을 하지 않는다.
            logLogCatKey_stdvb("++Device[%d] PIX_MLR_Initialize ", player->idx_player);
            player->mlrFunction = (MLR_FUNCTION) processMlr;
            //yiwoo 0426 : send MAC to MLR
            if(get_first_start() == 0)
            {
              MLR_set_path_and_Mac();
              set_first_start(1);
            }
            //yiwoo 0414 : add last argument 1 for new lib.
            player->hMlrClient = PIX_MLR_Initialize(0, locator->tune_param.ip.ip, server_port, 0, 0, 1);
            //player->hMlrClient = PIX_MLR_Initialize(0, locator->tune_param.ip.ip, server_port, 0, 0);

            if (player->hMlrClient == NULL) perror("[ERROR] Load MlrClient Library\n");
            else {
               // PIX_MLR_RegisterQiFunc(player->hMlrClient,
            }
        }

        player->isAdsClientHangup = false;

#ifdef USE_PIX_ADS
        if(player->enable_video) { //음악채널은 MLR을 하지 않는다.
            logLogCatKey_stdvb("[AddrAD][Device:%d] PIX_ADS_Initialize ", player->idx_player);
            player->adsFunction = (ADS_FUNCTION) processAds;
            player->hAdsClient = PIX_ADS_Initialize(0, locator->tune_param.ip.ip, server_port, 1);
            if (player->hAdsClient == NULL) perror("[AddrAD][ERROR] Load AdsClient Library\n");
        }
#endif
        pRtpInfo->ipAddr = inet_addr(locator->tune_param.ip.ip);
        pRtpInfo->ipPort = (unsigned short) locator->tune_param.ip.port;

#ifdef USE_QI_CALCULATOR
        if(player->enable_video) { //음악채널은 MLR을 하지 않는다.
            qi_calculator_register_mlr_functiion(player->hMlrClient);
        }
        qi_calculator_player_start(&pRtpInfo->ipAddr, 1, 0);

        if (player->cbQiCalcOnJoinRequested)
            player->cbQiCalcOnJoinRequested(pRtpInfo->ipAddr, locator->tune_param.ip.port);
#endif
        if(!DEMUX_Init(&player->demuxHandle, 188)){
                logLogCatKey_stdvb("demux_init failed");
            	player->demuxHandle = NULL;
        } else {
#if 0 // firstframe 체크하는 방법
            player->demuxHandle->video_codec = (locator->vcodec == AVP_CODEC_VIDEO_H265) ? 0x24:0x1B; //0x1B : hd, 0x24 : uhd
            logLogCatKey_stdvb("DEMUX_SetPesFilter : pid : %d, codec : 0x%x(0x%x)", locator->pid, locator->vcodec, player->demuxHandle->video_codec);
            DEMUX_SetPesFilter(player->demuxHandle, VIDEO_FILTER, locator->vid, player->demuxHandle->video_codec );
#endif
        }
    }
#endif

    if (pthread_create(&pRtpInfo->ThreadReader, NULL, iptv_media_player_recv_thread, (void *)player) != 0) {
        perror("pthread_create :");
    }

    if (pthread_create(&pRtpInfo->ThreadReaderFeed, NULL, iptv_media_player_feed_thread, (void *)player) != 0) {
        perror("pthread_create :");
    }

    return 0;    
}

int dvb_iptuner_lock(dvb_player_t *player, dvb_locator_t *locator) {
#ifdef USE_MULTIVIEW
    if(!player->isMultiview) return dvb_iptuner_lock_one_channel(player, locator);

    player->TerminateTuner = 0;

    logLogCatKey_stdvb("[MULTIVIEWTHREAD] Multiview Thread begin...");

	// multiview init global object;
	multiview_init();
	
    int ret = dvb_iptuner_multiview_lock(player, &locator[0], 0);
    if(ret != 0) return ret;

    ret = dvb_iptuner_multiview_lock(player, &locator[1], 1);
    if(ret != 0) return ret;

    ret = dvb_iptuner_multiview_lock(player, &locator[2], 2);
    if(ret != 0) return ret;
#else
    return dvb_iptuner_lock_one_channel(player, locator);
#endif

    return 0;
}

#ifdef USE_MULTIVIEW
int dvb_iptuner_multiview_lock(dvb_player_t *player, dvb_locator_t *locator, int multiviewNumber) {
    pt_MultiviewInfo ptMultiviewInfo = &player->pt_MultiviewInfo[multiviewNumber];
    ptMultiviewInfo->tLocator = *locator;
    ptMultiviewInfo->viewNumber = multiviewNumber;
    ptMultiviewInfo->pPlayer = player;
    ptMultiviewInfo->readCnt = 0;
    ptMultiviewInfo->idx_player_for_multiview = (multiviewNumber == 0) ? 0:(1 + multiviewNumber); // jung2604 : 0은 main, 1은 pip 0과 1만 쓰이고 2부터는 사용되지 않는다. 그래서 2번째 부터 multiview에서 사용하도록 한다
    //dvb_player_t *player = pptMultiviewInfo->pPlayer;
    int idx_player_for_multiview = ptMultiviewInfo->idx_player_for_multiview;
    struct sockaddr_in si;

    RtpInfo_t *pRtpInfo=gRtpInfo[idx_player_for_multiview];

    if (pRtpInfo==NULL) {
        pRtpInfo = (RtpInfo_t *) malloc(sizeof(RtpInfo_t));

        if (pRtpInfo == NULL) {
            printf("%s: Unable to create RTP context\n",__FUNCTION__ );
            return -1;
        }
#ifdef STB_BMT_LEAVE_10MS_JOIN_TEST_BULLSHIT_NOSENSEINDOING
        pRtpInfo->socketBeforeIn = -1;
#endif
        gRtpInfo[idx_player_for_multiview] = pRtpInfo;

        if(multiviewNumber == 0) ptMultiviewInfo->pPlayer->hMultiView = NULL;
    }

    {
        //clear
        memset(pRtpInfo, 0, sizeof(pRtpInfo));

        pRtpInfo->buf_readptr = &pRtpInfo->buffer[0];
        pRtpInfo->buf_writeptr = &pRtpInfo->buffer[0];
        pRtpInfo->buf_readOneOrMoreData = 0; // clear
        pRtpInfo->buf_max = &pRtpInfo->buffer[SIZE];
        pRtpInfo->buf_min = &pRtpInfo->buffer[0];
        pRtpInfo->connected = 0;

        //open
        pRtpInfo->socketIn = stdvbplayer_makeclientsocket(locator->tune_param.ip.ip,locator->tune_param.ip.port, 2, &si, &pRtpInfo->blub, idx_player_for_multiview);
        //printf ("%s: sock-fd=%d\n", __FUNCTION__, pRtpInfo->socketIn);


        if (pRtpInfo->socketIn <= 0) return -1;
        pRtpInfo->connected = 1;
    }

    if(multiviewNumber == 0 && player->hMultiView == NULL) {
        player->hMultiView = PIX_CreateMergeHevcEs((player->multiviewType == 1) ? 1:0);
        int ret = PIX_SetTimeStampId(player->hMultiView, 0);
        logLogCatKey_stdvb("++Device[%d] PIX_ handler : %d, PIX_SetTimeStampId ret : %d", player->idx_player, player->hMultiView, ret);
    }

    pthread_mutex_init(&ptMultiviewInfo->multiview_thread_lock, 0);
    pthread_cond_init(&ptMultiviewInfo->multiview_thread_cond, 0);
    ptMultiviewInfo->isTerminateComplete = 0;
    // jung2604 : multi view 3개의 recv 스레드로 구성...
    if (pthread_create(&pRtpInfo->ThreadReader, NULL, multiview_recv_thread, (void *)ptMultiviewInfo) != 0) {
        perror("pthread_create :");
        pthread_cond_destroy(&ptMultiviewInfo->multiview_thread_cond);
        pthread_mutex_destroy(&ptMultiviewInfo->multiview_thread_lock);
    }

    if(multiviewNumber == 0) { // jung2604 : feed 스레드는 FFMPEG사용으로 1개만 있으면 됨..
        if (pthread_create(&pRtpInfo->ThreadReaderFeed, NULL, multiview_feed_thread, (void *) ptMultiviewInfo) != 0) {
            perror("pthread_create :");
        }
    }
    return 0;
}
#endif

#ifdef USE_MLR
#define iPacketSize 1316 //188 * 7
int processMlr(dvb_player_t *player, char *buf, int *pLengthData) {
    //pix_int32 iRet;
    int iRet;
    //pix_int32 iPacketSize;
    //pix_int32 iMlrOutDataSize = 0;
    int iMlrOutDataSize = 0;
    char *pOutData = NULL;

    // jung2604 : 20190228 : MLR
    if(player->idx_player == 0 && !player->isMlrClientHangup && player->hMlrClient != NULL ) {
        //////////////////////////////////////////
        // push stream to MLRClient;
        //////////////////////////////////////////
        iRet = PIX_MLR_PutPackets(player->hMlrClient, (void *)buf, (unsigned int) *pLengthData);
        if(iRet < 0) {
            player->isMlrClientHangup = true;
            logLogCatKey_stdvb("[ERROR] PIX_PutPkt(). %d\n", iRet);
            return -2;
        }
        else {
            //////////////////////////////////////////
            // get stream from MLRClient
            //////////////////////////////////////////
            pOutData = buf;
            do {
                //jung2604 : UHD의 경우 RTP를 뺀 값을 얻어와야한다..ts이니 항상 1316 이다!
                iRet = PIX_MLR_GetPackets(player->hMlrClient, (unsigned char *) pOutData, iPacketSize);
                if (iRet >= 0) {
                    iMlrOutDataSize = iMlrOutDataSize + iPacketSize;
                    pOutData = pOutData + iPacketSize;
                    if ((iMlrOutDataSize + iPacketSize) >= (1316 * 20))
                        break;  // exit GetPkt() Loop
                } else {
                    break;
                }
            } while (!player->TerminateTuner);

            // jung2604 : 20190228 : MRL에 의해서 버퍼의 사이즈가 변경이 되었다!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if(*pLengthData != iMlrOutDataSize) {
                //logLogCatKey_stdvb("++[rtp.c][DEVICE %d][%s] recv size : %d, change mlr size : %d", player->idx_player, (iRet >= 0) ? "TRUE":"FALSE", *pLengthData, iMlrOutDataSize);
                *pLengthData = iMlrOutDataSize;
            }

            return iMlrOutDataSize;
        }
    }

    return -1;
}
#endif

#ifdef USE_PIX_ADS
int processAds(dvb_player_t *player, char *buf, int *pLengthData) {
    pix_int32 iRet;
    pix_int32 iAdsOutDataSize = 0;
    char *pOutData = NULL;

    if(player->idx_player == 0 && !player->isAdsClientHangup && player->hAdsClient != NULL ) {
        //logLogCatKey_stdvb("[call] PIX_ADS_PutPackets player->hAdsClient:%p, *pLengthData:%d\n", player->hAdsClient, *pLengthData);
        iRet = PIX_ADS_PutPackets(player->hAdsClient, (void *)buf, (unsigned int) *pLengthData);
        if(iRet < 0) {
            player->isAdsClientHangup = true;
            logLogCatKey_stdvb("[AddrAD][ERROR] PIX_ADS_PutPackets(). %d\n", iRet);
            return -2;
        }
        else {
            pOutData = buf;
            do {
                //jung2604 : UHD의 경우 RTP를 뺀 값을 얻어와야한다..ts이니 항상 1316 이다!
                iRet = PIX_ADS_GetPackets(player->hAdsClient, (unsigned char *) pOutData, iPacketSize);
                if (iRet >= 0) {
                    iAdsOutDataSize = iAdsOutDataSize + iPacketSize;
                    pOutData = pOutData + iPacketSize;
                    if ((iAdsOutDataSize + iPacketSize) >= (1316 * 20))
                        break;  // exit GetPkt() Loop
                } else {
                    break;
                }
            } while (!player->TerminateTuner);

            // jung2604 : 20190228 : ADS에 의해서 버퍼의 사이즈가 변경이 되었다!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if(*pLengthData != iAdsOutDataSize) {
                *pLengthData = iAdsOutDataSize;
            }

            return iAdsOutDataSize;
        }
    } else {
        logLogCatKey_stdvb("[AddrAD][ERROR] player->idx_player:%p , player->isAdsClientHangup:%d, player->hAdsClient:%p\n", player->idx_player, player->isAdsClientHangup, player->hAdsClient);
    }

    return -1;
}
#endif

//BlankStream(0,0);
#ifdef __cplusplus
} //extern "C"
#endif
