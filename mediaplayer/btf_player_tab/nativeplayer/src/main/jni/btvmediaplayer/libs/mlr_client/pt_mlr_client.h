/*
 * pt_mlr_client.h
 *
 * created by : kyungtaek.lee
 * Copyright (c) 2018 PIXTREE, Inc.
 * All rights reserved.
 *
 */
 
#ifndef __PT_MLR_CLIENT_H__
#define __PT_MLR_CLIENT_H__

#if _WIN32
#ifdef MLR_DLL_BUILD
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT __declspec(dllimport)
#endif
#else
#define DLL_EXPORT
#endif // ~_WIN32

typedef struct mlr_status_s
{
  int           iVersion;
  unsigned int  uiTimeStamp;
  unsigned int  uiMulticastIpAddr;

  // input stream
  int           iChannelBitrate;
  int           iMLS;
  int           iMLC;
  int           iJitter;
  unsigned int  uiJointTime;
  int           iDuration;
  int           iTotalTsCnt;

  // mlr status
  int           iRecoverySize;
  int           iReqSendCnt_Ok;
  int           iReqFailCnt_OverLimit;
  int           iReqFailCnt_Other;
  int           iResp_Cnt_Ok;
  int           iRespFailCnt_Over1ReqSize;
  int           iRespFailCnt_Over1SecSize;
  int           iRespFailCnt_NotCaching;
  int           iRespFailCnt_ServerOther;
  int           iRespFailCnt_NotRecv;
  int           iRespFailCnt_Other;
  int           iRescovryTime;

  unsigned int  uiOverwriteSize;
#if 0
  int           iMlrEnableCfg;        // 2018.10.10 Add Item : mlr enable : configuration.
  int           iMlrEnableSts;        // 2018.10.10 Add Item : mlr enable : running status.
  unsigned int  uiMlrServerIp;        // 2018.10.10 Add Item : mlr server IP : inet_addr().
#endif
} mlr_status_t;

typedef void* MLR_HANDLE;
typedef int (*CallBackQiFunc)(struct mlr_status_s *psMlrStatus, int iStatusSize);


#ifdef __cplusplus 
extern "C" {
#endif

DLL_EXPORT void PIX_MLR_GetVersion(int *pVersion);

// Active uesd function

// Add for MLR-Air
//DLL_EXPORT int PIX_MLR_SetTempStoragePath(char *pPath);
DLL_EXPORT int PIX_MLR_AirSetup(char *pConfigPath, char *pMacAddr);

//yiwoo 0414 : C not support default argument bUseFCZ=1
DLL_EXPORT MLR_HANDLE PIX_MLR_Initialize(int iId, char *pMulticastIP, char *pMulticastIpPort,
                                                  char *pCachingServerIp, char *pCachingServerIpPort, int bUseFCZ);
//DLL_EXPORT MLR_HANDLE PIX_MLR_Initialize(int iId, char *pMulticastIP, char *pMulticastIpPort,
//                                                  char *pCachingServerIp, char *pCachingServerIpPort, int bUseFCZ = 1);

DLL_EXPORT void PIX_MLR_Uninitialize(MLR_HANDLE pHandle);
DLL_EXPORT int PIX_MLR_PutPackets(MLR_HANDLE pHandle, unsigned char *pPackets, unsigned int iPacketSize);
DLL_EXPORT int PIX_MLR_GetPackets(MLR_HANDLE pHandle, unsigned char *pPackets, unsigned int iPacketSize);

// End Active uesd function

DLL_EXPORT void PIX_MLR_NotifyNextBufferStatus(MLR_HANDLE pHandle, int iBufferSize, int iFilledSize);
DLL_EXPORT int PIX_MLR_SetOperateMode(MLR_HANDLE pHandle, int iOpMode);
DLL_EXPORT int PIX_MLR_GetOperateMode(MLR_HANDLE pHandle);
DLL_EXPORT int PIX_MLR_GetLastError(MLR_HANDLE pHandle);
DLL_EXPORT int PIX_MLR_RegisterQiFunc(MLR_HANDLE pHandle, CallBackQiFunc fptrQiFunc);

#ifdef __cplusplus 
}
#endif

#endif

