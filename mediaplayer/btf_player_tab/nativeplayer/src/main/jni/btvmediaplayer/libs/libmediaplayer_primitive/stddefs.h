// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef _STDDEFS_H_
#define _STDDEFS_H_

/* C++ support */
/* ----------- */
#ifdef __cplusplus
extern "C" {
#endif

/*! Section for stapi easy integration */
/* ----------------------------------- */
typedef unsigned char      U8;
typedef unsigned short     U16;
typedef unsigned int       U32;
typedef unsigned long long U64;
typedef signed   char      S8;
typedef signed   short     S16;
typedef signed   int       S32;
typedef signed   long long S64;
typedef unsigned char UCHAR;
typedef UCHAR       BOOL;
#define TRUE  1
#define FALSE 0

/*! Error code list stapi like */
/* --------------------------- */
typedef enum
{
  ST_NO_ERROR=0,                  /*!< 0 No errors                               */
  ST_ERROR_BAD_PARAMETER,         /*!< 1 Bad parameter passed                    */
  ST_ERROR_NO_MEMORY,             /*!< 2 Memory allocation failed                */
  ST_ERROR_UNKNOWN_DEVICE,        /*!< 3 Unknown device name                     */
  ST_ERROR_ALREADY_INITIALIZED,   /*!< 4 Device already initialized              */
  ST_ERROR_FEATURE_NOT_SUPPORTED, /*!< 5 Feature unavailable                     */
  ST_ERROR_TIMEOUT,               /*!< 6 Timeout occured                         */
  ST_ERROR_DEVICE_BUSY,           /*!< 7 Device is currently busy                */
  ST_ERROR_UNKNOWN_ERROR=-1       /*!< 8 No explicit error, check errno variable */
} ST_ErrorCode_t;

/* C++ support */
/* ----------- */
#ifdef __cplusplus
}
#endif
#endif
