// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

//#define LOG_NDEBUG 0
//#define LOG_TAG "IptvSocPlayer"
#define LOG_TAG "BOXKEY-IptvSocPlayer"

#include <ALog.h> //USE "BOXKEY-IptvSocPlayer"

#include "IptvSocPlayer.h"

#include "libs/zooinnetrtspclient/play_rtsp.h"

#include <mcas_iptv.h>
#include <mcas_port.h>

#include <mcas-reg-dummy-impl.h>

//#include <mcas-port-impl.c>

#ifdef USE_CAS_DRM
#include <wise_skdrm.h>
#endif
#include <mcas-reg-dummy-impl.h>
#include <dmux_define.h>

#include <DRMCrypto.h>


#include <systemproperty.h>
#ifdef USE_QI_CALCULATOR
#include "qi_calculator.h"
#endif

#include <libs/libmediaplayer_primitive/IptvImplement.h>
#include <hal/iptvhaldef.h>

#include "dtvcc.h"
#include "../casdrmImpl/mcas_iptv.h"
#include "../../include/iptvmedia/util_logmgr.h"

#define IPTV_SIGNAL_INIT	-1
#define IPTV_SIGNAL_GOOD	0
#define IPTV_SIGNAL_BAD		1
#define IPTV_SIGNAL_UPDATE	2

#ifdef __cplusplus
extern "C" {
#endif

// mcas-port-impl.c 에서 참조하는 헤더파일에서 추출.
#include <pthread.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <btv_hal.h>
//#include <dbg.h> //USE "BOXKEY-STDBV"
#include <dcsxc_wrapper.h>

#include <android/native_window_jni.h>
#include <android/native_window.h>

extern int	gBoxBootup[];

extern void SKCAS_SetMCASTChannelAudioPid ( int WindowID, unsigned int AudioPid );
extern void SKCAS_SetMCASTChannelVideoPid ( int WindowID, unsigned int VideoPid );

extern void SKCAS_SetCurrentChannelWindowID ( int CurrentWindowID );
extern void SKCAS_SetMulticastServiceSwitchedStatus ( unsigned long WindowId, int MulticastServiceSwitchedStatus );
extern void SKCAS_SetPredefinedPMTChannelZapStatus ( unsigned long WindowId, int PredefinedPMTChannelZapStatus );

extern	int SKCAS_GetECMCallbackSetByCAS ( int WindowID );
extern unsigned short SKCAS_GetECMPidSetByCAS (int WindowID);
extern unsigned int SKCAS_GetMCASTChannelVideoPid ( int WindowID );
extern unsigned int SKCAS_GetMCASTChannelAudioPid ( int WindowID );

#define IPTV_CAS_TEMP_ALL_DCS_RM  "/btf/btvmedia/tmp/iptv_cas/*.dcs"
#define IPTV_CAS_STBID_TXT_RM  "/btf/btvmedia/cas/stb_id.txt"
#define IPTV_CAS_RSEID_BIN_RM  "/btf/btvmedia/cas/rse_id.bin"
#define IPTV_CAS_STBINFO_BIN_RM  "/btf/btvmedia/cas/stb_info.bin"
#define IPTV_CAS_CERTIFICATES_RM  "/btf/btvmedia/cas/Certificates"
#define IPTV_CAS_CERTIFICATES_USER_100  "/btf/btvmedia/cas/Certificates/user/1000"
#define IPTV_CAS_DRM_FOLDER_RM  "/btf/btvmedia/drm"

unsigned char * gPMTbuffer[2] = {0,};
unsigned short gPMTbufferLen[2] = {0,};
//void CASEpgMsgCheck(int w);
extern pthread_mutex_t	pmt2cas_lock;
unsigned char *PredefinedPMTBuffer4ServStop[2] = {0};

extern BYTE gCAdes ;

//extern pthread_t        TaskID = 0xFFFFFFFF;
extern pthread_t        TaskID;
extern int	 	        g_nTaskRun;// = 0;
//jung2604
//extern WISE_GROUP_STRUCT   stGroupSt[2];

char iptv_cas_tmp_all_dcs_rm[256];
char iptv_cas_stbid_txt_rm[256];
char iptv_cas_resid_bin_rm[256];
char iptv_cas_stbinfo_bin_rm[256];
char iptv_cas_certificates_rm[256];
char iptv_cas_drm_folder_rm[256];
char iptv_cas_certificates_user_100[256];


extern unsigned char GetEMMPidFilterStatus ( int windowId );
extern unsigned char SKCAS_GetStopEMMDataSending2CASStatus ( int WindowID );

int pWindowId0 = 0;
int pWindowId1 = 1;

enum ValueType {
	TYPE_INT    = 0,
	TYPE_LONG   = 1,		
	TYPE_STRING = 2
};

int gAuthChecked = 0;

void string_version() {
    char stVersion[256] ;
    sprintf(stVersion, "sptek__version__libiptvmediahal : 0.6.85");
}

//

// 여기서부터는 EMM 관련 루틴.

/* ================================================================================ */
/*                       Begin of Multicast Related Socket Codes                    */
/* ================================================================================ */
/*
 * Get the IP family by name
 * ------------------------------
 * ORG fname: We_wise_GetIPFamily => NEW fname: skcas_get_ip_family
 */
int skcas_get_ip_family ( char *phostname )
{
    struct addrinfo *result = NULL;
    struct addrinfo hint;
    int error = 0;
    int ai_family = 0;

    if (phostname == NULL)
        return AF_UNSPEC;

    memset(&hint, 0, sizeof(struct addrinfo));

    ai_family = AF_INET;
    hint.ai_family = AF_UNSPEC;

    error = getaddrinfo(phostname, NULL, &hint, &result);
    if (error != 0){
        ALOGD("ERROR:skcas_get_ip_family getaddrinfo error:%s", gai_strerror(error));
    }

    if (result != NULL) {
        ai_family = result->ai_family;

        freeaddrinfo(result);
    }

    return ai_family;
}


/*
 * Converts ascii text to in_addr struct.  NULL is returned if the address can not be found.
 * -----------------------------------------------------------------------------------------
 * ORG fname: We_atoaddr => NEW fname: skcas_atoaddr
*/
struct in_addr *skcas_atoaddr ( char *address )
{
    struct hostent *host;
    static struct in_addr saddr;

    saddr.s_addr = inet_addr(address);
    if (saddr.s_addr != (unsigned long)-1)
        return &saddr;

	// 2012.02.10 namsj gethostbyname() 이 실패할 경우가 있다.
	// 인터넷에 찾아 보면 대략 실제 호스트가 많을 경우 그럴 때가 있다는 것 같다.
	// gethostbyname2()를 사용하게 되면 잘 얻어 진다.
    //host = gethostbyname(address);
    host = gethostbyname2(address, AF_INET);
    if (host != NULL)
        return (struct in_addr *) *host->h_addr_list;

    return NULL;
}


/*
 *  ORG fname: We_get_connection => NEW fname: skcas_get_connection
 * @@@return value
 *      Error: -1
 *      Success: >= 0
*/
int skcas_get_connection ( int socket_type, u_short port, char *netaddress )
{
    struct in_addr *addr = NULL;
    struct sockaddr_in address;
    int sock = -1;
    int reuse_addr = 1;

    memset((char *) &address, 0, sizeof(address));
    address.sin_family = AF_INET;
    address.sin_port = htons(port);

    sock = socket(AF_INET, socket_type, 0);
    if (sock < 0) {
        ALOGD("ERROR:socket open error:%d", sock);
        return -1;
    }

    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &reuse_addr, sizeof(reuse_addr));

    if (netaddress) {
        addr = skcas_atoaddr(netaddress);
        address.sin_addr.s_addr = addr->s_addr;
    } else {
        address.sin_addr.s_addr = INADDR_ANY;
    }

    if (::bind(sock, (struct sockaddr *) &address, sizeof(address)) < 0) {
        ALOGD("ERROR bind error:%d", sock);
        close(sock);
        return -1;
    }

    return sock;

}


/*
 * ORG fname: We_SocketOpenDgram => NEW fname: skcas_open_dgram
 * @@@return value
 *      Error: -1
 *      Success: >= 0
*/
int skcas_open_dgram ( const char *ip, unsigned short port, int ipv6 )
{
    int newBufSize = 0;
    int sizeofBufSize = 0;

    int answer = skcas_get_connection(SOCK_DGRAM, port, (char*)ip);
    if (answer == -1)
        return answer;

    int bufSize = 1024*1024*4;

    if (setsockopt((int) answer, SOL_SOCKET, SO_RCVBUF, (void*)&bufSize, sizeof(bufSize)) < 0){
		ALOGD("Can't change system network size (wanted size = %d)", bufSize);
    }

    getsockopt((int) answer, SOL_SOCKET, SO_RCVBUF, (void*)&newBufSize, (socklen_t*)&sizeofBufSize);
    if (newBufSize != bufSize){
		ALOGD("WARNING:buffer size is %d, wanted was %d", newBufSize, bufSize);
    }
    return answer;
}


/*
 * ORG fname: We_wise_SSM_Join => NEW fname: skcas_join
 * @@@return value
 *      Error: RM_ERROR
 *      Success: RM_OK
*/
int skcas_join ( int sock, WISE_GROUP_STRUCT *pJoin_Info, int ipv6 )
{
    int ai_family = 0;
    char *srclist = NULL;
    char *lgroupip = NULL;
    int error = 0;

    lgroupip = pJoin_Info->pgroup_ip;
    srclist = pJoin_Info->psource_ip;

    ai_family = ( ipv6 ) ? AF_INET6 : AF_INET;

    // in case of SSM join, you can use protocol-independent API
    if ( srclist != NULL ) {
        struct addrinfo hints, *source_addinfo = NULL, *group_addinfo = NULL;
        group_source_req gsreq;

        int level = 0;

        // Get group address info
        memset(&hints, 0, sizeof(hints));
        hints.ai_family = ai_family;

        error = 0;
        error = getaddrinfo(lgroupip, NULL, &hints, &group_addinfo);
        if (error) {
            ALOGD("Error in group id getaddrinfo");
            perror( "Error in group id getaddrinfo \n" );

            return RM_ERROR;
        }

        // Get source address info
        memset(&hints, 0, sizeof(hints));
        hints.ai_family = ai_family;

        ALOGD("Checking add src for source");
        if (getaddrinfo(srclist, NULL, &hints, &source_addinfo)) {
            ALOGD("ERRO:Error in source id getaddrinfo");
            freeaddrinfo(source_addinfo);
            return RM_ERROR;
        }

        memset(&gsreq, 0, sizeof(group_source_req));
        gsreq.gsr_interface = 0; // default index
        memcpy(&gsreq.gsr_group, group_addinfo->ai_addr, group_addinfo->ai_addrlen);

        switch (ai_family) {
            case AF_INET6:
                level = IPPROTO_IPV6;
                break;
            case AF_INET:
                level = IPPROTO_IP;
                break;
            default:
                ALOGD("ERROR:unsupported addSource_addinfos family");
                break;

                return RM_ERROR;
        }

        memcpy(&gsreq.gsr_source, source_addinfo->ai_addr, source_addinfo->ai_addrlen);

        // store the last joined group information
        if ( pJoin_Info->pJoinReq != NULL )
            memcpy (pJoin_Info -> pJoinReq, &gsreq, sizeof(gsreq));

		ALOGD("setsockopt called with MCAST_JOIN_SOURCE_GROUP");

        if (setsockopt( (int) sock, level, 46, &gsreq, sizeof(gsreq)) != 0 ) {
            ALOGD("ERROR:setsockopt(MCAST_JOIN_SOURCE_GROUP)");
            return RM_ERROR;
        }

        freeaddrinfo(source_addinfo);
        freeaddrinfo(group_addinfo);
    }

    return RM_OK;
}


/*
 * ORG fname: We_SocketJoinMulticastSession => NEW fname: skcas_join_multicast_session
 * @@@return value
 *      Error: RM_ERROR
 *      Success: RM_OK
*/
int skcas_join_multicast_session (int sock, const char *ip, unsigned long ttl, int ipv6, WISE_GROUP_STRUCT *pJoin_Info )
{
    // if we have source list then check for source specific multicast
    if ( pJoin_Info != NULL &&  pJoin_Info->psource_ip != NULL ) {
        return skcas_join ( sock, pJoin_Info, ipv6 );
    }

    if (ipv6) {
        struct ipv6_mreq mreq;
        int ret = 0;

        ret = inet_pton(AF_INET6, ip, (void*)&(mreq.ipv6mr_multiaddr) );
        mreq.ipv6mr_interface = 0; // ???

        if (setsockopt((int)sock, IPPROTO_IPV6, IPV6_JOIN_GROUP, &mreq, sizeof(mreq)) < 0) {
            ALOGD("ERROR:skcas_join_multicast_session ipv6_mreq setsockopt");
            return RM_ERROR;
        }
    } else {
        struct ip_mreq mreq;

        mreq.imr_multiaddr.s_addr = inet_addr(ip);
        mreq.imr_interface.s_addr = htonl(INADDR_ANY);

        if (setsockopt((int)sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0) {
            ALOGD("ERROR:skcas_join_multicast_session setsockopt");
            return RM_ERROR;
        }
    }

    if (ttl > 0) {
        if (setsockopt((int)sock, IPPROTO_IP, IP_MULTICAST_TTL, (char*)&ttl, sizeof(ttl)) < 0) {
            ALOGD("ERROR:skcas_join_multicast_session ttl > 0 setsockopt");
            return RM_ERROR;
        }
    }

    return RM_OK;
}


/*
 * ORG fname: We_OpenMulticast => NEW fname: skcas_open_multicast
 * @@@return value
 *      Error: -1
 *      Success: >= 0
*/
int skcas_open_multicast ( char *address, unsigned short port, int iWindowID )
{
    int socket;
    int status;
    int IsIPv6 = 0;

    IsIPv6 = (skcas_get_ip_family(address) == AF_INET6) ? 1 : 0;
    socket = skcas_open_dgram((char *) address, port, IsIPv6);
    if (socket == -1)
        return socket;

    //jung2604 : 사용하는곳이 없어서 제거 함
    //stGroupSt[iWindowID].group_sock = (int)socket;
    status = skcas_join_multicast_session(socket, address, 0, IsIPv6, 0 );
    if (status != RM_OK) {
        ALOGD("ERROR:skcas_join_multicast_session status:%d", status);
        close((int)socket);
        return -1;
    }

    return socket;
}


// ================================================================================
//                         End of Multicast Related Socket Codes
// ================================================================================
// SAK - 25Feb2009 - Obtain the SCSHOST address from /mnt/config/svcagent.conf file
int ParseMntConfig ( char *filename, char *EMMMulticastAddr, char *EMMPortno )
{
    FILE *fp = NULL;
    char xbuf[256] = { 0, };
    char str1[50] = { 0, }, str2[50] = { 0, };

    fp = fopen( filename, "r" );
    if (!fp){
        strcpy(EMMMulticastAddr, EMM_MULTICAST_IP);
        sscanf("49220","%s", EMMPortno);
        return FALSE;
    }

    while (fgets(xbuf, sizeof(xbuf), fp)){
        if (xbuf[0] == '#' || xbuf[0] == ' ' || xbuf[0] == '\n' || xbuf[0] == '\r' || strlen(xbuf) < 6)
        {
            memset(xbuf, 0, sizeof(xbuf));
            continue;
        }

        if (xbuf[strlen(xbuf)-1] == '\n' || xbuf[strlen(xbuf)-1] == '\r')
        {
            xbuf[strlen(xbuf)-1] = '\0';
        }

        if( strstr(xbuf, "EmmHost")){
            sscanf ( xbuf, "EmmHost%s", str1 );
            sscanf ( str1, "%s", EMMMulticastAddr );
        }
        else if ( strstr(xbuf, "EmmPort")){
            sscanf ( xbuf, "EmmPort%s", str2 );
            sscanf ( str2, "%s", EMMPortno );
        }
        else{
            continue;
        }
    }

    fclose(fp);

	ALOGD("ParseMntConfig EMMMulticastAddr:%s EmmPort:%s", EMMMulticastAddr, EMMPortno);

    return TRUE;
}


int EMMDataCheck ( int windowId )
{
	ALOGD("EMMDataCheck()");

    // Check whether the EMM Pid filter is being invoked by CAS library or not
	//if ( GetEMMPidFilterStatus(windowId) ){
		int                       len = 0;
		int                       packet_size=TS_PACKET_LEN;
		unsigned char            packet_buf[ TS_PACKET_LEN ] = {0};
		uint32                        NoOfEMMTPSent2CAS = 0;
		unsigned char             *tempBuffer = NULL;
		unsigned char             validEMMPid = FALSE;

		//unsigned short          portno;
		int                       socket, EMMPid=0;

#ifdef DUMP_EMM_DATA
		FILE                   *EMM_saveFile=NULL;
#endif // DUMP_EMM_DATA

        int                 error = 0;
        int                 emm_portno = 0;
        char                EMMAddr[256] = {0,};	// SAK - 25Feb2009 -Added
        char                EMMPort[256] = {0,};

		char dataPath[128];
		get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
		strcat(dataPath, EMM_MULTICAST_CONF_FILE);
        ParseMntConfig (dataPath, EMMAddr, EMMPort);
        sscanf (EMMPort, "%d", &emm_portno);

		socket = skcas_open_multicast((char *)EMMAddr, emm_portno, 0);

        if (socket == -1){
            ALOGD("ERROR:EMMDataCheck skcas_open_multicast");
            return  FALSE;
        }

        while (g_nTaskRun){
            // Receive the 188 bytes EMM data from Multicast
            len = recv(socket, packet_buf, packet_size, 0);

            if(0 == g_nTaskRun){
            	ALOGD("g_nTaskRun : %d -> break loop!", g_nTaskRun);
            	break;
            }

            if(0 > len){
            	ALOGD("0 > len -> break loop!");
            	break;
            }

            tempBuffer = packet_buf;
            EMMPid = ((packet_buf[1] & 0x1f) << 8) | packet_buf[2];
            if ( EMMPid == 5000  ){	// 0x1388
                validEMMPid = TRUE;
            }
            else{
                ALOGD("WARNING InValid EMMPID[%d]", EMMPid);
                validEMMPid = FALSE;
            }

            validEMMPid = TRUE;

            if ( len <= 0 ){
				ALOGD("ERROR:Invalid EMM length:%d", len);
            }
            else{
            	// Only if the TS is valid, send it  to CAS library
                //	if ( validEMMPid == TRUE &&  ((bStopEMMDataSending2CAS[0] == FALSE) ||(bStopEMMDataSending2CAS[1] == FALSE)))

				if ( validEMMPid == TRUE &&
						((SKCAS_GetStopEMMDataSending2CASStatus(0) == FALSE) ||
								(SKCAS_GetStopEMMDataSending2CASStatus(1) == FALSE)) )
				{
					NoOfEMMTPSent2CAS++;

					//ALOGD("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
					//ALOGD("$$$$$$$  MCAS_CasTs( packet_buf, %d )  $$$$$$$", windowId);
					//ALOGD("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
					//error = MCAS_CasTs ( packet_buf, windowId );
					//ALOGD("result------>  error : %d",error);


					// fldgo note :  한개의 thread 에서 데이터를 얻어와서 두군대의 cas emm 에 전달하기로 한다.

                    error = MCAS_CasTs ( packet_buf, 0 );
                    error = MCAS_CasTs ( packet_buf, 1 );
                    if ( error != 0 ){}
                }

            }

            usleep(1000);
        }

        close(socket);
    //}

    return TRUE;
}


void EmmFiltering ( void *param )
{
	ALOGD("EmmFiltering THREAD START");


	int *windowId = (int*)param;


	while(g_nTaskRun){
		ALOGD( "INFO:EMMDataCheck ( )");
		EMMDataCheck (*windowId);

        if(0 == g_nTaskRun){
        	break;
        }
        usleep( 2000000 );
    }

	ALOGD("EmmFiltering THREAD RETURN");
}


int main_startemm_filtering ( void )
{
	ALOGD("main_startemm_filtering");

	int nRet = -1;

	void* (*pfnThreadFunc)( void*) = (void* (*)(void*)) EmmFiltering;
	g_nTaskRun = 1;

	int thrId;

	pthread_attr_t tattrEmmFiltering;
	nRet = pthread_attr_init(&tattrEmmFiltering);
	nRet = pthread_attr_setdetachstate(&tattrEmmFiltering, PTHREAD_CREATE_DETACHED);
	thrId = pthread_create(&TaskID, &tattrEmmFiltering, pfnThreadFunc, &pWindowId0);

	if (thrId < 0)
	{
		ALOGD( "daemon :EmmFiltering create error" );
		return -1;
	}

	return 0;
	// fldgo note : 굳이 두개의 thread 를 돌릴 필요없다.
	// 한개의 thread 에서 데이터를 얻어와서 두군대의 cas emm 에 전달하면 된다.
	// 따라서 하기 thread 생성은 하지 않는다.

	pthread_attr_t tattrEmmFiltering1;
	nRet = pthread_attr_init(&tattrEmmFiltering1);
	nRet = pthread_attr_setdetachstate(&tattrEmmFiltering1, PTHREAD_CREATE_DETACHED);
	thrId = pthread_create(&TaskID, &tattrEmmFiltering1, pfnThreadFunc, &pWindowId1);

	if (thrId < 0)
	{
		ALOGD( "daemon :EmmFiltering create error" );
		return -1;
	}

    return 0;
}


int main_endemm_filtering ( void )
{

	ALOGD("main_endemm_filtering");

	if(0xFFFFFFFF != TaskID){
		g_nTaskRun = 0;
		//fldgo note : lan 선 단절 또는 sk 망이 아닐경우 recv api 에서 무한 block 되는데 이때 close 를 시켜도 recv api 탈출이 되지 않는다.
		//recv api block 상태.. 즉 thread 가 구동중인 상태에서 main 구문을 탈출하게 되는데.. 정리에 대해 추후 더 고민해 봐야 한다.
		// 우선 join 을 하지 않게 한다.
		//THREAD_JOIN(TaskID, NULL);
		TaskID = 0xFFFFFFFF;
	}
	return 0;
}


// ================================================================================
//                         CAS Service Control Codes
// ================================================================================
static unsigned short PredefinedPmtInfoLength4ServStop[2] = {0};

unsigned short swap16bitNtst(unsigned short nInput)
{
	return (nInput % 256) * 256 + (nInput / 256);
}

void printPMTbuffer(char* buffer,unsigned int len)
{
	if (len < 0 ) return;

	int x=0;

	ALOGD("PMT buffer=%x len=%d",buffer,len);

	for(x=0; x<len; x++)
	{
		printf("%02x ",buffer[x]);
	}
	printf("\n");
}

int stopCasService(int viewID)
{
    int MCASStatusErr = -1;

    if(PredefinedPMTBuffer4ServStop[viewID] == NULL){
        return 0;
    }

    pthread_mutex_lock (&pmt2cas_lock);
    ALOGD("###########################################");
    ALOGD("########  EPG_MSG_PSI_SERVICE_STOP  #######");
    ALOGD("###########################################");
    MCASStatusErr = MCAS_GetStatus(MSG_RESPONSE,
                    EPG_MSG_PSI_SERVICE_STOP,
                    PredefinedPMTBuffer4ServStop[viewID],
                    PredefinedPmtInfoLength4ServStop[viewID]);

    PredefinedPMTBuffer4ServStop[viewID] = NULL;
    gPMTbuffer[viewID] = NULL;
    pthread_mutex_unlock(&pmt2cas_lock);

    return MCASStatusErr;
}


int startCasService(HTuner_IPTV_Info stTuneInfo, int viewID)
{
	int ret_v = -1;

	unsigned char *PredefinedPMTBuffer = NULL;
	unsigned char MCASStatusErr=0;
	long file_length=0;

	unsigned short msglength=0;
	BYTE	gblCAdescCount = 0;
	gCAdes = 0;

	char ip_addr[ 256 ];

	int ch_num = 0;
	HTuner_IPTV_Info h_i;

	strcpy( ip_addr, stTuneInfo.multicast_addr);

	strcpy( h_i.multicast_addr, stTuneInfo.multicast_addr);
	h_i.multicast_port	= stTuneInfo.multicast_port;
	h_i.video_pid		= stTuneInfo.video_pid;
	h_i.audio_pid		= stTuneInfo.audio_pid;
	h_i.pcr_pid 		= stTuneInfo.pcr_pid;
	h_i.video_stream	= stTuneInfo.video_stream;
	h_i.audio_stream	= stTuneInfo.audio_stream;
	h_i.ca_systemid 	= stTuneInfo.ca_systemid;	// CA system ID 0: not exist
	h_i.ca_pid 			= stTuneInfo.ca_pid;		// ecm ID 0: not exist
	h_i.channel_num		= stTuneInfo.channel_num;

	ALOGD( "#################################################" );
	ALOGD( "########    startCasService(view : %d)    #######",viewID );
	ALOGD( "#################################################" );
	ALOGD( "multicast_addr : %s",h_i.multicast_addr);
	ALOGD( "multicast_port : %d",h_i.multicast_port);
	ALOGD( "video_pid : %d",h_i.video_pid);
	ALOGD( "audio_pid : %d",h_i.audio_pid);
	ALOGD( "pcr_pid : %d",h_i.pcr_pid);
	ALOGD( "video_stream : %d",h_i.video_stream);
	ALOGD( "audio_stream : %d",h_i.audio_stream);
	ALOGD( "ca_systemid : %d",h_i.ca_systemid);
	ALOGD( "ca_pid : %d",h_i.ca_pid);
	ALOGD( "channel_num : %d",h_i.channel_num);
	if( stTuneInfo.res == 99 ){
		h_i.res = 0;
	}
	else{
		h_i.res = stTuneInfo.res;
	}

	ch_num = h_i.channel_num;

	SKCAS_SetMCASTChannelAudioPid(viewID, h_i.audio_pid);
	SKCAS_SetMCASTChannelVideoPid(viewID, h_i.video_pid);
	SKCAS_SetCurrentChannelWindowID(viewID);
	SKCAS_SetMulticastServiceSwitchedStatus (viewID, TRUE);
	SKCAS_SetPredefinedPMTChannelZapStatus(viewID, FALSE);

	PredefinedPMTBuffer = (unsigned char *) malloc(24);
	if (PredefinedPMTBuffer != NULL)
	{
		unsigned char * pmt = PredefinedPMTBuffer;
		int msg_length = 2;	//int msg_length = 4;
		unsigned short view_id = 0;	// added for multi CAS
		unsigned short nData = 0;
		unsigned char bData = 0;

		memset(pmt, 0, 24);

		view_id = swap16bitNtst(viewID);
		memcpy(&pmt[msg_length], &view_id, 2);	// added for multi CAS
		msg_length += 2;

		nData = swap16bitNtst(ch_num);
		memcpy(&pmt[msg_length], &nData, 2);
		msg_length += 2;

		if (h_i.ca_systemid != 0){
			bData = 1; //case exist ca system id
		} else {
			bData = 0; // case not exist ca system id
		}

		// pmt data should be writed even if count is 0
		memcpy(&pmt[msg_length], &bData, 1);
		msg_length++;

		// glbdescr
		for (unsigned int j = 0; j < bData; j++) {
			// descriptor_tag
			unsigned char descriptor_tag = 9;	// What for?
			memcpy(&pmt[msg_length], &descriptor_tag, 1);
			msg_length++;

			// descriptor_length
			unsigned char descriptor_length = 4;
			memcpy(&pmt[msg_length], &descriptor_length, 1);
			msg_length++;

			// CA_system_ID
			unsigned short CA_system_ID = swap16bitNtst(h_i.ca_systemid);
			memcpy(&pmt[msg_length], &CA_system_ID, 2);
			msg_length += 2;

			// CA_PID should be or-operated after modified
			unsigned short CA_PID = swap16bitNtst(h_i.ca_pid);
			memcpy(&pmt[msg_length], &CA_PID, 2);
			pmt[msg_length] |= 0xE0;	// 막으면 동작되는 경우도 존재. 이유 모름.
			msg_length += 2;
		}

		// streamcount
		unsigned char streamcount = 2;
		memcpy(&pmt[msg_length], &streamcount, 1);
		msg_length++;

		//	VID
		unsigned short StreamVPID = swap16bitNtst(h_i.video_pid);
		memcpy(&pmt[msg_length], &StreamVPID, 2);
		msg_length += 2;

		// reserved 0xFF
		// byte order is 0xFF, 0x00
		unsigned char reserved = 0xFF;	// 또는 0x00
		memcpy(&pmt[msg_length++], &reserved, 1);
		reserved = 0x00;
		memcpy(&pmt[msg_length++], &reserved, 1);

		// CA_system_ID AID
		unsigned short StreamAPID = swap16bitNtst(h_i.audio_pid);
		memcpy(&pmt[msg_length], &StreamAPID, 2);
		msg_length += 2;

		// reserved 0xFF
		// byte order is 0xFF, 0x00
		reserved = 0xFF;	// 또는 0x00
		memcpy(&pmt[msg_length++], &reserved, 1);
		reserved = 0x00;
		memcpy(&pmt[msg_length++], &reserved, 1);

		file_length = msg_length;

		unsigned short swaped_msg_length = swap16bitNtst(msg_length - 2);
		memcpy(&pmt[0], &swaped_msg_length, 2);

		msglength = *PredefinedPMTBuffer;
		msglength <<= 8;
		msglength |= *(PredefinedPMTBuffer + 1);

		gblCAdescCount = *(PredefinedPMTBuffer + 6);
		gCAdes = gblCAdescCount ;

		// No need to send the "EPG_MSG_PSI_SERVICE_STOP" to CAS library for the bootup case
		if (gBoxBootup[viewID] != TRUE) {
			MCASStatusErr = stopCasService(viewID);
		}

		PredefinedPMTBuffer4ServStop[viewID] = (unsigned char *)malloc(file_length);
		gPMTbuffer[viewID] = PredefinedPMTBuffer4ServStop[viewID];

		if (PredefinedPMTBuffer4ServStop[viewID] == NULL) {
			ALOGD( "WARNING:PredefinedPMTBuffer4ServStop[viewID] == NULL" );
		} else {
			// Store PMT data for Service stop usage
			memcpy(PredefinedPMTBuffer4ServStop[viewID],
					PredefinedPMTBuffer, file_length);
			PredefinedPmtInfoLength4ServStop[viewID] = file_length;
			gPMTbufferLen[viewID] = file_length;
		}

		// CAS 채널에 한해서만 실행.
		if(h_i.ca_pid != 0 && h_i.ca_systemid != 0){
			ALOGD("############################################");
			ALOGD("########  EPG_MSG_PSI_SERVICE_START  #######");
			ALOGD("############################################");
			MCASStatusErr = MCAS_GetStatus(MSG_RESPONSE, EPG_MSG_PSI_SERVICE_START, PredefinedPMTBuffer, file_length);
			// fldgo note : case case service start
			ret_v = 0;
		}else{
			// start 가 안된 경우에는 할당했던 메모리 다시 해제.
			if (PredefinedPMTBuffer4ServStop[viewID] != NULL) {
				free(PredefinedPMTBuffer4ServStop[viewID]);
				PredefinedPMTBuffer4ServStop[viewID] = NULL;
				gPMTbufferLen[viewID] = 0;
				PredefinedPmtInfoLength4ServStop[viewID] = 0;
			}
			free(PredefinedPMTBuffer);
		}

		ALOGD("MCASStatusErr Code : %d !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",(int) MCASStatusErr);

		SKCAS_SetPredefinedPMTChannelZapStatus(viewID, TRUE);
		SKCAS_SetMulticastServiceSwitchedStatus(viewID, FALSE);
		gBoxBootup[viewID] = FALSE;
	}

	return ret_v;
}


#ifdef __cplusplus
}
#endif

class IptvBtvEvent : public IptvTimedEventQueue::Event
{
public:
	IptvBtvEvent(
		IptvSocPlayer *player,
		void (IptvSocPlayer::*method)())
			: mPlayer(player),
			  mMethod(method)
	{

	}

	virtual ~IptvBtvEvent() {}

	virtual void fire(IptvTimedEventQueue *queue, int64_t /* now_us */)
	{
		(mPlayer->*mMethod)();
	}

private:
	IptvSocPlayer 	*mPlayer;
	void (IptvSocPlayer::*mMethod)();

	IptvBtvEvent(const IptvBtvEvent &);
	IptvBtvEvent &operator=(const IptvBtvEvent &);

};


#ifdef USE_RTSP
int RTSPErrorCB (int nErrorCode, char *szIp, int nPort, void *hInstance, int nWillClose)
{
	int nRet;
	IptvSocPlayer *pPlayer = (IptvSocPlayer *)hInstance;

	pPlayer->mQueue.cancelEvent(pPlayer->mRTSPPlayTimeCheckEvent->eventID());
	pPlayer->nSendRTSPADVIMP = 0;

	pPlayer->SendRTSPErrorStatus(nErrorCode, szIp, nPort);

	return 0;
}
#endif

void cc_dtv_data_callback(void* handle, int what, int arg1, int arg2, const char* data){
	dvb_player_t * player = (dvb_player_t*) handle;
	((IptvSocPlayer *)player->pIptvRkPlayer)->onRecvDtvCCData(what, arg1, arg2, data);
}

void dsmcc_data_callback(void *handle, int event, char* path, void *data){
	dvb_player_t * player = (dvb_player_t*) handle;
	((IptvSocPlayer *)player->pIptvRkPlayer)->onRecvDsmccData(event, path, data);
}

void section_filter_callback(const unsigned char *section, const unsigned int length, void *section_user_param)
{
	dvb_player_t * player = (dvb_player_t*)section_user_param;
	((IptvSocPlayer *)player->pIptvRkPlayer)->dataListener_l(SECTION_FILTER_EVENT, player->idx_player, length, (char*)section);
}

// jung2604
void playerEventCallback(AVP_MediaCallbackEvent event, void * data, uint32_t data_size, void * callback_user_param) {
    if(callback_user_param == NULL) {
        ALOGD("[%s] callback_user_param is null", __FUNCTION__);
        return;
    }

    dvb_player_t * player = (dvb_player_t*) callback_user_param;
    //ALOGD("player_callback  event: %d user_param=0x%x, data_size = %d\n", event, callback_user_param, data_size);
    if(player == NULL || !player->isAvpStart) return;

    switch (event) {
        case AVP_FIRST_IFRAME_DISPLAYED:
            ALOGD("[%s] AVP_FIRST_IFRAME_DISPLAYED...", __FUNCTION__);
            if(player->isFirstFrameDisplayed == false) {
	            { // TODO : jung2604 : 20190429 : zapping time check용..
	                if(player->avpInfo.playerConfig.clock_recovery_enable == false /*AVP_IPTV*/) { //iptv일때만..
	                    if ( 0 == clock_gettime( CLOCK_REALTIME, &player->tTempTimeVal)) {
	                        player->tempTimeMs = (player->tTempTimeVal.tv_sec * 1000) + (player->tTempTimeVal.tv_nsec / 1000000);
	                        player->timeCheckFirstIframeDisplayedTime = player->tempTimeMs;
	                        ALOGE("**_** fist iframe displayed : %ld ,start ~ now : %d ms ", player->tempTimeMs, player->tempTimeMs - player->timeCheckStartTime);
	                    }
	                }
	            }

	            player->isFirstFrameDisplayed = true;
#if 0
	            if (player->locator_head != NULL && !player->save_mute_enable ) {
	                int isUnmute = TRUE;
	                // jung2604 : IPTV일경우만, AudioMute처리를 해준다. FILE와 RTSP는 동일하가 DVB_FILE로 설정이 된다
	                //if(player->avpInfo.playerConfig.clock_recovery_enable == true /* != AVP_IPTV*/) {
	                if(player->locator_head->type != DVB_IP ) {
	                    if (player->isRtspSeekMode) { //rtsp seek중일때는...소리를 켜지 않는다.
	                        isUnmute = FALSE;
	                    }
	                }
	                // ALOGD("[%s] : AVP_FIRST_IFRAME_DISPLAYED., type : %d, isRtspSeekMode : %d, isUnmute : %d", __FUNCTION__,
	                //        player->avpInfo.playerConfig.clock_recovery_enable, player->isRtspSeekMode, isUnmute);
	                ALOGD("[%s] : AVP_FIRST_IFRAME_DISPLAYED., type : %d, isRtspSeekMode : %d, isUnmute : %d", __FUNCTION__,
	                        player->locator_head->type, player->isRtspSeekMode, isUnmute);

	                if(isUnmute) {
	                    //usleep(100000);
	                    ST_AudioMute(player, 0); // player->enable_audio
	                }
	            }
#endif
            }

           /* player->player->avpInfo.first_pts = ST_GetTimeStamp(player);

            ALOGD("++player_callback : change first pts : %llu", player->player->avpInfo.first_pts);*/
            break;

        case AVP_FIRST_AUDIO_FRAME_DECODED:
            ALOGD("[%s] AVP_FIRST_AUDIO_FRAME_DECODED...", __FUNCTION__);
            ALOGD("[%s] AVP_FIRST_AUDIO_FRAME_DECODED..., player->isFirstFrameDisplayed:%d", __FUNCTION__, player->isFirstFrameDisplayed);
            if(player->locator_head != NULL) ALOGD("[%s] AVP_FIRST_AUDIO_FRAME_DECODED..., type:%d(%d)", __FUNCTION__, player->locator_head->type, DVB_IP);
            ALOGD("[%s] AVP_FIRST_AUDIO_FRAME_DECODED..., player->save_mute_enable:%d", __FUNCTION__, player->save_mute_enable);
            ALOGD("[%s] AVP_FIRST_AUDIO_FRAME_DECODED..., player->enable_video:%d", __FUNCTION__, player->enable_video);

            if(player->isFirstFrameDisplayed == false && !player->enable_video) {
                    player->isFirstFrameDisplayed = true;
#if 0
                //if(player->avpInfo.playerConfig.mediaType == AVP_IPTV && !player->avpInfo.playerConfig.video_enable)
                if (player->locator_head != NULL && player->locator_head->type == DVB_IP && !player->save_mute_enable ) {
                    ST_AudioMute(player, 0);
                }
#endif
            }
            break;

        case AVP_EOS: //TODO : jung2604 : 여기를 타지 않는다. -> amp_vout_callback 를 탄다
            player->EOFReached = true;
            ALOGD("[%s] AVP_EOS...", __FUNCTION__);
            break;

        case AVP_PICTURE_USER_DATA:
            //ALOGD("[%s] AVP_PICTURE_USER_DATA...", __FUNCTION__);
            if(player->dtvcc != NULL){
                ccx_dtvcc_post_picture_data(player->dtvcc, (void *)data, data_size);
            }
            break;
        default:
//			LOGI("unhandled event: %d\n", event);
            break;
    }
}

BtvMediaPlayerServiceInterface *g_pOwnerService = nullptr;

IptvSocPlayer::IptvSocPlayer()
	: mQueueStarted(false), mQueueSecondStarted(false), mQueueTimeCheckStarted(false)
{
	ALOGD("[%s] called\n[%s]", __FUNCTION__, __FILE__);
	mSocketBufferCheckEvent = std::make_shared<IptvBtvEvent>(this, &IptvSocPlayer::onSocketBufferCheckEvent);//new IptvBtvEvent(this, &IptvSocPlayer::onSocketBufferCheckEvent);
	mFilePlayTimeCheckEvent = std::make_shared<IptvBtvEvent>(this, &IptvSocPlayer::onFilePlayTimeCheckEvent);
	mFileCloseTimeCheckEvent = std::make_shared<IptvBtvEvent>(this, &IptvSocPlayer::onFileCloseTimeCheckEvent);
#ifdef USE_RTSP
	mRTSPPlayTimeCheckEvent = std::make_shared<IptvBtvEvent>(this, &IptvSocPlayer::onRTSPPlayTimeCheckEvent);
#endif
	mAVPTSTimeCheckEvent = std::make_shared<IptvBtvEvent>(this, &IptvSocPlayer::onAVPTSTimeCheckEvent);
	mAVEventCheckEvent = std::make_shared<IptvBtvEvent>(this, &IptvSocPlayer::onAVEventCheckEvent);
#ifdef USE_RTSP
	mConstructCurrentTimeEvent = std::make_shared<IptvBtvEvent>(this, &IptvSocPlayer::onConstructCurrentTimeEvent);
#endif
#if 0
    shared_ptr<IptvBtvEvent> socketBufferCheckEvent(new IptvBtvEvent(this, &IptvSocPlayer::onSocketBufferCheckEvent));
    mSocketBufferCheckEvent = socketBufferCheckEvent;

    shared_ptr<IptvBtvEvent> filePlayTimeCheckEvent(new IptvBtvEvent(this, &IptvSocPlayer::onFilePlayTimeCheckEvent));
    mFilePlayTimeCheckEvent = filePlayTimeCheckEvent;

    shared_ptr<IptvBtvEvent> fileCloseTimeCheckEvent(new IptvBtvEvent(this, &IptvSocPlayer::onFileCloseTimeCheckEvent));
    mFileCloseTimeCheckEvent = fileCloseTimeCheckEvent;

#ifdef USE_RTSP
    shared_ptr<IptvBtvEvent> rTSPPlayTimeCheckEvent(new IptvBtvEvent(this, &IptvSocPlayer::onRTSPPlayTimeCheckEvent));
    mRTSPPlayTimeCheckEvent = rTSPPlayTimeCheckEvent;
#endif

    shared_ptr<IptvBtvEvent> aVPTSTimeCheckEvent(new IptvBtvEvent(this, &IptvSocPlayer::onAVPTSTimeCheckEvent));
    mAVPTSTimeCheckEvent = aVPTSTimeCheckEvent;

    shared_ptr<IptvBtvEvent> aVEventCheckEvent(new IptvBtvEvent(this, &IptvSocPlayer::onAVEventCheckEvent));
    mAVEventCheckEvent = aVEventCheckEvent;

#ifdef USE_RTSP
    shared_ptr<IptvBtvEvent> constructCurrentTimeEvent(new IptvBtvEvent(this, &IptvSocPlayer::onConstructCurrentTimeEvent));
    mConstructCurrentTimeEvent = constructCurrentTimeEvent;
#endif
#endif

    if (!mQueueStarted)
    {
        mQueue.start();
        mQueueStarted = true;
    }

	if (!mQueueSecondStarted)
    {
        mQueueSecond.start();
        mQueueSecondStarted = true;
    }

	if (!mQueueTimeCheckStarted)
    {
        mQueueTimeCheck.start();
        mQueueTimeCheckStarted = true;
    }

	nLastSignalStatus = IPTV_SIGNAL_INIT; // case not initialized
	eDeviceOpenStatus = DEVICE_STATUS_CLOSE; // 초기 close 상태 set
	ePlayerStatus = PLAYER_STATUS_NONE; // 초기 none 상태 set
	ePlayerMode = PLAYER_MODE_NONE; // 초기 none 상태 set

    // jung2604 : 초기화
    memset(&player, 0, sizeof(player));
    player.pIptvRkPlayer = this;
	player.cc_callback = &cc_dtv_data_callback;
    player.dtvcc = NULL;

    player.avpInfo.playerHandle = NULL;
    player.avpInfo.amixerHandle = NULL;
    player.avpInfo.vcompoHandle = NULL;
    player.videoDelayTimeMs = videoDelayTimeMs;
    player.enableGlobalMute = enableGlobalMute;
    player.enableSettingsDolby = enableSettingsDolby;
}

IptvSocPlayer::~IptvSocPlayer()
{
	ALOGD("[%s] called\n[%s]", __FUNCTION__, __FILE__);

#ifdef USE_QI_CALCULATOR
    qi_calculator_term();
#endif
}

#define BTVFRM_CONST_EPG_MSG_MONITOR_ECM "ecm"
#define BTVFRM_CONST_EPG_MSG_MONITOR_EMM = "emm"
#define BTVFRM_CONST_EPG_MSG_ENTITLEMENT_INFO = "entl"
#define BTVFRM_CONST_EPG_MSG_SCARD_INFO = "scard"
#define BTVFRM_CONST_EPG_MSG_SERVICE_INFO = "service"

void caNotify ( byte opcode, MCAS_MSG_EN msg, byte *pvParams, uint32 ulParamLength )
{
    ALOGD("[%s] called[%s], opcode : %d, msg : %d, pvParams:%p, ulParamLength:%d", __FUNCTION__, __FILE__, opcode, msg, pvParams, ulParamLength);

    if(pvParams == nullptr) return;

    if(msg == EPG_MSG_SCARD_INFO) {
        std::string infoData = std::string();
        char buffer[1024];

        scard_info_st *pScard_info_st = (scard_info_st*) pvParams;
/*
        info type : scard   infoData : Subscribed User / KOR[1]
        CSID =  5006915044
        Ver. Scard = 0100, Applet = 0100, Client = 0100 / Card Type = 0
*/
        if(pScard_info_st->result == NO_ERROR) {
            sprintf(buffer, "info type : scard\r\ninfoData : Subscribed User / KOR[1]\r\n");
            infoData += buffer;
            sprintf(buffer, "CSID : %s\r\n", pScard_info_st->bScSerNo );
            infoData += buffer;
            sprintf(buffer, "Ver. Scard = %X, Applet = %X, Client = %X / Card Type = %d\r\n", pScard_info_st->ScVersion,
                            pScard_info_st->AppletVersion, pScard_info_st->AppVersion, pScard_info_st->ScType);
            infoData += buffer;

            ALOGD("[casinfo] %s", infoData.c_str());

            ALOGD("[casinfo]info type : scard infoData  가입유무 : %d", pScard_info_st->subcribe_model);
#if 0
            ALOGD("[casinfo]CSID : %s", pScard_info_st->bScSerNo );
            ALOGD("[casinfo]Ver. Scard = %X, Applet = %X, Client = %X / Card Type = %d", pScard_info_st->ScVersion,
                   pScard_info_st->AppletVersion, pScard_info_st->AppVersion, pScard_info_st->ScType);
#endif

            if(g_pOwnerService != nullptr) g_pOwnerService->onCasInfoEvent(0, infoData);
        }
    } else {
        int16 result = *((int16*)pvParams);
        int16 count = *((int16*)(pvParams+2));

        if(result != NO_ERROR) return;

        std::string infoData = std::string();
        char buffer[1024];

        if(msg == EPG_MSG_SERVICE_INFO) {
            //info type : service   infoData : Service ID [0], Status Code[120], ECM PID[0x00fe]

            service_info_st *pServiceInfoSt = (service_info_st*) (pvParams + 4);

            sprintf(buffer, "info type : service   infoData : ");
            infoData += buffer;

            for(int i = 0 ; i < count ; i++) {
                sprintf(buffer, "Service ID [%d], Status Code[%d], ECM PID[0x%X]\r\n",
                       pServiceInfoSt->service_id, pServiceInfoSt->service_status, pServiceInfoSt->ecm_pid);
                infoData += buffer;
#if 0
                ALOGD("[casinfo]info type : service   infoData : Service ID [%d], Status Code[%d], ECM PID[0x%X] ",
                       pServiceInfoSt->service_id, pServiceInfoSt->service_status, pServiceInfoSt->ecm_pid);

                ALOGD("[casinfo] EPG_MSG_SERVICE_INFO, pServiceInfoSt->view_id:%d, pServiceInfoSt->result:%d, pServiceInfoSt->ecm_pid:%d",
                       pServiceInfoSt->view_id, pServiceInfoSt->result, pServiceInfoSt->ecm_pid);

                for(int k = 0 ; k < 3 ; k++) {
                    ALOGD("[casinfo] ==> EPG_MSG_SERVICE_INFO , pServiceInfoSt->es_pids->es_ecm_pid:%d, pServiceInfoSt->es_pids->es_pid:%d",
                           pServiceInfoSt->es_pids->es_ecm_pid, pServiceInfoSt->es_pids->es_pid);
                }
#endif
                pServiceInfoSt++;
            }

            if(g_pOwnerService != nullptr) g_pOwnerService->onCasInfoEvent(1, infoData);

            ALOGD("[casinfo] %s", infoData.c_str());
        } else if(msg == EPG_MSG_SS_ENTL) {
            //info type : entl   infoData : Entl ID[197], Start[2020/08/25, 16:21:01], End[2021/08/20, 16:21:01]
            //Entl ID[199], Start[2020/08/25, 16:21:01], End[2021/08/20, 16:21:01]

            entl_info_st *pEntl_info_st = (entl_info_st*) (pvParams + 4);

            sprintf(buffer, "info type : entl   infoData : ");
            infoData += buffer;

            for(int i = 0 ; i < count ; i++) {
                sprintf(buffer, "Entl ID[%d], Start[%4d/%02d/%02d, %02d:%02d:%02d],",
                        pEntl_info_st->entl_id,
                        pEntl_info_st->start_time.nYear, pEntl_info_st->start_time.nMonth, pEntl_info_st->start_time.nDay,
                        pEntl_info_st->start_time.nHour, pEntl_info_st->start_time.nMin, pEntl_info_st->start_time.nSec);
                infoData += buffer;

                sprintf(buffer, " End[%4d/%02d/%02d, %02d:%02d:%02d]\r\n",
                        pEntl_info_st->end_time.nYear, pEntl_info_st->end_time.nMonth, pEntl_info_st->end_time.nDay,
                        pEntl_info_st->end_time.nHour, pEntl_info_st->end_time.nMin, pEntl_info_st->end_time.nSec);
                infoData += buffer;
#if 0
                ALOGD("[casinfo]Entitlement ID : %d", pEntl_info_st->entl_id);

                ALOGD("[casinfo]String time : %d년 %d월 %d일 %d시 %d분 %d초", pEntl_info_st->start_time.nYear, pEntl_info_st->start_time.nMonth, pEntl_info_st->start_time.nDay,
                       pEntl_info_st->start_time.nHour, pEntl_info_st->start_time.nMin, pEntl_info_st->start_time.nSec);

                ALOGD("[casinfo]End time : %d년 %d월 %d일 %d시 %d분 %d초", pEntl_info_st->end_time.nYear, pEntl_info_st->end_time.nMonth, pEntl_info_st->end_time.nDay,
                       pEntl_info_st->end_time.nHour, pEntl_info_st->end_time.nMin, pEntl_info_st->end_time.nSec);
#endif
                pEntl_info_st++;
            }

            if(g_pOwnerService != nullptr) g_pOwnerService->onCasInfoEvent(2, infoData);

            ALOGD("[casinfo] %s", infoData.c_str());
        }
    }

	return;
}

int init_skcas_lib( void )
{
    unsigned char 	errr = 0;


    MCAS_NotificationRegister(caNotify);

    {

        struct timeval time;
        long lStart, lEnd;
        gettimeofday(&time, NULL);
    }

    LOG_TIME_STARTV("MCAS_Init");
    errr = MCAS_Init ( MCAS_CRITICAL );
    LOG_TIME_END("MCAS_Init errr(%d)", errr);
    if ( NO_ERROR == errr ){
		ALOGD("[SUCCESS] MCAS INIT SUCCESS!!!!!");
    }
    else{
		ALOGD("[ERROR] MCAS INIT ERROR!!!!!");
    }

    gBoxBootup[0] = TRUE;
    gBoxBootup[1] = TRUE;

    main_startemm_filtering();
    return 0;
}


int uninit_skcas_lib( void )
{
	main_endemm_filtering ();
	MCAS_Destroy();
	return 0;
}


int _Check_CertVerify(int req_date)
{
	int 	nRet = 0;
	char 	STBID[64];
	struct  timeval cur_time;
	struct  tm *time_v = NULL;
	int 	time_val = 0;
	int 	req_time = 0;

	memset( STBID, 0, sizeof( STBID ) );
	nRet = get_systemproperty(PROPERTY__STB_ID, STBID, sizeof( STBID ));

	if(0 != nRet || strlen(STBID) <= 16) {
		ALOGD("[ERROR]STBID GET ERROR");
		return -1;
	}

	gettimeofday( &cur_time, NULL );
	time_v = localtime( &cur_time.tv_sec );

	if( time_v->tm_year + 1900 < 2008 ){
		ALOGD("[ERROR]DATE ERROR");
		return -2;
	}

	time_val = (int) time( ( time_t* )&time_val );
	req_time = time_val + ( 3600 * 24 * req_date );

    ALOGD("++[_Check_CertVerify] SXCW_dci_CertVerify");
	//CIR cirRet = dci_CertVerify(0, 0);
    INT32 cirRet = SXCW_dci_CertVerify(0, 0);
	if( SXC_CA_CIR_NOT_FOUND == cirRet || SXC_CA_CIR_NOT_EXIST == cirRet ){
		ALOGD("[ERROR]NOT FOUND CERTIFICATE");
		return -3;
	}

    ALOGD("++[_Check_CertVerify] SXCW_dci_CertVerify");
	LOG_TIME_STARTV("SXCW_dci_CertVerify(req_time(%d), CIC_CERT_VERIFY_SIGN) req_date(%d)", req_time, req_date);
    cirRet = SXCW_dci_CertVerify( (INT64)req_time, CIC_CERT_VERIFY_SIGN);
	LOG_TIME_END("SXCW_dci_CertVerify() nRet(%d)", nRet);
	if(SXC_CA_CIR_OK != cirRet) {
        CHAR* errmessage  = SXCW_dci_DirectGetErrorMessage();
        ALOGD("[ERROR]CERTIFICATE VERIFY ERROR [%s]", errmessage);
        SXCW_dci_DirectClearErrorMessage();
        return -4;
    }

    ALOGD("++[_Check_CertVerify] SUCCESS");
	return 0;
}


int RegisterSTB()
{
    ALOGD("++[RegisterSTB]RegisterSTB");
	int 	nRet = 0;
	int 	nTryCnt = 0;

	LOG_TIME_STARTV("process_reg_confirm() nTryCnt(%d)", nTryCnt);
	nRet = process_reg_confirm();
	LOG_TIME_END("process_reg_confirm() nRet(%d)", nRet);
	if( NO_ERROR  == nRet ){
		ALOGD("[SUCCESS] Certification receive success!!!!!");
	}
	else{
		ALOGD("[ERROR] Certification receive ERROR!!!!!");
	}

	return nRet;
}


int AuthSTB()
{
	int 	nRet = 0;
	int 	nTryCnt = 0;

	LOG_TIME_STARTV("process_auth_confirm() nTryCnt(%d)", nTryCnt);
	nRet = process_auth_confirm();
	LOG_TIME_END("process_auth_confirm() nRet(%d)", nRet);
	if( NO_ERROR  == nRet ){

		ALOGD("[SUCCESS] AuthSTB receive success!!!!! : %d",nRet);

	}
	else{

		ALOGD("[ERROR] AuthSTB receive ERROR!!!!! : %d",nRet);

	}

	return nRet;
}

int32_t IptvSocPlayer::getAuthChecked() {
	ALOGD("getAuthChecked() : %d",gAuthChecked);
    return gAuthChecked;
}

int32_t IptvSocPlayer::CertCheck()
{
	int 	nRet = 0;
	int		nTry = 1;	//jung2604 : 0315 : 인증한번만 태우자..

	// 인증서 체크루틴
	// 1. stbid 오류가 있거나.
	// 2. 인증서가 없거나.
	// 3. 인증서 기간이 만료 되거나.
	// 4. 인증서가 유효하지 않은지
	if (_Check_CertVerify(90) != 0)
	{
		// 조건에 합당하지 않으면
		RegisterSTB();
#ifdef USE_CAS_DRM
		WISE_SKDRM_CleanupROStorage();
#endif
	}

	// WISE_SKDRM_Init(); 이유는 알수 없지만 오동작한다. 추후 해결 우선은 rtsp open 에서 매번 하도록 한다.
	// digicap 에서 WISE_SKDRM_Init api 는 초기 한번 호출말고
	// drm contents 를 실제 decrypt 하기전 매번 호출하는게 좋다고 guide 함
	// 따라서 rtsp open 에서 매번 하고 있는 현재 루틴을 그대로 적용.

	// 단말 인증 프로세스 시작.

    ALOGD("++[CertCheck] AuthSTB");
	nRet = AuthSTB();

	if (nRet == 10) { //ERR_STB_REG_CMD : H/E Failed
		ALOGD("#######################################################");
		ALOGD("#########  CAS H/E Authentification Failed  ###########");
		ALOGD("#######################################################");
    }

	while (nRet == 11 || nRet == 12) {
		// folder delete
		ALOGD("#######################################################");
		ALOGD("#########       Delete and process again      #########");
		ALOGD("#######################################################");

		//	/Root Folder/iptv_cas / *.dcs 삭제.
		//ALOGD("rm -r /data/user/0/com.skb.framework.myapplication/files/btf/btvmedia/tmp/iptv_cas/*.dcs");
		//system("rm -r /data/user/0/com.skb.framework.myapplication/files/btf/btvmedia/tmp/iptv_cas/*.dcs");

		ALOGD("%s", iptv_cas_tmp_all_dcs_rm);
		system(iptv_cas_tmp_all_dcs_rm);
		//	/Root Folder/내의 3개 파일(stb_id.txt, rse_id.bin, stb_info.bin) 삭제.
		//ALOGD("rm -r /data/user/0/com.skb.framework.myapplication/files/btf/btvmedia/cas/stb_id.txt");
		//system("rm -r /data/user/0/com.skb.framework.myapplication/files/btf/btvmedia/cas/stb_id.txt");
		ALOGD("%s", iptv_cas_stbid_txt_rm);
		system(iptv_cas_stbid_txt_rm);
		//ALOGD("rm -r /data/user/0/com.skb.framework.myapplication/files/btf/btvmedia/cas/rse_id.bin");
		//system("rm -r /data/user/0/com.skb.framework.myapplication/files/btf/btvmedia/cas/rse_id.bin");
		ALOGD("%s", iptv_cas_resid_bin_rm);
		system(iptv_cas_resid_bin_rm);
		//ALOGD("rm -r /data/user/0/com.skb.framework.myapplication/files/btf/btvmedia/cas/stb_info.bin");
		//system("rm -r /data/user/0/com.skb.framework.myapplication/files/btf/btvmedia/cas/stb_info.bin");
		ALOGD("%s", iptv_cas_stbinfo_bin_rm);
		system(iptv_cas_stbinfo_bin_rm);
		//	/ Root Folder/Certificates 삭제.
		//ALOGD("rm -r /data/user/0/com.skb.framework.myapplication/files/btf/btvmedia/cas/Certificates");
		//system("rm -r /data/user/0/com.skb.framework.myapplication/files/btf/btvmedia/cas/Certificates");
		ALOGD("%s", iptv_cas_certificates_rm);
		system(iptv_cas_certificates_rm);

		RegisterSTB();

		nRet = AuthSTB();

		if(--nTry <= 0) break;
	}

	// auth 동작 성공
    if (nRet == 0)
	{
		gAuthChecked = 1;
	}

	return TRUE;
}

int32 SectionFilterImpl ( PidType type, uint32 pid, BYTE *buf, int viewID )
{
   int  error = 0;
   if ( type == ECMPID ){
       // Send ECM data to CAS library
       //error = MCAS_CasTs (buf);
       error = MCAS_CasTs (buf, viewID);
       if ( error != 0 ){
//                     ALOGD( "ERROR:SectionFilterImpl MCAS_CasTs error:%d" , error);
       } else{
                    //ALOGD("MCAS_CasTS Success!!!!!!!!!!!!!!!!");
             }

   }
   return 1;
}



int DemuxCheckTS( SWDemux *demux, int pos, unsigned char *InputBuffer )
{
	    int SyncbyteCount = 0;
	    unsigned char   ScrStatus =0;
	    unsigned char   *buf= 0;
	    unsigned char   *_tmp =0;
	    int temp_pos = 0;

	    temp_pos = pos;
	    buf = InputBuffer;

	    do {
	        if ( buf[pos] == TS_SYNC_BYTE )
	        {
	            if ((pos+TS_PACKET_LEN < demux->inBuffer.ep && buf[pos+TS_PACKET_LEN] == TS_SYNC_BYTE) &&
	                    (pos+2*TS_PACKET_LEN < demux->inBuffer.ep && buf[pos+2*TS_PACKET_LEN] == TS_SYNC_BYTE) &&
	                    (pos+3*TS_PACKET_LEN < demux->inBuffer.ep && buf[pos+3*TS_PACKET_LEN] == TS_SYNC_BYTE) &&
	                    (pos+4*TS_PACKET_LEN < demux->inBuffer.ep && buf[pos+4*TS_PACKET_LEN] == TS_SYNC_BYTE) &&
	                    (pos+5*TS_PACKET_LEN < demux->inBuffer.ep && buf[pos+5*TS_PACKET_LEN] == TS_SYNC_BYTE) &&
	                    (pos+6*TS_PACKET_LEN < demux->inBuffer.ep && buf[pos+6*TS_PACKET_LEN] == TS_SYNC_BYTE))
	            {
	                demux->TSFound = 1;

	                // SAK - 08June2009 - Do memcpy only when the "pos" is changed
	                if ( temp_pos != pos ){
	                    _tmp = (unsigned char*) malloc( sizeof(char) * (demux->inBuffer.ep - pos));
	                    if ( _tmp == NULL ) {
	                    	ALOGD("ERROR:_tmp == NULL");
	                        return 0;
	                    }
	                    memcpy( _tmp, InputBuffer+pos, demux->inBuffer.len - pos);
	                    memcpy( InputBuffer, _tmp, demux->inBuffer.len - pos);
	                    free( _tmp);
	                    _tmp = NULL;
	                }

	                demux->inBuffer.ep -= pos;
	                demux->inBuffer.cp = 0;
	                demux->inBuffer.len = demux->inBuffer.ep+1;
	                break;
	            }
	            else{
	                SyncbyteCount++;
	                demux->inBuffer.cp = -1;
	                demux->TSFound = 0;
	            }
	        }
	        else{
	            SyncbyteCount++;
	            ScrStatus = buf[pos+3] & 0xC0;
	            demux->inBuffer.cp = -1;
	            demux->TSFound = 0;
	        }
#if 1
	        if ( SyncbyteCount >= 100 ){
	            SyncbyteCount = 0;
			    break;
	        }
#endif
	    } while( ++pos < demux->inBuffer.ep && !demux->TSFound);


	    return demux->inBuffer.cp;
}

int DemuxParseMulti ( void *buffer, int size, int viewID)
{
	unsigned int iWindowID = viewID;

	int             cp = 0;
	unsigned int   pid = 0;
	unsigned int   scrambled_bit = 0;

	unsigned char   *buf = 0;
	int                    NoOfECMPackets = 0;
	int             NoOfAVPackets = 0;
	int             NoOfOtherPIDPackets = 0;
	int                        error = 0;
	char                   SyncByteFailCheckFlag = FALSE;
	static SWDemux  demux[2];

	if (buffer == NULL) {
		ALOGD( "ERROR:buffer == NULL");
		return FALSE;
	}

#if FEATURE_ALIGN_188
	    if(size%188 != 0){
	    	return 0;
	    }
#endif // FEATURE_ALIGN_188

	    if(size%1328 == 0 ){
	    	int cnt = 0;
	    	int i;
	    	cnt = size/1328;
	    	for(i = 0;i < cnt ; i++){
	    		memmove((unsigned char*)buffer+(1316*i), ((unsigned char*)buffer+((1328*i)+12)), 1316);
	    	}

	    	if(cnt > 0 ){
	    		size = 1316 * cnt;
	    		memset((unsigned char*)buffer+size,0,12*cnt);
	    	}
	    }

#if 0
	    if (buffer == NULL) {
	    	ALOGD( "ERROR:buffer == NULL");
	    	return FALSE;
	    }
#endif

//	checkAndSaveTS(buffer, size, viewID);
//	return size;

	// SAK - 07May2009 - Moved the SWDemux handling here, to avoid module(MediaHAL) dependancy
	memset(&demux[iWindowID], 0, sizeof(SWDemux));
	if ( SKCAS_GetECMCallbackSetByCAS (iWindowID) == TRUE ){ // ECMCallback was already set by filter_set_pid()
		demux[iWindowID].pidTab.ecmpid = SKCAS_GetECMPidSetByCAS (iWindowID);     // ECMPid was already set by filter_set_pid()
		demux[iWindowID].ECMFilterCB = SectionFilterImpl;
		//ALOGD("EEEEECCCCCCCCCCMMMMMMMMMMM  Pid : %d", SKCAS_GetECMPidSetByCAS(iWindowID));
	}
	else{
		// SAK - added instead of doing in filter_unset_pid
		demux[iWindowID].pidTab.ecmpid = 0x1fff;
		demux[iWindowID].ECMFilterCB    = NULL;
	}

	demux[iWindowID].pidTab.audiopid  = SKCAS_GetMCASTChannelAudioPid(iWindowID); // SKCAS_GetAppParamsAudioPid (iWindowID)   // was set in we_broadcast_handle
	demux[iWindowID].pidTab.videopid  = SKCAS_GetMCASTChannelVideoPid(iWindowID); // SKCAS_GetAppParamsVideoPid (iWindowID)   // was set in we_broadcast_handle
	demux[iWindowID].AVFilterCB              = SectionFilterImpl;
	demux[iWindowID].TSFound                 = 0;
	demux[iWindowID].inBuffer.cp             = demux[iWindowID].inBuffer.sp = 0;
	demux[iWindowID].inBuffer.len            = size;
	demux[iWindowID].inBuffer.ep             = demux[iWindowID].inBuffer.len-1;
	demux[iWindowID].inBuffer.cp             = DemuxCheckTS( &demux[iWindowID], demux[iWindowID].inBuffer.cp, (unsigned char *)buffer );

	buf = (unsigned char *)buffer;

	do {
		cp = demux[iWindowID].inBuffer.cp;

	        if (cp < 0){
//	        	ALOGD( "if (cp < 0)");
	        	usleep(20000);
	        	break;
	        }

	        if( buf[cp] != TS_SYNC_BYTE ){
	        	SyncByteFailCheckFlag = TRUE;
	        }

		// Check whether transport error indicator is set, if so then just skip the packet
		if( buf[cp+1] & 0x80){
			demux[iWindowID].inBuffer.cp += TS_PACKET_LEN;
			continue;
		}


        // Also check for PUSI (Payload Unit Start Indicator) is set to 1 or not for Audio and
        // Video pid(Check this, if and only if the scramble bit is set.
        // That is for non scrambled packet, no need to pass the TS to descrambleTS() fn).
        // If set 1, then we must all the TP's to descrambleTS() until the next PUSI is set 1 in the transport packet
        //

        // Retrieve the PID from transport packet
        pid = (buf[cp+1] & 0x1F) << 8;

        //ALOGD("pid from tansport packet : %d",pid);

        pid |= (buf[cp+2]);

        //ALOGD("pid(mask) from tansport packet : %d",pid);

        // Initialise the scramble bit for each packet
        scrambled_bit = 0;

        // Check for the scrambled bit value other than 0 to identify scrambled data, 0 denotes FTA
        if( ((buf[cp+3] & 0xC0) != 0) && ((buf[cp+3] & 0xC0) != 1)){
            scrambled_bit = 1;

        }

        //ALOGD("scrambled_bit : %d",scrambled_bit);

		if( demux[iWindowID].AVFilterCB && (pid == demux[iWindowID].pidTab.audiopid || pid == demux[iWindowID].pidTab.videopid) ){
			//ALOGD("This is AVPackets(%d)",NoOfAVPackets);
			NoOfAVPackets++;
		}
		else if ( demux[iWindowID].ECMFilterCB && pid == demux[iWindowID].pidTab.ecmpid){
			//ALOGD("This is ECMPackets(%d)",NoOfECMPackets);
			//demux.ECMFilterCB( ECMPID, pid, &buf[cp] );
			demux[iWindowID].ECMFilterCB( ECMPID, pid, &buf[cp], iWindowID);
			NoOfECMPackets++;
		}
		else{
			//ALOGD("This is OtherPIDPackets(%d)",NoOfOtherPIDPackets);
			NoOfOtherPIDPackets++;
		}

		demux[iWindowID].inBuffer.cp += TS_PACKET_LEN;
	} while( (demux[iWindowID].inBuffer.cp + TS_PACKET_LEN) < demux[iWindowID].inBuffer.ep );

	if (SyncByteFailCheckFlag == FALSE){
		if ( buffer ) {
			// Copy the descramble buffer data to HW demux buffer
            error = MCAS_DescrambleTS (( unsigned char *)buffer, (unsigned char *)buffer, size, iWindowID);

            if ( error != 0 ){
//					ALOGD("MCAS_DescrambleTS ERROR:error != 0");
                return -1;
            }
        }
        else{
            ALOGD("ERROR:buffer==NULL");
        }
    }
    return size;
}

#include <dirent.h>
#include <sys/stat.h>
// foder 가 존재하고 파일도 존재하지만 파일 size 가0일경우 에러(-1)리턴한다.
// 이와 같이 한 이유는 cas 와 drm 쪽에서 오류가 발생하기 때문이다.
int checkFolderFileSize(char *szBufFolder)
{
	struct dirent* ent ;
	DIR* dir ;
	int len = 0;
	struct stat st;
	char szFileName[512] = {0,};
	
	ALOGD("checkFolderFileSize szBufFolder == %s", szBufFolder);

	dir = opendir(szBufFolder) ;
	if ( NULL == dir )
	{
        ALOGD("checkFolderFileSize NULL == dir");
        return 0;
	}

	ent = readdir(dir) ;
	len = 0;

    if ( NULL == ent ) {
        ALOGD("checkFolderFileSize NULL == ent");
        return 0;
    }

	while ( NULL != ent )
	{
		if(ent->d_type == DT_REG)
		{
			sprintf(szFileName, "%s/%s", szBufFolder, ent->d_name);
			stat( szFileName, &st );

			closedir(dir);

            ALOGD("checkFolderFileSize %s -> size: %d", ent->d_name, st.st_size);
			if (st.st_size == 0)
				return -1;
			else
				return st.st_size;
		}

		ent = readdir(dir) ;
	}

	closedir(dir);
    ALOGD("checkFolderFileSize not file");

	return 0;
}

// jung2604
//#include <alsa/asoundlib.h>

//static int get_ctl_enum_item_index(snd_ctl_t *handle, snd_ctl_elem_info_t *info, char *pType)
static int get_ctl_enum_item_index(void *handle, void *info, char *pType)
{
#if 0 // TODO : jung2604 : alsa일단 주석처리
	int items, i, len;
	const char *name;

	items = snd_ctl_elem_info_get_items(info);
	if (items <= 0)
		return -1;

	for (i = 0; i < items; i++) {
		snd_ctl_elem_info_set_item(info, i);
		if (snd_ctl_elem_info(handle, info) < 0)
			return -1;
		name = snd_ctl_elem_info_get_item_name(info);
		ALOGD("%s %d name[%s] ptr[%s]\n", __func__, __LINE__, name, pType);
		len = strlen(name);
		if (! strncmp(name, pType, len)) {
			printf("%s %d i[%d]\n", __func__, __LINE__, i);
			return i;
		}
	}
#endif
	return -1;
}

//#define KEC_ENABLE_REFERENCE_KEY	"vendor.skb.btv.manufacturer.kec"
#define REFERENCE_KEC_ENABLE_DEFAULT_STR "0"
void getKecModeSettings(dvb_player_t *pPlayer) {
    if(pPlayer == NULL) return;

    char retBuf[128] = {0};

    if(get_systemproperty(KEC_ENABLE_REFERENCE_KEY, retBuf, 128) != 0 || strcmp(retBuf, "-1") == 0) {
        ccx_dtvcc_log("+============================+\n");
        ccx_dtvcc_log("|  property_set : %s\n", KEC_ENABLE_REFERENCE_KEY);
        ccx_dtvcc_log("+============================+\n" );
        set_systemproperty(KEC_ENABLE_REFERENCE_KEY, REFERENCE_KEC_ENABLE_DEFAULT_STR, 1);
        pPlayer->kec_mode = 0;
    }else{
        pPlayer->kec_mode = atoi(retBuf);
    }
}

int32_t IptvSocPlayer::initCheck(BtvMediaPlayerServiceInterface *pOwnerService, int deviceID)
{
	ALOGD("[%s] called", __FUNCTION__);

    g_pOwnerService = pOwnerService;

	setDeviceID(deviceID);

	// jitter value 100으로 맞춘다.
	ST_InitPlayer(&player, deviceID, 100, 0);

	char dataPath[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
	sprintf(iptv_cas_tmp_all_dcs_rm, "rm -r %s%s", dataPath, IPTV_CAS_TEMP_ALL_DCS_RM);
	sprintf(iptv_cas_stbid_txt_rm, "rm -r %s%s", dataPath, IPTV_CAS_STBID_TXT_RM);
	sprintf(iptv_cas_resid_bin_rm, "rm -r %s%s", dataPath, IPTV_CAS_RSEID_BIN_RM);
	sprintf(iptv_cas_stbinfo_bin_rm, "rm -r %s%s", dataPath, IPTV_CAS_STBINFO_BIN_RM);
	sprintf(iptv_cas_certificates_rm, "rm -r %s%s", dataPath, IPTV_CAS_CERTIFICATES_RM);
	sprintf(iptv_cas_drm_folder_rm, "rm -r %s%s", dataPath, IPTV_CAS_DRM_FOLDER_RM);
	sprintf(iptv_cas_certificates_user_100, "%s%s", dataPath, IPTV_CAS_CERTIFICATES_USER_100);

	// 부팅후 초기화 루틴은 deviceid ==0 일때 로 가정한다.
	if (deviceID == 0)
	{
		// 최초 구동시 특정 file size 가 0 일경우 cas library 에서 죽는다.
		// 체크후 해당 폴더 자체를 삭제한다.
		//if (checkFolderFileSize("/data/user/0/com.skb.framework.myapplication/btf/btvmedia/cas/Certificates/user/1000") == -1)
		if (checkFolderFileSize(iptv_cas_certificates_user_100) == -1)
		{
			ALOGD("[FATAL]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			ALOGD("[FATAL]!!!!!!!Certificate file size is 0 !!!!!!!!!!!!!!!!");
			ALOGD("[FATAL]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

			//system("rm -r /data/user/0/com.skb.framework.myapplication/btf/btvmedia/tmp/iptv_cas/*.dcs");
			//system("rm -r /data/user/0/com.skb.framework.myapplication/btf/btvmedia/cas/stb_id.txt");
			//system("rm -r /data/user/0/com.skb.framework.myapplication/btf/btvmedia/cas/rse_id.bin");
			//system("rm -r /data/user/0/com.skb.framework.myapplication/btf/btvmedia/cas/stb_info.bin");
			//system("rm -r /data/user/0/com.skb.framework.myapplication/btf/btvmedia/cas/Certificates");
			system(iptv_cas_tmp_all_dcs_rm);
			system(iptv_cas_stbid_txt_rm);
			system(iptv_cas_resid_bin_rm);
			system(iptv_cas_stbinfo_bin_rm);
			system(iptv_cas_certificates_rm);
		}

		// 특정상황의 경우 DRM 폴더의 특정 파일이 0 일경우 DRM decrypt 가 되지 않는다.
		// 해당 폴더를 깨끗이 삭제한다.
		//system( "rm -r /data/user/0/com.skb.framework.myapplication/btf/btvmedia/drm" );
		system( iptv_cas_drm_folder_rm );

		// Excute qi agent here.
#ifdef USE_QI_CALCULATOR
		qi_calculator_init();
#endif

		// 최초 CAS library 를 초기화 한다.
		init_skcas_lib();

#ifdef ENABLE_DSMCC_LOGIC
		// dsmcc initialize
		player.dsmcc = dsmcc_init(&player, &dsmcc_data_callback);
#endif
	}

#ifdef USE_RTSP
	// 각 device 마다 100000의 gap 을 둔다.
	nRTSPSessionID = deviceID * 100000;
#endif

	return IPTV_ERR_SUCCESS;
}

int32_t IptvSocPlayer::open(const char* dataXML)
{
	// 전달인자 틀리므로 이렇게 사용하지 않는다.
	ALOGD("open API Not used just to make abstract function!!!!!");

	return IPTV_ERR_SUCCESS;
}

int32_t IptvSocPlayer::tuneTV(const char* dataXML)
{
	// 전달인자 틀리므로 이렇게 사용하지 않는다.
	ALOGD("tuneTV API Not used just to make abstract function!!!!!");

	return IPTV_ERR_SUCCESS;
}

int32_t IptvSocPlayer::bindingFilter(const char* dataXML)
{
	// 전달인자 틀리므로 이렇게 사용하지 않는다.
	ALOGD("tuneTV API Not used just to make abstract function!!!!!");

	return IPTV_ERR_SUCCESS;
}

int32_t IptvSocPlayer::releaseFilter(const char* dataXML)
{
	// 전달인자 틀리므로 이렇게 사용하지 않는다.
	ALOGD("tuneTV API Not used just to make abstract function!!!!!");

	return IPTV_ERR_SUCCESS;
}

int32_t IptvSocPlayer::seek(const char* dataXML, int flag)
{
	// 전달인자 틀리므로 이렇게 사용하지 않는다.
	ALOGD("seek API Not used just to make abstract function!!!!!");

	return IPTV_ERR_SUCCESS;
}

int32_t IptvSocPlayer::pauseAt(const char* dataXML)
{
    // 전달인자 틀리므로 이렇게 사용하지 않는다.
    ALOGD("seek API Not used just to make abstract function!!!!!");

    return IPTV_ERR_SUCCESS;
}

int32_t IptvSocPlayer::trick(const char* dataXML)
{
	// 전달인자 틀리므로 이렇게 사용하지 않는다.
	ALOGD("trick API Not used just to make abstract function!!!!!");

	return IPTV_ERR_SUCCESS;
}


int32_t IptvSocPlayer::closeTV()
{
	ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);

	std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");

	int nRet;
	int nCasRet;

	mSocketBufferCheckEventStop = true;
	mQueue.cancelEvent(mSocketBufferCheckEvent->eventID());
    player.nPollingRunFlag = 0;
	mQueue.cancelEvent(mAVPTSTimeCheckEvent->eventID());
	mQueue.cancelAllEvent();
	nLastSignalStatus = IPTV_SIGNAL_INIT;

	// device 가 close 상태라면
	if (InGetDeviceOpenStatus() == DEVICE_STATUS_CLOSE)
	{
		// 정상 리턴한다.
		ALOGD("[WARNING] DEVICE IS CLOSED. WILL RETURN OK");
		return IPTV_ERR_SUCCESS;
	}

	// 현재 play mode 를 체크해서
	if (InGetPlayerMode() != PLAYER_MODE_IPTV)
	{
		// IPTV 가 아니면 에러 리턴한다.
		ALOGD("[WARNING] NOT IPTV. WILL RETURN ERROR");
		return IPTV_ERR_INVALIDSTATUS;
	}

	// 현재 play status 를 체크해서
	if (InGetPlayerStatus() == PLAYER_STATUS_NONE )
	{
		// None 이면 정상 리턴한다.
		ALOGD("[WARNING] IPTV IS Stopped. RETURN OK");
		return IPTV_ERR_SUCCESS;
	}

	ALOGD("[DEBUG]Stop TV Action will doing!!!");

       if (getDeviceID() == 0)
       {
    	   nCasRet = stopCasService(0);
       }
       else if(getDeviceID() == 1)
       {
    	   nCasRet = stopCasService(1);
       }


    // jung2604 : 종료할때는 무조건..lastframe을 없애준다...
    player.avpInfo.playerConfig.keep_last_frame = false;
    if(player.idx_player == 0) {
        set_systemproperty(PROPERTY__LASTFRAME, "0", 1);
    }

	LOG_TIME_STARTV("ST_CloseChannel [%d]", player.idx_player);
	nRet = ST_CloseChannel(&player, &locator);
	LOG_TIME_END("ST_CloseChannel nRet(%d)", nRet);

	switch(nRet)
	{
	case 0:
		ALOGD("[SUCCESS] ST_CloseChannel !!!!");
		break;
	case -1:
		ALOGD("[ERROR] dvb_tuner_unlock!!!!");
		goto error;
	case -2:
		ALOGD("[ERROR] dvb_clear_av_buffer!!!!");
		goto error;
	case -3:
		ALOGD("[ERROR] dvb_device_close!!!!");
		goto error;
	default:
		ALOGD("[ERROR] unknown ERROR [%d]!!!!", nRet);
		goto error;
	}

	InSetPlayerMode(PLAYER_MODE_NONE);
	InSetPlayerStatus(PLAYER_STATUS_NONE);
	InSetDeviceOpenStatus(DEVICE_STATUS_CLOSE);
    #ifdef USE_MLR
    set_first_start(0);
    #endif
	ALOGD("[SUCCESS]close TV Action done!!!");

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return IPTV_ERR_SUCCESS;

error:
	InSetPlayerMode(PLAYER_MODE_NONE);
	InSetPlayerStatus(PLAYER_STATUS_NONE);
	InSetDeviceOpenStatus(DEVICE_STATUS_CLOSE);

	ALOGD("[FAIL]close TV Action done!!!");

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return IPTV_ERR_ERROR;
}

int32_t IptvSocPlayer::changeAudioChannel(const char* dataXML)
{
	ALOGD("changeAudioChannel API Not used just to make abstract function!!!!!");

	return IPTV_ERR_SUCCESS;
}

int32_t IptvSocPlayer::changeAudioOnOff(const char* dataXML)
{
	ALOGD("changeAudioOnOff API Not used just to make abstract function!!!!!");

	return IPTV_ERR_SUCCESS;
}


int32_t IptvSocPlayer::getPlayerMode()
{
	ALOGD("[%s] called", __FUNCTION__);

	return InGetPlayerMode();
}

int32_t IptvSocPlayer::getPlayerStatus()
{
	ALOGD("[%s] called", __FUNCTION__);

	return InGetPlayerStatus();
}

int32_t IptvSocPlayer::getCurrentPosition()
{
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);

	std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");

#ifdef USE_RTSP
	int32_t pts = mRTSPCurrentPlayTimePTS / 90000;
#else
    int32_t pts = 0;
#endif

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return pts;
}

int32_t IptvSocPlayer::setWindowSize(const char* dataXML)
{
	ALOGD("[%s] called", __FUNCTION__);

	return IPTV_ERR_SUCCESS;
}

int32_t IptvSocPlayer::play_file_l()
{
	int nRet;
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);

	LOG_TIME_STARTV("ST_PlayStDvbBuffer [%d]", player.idx_player);
	nRet = ST_PlayStDvbBuffer(&player, &locator);
	LOG_TIME_END("ST_PlayStDvbBuffer nRet(%d)", nRet);

	switch(nRet)
	{
	case 0:
		ALOGD("[SUCCESS] ST_PlayStDvbBuffer !!!!");
		break;
	case -1:
		ALOGD("[ERROR] dvb_tuner_lock!!!!");
		goto error;
	}

	InSetPlayerStatus(PLAYER_STATUS_PLAY);

	// 기존 time event 종료
	mQueue.cancelEvent(mFilePlayTimeCheckEvent->eventID());
	nSendFILEADVIMP = 0;
	nCheckFileTimeCount = 0;

	player.nPollingRunFlag = 1;
	LOG_TIME_STARTV("before call ST_GetClientEvent to Get FirstFrame");
	player.isFirstFrameDisplayed = 0; //jung2604 : first iframe 찾기 초기화
	nRet = ST_GetClientEvent(&player);
	LOG_TIME_END("after call ST_GetClientEvent to Get FirstFrame nRet (%d)" ,nRet);
	player.nPollingRunFlag = 0;

	// time event 재실행 -> 2초마다 지만 이상하게 최초엔 바로 떨어진다.
	postFilePlayTimeCheckEvent_l(200000);

	// play status play 를 전달한다.
	SendFilePlayStatus(0, STATUS_FILE_START);


	ALOGD("[SUCCESS] PLAY FILE done!!!!!");

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return IPTV_ERR_SUCCESS;

error:
	// 기존 time event 종료
	mQueue.cancelEvent(mFilePlayTimeCheckEvent->eventID());
	nSendFILEADVIMP = 0;
	nCheckFileTimeCount = 0;

	InSetPlayerStatus(PLAYER_STATUS_STOP);
	ALOGD("[ERROR] PLAY FILE!!!!");

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return IPTV_ERR_ERROR;
}

#ifdef USE_RTSP
int32_t IptvSocPlayer::play_rtsp_l(int nEnforcePlay)
{
	//nEnforcePlay : seek ,이어보기 시작 정보(PTS정보)
	int nRet, nStatus;
	long long nMiliSec;

    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);

	nStatus = InGetPlayerStatus();

	if (nEnforcePlay == 1)
	{
		ALOGD("[SUCCESS] ST_Play enforce mode by bof !!!!");
	}


	if (nStatus == PLAYER_STATUS_STOP)
	{
		InSetPlayerStatus(PLAYER_STATUS_PLAY);

		// 기존 time event 종료
		mQueue.cancelEvent(mRTSPPlayTimeCheckEvent->eventID());
		nSendRTSPADVIMP = 0;
		mJumpRtspStatusSend = 0;

		mRTSPWannaBeGonePTS = mRTSPStartTime * 90;
        mRTSPCurrentPlayTimePTS = mRTSPWannaBeGonePTS; // UI가 뒤로 돌아가지 않게 하기 위해서 현재의 PTS정보를 미리 갱신해준다.
		//ALOGD("[test] 1 mRTSPStartTime:%d, %lld", mRTSPStartTime, mRTSPCurrentPlayTimePTS);

		// 신규 time event 생성
		postRTSPPlayTimeCheckEvent_l(1000000);

		nRet = RTSP_Play(nRTSPSessionID, mRTSPStartTime, &player);
		if (nRet != 0)
		{
			ALOGD("failed RTSP_Play()");
            SendRTSPPlayStatus(mRTSPStartTime, STATUS_RTSP_STOP); //jung2604 : rtsp_play가 실패할때가 있다 그때 UI에게 알려주는 용도
			goto error;
		}

        player.isFirstFrameDisplayed = 0; //jung2604 : first iframe 찾기 초기화

		// 신규 current time 이벤트 생성
		postConstructCurrentTimeEvent_l(10000);

		SendRTSPPlayStatus(mRTSPStartTime, STATUS_RTSP_START);

		ALOGD("[SUCCESS] PLAY RTSP done!!!!!");

	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_SUCCESS;
	}
	// case trick
	// 현재 상태가 배속재생 중일떄 play 버튼을 누를 경우
	else if ( (nStatus >= PLAYER_STATUS_2XFF && nStatus <= PLAYER_STATUS_16XBW)
		|| nEnforcePlay == 1)
	{
        // jung2604 : 20190520 : vod를 제어할수 없을때(EOF를 받았을때) 명령을 무시하도록 한다.
        if(checkRtspUncontrollableState()) return IPTV_ERR_INVALIDSTATUS;

        // wil pause work doing
		LOG_TIME_STARTV("ST_PauseBuffer [%d]", player.idx_player);
		nRet = ST_PauseBuffer(&player);
		LOG_TIME_END("ST_PauseBuffer nRet(%d)", nRet);

		mJumpRtspStatusSend = 1; // 반드시 나가기 전에 0으로 맞춰야 한다.!!!!
		nMiliSec = mRTSPCurrentPlayTimePTS / 90;

		mRTSPCalculateTimeCheckExceptWait = 1;

		mRTSPBaseTime = mRTSPCurrentPlayTimePTS;

		ALOGD("[fldgo] trick -> play nMiliSec [%lld] mRTSPBaseTime(mRTSPCurrentPlayTimePTS) [%lld] !!!!", nMiliSec, mRTSPCurrentPlayTimePTS);


		RTSP_SetWaitWriteDVR(nRTSPSessionID, 1);    //대기

		if (mRTSPStartTime == -99) // case bof;
		{
			ALOGD("[fldgo] mRTSPStartTime BOF reset nMilisec = 0 mRTSPStartTime = 0!!!!");
			nMiliSec = 0;
			mRTSPStartTime = 0;
		}

		LOG_TIME_STARTV("ST_FlushAVBuffer [%d]", player.idx_player);
		nRet = ST_FlushAVBuffer(&player);
		LOG_TIME_END("ST_FlushAVBuffer nRet(%d)", nRet);

		LOG_TIME_STARTV("RTSP_WaitWriteCheckDVR [%d]", player.idx_player);
		nRet = RTSP_WaitWriteCheckDVR(nRTSPSessionID);  //RTSP 버퍼의 대기 상태를 체크, 버퍼수신 시
		LOG_TIME_END("RTSP_WaitWriteCheckDVR nRet(%d)", nRet);

		ALOGD("[fldgo] before RTSP_Play in TRICK mode [%d] !!!!", nMiliSec);
		nRet = RTSP_Play(nRTSPSessionID, nMiliSec, &player);

		RTSP_SetWaitWriteDVR(nRTSPSessionID, 0);

		LOG_TIME_STARTV("ST_ResumeBuffer [%d]", player.idx_player);
		nRet = ST_ResumeBuffer(&player);
		LOG_TIME_END("ST_ResumeBuffer nRet(%d)", nRet);

		InSetPlayerStatus(PLAYER_STATUS_PLAY);

		player.nPollingRunFlag = 1;
        long long nTestSec = ST_GetTimeStamp(&player);
		ALOGD("before time %lld ", nTestSec);
		LOG_TIME_STARTV("before call WaitTimeMatch to Get FirstFrame");
        player.isFirstFrameDisplayed = 0; //jung2604 : first iframe 찾기 초기화
		nRet = WaitTimeMatch(0, 0, 0, 0, 0, 1);
		LOG_TIME_END("after call WaitTimeMatch to Get FirstFrame nRet (%d)" ,nRet);
		player.nPollingRunFlag = 0;
		nTestSec = ST_GetTimeStamp(&player);
		ALOGD("after time %lld ", nTestSec);

        if (!player.save_mute_enable) ST_AudioMute(&player, 0);

		SendRTSPPlayStatus(nMiliSec, STATUS_RTSP_PLAY);

		mRTSPWannaBeGonePTS = nMiliSec * 90;
        mRTSPCurrentPlayTimePTS = mRTSPWannaBeGonePTS; //jung2604 : UI가 뒤로 돌아가지 않게 하기 위해서 현재의 PTS정보를 미리 갱신해준다.
		mRTSPCalculateTimeCheck = 1;
		while (mRTSPCalculateTimeCheck == 1)
		{
			usleep(3000);
		}
		mRTSPCalculateTimeCheckExceptWait = 0;
		mJumpRtspStatusSend = 0;

		ALOGD("[SUCCESS] PLAY RTSP done!!!!!");

        ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_SUCCESS;
	}
	else
	{
		ALOGD("[WARNING] WHAT CASE IS NOW????");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_UNKNOWN;
	}

error:

	// 기존 time event 종료
	mQueue.cancelEvent(mRTSPPlayTimeCheckEvent->eventID());
	nSendRTSPADVIMP = 0;
	mRTSPCalculateTimeCheckExceptWait = 0;

	InSetPlayerStatus(PLAYER_STATUS_STOP);
	ALOGD("[ERROR] PLAY RTSP!!!!");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return IPTV_ERR_ERROR;
}
#endif

int32_t IptvSocPlayer::play()
{
	int nRet;
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
	std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");

	// device open 상태를 체크
	if (InGetDeviceOpenStatus() == DEVICE_STATUS_CLOSE)
	{
		// device close 상태에서 call 함.
		ALOGD("[ERROR] DEVICE CLOSED Cannot PLAY!!!!!");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}

	// player status 상태를 체크
	if (InGetPlayerStatus() == PLAYER_STATUS_NONE ||
		InGetPlayerStatus() == PLAYER_STATUS_PLAY ||
		InGetPlayerStatus() == PLAYER_STATUS_PAUSE
		)
	{
		// Play + trick  상태가 아닌데 play 요청 들어옴.
		ALOGD("[ERROR] PLAYER STATUS NOT STOP!!!!!");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}

	if (InGetPlayerMode() == PLAYER_MODE_NORMAL_FILE)
	{
		// file play 요청
		int32_t tStatus =  play_file_l();
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	    return tStatus;
	}

#ifdef USE_RTSP
	if (InGetPlayerMode() == PLAYER_MODE_RTSP)
	{
		// rtsp play 요청
		int32_t tStatus = play_rtsp_l(0);
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	    return tStatus;
	}
#endif

	ALOGD("[ERROR] WHAT STATUS NOW???");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return IPTV_ERR_INVALIDSTATUS;
}

int32_t IptvSocPlayer::pause() {
    int nRet = 0;
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__, __LINE__);
    std::lock_guard<std::mutex> lock(mLock);

    nRet = pause_impl();

    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
    return nRet;
}

int32_t IptvSocPlayer::pause_impl()
{
#ifdef USE_RTSP
	int nRet = 0;
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
	//std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");

	// device 상태를 체크하여
	if (InGetDeviceOpenStatus() == DEVICE_STATUS_CLOSE)
	{
		// close 상태이면 에러 리턴한다.
		ALOGD("[WARNING] DEVICE IS CLOSED. WILL RETURN OK");
		goto error_invalid_status;
	}

	// 현재 play mode 를 체크해서
	if	((InGetPlayerMode() != PLAYER_MODE_RTSP)  &&
	 (InGetPlayerMode() != PLAYER_MODE_NORMAL_FILE) )// rtsp 아직 pause resume 작업안됨

	{
		// RTSP 나 FILE PLAY 가 아니면 에러 리턴한다.
		ALOGD("[WARNING] NOT RTSP/FILEPLAY. WILL RETURN ERROR");
		goto error_invalid_status;
	}

	// 현재 play status 를 체크해서
	if (InGetPlayerStatus() == PLAYER_STATUS_NONE ||
		InGetPlayerStatus() == PLAYER_STATUS_STOP)
	{
		// None 이나 Stop 이면 에러 리턴한다.
		ALOGD("[WARNING] RTSP/FILEPLAY IS Already Stop Or None. WILL RETURN ERROR");
		goto error_invalid_status;
	}

	// 현재 play status 를 체크해서 이미 pause 상태이면 success 리턴한다.
	if (InGetPlayerStatus() == PLAYER_STATUS_PAUSE)
	{
		// None 이나 Stop 이면 에러 리턴한다.
		ALOGD("[WARNING] RTSP/FILEPLAY IS Already pausedWILL RETURN OK");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_SUCCESS;
	}

	if ( (InGetPlayerStatus() >= PLAYER_STATUS_2XFF && InGetPlayerStatus() <= PLAYER_STATUS_16XBW)) {

        play_rtsp_l(0);
    }

	// wil pause work doing
	LOG_TIME_STARTV("ST_PauseBuffer [%d]", player.idx_player);
	nRet = ST_PauseBuffer(&player);
	LOG_TIME_END("ST_PauseBuffer nRet(%d)", nRet);

	switch(nRet)
	{
	case 0:
		ALOGD("[SUCCESS] ST_PauseBuffer !!!!");
		break;
	default:
		ALOGD("[ERROR] unknown ERROR [%d]!!!!", nRet);
		goto error;
	}

	// rtsp 의 경우
	if (InGetPlayerMode() == PLAYER_MODE_RTSP)
	{
	    //시간 저장 순서가 중요하다
        mRTSPPauseTime = mRTSPCurrentPlayTimePTS / 90;
        ALOGD("RTSP_PAUSE mRTSPPauseTime : %d", mRTSPPauseTime);

#if 1 //TODO : jung2604 : 테스트
		nRet = RTSP_Pause(nRTSPSessionID);
		check(!nRet,"failed RTSP pause");
#endif

		ALOGD("[success] RTSP_PAUSE!!!!");
		SendRTSPPlayStatus(mRTSPPauseTime, STATUS_RTSP_PAUSE);
	}

	InSetPlayerStatus(PLAYER_STATUS_PAUSE);

	ALOGD("[SUCCESS]PAUSE Action done!!!");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return IPTV_ERR_SUCCESS;

error:
    ALOGD("[ERROR] PAUSE FAILED!!!!");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
    return IPTV_ERR_ERROR;

error_invalid_status:
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
#endif
    return IPTV_ERR_INVALIDSTATUS;
}

int32_t IptvSocPlayer::resume()
{
	int nRet = 0;
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
	std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");

	// device 상태를 체크하여
	if (InGetDeviceOpenStatus() == DEVICE_STATUS_CLOSE)
	{
		// close 상태이면 에러 리턴한다.
		ALOGD("[WARNING] DEVICE IS CLOSED. WILL RETURN OK");
		goto error_invalid_status;
	}

	// 현재 play mode 를 체크해서
	if ( (InGetPlayerMode() != PLAYER_MODE_NORMAL_FILE) &&
		(InGetPlayerMode() != PLAYER_MODE_RTSP) )
	{
		// RTSP 나 FILE PLAY 가 아니면 에러 리턴한다.
		ALOGD("[WARNING] NOT RTSP/FILEPLAY. WILL RETURN ERROR");
		goto error_invalid_status;
	}

	// 현재 play status 를 체크해서
	if (InGetPlayerStatus() != PLAYER_STATUS_PAUSE)
	{
		// pause 가 아니면 에러 리턴한다.
		ALOGD("[WARNING] RTSP/FILEPLAY IS not pause mode. WILL RETURN ERROR");
		goto error_invalid_status;
	}

	LOG_TIME_STARTV("ST_ResumeBuffer [%d]", player.idx_player);
	nRet = ST_ResumeBuffer(&player);
	LOG_TIME_END("ST_ResumeBuffer nRet(%d)", nRet);

	switch(nRet)
	{
	case 0:
		ALOGD("[SUCCESS] ST_ResumeBuffer !!!!");
		break;
	default:
		ALOGD("[ERROR] unknown ERROR [%d]!!!!", nRet);
		goto error;
	}

	if (!player.save_mute_enable) ST_AudioMute(&player, 0);

#ifdef USE_RTSP
	if (InGetPlayerMode() == PLAYER_MODE_RTSP)
	{
		nRet = RTSP_Resume(nRTSPSessionID);
		check(!nRet,"failed RTSP/FILE resume");

		mRTSPWannaBeGonePTS = mRTSPPauseTime * 90;
        mRTSPCurrentPlayTimePTS = mRTSPWannaBeGonePTS; //jung2604 : UI가 뒤로 돌아가지 않게 하기 위해서 현재의 PTS정보를 미리 갱신해준다.
		mRTSPCalculateTimeCheck = 1;
		while (mRTSPCalculateTimeCheck == 1)
		{
			usleep(3000);
		}

        InSetPlayerStatus(PLAYER_STATUS_PLAY);
		SendRTSPPlayStatus(mRTSPPauseTime, STATUS_RTSP_PLAY);
	}
	else
#endif
	{
		InSetPlayerStatus(PLAYER_STATUS_PLAY);
	}

	ALOGD("[SUCCESS]RESUME Action done!!!");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return IPTV_ERR_SUCCESS;

error:

    ALOGD("[ERROR] RESUME FAILED!!!!");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
    return IPTV_ERR_ERROR;

error_invalid_status:
    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
    return IPTV_ERR_INVALIDSTATUS;
}


int32_t IptvSocPlayer::stop()
{
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
	std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");

    player.isFirstFrameDisplayed = 0; //jung2604 : first iframe 찾기 초기화

	if (InGetPlayerMode() == PLAYER_MODE_NORMAL_FILE)
	{
		player.nPollingRunFlag = 0;
		mQueue.cancelEvent(mAVEventCheckEvent->eventID());
		int32_t tStatus = stop_file_l(0);
        ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
        return tStatus;
	}

#ifdef USE_RTSP
	if (InGetPlayerMode() == PLAYER_MODE_RTSP)
	{
		player.nPollingRunFlag = 0;
		mQueue.cancelEvent(mAVEventCheckEvent->eventID());
		int32_t tStatus = stop_rtsp_l();
        ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
        return tStatus;
	}
#endif
	if (InGetPlayerMode() == PLAYER_MODE_IPTV)
	{
		int32_t tStatus = stop_tv_l();
        ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
        return tStatus;
	}


	ALOGD("[ERROR] WHAT STATUS NOW???");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return IPTV_ERR_INVALIDSTATUS;
}

#ifdef USE_RTSP
int32_t IptvSocPlayer::close_rtsp_l(int nStopFlag)
{
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);

	int nRet = 0;

	// device 가 close 상태라도... 한번 더 체크하는게.. 낫다.
	if (InGetDeviceOpenStatus() == DEVICE_STATUS_CLOSE)
	{
		// 정상 리턴한다.
		ALOGD("[WARNING] DEVICE IS CLOSED. WILL RETURN OK");
        goto error;
	}

	// 현재 play mode 를 체크해서
	if ((InGetPlayerMode() != PLAYER_MODE_RTSP) )
	{
		// RTSP 가 아니면 에러 리턴한다.
		ALOGD("[WARNING] NOT RTSP. WILL RETURN ERROR");
        goto error_invalid_status;
	}

    {
        int nStatus = InGetPlayerStatus();
        if (nStatus >= PLAYER_STATUS_2XFF && nStatus <= PLAYER_STATUS_16XBW) {
            //jung2604 : 소리가 켜져있어야 하는경우에만 켠다..
            if (!player.save_mute_enable) ST_AudioMute(&player, 0);
        }
    }

	// 현재 play status 를 체크해서
	if ( (InGetPlayerStatus() != PLAYER_STATUS_NONE ) &&
		(InGetPlayerStatus() != PLAYER_STATUS_STOP) )
	{
		// None 도 아니고 stop 도 아니면
		ALOGD("[WARNING] RTSP/FILEPLAY IS not Stopped. stop process start");
		if (nStopFlag == 1)
		{
			stop_rtsp_l();
		}
	}

	// rtsp 세션 close 시킨다.

	if (nStopFlag == 1)
	{
	    RTSP_Close(nRTSPSessionID);
	}

	// device close 시킨다.
	LOG_TIME_STARTV("ST_CloseDevice [%d]", player.idx_player);
	nRet = ST_CloseDevice(&player);
	LOG_TIME_END("ST_CloseDevice nRet(%d)", nRet);

	InSetPlayerMode(PLAYER_MODE_NONE);
	InSetPlayerStatus(PLAYER_STATUS_NONE);
	InSetDeviceOpenStatus(DEVICE_STATUS_CLOSE);

	ALOGD("[SUCCESS]CLOSE RTSP Action done!!!");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return IPTV_ERR_SUCCESS;

error:
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return IPTV_ERR_ERROR;

error_invalid_status:
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return IPTV_ERR_INVALIDSTATUS;
}
#endif

int32_t IptvSocPlayer::close_file_l()
{
	int nRet = 0;

    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);

	// device 가 close 상태라면 한번 더 체크하는게.. 낫다.
	if (InGetDeviceOpenStatus() == DEVICE_STATUS_CLOSE)
	{
		// 정상 리턴한다.
		ALOGD("[WARNING] DEVICE IS CLOSED. WILL RETURN OK");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_SUCCESS;
	}

	// 현재 play mode 를 체크해서
	if (InGetPlayerMode() != PLAYER_MODE_NORMAL_FILE)
	{
		// RTSP 나 FILE PLAY 가 아니면 에러 리턴한다.
		ALOGD("[WARNING] NOT RTSP/FILEPLAY. WILL RETURN ERROR");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}

	// 현재 play status 를 체크해서
	if ( (InGetPlayerStatus() != PLAYER_STATUS_NONE ) &&
		(InGetPlayerStatus() != PLAYER_STATUS_STOP) )
	{
		// None 도 아니고 stop 도 아니면
		ALOGD("[WARNING] RTSP/FILEPLAY IS not Stopped. stop process start");
		stop_file_l(0);
	}

	// device close 시킨다.

	LOG_TIME_STARTV("ST_CloseDevice [%d]", player.idx_player);
	nRet = ST_CloseDevice(&player);
	LOG_TIME_END("ST_CloseDevice nRet(%d)", nRet);

	InSetPlayerMode(PLAYER_MODE_NONE);
	InSetPlayerStatus(PLAYER_STATUS_NONE);
	InSetDeviceOpenStatus(DEVICE_STATUS_CLOSE);

	ALOGD("[SUCCESS]CLOSE FILE Action done!!!");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return IPTV_ERR_SUCCESS;
}


int32_t IptvSocPlayer::close()
{
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
	std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");

	// device 가 close 상태라면
	if (InGetDeviceOpenStatus() == DEVICE_STATUS_CLOSE)
	{
		// 정상 리턴한다.
		ALOGD("[WARNING] DEVICE IS CLOSED. WILL RETURN OK");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_SUCCESS;
	}

	// 현재 play mode 를 체크해서
	if (InGetPlayerMode() == PLAYER_MODE_NORMAL_FILE)
	{
		int32_t tStatus = close_file_l();
        ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
        return tStatus;
	}

#ifdef USE_RTSP
	if (InGetPlayerMode() == PLAYER_MODE_RTSP)
	{
		int32_t tStatus = close_rtsp_l(1);
        ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
        return tStatus;
	}
#endif

	ALOGD("[ERROR] WHAT STATUS NOW???");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return IPTV_ERR_INVALIDSTATUS;
}

#if 0
int32_t IptvSocPlayer::invoke(const Parcel& request, Parcel *reply)
{
    ALOGD("[INFO] [%s] called", __FUNCTION__);
    request.setDataPosition(0);
    size_t countEntries = request.readUint32();
    for (size_t i = 0; i < countEntries; i++) {
        int32_t type = request.readInt32();
        //ALOGD("[INFO] [%s] TYPE : %d", __FUNCTION__, type);
        switch (type) {
            case TYPE_INT : {
                int32_t param = request.readInt32();
                if(param == 1105){
                    int64_t val_long = mRTSPCurrentPlayTimePTS;
                    //jung2604 : 20190518 : UI에게 마지막으로 던져준 play time 값.
                    // UI에게 시간은 던졌고 media는 아직 firstframe을 못받아서 시간갱신이 되지 않았을때 ui상태바가 되돌아가는것을 방지한다.
                    if (mRTSPWaitTimeChanged == 1 && val_long <= 0) {
                        val_long = mLastSendRTSPStatusPlayTimeMs * 90;
                        ALOGD("[INFO] [%s] last play time : %d, val : %ld", __FUNCTION__, mLastSendRTSPStatusPlayTimeMs, val_long);
                    }

                    if(val_long <= 0) val_long = 0;
                    else val_long /= 2;
                    //ALOGD("[INFO] [%s] val : %d", __FUNCTION__, val_long);
                      reply->writeUint32(1); // Size
                      reply->writeInt32(TYPE_LONG); // Type
                      reply->writeInt64(val_long);  // Value
                      //ALOGD("[INFO] [%s] val_long : %lld(%lld ms), val_long : %lld(%lld ms), ", __FUNCTION__, val_long, val_long / 90, val_long2, val_long2 / 90);
                      return IPTV_ERR_SUCCESS;
                }

                break;
            }
            case TYPE_LONG : {
                int64_t val = request.readInt64();
                break;
            }
            case TYPE_STRING : {
                std::string val;
                request.readUtf8FromUtf16(&val);
                const char* cval = val.c_str();
                break;
            }
        }
    }
    return IPTV_ERR_SUCCESS;
}
#endif

int32_t IptvSocPlayer::reset()
{
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
	std::lock_guard<std::mutex> lock(mLock);

	int32_t tStatus = reset_l();
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return tStatus;
}

void  IptvSocPlayer::convertCodecInfo(dvb_locator_t &locator) {
    ALOGD("convertCodecInfo vcodec : %d(%x), acodec : %d(%x)", locator.vcodec, locator.vcodec, locator.acodec, locator.acodec);
     player.enable_video = true;
	if (locator.vcodec == 27) {
		locator.vcodec = AVP_CODEC_VIDEO_H264;
	} else if(locator.vcodec == 36) {
        locator.vcodec = AVP_CODEC_VIDEO_H265;
    } else
        player.enable_video = false;

    if (locator.acodec == 5) {
        ALOGD("CODEC EAC3");
        locator.acodec = AVP_CODEC_AUDIO_EAC3;;
    } else if (locator.acodec == 15) {
		ALOGD("CODEC AAC");
		locator.acodec = AVP_CODEC_AUDIO_AAC;
	} else if (locator.acodec == 129) {
		ALOGD("CODEC AC3");
		locator.acodec = AVP_CODEC_AUDIO_AC3;
	} else {
		ALOGD("default CODEC AAC");
		locator.acodec = AVP_CODEC_AUDIO_AAC;
	}
}

dvb_locator_t IptvSocPlayer::locator_create(HTuner_IPTV_Info stTuneInfo)
{
    dvb_locator_t locator;

	ALOGD("[%s] called\n[%s]", __FUNCTION__, __FILE__);

    memset(&locator,0,sizeof(dvb_locator_t));

    locator.type = DVB_IP;

    locator.tune_param.ip.port = stTuneInfo.multicast_port;
    strcpy(locator.tune_param.ip.ip, stTuneInfo.multicast_addr);
    snprintf(locator.name, sizeof(locator.name), "%d", stTuneInfo.channel_num);
    locator.vid = stTuneInfo.video_pid;
    locator.aid = stTuneInfo.audio_pid;
	locator.pcr_pid = stTuneInfo.pcr_pid;

    locator.vcodec = stTuneInfo.video_stream;
    locator.acodec = stTuneInfo.audio_stream;

    locator.keep_last_frame = stTuneInfo.enableLastFrame;

	// jung2604
	convertCodecInfo(locator);

    ALOGD("end locator create");
    return locator;
}

dvb_locator_t IptvSocPlayer::locator_create(HPlayer_IPTV_Info stPlayInfo)
{
    dvb_locator_t locator;

	ALOGD("[%s] called\n[%s]", __FUNCTION__, __FILE__);

    memset(&locator,0,sizeof(dvb_locator_t));

    if (stPlayInfo.nPlayMode == 1) //case FILE PLAY
        locator.type = DVB_FILE;
    else if (stPlayInfo.nPlayMode == 2) //case rtsp
        locator.type = DVB_VOD;

    strcpy(locator.tune_param.file.filename, stPlayInfo.szPath);

	// jung2604 : 추가
	locator.vid = stPlayInfo.video_pid;
	locator.aid = stPlayInfo.audio_pid;
	locator.pcr_pid = 0;

	locator.vcodec = stPlayInfo.video_stream;
	locator.acodec = stPlayInfo.audio_stream;

    locator.keep_last_frame = stPlayInfo.enableLastFrame;

    ALOGD("end locator create");
    return locator;
}

int IptvSocPlayer::_TuneMultiviewChannel(HTuner_IPTV_Info *ptTuneInfo) {
    int nRet = 0, nAudioEnable;
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
    ALOGD("[%s][DEVICE:%d][sptek_player:%d] called", __FUNCTION__, player.idx_player, player.avpInfo.playerHandle);

    dvb_locator_t ptLocators[3];
    locator = ptLocators[0] = locator_create(ptTuneInfo[0]);
    ptLocators[1] = locator_create(ptTuneInfo[1]);
    ptLocators[2] = locator_create(ptTuneInfo[2]);

    //jung2604 : mute값 저장
    player.save_mute_enable = nAudioEnable = (ptTuneInfo[0].audioenable == 0);

    nRet = SPTEK_TuneMultiviewChannel(&player, ptLocators, playerEventCallback);

    switch(nRet)
    {
        case 0:
            ALOGD("[SUCCESS] SPTEK_TuneMultiviewChannel !!!!");
            break;
        case -1:
            ALOGD("[ERROR] dvb_device_open!!!!");
            goto error;
        case -4:
            ALOGD("[ERROR] dvb_set_stm_dvb_option!!!!");
            goto error;
        case -5:
            ALOGD("[ERROR] dvb_tuner_lock!!!!");
            goto error;
        default:
            ALOGD("[ERROR] create SPTEK_TuneMultiviewChannel!!!!");
            goto error;
    }

    InSetDeviceOpenStatus(DEVICE_STATUS_OPEN);
    InSetPlayerMode(PLAYER_MODE_IPTV);
    InSetPlayerStatus(PLAYER_STATUS_TUNE);

    mSocketBufferCheckEventStop = false;
    postSocketBufferCheckEvent_l(1000000);
    postAVPTSTimeCheckEvent_l(10000);

/*
    ALOGD("before CALL ST_AudioMute DeviceID[%d]", player.idx_player);
    LOG_TIME_STARTV("ST_AudioMute [%d]", player.idx_player);
    //jung2604 : realtek에서 화면이 나오기 전까지 음소거후, First Frame이 올때 소리를 키워달라고 요청함
//    ST_AudioMute(&player, 1);
    //ST_AudioMute(&player, nAudioEnable);
    LOG_TIME_END("ST_AudioMute nRet(%d)", nRet);
    ALOGD("after CALL ST_AudioMute DeviceID[%d]", player.idx_player);
*/

    ALOGD("[SUCCESS] TUNE SUCCESS!!!!");

    ALOGD("Success Tune channel Work. DeviceID[%d]", player.idx_player);
    ALOGD("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
    return IPTV_ERR_SUCCESS;

    error:

    InSetPlayerMode(PLAYER_MODE_NONE);
    InSetPlayerStatus(PLAYER_STATUS_NONE);
    InSetDeviceOpenStatus(DEVICE_STATUS_CLOSE);

    ALOGD(">>>>>>>>>>>>>>>>>>>>>>> TUNE FAIL .. BUFFER ERROR MESSAGE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    mSocketBufferCheckEventStop = false;
    postSocketBufferCheckEvent_l(1000000);
    ALOGD("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return IPTV_ERR_ERROR;
}

int IptvSocPlayer::_TuneChannel(HTuner_IPTV_Info stTuneInfo)
{
	int nRet = 0, nAudioEnable;
	//ALOGD("[%s] called", __FUNCTION__);
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
	ALOGD("[%s][DEVICE:%d][sptek_player:%d] called", __FUNCTION__, player.idx_player, player.avpInfo.playerHandle);

	locator = locator_create(stTuneInfo);

	if (stTuneInfo.audioenable == 0)
	{
		nAudioEnable = 1;
	}
	else
	{
		nAudioEnable = 0;
	}

	ALOGD("before CALL ST_TuneChannel DeviceID[%d]", player.idx_player);
	LOG_TIME_STARTV("ST_TuneChannel [%d]", player.idx_player);

	nRet = ST_TuneChannel(&player, &locator, playerEventCallback);

	LOG_TIME_END("ST_TuneChannel nRet(%d)", nRet);
	ALOGD("after CALL ST_TuneChannel DeviceID[%d]", player.idx_player);

	switch(nRet)
	{
    case 0:
        ALOGD("[SUCCESS] ST_TuneChannel !!!!");
        break;
	case -1:
		ALOGD("[ERROR] dvb_device_open!!!!");
		goto error;
	case -4:
		ALOGD("[ERROR] dvb_set_stm_dvb_option!!!!");
		goto error;
	case -5:
		ALOGD("[ERROR] dvb_tuner_lock!!!!");
		goto error;
	default:
		ALOGD("[ERROR] create ST_TuneChannel!!!!");
		goto error;
	}

	InSetDeviceOpenStatus(DEVICE_STATUS_OPEN);
	InSetPlayerMode(PLAYER_MODE_IPTV);
	InSetPlayerStatus(PLAYER_STATUS_TUNE);

	mSocketBufferCheckEventStop = false;
	postSocketBufferCheckEvent_l(1000000);

	postAVPTSTimeCheckEvent_l(10000);

    //jung2604 : mute값 저장
    player.save_mute_enable = !stTuneInfo.audioenable;

/*
	ALOGD("before CALL ST_AudioMute DeviceID[%d]", player.idx_player);
	LOG_TIME_STARTV("ST_AudioMute [%d]", player.idx_player);
    //jung2604 : realtek에서 화면이 나오기 전까지 음소거후, First Frame이 올때 소리를 키워달라고 요청함
//    ST_AudioMute(&player, 1);
	//ST_AudioMute(&player, nAudioEnable);
	LOG_TIME_END("ST_AudioMute nRet(%d)", nRet);
	ALOGD("after CALL ST_AudioMute DeviceID[%d]", player.idx_player);
*/

	ALOGD("[SUCCESS] TUNE SUCCESS!!!!");

	ALOGD("Success Tune channel Work. DeviceID[%d]", player.idx_player);
	ALOGD("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
    return IPTV_ERR_SUCCESS;

error:

	InSetPlayerMode(PLAYER_MODE_NONE);
	InSetPlayerStatus(PLAYER_STATUS_NONE);
	InSetDeviceOpenStatus(DEVICE_STATUS_CLOSE);

    ALOGD(">>>>>>>>>>>>>>>>>>>>>>> TUNE FAIL .. BUFFER ERROR MESSAGE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    mSocketBufferCheckEventStop = false;
    postSocketBufferCheckEvent_l(1000000);
    ALOGD("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return IPTV_ERR_ERROR;
}

int IptvSocPlayer::_ChangeChannel(HTuner_IPTV_Info stTuneInfo)
{
	int nRet=0, nAudioEnable;

    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
	ALOGD("[%s][DEVICE:%d][sptek_player:%d] called", __FUNCTION__, player.idx_player, player.avpInfo.playerHandle);

	locator = locator_create(stTuneInfo);

	if (stTuneInfo.audioenable == 0)
	{
		nAudioEnable = 1;
	}
	else
	{
		nAudioEnable = 0;
	}

	// tune은 main 일때에만 dolby체크
	if (player.idx_player == 0)
	{
		ALOGD("before CALL ST_StopChannelSilent DeviceID[%d]", player.idx_player);
		LOG_TIME_STARTV("ST_StopChannelSilent [%d]", player.idx_player);
		nRet = ST_StopChannelSilent(&player, &locator);
		LOG_TIME_END("ST_StopChannelSilent nRet(%d)", nRet);
		ALOGD("after CALL ST_StopChannelSilent DeviceID[%d]", player.idx_player);

		ALOGD("before CALL ST_ChangeChannel DeviceID[%d]", player.idx_player);
		LOG_TIME_STARTV("ST_ChangeChannel [%d]", player.idx_player);
		nRet = ST_ChangeChannel(&player, &locator, playerEventCallback);
		LOG_TIME_END("ST_ChangeChannel nRet(%d)", nRet);
		ALOGD("after CALL ST_ChangeChannel DeviceID[%d]", player.idx_player);
	}
	else if (player.idx_player == 1) // pip 일경우에만 dolby delay
	{
		ALOGD("before CALL ST_ChangeChannel DeviceID[%d]", player.idx_player);
		LOG_TIME_STARTV("ST_ChangeChannel [%d]", player.idx_player);
		nRet = ST_ChangeChannel(&player, &locator, playerEventCallback);
		LOG_TIME_END("ST_ChangeChannel nRet(%d)", nRet);
		ALOGD("after CALL ST_ChangeChannel DeviceID[%d]", player.idx_player);
	}

	switch(nRet)
	{
    case 0:
        ALOGD("[SUCCESS] ST_ChangeChannel !!!!");
        break;
	case -1:
		ALOGD("[ERROR] dvb_tuner_unlock!!!!");
		goto error;
	case -2:
		ALOGD("[ERROR] dvb_clear_av_buffer!!!!");
		goto error;
	case -3:
		ALOGD("[ERROR] dvb_set_decoder!!!!");
		goto error;
	case -4:
		ALOGD("[ERROR] dvb_set_stm_dvb_option!!!!");
		goto error;
	case -6:
		ALOGD("[ERROR] create dvb_tuner_lock!!!!");
		goto error;
	default:
		ALOGD("[ERROR] ST_ChangeChannel!!!!");
		goto error;

	}


	InSetPlayerMode(PLAYER_MODE_IPTV);
	InSetPlayerStatus(PLAYER_STATUS_TUNE);

	mSocketBufferCheckEventStop = false;
	postSocketBufferCheckEvent_l(1000000);

	postAVPTSTimeCheckEvent_l(10000);

    //jung2604 : mute값 저장
    player.save_mute_enable = !stTuneInfo.audioenable;

/*
	ALOGD("before CALL ST_AudioMute DeviceID[%d]", player.idx_player);
	LOG_TIME_STARTV("ST_AudioMute [%d]", player.idx_player);

    //jung2604 : realtek에서 화면이 나오기 전까지 음소거후, First Frame이 올때 소리를 키워달라고 요청함
//    ST_AudioMute(&player, 1);
	//ST_AudioMute(&player, nAudioEnable);
	LOG_TIME_END("ST_AudioMute nRet(%d)", nRet);
	ALOGD("after CALL ST_AudioMute DeviceID[%d]", player.idx_player);
*/

    ALOGD("succeeded tuner_tune_change\n");

	ALOGD("Success Change channel Work. DeviceID[%d]", player.idx_player);
	ALOGD("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return IPTV_ERR_SUCCESS;

error:
    ALOGD("[ERROR] CHANNEL CHANGE FAILED!!!!");

	InSetPlayerMode(PLAYER_MODE_NONE);
	InSetPlayerStatus(PLAYER_STATUS_NONE);
	InSetDeviceOpenStatus(DEVICE_STATUS_CLOSE);
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return IPTV_ERR_ERROR;

}

int32_t IptvSocPlayer::open_file_l(HPlayer_IPTV_Info stPlayInfo)
{
	int nRet = 0, nDeviceWillOpen = 0;
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);

	if ((InGetPlayerMode() != PLAYER_MODE_NONE) &&
		(InGetPlayerMode() != PLAYER_MODE_NORMAL_FILE))
	{
		// 플레이 가능한 상태가아니다.
		ALOGD("[ERROR] NOT NONE or NOT NORMAL FILE");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}


	if ((InGetPlayerStatus() != PLAYER_STATUS_STOP) &&
		(InGetPlayerStatus() != PLAYER_STATUS_NONE)) //case not stopped and not none
	{
		// 대기상태가 아니다.
		ALOGD("[ERROR] NOT STOP or NOT NONE");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}

	if( InGetDeviceOpenStatus() == DEVICE_STATUS_OPEN) {
        LOG_TIME_STARTV("ST_CloseDevice [%d], in open_file_l", player.idx_player);
        nRet = ST_CloseDevice(&player);
        LOG_TIME_END("ST_CloseDevice nRet(%d), in open_file_l", nRet);
        InSetDeviceOpenStatus(DEVICE_STATUS_CLOSE);
	}

	// linuxdvb가 사용하는 locator 를 설정한다.
    locator = locator_create(stPlayInfo);

	// path 와 meta info 적재
    strcpy(szMediaInfo, stPlayInfo.szPath);
	strcpy(szMetaInfo, stPlayInfo.szMetaInfo);

	if (InGetDeviceOpenStatus() == DEVICE_STATUS_CLOSE)
	{
		nDeviceWillOpen = 1;
	}

	player.enable_audio = true;

    //jung2604 : mute값 저장
    player.save_mute_enable = !player.enable_audio;

	LOG_TIME_STARTV("ST_OpenTsFile [%d] nDeivceWillOpen [%d]", player.idx_player, nDeviceWillOpen);
	nRet = ST_OpenTsFile(&player, &locator, 1, playerEventCallback);
	LOG_TIME_END("ST_OpenTsFile nRet(%d)", nRet);

	switch(nRet)
	{
	case 0:
		ALOGD("[SUCCESS] ST_OpenTsFile !!!!");
		break;
	case -1:
		ALOGD("[ERROR] dvb_device_open!!!!");
		goto error;
	case -3:
		ALOGD("[ERROR] dvb_set_stream_info!!!!");
		goto error;
	case -6:
		ALOGD("[ERROR] dvb_set_stm_dvb_option!!!!");
		goto error;
	default:
		ALOGD("[ERROR] unknown ERROR [%d]!!!!", nRet);
		goto error;
	}

//    LOG_TIME_STARTV("ST_AudioMute [%d]", player.idx_player);
//    ST_AudioMute(&player, 1);//player.save_mute_enable);
//    LOG_TIME_END("ST_AudioMute nRet(%d)", nRet);

	InSetDeviceOpenStatus(DEVICE_STATUS_OPEN);
	InSetPlayerStatus(PLAYER_STATUS_STOP);
	InSetPlayerMode(PLAYER_MODE_NORMAL_FILE);

	ALOGD("[SUCCESS] OPEN SUCCESS!!!!");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
    return IPTV_ERR_SUCCESS;

error:
    ALOGD("[ERROR] OPEN FAILED!!!!");

	InSetPlayerMode(PLAYER_MODE_NONE);
	InSetPlayerStatus(PLAYER_STATUS_NONE);
	InSetDeviceOpenStatus(DEVICE_STATUS_CLOSE);
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return IPTV_ERR_ERROR;

}

#ifdef USE_RTSP
int32_t IptvSocPlayer::open_rtsp_l(HPlayer_IPTV_Info stPlayInfo)
{
	int nRet=0;
	int nDeviceWillOpen = 0;
	char szMediaURL[1024];

    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);

	if ((InGetPlayerMode() != PLAYER_MODE_NONE) &&
		(InGetPlayerMode() != PLAYER_MODE_RTSP))
	{
		// 플레이 가능한 상태가아니다.
		ALOGD("[ERROR] NOT NONE or NOT RTSP");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_SUCCESS;
	}

	if ((InGetPlayerStatus() != PLAYER_STATUS_STOP) &&
		(InGetPlayerStatus() != PLAYER_STATUS_NONE)) //case not stopped and not none
	{
		// 대기상태가 아니다.
		ALOGD("[ERROR] NOT STOP or NOT NONE");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}

	if ((InGetPlayerMode() != PLAYER_MODE_NONE) &&
		(InGetPlayerMode() != PLAYER_MODE_RTSP)) //case not rtsp or none
	{
		// 대기상태가 아니다.
		ALOGD("[ERROR] NOT STOP or NOT NONE");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}

	// linuxdvb가 사용하는 locator 를 설정한다.
    locator = locator_create(stPlayInfo);

	// audio enable 상태로 만든다.
    player.enable_audio = true;

    //jung2604 : mute값 저장
    player.save_mute_enable = !player.enable_audio;

	// path 와 meta info 적재

	strcpy(szOTPID, stPlayInfo.szOTPID);
	strcpy(szOTPPASSWD, stPlayInfo.szOTPPasswd);
    strcpy(szMediaInfo, stPlayInfo.szPath);

	if (strlen(szOTPID) == 0)
	{
		sprintf(szMediaURL, "%s", szMediaInfo);
	}
	else
	{
		sprintf(szMediaURL, "%s?id=%s?passwd=%s", szMediaInfo, szOTPID, szOTPPASSWD);
	}

	strcpy(szMetaInfo, stPlayInfo.szMetaInfo);
	strcpy(szContentID, stPlayInfo.szContentID);

	if (InGetDeviceOpenStatus() == DEVICE_STATUS_CLOSE)
	{
		nDeviceWillOpen = 1;
	}

	// rtsp Session ID는 계속 증가한다.
	nRTSPSessionID ++;
	mRTSPEOFTime = -1;

	mRTSPStartTime = stPlayInfo.nResumeSec * 1000;
    mRTSPCurrentPlayTimePTS = 0;

	nRet = RTSP_Open(nRTSPSessionID, 0, szMediaURL, szContentID, mRTSPStartTime, (void*)RTSPErrorCB, this);

	if (nRet != 0)
	{
		ALOGD("FAILED RTSP_OPEN()");
		goto error;
	}

    locator.vid = RTSP_GetVideoPID(nRTSPSessionID);
    locator.aid = RTSP_GetAudioPID(nRTSPSessionID);

    locator.vcodec = RTSP_GetVideoCodec(nRTSPSessionID);
    locator.acodec = RTSP_GetAudioCodec(nRTSPSessionID);

    //jung2604
    convertCodecInfo(locator);

	LOG_TIME_STARTV("ST_OpenRTSP [%d] nDeivceWillOpen [%d]", player.idx_player, nDeviceWillOpen);
	nRet = ST_OpenRTSP(&player, &locator, nDeviceWillOpen, playerEventCallback);
	LOG_TIME_END("ST_OpenRTSP nRet(%d)", nRet);

	switch(nRet)
	{
	case 0:
		ALOGD("[SUCCESS] ST_OpenRTSP !!!!");
		break;
	case -1:
		ALOGD("[ERROR] dvb_device_open!!!!");
		goto error;
	case -4:
		ALOGD("[ERROR] dvb_set_stm_dvb_option!!!!");
		goto error;
	default:
		ALOGD("[ERROR] unknown ERROR [%d]!!!!", nRet);
		goto error;
	}

/*
    LOG_TIME_STARTV("ST_AudioMute [%d]", player.idx_player);
    ST_AudioMute(&player, 1);//player.save_mute_enable); //뮤트 처리
    LOG_TIME_END("ST_AudioMute nRet(%d)", nRet);
*/

	InSetDeviceOpenStatus(DEVICE_STATUS_OPEN);
	InSetPlayerStatus(PLAYER_STATUS_STOP);
	InSetPlayerMode(PLAYER_MODE_RTSP);

	ALOGD("[SUCCESS] OPEN RTSP SUCCESS!!!!");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
    return IPTV_ERR_SUCCESS;

error:

	ALOGD("[ERROR] OPEN RTSP FAILED!!!!");

	InSetPlayerMode(PLAYER_MODE_NONE);
	InSetPlayerStatus(PLAYER_STATUS_NONE);
	InSetDeviceOpenStatus(DEVICE_STATUS_CLOSE);
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return IPTV_ERR_ERROR;

}
#endif

///////////////////////////////////////////////////////////////////////////////////////////////
// implementation
int32_t IptvSocPlayer::open_l(HPlayer_IPTV_Info stPlayInfo)
{
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
	std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");


	if (InGetPlayerMode() == PLAYER_MODE_IPTV)
	{
		ALOGD("[ERROR] CURRENT IPTV MODE!!!!");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

		return IPTV_ERR_INVALIDSTATUS;
	}

	// 인증 실패한 상태일 경우만
	//if (gAuthChecked == 0 || checkFolderFileSize("/data/user/0/com.skb.framework.myapplication/files/btf/btvmedia/cas/Certificates/user/1000") <= 0)
	if (gAuthChecked == 0 || checkFolderFileSize(iptv_cas_certificates_user_100) <= 0)
	{
	    CertCheck();
	}

    player.nPuaseAtMs = 0;

	player.nPollingRunFlag = 0;
    player.enable_video = true;
    player.avpInfo.isFeedBufferEOF = false;
	mQueue.cancelEvent(mAVEventCheckEvent->eventID());
    player.isFirstFrameDisplayed = 0; //jung2604 : first iframe 찾기 초기화

    setLastFrame(stPlayInfo.enableLastFrame == 1);
	postAVEventCheckEvent_l(100000);

    getKecModeSettings(&player); //kecMode check

	if (stPlayInfo.nPlayMode == 1) //case FILE PLAY
	{
		eFilePlayType = (BTV_FILE_PLAY_TYPE)stPlayInfo.nPlayType;
		int32_t tStatus = open_file_l(stPlayInfo);
        ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
        return tStatus;
	}
#ifdef USE_RTSP
	else if (stPlayInfo.nPlayMode == 2) //case rtsp
	{
		eRTSPPlayType = (BTV_RTSP_PLAY_TYPE)stPlayInfo.nPlayType;
		int32_t tStatus = open_rtsp_l(stPlayInfo);
        ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
        return tStatus;
	}
#endif

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return IPTV_ERR_INVALID;
}

int32_t IptvSocPlayer::setLastFrame(int enableLastFrame) {
    player.enableLastFrame = (enableLastFrame == 1);

#if 1
    //jung2604 : 20190403 : MAIN만 저장을 하자..
    if(player.idx_player == 0) {
        bool currentEnableLastFrame = true;
        char buff[256];
        get_systemproperty(PROPERTY__LASTFRAME, buff, 20);
        currentEnableLastFrame = (bool)(memcmp(buff, "1", 1) == 0);

        //ALOGD("setLastFrame DeviceID[%d] : enableLastFrame : %d, currentEnableLastFrame : %d", player.idx_player, enableLastFrame, currentEnableLastFrame);
        if(currentEnableLastFrame != enableLastFrame) {
            ALOGD("setLastFrame DeviceID[%d] : enableLastFrame : %d(read last frame : %d)", player.idx_player, enableLastFrame, currentEnableLastFrame);
            sprintf(buff, "%d", enableLastFrame ? 1:0 );
            set_systemproperty(PROPERTY__LASTFRAME, buff, 1);
        }
    }
#endif
    return IPTV_ERR_SUCCESS;
}


int32_t IptvSocPlayer::startDmxFilter(int32_t pid, int32_t tid) {
    dvb_clear_section_filter(&player);
    dvb_set_section_filter(&player, pid, tid, section_filter_callback);
    return IPTV_ERR_SUCCESS;
}

int32_t IptvSocPlayer::stopDmxFilter(int32_t pid, int32_t tid) {
    dvb_clear_section_filter(&player);
    return IPTV_ERR_SUCCESS;
}

#ifdef USE_MULTIVIEW

// multiview
int32_t IptvSocPlayer::tuneTV_Multiview_l(HTuner_IPTV_Info *ptTuneInfo)
{
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
    std::lock_guard<std::mutex> lock(mLock);
    ALOGD("[INFO] AFTER MUTEX");

    ALOGD(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    ALOGD("tuneTV Multiview API called DeviceID[%d]", player.idx_player);

    getKecModeSettings(&player); //kecMode check

    { // TODO : jung2604 : 20190429 : zapping time check용..
        if ( 0 == clock_gettime( CLOCK_REALTIME, &player.tTempTimeVal)) {
            player.tempTimeMs = (player.tTempTimeVal.tv_sec * 1000) + (player.tTempTimeVal.tv_nsec / 1000000);
            player.timeCheckStartTime = player.tempTimeMs;
            ALOGD("**_**================================ zapping start : %ld =================================", player.timeCheckStartTime);
        }
    }

    androidSetThreadPriority(0, ANDROID_PRIORITY_FOREGROUND);

    // 현재 play mode 를 체크해서
    if ( (InGetPlayerMode() == PLAYER_MODE_NORMAL_FILE) ||
         (InGetPlayerMode() == PLAYER_MODE_RTSP) )
    {
        // RTSP 나 FILE PLAY  이면 에러 리턴한다.
        ALOGD("[WARNING] CURRENT RTSP/FILEPLAY PLAYING. WILL RETURN ERROR");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
        return IPTV_ERR_INVALIDSTATUS;
    }

    // 현재 play mode 를 체크해서
    if ( (InGetPlayerMode() == PLAYER_MODE_IPTV)) {
            ST_StopChannelSilent(&player, &locator);
            ST_CloseChannel(&player, player.locator_head);
    }

    // 인증 실패한 상태일 경우만
    if (gAuthChecked == 0)
    {
        ALOGD("before CALL CertCheck DeviceID[%d]", player.idx_player);
        CertCheck();
        ALOGD("after CALL CertCheck DeviceID[%d]", player.idx_player);
    }

    ALOGD("[INFO] multiview enableLastFrame : %d", ptTuneInfo[0].enableLastFrame);
    setLastFrame(ptTuneInfo[0].enableLastFrame == 1);

    // 최초 signal init 셋팅한후
    nLastSignalStatus = IPTV_SIGNAL_INIT;
    nFirstFrameChanged = 0;

    player.multiviewType = ptTuneInfo[0].multiviewType;
    player.isMultiview = true; //jung2604 : Multiview 설정
    player.isFeedMultiviewData = 0;

    // 200ms buffer check event 셋팅 한다. -> timer 로
    mSocketBufferCheckEventStop = true;
    mQueue.cancelEvent(mSocketBufferCheckEvent->eventID());
    player.nPollingRunFlag = 0;
    mQueue.cancelEvent(mAVPTSTimeCheckEvent->eventID());
    mQueue.cancelAllEvent();

    // 멀티뷰에서는 cas를 사용하지 않음
    player.cbHandler = null;
    player.cbCASDescramble = null;

    int32_t tStatus = _TuneMultiviewChannel(ptTuneInfo);
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return tStatus;
}
#endif

int32_t IptvSocPlayer::tuneTV_l(HTuner_IPTV_Info stTuneInfo)
{
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
	std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");

	ALOGD(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	ALOGD("tuneTV API called DeviceID[%d]", player.idx_player);

    getKecModeSettings(&player); //kecMode check

    { // TODO : jung2604 : 20190429 : zapping time check용..
        if ( 0 == clock_gettime( CLOCK_REALTIME, &player.tTempTimeVal)) {
            player.tempTimeMs = (player.tTempTimeVal.tv_sec * 1000) + (player.tTempTimeVal.tv_nsec / 1000000);
            player.timeCheckStartTime = player.tempTimeMs;
            ALOGD("**_**================================ zapping start : %ld =================================", player.timeCheckStartTime);
        }
    }

#ifdef USE_QI_CALCULATOR
    { // jung2604 : 20190531 : qi loader 관련 실행이 안되어있다면 실행해주도록 하자.
        char retBuf[128] = {0};
        int retLen = 0;
        retLen = property_get("vendor.skb.btv.qiuploader.run", retBuf, "");

        //ALOGD("[TEST] property_get : vendor.skb.btv.qiuploader.run = %s", retBuf);
        if (retLen <= 0) { // 값이 비어있을경우 0먼저 한번 해준다.
            ALOGD("+============================+");
            ALOGD("| first property_set : vendor.skb.btv.qiuploader.run = 0");
            ALOGD("+============================+");
            property_set("vendor.skb.btv.qiuploader.run", "0");
            usleep(100000);
        }

        if (retLen <= 0 || strcmp(retBuf, "0") == 0) {
            ALOGD("+============================+");
            ALOGD("|  property_set : vendor.skb.btv.qiuploader.run");
            ALOGD("+============================+");
            property_set("vendor.skb.btv.qiuploader.run", "1");
        }
    }
#endif

//    androidSetThreadPriority(0, ANDROID_PRIORITY_FOREGROUND);

#if 0 //remove ELP
#ifndef FEATURE_SOC_AMLOGIC //TODO : amlogic 용 임시 제거
    { // jung2604 : first media에서 미디어를 재생하였는지 체크
        char retBuf[128] = {0};
        int retLen = 0;

        retLen = property_get("vendor.skb.btv.boot.early.liveplay", retBuf, "");

        if (retLen > 0 && strcmp(retBuf, "1") == 0) {
            ALOGD("+============================+");
            ALOGD("|  EarlyLivePlayback : vendor.skb.btv.boot.early.liveplay");
            ALOGD("+============================+");

            if(property_get("sys.boot_completed", retBuf, "0") > 0 && strcmp(retBuf, "1") == 0){
                //부팅이 완료 되었고, 부팅이전에 미디어가 플레이가 되었다면 처음 들어온 tune는 무시한다.
                ALOGD("Set Disable vendor.skb.btv.boot.early.liveplay !!!!");
                property_set("vendor.skb.btv.boot.early.liveplay", "0");

                if(InGetPlayerMode() == PLAYER_MODE_IPTV) {
	                ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
                    return IPTV_ERR_INVALIDSTATUS;
                }
            }
        }
    }
#endif
#endif

	// 현재 play mode 를 체크해서
	if ( (InGetPlayerMode() == PLAYER_MODE_NORMAL_FILE) ||
		(InGetPlayerMode() == PLAYER_MODE_RTSP) )
	{
		// RTSP 나 FILE PLAY  이면 에러 리턴한다.
		ALOGD("[WARNING] CURRENT RTSP/FILEPLAY PLAYING. WILL RETURN ERROR");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}

    // 현재 play mode 를 체크해서 이전에 multiview를 했을경우 stop을 먼저 해준다.
    if ( (InGetPlayerMode() == PLAYER_MODE_IPTV) && !player.isMultiview) {
        ST_StopChannelSilent(&player, &locator);
    }

	// 인증 실패한 상태일 경우만
	if (gAuthChecked == 0)
	{
		ALOGD("before CALL CertCheck DeviceID[%d]", player.idx_player);
		CertCheck();
		ALOGD("after CALL CertCheck DeviceID[%d]", player.idx_player);
	}

    setLastFrame(stTuneInfo.enableLastFrame == 1);

	// 최초 signal init 셋팅한후
	nLastSignalStatus = IPTV_SIGNAL_INIT;
	nFirstFrameChanged = 0;
    player.isMultiview = false;
    player.avpInfo.isFeedBufferEOF = false; // iframe 체크 timeout에 영향을 미친다.

    // 200ms buffer check event 셋팅 한다. -> timer 로
	mSocketBufferCheckEventStop = true;
	mQueue.cancelEvent(mSocketBufferCheckEvent->eventID());
    player.nPollingRunFlag = 0;
	mQueue.cancelEvent(mAVPTSTimeCheckEvent->eventID());
	mQueue.cancelAllEvent();

	ALOGD("[INFO] multicast_addr : %s", stTuneInfo.multicast_addr);
	ALOGD("[INFO] multicast_port : %d", stTuneInfo.multicast_port);
	ALOGD("[INFO] video_pid : %d", stTuneInfo.video_pid);
	ALOGD("[INFO] audio_pid : %d", stTuneInfo.audio_pid);
	ALOGD("[INFO] pcr_pid : %d", stTuneInfo.pcr_pid);
	ALOGD("[INFO] video_stream : %d", stTuneInfo.video_stream);
	ALOGD("[INFO] audio_stream : %d", stTuneInfo.audio_stream);
	ALOGD("[INFO] ca_systemid : %d", stTuneInfo.ca_systemid);
	ALOGD("[INFO] ca_pid : %d", stTuneInfo.ca_pid);
	ALOGD("[INFO] channel_num : %d",stTuneInfo.channel_num);
	ALOGD("[INFO] audioenable : %d",stTuneInfo.audioenable);
    ALOGD("[INFO] isMainChannelSame : %d",stTuneInfo.isMainChannelSame);
    ALOGD("[INFO] enableLastFrame : %d",stTuneInfo.enableLastFrame);
    ALOGD("[INFO] settingsAspectRatioMode : %d",player.settingsVideoScaleMode);

	// PMT Create and CAS Service start.

	if (getDeviceID() == 0) {   //0 : main , 나머지 pip

		player.channel_num = stTuneInfo.channel_num;

#ifdef USE_QI_CALCULATOR
		// qi calculator callback ???.
        player.cbQiCalcScan = (TQICalcScanCB) qi_calculator_scan;
		player.cbQiCalcOnJoinRequested = (TQICalcOnJoinRequestedCB) qi_calculator_on_join_requested;
#endif

		ALOGD("before CALL startCasService DeviceID[%d]", player.idx_player);
		//CAS 서비스 시작
		if (startCasService(stTuneInfo, 0) == 0)     // ?????????δ? CAS a?ο? ??????? ????.
		{
			player.cbHandler = (void*) this;
			player.cbCASDescramble = (TCASDemuxCB) DemuxParseMulti;

		} else {
			//player.cbHandler =  (void*) this;;
			//player.cbCASDescramble = (TCASDemuxCB) checkAndSaveTS;

            player.cbHandler = null;
			player.cbCASDescramble = null;
		}

		ALOGD("after CALL startCasService DeviceID[%d]", player.idx_player);
	}
	else if(getDeviceID() == 1)
	{
		ALOGD("before CALL startCasService DeviceID[%d]", player.idx_player);
		if (startCasService(stTuneInfo, 1) == 0)     // 내부적으로는 CAS 채널에 한해서만 실행.
		{
			player.cbHandler = (void*) this;
			player.cbCASDescramble = (TCASDemuxCB) DemuxParseMulti;

		}
		else
		{
			player.cbHandler = null;
			player.cbCASDescramble = null;
		}

		ALOGD("after CALL startCasService DeviceID[%d]", player.idx_player);
	}
	else
	{
    	   player.cbHandler = null;
		   player.cbCASDescramble = null;
	}

	if (InGetPlayerMode() == PLAYER_MODE_IPTV)
	{
		ALOGD("[INFO] will change channel");
		player.enable_audio = true;
		// 기존 tune 이 동작중이다. 즉 change channel 상태
		// pip 에서 fail 나는 경우 가 자주 발생하므로 retune 로직을 추가한다.
		if (_ChangeChannel(stTuneInfo) != IPTV_ERR_SUCCESS)
		{
			int32_t tStatus = _TuneChannel(stTuneInfo);
            ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
            return tStatus;
		}
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_SUCCESS;
	}
	else
	{
		ALOGD("[INFO] will tune channel");
		player.enable_audio = true;
		// 기존 tune 이 동작중이 아니다
		int32_t tStatus = _TuneChannel(stTuneInfo);
        ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
        return tStatus;
	}

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	// will not called
	return IPTV_ERR_SUCCESS;
}

int32_t IptvSocPlayer::stop_tv_l()
{
	int nRet = 0, nMiliSec = 0, nMultiple = 1, nGetTimePTS;
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);

    player.isMultiview = false; // jung2604 : multiview 해제

	// device 상태를 체크하여
	if (InGetDeviceOpenStatus() == DEVICE_STATUS_CLOSE)
	{
		// close 상태이면 정상 리턴한다.
		ALOGD("[WARNING] DEVICE IS CLOSED. WILL RETURN OK");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_SUCCESS;
	}

	// 현재 play mode 를 체크해서
	if (InGetPlayerMode() != PLAYER_MODE_IPTV)
	{
		// RTSP PLAY 가 아니면 에러 리턴한다.
		ALOGD("[WARNING] NOT TV PLAY. WILL RETURN ERROR");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}

	// 현재 play status 를 체크해서
	if (InGetPlayerStatus() == PLAYER_STATUS_NONE)
	{
		// None 이면 정상 리턴한다.
		ALOGD("[WARNING] TUNE IS None. WILL RETURN OK");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_SUCCESS;
	}

	mSocketBufferCheckEventStop = true;
	player.nPollingRunFlag = 0;
	mQueue.cancelEvent(mSocketBufferCheckEvent->eventID());
    mQueue.cancelAllEvent();


	LOG_TIME_STARTV("ST_StopChannel [%d]", player.idx_player);
	nRet = ST_StopChannel(&player, &locator);
	LOG_TIME_END("ST_StopChannel nRet(%d)", nRet);

	if (nRet != 0)
		goto error;

	InSetPlayerStatus(PLAYER_STATUS_STOP);
	ALOGD("[SUCCESS] stop_tv_l SUCCESS!!!!");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return IPTV_ERR_SUCCESS;

error:
	ALOGD("[ERROR] stop_tv_l FAILED!!!!");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return IPTV_ERR_ERROR;
}


int32_t IptvSocPlayer::bindingFilter_l(int nIndex, int nPID, int nTableID, int nTimeOut)
{
	ALOGD("[INFO] [%s] called", __FUNCTION__);
	std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");

	return IPTV_ERR_NOTSUP;
}

int32_t IptvSocPlayer::releaseFilter_l(int nIndex)
{
	ALOGD("[INFO] [%s] called", __FUNCTION__);
	std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");

	return IPTV_ERR_NOTSUP;
}

int32_t IptvSocPlayer::changeAudioChannel_l(int nAPID, int nACodecID)
{
	int nRet = 0;

    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
	std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");

	// device 상태를 체크하여
	if (InGetDeviceOpenStatus() == DEVICE_STATUS_CLOSE)
	{
		// close 상태이면 에러 리턴한다.
		ALOGD("[WARNING] DEVICE IS CLOSED. WILL RETURN OK");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}

	// 현재 play mode 를 체크해서
	if ( (InGetPlayerMode() != PLAYER_MODE_NORMAL_FILE) &&
		(InGetPlayerMode() != PLAYER_MODE_RTSP) &&
		(InGetPlayerMode() != PLAYER_MODE_IPTV) )
	{
		// RTSP 나 FILE PLAY 나 IPTV 가 아니면 에러 리턴한다.
		ALOGD("[WARNING] NOT RTSP/FILEPLAY. WILL RETURN ERROR");
    	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}

    locator.aid = nAPID;
    locator.acodec = nACodecID;
    convertCodecInfo(locator);

	LOG_TIME_STARTV("ST_ChangeAudio [%d] nAPID [%d] nACodecID [%d]", player.idx_player, nAPID, nACodecID);
	//nRet = ST_ChangeAudio(&player, &locator, nAPID, nACodecID);
    nRet = ST_ChangeAudio(&player, &locator);
	LOG_TIME_END("ST_ChangeAudio nRet(%d)", nRet);
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

	return IPTV_ERR_SUCCESS;
}

int32_t IptvSocPlayer::changeAudioOnOff_l(int audioenable)
{
	int nRet = 0;
    int nOnOff = 0;

    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
	std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");

	// device 상태를 체크하여
	if (InGetDeviceOpenStatus() == DEVICE_STATUS_CLOSE)
	{
		// close 상태이면 에러 리턴한다.
		ALOGD("[WARNING] DEVICE IS CLOSED. WILL RETURN OK");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}

	// 현재 play mode 를 체크해서
	if ( (InGetPlayerMode() != PLAYER_MODE_NORMAL_FILE) &&
		(InGetPlayerMode() != PLAYER_MODE_RTSP) &&
		(InGetPlayerMode() != PLAYER_MODE_IPTV) )
	{
		// RTSP 나 FILE PLAY 나 IPTV 가 아니면 에러 리턴한다.
		ALOGD("[WARNING] NOT RTSP/FILEPLAY. WILL RETURN ERROR");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}

	if (nOnOff != 0 && nOnOff != 1)
	{
		// 0도 아니고 1도 아니다.
		ALOGD("[WARNING] on/off flag set error [%d]", nOnOff);
	}

	if (audioenable == 0)
	{
		nOnOff = 1;
	}
	else
	{
		nOnOff = 0;
	}

    //jung2604 : mute값 저장
    player.save_mute_enable = !audioenable;

	LOG_TIME_STARTV("ST_AudioMute [%d]", player.idx_player);
	ST_AudioMute(&player, nOnOff);
	LOG_TIME_END("ST_AudioMute nRet(%d)", nRet);

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return IPTV_ERR_SUCCESS;
}


int32_t IptvSocPlayer::stop_file_l(int isEOF)
{
	int nRet = 0, nMiliSec;
    long long nGetTimePTS;
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
	std::lock_guard<std::mutex> lock(mLockStopFile);

	ALOGD("[%s] called", __FUNCTION__);

	// device 상태를 체크하여
	if (InGetDeviceOpenStatus() == DEVICE_STATUS_CLOSE)
	{
		// close 상태이면 정상 리턴한다.
		ALOGD("[WARNING] DEVICE IS CLOSED. WILL RETURN OK");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_SUCCESS;
	}

	// 현재 play mode 를 체크해서
	if (InGetPlayerMode() != PLAYER_MODE_NORMAL_FILE)
	{
		//FILE PLAY 가 아니면 에러 리턴한다.
		ALOGD("[WARNING] NOT FILEPLAY. WILL RETURN ERROR");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}

	// 현재 play status 를 체크해서
	if (InGetPlayerStatus() == PLAYER_STATUS_NONE ||
		InGetPlayerStatus() == PLAYER_STATUS_STOP)
	{
		// None 이나 Stop 이면 정상 리턴한다.
		ALOGD("[WARNING] RTSP/FILEPLAY IS Already Stop Or None. WILL RETURN OK");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_SUCCESS;
	}

	// 현재 timer 종료
	mQueue.cancelEvent(mFilePlayTimeCheckEvent->eventID());
	nSendFILEADVIMP = 0;
	nCheckFileTimeCount = 0;

	nGetTimePTS = ST_GetTimeStamp(&player);

	if (nGetTimePTS < 0)
	{
		ALOGD(" PTS IS MINUS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        ALOGD(" PTS IS MINUS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        ALOGD(" PTS IS MINUS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        ALOGD(" PTS IS MINUS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        ALOGD(" PTS IS MINUS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        ALOGD(" PTS IS MINUS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        ALOGD(" PTS IS MINUS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

		nGetTimePTS = 0;
	}


	LOG_TIME_STARTV("ST_StopStDvbBuffer [%d]", player.idx_player);
	nRet = ST_StopStDvbBuffer(&player, &locator);
	LOG_TIME_END("ST_StopStDvbBuffer nRet(%d)", nRet);

	switch(nRet)
	{
	case 0:
		ALOGD("[SUCCESS] ST_StopStDvbBuffer !!!!");
		break;
	case -1:
		ALOGD("[ERROR] dvb_tuner_unlock!!!!");
		goto error;
	case -4:
		ALOGD("[ERROR] dvb_clear_av_buffer!!!!");
		goto error;
	case -5:
		ALOGD("[ERROR] ST_ReopenDMXDevice!!!!");
		goto error;
	case -7:
		ALOGD("[ERROR] dvb_set_stm_dvb_option!!!!");
		goto error;
	}

	nMiliSec = (int)((nGetTimePTS / 90) - 10000); // 왜 일까 전부 10초 부터 시작한다.
	if (nMiliSec < 0)
		nMiliSec = 0;

	SendFilePlayStatus(nMiliSec, STATUS_FILE_STOP, isEOF);
	InSetPlayerStatus(PLAYER_STATUS_STOP);

	ALOGD("[SUCCESS] stop_file_l SUCCESS!!!!");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return IPTV_ERR_SUCCESS;

error:
	ALOGD("[ERROR] stop_file_l FAILED!!!!");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return IPTV_ERR_ERROR;
}

#ifdef USE_RTSP
int32_t IptvSocPlayer::stop_rtsp_l()
{
	int nRet = 0, nMiliSec = 0, nMultiple = 1, nGetTimePTS;
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
	// device 상태를 체크하여
	if (InGetDeviceOpenStatus() == DEVICE_STATUS_CLOSE)
	{
		// close 상태이면 정상 리턴한다.
		ALOGD("[WARNING] DEVICE IS CLOSED. WILL RETURN OK");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_SUCCESS;
	}

	// 현재 play mode 를 체크해서
	if (InGetPlayerMode() != PLAYER_MODE_RTSP)
	{
		// RTSP PLAY 가 아니면 에러 리턴한다.
		ALOGD("[WARNING] NOT RTSP PLAY. WILL RETURN ERROR");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}

	// 현재 play status 를 체크해서
	if (InGetPlayerStatus() == PLAYER_STATUS_NONE ||
		InGetPlayerStatus() == PLAYER_STATUS_STOP)
	{
		// None 이나 Stop 이면 정상 리턴한다.
		ALOGD("[WARNING] RTSP/FILEPLAY IS Already Stop Or None. WILL RETURN OK");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_SUCCESS;
	}

    // jung2604
    {
        int nStatus = InGetPlayerStatus();
        if (nStatus >= PLAYER_STATUS_2XFF && nStatus <= PLAYER_STATUS_16XBW) {
            //jung2604 : 소리가 켜져있어야 하는경우에만 켠다..
            if (!player.save_mute_enable) ST_AudioMute(&player, 0);
        }
    }

	mQueue.cancelEvent(mRTSPPlayTimeCheckEvent->eventID());
	nSendRTSPADVIMP = 0;

	RTSP_Stop(nRTSPSessionID);

	if (InGetPlayerStatus() == PLAYER_STATUS_PAUSE)
	{
		LOG_TIME_STARTV("ST_ResumeBuffer [%d]", player.idx_player);
		nRet = ST_ResumeBuffer(&player);
		LOG_TIME_END("ST_ResumeBuffer nRet(%d)", nRet);

		switch(nRet)
		{
		case 0:
			ALOGD("[SUCCESS] ST_ResumeBuffer !!!!");
			break;
		default:
			ALOGD("[ERROR] unknown ERROR [%d]!!!!", nRet);
			return IPTV_ERR_ERROR;
		}
	}

	if (mRTSPEOFTime == -1) // case not eof stop
	{
		nMiliSec = mRTSPCurrentPlayTimePTS / 90;
	}
	else
	{
		nMiliSec = mRTSPEOFTime;
	}

	SendRTSPPlayStatus(nMiliSec, STATUS_RTSP_STOP);

	InSetPlayerStatus(PLAYER_STATUS_STOP);
	ALOGD("[SUCCESS] stop_rtsp_l SUCCESS!!!!");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return IPTV_ERR_SUCCESS;

error:
	ALOGD("[ERROR] stop_rtsp_l FAILED!!!!");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);

    return IPTV_ERR_ERROR;
}

int IptvSocPlayer::checkRtspUncontrollableState() {
    // jung2604 : 20190520 : vod를 제어할수 없을때(EOF를 받았을때) 명령을 무시하도록 한다.
    if(player.avpInfo.isFeedBufferEOF == true) {//if (RTSP_GetEOFBOFStatus(nRTSPSessionID) == 1 || player->EOFReached == true) {
        ALOGD("[INFO] [%s] called, EOFReached = TRUE", __FUNCTION__);
        return 1;
    }
    return 0;
}
#endif

int32_t IptvSocPlayer::seek_l(long nSec, int puaseFlag)
{
#ifdef USE_RTSP
	int nRet;
    ALOGD("BTF|%s|%d|IN|  sec : %d\n", __FUNCTION__,__LINE__, nSec);
	std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");

	// 현재 play mode 를 체크해서
	if (InGetPlayerMode() != PLAYER_MODE_RTSP)
	{
		// RTSP PLAY 가 아니면 에러 리턴한다.
		ALOGD("[WARNING] NOT RTSP PLAY. WILL RETURN ERROR");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}

	// 현재 play status 를 체크해서
	if (InGetPlayerStatus() == PLAYER_STATUS_NONE ||
		InGetPlayerStatus() == PLAYER_STATUS_STOP)
	{
		// None 이나 Stop 이면 에러 리턴한다.
		ALOGD("[WARNING] RTSP IS Already Stop Or None. WILL RETURN ERROR");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}

	// jung2604 : 20190520 : vod를 제어할수 없을때(EOF를 받았을때) 명령을 무시하도록 한다.
	if(checkRtspUncontrollableState()) return IPTV_ERR_INVALIDSTATUS;

    //jung2604
    {
        int nStatus = InGetPlayerStatus();
        if (nStatus >= PLAYER_STATUS_2XFF && nStatus <= PLAYER_STATUS_16XBW) {
            //jung2604 : 소리가 켜져있어야 하는경우에만 켠다..
            if (!player.save_mute_enable) ST_AudioMute(&player, 0);
        }
    }

	mRTSPCalculateTimeCheckExceptWait = 1;
	mJumpRtspStatusSend = 1; // 반드시 나가기 전에 0으로 맞춰야 한다.!!!!

    //jung2604 : seek후 다시 시간이 이전으로 돌아가는 증상이 있다.. mJumpRtspStatusSend를 설정하는 것 만으로는 안된다.. 이벤트를 취소하고 다시 시작하자..
    mQueue.cancelEvent(mRTSPPlayTimeCheckEvent->eventID());

	RTSP_SetWaitWriteDVR(nRTSPSessionID, 1);

	LOG_TIME_STARTV("ST_FlushAVBuffer [%d]", player.idx_player);
	nRet = ST_FlushAVBuffer(&player);
	LOG_TIME_END("ST_FlushAVBuffer nRet(%d)", nRet);

	LOG_TIME_STARTV("RTSP_WaitWriteCheckDVR [%d]", player.idx_player);
	nRet = RTSP_WaitWriteCheckDVR(nRTSPSessionID);
	LOG_TIME_END("RTSP_WaitWriteCheckDVR nRet(%d)", nRet);

    // jung2604 : 위치 조정
	if (InGetPlayerStatus() == PLAYER_STATUS_PAUSE) {
		LOG_TIME_STARTV("ST_ResumeBuffer [%d]", player.idx_player);
		nRet = ST_ResumeBuffer(&player);
		LOG_TIME_END("ST_ResumeBuffer nRet(%d)", nRet);

		switch(nRet)
		{
		case 0:
			ALOGD("[SUCCESS] ST_ResumeBuffer !!!!");
			break;
		default:
			ALOGD("[ERROR] unknown ERROR [%d]!!!!", nRet);
			return IPTV_ERR_ERROR;
		}
	}

	RTSP_Seek(nRTSPSessionID, nSec);//nSec * 1000); //long으로 수정

	RTSP_SetWaitWriteDVR(nRTSPSessionID, 0);

	InSetPlayerStatus(PLAYER_STATUS_PLAY);

	SendRTSPPlayStatus(nSec /*nSec * 1000*/, STATUS_RTSP_PLAY); //jung2604 : 위로 올린다.//long으로 수정

    player.nPollingRunFlag = 1;

	long long nTestSec = ST_GetTimeStamp(&player);
	ALOGD("before time %lld ", nTestSec / 90);
	LOG_TIME_STARTV("before call WaitTimeMatch to Get FirstFrame");
    player.isFirstFrameDisplayed = 0; //jung2604 : first iframe 찾기 초기화
	nRet = WaitTimeMatch(0, 0, 0, 0, 0, 1);
	LOG_TIME_END("after call WaitTimeMatch to Get FirstFrame nRet (%d)" ,nRet);
	nTestSec = ST_GetTimeStamp(&player);
	ALOGD("after time %lld ", nTestSec / 90);
	player.nPollingRunFlag = 0;

    // jung2604 : resume을 seek가 완료된뒤 한다.
    //nRet = ST_ResumeBuffer(&player);

    //SendRTSPPlayStatus(nSec * 1000, STATUS_RTSP_PLAY); //jung2604 : 위로 올린다.

	mRTSPWannaBeGonePTS = nSec * 90; //nSec * 1000 * 90; //long으로 수정
    mRTSPCurrentPlayTimePTS = mRTSPWannaBeGonePTS; //jung2604 : UI가 뒤로 돌아가지 않게 하기 위해서 현재의 PTS정보를 미리 갱신해준다.
	mRTSPCalculateTimeCheck = 1;
	while (mRTSPCalculateTimeCheck == 1)
	{
		usleep(3000);
	}

	mJumpRtspStatusSend = 0; // 반드시 나가기 전에 0으로 맞춰야 한다.!!!!
	mRTSPCalculateTimeCheckExceptWait = 0;

    //jung2604 : seek후 다시 시간이 이전으로 돌아가는 증상이 있다.. mJumpRtspStatusSend를 설정하는 것 만으로는 안된다.. 500ms후에 시작하자..
    postRTSPPlayTimeCheckEvent_l(500000);

	SendRTSPSeekCompleteEvent(); //juing2604 : 추가..살이있는 동화에서 Seek 완료후 메시지 받기를 원한..

	if(puaseFlag) {
        pause_impl();
	}

	ALOGD("[SUCCESS] seek_l SUCCESS!!!!");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
#endif
    return IPTV_ERR_SUCCESS;
}

int32_t IptvSocPlayer::pauseAt_l(long nMs) {
    ALOGD("BTF|%s|%d|IN|  sec : %d\n", __FUNCTION__,__LINE__, nMs);
    std::lock_guard<std::mutex> lock(mLock);
    ALOGD("[INFO] AFTER MUTEX");

    player.nPuaseAtMs = nMs;

    ALOGD("[SUCCESS] seek_l SUCCESS!!!!");
    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
    return IPTV_ERR_SUCCESS;
}

int32_t IptvSocPlayer::keepLastFrame(int flag) {
    ALOGD("BTF|%s|%d|IN|  flag : %d\n", __FUNCTION__,__LINE__, flag);
    std::lock_guard<std::mutex> lock(mLock);
    ALOGD("[INFO] AFTER MUTEX");

    setLastFrame(flag == 1);

    ALOGD("[SUCCESS] seek_l SUCCESS!!!!");
    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
    return IPTV_ERR_SUCCESS;
}

int32_t IptvSocPlayer::trick_l(int nTrick)
{
#ifdef USE_RTSP
	int nRet, nMiliSec = 0, nSpeed, nGetTimePTS, nChangedFlag;
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
	std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");

	// 현재 play mode 를 체크해서
	if (InGetPlayerMode() != PLAYER_MODE_RTSP)
	{
		// RTSP PLAY 가 아니면 에러 리턴한다.
		ALOGD("[WARNING] NOT RTSP PLAY. WILL RETURN ERROR");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}

	// 현재 광고 상태인지 체크한다.
	if (eRTSPPlayType == TYPE_RTSP_ADV)
	{
		// 광고 상태이면 리턴한다..
		ALOGD("[WARNING] RTSP IS ADV STATUS so return SUCCESS");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_SUCCESS;
	}

	// 현재 play status 를 체크해서
	if (InGetPlayerStatus() == PLAYER_STATUS_NONE ||
		InGetPlayerStatus() == PLAYER_STATUS_STOP)
	{
		// None 이나 Stop 이면 에러 리턴한다.
		ALOGD("[WARNING] RTSP IS Already Stop Or None. WILL RETURN ERROR");
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}

	if (RTSP_GetEOFBOFStatus(nRTSPSessionID) == 2) // case bof 
	{
		usleep(1000000); // 1초간 대기한다.
	    ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
		return IPTV_ERR_INVALIDSTATUS;
	}


	if (InGetPlayerStatus() == PLAYER_STATUS_PAUSE)
	{
		LOG_TIME_STARTV("ST_ResumeBuffer [%d]", player.idx_player);
		nRet = ST_ResumeBuffer(&player);
		LOG_TIME_END("ST_ResumeBuffer nRet(%d)", nRet);
	}

    // jung2604 : 20190520 : vod를 제어할수 없을때(EOF를 받았을때) 명령을 무시하도록 한다.
    if(checkRtspUncontrollableState()) return IPTV_ERR_INVALIDSTATUS;

    //jung2604
    {
        if(nTrick >= 0 && nTrick <= 3) {
            ALOGD("++Trick [%d], nTrick >= 0 && nTrick <= 3!!!!", nTrick);
            int nStatus = InGetPlayerStatus();
            if (nStatus >= PLAYER_STATUS_2XFF && nStatus <= PLAYER_STATUS_16XBW) {
                play_rtsp_l(0);
                //jung2604 : 소리가 켜져있어야 하는경우에만 켠다..
				if (!player.save_mute_enable) ST_AudioMute(&player, 0);
            }
        }
    }

    switch(nTrick)
	{
        case 0: // 1배속
            // TODO : jung2604
            {
                int nStatus = InGetPlayerStatus();
                if (nStatus >= 0 && nStatus <= 3) {
                    RTSP_SetPlaySpeed(nRTSPSessionID, 1.0);
                }
             }
            ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
            return IPTV_ERR_SUCCESS;
        case  1:    //0.8 배속
            RTSP_SetPlaySpeed(nRTSPSessionID, 0.8);
            ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
            return IPTV_ERR_SUCCESS;
        case 2:     //1.2 배속
            RTSP_SetPlaySpeed(nRTSPSessionID, 1.2);
            ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
            return IPTV_ERR_SUCCESS;
        case  3:    //1.5배속
            RTSP_SetPlaySpeed(nRTSPSessionID, 1.5);
            ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
            return IPTV_ERR_SUCCESS;
	case PLAYER_STATUS_2XFF:
		nSpeed = 2;
		break;
	case PLAYER_STATUS_4XFF:
		nSpeed = 4;
		break;
	case PLAYER_STATUS_8XFF:
		nSpeed = 8;
		break;
	case PLAYER_STATUS_16XFF:
		nSpeed = 16;
		break;
	case PLAYER_STATUS_2XBW:
		nSpeed = -2;
		break;
	case PLAYER_STATUS_4XBW:
		nSpeed = -4;
		break;
	case PLAYER_STATUS_8XBW:
		nSpeed = -8;
		break;
	case PLAYER_STATUS_16XBW:
		nSpeed = -16;
		break;
	default:
		nSpeed = 2;
		nTrick = PLAYER_STATUS_2XFF;
		break;
	}

	ALOGD("[INFO] Trick speed [%d]!!!!", nSpeed);

	if (InGetPlayerStatus() >= PLAYER_STATUS_2XBW && nSpeed < 0) // REW 상태에서 다시 REW 진입시
	{
		if (mRTSPCurrentPlayTimePTS / 90 < 3000) // 3초보다 작으면
		{
		
			usleep(1000000); // 1초간 대기한다.
	        ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
			return IPTV_ERR_INVALIDSTATUS;
		}
	}

	/*
	if (InGetPlayerStatus() == PLAYER_STATUS_PLAY) // play 상태에서 rew 진입시
	{
		printf("adsfasdf [%d]\n", mRTSPCurrentPlayTimePTS / 90);
		if (mRTSPCurrentPlayTimePTS / 90 < 1000) // 1초보다 작으면
		{
			usleep(1000000); // 1초간 대기한다.
			return IPTV_ERR_INVALIDSTATUS;
		}
	}
	*/

	mRTSPCalculateTimeCheckExceptWait = 1;

	RTSP_SetWaitWriteDVR(nRTSPSessionID, 1);

	mJumpRtspStatusSend = 1; // 반드시 나가기 전에 0으로 맞춰야 한다.!!!!

    LOG_TIME_STARTV("ST_PauseBuffer [%d]", player.idx_player);
    nRet = ST_PauseBuffer(&player);
    LOG_TIME_END("ST_PauseBuffer nRet(%d)", nRet);

	// pause 상태 였으면
	if (InGetPlayerStatus() == PLAYER_STATUS_PAUSE)
	{
		mRTSPBaseTime = mRTSPPauseTime * 90;
        ALOGD("Trick pause status mRTSPBaseTIme = %d (%d millisec))", mRTSPBaseTime, mRTSPBaseTime/90);
	}
	// play 상태 였으면
	else if (InGetPlayerStatus() == PLAYER_STATUS_PLAY)
	{
		// wil pause work doing
//		LOG_TIME_STARTV("ST_PauseBuffer [%d]", player.idx_player);
//		nRet = ST_PauseBuffer(&player);
//		LOG_TIME_END("ST_PauseBuffer nRet(%d)", nRet);
		
		// base pts time 을 현재로 셋팅한다.		
		mRTSPBaseTime = mRTSPCurrentPlayTimePTS;
		ALOGD("Trick normal status mRTSPBaseTIme = %d (%d millisec))", mRTSPBaseTime, mRTSPBaseTime/90);
	}
	
	// trick 이었을 경우
	else if (InGetPlayerStatus() >= PLAYER_STATUS_2XFF)
	{
		// wil pause work doing
//		LOG_TIME_STARTV("ST_PauseBuffer [%d]", player.idx_player);
//		nRet = ST_PauseBuffer(&player);
//		LOG_TIME_END("ST_PauseBuffer nRet(%d)", nRet);

		// base pts time 을 현재로 셋팅한다.		
		mRTSPBaseTime = mRTSPCurrentPlayTimePTS;
		ALOGD("Trick trick status mRTSPBaseTIme = %d (%d millisec))", mRTSPBaseTime, mRTSPBaseTime/90);
	} else {
        mRTSPBaseTime = mRTSPCurrentPlayTimePTS;
        ALOGD("!!!!!!!!!!!!!!!!!!!!!!Trick trick status = %d , mRTSPBaseTIme = %d (%d millisec))", InGetPlayerStatus(), mRTSPBaseTime, mRTSPBaseTime/90);
	}

    //jung2604 : trick하는 동안 음소거를 한다.,
    ALOGD("trick_l set mute [%s] ", player.save_mute_enable ? "mute on":"mute off");
    if (!player.save_mute_enable) ST_AudioMute(&player, 1);

	LOG_TIME_STARTV("ST_FlushAVBuffer [%d]", player.idx_player);
	nRet = ST_FlushAVBuffer(&player);
	LOG_TIME_END("ST_FlushAVBuffer nRet(%d)", nRet);

	LOG_TIME_STARTV("RTSP_WaitWriteCheckDVR [%d]", player.idx_player);
	nRet = RTSP_WaitWriteCheckDVR(nRTSPSessionID);
	LOG_TIME_END("RTSP_WaitWriteCheckDVR nRet(%d)", nRet);

    SendRTSPPlayStatus(mRTSPBaseTime / 90, STATUS_RTSP_PLAY);

    RTSP_Trick(nRTSPSessionID, (float)nSpeed, mRTSPBaseTime / 90);  //주인넷 TRICK

	RTSP_SetWaitWriteDVR(nRTSPSessionID, 0);

	LOG_TIME_STARTV("ST_ResumeBuffer [%d]", player.idx_player);
	nRet = ST_ResumeBuffer(&player);
	LOG_TIME_END("ST_ResumeBuffer nRet(%d)", nRet);

	//  2배속 이상일때 avsync를 무시하도록 알린다.
    HalBtv_SetCommand(player.idx_player, COMMAND_CHANGE_NO_AVSYNC_MODE, NULL, NULL);

	InSetPlayerStatus((BTV_PLAYER_STATUS)nTrick);

	player.nPollingRunFlag = 1;
	long long nTestSec = ST_GetTimeStamp(&player);
	ALOGD("before time %lld ", nTestSec);
	LOG_TIME_STARTV("before call WaitTimeMatch to Get FirstFrame");
    player.isFirstFrameDisplayed = 0; //jung2604 : first iframe 찾기 초기화
	nRet = WaitTimeMatch(0, 0, 0, 0, 0, 1);
	LOG_TIME_END("after call WaitTimeMatch to Get FirstFrame nRet (%d)" ,nRet);
	player.nPollingRunFlag = 0;
	nTestSec = ST_GetTimeStamp(&player);
	ALOGD("after time %lld ", nTestSec);

	mRTSPCalculateTimeCheck = 1;
	while (mRTSPCalculateTimeCheck == 1)
	{
		usleep(3000);
	}

	mRTSPCalculateTimeCheckExceptWait = 0;

	mJumpRtspStatusSend = 0; // 반드시 나가기 전에 0으로 맞춰야 한다.!!!!

	ALOGD("[SUCCESS] trick_l SUCCESS!!!!");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
#endif
    return IPTV_ERR_SUCCESS;
}

//int32_t IptvSocPlayer::setPlayerSurfaceView_l(int deviceID, uint32_t nativWindows)
int32_t IptvSocPlayer::setPlayerSurfaceView_l(int deviceID, uint64_t nativWindows) //for 64bit build
{

    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
	std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");
	
	LOG_TIME_STARTV("ST_setPlayerSurfaceView[%d] ", player.idx_player);
#if 0
    ST_setPlayerSurfaceView(&player, deviceID, nativWindows);
#else
	mAnativeWindow = (ANativeWindow *)nativWindows;
	//ANativeWindow *window = (ANativeWindow *)(nativWindows);
    player.m_pAnative = (void *)mAnativeWindow;
    int width = ANativeWindow_getWidth((ANativeWindow *)player.m_pAnative);
    int height = ANativeWindow_getHeight((ANativeWindow *)player.m_pAnative);
    ALOGD("++--++ [device:%d] Got window %p [%d x %d]", deviceID, player.m_pAnative, width, height);
	//ST_setPlayerSurface(&player, deviceID, player.m_pAnative);
#endif
	LOG_TIME_END("ST_setPlayerSurfaceView");
	ALOGD("After Call ST_setPlayerSurfaceView API DeviceID[%d]", player.idx_player);

	ALOGD("[%s] called", __FUNCTION__);
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return IPTV_ERR_SUCCESS;
}

#if 0//def FEATURE_SOC_AMLOGIC

int32_t IptvSocPlayer::setPlayerSurface_l(int deviceID, const std::shared_ptr<Surface> &surface)
{
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
	//std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");
	
	//mAnativeWindow = sp<ANativeWindow>(surface);
    mAnativeWindow = const std::shared_ptr<ANativeWindow>(surface);

    player.m_pAnative = mAnativeWindow.get();
    int width = ANativeWindow_getWidth((ANativeWindow *)player.m_pAnative);
    int height = ANativeWindow_getHeight((ANativeWindow *)player.m_pAnative);
    ALOGD("++--++ [device:%d] Got window %p [%d x %d]", deviceID, player.m_pAnative, width, height);
	
	//player.m_pSurface = pSurface;
	ST_setPlayerSurface(&player, deviceID, player.m_pAnative);
	
	ALOGD("[%s] called", __FUNCTION__);
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return IPTV_ERR_SUCCESS;
}
#endif

int32_t IptvSocPlayer::setWindowSize_l(int nX, int nY, int nWidth, int nHeight)
{
    ALOGD("BTF|%s|%d|IN| x=%d y=%d width=%d height=%d \n", __FUNCTION__,__LINE__, nX, nY, nWidth, nHeight);
	std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");

	ALOGD("setWindowSize API called DeviceID[%d] x [%d] y [%d] width [%d] height [%d]", player.idx_player, nX, nY, nWidth, nHeight);
	LOG_TIME_STARTV("ST_SetWindowSize [%d]", player.idx_player);
	ST_SetWindowSize(&player, nX, nY, nWidth, nHeight);
	LOG_TIME_END("ST_SetWindowSize");
	ALOGD("After Call ST_SetWindowSize API DeviceID[%d]", player.idx_player);

	ALOGD("[%s] called", __FUNCTION__);
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return IPTV_ERR_SUCCESS;
}

int32_t IptvSocPlayer::setAVOffset_l(int type, int time_ms)
{
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
    std::lock_guard<std::mutex> lock(mLock);

    AVP_AVoffset offset;
    offset.type = (AVP_PES_type)type;
    offset.delay_time_ms = time_ms;
    if(offset.type == AVP_PES_VIDEO)
    {
      player.videoDelayTimeMs = time_ms;
    }
    ALOGD("[%s] called, type : %d  TimeMs : %d", __FUNCTION__, offset.type, offset.delay_time_ms);
    ST_SetAVOffset(&player, offset);

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
    return IPTV_ERR_SUCCESS;
}

int IptvSocPlayer::videoDelayTimeMs = 0;
int32_t IptvSocPlayer::setVideoDelay_l(int delay) {
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
    std::lock_guard<std::mutex> lock(mLock);
    ALOGD("[INFO] AFTER MUTEX");

    player.videoDelayTimeMs = videoDelayTimeMs;

    AVP_AVoffset offset;
    offset.type = AVP_PES_VIDEO;
    offset.delay_time_ms = player.videoDelayTimeMs;
    ST_SetVideoDelay(&player, offset);

    ALOGD("[%s] called, videoDelayTimeMs : %d", __FUNCTION__, player.videoDelayTimeMs);
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
    return IPTV_ERR_SUCCESS;
}

int IptvSocPlayer::enableGlobalMute = 0;
int32_t IptvSocPlayer::changeGlobalAudioMute_l() {
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
    std::lock_guard<std::mutex> lock(mLock);
    ALOGD("[INFO] AFTER MUTEX");

    player.enableGlobalMute = enableGlobalMute;

    // device 가 close 상태라면
    if (InGetDeviceOpenStatus() == DEVICE_STATUS_CLOSE) {
        // 정상 리턴한다.
        ALOGD("[%s] called, mute : %d", __FUNCTION__, player.enableGlobalMute);
        ALOGD("[WARNING] DEVICE IS CLOSED. WILL RETURN OK");
        return IPTV_ERR_SUCCESS;
    }

    ST_ChangeGlobalAudioMute(&player);

    ALOGD("[%s] called, mute : %d", __FUNCTION__, player.enableGlobalMute);
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
    return IPTV_ERR_SUCCESS;
}

AVP_AudioOutputMode IptvSocPlayer::enableSettingsDolby = AVP_AUDIO_PCM;
int32_t IptvSocPlayer::changeSettingDolby() {
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
    std::lock_guard<std::mutex> lock(mLock);
    ALOGD("[INFO] AFTER MUTEX");

    player.enableSettingsDolby = enableSettingsDolby;

    ST_ChangeDolbyMode(&player, player.enableSettingsDolby);

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
    return IPTV_ERR_SUCCESS;
}

AVP_AspectRatioMode IptvSocPlayer::settingsAspectRatioMode = AVP_ASPECT_RATIO_ORIGINAL_SCREEN;
int32_t IptvSocPlayer::changeAspectRatioMode() {
    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
    std::lock_guard<std::mutex> lock(mLock);
    ALOGD("[INFO] AFTER MUTEX");

    player.settingsVideoScaleMode = settingsAspectRatioMode;

    ST_AspectRatioMode(&player, settingsAspectRatioMode);

	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
    return IPTV_ERR_SUCCESS;
}

AVP_AspectRatioMode IptvSocPlayer::getAspectRationMode(){
    return settingsAspectRatioMode;
}

void IptvSocPlayer::setAspectRationMode(int rationMode){
    if(rationMode == 0) {
        settingsAspectRatioMode = AVP_ASPECT_RATIO_ORIGINAL_SCREEN;
    } else {
        settingsAspectRatioMode = AVP_ASPECT_RATIO_FULL_SCREEN;
    }
}

int32_t IptvSocPlayer::reset_l()
{

    ALOGD("BTF|%s|%d|IN| \n", __FUNCTION__,__LINE__);
    ALOGD("++reset_l in");

    ST_ReleasePlayer(&player);

    ALOGD("++reset_l out");
	ALOGD("BTF|%s|%d|OUT| \n", __FUNCTION__,__LINE__);
	return IPTV_ERR_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////////////////////

void IptvSocPlayer::onFilePlayTimeCheckEvent()
{
//	ALOGD("[INFO] [%s] called", __FUNCTION__);
	std::lock_guard<std::mutex> lock(mLock);
//	ALOGD("[INFO] AFTER MUTEX");

	int nMiliSec;

	if (InGetPlayerStatus() == PLAYER_STATUS_PLAY)
	{
		if (ST_IsFilePlayEOFReached(&player) == 1) // EOF reached
		{
			ALOGD("[INFO] EOF REACHED so will stop file play");
			postFileCloseTimeCheckEvent_l(100);
		}

		if (eFilePlayType == TYPE_FILE_ADV) // 광고 일 경우
		{
			if (++nCheckFileTimeCount % 5 == 0) // 1초마다 체크한다. // 최초 쓰레기가 온다.
			{	
				nMiliSec = (int)((ST_GetTimeStamp(&player) / 90) - 10000); // 왜 일까 전부 10초 부터 시작한다.
				if (nMiliSec < 0)
					nMiliSec = 0;
				
				// 우선 7초에 file adv impl 을 보내기로 한다 추후 변경
				if (nSendFILEADVIMP == 0 && nMiliSec >= 7000)
				{
					nSendFILEADVIMP = 1;
					ALOGD("[INFO] IMPL send event");
					SendFilePlayStatus(nMiliSec, STATUS_FILE_PLAY);
				}
			}
		}
		else // 일반 file 일 경우
		{
			if (++nCheckFileTimeCount % 5 == 0) // 1초마다 체크한다. 최초 쓰레기가 온다.
			{
				nMiliSec = (int)((ST_GetTimeStamp(&player) / 90) - 10000); // 왜일까 전부 10초부터 시작한다.
				if (nMiliSec < 0)
					nMiliSec = 0;
				
				// 우선 7초에 file adv impl 을 보내기로 한다 추후 변경
				ALOGD("[INFO] PLAY send event");
				SendFilePlayStatus(nMiliSec, STATUS_FILE_PLAY);
					
			}
		}

	}
	else
	{
		ALOGD("not play status current %d will skip", InGetPlayerStatus());
	}

	postFilePlayTimeCheckEvent_l(200000);

}

void IptvSocPlayer::onFileCloseTimeCheckEvent()
{
	// 다음과 같은 현상이 있다. 2013.11.22
	// 1. 가끔 영상이 깨지는 현상이 발생한다.
	// - video flush 정상 진행하나 -> audio flush 때 깨지는 컨텐츠 다수
	// - audio flush -> video flush 로 바꿀경우 audio flush 정상진행하나 video flush 에서 깨지는 컨텐츠 다수
	//    - audio flush -> video flush 일경우 video flush 에서 전체적으로 blocking 이 걸릴 경우가 가끔 발생한다. ooooops!

	stop_file_l(1); //jung2604 : set EOF
	ALOGD("[INFO] onFileCloseTimeCheckEvent END");
}

void IptvSocPlayer::onSocketBufferCheckEvent()
{
//	ALOGD("[INFO] [%s] called [%d]", __FUNCTION__, player.idx_player);
	std::lock_guard<std::mutex> lock(mLock);
//	ALOGD("[INFO] AFTER MUTEX");

	int nRet = 0, nSendEvent = 0;

	// will check socket buffer
	nRet = ST_PlayerGetBufferStatus(&player);


        if (nRet == 0) // case good!
	{
		if (nLastSignalStatus != IPTV_SIGNAL_GOOD)
		{
			nLastSignalStatus = IPTV_SIGNAL_GOOD;
			ALOGD("[GOOD] Send multicast data received GOOD device = [%d]!!", player.idx_player);
			if (nFirstFrameChanged != 0)
			{			
				nSendEvent = 1;
			}
		}
	}

	if (nRet == -1) // case bad
	{
		nLastSignalStatus = IPTV_SIGNAL_BAD;
		nSendEvent = 1;
		if(player.isAvpStart == 0) ALOGD("[WARNING] multicast data received delay  device = [%d] !! (close device) ", player.idx_player);
		else  ALOGD("[WARNING] multicast data received delay  device = [%d] !!", player.idx_player);
	}

	if (nRet == -2) // case update
	{
		if (nLastSignalStatus != IPTV_SIGNAL_UPDATE)
		{
			nLastSignalStatus = IPTV_SIGNAL_UPDATE;
			nSendEvent = 1;
		}
	}

	if (nSendEvent == 1)
	{
		notifyListener_l(TUNE_EVENT, player.idx_player, nLastSignalStatus, NULL); // send good or bad
	}

	if (mSocketBufferCheckEventStop == false) {
        mQueue.cancelEvent(mSocketBufferCheckEvent->eventID());
        if(player.isAvpStart == 0) postSocketBufferCheckEvent_l(500000);
        else postSocketBufferCheckEvent_l(2000000);
    }// else mQueue.cancelEvent(mSocketBufferCheckEvent->eventID());
}


#ifdef USE_RTSP
void IptvSocPlayer::onRTSPPlayTimeCheckEvent()
{
	ALOGD("[INFO] [%s] called", __FUNCTION__);
	std::lock_guard<std::mutex> lock(mLock);
	ALOGD("[INFO] AFTER MUTEX");

	int nChangedFlag;
	long nMiliSec = 0;
	long long nLastTime = 0;
	long long nGetTimePTS = mRTSPCurrentPlayTimePTS;
	int nMultiple = 1;
	long long nDuration = RTSP_GetDuration(nRTSPSessionID);


	int nIntervalMiliSec = 0;	// absolute value
	ALOGD("[nGetTimePTS] is %lld", nGetTimePTS);

	if (mJumpRtspStatusSend == 1)
	{
		ALOGD("jump rtsp time status send!!!");
		postRTSPPlayTimeCheckEvent_l(1000000);
		return;
	}

	if (InGetPlayerStatus() == PLAYER_STATUS_PLAY)
	{
		ALOGD("[STATUS] PLAY");

		if (mRTSPWaitTimeChanged == 1)
		{
			ALOGD("[STATUS] play status is not worked! so drop dis event");
			postRTSPPlayTimeCheckEvent_l(1000000);
			return;
		}
	}
	else if (InGetPlayerStatus() == PLAYER_STATUS_PAUSE)
	{
		ALOGD("[STATUS] PAUSE");
		SendRTSPPlayStatus(mRTSPPauseTime, STATUS_RTSP_PAUSE);
		postRTSPPlayTimeCheckEvent_l(1000000);
		return;
	}
	else if (InGetPlayerStatus() >= PLAYER_STATUS_2XFF
				&& InGetPlayerStatus() < PLAYER_STATUS_2XBW)
	{
		ALOGD("[STATUS] FF");
		if (mRTSPWaitTimeChanged == 1)
		{
			ALOGD("[STATUS] trick status is not worked! so drop dis event");
			postRTSPPlayTimeCheckEvent_l(1000000);
			return;
		}
	}
//	mRTSPCurrentPlayTimePTS

	else if (InGetPlayerStatus() >= PLAYER_STATUS_2XBW)
	{
		ALOGD("[STATUS] BW");
		if (mRTSPWaitTimeChanged == 1)
		{
			ALOGD("[STATUS] trick status is not worked! so drop dis event");
			postRTSPPlayTimeCheckEvent_l(1000000);
			return;
		}
	}

	nMiliSec = mRTSPCurrentPlayTimePTS / 90;

	if (nMiliSec < 0)
		nMiliSec = 0;

	if (RTSP_GetEOFBOFStatus(nRTSPSessionID) == 1) // case rtcp bye -> eof
	{
		ALOGD("EOF!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

		if(!player.rtsp_last_eos_status) {
		    player.rtsp_last_eos_status = true;
	        SendRTSPPlayStatus(nMiliSec, STATUS_RTSP_EOS);
		}

		if (InGetPlayerStatus() >= PLAYER_STATUS_2XFF && 
			(InGetPlayerStatus() < PLAYER_STATUS_2XBW))
		{
			mRTSPEOFTime = RTSP_GetDuration(nRTSPSessionID);
			close_rtsp_l(1);
			return;
		}

		nLastTime = RTSP_GetDuration(nRTSPSessionID) - nMiliSec;
		if (nLastTime < 0)
			nLastTime = 0;

		if (InGetPlayerStatus() == PLAYER_STATUS_PLAY)
		{
            //jung2604 : 5초동안 같은 위치라면 데이터가 안들어오는 경우다..강제로 close해주자.
            {
	            struct  timeval cur_time;
	            gettimeofday( &cur_time, NULL );
	            long currentTime =  ( (cur_time.tv_sec << 20) + cur_time.tv_usec);

	            if(player.rtsp_last_pts == nLastTime) {
	                if ((player.avpInfo.isFeedBufferEOF && (currentTime - player.rtsp_last_pts_check_time) > 2000000) || // 버퍼가 비어있고 2초간 지속된다면..
                            (currentTime - player.rtsp_last_pts_check_time) > 5000000) { // 5초간 지속된다면..
	                    ALOGD("++rtsp no data, check time %ld sec, last pts : [%ld] , exit set nLastTime = 0", (currentTime - player.rtsp_last_pts_check_time), nLastTime);
	                    nLastTime = 0;
	                } else if(nLastTime < 1000/* || player.EOFReached */|| ((currentTime - player.rtsp_last_pts_check_time) > 2000000 && nLastTime < 2000)) {
                        ALOGD("++rtsp  nLastTime < 1000 OK~~~ last pts : [%lld]", nLastTime);
                        mRTSPEOFTime = RTSP_GetDuration(nRTSPSessionID);
                        //		usleep(500000);
                        close_rtsp_l(1);
                        return;
                    } else ALOGD("++rtsp no data, 10 sec, last pts : [%lld] , exit set nLastTime = 0, player.EOFReached = %d", nLastTime, player.EOFReached);
	            } else {
	                player.rtsp_last_pts = nLastTime;
	                player.rtsp_last_pts_check_time = currentTime;
	            }
            }
            ALOGD("++rtsp last pts : [%lld] , player.EOFReached = %d", nLastTime, player.EOFReached);
			if (nLastTime < 500 || player.EOFReached)
			{
                ALOGD("++rtsp  nLastTime < 500 OK~~~ last pts : [%lld]", nLastTime);
				mRTSPEOFTime = RTSP_GetDuration(nRTSPSessionID);
		//		usleep(500000);
				close_rtsp_l(1);
				return;
			}
		}
	}
	else if (RTSP_GetEOFBOFStatus(nRTSPSessionID) == 2) // bof
	{
		ALOGD("BOF!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		mRTSPStartTime = -99;
		play_rtsp_l(1);
		player.rtsp_last_eos_status = false;
	} else if(player.rtsp_last_eos_status) {
		player.rtsp_last_eos_status = false;
	}

	SendRTSPPlayStatus(nMiliSec, STATUS_RTSP_PLAY);

	postRTSPPlayTimeCheckEvent_l(1000000);

    if(player.nPuaseAtMs > 0 && (player.nPuaseAtMs - 1000 <= nMiliSec/1000 && player.nPuaseAtMs + 1000 >= nMiliSec) ) {
        player.nPuaseAtMs = 0;
        pause_impl();
    }

}
#endif

void IptvSocPlayer::onRecvDtvCCData(int what, int arg1, int arg2, const char* strData) {
	notifyListener_l(what, player.idx_player, arg2, strData);
}

void IptvSocPlayer::onRecvDsmccData(int event, char* path, void* data) {
	dsmcc_sigevent* dsmcc_ev = (dsmcc_sigevent*) data;

	if(event == INFO_MUSIC_CACHE || event == INFO_MUSIC_DOWNLOAD){
		audioprogramListener_l(path, event, dsmcc_ev->application.dsmccPid);
		return;
	}

	SignalEvent signal_event;
	signal_event.eventType = dsmcc_ev->eventType;
	signal_event.serviceId = dsmcc_ev->serviceId;
	signal_event.demux = dsmcc_ev->demux;
	strcpy(signal_event.uri, dsmcc_ev->uri);

	ApplicationInfo app;
	app.appType = dsmcc_ev->application.appType;
	app.organizationId = dsmcc_ev->application.organizationId;
	app.applicationId = dsmcc_ev->application.applicationId;
	app.dsmccPid = dsmcc_ev->application.dsmccPid;
	app.isServiceBound = (dsmcc_ev->application.isServiceBound ? true : false);
	app.controlCode = dsmcc_ev->application.controlCode;	
	strcpy(app.name, dsmcc_ev->application.name);
	app.priority = dsmcc_ev->application.priority;
	strcpy(app.initialPath, dsmcc_ev->application.initialPath);

	signal_event.appLists.push_back(app);
	
    dsmccListener_l(event, path, &signal_event); 
}



void IptvSocPlayer::onAVPTSTimeCheckEvent()
{
	ALOGD("[INFO] [%s] called [%d]", __FUNCTION__, player.idx_player);
    int nRet;

    player.nPollingRunFlag = 1;

    nRet = ST_GetClientEvent(&player);
    ALOGD("[INFO] [%s] nRet = %d" ,__FUNCTION__, nRet);

    if (nRet == 2 || nRet == -1) // time out or First Frame changed
    {
        ALOGD("Channel Num [%s] !!!!!!!!!!!!!!!!!!!!!!!!!!! FIRST FRAME CHANGED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!", locator.name);
        nFirstFrameChanged = 1;
    }

    if (nRet == 2) // case first Frame changed
    {
        notifyListener_l(TUNE_EVENT, player.idx_player, IPTV_SIGNAL_GOOD, NULL); // send good or bad
    }
    else if (nRet == 1) // case size changed event received
    {
        if(InGetPlayerStatus() != PLAYER_STATUS_NONE) { //add for BTVQ_21452
            postAVPTSTimeCheckEvent_l(10000);
        }
    } else if(nRet == -1) {
        notifyListener_l(TUNE_EVENT, player.idx_player, IPTV_SIGNAL_BAD, NULL); // send good or bad
        if(InGetPlayerStatus() != PLAYER_STATUS_NONE) { //add for BTVQ_21452
            postAVPTSTimeCheckEvent_l(10000);
        }
    }
}

void IptvSocPlayer::onAVEventCheckEvent()
{
	ALOGD("[INFO] [%s] called", __FUNCTION__);

	int nRet;

	player.nPollingRunFlag = 1;

	nRet = ST_GetClientEvent(&player);

    ALOGD("[INFO] [%s] nRet = %d" ,__FUNCTION__, nRet);

	if (nRet == 1) // case size changed
	{
		char szBuf[4096] = {0,};
		
		sprintf(szBuf, "<?xml version=\"1.0\"?>\n<event name=\"SizeChanged\">\n</event>");
		
		notifyListener_l(PLAY_STATUS_EVENT, player.idx_player, 200, szBuf);

	}

    if (nRet == 2) // case first Frame changed
    {
        if(player.idx_player != 0) usleep(200000);//jung2604 : pip일경우 딜레이를 줘보자
        notifyListener_l(TUNE_EVENT, player.idx_player, IPTV_SIGNAL_GOOD, NULL); // send good or bad
        return; //jung2604 : first frame 체크가 끝나면 더이상 사용하지 않는다
    }
	ALOGD("[INFO] [%s] END called", __FUNCTION__);
	if (player.nPollingRunFlag == 1 && InGetPlayerMode() != PLAYER_MODE_IPTV)
		postAVEventCheckEvent_l(100000);
}

void IptvSocPlayer::postFilePlayTimeCheckEvent_l(int64_t delayUs)
{
//	ALOGD("[%s] called", __FUNCTION__);

	mQueue.postEventWithDelay(mFilePlayTimeCheckEvent, delayUs < 0 ? 10000 : delayUs);
}

void IptvSocPlayer::postFileCloseTimeCheckEvent_l(int64_t delayUs)
{
//	ALOGD("[%s] called", __FUNCTION__);

	mQueueSecond.postEventWithDelay(mFileCloseTimeCheckEvent, delayUs < 0 ? 10000 : delayUs);
}

void IptvSocPlayer::postSocketBufferCheckEvent_l(int64_t delayUs)
{
//	ALOGD("[%s] called [%lld] [%d]", __FUNCTION__, delayUs, player.idx_player);

    // jung2604
	mQueue.postEventWithDelay(mSocketBufferCheckEvent, delayUs < 0 ? 10000 : delayUs);
}

#ifdef USE_RTSP
void IptvSocPlayer::postRTSPPlayTimeCheckEvent_l(int64_t delayUs)
{
	mQueue.postEventWithDelay(mRTSPPlayTimeCheckEvent, delayUs < 0 ? 10000 : delayUs);
}
#endif

void IptvSocPlayer::postAVPTSTimeCheckEvent_l(int64_t delayUs)
{
    //jung2604
	mQueue.postEventWithDelay(mAVPTSTimeCheckEvent, delayUs < 0 ? 10000 : delayUs);
}

void IptvSocPlayer::postAVEventCheckEvent_l(int64_t delayUs)
{
	mQueue.postEventWithDelay(mAVEventCheckEvent, delayUs < 0 ? 10000 : delayUs);
}

#ifdef USE_RTSP
void IptvSocPlayer::postConstructCurrentTimeEvent_l(int64_t delayUs)
{
	mQueueTimeCheck.postEventWithDelay(mConstructCurrentTimeEvent, delayUs < 0 ? 10000 : delayUs);
}
#endif

void IptvSocPlayer::notifyListener_l(int32_t msg, int32_t ext1, int32_t ext2, const char* strData)
{
	if (strData != NULL)
	{
		sendEvent(msg, ext1, ext2, (const int8_t*)strData);
	}
	else
	{
		sendEvent(msg, ext1, ext2);
	}
}

void IptvSocPlayer::dataListener_l(int32_t msg, int32_t ext1, int32_t ext2, const char* strData)
{
	if (strData != NULL)
	{
		sendDataEvent(msg, ext1, ext2, (const int8_t*)strData);
	}
}

void IptvSocPlayer::dsmccListener_l(int32_t dsmccEventType, char* dsmccRootPath, SignalEvent* event)
{
	sendDsmccEvent(dsmccEventType, dsmccRootPath, event);
}

void IptvSocPlayer::audioprogramListener_l(char* filePath, int updateType, int audioPid)
{
	sendAudioProgramUpdateEvent(filePath, updateType, audioPid);
}


int IptvSocPlayer::SendFilePlayStatus(int nMilisec, BTV_FILE_PLAY_STATUS eStatus, int isEOF)
{
	int ret;
	char szBuf[1024];
	char szBufMediaInfo[1024];
	
	ALOGD("[%s] called", __FUNCTION__);
	
	if (InGetPlayerStatus() == PLAYER_STATUS_STOP || InGetPlayerStatus() == PLAYER_STATUS_NONE)
	{
		ALOGD("[WARNING] FILE STATUS IS %d, SO Cannot send NOTIFY DATA!!!", InGetPlayerStatus());
		return IPTV_ERR_SUCCESS;
	}
	
	// 광고 영상일 경우
	if (eFilePlayType == TYPE_FILE_ADV)
	{
		if (eStatus == STATUS_FILE_START) // start status 전달시
		{
			sprintf(szBuf, "<?xml version=\"1.0\"?>\n<event name=\"PlayerStatusChanged\">\n\t<sWindow>%s</sWindow>\n\t<sPlayerMode>ADV</sPlayerMode>\n\t<sPlaybackStatus>PLAY</sPlaybackStatus>\n\t<dCurrentPositionKiloByte>0</dCurrentPositionKiloByte>\n\t<dCurrentPositionInMiliSec>%d</dCurrentPositionInMiliSec>\n\t<sLastADURL>%s?%s</sLastADURL>\n</event>",
				getDeviceID() == 0 ? "MAIN" : "SUB", nMilisec, szMediaInfo, szMetaInfo);
		}
		else if (eStatus == STATUS_FILE_PLAY) // play status(adv immpl) 전달시
		{
			sprintf(szBuf, "<?xml version=\"1.0\"?>\n<event name=\"PlayerStatusChanged\">\n\t<sWindow>%s</sWindow>\n\t<sPlayerMode>ADV</sPlayerMode>\n\t<sPlaybackStatus>PLAYIMP</sPlaybackStatus>\n\t<dCurrentPositionKiloByte>0</dCurrentPositionKiloByte>\n\t<dCurrentPositionInMiliSec>%d</dCurrentPositionInMiliSec>\n\t<sLastADURL>%s?%s</sLastADURL>\n</event>",
				getDeviceID() == 0 ? "MAIN" : "SUB", nMilisec, szMediaInfo, szMetaInfo);
		}
		else if (eStatus == STATUS_FILE_STOP) // stop status 전달시
		{
            if (isEOF == 1) {// jung2604 : STOP_EOF 추가
                sprintf(szBuf, "<?xml version=\"1.0\"?>\n<event name=\"PlayerStatusChanged\">\n\t<sWindow>%s</sWindow>\n\t<sPlayerMode>ADV</sPlayerMode>\n\t<sPlaybackStatus>STOP_EOF</sPlaybackStatus>\n\t<dCurrentPositionKiloByte>0</dCurrentPositionKiloByte>\n\t<dCurrentPositionInMiliSec>%d</dCurrentPositionInMiliSec>\n\t<sLastADURL>%s?%s</sLastADURL>\n</event>",
                        getDeviceID() == 0 ? "MAIN" : "SUB", nMilisec, szMediaInfo, szMetaInfo);
            } else {
                sprintf(szBuf, "<?xml version=\"1.0\"?>\n<event name=\"PlayerStatusChanged\">\n\t<sWindow>%s</sWindow>\n\t<sPlayerMode>ADV</sPlayerMode>\n\t<sPlaybackStatus>STOP</sPlaybackStatus>\n\t<dCurrentPositionKiloByte>0</dCurrentPositionKiloByte>\n\t<dCurrentPositionInMiliSec>%d</dCurrentPositionInMiliSec>\n\t<sLastADURL>%s?%s</sLastADURL>\n</event>",
                        getDeviceID() == 0 ? "MAIN" : "SUB", nMilisec, szMediaInfo, szMetaInfo);
            }

			// fldgo note : 잠깐 사이에 play event 가 오는 경우가 있다. 이때 status 가 stop이 아니기 때문에 무시되는 경우가 발생하기에 여기서 stop status 로 만든다.
			InSetPlayerStatus(PLAYER_STATUS_STOP);
		}
		else
		{
			return IPTV_ERR_SUCCESS;
		}
	}
	// 일반 파일 경우
	else if (eFilePlayType == TYPE_FILE_NORMAL)
	{
		if (eStatus == STATUS_FILE_START || eStatus == STATUS_FILE_PLAY) // start/play status 전달시
		{
            sprintf(szBufMediaInfo, "%s", szMediaInfo);

			sprintf(szBuf, "<?xml version=\"1.0\"?>\n<event name=\"PlayerStatusChanged\">\n\t<sWindow>%s</sWindow>\n\t<sPlayerMode>VOD</sPlayerMode>\n\t<sPlaybackStatus>PLAY</sPlaybackStatus>\n\t<dCurrentPositionKiloByte>0</dCurrentPositionKiloByte>\n\t<dCurrentPositionInMiliSec>%d</dCurrentPositionInMiliSec>\n\t<sLastURL>%s</sLastURL>\n\t<dContentSize>%d</dContentSize>\n</event>",
				getDeviceID() == 0 ? "MAIN" : "SUB", nMilisec, szBufMediaInfo, 0);
		}
		else if (eStatus == STATUS_FILE_STOP) // stop status 전달시
		{
            sprintf(szBufMediaInfo, "%s", szMediaInfo);

            if (isEOF == 1) { // jung2604 : STOP_EOF 추가
                sprintf(szBuf, "<?xml version=\"1.0\"?>\n<event name=\"PlayerStatusChanged\">\n\t<sWindow>%s</sWindow>\n\t<sPlayerMode>VOD</sPlayerMode>\n\t<sPlaybackStatus>STOP_EOF</sPlaybackStatus>\n\t<dCurrentPositionKiloByte>0</dCurrentPositionKiloByte>\n\t<dCurrentPositionInMiliSec>%d</dCurrentPositionInMiliSec>\n\t<sLastURL>%s</sLastURL>\n\t<dContentSize>%d</dContentSize>\n</event>",
                        getDeviceID() == 0 ? "MAIN" : "SUB", nMilisec, szBufMediaInfo, 0);
            } else {
                sprintf(szBuf, "<?xml version=\"1.0\"?>\n<event name=\"PlayerStatusChanged\">\n\t<sWindow>%s</sWindow>\n\t<sPlayerMode>VOD</sPlayerMode>\n\t<sPlaybackStatus>STOP</sPlaybackStatus>\n\t<dCurrentPositionKiloByte>0</dCurrentPositionKiloByte>\n\t<dCurrentPositionInMiliSec>%d</dCurrentPositionInMiliSec>\n\t<sLastURL>%s</sLastURL>\n\t<dContentSize>%d</dContentSize>\n</event>",
                        getDeviceID() == 0 ? "MAIN" : "SUB", nMilisec, szBufMediaInfo, 0);
            }

		}
		else
		{
			return IPTV_ERR_SUCCESS;
		}
	}
	else
	{
		return IPTV_ERR_SUCCESS;
	}
	
	notifyListener_l(PLAY_STATUS_EVENT, player.idx_player, 200, szBuf);
	
	return IPTV_ERR_SUCCESS;
}

#ifdef USE_RTSP
int IptvSocPlayer::SendRTSPPlayStatus(int nMilisec, BTV_RTSP_PLAY_STATUS eStatus)
{
	//	ALOGD("[%s] called", __FUNCTION__);
	
	int ret;
	char szBuf[4096] = {0,}, szBufTemp[4096] = {0,}, szStatus[256] = {0,};
	char szBufMediaInfo[1024] = {0,};
	
	ALOGD("[%s] called nMilisec [%d] status [%d]", __FUNCTION__, nMilisec, eStatus);

    mLastSendRTSPStatusPlayTimeMs = nMilisec; //jung2604 : 20190518 : UI에게 마지막으로 던져준 play time 값..

	if (eRTSPPlayType == TYPE_RTSP_ADV)
	{
		if (eStatus == STATUS_RTSP_START) // start status 전달시
		{
			sprintf(szBuf, "<?xml version=\"1.0\"?>\n<event name=\"PlayerStatusChanged\">\n\t<sWindow>%s</sWindow>\n\t<sPlayerMode>ADV</sPlayerMode>\n\t<sPlaybackStatus>PLAY</sPlaybackStatus>\n\t<dCurrentPositionKiloByte>0</dCurrentPositionKiloByte>\n\t<dCurrentPositionInMiliSec>%d</dCurrentPositionInMiliSec>\n\t<sLastADURL>%s?%s</sLastADURL>\n</event>",
				getDeviceID() == 0 ? "MAIN" : "SUB", nMilisec, szMediaInfo, szMetaInfo);
		}
		else if (eStatus == STATUS_RTSP_PLAY) // play status 전달시
		{
			if (nSendRTSPADVIMP == 0) // IMP 미전달된 case
			{
				if (nMilisec >= (RTSP_GetDuration(nRTSPSessionID) / 10 * 3)) // 30 % 보다 크거나 같을시 IMP 전달 case
				{
					sprintf(szBuf, "<?xml version=\"1.0\"?>\n<event name=\"PlayerStatusChanged\">\n\t<sWindow>%s</sWindow>\n\t<sPlayerMode>ADV</sPlayerMode>\n\t<sPlaybackStatus>PLAYIMP</sPlaybackStatus>\n\t<dCurrentPositionKiloByte>0</dCurrentPositionKiloByte>\n\t<dCurrentPositionInMiliSec>%d</dCurrentPositionInMiliSec>\n\t<sLastADURL>%s?%s</sLastADURL>\n</event>",
						getDeviceID() == 0 ? "MAIN" : "SUB", nMilisec, szMediaInfo, szMetaInfo);

					nSendRTSPADVIMP = 1;
				}
				else
				{
					return IPTV_ERR_SUCCESS;
				}
			}
			else
			{
				return IPTV_ERR_SUCCESS;
			}
		}
		else if (eStatus == STATUS_RTSP_STOP) // stop status 전달시
		{
            if (RTSP_GetEOFBOFStatus(nRTSPSessionID) == 1) { // jung2604 : STOP_EOF 추가
                sprintf(szBuf, "<?xml version=\"1.0\"?>\n<event name=\"PlayerStatusChanged\">\n\t<sWindow>%s</sWindow>\n\t<sPlayerMode>ADV</sPlayerMode>\n\t<sPlaybackStatus>STOP_EOF</sPlaybackStatus>\n\t<dCurrentPositionKiloByte>0</dCurrentPositionKiloByte>\n\t<dCurrentPositionInMiliSec>%d</dCurrentPositionInMiliSec>\n\t<sLastADURL>%s?%s</sLastADURL>\n</event>",
                        getDeviceID() == 0 ? "MAIN" : "SUB", nMilisec, szMediaInfo, szMetaInfo);
            } else {
                sprintf(szBuf, "<?xml version=\"1.0\"?>\n<event name=\"PlayerStatusChanged\">\n\t<sWindow>%s</sWindow>\n\t<sPlayerMode>ADV</sPlayerMode>\n\t<sPlaybackStatus>STOP</sPlaybackStatus>\n\t<dCurrentPositionKiloByte>0</dCurrentPositionKiloByte>\n\t<dCurrentPositionInMiliSec>%d</dCurrentPositionInMiliSec>\n\t<sLastADURL>%s?%s</sLastADURL>\n</event>",
                        getDeviceID() == 0 ? "MAIN" : "SUB", nMilisec, szMediaInfo, szMetaInfo);
            }
		}
		else if (eStatus == STATUS_RTSP_EOS) // eos status 전달시
		{
            sprintf(szBuf, "<?xml version=\"1.0\"?>\n<event name=\"PlayerStatusChanged\">\n\t<sWindow>%s</sWindow>\n\t<sPlayerMode>ADV</sPlayerMode>\n\t<sPlaybackStatus>RTSP_EOS</sPlaybackStatus>\n\t<dCurrentPositionKiloByte>0</dCurrentPositionKiloByte>\n\t<dCurrentPositionInMiliSec>%d</dCurrentPositionInMiliSec>\n\t<sLastADURL>%s?%s</sLastADURL>\n</event>",
                    getDeviceID() == 0 ? "MAIN" : "SUB", nMilisec, szMediaInfo, szMetaInfo);

		}
		else
		{
			return IPTV_ERR_SUCCESS;
		}
		
	}
	else if (eRTSPPlayType == TYPE_RTSP_NORMAL)
	{
		if (eStatus == STATUS_RTSP_START || eStatus == STATUS_RTSP_PLAY) // start/play status 전달시
		{
			if (strlen(szOTPID) == 0)
			{
				sprintf(szBufMediaInfo, "%s", szMediaInfo);
			}
			else
			{
				sprintf(szBufMediaInfo, "%s?%s?%s", szMediaInfo, szOTPID, szOTPPASSWD);
			}
			
			sprintf(szBufTemp, "<?xml version=\"1.0\"?>\n<event name=\"PlayerStatusChanged\">\n\t<sWindow>%s</sWindow>\n\t<sPlayerMode>VOD</sPlayerMode>\n\t<sPlaybackStatus>%%s</sPlaybackStatus>\n\t<dCurrentPositionKiloByte>0</dCurrentPositionKiloByte>\n\t<dCurrentPositionInMiliSec>%d</dCurrentPositionInMiliSec>\n\t<sLastURL>%s</sLastURL>\n\t<dContentSize>%d</dContentSize>\n</event>",
				getDeviceID() == 0 ? "MAIN" : "SUB", nMilisec, szBufMediaInfo, RTSP_GetDuration(nRTSPSessionID));

			switch(InGetPlayerStatus())
			{
				case PLAYER_STATUS_2XFF:
					strcpy(szStatus, "FF_X2");
					break;
				case PLAYER_STATUS_4XFF:
					strcpy(szStatus, "FF_X4");
					break;
				case PLAYER_STATUS_8XFF:
					strcpy(szStatus, "FF_X8");
					break;
				case PLAYER_STATUS_16XFF:
					strcpy(szStatus, "FF_X16");
					break;
				case PLAYER_STATUS_2XBW:
					strcpy(szStatus, "REW_X2");
					break;
				case PLAYER_STATUS_4XBW:
					strcpy(szStatus, "REW_X4");
					break;
				case PLAYER_STATUS_8XBW:
					strcpy(szStatus, "REW_X8");
					break;
				case PLAYER_STATUS_16XBW:
					strcpy(szStatus, "REW_X16");
					break;
				case PLAYER_STATUS_PLAY:
				default:
					strcpy(szStatus, "PLAY");
					break;
			}

			sprintf(szBuf, szBufTemp, szStatus);
		}
		else if (eStatus == STATUS_RTSP_STOP) // stop status 전달시
		{
			if (strlen(szOTPID) == 0)
			{
				sprintf(szBufMediaInfo, "%s", szMediaInfo);
			}
			else
			{
				sprintf(szBufMediaInfo, "%s?%s?%s", szMediaInfo, szOTPID, szOTPPASSWD);
			}

            if (RTSP_GetEOFBOFStatus(nRTSPSessionID) == 1) { // jung2604 : STOP_EOF 추가
                sprintf(szBuf, "<?xml version=\"1.0\"?>\n<event name=\"PlayerStatusChanged\">\n\t<sWindow>%s</sWindow>\n\t<sPlayerMode>VOD</sPlayerMode>\n\t<sPlaybackStatus>STOP_EOF</sPlaybackStatus>\n\t<dCurrentPositionKiloByte>0</dCurrentPositionKiloByte>\n\t<dCurrentPositionInMiliSec>%d</dCurrentPositionInMiliSec>\n\t<sLastURL>%s</sLastURL>\n\t<dContentSize>%d</dContentSize>\n</event>",
                        getDeviceID() == 0 ? "MAIN" : "SUB", nMilisec, szBufMediaInfo, RTSP_GetDuration(nRTSPSessionID));
            } else {
                sprintf(szBuf, "<?xml version=\"1.0\"?>\n<event name=\"PlayerStatusChanged\">\n\t<sWindow>%s</sWindow>\n\t<sPlayerMode>VOD</sPlayerMode>\n\t<sPlaybackStatus>STOP</sPlaybackStatus>\n\t<dCurrentPositionKiloByte>0</dCurrentPositionKiloByte>\n\t<dCurrentPositionInMiliSec>%d</dCurrentPositionInMiliSec>\n\t<sLastURL>%s</sLastURL>\n\t<dContentSize>%d</dContentSize>\n</event>",
                        getDeviceID() == 0 ? "MAIN" : "SUB", nMilisec, szBufMediaInfo, RTSP_GetDuration(nRTSPSessionID));
            }
        }
		else if (eStatus == STATUS_RTSP_PAUSE)
		{
			if (strlen(szOTPID) == 0)
			{
				sprintf(szBufMediaInfo, "%s", szMediaInfo);
			}
			else
			{
				sprintf(szBufMediaInfo, "%s?%s?%s", szMediaInfo, szOTPID, szOTPPASSWD);
			}
			
			sprintf(szBuf, "<?xml version=\"1.0\"?>\n<event name=\"PlayerStatusChanged\">\n\t<sWindow>%s</sWindow>\n\t<sPlayerMode>VOD</sPlayerMode>\n\t<sPlaybackStatus>PAUSE</sPlaybackStatus>\n\t<dCurrentPositionKiloByte>0</dCurrentPositionKiloByte>\n\t<dCurrentPositionInMiliSec>%d</dCurrentPositionInMiliSec>\n\t<sLastURL>%s</sLastURL>\n\t<dContentSize>%d</dContentSize>\n</event>",
				getDeviceID() == 0 ? "MAIN" : "SUB", nMilisec, szBufMediaInfo, RTSP_GetDuration(nRTSPSessionID));
		}
		else if (eStatus == STATUS_RTSP_EOS) // eos status 전달시
		{
			if (strlen(szOTPID) == 0)
			{
				sprintf(szBufMediaInfo, "%s", szMediaInfo);
			}
			else
			{
				sprintf(szBufMediaInfo, "%s?%s?%s", szMediaInfo, szOTPID, szOTPPASSWD);
			}

            sprintf(szBuf, "<?xml version=\"1.0\"?>\n<event name=\"PlayerStatusChanged\">\n\t<sWindow>%s</sWindow>\n\t<sPlayerMode>VOD</sPlayerMode>\n\t<sPlaybackStatus>RTSP_EOS</sPlaybackStatus>\n\t<dCurrentPositionKiloByte>0</dCurrentPositionKiloByte>\n\t<dCurrentPositionInMiliSec>%d</dCurrentPositionInMiliSec>\n\t<sLastURL>%s</sLastURL>\n\t<dContentSize>%d</dContentSize>\n</event>",
                    getDeviceID() == 0 ? "MAIN" : "SUB", nMilisec, szBufMediaInfo, RTSP_GetDuration(nRTSPSessionID));

        }
		else
		{
			return IPTV_ERR_SUCCESS;
		}
	}
	else
	{
		// what case is this?? 
		return IPTV_ERR_SUCCESS;
	}

	notifyListener_l(PLAY_STATUS_EVENT, player.idx_player, 200, szBuf);
	
	return IPTV_ERR_SUCCESS;
}

int IptvSocPlayer::SendRTSPSeekCompleteEvent()
{
	ALOGD("[%s] called", __FUNCTION__);

	notifyListener_l(SEEK_COMPLETION_EVENT, player.idx_player, 1, NULL);

	return IPTV_ERR_SUCCESS;
}

int IptvSocPlayer::SendRTSPErrorStatus(int nErrorCode, char *szIp, int nPort)
{
	//	ALOGD("[%s] called", __FUNCTION__);
	
	int ret;
	char szBuf[4096] = {0,};
	char szBufMediaInfo[1024] = {0,};
	
	ALOGD("[%s] called nErrorCode [%d]", __FUNCTION__, nErrorCode);

	if (szIp == NULL)
		return -1;
	
	sprintf(szBuf, "<?xml version=\"1.0\"?>\n<event name=\"RTSPStatus\">\n\t<ErrCode>%d</ErrCode>\n\t<IPAddress>%s</IPAddress>\n\t<Port>%d</Port>\n</event>",
				nErrorCode, szIp, nPort);
	
	notifyListener_l(PLAY_STATUS_EVENT, player.idx_player, 200, szBuf);
	
	return IPTV_ERR_SUCCESS;
	
}
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// status get set api 

int IptvSocPlayer::InGetDeviceOpenStatus()
{
	ALOGD("[%s] called\n[%s] current %d", __FUNCTION__, __FILE__, eDeviceOpenStatus);
	return eDeviceOpenStatus;
}

void IptvSocPlayer::InSetDeviceOpenStatus(BTV_DEVICE_STATUS eStatus)
{
	ALOGD("[%s] called\n[%s] current %d will set %d", __FUNCTION__, __FILE__, eDeviceOpenStatus, eStatus);
	eDeviceOpenStatus = eStatus;
}

int IptvSocPlayer::InGetPlayerStatus()
{
	ALOGD("[%s] called\n[%s] current %d", __FUNCTION__, __FILE__, ePlayerStatus);
	return ePlayerStatus;
}

void IptvSocPlayer::InSetPlayerStatus(BTV_PLAYER_STATUS eStatus)
{
	ALOGD("[%s] called\n[%s] current %d will set %d", __FUNCTION__, __FILE__, ePlayerStatus, eStatus);

    if (eStatus >= PLAYER_STATUS_2XFF && eStatus <= PLAYER_STATUS_16XBW) player.isRtspSeekMode = true;
    else player.isRtspSeekMode = false;
	ePlayerStatus = eStatus;
}

int IptvSocPlayer::InGetPlayerMode()
{
	ALOGD("[%s] called\n[%s] current %d", __FUNCTION__, __FILE__, ePlayerMode);
	return ePlayerMode;
}

void IptvSocPlayer::InSetPlayerMode(BTV_PLAYER_MODE eMode)
{
	ALOGD("[%s] called\n[%s] current %d will set %d", __FUNCTION__, __FILE__, ePlayerMode, eMode);
	ePlayerMode = eMode;
}

#ifdef USE_RTSP
long long  IptvSocPlayer::GetRTSPTimeStamp(int *nTimeChanged)
{
	std::lock_guard<std::mutex> lock(mLockRTSPGetTimeStamp);

    long long  nGetTimePTS = ST_GetTimeStamp (&player);

	if (mLastGetTimeStamp != nGetTimePTS)
	{
		*nTimeChanged = 1;
	}
	else
	{
		*nTimeChanged = 0;
	}
	mLastGetTimeStamp = nGetTimePTS;
    //ALOGD("[%s] called\n[%s] mLastGetTimeStamp %lld", __FUNCTION__, __FILE__, ePlayerMode, mLastGetTimeStamp);
	return mLastGetTimeStamp;
}

int IptvSocPlayer::WaitTimeMatch(long long nCorrectTimePTS, int nMinusIntervalPTS, int nPlusIntervalPTS, int nRetryCount, int nChangeCheck, int nFirstFrameCheck)
{
	long long nGetTimePTS = 0;
	int nTimeChanged = 0;

	ALOGD("WaitTimeMatch Called!! [%lld], nMinusIntervalPTS [%d], nPlusIntervalPTS [%d],", nCorrectTimePTS, nMinusIntervalPTS, nPlusIntervalPTS);

	if (nFirstFrameCheck == 1)
	{
		nGetTimePTS = GetRTSPTimeStamp(&nTimeChanged);

        long long nFirstIFramePTS = RTSP_GetFirstIFreamePTS(nRTSPSessionID);
        nGetTimePTS -= nFirstIFramePTS;

		if(nGetTimePTS > 0) {
            switch(InGetPlayerStatus()) {
                case PLAYER_STATUS_2XBW:
                    nGetTimePTS -= 2 * 90000;
                    break;
                case PLAYER_STATUS_4XBW:
                    nGetTimePTS -= 4 * 90000;
                    break;
                case PLAYER_STATUS_8XBW:
                    nGetTimePTS -= 8 * 90000;
                    break;
                case PLAYER_STATUS_16XBW:
                    nGetTimePTS -= 16 * 90000;
                    break;
            }
		}

		if(nGetTimePTS <= 0 && (nCorrectTimePTS - nMinusIntervalPTS) <= 0) {
		    ALOGD("FIRST Frame check out, nGetTimePTS : %lld", nGetTimePTS);
		    return -1;
		}

		ALOGD("FIRST Frame Check wait!!");

		return ST_GetClientEvent(&player);
	}
	else if(nCorrectTimePTS - nMinusIntervalPTS <= 0 && InGetPlayerStatus() >= PLAYER_STATUS_2XBW && InGetPlayerStatus() <= PLAYER_STATUS_16XBW) {
		return -1;
	}
	else {
		ALOGD("time check start !!");
		
		while(nRetryCount > 0 && RTSP_GetEOFBOFStatus(nRTSPSessionID) != 1)
		{
			nGetTimePTS = GetRTSPTimeStamp(&nTimeChanged);
			
			if (nGetTimePTS < 0)
				nGetTimePTS = 0;
			
			ALOGD("nGetTimePTS in WaitTimeMatch nGetTimePTS = [%lld], nCorrectTimePTS - nMinusIntervalPTS [%lld], nCorrectTimePTS + nPlusIntervalPTS [%lld]", nGetTimePTS, nCorrectTimePTS - nMinusIntervalPTS, nCorrectTimePTS + nPlusIntervalPTS);
			
			if (mRTSPCalculateTimeCheck == 1)
			{
				ALOGD("Not Found !! nGetTimePTS in WaitTimeMatch nGetTimePTS = [%lld] CorrectTimePTS [%lld]", nGetTimePTS, nCorrectTimePTS);
				return -2;
			}
			
			if ( (nGetTimePTS >= nCorrectTimePTS - nMinusIntervalPTS) &&
				(nGetTimePTS <= nCorrectTimePTS + nPlusIntervalPTS))
			{
				
				ALOGD("In Interval !!! nCorrectTimePTS[%lld] nGetTimePTS[%lld]", nCorrectTimePTS, nGetTimePTS);
				
				if (nChangeCheck == 0)
				{
					//usleep(9000000);
					return 0;
				}
				
				if (nTimeChanged == 1)
				{
					ALOGD("In Interval !!! TIME changed");
					//usleep(9000000);
					
					return 0;
				}
				else
				{
					ALOGD("In Interval !!! but TIME NOT changed");
				}
				
			}
			usleep(100000);
			
			nRetryCount--;
		}
		
		ALOGD("Not In Interval !!! nCorrectTimePTS[%lld] nGetTimePTS[%lld]", nCorrectTimePTS, nGetTimePTS);
		//usleep(9000000);
		
		return -1;
	}
}

void IptvSocPlayer::onConstructCurrentTimeEvent()
{
	int nRet;
	int nStatus;
	int nChangedFlag;
	long long nGetTimePTS;
	int nMultiple = 1;
	long long nDuration = RTSP_GetDuration(nRTSPSessionID);
	long long nFirstIFramePTS = RTSP_GetFirstIFreamePTS(nRTSPSessionID);

	//first iframe을 기다리자...
    mRTSPWaitTimeChanged = 1;
    WaitTimeMatch(0, 0, 0, 0, 0, 1);
    usleep(10000);
    mRTSPWaitTimeChanged = 0;

	while (ePlayerStatus != PLAYER_STATUS_STOP && ePlayerStatus != PLAYER_STATUS_NONE)
	{
		if (mRTSPCalculateTimeCheck)
		{
retry:
			mRTSPCalculateTimeCheck = 0;
			nStatus = ePlayerStatus;
			nRet = 0;

			mRTSPWaitTimeChanged = 1;

			if (nStatus == PLAYER_STATUS_PLAY)
			{
				// -2초 +2초, 총 4초의 interval을 찾고 못찾으면 5초간 기다린다.
                // jung2604 : UHD에서는 설정한 180000보다 높은 값이 처음으로 넘어온다. 그래서 조정을 한다. 2.5
				nRet = WaitTimeMatch(mRTSPWannaBeGonePTS + RTSP_GetFirstIFreamePTS(nRTSPSessionID), 2500 * 90, 2500 * 90, 50, 1, 0);
			}
			else if (nStatus >= PLAYER_STATUS_2XFF && nStatus <= PLAYER_STATUS_16XBW) // case trick
			{
				// 1초의 interval 을 찾고 못찾으면 5초간 기다린다.
                // jung2604 : UHD에서는 설정한 90000보다 높은 값이 처음으로 넘어온다.. 92970 98000등등.. 그래서 조정을 한다. 1.5
				nRet = WaitTimeMatch(0, 0, 1500 * 90, 50, 1, 0);
			}
			mRTSPWaitTimeChanged = 0;

			// case in change
			if (nRet == -2)
			{
				goto retry;
			}
		}

		if (mRTSPCalculateTimeCheckExceptWait == 1)
		{
			usleep(30000);
			continue;
		}

		
		nStatus = ePlayerStatus;
		
		nGetTimePTS = GetRTSPTimeStamp (&nChangedFlag);
        //ALOGD("++onConstructCurrentTimeEvent,  nStatus [%d], nFirstIFramePTS [%lld], nGetTimePTS [%lld]", nStatus, nFirstIFramePTS, nGetTimePTS);
		
		if (nGetTimePTS < 0) {
			//nGetTimePTS = 0;
		} else {
            switch(nStatus)
            {
                case PLAYER_STATUS_PLAY:
                    //추가 : eof가 내려왔고, iframe이 터지지 않았다면 pts를 갱신하지 않는다.
                    if(player.isFirstFrameDisplayed != 0 || RTSP_GetEOFBOFStatus(nRTSPSessionID) != 1)
                        mRTSPCurrentPlayTimePTS = nGetTimePTS - nFirstIFramePTS;
                    break;
                case PLAYER_STATUS_NONE:
                case PLAYER_STATUS_STOP:
                    mRTSPCurrentPlayTimePTS = 0;
                    break;
                case PLAYER_STATUS_PAUSE:
                    break;
                case PLAYER_STATUS_2XFF:
                    nMultiple = 2;
                    mRTSPCurrentPlayTimePTS = mRTSPBaseTime + nGetTimePTS * nMultiple;
                    break;
                case PLAYER_STATUS_4XFF:
                    nMultiple = 4;
                    mRTSPCurrentPlayTimePTS = mRTSPBaseTime + nGetTimePTS * nMultiple;
                    break;
                case PLAYER_STATUS_8XFF:
                    nMultiple = 8;
                    mRTSPCurrentPlayTimePTS = mRTSPBaseTime + nGetTimePTS * nMultiple;
                    break;
                case PLAYER_STATUS_16XFF:
                    nMultiple = 16;
                    mRTSPCurrentPlayTimePTS = mRTSPBaseTime + nGetTimePTS * nMultiple;
                    break;
                case PLAYER_STATUS_2XBW:
                    nMultiple = 2;
                    mRTSPCurrentPlayTimePTS = mRTSPBaseTime - (nGetTimePTS * nMultiple);
                    break;
                case PLAYER_STATUS_4XBW:
                    nMultiple = 4;
                    mRTSPCurrentPlayTimePTS = mRTSPBaseTime - (nGetTimePTS * nMultiple);
                    break;
                case PLAYER_STATUS_8XBW:
                    nMultiple = 8;
                    mRTSPCurrentPlayTimePTS = mRTSPBaseTime - (nGetTimePTS * nMultiple);
                    break;
                case PLAYER_STATUS_16XBW:
                    nMultiple = 16;
                    mRTSPCurrentPlayTimePTS = mRTSPBaseTime - (nGetTimePTS * nMultiple);
                    break;
            }
		}


		if (mRTSPCurrentPlayTimePTS <= 0)
		{
           // ALOGD("++onConstructCurrentTimeEvent,  [TEST] nStatus [%d], mRTSPCurrentPlayTimePTS [%lld], mRTSPBaseTime : %lld, nMultiple : %d", nStatus, mRTSPCurrentPlayTimePTS, mRTSPBaseTime, nMultiple);
			mRTSPCurrentPlayTimePTS = 0;
		}
		
		if (mRTSPCurrentPlayTimePTS > nDuration * 90)
		{
			mRTSPCurrentPlayTimePTS = nDuration * 90;
		}
       //ALOGD("++onConstructCurrentTimeEvent,  nStatus [%d], mRTSPCurrentPlayTimePTS [%lld]", nStatus, mRTSPCurrentPlayTimePTS);

//		printf("whatadata %d [%d] [%d] [%02d:%02d]\n",  __LINE__, mRTSPCurrentPlayTimePTS, mRTSPCurrentPlayTimePTS / 90000, 
//			(mRTSPCurrentPlayTimePTS / 90000) / 60, (mRTSPCurrentPlayTimePTS / 90000) % 60);

		usleep(30000);
	}
}
#endif

int32_t IptvSocPlayer::enableCasInfo(int enable) {
    ALOGD("[%s] called\n[%s] , enable : %d", __FUNCTION__, __FILE__, enable);
    int ret_v = -1;
    if(enable) {
        ALOGD("[%s] start getting cas info status", __FUNCTION__, __FILE__, enable);
        int MCASStatusErr = MCAS_GetStatus(MSG_REQUEST, EPG_MSG_SCARD_INFO, null, 0);
        MCASStatusErr = MCAS_GetStatus(MSG_REQUEST, EPG_MSG_SERVICE_INFO, null, 0);
        MCASStatusErr = MCAS_GetStatus(MSG_REQUEST, EPG_MSG_SS_ENTL, null, 0);
    }

    ALOGD("[%s] out\n[%s]", __FUNCTION__, __FILE__);
    return 0;
}

