// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <sys/poll.h>

#include <stdio.h>
#include <pthread.h>

#include <dbg.h>
#include "dvb_primitive.h"

#define TP_PACKET_SIZE                          188

#define print_debug( ... ) 

static pthread_t ThreadFeeder;

int dvb_file_unlock(dvb_player_t *player)
{    
	player->TerminateFilePlay = 1;

    return 0;
}

int dvb_wait_file_unlock(dvb_player_t *player)
{
    pthread_yield(); 
    pthread_join(ThreadFeeder,NULL);
    if(player->fStream != NULL) fclose(player->fStream);

    return 0;
}
void *feederfn(void* param)
{
	dvb_player_t *player = (dvb_player_t*) param;
    volatile int *Terminated = &player->TerminateFilePlay;
    int dataLength = 0;
    int dataSize = 0;
    unsigned int written = 0;
    unsigned long lastFileOffset = 0;

    FILE *fp = player->fStream;

    char readBuffer[BUFFER_SIZE];

    while(!*Terminated)
    {
        //read
        fseeko (fp, lastFileOffset, SEEK_SET);

        dataLength = (int) fread (readBuffer, TP_PACKET_SIZE, NUMBER_PACKETS, fp);
        dataSize = dataLength * TP_PACKET_SIZE;
        //write
        if(dataLength > 0) {
#ifdef USE_BTV_HAL_MANAGER
            written = (unsigned int) HalBtv_InjectTS(player->idx_player, readBuffer, dataSize);
#else
            written = (unsigned int) AVP_InjectTS(player->avpInfo.playerHandle, readBuffer, dataSize);
#endif
            if(written <= 0) { // feed에 실패했을경우 delay
                usleep(200000); //FIXME : SOC마다 틀릴수 있다 영상이 끊긴다면 수정
            } else { // feed가 되었다면 포인터 이동..
                lastFileOffset += written;
                if(feof(fp) && dataLength == written) {
#ifdef USE_BTV_HAL_MANAGER
                    if(HalBtv_InjectCommand(player->idx_player, AVP_INJECT_EOS) == 0)
#else
                    if(AVP_InjectCommand(player->avpInfo.playerHandle, AVP_INJECT_EOS) == 0) 
#endif						
                    {
                        player->avpInfo.isFeedBufferEOF = true;
                        *Terminated = 1;
                    }
                }
#if 0
            }

            if(written <= 0) usleep(200000);
            else {
                lastFileOffset += written;
                if(isEof && dataSize == written) {
                    *Terminated = 1;
                    break;
                } else if(dataSize != written) {
                    logLogCatKey_stdvb("+++ packet fail , dataSize[%d] != written[%d]", dataSize, written);
                    usleep(200000);
                } else pthread_yield();

#else
                pthread_yield();
#endif
            }
        } else {
           logLogCatKey_stdvb("++ empty packet??");
            for(int i = 0 ; i < 3 && !*Terminated ; i++) {
#ifdef USE_BTV_HAL_MANAGER
                if(HalBtv_InjectCommand(player->idx_player, AVP_INJECT_EOS) == 0) break;
#else

                if(AVP_InjectCommand(player->avpInfo.playerHandle, AVP_INJECT_EOS) == 0) break;
#endif

                usleep(100000); //wait 100ms
                if(i == 2) player->EOFReached = true;
            }

            player->avpInfo.isFeedBufferEOF = true;
            *Terminated = 1;
        }
    }

    logLogCatKey_stdvb("++ feederfn end");

    return (void*)0;    
}

int dvb_file_lock(dvb_player_t *player, dvb_locator_t *locator)
{
    player->fStream = fopen64(locator->tune_param.file.filename,"rb");

    if(player->fStream == NULL)
    {
		print_debug("Fail to open file : %s", locator->tune_param.file.filename);
        return -1;
     }

	player->TerminateFilePlay = 0;

    //read from file
    if (pthread_create(&ThreadFeeder, NULL, feederfn, (void *)player) != 0)
	{
        perror("pthread_create :");
    }

    return 0;    
}
        
#ifdef __cplusplus
} //extern "C"
#endif
