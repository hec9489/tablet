// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#include <pthread.h>
#include "demux.h"

//---------------------------------- EVENT QUEUE------------------------------------

void lock_event_list(event_head_t *l)
{
    pthread_mutex_lock(&l->lock);
}

void unlock_event_list(event_head_t *l)
{
    pthread_mutex_unlock(&l->lock);
    pthread_cond_signal(&l->cond);	
}

event_list_t* create_event_entry(int event, int param, void *data)
{
    event_list_t *c = calloc(sizeof(char), sizeof(event_list_t));
    if (!c)
        return 0;

    c->event = event;
    c->param = param;    
    c->data = data;
    c->next = 0;
    return c;
}

void destroy_event_entry(event_list_t* c)
{
    if(c!=0){
        free(c);
    }
}

event_list_t *dequeue_event(event_head_t *list)
{
    event_list_t *eptr;
    lock_event_list(list);
    eptr = list->head;
    if (eptr) {
        list->head = eptr->next;
        if (!list->head) {
            assert(list->last == eptr);
            list->last = 0;
        }
    }
    unlock_event_list(list);
    return eptr;
}

void queue_event(event_head_t *list, event_list_t *c, int add_to_front)
{
    c->next = 0;
    lock_event_list(list);
    /* appending a new entry to the back of the list */
    if (list->last) {
        assert(list->head);
        // List is not empty
        if (!add_to_front) {
            list->last->next = c;
            list->last = c;
        } else {
            c->next = list->head;
            list->head = c;
        }
    } else {
        // List is empty
        assert(!list->head);
        list->head = c;
        list->last = c;
    }
    unlock_event_list(list);
}

int queue_event_data(event_head_t *list, int event, int param, void *data)
{
    event_list_t *b  = create_event_entry(event, param, data);
    if (!b)
        return -1;
    queue_event(list, b, 0);
    return 0;
}

int queue_front_event_data(event_head_t *list, int event, int param, void *data)
{
    event_list_t *b  = create_event_entry(event, param, data);
    if (!b)
        return -1;
    queue_event(list, b, 1);
    return 0;
}


int remove_event(event_head_t *list)
{
    event_list_t *c = dequeue_event(list);
    if (!c) {
        return 0;
    }
    destroy_event_entry(c);
    return 1;
}

void free_events(event_head_t *list)
{
    while (remove_event(list))
        ;
}


/******************************************************************************
 * LOCAL FUNCTION
 *****************************************************************************/

static inline bool sDEMUX_FindFilter(demux_context *demux, uint16_t pid, uint32_t *type, ts_filter **filter)
{
    int32_t i, j;

    if(demux == NULL ){        
        LOGE("DEMUX : demux is NULL.");
        return false;
    }   

    pthread_mutex_lock(&(demux->section_table_mutex));
    
    CHECK_DEMUX_ALIVE(demux);
    
    for (i = 0; i < FILTER_TYPE_MAX; i++) {
        for (j = 0; j < demux->filter_table[i].used_cnt; j++) {
            if (demux->filter_table[i].filters[j].pid == pid) {
                *type = demux->filter_table[i].type;
                *filter = &demux->filter_table[i].filters[j];

                pthread_mutex_unlock(&(demux->section_table_mutex));
                return true;
            }
        }
    }
    pthread_mutex_unlock(&(demux->section_table_mutex));    
    return false;
}

static bool sDEMUX_IsContinuous(ts_header *ts_hdr, uint8_t discontinuity, int16_t last_cc)
{
    bool cc_ok = (ts_hdr->pid == 0x1FFF) /* null packet PID */
             || (discontinuity)
             || (last_cc < 0) 
             || (ts_hdr->continuity_counter == (last_cc + 1));

    cc_ok = true; 
    if (ts_hdr->pid == 0x1FFF) {
        cc_ok = false; /* check only null packet for now. */
    } 
    return cc_ok; 
}

uint32_t sDEMUX_ParsePcr(uint8_t *pkt, uint32_t size, int64_t *pcr_base, int32_t *pcr_ext)
{
    uint8_t pcr_flag = 0;
    uint32_t val = 0;
    uint32_t cnt = size;

    pcr_flag = pkt[0] & 0x10;
    if (pcr_flag) {
        val = ((uint32_t)pkt[1] << 24) | ((uint32_t)pkt[2] << 16) |
              ((uint32_t)pkt[3] <<  8) | ((uint32_t)pkt[4]);
        *pcr_base = ((int64_t)val << 1) | (pkt[5] >> 7);
        *pcr_ext  = ((pkt[5] & 1) << 8) | pkt[6];
    }
    return cnt;
}

static inline uint32_t sDEMUX_ParseTsHeader(uint8_t *packet, ts_header *ts_hdr)
{
    uint32_t used = 0;

    memset(ts_hdr, 0x00, sizeof(ts_header));

    ts_hdr->sync_byte = (uint8_t)packet[used]; 
    if (ts_hdr->sync_byte != SYNC_BYTE) {
        LOGE("sDEMUX_ParseTsHeader : Sync syte error (%x)\n", ts_hdr->sync_byte);
        return used;
    }
    used++;
        
    ts_hdr->transport_error_indicator    = (uint8_t)(packet[used] >> 7) & 0x01;
    ts_hdr->payload_unit_start_indicator = (uint8_t)(packet[used] >> 6) & 0x01;
    ts_hdr->transport_priority           = (uint8_t)(packet[used] >> 5) & 0x01;
    ts_hdr->pid = (((uint16_t)packet[used] << 8) | (uint16_t)packet[used + 1]) & (0x1fff); 
    used += 2;
    ts_hdr->scramble_control             = (uint8_t)(packet[used] >> 6) & 0x03;
    ts_hdr->adaptation_field_control     = (uint8_t)(packet[used] >> 4) & 0x03;
    ts_hdr->continuity_counter           = (uint8_t)(packet[used] >> 0) & 0x0f;
    used++;
        
    if (ts_hdr->adaptation_field_control >= 2) {
        ts_hdr->adaptation_byte_length = (uint8_t)(packet[used] & 0xff); 
        used++;
    }
    return used;
}

int demux_process(demux_context *demux)
{
	event_list_t *eptr;

	char* handleData = 0;
	int size = 0;

	while ((eptr=dequeue_event(&demux->demux_event))) {
		switch(eptr->event) {
		case DEMUX_EVENT_FEED_DATA:
			handleData = (char *)eptr->data;	
			size = (int)eptr->param;
			if(!demux->stop_requested)
				DEMUX_ProcFeedData(demux, handleData, size);
			break;

		case DEMUX_EVENT_EVENT_SECTION_FILTER_DATA:
			break;
		}
		destroy_event_entry(eptr);
	}
	return 0;
}

void demux_finish(demux_context *demux)
{
    /* release section filter memory */ 
    int32_t cnt, i;

	if(!demux) return;
	LOG("demux_finish: finishing\n");
	free_events(&demux->demux_event);	
	
	pthread_mutex_destroy(&demux->demux_event.lock);
	pthread_cond_destroy(&demux->demux_event.cond);

    pthread_mutex_lock(&(demux->section_table_mutex));

    /* All function that use demux instance have to check 'demux->alive' variable.
         (Some thread can use demux instance)
     */
    demux->alive = 0;
    
    cnt = demux->filter_table[SECTION_FILTER].used_cnt;
    for (i = 0; i < cnt; i++) {        
        section_filter* pSection = demux->filter_table[SECTION_FILTER].filters[i].sect;
        if(pSection){
            LOG("DEMUX_Destroy : memory free section filter of pid(%d)\n", pSection->pid);
            free(pSection);
            demux->filter_table[SECTION_FILTER].filters[i].sect = NULL;
        }
        memset(&demux->filter_table[SECTION_FILTER].filters[i], 0x00, sizeof(ts_filter));
        demux->filter_table[SECTION_FILTER].filters[i].pid = -1;
        demux->filter_table[SECTION_FILTER].filters[i].tid = -1;
        demux->filter_table[SECTION_FILTER].filters[i].last_cc = -1;        
    }       
    demux->filter_table[SECTION_FILTER].used_cnt = 0;    

    cnt = demux->filter_table[VIDEO_FILTER].used_cnt;
    for (i = 0; i < cnt; i++) {
        pes_filter* pPes = demux->filter_table[VIDEO_FILTER].filters[i].pes;
        if(pPes){
            LOG("DEMUX_Destroy : memory free pes filter of pid(%d)\n", pPes->pid);		
            free(pPes);
            demux->filter_table[VIDEO_FILTER].filters[i].pes = NULL;			
        }        
        memset(&demux->filter_table[VIDEO_FILTER].filters[i], 0x00, sizeof(ts_filter));
        demux->filter_table[VIDEO_FILTER].filters[i].pid = -1;
        demux->filter_table[VIDEO_FILTER].filters[i].tid = -1;
        demux->filter_table[VIDEO_FILTER].filters[i].last_cc = -1;
    }
    demux->filter_table[VIDEO_FILTER].used_cnt = 0;

    pthread_mutex_unlock(&(demux->section_table_mutex));

    /* release handle memory */ 
    free(demux);
	LOG("demux_finish: finished...\n");
}


void *demux_thread_handle(void *arg )
{
    int32_t i;
	demux_context* demux = (demux_context *)arg;
	
	LOG("demux_thread_handle: started.. \n");	

	/* init filter_table for section, audio, video requests. */
	for (i = 0; i < FILTER_TYPE_MAX; i++) {
		demux->filter_table[i].type    = SECTION_FILTER + i;
		demux->filter_table[i].max_cnt = FILTERS_MAX;
		demux->filter_table[i].used_cnt= 0;
		memset(demux->filter_table[i].filters, 0x00, sizeof(ts_filter)*FILTERS_MAX);
	}
	demux->pcr_pid = -1;
	demux->alive = 1;
	demux->stop_requested = 0;
	
	while(!demux->stop_requested) {
		pthread_mutex_lock(&demux->demux_event.lock);
		while(!demux->demux_event.head && !demux->stop_requested) {
			pthread_cond_wait(&demux->demux_event.cond, &demux->demux_event.lock);
		}
		pthread_mutex_unlock(&demux->demux_event.lock);
		demux_process(demux);
	}
	LOG("demux_thread_handle: ended.. \n");	
	demux_finish(demux);
	return 0;
}


/******************************************************************************
 * GLOBAL FUNCTION
 *****************************************************************************/
bool DEMUX_Init(demux_context ** handle, int ts_size) 
{
    demux_context* demux = (demux_context *)calloc(sizeof(char), sizeof(demux_context));
    LOG("DEMUX_Init.. \n");   

    if (demux == NULL) {
        LOGE("DEMUX_Init : demux_context alloc failed.\n");
        goto err;
    }

    demux->ts_size = ts_size;
    *handle = demux;

	int thread_id = 0;
    pthread_mutex_init(&demux->demux_event.lock, 0);	
    pthread_cond_init(&demux->demux_event.cond, 0);

	pthread_attr_t attr_demux_thread;
	/* create the signal handling thread */
	pthread_attr_init(&attr_demux_thread);
	/* set the thread detach state */
	pthread_attr_setdetachstate(&attr_demux_thread, PTHREAD_CREATE_DETACHED);
	
	thread_id = pthread_create( &demux->demux_thread, &attr_demux_thread, demux_thread_handle, demux );
	if (thread_id < 0){
		LOG("DEMUX_Init : pthread_create demux_process returned: %d\n", thread_id);
	}
	
    return true;

err:    
    if (demux) {
        free(demux);
    }
    *handle = NULL;

    return false;
}

void DEMUX_Destroy(demux_context ** handle){

    demux_context* demux;   
    LOG("DEMUX_Destroy.. \n");   

    if(handle == NULL || *handle == NULL){
        LOGE("DEMUX_Destroy : handle is NULL");
        return;
    }   
    demux = *handle;

	demux->stop_requested = 1;
	pthread_cond_broadcast(&demux->demux_event.cond);
	
    *handle = NULL; 
}

bool DEMUX_InitSectionFilter(demux_context * handle) {

    /* start from PAT */

    if(handle == NULL){
        return false;
    }    
    
    section_filter sect;
    sect.pid = PAT_PID;
    sect.tid = PAT_TID;
    sect.callback= DEMUX_PATCallback;
    DEMUX_SetSectionFilter(handle, &sect);
    
    return true;
}

int DEMUX_GetTsSize(char* buffer, int size){

    int n, head=-1;
    int start_pos;
    int ts_size;

    if(buffer == NULL){
        LOGE("DEMUX_GetTsSize : buffer is NULL\n");     
        goto err;
    }
        
    /* search 0x47(ts header) */
    for (n = 0; n < size; n++) {
        if (buffer[n] == SYNC_BYTE) {
            start_pos = n;
            head = n;
            break;
        }            
    }
    if (head == -1) {
        LOGE("DEMUX_GetTsSize : error. not ts data. 0x47 bytes does not exist \n");
        goto err;
    }

    /* calculate ts packet size */
    if (buffer[head] == buffer[head + 188]) {
        ts_size = 188;
    } else if (buffer[head] == buffer[head + 204]) {
        ts_size = 204;
    } else if (buffer[head] == buffer[head + 208]) {
        ts_size = 208;
    } else {
       LOGE("DEMUX_GetTsSize : error. not ts data.\n");
       goto err;
    }
    LOG("DEMUX_GetTsSize : ts_size = %d\n", ts_size);
    
    return ts_size;

err:    
    return 0;
}

bool DEMUX_SetSectionFilter(demux_context *handle, section_filter *sect)
{
    int32_t idx, i;
    section_filter* pSection;

    if(handle == NULL || sect == NULL){
        LOGE("DEMUX_SetSectionFilter : handle=NULL or sect=NULL");
        return false;
    }

    pthread_mutex_lock(&(handle->section_table_mutex));
    CHECK_DEMUX_ALIVE(handle);  
    
    pSection = (section_filter*)malloc(sizeof(section_filter));
    if(pSection==NULL){
        LOGE("DEMUX_SetSectionFilter : malloc failed.");
        return false;
    }   
    pSection->pid = sect->pid;
    pSection->tid = sect->tid;
    pSection->callback = sect->callback;    
    pSection->user_param = sect->user_param;    

    idx = handle->filter_table[SECTION_FILTER].used_cnt;
    if (idx < handle->filter_table[SECTION_FILTER].max_cnt) {
        for (i = 0; i < idx; i++) {
            if (handle->filter_table[SECTION_FILTER].filters[i].pid == pSection->pid
                 && handle->filter_table[SECTION_FILTER].filters[i].tid == pSection->tid) {
                LOG("DEMUX_SetSectionFilter: section pid(%d) duplicated.\n", pSection->pid);
                free(pSection);
                goto end;
            }
        }
        handle->filter_table[SECTION_FILTER].filters[idx].pid = pSection->pid;
        handle->filter_table[SECTION_FILTER].filters[idx].tid = pSection->tid;
        handle->filter_table[SECTION_FILTER].filters[idx].last_cc = -1;
        handle->filter_table[SECTION_FILTER].filters[idx].last_pcr = 0;
        handle->filter_table[SECTION_FILTER].filters[idx].sect = pSection;
        handle->filter_table[SECTION_FILTER].used_cnt++;
        LOG("DEMUX_SetSectionFilter : pid(%d) tid(%x)idx(%d)\n", pSection->pid, pSection->tid,idx);
    }
    else {
        LOGE("DEMUX_SetSectionFilter : failed. pid(%d) tid(%x) \n", pSection->pid, pSection->tid);
        free(pSection);
        pthread_mutex_unlock(&(handle->section_table_mutex));
        return false;
    }
end:
    pthread_mutex_unlock(&(handle->section_table_mutex));
    return true;
}

bool DEMUX_SetPesFilter(demux_context *demux, int32_t ftype, int32_t pid, int32_t codec_type)
{
    bool ret = true;
    int32_t idx, i;
    uint8_t *buf = NULL;
    pes_filter *filter = NULL;

    if (ftype != VIDEO_FILTER) {
        LOG("DEMUX_SetPesFilter: wrong filter type"); 
        goto err;
    }

   if(codec_type == 0x1B) LOG("Video : h.264");	

    idx = demux->filter_table[ftype].used_cnt;
    if (idx < demux->filter_table[ftype].max_cnt) {
        for (i = 0; i < idx; i++) {
            if (demux->filter_table[ftype].filters[i].pid == pid) {
               LOG("DEMUX_SetPesFilter: request pid(%d) duplicated. enc_type(%d)\n", pid, codec_type);
               goto end;
           }
        }
 
        if ((filter = malloc(sizeof(pes_filter))) == NULL) {
            ret = false;
            LOG("DEMUX_SetPesFilter: malloc failed "); 
            goto err;
        }
        memset(filter, 0x00, sizeof(pes_filter));

        filter->pid       = (uint16_t)pid;
        filter->codec_type  = (uint16_t)codec_type;
        
        demux->filter_table[ftype].filters[idx].pid = pid;
        demux->filter_table[ftype].filters[idx].last_cc = -1;
        demux->filter_table[ftype].filters[idx].last_pcr = 0;
        demux->filter_table[ftype].filters[idx].pes = filter;
        if (ftype == VIDEO_FILTER) {
            filter->strm_type = (int32_t)STRM_TYPE_VIDEO; 
        }
        demux->filter_table[ftype].used_cnt++;
    }
    else {
        ret = false;
        LOG("DEMUX_SetPesFilter: max count. failed ");
        goto err;
    }

end:
    return true;

err:
    if (filter) {
        if (buf) {
            free(buf);
        }
        free(filter);
    }
    return false;
}
bool DEMUX_ClearSectionFilter(demux_context *handle, section_filter *sect)
{
    int32_t cnt, i;

    if(handle==NULL || sect==NULL){
        LOGE("DEMUX_ClearSectionFilter : handle=NULL or sect=NULL");
        return false;
    }

    pthread_mutex_lock(&(handle->section_table_mutex));
    CHECK_DEMUX_ALIVE(handle);
    
    cnt = handle->filter_table[SECTION_FILTER].used_cnt;

    for (i = 0; i < cnt; i++) {
        if (handle->filter_table[SECTION_FILTER].filters[i].pid == sect->pid
            && handle->filter_table[SECTION_FILTER].filters[i].tid == sect->tid) {
            
            section_filter* pSection = handle->filter_table[SECTION_FILTER].filters[i].sect;
            if(pSection){
               free(pSection);
               handle->filter_table[SECTION_FILTER].filters[i].sect = NULL;
            }
            if (cnt >= 2) {
                /* move the last section to the removed sapce */
                memcpy(&handle->filter_table[SECTION_FILTER].filters[i], 
                       &handle->filter_table[SECTION_FILTER].filters[cnt-1],
                       sizeof(ts_filter));
            }
            break;
        }        
    }
    /* clear the last section space */
    if(cnt > 0){
        memset(&handle->filter_table[SECTION_FILTER].filters[cnt-1], 0x00, sizeof(ts_filter));
        handle->filter_table[SECTION_FILTER].filters[cnt-1].pid = -1;
        handle->filter_table[SECTION_FILTER].filters[cnt-1].tid = -1;
        handle->filter_table[SECTION_FILTER].filters[cnt-1].last_cc = -1;
        handle->filter_table[SECTION_FILTER].used_cnt--;    
    }
    pthread_mutex_unlock(&(handle->section_table_mutex));
    
    return true;
}

bool DEMUX_ClearAllSectionFilter(demux_context *handle)
{
    int32_t cnt, i;

    if(handle==NULL){
        LOGE("DEMUX_ClearAllSectionFilter : handle=NULL");
        return false;
    }

    pthread_mutex_lock(&(handle->section_table_mutex));
    CHECK_DEMUX_ALIVE(handle);
    
    cnt = handle->filter_table[SECTION_FILTER].used_cnt;

    for (i = 0; i < cnt; i++) {
        section_filter* pSection = handle->filter_table[SECTION_FILTER].filters[i].sect;
        if(pSection){
            free(pSection);
            handle->filter_table[SECTION_FILTER].filters[i].sect = NULL;
           //LOG("DEMUX_ClearAllSectionFilter : memory free section filter of pid(%d) tid(%x)\n", pSection->pid, pSection->tid);        
        }        
        memset(&handle->filter_table[SECTION_FILTER].filters[i], 0x00, sizeof(ts_filter));
        handle->filter_table[SECTION_FILTER].filters[i].pid = -1;
        handle->filter_table[SECTION_FILTER].filters[i].tid = -1;
        handle->filter_table[SECTION_FILTER].filters[i].last_cc = -1;
    }
    handle->filter_table[SECTION_FILTER].used_cnt = 0;
    pthread_mutex_unlock(&(handle->section_table_mutex));
    
    return true;
}

bool DEMUX_ClearAllPesFilter(demux_context *handle)
{
    int32_t cnt, i;

    if(handle==NULL){
        LOGE("DEMUX_ClearAllPesFilter : handle=NULL");
        return false;
    }

    pthread_mutex_lock(&(handle->section_table_mutex));
    CHECK_DEMUX_ALIVE(handle);
    
    cnt = handle->filter_table[VIDEO_FILTER].used_cnt;

    for (i = 0; i < cnt; i++) {
        pes_filter* pPes = handle->filter_table[VIDEO_FILTER].filters[i].pes;
        if(pPes){
            free(pPes);
            LOG("DEMUX_ClearAllPesFilter : memory free pes filter of pid(%d)\n", pPes->pid);
            handle->filter_table[VIDEO_FILTER].filters[i].pes = NULL;
        }        
        memset(&handle->filter_table[VIDEO_FILTER].filters[i], 0x00, sizeof(ts_filter));
        handle->filter_table[VIDEO_FILTER].filters[i].pid = -1;
        handle->filter_table[VIDEO_FILTER].filters[i].tid = -1;
        handle->filter_table[VIDEO_FILTER].filters[i].last_cc = -1;
    }
    handle->filter_table[VIDEO_FILTER].used_cnt = 0;
    pthread_mutex_unlock(&(handle->section_table_mutex));
    
    return true;
}

bool DEMUX_FeedData(demux_context *handle, char *buf, uint32_t size)
{
	if(handle && !handle->stop_requested){
		queue_event_data(&handle->demux_event, DEMUX_EVENT_FEED_DATA, size, buf);
	}
    return true;
}


bool DEMUX_ProcFeedData(demux_context *handle, char *buf, uint32_t size)
{
    bool ret = true;
    ts_header ts_hdr = {0};
    adapt_field_header adapt_hdr = {0};
    ts_filter *filter = NULL;
    int64_t pcr_base;
    int32_t pcr_ext;
    bool cc_ok;
    uint32_t type = FILTER_NONE;
    uint32_t used;
    uint8_t *ptr = (uint8_t*)buf;
    int pid;

    while (size > 0) 
    {
        /* parse header */
        used = sDEMUX_ParseTsHeader(ptr, &ts_hdr);
        pid = ts_hdr.pid;        
        if (((pid > 1) && (pid < 16)) || (pid == 0x1fff)) {
            goto out;
        }

        if (ts_hdr.adaptation_byte_length > 0) {
            if (handle->pcr_pid == ts_hdr.pid) {
                used += sDEMUX_ParsePcr((ptr + used), ts_hdr.adaptation_byte_length, &pcr_base, &pcr_ext);
                handle->curr_pcr = pcr_base * 300 + pcr_ext;
            }
            else {
                used += ts_hdr.adaptation_byte_length;
            }
        }

        if ((ts_hdr.adaptation_field_control == 0x1) || (ts_hdr.adaptation_field_control == 0x3)) 
        {
            //LOG("sDEMUX_FindFilter ts_hdr.pid=%d\n",ts_hdr.pid);
            if (sDEMUX_FindFilter(handle, ts_hdr.pid, &type, &filter) != true) {
                goto out;
            }

            cc_ok = sDEMUX_IsContinuous(&ts_hdr, adapt_hdr.discontinuity_indicator, filter->last_cc);
            filter->last_cc = ts_hdr.continuity_counter;        
            if (cc_ok != true) {
                goto out;
            }

            switch (type) {
                case SECTION_FILTER:
                    used += DEMUX_SectionAddData(handle, &ts_hdr, (ptr + used),
                         (handle->ts_size - used), filter->sect);
                    break;
    
                case VIDEO_FILTER:
                    break;
    
                default:
                    LOGE("unknown filter.\n");
                    break;
            }
        }
out:
        ptr += handle->ts_size;
        size -= handle->ts_size;
    }

    return ret;
}

bool DEMUX_CheckIFrame(demux_context *handle, char *buf, uint32_t size, bool* isIframe)
{
    bool ret = true;
    ts_header ts_hdr = {0};
    adapt_field_header adapt_hdr = {0};
    ts_filter *filter = NULL;
    int64_t pcr_base;
    int32_t pcr_ext;
    bool cc_ok;
    uint32_t type = FILTER_NONE;
    uint32_t used;
    uint8_t *ptr = (uint8_t*)buf;
    int pid;

    while (size > 0) 
    {
        /* parse header */
        used = sDEMUX_ParseTsHeader(ptr, &ts_hdr);
        pid = ts_hdr.pid;        
        if (((pid > 1) && (pid < 16)) || (pid == 0x1fff)) {
            goto out;
        }

        if (ts_hdr.adaptation_byte_length > 0) {
            if (handle->pcr_pid == ts_hdr.pid) {
                used += sDEMUX_ParsePcr((ptr + used), ts_hdr.adaptation_byte_length, &pcr_base, &pcr_ext);
                handle->curr_pcr = pcr_base * 300 + pcr_ext;
            }
            else {
                used += ts_hdr.adaptation_byte_length;
            }
        }

        if ((ts_hdr.adaptation_field_control == 0x1) || (ts_hdr.adaptation_field_control == 0x3)) 
        {
            if (sDEMUX_FindFilter(handle, ts_hdr.pid, &type, &filter) != true) {
                goto out;
            }

            cc_ok = sDEMUX_IsContinuous(&ts_hdr, adapt_hdr.discontinuity_indicator, filter->last_cc);
            filter->last_cc = ts_hdr.continuity_counter;
            if (cc_ok != true) {
                goto out;
            }

            switch (type) {
                case SECTION_FILTER:
                    break;
    
                case VIDEO_FILTER:
                    used += DEMUX_Pes_AddData(handle, &ts_hdr, 
                                                (ptr + used), (handle->ts_size - used), 
                                                filter->pes, isIframe);
                    break;
    
                default:
                    LOGE("unknown filter.\n");
                    break;
            }
        }
out:
        ptr += handle->ts_size;
        size -= handle->ts_size;
    }

    return ret;
}

