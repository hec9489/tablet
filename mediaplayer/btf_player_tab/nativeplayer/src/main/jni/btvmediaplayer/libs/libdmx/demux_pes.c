// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#include <pthread.h> 
#include "demux.h"

/******************************************************************************
 * DEFINE
 *****************************************************************************/
enum{
    SKIP=0,
    OPT_HEADER,
    OPT_HEADER_DATA,
    PAYLOAD
};

enum H265_NALUnitType {
    NAL_TRAIL_N    = 0,
    NAL_TRAIL_R    = 1,
    NAL_TSA_N      = 2,
    NAL_TSA_R      = 3,
    NAL_STSA_N     = 4,
    NAL_STSA_R     = 5,
    NAL_RADL_N     = 6,
    NAL_RADL_R     = 7,
    NAL_RASL_N     = 8,
    NAL_RASL_R     = 9,
    NAL_VCL_N10    = 10,
    NAL_VCL_R11    = 11,
    NAL_VCL_N12    = 12,
    NAL_VCL_R13    = 13,
    NAL_VCL_N14    = 14,
    NAL_VCL_R15    = 15,
    NAL_BLA_W_LP   = 16,
    NAL_BLA_W_RADL = 17,
    NAL_BLA_N_LP   = 18,
    NAL_IDR_W_RADL = 19,
    NAL_IDR_N_LP   = 20,
    NAL_CRA_NUT    = 21,
    NAL_IRAP_VCL22 = 22,
    NAL_IRAP_VCL23 = 23,
    NAL_RSV_VCL24  = 24,
    NAL_RSV_VCL25  = 25,
    NAL_RSV_VCL26  = 26,
    NAL_RSV_VCL27  = 27,
    NAL_RSV_VCL28  = 28,
    NAL_RSV_VCL29  = 29,
    NAL_RSV_VCL30  = 30,
    NAL_RSV_VCL31  = 31,
    NAL_VPS        = 32,
    NAL_SPS        = 33,
    NAL_PPS        = 34,
    NAL_AUD        = 35,
    NAL_EOS_NUT    = 36,
    NAL_EOB_NUT    = 37,
    NAL_FD_NUT     = 38,
    NAL_SEI_PREFIX = 39,
    NAL_SEI_SUFFIX = 40,
    NAL_RSV_NVCL41 = 41,
    NAL_RSV_NVCL42 = 42,
    NAL_RSV_NVCL43 = 43,
    NAL_RSV_NVCL44 = 44,
    NAL_RSV_NVCL45 = 45,
    NAL_RSV_NVCL46 = 46,
    NAL_RSV_NVCL47 = 47,
    NAL_UNSPEC48   = 48,
    NAL_UNSPEC49   = 49,
    NAL_UNSPEC50   = 50,
    NAL_UNSPEC51   = 51,
    NAL_UNSPEC52   = 52,
    NAL_UNSPEC53   = 53,
    NAL_UNSPEC54   = 54,
    NAL_UNSPEC55   = 55,
    NAL_UNSPEC56   = 56,
    NAL_UNSPEC57   = 57,
    NAL_UNSPEC58   = 58,
    NAL_UNSPEC59   = 59,
    NAL_UNSPEC60   = 60,
    NAL_UNSPEC61   = 61,
    NAL_UNSPEC62   = 62,
    NAL_UNSPEC63   = 63,
};

#define PAYLOAD_MAX_SIZE 1024*1024*3

/******************************************************************************
 * LOCAL FUCTION  
 *****************************************************************************/

static void sDEMUX_Pes_ClearState(pes_filter *pes)
{
    pes->header_pos = 0;
    memset(pes->header_buf, 0x00, PES_HDR_BUF_LEN);
}

static inline uint32_t sDEMUX_Pes_ParseHeader(uint8_t *data, pes_header *hdr)
{
    uint32_t used = 0;

    if ((data[0] == 0x00)
       && (data[1] == 0x00)
       && (data[2] == 0x01)) {

        hdr->stream_id = data[3]; 
        hdr->pes_packet_length = ((uint16_t)data[4] << 8) | (uint16_t)data[5];
        
        if (hdr->pes_packet_length == 0) {
            hdr->pes_packet_length = PAYLOAD_MAX_SIZE;
        }
        used = 6;

        /* video only */
        if(hdr->stream_id>=0xe0 && hdr->stream_id<=0xef) {

            hdr->pes_scrambling_control   = (data[6] >> 4) & 0x03; 
            hdr->pes_priority             = (data[6] >> 3) & 0x01; 
            hdr->data_alignment_indicator = (data[6] >> 2) & 0x01; 
            hdr->copyright                = (data[6] >> 1) & 0x01; 
            hdr->original_or_copy         = (data[6] >> 0) & 0x01; 
            hdr->pts_dts_flags            = (data[7] >> 6) & 0x03; 
            hdr->escr_flag                = (data[7] >> 5) & 0x01; 
            hdr->es_rate_flag             = (data[7] >> 4) & 0x01; 
            hdr->dsm_trick_mode_flag      = (data[7] >> 3) & 0x01; 
            hdr->additional_copy_info_flag= (data[7] >> 2) & 0x01; 
            hdr->pes_crc_flag             = (data[7] >> 1) & 0x01; 
            hdr->pes_extension_flag       = (data[7]) & 0x01; 
            hdr->pes_header_data_length   = data[8]; 
            used = 9;
        }
    }

    return used;
}

static inline void sDEMUX_Pes_ParseHeaderData(pes_filter *pes, uint8_t *data)
{
    int32_t used = 0;
    if (pes->header.pts_dts_flags == 2) {
        used += 5;
    }
    else if (pes->header.pts_dts_flags == 3) {
        used += 10;
    }
    else {
        LOG("pts dts flag=%d\n", pes->header.pts_dts_flags);
    }

    if (pes->header.escr_flag == 1) {
        used += 6;
    }
    if (pes->header.es_rate_flag == 1) {
        used += 3;
    }
    if (pes->header.dsm_trick_mode_flag == 1) {
        used += 1;
    }
    if (pes->header.additional_copy_info_flag == 1) {
        used += 1;
    }
    if (pes->header.pes_crc_flag == 1) {
        used += 2;
    }

    if (pes->header.pes_extension_flag == 1) {
        int8_t pes_private_data_flag        = data[used] & 0x80;
        int8_t pack_header_field_flag       = data[used] & 0x40;
        int8_t program_packet_seq_coun_flag = data[used] & 0x20;
        int8_t p_std_buffer_flag            = data[used] & 0x10;
        int8_t pes_extension_flag_2         = data[used] & 0x01;
        if (pes_private_data_flag) {
            used += 16;
        }
        if (pack_header_field_flag) {
            used += 1;
        }
        if (program_packet_seq_coun_flag) {
            used += 2;
        }
        if (p_std_buffer_flag) {
            used += 2;
        }

        if (pes_extension_flag_2) {
            used += 2;
            if (((data[used] & 0x07) > 0) && ((data[used+1] & 0x80) == 0)) {
                pes->stream_id_extension = pes->header_buf[pes->header_pos+1];
            }
        }
    }
}

static int sDEMUX_Pes_FindNextStartCode(uint8_t *buf, uint8_t *next_avc,
    int start_code_size, bool* found){ 
    int i = 0;  
    
    while (buf + i + start_code_size < next_avc) {
        if(start_code_size == 3){ 
            if (buf[i] == 0 && buf[i + 1] == 0 && buf[i + 2] == 1){ 
                *found = true;
                break;
            }
        }else if(start_code_size == 4){ 
            if (buf[i] == 0 && buf[i + 1] == 0 && buf[i + 2] == 0 && buf[i + 3] == 1){
                *found = true;
                break;
            }
        }else{
            LOGE("sDEMUX_Pes_FindNextStartCode : start code error");
            return -1;
        }
        i++;
    }
    return i + start_code_size;
}

extern unsigned int sDEMUX_Pes_ReadBits(unsigned char bytes[], int num_read, int *bit_offset)
{        
    int   i;
    int   bit_shift;
    int   byte_offset, bit_offset_in_byte;
    int   num_bytes_copy;
    unsigned int   bits;
    unsigned int   utmp;
    
    if (num_read > 24)
        return 0xFFFFFFFF;
    if (num_read == 0)
        return 0;

    byte_offset = (*bit_offset) >> 3;
    bit_offset_in_byte = (*bit_offset) - (byte_offset << 3);
    num_bytes_copy = ((*bit_offset + num_read) >> 3) - (*bit_offset >> 3) + 1;
    bits = 0;

    for (i=0; i<num_bytes_copy; i++) {
        utmp = bytes[byte_offset + i];
        bits = (bits << 8) | (utmp);
    }
    
    bit_shift = (num_bytes_copy << 3) - (bit_offset_in_byte + num_read);
    bits >>= bit_shift;
    bits &= (0xFFFFFFFF >> (32 - num_read));
    
    *bit_offset += num_read;
    
    return bits;
}

extern unsigned int sDEMUX_Pes_Ue_v(unsigned char* bytes, int *bit_offset)
{
    unsigned int   b;
    int            leadingZeroBits = -1;
    unsigned int   codeNum;
    
    for (b=0; !b; leadingZeroBits++) {
        b = sDEMUX_Pes_ReadBits(bytes, 1, bit_offset);
    }
    codeNum = (1 << leadingZeroBits)-1 + sDEMUX_Pes_ReadBits(bytes, leadingZeroBits, bit_offset);
    return codeNum;
}

static bool sDEMUX_Pes_IsIframe(int slice_type){
    switch(slice_type){
        case 2:
        case 4: 
        case 7:
        case 9:
            return true;
    }
    return false;
}

static bool sDEMUX_Pes_FindH264Iframe(uint8_t *data, int len){ 
    int nalu_type;
    int start_code_size;
    int currentIndex = 0;
    bool nonIDR=false;
    bool sps=false;
    bool pps=false;
    uint32_t first_mb = 1;
    uint32_t slice_type;    

    if((data[3]  & 0x1F ) == 9){
        start_code_size = 3;
        currentIndex = 3;
    }else if((data[4]  & 0x1F ) == 9){
        start_code_size = 4;
        currentIndex = 4;
    }else{
        LOGE("sDEMUX_Pes_FindNextStartCode : start code error");
        return false;
    }

    while(currentIndex < len){
        bool found = false;     
        currentIndex += sDEMUX_Pes_FindNextStartCode(data+currentIndex, data+len, start_code_size, &found);
        if(found){      
            nalu_type = data[currentIndex] & 0x1F;
        }else{
            continue; 
        }
        if(nalu_type == 5){
            LOG("sDEMUX_Pes_FindNextStartCode : found I frame");
            return true;
        }else if(nalu_type == 7){
            sps = true;
        }else if(nalu_type == 8){
            pps = true;
        }else if(nalu_type == 1){
            int bit_offset=0;
            first_mb = sDEMUX_Pes_Ue_v(&data[currentIndex+1], &bit_offset);
            slice_type = sDEMUX_Pes_Ue_v(&data[currentIndex+1], &bit_offset);
            nonIDR = true;
            if(nonIDR && sDEMUX_Pes_IsIframe(slice_type)){
                LOG("sDEMUX_Pes_FindNextStartCode : found non IDR I frame. slice_type=%d", slice_type);
                return true;
            }
        }
    }
    
    if(sps & pps){
        LOG("sDEMUX_Pes_FindNextStartCode : found non IDR I frame. sps=%d, pps=%d", sps, pps);
        return true;
    }   
    return false;
}

static bool sDEMUX_Pes_FindH265Iframe(uint8_t *data, int len){ 
    int nalu_type;
    int start_code_size;
    int currentIndex = 0;
    bool sps=false;
    bool pps=false;

    if((data[2]) == 1){
        start_code_size = 3;
    }else if((data[3]) == 1){
        start_code_size = 4;
    }else{
        LOGE("sDEMUX_Pes_FindNextStartCode : start code error");
        return false;
    }    

    while(currentIndex < len){
        bool found = false;     
        currentIndex += sDEMUX_Pes_FindNextStartCode(data+currentIndex, data+len, start_code_size, &found);
        if(found){      
           nalu_type = (data[currentIndex]>>1) & 0x3F;
        }else{
               continue;
        }
		
        //LOGV("sDEMUX_Pes_FindNextStartCode : nalu_type=%x",nalu_type);      
        if(nalu_type == NAL_IDR_W_RADL || nalu_type == NAL_IDR_N_LP){
            LOG("sDEMUX_Pes_FindNextStartCode : found I frame(%d)",nalu_type);
            return true;
        }else if(nalu_type == NAL_SPS){
            sps = true;
        }else if(nalu_type == NAL_PPS){
            pps = true;
        }else if(nalu_type == NAL_CRA_NUT){
            LOG("sDEMUX_Pes_FindNextStartCode : found I frame(%d)",nalu_type);
            return true;
        }
    }
    
    if(sps & pps){
        LOG("sDEMUX_Pes_FindNextStartCode : found non IDR I frame. sps=%d, pps=%d", sps, pps);
        return true;
    }   
    return false;
}
static  bool sDEMUX_Pes_Parse(demux_context *handle, ts_header *ts_header, 
                                uint8_t *data, int32_t size, pes_filter *pes, bool* pIsIframe)
{
    int32_t len;
    uint8_t is_start = ts_header->payload_unit_start_indicator;

    *pIsIframe = false;
    
    if (is_start) {
        pes->es_buf.data_size = 0; 
        pes->state = OPT_HEADER;
        sDEMUX_Pes_ClearState(pes);
    }else{
        /* we can only check I frame in start packet. so skip it */ 
        return true;
    }

    while (size > 0) 
    {
        switch (pes->state) {
        case OPT_HEADER:
            len = size;
            if (len >= PES_OPT_HDR_LEN) {
                len = PES_OPT_HDR_LEN;
            }

            memcpy(&pes->header_buf[pes->header_pos], data, len);
            pes->header_pos += len;

            if (pes->header_pos >= PES_OPT_HDR_LEN) 
            {
                uint32_t hdr_size;
                hdr_size = sDEMUX_Pes_ParseHeader(pes->header_buf, &pes->header);

                data += hdr_size;
                size -= hdr_size;
                
                if (hdr_size == PES_START_HDR_LEN) {
                    pes->state = PAYLOAD;
                    pes->es_buf.data_size = 0;
                    pes->header_pos = 0;
                    sDEMUX_Pes_ClearState(pes);
                }
                else if (hdr_size == PES_OPT_HDR_LEN) {
                    pes->state = OPT_HEADER_DATA;
                    pes->header_pos = 0;
                }
                else {
                    pes->state = SKIP;
                    LOGE("header is wrong");    
                }
            }
            else {
                data += len;
                size -= len;
            }
            break;

        case OPT_HEADER_DATA:
            len = pes->header.pes_header_data_length - pes->header_pos;
            if (len < 0) {
                return false;
            }
            if (len > size) {
                len = size;
            }

            memcpy(pes->header_buf + pes->header_pos, data, len);
            pes->header_pos += len;
            data += len;
            size -= len;

            if (pes->header_pos == pes->header.pes_header_data_length) {
                sDEMUX_Pes_ParseHeaderData(pes, pes->header_buf);

                pes->state = PAYLOAD;
                pes->es_buf.data_size = 0;
            }
            break;

        case PAYLOAD:
            len = pes->header.pes_packet_length - pes->es_buf.data_size;
            if (len < 0) {
                return false;
            }
            if (len > size) {
                len = size;
            }

            /* check I frame at the start packet os ES */
            if(pes->es_buf.data_size == 0){
                switch(handle->video_codec){
					
                    case 0x1B: /* H.264 */
                        if(sDEMUX_Pes_FindH264Iframe(data, len)){
                            *pIsIframe = true;
                            return true;
                        }
                        break;
						
                   case 0x24: /* H.265 */
                        if(sDEMUX_Pes_FindH265Iframe(data, len)){
                            *pIsIframe = true;
                            return true;
                        }
                        break;
						
                   default:
                        break;
                }                
            }

            pes->es_buf.data_size += len;
            data += len;
            size -= len;

            /* a packet is completed */
            if (pes->es_buf.data_size == pes->header.pes_packet_length) {
                pes->state = SKIP;
                sDEMUX_Pes_ClearState(pes);
            }
            break;

        case SKIP:
            size = 0;
            break;
        default:
            LOG("invalid pes state : (%d)\n", pes->state);
            break;
        }
    }

    return true;
}

/******************************************************************************
 * GLOBAL FUCTION  
 *****************************************************************************/
uint32_t DEMUX_Pes_AddData(demux_context *handle, ts_header *ts_hdr, 
                           uint8_t *data, uint32_t size, pes_filter *filter, bool* pIsIframe)
{
    pthread_mutex_lock(&(handle->section_table_mutex));
    CHECK_DEMUX_ALIVE(handle);
    
    if (!sDEMUX_Pes_Parse(handle, ts_hdr, data, size, filter, pIsIframe)) {
        LOGE("parse_pes failed.\n");
        sDEMUX_Pes_ClearState(filter);
    }
    pthread_mutex_unlock(&(handle->section_table_mutex));
    return size;
}
