// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
/* open */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
/*open*/

#include <pthread.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <linux/dvb/dmx.h>
#include <stdbool.h>
#include <sched.h>
#include "multiViewRtp.h"

#include <libavformat/avio.h>
#include <libavformat/avformat.h>

#include <multiview/source/multiview_demuxer.h>
#include <multiview/source/multiview_muxer.h>

static ST_FFMPEG_DEMUX_CTX** g_pDemuxer;
static fd_set               g_fdDemux;
pthread_mutex_t             g_mutex = PTHREAD_MUTEX_INITIALIZER;

#include <dbg.h>
#include "dvb_primitive.h"
#include <RtpUtils.h>

struct timeval tv_now;
#if 0
int getrtp2(int fd, struct rtpheader *rh, char **data, int *lengthData, int nDevice)
{
    getRtp2Buffer(fd, rh, *data, lengthData, nDevice, NULL, NULL);
    return 0;
}
#endif
#if 1
static int getrtp2(int fd, struct rtpheader *rh, char **data, int *lengthData, int nDevice) {
    static char buf[6][1600];
    unsigned int intP;
    char *charP = (char *)&intP;
    int headerSize;
    int lengthPacket;

    lengthPacket = recv(fd, buf[nDevice], 1590, 0);

    if (lengthPacket == 0)
        exit(1);
    if (lengthPacket < 0) {
        fprintf(stderr, "socket read error\n");
        printf("getrtp2 failed: - %s\n", strerror (errno));
        exit(2);
    }
    if (lengthPacket < 12) {
        fprintf(stderr, "packet too small (%d) to be an rtp frame (>12bytes)\n", lengthPacket);
        exit(3);
    }

    if(buf[nDevice][0]==0x47)
    {
        headerSize =0;
    }
    else if ((unsigned int)((buf[nDevice][0] >> 6) & 0x03)==2)  // Assuming it's rtp header
    {
        rh->b.v = (unsigned int)((buf[nDevice][0] >> 6) & 0x03);
        rh->b.p = (unsigned int)((buf[nDevice][0] >> 5) & 0x01);
        rh->b.x = (unsigned int)((buf[nDevice][0] >> 4) & 0x01);
        rh->b.cc = (unsigned int)((buf[nDevice][0] >> 0) & 0x0f);
        rh->b.m = (unsigned int)((buf[nDevice][1] >> 7) & 0x01);
        rh->b.pt = (unsigned int)((buf[nDevice][1] >> 0) & 0x7f);
        intP = 0;
        memcpy(charP + 2, &buf[2], 2);
        rh->b.sequence = ntohl(intP);
        intP = 0;
        memcpy(charP, &buf[nDevice][4], 4);
        rh->timestamp = ntohl(intP);
        headerSize = 12 + 4 * rh->b.cc;	/* in bytes */
    }
    else // what's this
    {
        fprintf(stderr, "can't recognize header\n");
        exit(3);
    }

    *lengthData = lengthPacket - headerSize;
    *data = (char *)buf[nDevice] + headerSize;

    //  fprintf(stderr,"Reading rtp: v=%x p=%x x=%x cc=%x m=%x pt=%x seq=%x ts=%x lgth=%d\n",rh->b.v,rh->b.p,rh->b.x,rh->b.cc,rh->b.m,rh->b.pt,rh->b.sequence,rh->timestamp,lengthPacket);

    return 0;
}
#endif

static int read_packet(void *opaque, uint8_t *buffer, int buf_size) {
    pt_MultiviewInfo ptMultiviewInfo = (pt_MultiviewInfo) opaque;
    dvb_player_t *player = ptMultiviewInfo->pPlayer;

    int multiViewNumber = ptMultiviewInfo->viewNumber;
    int *Terminate = &player->TerminateTuner;
    int nDevice = ptMultiviewInfo->idx_player_for_multiview; // player->idx_player;

    RtpInfo_t *pRtpInfo = gRtpInfo[nDevice];

    int ret     = 0;
    fd_set fds;
    struct timeval tv;

    char *buf;
    struct rtpheader rh;
    int lengthData = 0;
    int total_lengthData = 0;

    int nQiSendSize = 0;
    char *nBufQiPtr = NULL;

    //logLogCatKey_stdvb("multiview read_packet called ****");
    FD_ZERO(&fds);

    FD_SET(pRtpInfo->socketIn,&fds);

    tv.tv_sec   = 1;  //
    tv.tv_usec  = 0;

    ret = select(pRtpInfo->socketIn +1,&fds, NULL, NULL, &tv);

    if(!*Terminate && ret > 0 && FD_ISSET(pRtpInfo->socketIn,&fds)) {
       /* if(pFlag_first_socket_read != NULL && *pFlag_first_socket_read) {
            *pFlag_first_socket_read = 0;
            logLogCatKey_stdvb("++[rtp.c][DEVICE %d] *************************************************", nDevice);
            logLogCatKey_stdvb("++[rtp.c][DEVICE %d] First read from socket", nDevice);

        }*/

        getrtp2(pRtpInfo->socketIn, &rh, &buf, &lengthData, nDevice);

        if(*Terminate ) return 0;

        //logLogCatKey_stdvb("++ recv size : %d", lengthData);
        total_lengthData = (int) (pRtpInfo->buf_writeptr - pRtpInfo->buf_readptr);

        //if( (total_lengthData + lengthData) < SIZE ) {
        if(lengthData < buf_size) {
            if (lengthData < 0) {
                logLogCatKey_stdvb("++[rtp.c][DEVICE %d] error, cas packet size : %d", nDevice, lengthData);
                return 0;
            }

//	        logLogCatKey_stdvb("read_packet bufsize(%d)", lengthData);
            memcpy((void *)buffer, (void *)buf, lengthData);

            pRtpInfo->buf_readOneOrMoreData = 1; // always set true and it's cleared by other thread
        } else {
            //log_err("recv buffer is full", __FUNCTION__);
            //log_err("dropped received packets", __FUNCTION__);
            logLogCatKey_stdvb("+++ [MultiView:%d] Device[%d] ecv buffer is full, dropped received packets" , multiViewNumber, nDevice);
        }
        usleep(10);
    } else logLogCatKey_stdvb("Multiview ++Device[%d] socket timeout" , nDevice);

    return lengthData;
}


//종료관련..
void thread_exit_handler(int sig)
{
    logLogCatKey_stdvb("[MULTIVIEW] this signal is %d \n", sig);
    pthread_exit(0);
}

void *multiview_recv_thread(void *ThreadParam) {

    pt_MultiviewInfo ptMultiviewInfo = (pt_MultiviewInfo) ThreadParam;
    dvb_player_t *player = ptMultiviewInfo->pPlayer;

    int result = 0;
    int multiViewNumber = ptMultiviewInfo->viewNumber;
    int *Terminate = &player->TerminateTuner;
    int nDevice = ptMultiviewInfo->idx_player_for_multiview; // player->idx_player;

    RtpInfo_t *pRtpInfo = gRtpInfo[nDevice];
    logLogCatKey_stdvb("[MULTIVIEWTHREAD] recv thread start.. threadParam(0x%x) , pid(%d), tid(%d)", ThreadParam, getpid(), gettid());
    logLogCatKey_stdvb("++ [Device %d][Multiview %d] %s start!!!!! ", nDevice, multiViewNumber, __FUNCTION__);

#ifdef MEASURE_ZAPPING
    int flag_first_socket_read = 1;
#endif
    int flag_find_first_iframe = 1;

    set_thread_priority(2);

    //종료관련..
    struct sigaction actions;
    memset(&actions, 0, sizeof(actions));
    sigemptyset(&actions.sa_mask);
    //sigaddset(&actions, SIGUSR1);
    actions.sa_flags = 0;
    actions.sa_handler = thread_exit_handler;
    sigaction(SIGUSR1, &actions, NULL);


    sigset_t newmask;
    sigemptyset(&newmask);
    sigaddset(&newmask, SIGUSR1);
    pthread_sigmask(SIG_UNBLOCK , &newmask, NULL);

    logLogCatKey_stdvb("++ [DEVICE %d][Multiview %d] recv data ready...", nDevice, multiViewNumber);

    logLogCatKey_stdvb("multiview channelidx(%d), pid(%d), vid(%d), aid(%d)", multiViewNumber,
         ptMultiviewInfo->tLocator.pid,  ptMultiviewInfo->tLocator.vid,  ptMultiviewInfo->tLocator.aid);
    // TODO - multiview 여기에 channel index 값이 포함되어야 함.
    result = multiview_create_demuxer_stream(&g_pDemuxer[multiViewNumber], multiViewNumber,
                                             ptMultiviewInfo->tLocator.pid,
                                             ptMultiviewInfo->tLocator.vid,
                                             ptMultiviewInfo->tLocator.aid,
                                             (void*)ptMultiviewInfo, &read_packet, Terminate);
    if(result < 0)
    {
        logLogCatKey_stdvb("create demuxer failed.");
        goto multiview_demuxer_thread_final;
    }
    pthread_mutex_lock(&g_mutex);
    FD_SET(multiViewNumber, &g_fdDemux);
    logLogCatKey_stdvb("DemuxerContext created.  multiviewNumber(%d)", multiViewNumber);
    pthread_mutex_unlock(&g_mutex);

    multiview_demux_stream(g_pDemuxer[multiViewNumber], player->hMultiView, Terminate);
    multiview_destroy_demuxer(&g_pDemuxer[multiViewNumber]);


multiview_demuxer_thread_final:
    if(!ptMultiviewInfo->isTerminateComplete) {
        ptMultiviewInfo->isTerminateComplete = true;

        pthread_mutex_lock(&ptMultiviewInfo->multiview_thread_lock);
        int e = pthread_cond_signal(&ptMultiviewInfo->multiview_thread_cond);
        pthread_mutex_unlock(&ptMultiviewInfo->multiview_thread_lock);
    }
	logLogCatKey_stdvb("++ [DEVICE %d][Multiview %d] %s terminate!!!!!", nDevice, multiViewNumber, __FUNCTION__);
    logLogCatKey_stdvb("[MULTIVIEWTHREAD] recv thread terminate.. threadParam(0x%x)", ThreadParam);

    return 0;
}

static int write_packet(void *opaque, uint8_t *buffer, int buf_size)
{
	int result = 0;

	//logLogCatKey_stdvb("multiview write_packet buffer(0x%x), size(%d)", buffer, buf_size);

	if(opaque == NULL || buffer == NULL || buf_size <= 0)
	{
		logLogCatKey_stdvb("multiview write_packet failed. func(%s)", __FUNCTION__);
		return -1;
	}

    pt_MultiviewInfo ptMultiviewInfo = (pt_MultiviewInfo) opaque;
    dvb_player_t *player = ptMultiviewInfo->pPlayer;

    int *Terminate = &player->TerminateTuner;
    int nDevice = ptMultiviewInfo->idx_player_for_multiview; // player->idx_player;
    RtpInfo_t *pRtpInfo = gRtpInfo[nDevice];

	//saveMultiviewRawData(buffer, buf_size, false);

    int written = 0;
    int writeTotalFeedSize = 0;

	//logLogCatKey_stdvb("multiview write_packet bufsize(%d)", buf_size);
    int lengthData = buf_size;
    int total_lengthData = 0;
    int remainBufferSize = 0;
    char *buf = buffer;

    if(pRtpInfo->buf_writeptr < pRtpInfo->buf_readptr) {
        total_lengthData = pRtpInfo->buf_max - pRtpInfo->buf_readptr;
        total_lengthData += (pRtpInfo->buf_writeptr - pRtpInfo->buf_min);
    } else total_lengthData = pRtpInfo->buf_writeptr - pRtpInfo->buf_readptr;

    if ((total_lengthData + lengthData) < SIZE) {
        remainBufferSize = pRtpInfo->buf_max - pRtpInfo->buf_writeptr;
        if (remainBufferSize < lengthData) {
            memcpy((void *) pRtpInfo->buf_writeptr, (void *) buf, remainBufferSize);
            pRtpInfo->buf_writeptr = pRtpInfo->buf_min;
            total_lengthData += remainBufferSize;
            buf += remainBufferSize;
            lengthData -= remainBufferSize;
        }
    } else {
        //full buffer... reset
        total_lengthData = 0;
        pRtpInfo->buf_writeptr = pRtpInfo->buf_readptr = pRtpInfo->buf_min;
    }

    memcpy((void *)pRtpInfo->buf_writeptr, (void *)buf, lengthData);
    pRtpInfo->buf_writeptr += lengthData;
    total_lengthData += lengthData;

    for(int i = 0 ; i < 5 && !*Terminate && writeTotalFeedSize < buf_size ; i++) {
        int minBuffer = 0;
        if(pRtpInfo->buf_writeptr < pRtpInfo->buf_readptr) {
            minBuffer = pRtpInfo->buf_max - pRtpInfo->buf_readptr;

#ifdef USE_BTV_HAL_MANAGER
            written = HalBtv_InjectTS(player->idx_player, pRtpInfo->buf_readptr, minBuffer);
#else
            written = AVP_InjectTS(player->avpInfo.playerHandle, pRtpInfo->buf_readptr, minBuffer);
#endif
            // 넘긴많큼 썻다면 나머지도 밀어넣어본다..
            if(written == minBuffer && total_lengthData > written) {
#ifdef USE_BTV_HAL_MANAGER
                written += HalBtv_InjectTS(player->idx_player, pRtpInfo->buf_min, total_lengthData - written);
#else
                written += AVP_InjectTS(player->avpInfo.playerHandle, pRtpInfo->buf_min, total_lengthData - written);
#endif
            }
        } else {
#ifdef USE_BTV_HAL_MANAGER
            written = HalBtv_InjectTS(player->idx_player, pRtpInfo->buf_readptr, total_lengthData);
#else
            written = AVP_InjectTS(player->avpInfo.playerHandle, pRtpInfo->buf_readptr, total_lengthData);
#endif
        }

        if (written > 0) {
            player->isFeedMultiviewData = 2;
            if(ptMultiviewInfo->readCnt == 0) {
                ptMultiviewInfo->readCnt++;

                { // TODO : jung2604 : 20190429 : zapping time check용..
                    if ( 0 == clock_gettime( CLOCK_REALTIME, &player->tTempTimeVal)) {
                        player->tempTimeMs = (player->tTempTimeVal.tv_sec * 1000) + (player->tTempTimeVal.tv_nsec / 1000000);
                        player->timeCheckFirstFeedingTime = player->tempTimeMs;
                        LOGI("**_** multiview first data feeding start : %ld ,start ~ now : %d ms ", player->tempTimeMs, player->tempTimeMs - player->timeCheckStartTime);
                    }
                }
            }
            if(minBuffer > 0 && minBuffer <= written) {
                pRtpInfo->buf_readptr = pRtpInfo->buf_min + (written - minBuffer);
            } else pRtpInfo->buf_readptr += written;
            if(pRtpInfo->buf_max <= pRtpInfo->buf_readptr) pRtpInfo->buf_readptr = pRtpInfo->buf_min;
        }

        if(written <= 0) {
            usleep(200000); // TODO : feed실패시 딜레이 발생!! 학인!!
            //usleep(10);
            break;
        }

        writeTotalFeedSize += written;
        break;
        //logLogCatKey_stdvb("++ write size : %d/%d", buf_size, written);
    }

//	LOGI("multiview write_packet successresult(%d)", result);

	return result;
}

int multiviewMuxerStream(pt_MultiviewInfo ptMultiviewInfo) {
    int *Terminate = &ptMultiviewInfo->pPlayer->TerminateTuner;
    dvb_player_t *player = ptMultiviewInfo->pPlayer;
	ST_FFMPEG_MUX_CTX* ctx = NULL;
	int demuxerCnt = 3;
	struct timeval timeout;
	int state;
	bool isReady = false;

	timeout.tv_sec = 0;
	timeout.tv_usec = 300;

	// TODO - g_pDemuxer 의 값이 모두 할당되었는지 확인이 필요함.
	while(!*Terminate) {
		pthread_mutex_lock(&g_mutex);
#if 0
		state = select(MULTIVIEW_MUXER_CHANNEL, &g_fdDemux, NULL, NULL, &timeout);
		if (state >= MULTIVIEW_MUXER_CHANNEL ) {
#else
		if(FD_ISSET(0, &g_fdDemux) && FD_ISSET(1, &g_fdDemux) && FD_ISSET(2, &g_fdDemux)) {
#endif
//			logLogCatKey_stdvb("multiview DemuxerContext initialize result(%d)", state);
			isReady = true;
		}
		pthread_mutex_unlock(&g_mutex);
		if(isReady) break;

//		logLogCatKey_stdvb("multiview select failed.");
		usleep(1000);
	}
	
    if(!isReady) {
        logLogCatKey_stdvb("multiview DemuxerContext not ready.");
        return 0;
    }

	int result = multiview_create_muxer_stream(&ctx, g_pDemuxer, demuxerCnt,
                                               ptMultiviewInfo, &write_packet);
	if(result < 0)
	{
		logLogCatKey_stdvb("multiview_create_muxer_stream failed.");
		return 0;
	}

	multiview_mux_stream(ctx, player->hMultiView, Terminate);

	//saveMultiviewRawData(NULL, 0, true);

	result = multiview_destroy_muxer(&ctx);
	if(result < 0)
	{
		logLogCatKey_stdvb("multiview_destroy_muxer failed.");
		return 0;
	}

    return 1;
}

void *multiview_feed_thread(void *ThreadParam) {
    pt_MultiviewInfo ptMultiviewInfo = (pt_MultiviewInfo) ThreadParam;
    dvb_player_t *player = ptMultiviewInfo->pPlayer;

    int multiViewNumber = ptMultiviewInfo->viewNumber;
    int *Terminate = &player->TerminateTuner;
    int nDevice = ptMultiviewInfo->idx_player_for_multiview; // player->idx_player;

    logLogCatKey_stdvb("[MULTIVIEWTHREAD] feed thread start.. threadParam(0x%x), idx(%d), pid(%d), tid(%d)", ThreadParam, player->multiviewType, getpid(), gettid());

    logLogCatKey_stdvb("++ [Device %d][Multiview %d] %s start!!!!! ", nDevice, multiViewNumber, __FUNCTION__);

    RtpInfo_t *pRtpInfo = gRtpInfo[nDevice];

    long long  buffering_usec=0;
    long long  now_usec = 0;
    long long  last_injection_usec = 0;

    set_thread_priority(2);

    int nQiSendSize = 0;
    char *nBufQiPtr = NULL;

    int total_lengthData = 0;
    int moveSize = 0;
    int size    = 0;

    int written = 0;

    int iRet = 0;

    gettimeofday(&tv_now,NULL);
    long long  start_usec =  ( (tv_now.tv_sec << 20) + tv_now.tv_usec);
    long long  end_usec = start_usec;

    last_injection_usec = end_usec + ((BUFFERING_DURATION) * 2);

    while (!*Terminate ) {
        multiviewMuxerStream(ptMultiviewInfo);
        break;
    } //while (!*Terminate )

    logLogCatKey_stdvb("++ [DEVICE %d][Multiview %d] %s terminate!!!!!", nDevice, multiViewNumber, __FUNCTION__);
    logLogCatKey_stdvb("[MULTIVIEWTHREAD] feed thread terminate.. threadParam(0x%x)", ThreadParam);

    return 0;
}

static void saveMultiviewRawData(uint8_t* buf, int size, bool isClose){
	static FILE* fp = NULL;
	int result = 0;
	static uint16_t totalsize = 0;

	if(!fp) {
		fp = fopen("/mnt/sdcard/h265/multiview_video_20171221.ts", "wb");
	}

	if(isClose && fp != NULL)
	{
		fclose(fp);
		return;
	}

	if(buf == NULL || size <= 0)
	{
		log_err("buffer or size invalid data.  buf(0x%x), size(%d)", (unsigned int)buf, size);
		return;
	}

	result = fwrite(buf, 1, size, fp);
	totalsize += result;
	if(result >= size)
		log_info("ts result data write success data size(%d), totalsize(%u)", result, totalsize);
}

void multiview_log_callback(void *ptr, int level, const char *fmt, ...)
{
    char szBuf[4096] = {0,};
    va_list vargs;

    va_start(vargs, fmt);
    vsprintf(szBuf, fmt, vargs);
    va_end(vargs);
    //sprintf(szBuf, fmt, vargs);

    if(level == AV_LOG_ERROR)
        logLogCatKey_stdvb("multivew - %s", szBuf);
//    else
//        log_info("multiview - %s", szBuf);

}

void multiview_init()
{
    logLogCatKey_stdvb("++ multiview_init.");

    if(g_pDemuxer != NULL)
    {
        free(g_pDemuxer);
        g_pDemuxer = NULL;
    }

    av_register_all();
    //avcodec_register_all ();

    // ffmpeg log 출력 처리
//    av_log_set_level(AV_LOG_ERROR);

    //FIXME : 에러가 나는 경우가 있어서 제거..av_log_set_callback(multiview_log_callback);

//	g_mutex = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
	pthread_mutex_init(&g_mutex, NULL);

    g_pDemuxer = (ST_FFMPEG_DEMUX_CTX**)malloc(sizeof(ST_FFMPEG_DEMUX_CTX*) * MULTIVIEW_MUXER_CHANNEL);
    if(g_pDemuxer == NULL)
    {
        logLogCatKey_stdvb("memory alloc failed.");
        return;
    }

    mvQueue_init();

    memset(g_pDemuxer, 0x0, sizeof(ST_FFMPEG_DEMUX_CTX*) * MULTIVIEW_MUXER_CHANNEL);
//	g_mutex = PTHREAD_MUTEX_INITIALIZER;
    multiview_demuxer_init();

	FD_ZERO(&g_fdDemux);
}

void multiview_final()
{
	logLogCatKey_stdvb("multiview finalize... (%s)", __FUNCTION__);
    if(g_pDemuxer != NULL)
    {
        free(g_pDemuxer);
        g_pDemuxer = NULL;
    }

    multiview_demuxer_final();
}
