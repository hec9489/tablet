// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <zlib.h>

#include "dsmcc-carousel.h"
#include "dsmcc-receiver.h"
#include "dsmcc-descriptor.h"
#include "dsmcc-biop.h"
#include "dsmcc-cache.h"
#include "dsmcc-util.h"
#include "dsmcc.h"
#include <systemproperty.h>
void
dsmcc_handle_download_done(dsmcc_ctx *ctx, int cached) {
	struct obj_carousel* car;
	car = ctx->active_carousel;

	int eventType = APP_INFO_SIGNAL;
/*	
	if(car->flag == APP_UPDATE_DOWN){
		eventType = APP_INFO_CHANGED;
		car->flag = APP_NEW_DOWN;
	}
*/	
	dsmcc_process_send_event_toapp(ctx, eventType, cached);
	ctx->done_callback(DSMCC_SECTION_DATA, ctx->user_param);		
}

int
dsmcc_check_download_done(dsmcc_ctx *ctx) {
	int found = 0;
	struct cache_module_data *cachep = NULL;
	struct obj_carousel *car;

	car = ctx->active_carousel;
	cachep = car->cache;
	for(; cachep != NULL; cachep = cachep->next) {
		if(!cachep->cached) {
			dsmcc_log("[libdsmcc] module (%d) doing \n", cachep->module_id);
			found = 1;
			break;
		}else{
			dsmcc_log("[libdsmcc] module (%d) done \n", cachep->module_id);		
		}
	}

	if(found) return 0;

	return 1;		
}


int
dsmcc_process_section_header(struct dsmcc_section *section, unsigned char *data, int length) {
	struct dsmcc_section_header *header = &section->sec; 

	int crc_offset = 0;

	header->table_id = data[0];

	header->flags[0] = data[1];
	header->flags[1] = data[2];

	/* Check CRC is set and private_indicator is set to its complement,
	 * else skip packet */
	if(((header->flags[0] & 0x80) == 0) || (header->flags[0] & 0x40) != 0) {
		return 1; /* Section invalid */
	}

	/* data[3] - reserved */

	header->table_id_extension = (data[4] << 8) | data[5];

	header->flags2 = data[6];

	crc_offset = length - 4 - 1;    /* 4 bytes */

	/* skip to end, read last 4 bytes and store in crc */

	header->crc = (data[crc_offset] << 24) | (data[crc_offset+1] << 16) |
					(data[crc_offset+2] << 8) | (data[crc_offset+3]);

	return 0;
}

int
dsmcc_process_msg_header(struct dsmcc_section *section, unsigned char *data) {
	struct dsmcc_message_header *header = &section->hdr.info;

	header->protocol = data[0];
	if(header->protocol != 0x11) {
		return 1;
	}
	header->type = data[1];
	if(header->type != 0x03) {
		return 1;
	}
	header->message_id = (data[2] << 8) | data[3];
	header->transaction_id = (data[4] << 24) | (data[5] << 16) |
                                (data[6] << 8) | data[7];

	/* data[8] - reserved */
	/* data[9] - adapationlength 0x00 */

	header->message_len = (data[10] << 8) | data[11];
	if(header->message_len > 4076) { /* Beyond valid length */
		return 1;
	}
	
#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("Protocol: 0x%02X\n", header->protocol);
	dsmcc_log("Type: 0x%02X\n", header->type);
	dsmcc_log("Message ID: 0x%04X\n", header->message_id);
	dsmcc_log("Transaction ID: 0x%08X\n", header->transaction_id);
	dsmcc_log("Message length: %d\n", header->message_len);
#endif

	return 0;
}

int
dsmcc_process_section_gateway(dsmcc_ctx *ctx, unsigned char *data) {
	int off = 0, ret;
	struct obj_carousel *car;

	car = ctx->active_carousel;
	if(car->gate != NULL) { /* TODO check gate version not changed */
		dsmcc_log("[libdsmcc] We already have gateway\n");
		return 0;	/* We already have gateway */
	}

	dsmcc_log("[libdsmcc] Setting gateway\n");
	car->gate = (struct dsmcc_dsi *)calloc(sizeof(unsigned char), sizeof(struct dsmcc_dsi));
#ifdef DSMCC_MALLOC_PRINT_DEBUG
	dsmcc_log("[MEMINFO] calloc -> cari->gateway len : %d", sizeof(struct dsmcc_dsi));
#endif

	/* 0-19 Server id = 20 * 0xFF */
	/* 20,21 compatibilydescriptorlength = 0x0000 */

	off = 22;
	car->gate->data_len = (data[off] << 8) | data[off+1];

	off+=2;

	/* does not even exist ?
		gate->num_groups = (data[off] << 8) | data[off+1];
		off+=2;
		dsmcc_log("Num. Groups: %d\n", gate->num_groups);
	*/

	/* TODO - process groups ( if ever exist ? ) */

#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("data length: %d\n", car->gate->data_len);
	dsmcc_log("Processing BiopBody...\n");
#endif
	ret = dsmcc_biop_process_ior(&car->gate->profile, data+DSMCC_BIOP_OFFSET);
	if(ret > 0) { off += ret; } else { /* TODO error */ }

	/* Set carousel id if not already given in data_broadcast_id_descriptor
		(only teletext doesnt bother with this ) */

	if(car->id == 0) { 	/* TODO is carousel id 0 ever valid ? */
		car->id = car->gate->profile.body.full.obj_loc.carousel_id;
	}

	dsmcc_log("[libdsmcc] Gateway Module: (%d) on Carousel ID: (0x%04X)\n", car->gate->profile.body.full.obj_loc.module_id, car->id);

	/* skip taps and context */
	off+=2;

	/* TODO process descriptors */
	car->gate->user_data_len = data[off++];
	if(car->gate->user_data_len > 0) {
		car->gate->user_data = (unsigned char *)calloc(sizeof(unsigned char), car->gate->data_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> car->gate->user_data len : %d", car->gate->data_len);
#endif
		memcpy(car->gate->user_data, data+off, car->gate->data_len);
	}

#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("BiopBody - data length %ld\n", 
			car->gate->profile.body.full.data_len);
	dsmcc_log("BiopBody - Lite Components %d\n", 
			car->gate->profile.body.full.lite_components_count);
#endif

	dsmcc_gateway_free(car->gate);
	return 0;
}

int
dsmcc_process_section_info(dsmcc_ctx *ctx, struct dsmcc_section *section, unsigned char *data) {
	struct dsmcc_dii *dii = &section->msg.dii;
	struct obj_carousel *car = NULL;
	int off=0, i, ret;

	car = ctx->active_carousel;

	dii->download_id = (data[0] << 24)|(data[1] << 16)|(data[2] << 8)|(data[3]) ;
	off+=4;

	dii->block_size = data[off] << 8 | data[off+1];
	off+=2;

	off+=6; /* not used fields */

	dii->tc_download_scenario = (data[off] << 24)|(data[off+1] << 16)|(data[off+2] << 8)|data[off+3];
	off+=4;

	/* skip unused compatibility descriptor len */
	off+=2;

	dii->number_modules = (data[off] << 8) | data[off+1];
	off+=2;

#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("Info -> Download ID = 0x%04X\n", dii->download_id);
	dsmcc_log("Info -> Block Size = %d\n", dii->block_size);
	dsmcc_log("Info -> tc download scenario = 0x%04X\n", dii->tc_download_scenario);
	dsmcc_log("Info -> number modules = %d\n",dii->number_modules);
#endif

	dii->modules = (struct dsmcc_module_info*)calloc(sizeof(unsigned char), sizeof(struct dsmcc_module_info)*dii->number_modules);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
	dsmcc_log("[MEMINFO] calloc -> dii->modules len : %d", sizeof(struct dsmcc_module_info)*dii->number_modules);
#endif

	for(i = 0; i < dii->number_modules; i++) {
		dii->modules[i].module_id = (data[off] << 8)|data[off+1];
		off+=2;

		dii->modules[i].module_size = (data[off] << 24)|(data[off+1] << 16)|(data[off+2] << 8)|data[off+3];
		off+=4;
		
		dii->modules[i].module_version = data[off++];
		dii->modules[i].module_info_len = data[off++];
		
#ifdef DSMCC_PRINT_DEBUG
		dsmcc_log("Module Info -> Module ID      = 0x%04X (%d)\n",  dii->modules[i].module_id, dii->modules[i].module_id);
		dsmcc_log("Module Info -> Module Size    = 0x%08X (%ld)\n", dii->modules[i].module_size, dii->modules[i].module_size);
		dsmcc_log("Module Info -> Module Version = %d\n", dii->modules[i].module_version);
#endif
		ret = dsmcc_biop_process_module_info(&dii->modules[i].modinfo, data+off);

		if(ret > 0) { off += ret; } else { /* TODO error */ }
	}

	dii->private_data_len = (data[off] << 8) | data[off+1];
	
#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("Info -> Private data length = %d\n",	dii->private_data_len);
#endif

	/* UKProfile - ignored
	dii->private_data = (char *)calloc(sizeof(unsigned char), dii->private_data_len);
	memcpy(dii->private_data, data+off, dii->private_data_len);
    */

	/* check module_version updated */
	dsmcc_check_module_version_updated(section, car);

	/* add module info within this function */
	dsmcc_add_module_info(section, car);

	/* Free most of the memory up... all that effort for nothing */
	for(i = 0; i < dii->number_modules; i++) {
		if(dii->modules[i].modinfo.tap.selector_data){
			free(dii->modules[i].modinfo.tap.selector_data);
			dii->modules[i].modinfo.tap.selector_data = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> dii->modules[%d].modinfo.tap.selector_data", i);
#endif
		}
	}

	free(dii->modules);	/* TODO clean up properly... done? */
	dii->modules = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
	dsmcc_log("[MEMINFO] free   -> dii->modules");
#endif
	return 0;
}

void
dsmcc_process_section_indication(dsmcc_ctx *ctx, unsigned char *data, int length) {
	struct dsmcc_section section;
	int ret;

	ret = dsmcc_process_section_header(&section, data+DSMCC_SECTION_OFFSET, length);

	if(ret != 0) {
		dsmcc_log("Indication Section Header error");
		return;
	}

	ret = dsmcc_process_msg_header(&section, data+DSMCC_MSGHDR_OFFSET);

	if(ret != 0) {
		dsmcc_log("Indication Msg Header error");
		return;
	}

	if(section.hdr.info.message_id == DSMCC_MESSAGE_DSI) {
		dsmcc_log("[libdsmcc] Server Gateway\n"); 
		dsmcc_process_section_gateway(ctx, data+DSMCC_DSI_OFFSET);
	} else if(section.hdr.info.message_id == DSMCC_MESSAGE_DII) {
		dsmcc_log("[libdsmcc] Module Information\n"); 
		dsmcc_process_section_info(ctx, &section, data+DSMCC_DII_OFFSET);
		int ret = dsmcc_check_download_done(ctx);
		if(ret) {
			int cached = 1;
			dsmcc_log("[libdsmcc] DSMCC Download aleady done \n");
			dsmcc_handle_download_done(ctx, cached);			
			return;
		}
		ctx->done_callback(DSMCC_SECTION_INDICATION, ctx->user_param);	
	} else {
		/* Error */
	}
}

void
dsmcc_check_module_version_updated(struct dsmcc_section *section, struct obj_carousel *car) {
	int i;
	struct cache_module_data *cachep, *cachepnext;
	struct dsmcc_dii *dii = &section->msg.dii;

	/* loop through modules and add to cache list if no module with
	 * same id or a different version. */
	cachep = car->cache;
	for(i = 0; i < dii->number_modules; i++) {
		for(;cachep!=NULL;cachep=cachep->next) {
			if(cachep->carousel_id == dii->download_id &&
				cachep->module_id == dii->modules[i].module_id) {
				if(cachep->version != dii->modules[i].module_version){
					dsmcc_log("[libdsmcc] Updated Module [%d] -> Existing Version(%d), Update Version(%d)\n", dii->modules[i].module_id, 
								cachep->version, dii->modules[i].module_version); 

					// Version Updated => File deletion and cache initialization
					/* Drop old data */
					char dirbuf[256];
					char dataPath[128];
					get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
					sprintf(dirbuf, "rm -r %s%s/%d", dataPath, DSMCC_ROOTPATH, car->application.dsmccPid);
					dsmcc_log("[libdsmcc] %s systemcall\n", dirbuf);					
					system(dirbuf);

					int buf_len = sprintf(dirbuf, "%d", car->application.dsmccPid);
					dirbuf[buf_len]='\0';
					
					/* Free cache_module_data info */
					cachep = car->cache;
					while(cachep!=NULL) {
						cachepnext = cachep->next;
						dsmcc_cache_module_data_free(cachep);
						cachep = cachepnext;
					}
					car->cache = NULL;

					/* Free filecache info */
					dsmcc_cache_free(car->filecache);

					/* filecache info initialize */
					dsmcc_cache_init(car->filecache, dirbuf);
					car->flag = APP_UPDATE_DOWN;
					return;

				}
			}
		}
	}
}


void
dsmcc_add_module_info(struct dsmcc_section *section, struct obj_carousel *car) {
	int i, num_blocks, found;
	struct cache_module_data *cachep;
	struct dsmcc_dii *dii = &section->msg.dii;

	cachep = car->cache;
	for(i = 0; i < dii->number_modules; i++) {
		found = 0;
		for(;cachep!=NULL;cachep=cachep->next) {
			if(cachep->carousel_id == dii->download_id &&
				cachep->module_id == dii->modules[i].module_id) {

				/* already known */
				if(cachep->version == dii->modules[i].module_version){
					dsmcc_log("[libdsmcc] AddModuleInfo -> Already Known Module [%d]\n", dii->modules[i].module_id);
					found =  1;
					break;
				} 
			}
		}

		if(found == 0) {
			if(car->cache != NULL) {
				for(cachep=car->cache;cachep->next!=NULL;cachep=cachep->next) {;}
				cachep->next = (struct cache_module_data *)calloc(sizeof(unsigned char), sizeof(struct cache_module_data));
#ifdef DSMCC_MALLOC_PRINT_DEBUG
				dsmcc_log("[MEMINFO] calloc -> cachep->next len : %d", sizeof(struct cache_module_data));
#endif
				cachep->next->prev = cachep;
				cachep = cachep->next;
			} else {
				car->cache = (struct cache_module_data *)calloc(sizeof(unsigned char), sizeof(struct cache_module_data));
#ifdef DSMCC_MALLOC_PRINT_DEBUG
				dsmcc_log("[MEMINFO] calloc -> cache len : %d", sizeof(struct cache_module_data));
#endif
				cachep = car->cache;
				cachep->prev = NULL;
			}

			cachep->carousel_id = dii->download_id;
			cachep->module_id = dii->modules[i].module_id;
			cachep->version = dii->modules[i].module_version;
			cachep->size = dii->modules[i].module_size;
			cachep->curp = cachep->block_num = 0;
			num_blocks = cachep->size / dii->block_size;

			if((cachep->size % dii->block_size) != 0)
				num_blocks++;
			cachep->bstatus=(char*)calloc(sizeof(char), (num_blocks/8)+1);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] calloc -> cachep->bstatus len : %d", (num_blocks/8)+1);
#endif
			cachep->block_num = num_blocks;

			dsmcc_log("[libdsmcc] Saving Info for module %d => Total Blocks:%d\n", dii->modules[i].module_id, cachep->block_num); 

/*
			dsmcc_log("Allocated %d bytes to store status for module %d",
					(num_blocks/8)+1, cachep->module_id);
 */               
			cachep->data = NULL;
			cachep->next = NULL;
			cachep->blocks = NULL;
			cachep->tag = dii->modules[i].modinfo.tap.assoc_tag;

			/* Steal the descriptors  TODO this is very bad... */
			cachep->descriptors = dii->modules[i].modinfo.descriptors;
			dii->modules[i].modinfo.descriptors = NULL;
			cachep->cached = 0;
		}
	}
}

int
dsmcc_process_data_header(struct dsmcc_section *section, unsigned char *data) {
	struct dsmcc_data_header *hdr = &section->hdr.data;

	hdr->protocol = data[0];
	hdr->type = data[1];
	hdr->message_id = (data[2] << 8) | data[3];
	hdr->download_id = (data[4]<<24)|(data[5]<<16)|(data[6]<<8)|data[7];
	
	/* skip reserved byte */
	hdr->adaptation_len = data[9];
	hdr->message_len = (data[10] << 8) | data[11];
	
	/* TODO adapationHeader ?? */
#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("data -> Header - > Protocol 0x%02X\n", hdr->protocol);
	dsmcc_log("data -> Header - > Type 0x%02X\n", hdr->type);
	dsmcc_log("data -> Header - > MessageID 0x%04X\n",hdr->message_id);
	dsmcc_log("data -> Header - > DownloadID 0x%08X\n", hdr->download_id);
	dsmcc_log("data -> Header - > Adaptation Len %d\n", hdr->adaptation_len);
	dsmcc_log("data -> Header - > Message Len %d\n", hdr->message_len);
#endif

	return 0;
}

int
dsmcc_process_section_block(dsmcc_ctx *ctx, struct dsmcc_section *section, unsigned char *data) {
	struct dsmcc_ddb *ddb = &section->msg.ddb;

	ddb->module_id = (data[0] << 8) | data[1];
	ddb->module_version = data[2];

	/* skip reserved byte */

	ddb->block_number = (data[4] << 8) | data[5];
	ddb->len = section->hdr.data.message_len - 6;
	ddb->next = NULL;	/* Not used here, used to link all data blocks
				   in order in AddModuledata. Hmmm. */
	ddb->blockdata = NULL;

#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("data -> Block - > Module ID 0x%04X (%d)\n", ddb->module_id, ddb->module_id);
	dsmcc_log("data -> Block - > Module Version %u\n", ddb->module_version);
	dsmcc_log("data -> Block - > Block Num %u\n",ddb->block_number);
#endif
	dsmcc_log("[libdsmcc] Data Block Module ID: (%d), Pos: (%d), Version: (%d)\n", 
				ddb->module_id, ddb->block_number, ddb->module_version);

	dsmcc_add_module_data(ctx, section, data+6);
	return 0;
}


void 
dsmcc_process_section_data(dsmcc_ctx *ctx, unsigned char * data, unsigned int data_size) {
	struct dsmcc_section *section;
	section = (struct dsmcc_section *)calloc(sizeof(unsigned char), sizeof(struct dsmcc_section));
#ifdef DSMCC_MALLOC_PRINT_DEBUG
	dsmcc_log("[MEMINFO] calloc -> section len : %d", sizeof(struct dsmcc_section));
#endif

	dsmcc_process_section_header(section, data+DSMCC_SECTION_OFFSET, data_size);

	dsmcc_process_data_header(section, data+DSMCC_DATAHDR_OFFSET);

	dsmcc_process_section_block(ctx, section, data+DSMCC_DDB_OFFSET);

	free(section);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
	dsmcc_log("[MEMINFO] free   -> section");
#endif
	
}

void 
dsmcc_add_module_data(dsmcc_ctx *ctx, struct dsmcc_section *section, unsigned char *data) {
	int i, ret, found = 0;
	unsigned char *block_data = NULL;
	unsigned long data_len = 0;
	struct cache_module_data *cachep = NULL;
	struct descriptor *desc = NULL, *last;
	struct dsmcc_ddb *lb, *pb, *nb, *ddb = &section->msg.ddb;
	struct obj_carousel *car;

	i = ret = 0;

	car = ctx->active_carousel;

//	dsmcc_log("[libdsmcc] data block on carousel_id: 0x%04X , download_id: 0x%04X\n", car->id, section->hdr.data.download_id);

	cachep = car->cache;
	for(; cachep != NULL; cachep = cachep->next) {
		if(cachep->carousel_id == section->hdr.data.download_id && 
			cachep->module_id == ddb->module_id) 
		{
			found = 1;
			dsmcc_log("Found linking module (%d)...\n",	ddb->module_id);
			break;
		}
	}

	if(found == 0) { /* Not found module info */
		dsmcc_log("Not found module info (%d)...\n", ddb->module_id);
		return; 
	}

	if(cachep->version == ddb->module_version) {
		if(cachep->cached) {
			dsmcc_log("[libdsmcc] Cached complete module (%d) already \n", cachep->module_id);
			return;/* Already got it */
		} else {
			/* Check if we have this block already or not. If not append
			 * to list
			 */
			if(BLOCK_GOT(cachep->bstatus, ddb->block_number) == 0) {
				if((cachep->blocks==NULL)||(cachep->blocks->block_number>ddb->block_number)) {
					nb=cachep->blocks;	/* NULL or previous first in list */
					cachep->blocks=(struct dsmcc_ddb*)calloc(sizeof(unsigned char), sizeof(struct dsmcc_ddb));
#ifdef DSMCC_MALLOC_PRINT_DEBUG
					dsmcc_log("[MEMINFO] calloc -> cachep->blocks len : %d", sizeof(struct dsmcc_ddb));
#endif
					lb=cachep->blocks;
				} else {
					for(pb=lb=cachep->blocks;(lb!=NULL) && (lb->block_number < ddb->block_number);pb=lb,lb=lb->next) {;}
					nb = pb->next;
					pb->next = (struct dsmcc_ddb*)calloc(sizeof(unsigned char), sizeof(struct dsmcc_ddb));
#ifdef DSMCC_MALLOC_PRINT_DEBUG
					dsmcc_log("[MEMINFO] calloc -> cachep->blocks->next len : %d", sizeof(struct dsmcc_ddb));
#endif
					lb=pb->next;
				}
				dsmcc_log("[libdsmcc] this block (%d)/(%d) for module (%d) received \n", ddb->block_number, cachep->block_num, ddb->module_id);
				lb->module_id = ddb->module_id;
				lb->module_version = ddb->module_version;
				lb->block_number = ddb->block_number;
				lb->blockdata = (unsigned char*)calloc(sizeof(unsigned char), ddb->len); 
#ifdef DSMCC_MALLOC_PRINT_DEBUG
				dsmcc_log("[MEMINFO] calloc -> cachep->blocks->blockdata[%d/%d] len : %d", ddb->block_number, cachep->block_num, ddb->len);
#endif
				memcpy(lb->blockdata, data, ddb->len);
				lb->len = ddb->len;
				cachep->curp += ddb->len;
				lb->next = nb;
				BLOCK_SET(cachep->bstatus, ddb->block_number);
			}
			else{
				dsmcc_log("[libdsmcc] this block (%d)/(%d) for module (%d) already received \n", ddb->block_number, cachep->block_num, ddb->module_id);
			}
		}

		dsmcc_log("[libdsmcc] Module: (%d), Current Size: (%d), Total Size: (%d)\n", cachep->module_id, cachep->curp, cachep->size);

		if(cachep->curp >= cachep->size) {
			dsmcc_log("[libdsmcc] Reconstructing module: (%d) from Total blocks: (%d)\n", cachep->module_id, cachep->block_num);

			/* Re-assemble the blocks into the complete module */
			cachep->data = (unsigned char*)calloc(sizeof(unsigned char), cachep->size);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] calloc -> cachep->data[%d] len : %d", cachep->module_id, cachep->size);
#endif
			cachep->curp = 0;
			pb=lb=cachep->blocks;
			int count = 0;
			while(lb!=NULL) {
				memcpy(cachep->data+cachep->curp,lb->blockdata,lb->len);
				cachep->curp += lb->len;

				pb=lb; lb=lb->next;

				if(pb->blockdata){
					free(pb->blockdata);
					pb->blockdata = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
					dsmcc_log("[MEMINFO] free   -> cachep->blocks->blockdata[%d]", count);
#endif
				}
				free(pb);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
				dsmcc_log("[MEMINFO] free   -> cachep->blocks[%d]", count);
#endif
				count++;
			}

			cachep->blocks = NULL;

			/* Uncompress.... TODO - scan for compressed descriptor */
			for(desc=cachep->descriptors;desc !=NULL; desc=desc->next) {
				if(desc && (desc->tag == TS_CAROUSEL_DT_Compressed)) { break; }
			}
			if(desc != NULL) {
				dsmcc_log("Uncompressing...(%lu bytes compressed - %lu bytes memory", cachep->curp, desc->data.compressed.original_size);

				data_len=desc->data.compressed.original_size+1;
				block_data = (unsigned char *)calloc(sizeof(unsigned char), data_len+1);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
				dsmcc_log("[MEMINFO] calloc -> block_data len : %d", data_len+1);
#endif
				dsmcc_log("Compress data memory %p - %p (%ld bytes)", cachep->data, cachep->data+cachep->size, cachep->size);
				dsmcc_log("Uncompress data memory %p - %p (%ld bytes)", block_data, block_data+data_len, data_len);

				dsmcc_log("(set %lu ", data_len);
				ret = uncompress(block_data, &data_len, cachep->data,cachep->size);
				dsmcc_log("expected %lu real %lu ret %d)", cachep->size, data_len, ret);
				if(ret == Z_DATA_ERROR) {
					dsmcc_log("[libdsmcc] compression error - invalid data, skipping\n");
					if(block_data) { 
						free(block_data);
						block_data = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
						dsmcc_log("[MEMINFO] free	-> block_data");
#endif
					}
					if(cachep->data) { 
						free(cachep->data); 
#ifdef DSMCC_MALLOC_PRINT_DEBUG
						dsmcc_log("[MEMINFO] free	-> cachep->data[%d]", cachep->module_id);
#endif
						cachep->data = NULL; 
					}
					cachep->curp = 0;					
					return;
				} else if(ret == Z_BUF_ERROR) {
					dsmcc_log("[libdsmcc] compression error - buffer error, skipping\n");
					if(block_data) { 
						free(block_data);
						block_data = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
						dsmcc_log("[MEMINFO] free	-> block_data");
#endif
					}
					if(cachep->data) { 
						free(cachep->data); 
#ifdef DSMCC_MALLOC_PRINT_DEBUG
						dsmcc_log("[MEMINFO] free	-> cachep->data[%d]", cachep->module_id);
#endif
						cachep->data = NULL; 
					}
					cachep->curp = 0;					
					return;
				} else if(ret == Z_MEM_ERROR) {
					dsmcc_log("[libdsmcc] compression error - out of mem, skipping\n");
					if(block_data) { 
						free(block_data);
						block_data = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
						dsmcc_log("[MEMINFO] free	-> block_data");
#endif
					}
					if(cachep->data) { 
						free(cachep->data); 
#ifdef DSMCC_MALLOC_PRINT_DEBUG
						dsmcc_log("[MEMINFO] free	-> cachep->data[%d]", cachep->module_id);
#endif
						cachep->data = NULL; 
					}

					cachep->curp = 0;					
					return;
				}

				if(cachep->data) { 
					free(cachep->data); 
#ifdef DSMCC_MALLOC_PRINT_DEBUG
					dsmcc_log("[MEMINFO] free	-> cachep->data[%d]", cachep->module_id);
#endif
					cachep->data = NULL; 
				}

				cachep->data = block_data;
				dsmcc_log("[libdsmcc] Processing data\n");

				// Return list of streams that directory needs
				dsmcc_biop_process_data(car->filecache, cachep);
				cachep->cached = 1;

				if(cachep->data) { 
					free(cachep->data); 
#ifdef DSMCC_MALLOC_PRINT_DEBUG
					dsmcc_log("[MEMINFO] free	-> cachep->data[%d]", cachep->module_id);
#endif
					cachep->data = NULL; 
				}
				if(cachep->bstatus) { 
					free(cachep->bstatus); 
#ifdef DSMCC_MALLOC_PRINT_DEBUG
					dsmcc_log("[MEMINFO] free	-> cachep->bstatus[%d]", cachep->module_id);
#endif
					cachep->bstatus = NULL; 
				}
				if(cachep->descriptors) {
					desc = cachep->descriptors;
					while(desc != NULL) {
						last = desc->next;
						dsmcc_desc_free(desc);
						desc = last;
					}
					cachep->descriptors = NULL;
				}

				ret = dsmcc_check_download_done(ctx);
				if(ret) {
					dsmcc_log("[libdsmcc] DSMCC Download done \n");
					dsmcc_application *application = &car->application;
					if(application->appType == DSMCC_MUSIC_APP_TYPE){
						memset(application->initialPath, 0x00, 32);
						memcpy(application->initialPath, car->filecache->files->filename, strlen(car->filecache->files->filename));
					}
					int cached = 0;
					dsmcc_handle_download_done(ctx, cached);					
					
					/* Free filecache info */
					dsmcc_cache_free(car->filecache);
				}
			} else {
				/* not compressed */
				dsmcc_log("[libdsmcc] Processing data (uncompressed)\n");
				// Return list of streams that directory needs
				dsmcc_biop_process_data(car->filecache, cachep);
				cachep->cached = 1;

				if(cachep->data) { 
					free(cachep->data); 
#ifdef DSMCC_MALLOC_PRINT_DEBUG
					dsmcc_log("[MEMINFO] free   -> cachep->data[%d]", cachep->module_id);
#endif
					cachep->data = NULL; 
				}
				if(cachep->bstatus) { 
					free(cachep->bstatus); 
#ifdef DSMCC_MALLOC_PRINT_DEBUG
					dsmcc_log("[MEMINFO] free   -> cachep->bstatus[%d]", cachep->module_id);
#endif
					cachep->bstatus = NULL; 
				}
				if(cachep->descriptors) {
					desc = cachep->descriptors;
					while(desc != NULL) {
						last = desc->next;
						dsmcc_desc_free(desc);
						desc = last;
					}
					cachep->descriptors = NULL;					
				}

				ret = dsmcc_check_download_done(ctx);
				if(ret) {
					dsmcc_log("[libdsmcc] DSMCC Download Done \n");
					dsmcc_application *application = &car->application;
					if(application->appType == DSMCC_MUSIC_APP_TYPE){
						memset(application->initialPath, 0x00, 32);
						memcpy(application->initialPath, car->filecache->files->filename, strlen(car->filecache->files->filename));
					}
					int cached = 0;
					dsmcc_handle_download_done(ctx, cached);

					/* Free filecache info */
					dsmcc_cache_free(car->filecache);
				}
			}
		}
	}
}

void 
dsmcc_process_section_desc(dsmcc_ctx *ctx, unsigned char *data, int length) {
	struct dsmcc_section section;
	int ret;
	ret = dsmcc_process_section_header(&section, data+DSMCC_SECTION_OFFSET, length);
	/* TODO */
}

void
dsmcc_process_section(dsmcc_ctx *ctx,unsigned char *data,int length) {
	unsigned long  crc32_decode;
	unsigned short section_len;

	/* Check CRC before trying to parse */
	section_len = ((data[1] & 0xF) << 8) | (data[2]) ;
	section_len += 3;/* 3 bytes before length count starts */

	crc32_decode = dsmcc_crc32(data, section_len);
#ifdef DSMCC_PRINT_DEBUG	
	dsmcc_log("length %d CRC - %lX \n",section_len, crc32_decode);
#endif
	if(crc32_decode != 0) {
		dsmcc_log("[libdsmcc] Corrupt CRC for section, dropping");
		dsmcc_log("[libdsmcc] Dropping corrupt section (Got %lX)", crc32_decode);		
		return;
	}

	switch(data[0]) {
		case DSMCC_SECTION_INDICATION:
			dsmcc_process_section_indication(ctx, data, length);
			break;
		case DSMCC_SECTION_DATA:
			dsmcc_process_section_data(ctx, data, length);
			break;
		case DSMCC_SECTION_DESCR:
			dsmcc_process_section_desc(ctx, data, length);
			break;
		default:
			break;
	}
}
