// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef DSMCC_CAROUSEL
#define DSMCC_CAROUSEL

#include "dsmcc.h"
void dsmcc_gateway_free(struct dsmcc_dsi *gateway);
void dsmcc_objcar_free(struct obj_carousel *);
#endif
