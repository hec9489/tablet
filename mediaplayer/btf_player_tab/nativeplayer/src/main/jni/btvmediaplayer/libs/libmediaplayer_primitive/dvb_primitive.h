// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef __dvb_primitive__
#define __dvb_primitive__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <linux/dvb/dmx.h>

#include <linux/dvb/video.h>
#include <linux/dvb/audio.h>

#include <linux/videodev2.h>

#include <btv_hal_mgr.h>

#include "DvbPlayerInfoDefine.h"

int  dvb_init_player(dvb_player_t *player, int idx_player, int jitter,int non_tunneled);

int  dvb_set_stream_info(dvb_player_t *player, dvb_locator_t *locator);

int dvb_tuner_lock(dvb_player_t *player, dvb_locator_t *locator);
int dvb_tuner_unlock(dvb_player_t *player, dvb_locator_t *locator);
int dvb_wait_tuner_unlock(dvb_player_t *player, dvb_locator_t *locator);

int dvb_device_open(dvb_player_t *player, AVP_MediaCallback callback, dvb_locator_t *locator_head);
int dvb_device_close(dvb_player_t *player);
int dvb_wait_for_video_event_fistframe(dvb_player_t *player);

/* Tuner functions */
int dvb_file_lock(dvb_player_t *player, dvb_locator_t *locator);
int dvb_file_unlock(dvb_player_t *player);
int dvb_wait_file_unlock(dvb_player_t *player);
int dvb_iptuner_lock(dvb_player_t *player, dvb_locator_t *locator);
int dvb_iptuner_unlock(dvb_player_t *player);
int dvb_wait_iptuner_unlock(dvb_player_t *player);

// stragon
int dvb_set_section_filter(dvb_player_t *player, unsigned short pid, unsigned char tid, AVP_SectionFilterCallback callback);
int dvb_clear_section_filter(dvb_player_t *player);

// dsmcc
int dvb_set_dsmcc_section_filter(dvb_player_t *player, unsigned short pid, unsigned char tid, AVP_SectionFilterCallback callback);
int dvb_clear_dsmcc_section_filter(dvb_player_t *player);


#ifdef STB_BMT_LEAVE_10MS_JOIN_TEST_BULLSHIT_NOSENSEINDOING
void multiview_unlock(dvb_player_t *player);
int dvb_iptuner_unlock_silent(dvb_player_t *player);
int dvb_wait_iptuner_unlock_silent(dvb_player_t *player);
int dvb_close_silent_byerror(dvb_player_t *player);
#endif

#ifdef USE_MLR
//int MLR_set_storage_path(char *path);
void set_first_start(int set);
int mlr_conf_file_operation(int set, char *value);
int MLR_set_path_and_Mac(void);
#endif

#define STDVBPLAYER_VERSION "v4.2"

#define pthread_yield() sched_yield()

#ifdef __cplusplus
}
#endif
#endif //__dvb_primitive__
