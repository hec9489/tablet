// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef ___RTP__________H_____
#define ___RTP__________H_____

#include "utils/RtpInfoDefine.h"

#ifdef __cplusplus
extern "C" {
#endif

void *iptv_media_player_recv_thread(void *ThreadParam);
void *iptv_media_player_feed_thread(void *ThreadParam);

#ifdef __cplusplus
}
#endif

#endif
