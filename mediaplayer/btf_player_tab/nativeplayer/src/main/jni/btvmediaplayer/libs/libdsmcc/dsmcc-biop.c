// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dsmcc-biop.h"
#include "dsmcc-descriptor.h"
#include "dsmcc-receiver.h"
#include "dsmcc-cache.h"
#include "dsmcc.h"

/*
struct biop_stream *
	dsmcc_biop_process_strream() { ; }

struct biop_streamevent *
	dsmcc_biop_process_event() { ; }

*/

int
dsmcc_biop_process_msg_hdr(struct biop_message *bm, struct cache_module_data *cachep) {
	struct biop_msg_header *hdr = &bm->hdr;
	unsigned char *data = cachep->data + cachep->curp;
	int off = 0;

	if(data == NULL) { 
		return -1; 
	}
	
	memset((void*)hdr, 0x00, sizeof(struct biop_msg_header));
#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("Hdr -> Checking magic\n");
#endif
	if(data[0] !='B' || data[1] !='I' || data[2] !='O' || data[3] !='P') {
		return -2;
	}
	off+=4;/* skip magic */

	hdr->version_major = data[off++];
	hdr->version_minor = data[off++];

	off+=2; /* skip byte order & message type */

	hdr->message_size  = (data[off] << 24) | (data[off+1] << 16) |
		     (data[off+2] << 8)  | data[off+3];
	off+=4;


	hdr->objkey_len = data[off++];
	if(hdr->objkey_len > 0){
		hdr->objkey = (char *)calloc(sizeof(unsigned char), hdr->objkey_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> hdr->objkey len : %d", hdr->objkey_len);
#endif
		memcpy(hdr->objkey, data+off, hdr->objkey_len);
		off+= hdr->objkey_len;
	}
	hdr->objkind_len = (data[off] << 24) | (data[off+1] << 16) |
			   (data[off+2] << 8) | data[off+3];
	off+=4;

	if(hdr->objkind_len > 0){
		hdr->objkind = (char *)calloc(sizeof(unsigned char), hdr->objkind_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> hdr->objkind len : %d", hdr->objkind_len);
#endif
		memcpy(hdr->objkind, data+off, hdr->objkind_len);
		off+= hdr->objkind_len;
	}
	
	hdr->objinfo_len = data[off] << 8 | data[off+1];
	off+=2;

	if(hdr->objinfo_len > 0){
		hdr->objinfo = (char *)calloc(sizeof(unsigned char), hdr->objinfo_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> hdr->objinfo len : %d", hdr->objinfo_len);
#endif
		memcpy(hdr->objinfo, data+off, hdr->objinfo_len);
		off+= hdr->objinfo_len;
	}	
	cachep->curp += off;

#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("Hdr -> Magic OK!\n");
	dsmcc_log("Hdr -> Version Major = %d\n", hdr->version_major);
	dsmcc_log("Hdr -> Version Minor = %d\n", hdr->version_minor);
	dsmcc_log("Hdr -> Message Size = %d\n", hdr->message_size);
	dsmcc_log("Hdr -> ObjKey Len = %d\n", hdr->objkey_len);
	dsmcc_log("Hdr -> ObjKey = %02X%02X%02X%02X\n", hdr->objkey[0], hdr->objkey[1], hdr->objkey[2], hdr->objkey[3]);
	dsmcc_log("Hdr -> ObjKind Len = %ld\n", hdr->objkind_len);
	dsmcc_log("Hdr -> ObjKind data = %s\n", hdr->objkind);
	dsmcc_log("Hdr -> ObjInfo Len = %d\n", hdr->objkey_len);
	dsmcc_log("Hdr -> ObjInfo = %02X%02X%02X%02X\n", hdr->objinfo[0], hdr->objinfo[1], hdr->objinfo[2], hdr->objinfo[3]);
#endif
	return 0;
}

int
dsmcc_biop_process_name_comp(struct biop_name_comp *comp, unsigned char *data) {
	int off = 0;

	comp->id_len = data[off++];
	if(comp->id_len > 0){	
		comp->id = (char *)calloc(sizeof(unsigned char), comp->id_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> binding->name.comps.id len : %d", comp->id_len);
#endif
		memcpy(comp->id, data+off, comp->id_len);
		off+=comp->id_len;
	}

	comp->kind_len = data[off++];
	if(comp->kind_len > 0){		
		comp->kind = (char *)calloc(sizeof(unsigned char), comp->kind_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> binding->name.comps.kind len : %d", comp->kind_len);
#endif
		memcpy(comp->kind, data+off, comp->kind_len);
		off+= comp->kind_len;
	}

#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("Dir -> Binding -> Name -> Comp -> Id Len = %d\n", comp->id_len);
	dsmcc_log("Dir -> Binding -> Name -> Comp -> Id = %s\n", comp->id);
	dsmcc_log("Dir -> Binding -> Name -> Comp -> Kind Len = %d\n", comp->kind_len);
	dsmcc_log("Dir -> Binding -> Name -> Comp -> Kind = %s\n", comp->kind);
#endif
	return off;
}

int
dsmcc_biop_process_name(struct biop_name *name, unsigned char *data) {
	int i, off = 0, ret;

	name->comp_count = data[0];
	off++;

#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("Dir -> Binding -> Name -> Comp Count = %d\n", name->comp_count);
#endif

	name->comps = (struct biop_name_comp *)
		calloc(sizeof(unsigned char), sizeof(struct biop_name_comp)*name->comp_count);

#ifdef DSMCC_MALLOC_PRINT_DEBUG
	dsmcc_log("[MEMINFO] calloc -> binding->name.comps comp_count:%d len : %d", name->comp_count, sizeof(struct biop_name_comp)*name->comp_count);
#endif

	for(i = 0; i < name->comp_count; i++) {
		ret = dsmcc_biop_process_name_comp(&name->comps[i], data+off);
		if(ret > 0) { off += ret; } else { /* TODO error */ }
	}

	return off;
}

int
dsmcc_biop_process_binding(struct biop_binding *bind, unsigned char *data) {
	int off = 0, ret;

	ret = dsmcc_biop_process_name(&bind->name, data);
	if(ret > 0) { off += ret; } else { /* TODO error */ }

	bind->binding_type = data[off++];

	ret = dsmcc_biop_process_ior(&bind->ior, data+off);
	if(ret > 0) { off += ret; } else { /*TODO error */ }

	bind->objinfo_len = (data[off] << 8) | data[off+1];
	off+=2;

	if(bind->objinfo_len > 0) {
		bind->objinfo = (char *)calloc(sizeof(unsigned char), bind->objinfo_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> bind->objinfo len : %d", bind->objinfo_len);
#endif
		memcpy(bind->objinfo, data+off, bind->objinfo_len);
		off+= bind->objinfo_len;		
	} else {
		bind->objinfo = NULL;
	}
	
#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("Dir -> Binding ->  Type = 0x%02X\n", bind->binding_type);
	dsmcc_log("Dir -> Binding ->  ObjInfo Len = %d\n", bind->objinfo_len);
	dsmcc_log("Dir -> Binding ->  ObjInfo = %s\n", bind->objinfo);
#endif

	return off;
}

int dsmcc_biop_process_srg(struct biop_message *bm,struct cache_module_data *cachep, struct cache *filecache) {
	unsigned int i;
	int off = 0, ret;
	unsigned char *data = cachep->data + cachep->curp;

	off++; /* skip service context count */

	bm->body.srg.msgbody_len = (data[off] << 24) | (data[off+1] << 16) |
				    (data[off+2] << 8) | data[off+3];
	off+=4;

	bm->body.srg.bindings_count = data[off] << 8 | data[off+1];
	off+=2;
	
#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("Gateway -> MsgBody Len = %ld\n", bm->body.srg.msgbody_len);
	dsmcc_log("Gateway -> Bindings Count = %d\n", bm->body.srg.bindings_count);
#endif

	for(i = 0; i < bm->body.srg.bindings_count; i++) {
		ret = dsmcc_biop_process_binding(&bm->body.srg.binding, data+off);
		if(ret > 0) { off += ret; } else { /* TODO error */ }
		if(!strcmp("dir", bm->body.srg.binding.name.comps[0].kind)) {
			dsmcc_cache_dir_info(filecache, 0,0,NULL,&bm->body.srg.binding);
		} else if(!strcmp("fil",bm->body.srg.binding.name.comps[0].kind)) {
			dsmcc_cache_file_info(filecache, 0,0,NULL,&bm->body.srg.binding);
		}
		dsmcc_biop_free_binding(&bm->body.srg.binding);
	}

	cachep->curp += off;

	return 0;
}

void
dsmcc_biop_free_binding(struct biop_binding *binding) {
	int i;

	for(i = 0; i < binding->name.comp_count; i++) {
		if(binding->name.comps[i].id_len > 0){
			free(binding->name.comps[i].id);
			binding->name.comps[i].id = NULL; binding->name.comps[i].id_len = 0;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> binding->name.comps[%d].id", i);
#endif
		}
		if(binding->name.comps[i].kind_len > 0){
			free(binding->name.comps[i].kind);
			binding->name.comps[i].kind = NULL; binding->name.comps[i].kind_len = 0;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> binding->name.comps[%d].kind", i);
#endif
		}
	}

	free(binding->name.comps);
	binding->name.comps = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
	dsmcc_log("[MEMINFO] free   -> binding->name.comps");
#endif
	
	if(binding->ior.type_id_len > 0){
		free(binding->ior.type_id);
		binding->ior.type_id = NULL; binding->ior.type_id_len = 0;		
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free   -> binding->ior.type_id");
#endif
	}

	if(binding->ior.body.full.obj_loc.objkey_len > 0){
		free(binding->ior.body.full.obj_loc.objkey);
		binding->ior.body.full.obj_loc.objkey = NULL; binding->ior.body.full.obj_loc.objkey_len = 0;	
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free   -> binding->ior.body.full.obj_loc.objkey");
#endif
	}

	if(binding->ior.body.full.dsm_conn.tap.selector_data){
		free(binding->ior.body.full.dsm_conn.tap.selector_data);
		binding->ior.body.full.dsm_conn.tap.selector_data = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free   -> binding->ior.body.full.dsm_conn.tap.selector_data");
#endif
	}

	if(binding->objinfo_len > 0){
		free(binding->objinfo);
		binding->objinfo = NULL; binding->objinfo_len = 0;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free   -> binding->objinfo");
#endif
	}

}

void dsmcc_biop_process_dir(struct biop_message *bm, struct cache_module_data *cachep, struct cache *filecache){
	unsigned int i;
	int off = 0, ret;
	unsigned char *data = cachep->data + cachep->curp;

	off++; /* skip service context count */

	bm->body.dir.msgbody_len = (data[off] << 24) | (data[off+1] << 16) |
				    (data[off+2] << 8) | data[off+3];
	off+=4;

	bm->body.dir.bindings_count = data[off] << 8 | data[off+1];
	off+=2;
	
#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("Dir -> MsgBody Len = %ld\n", bm->body.dir.msgbody_len);
	dsmcc_log("Dir -> Bindings Count = %d\n", bm->body.dir.bindings_count);
#endif

	for(i = 0; i < bm->body.dir.bindings_count; i++) {
		ret = dsmcc_biop_process_binding(&bm->body.dir.binding, data+off);
		if(ret > 0) { off+= ret; } else { /* TODO error */ }
		if(!strcmp("dir",bm->body.dir.binding.name.comps[0].kind)) {
			dsmcc_cache_dir_info(filecache, cachep->module_id, bm->hdr.objkey_len,
				bm->hdr.objkey, &bm->body.dir.binding);
		} else if(!strcmp("fil", bm->body.dir.binding.name.comps[0].kind)){
			dsmcc_cache_file_info(filecache, cachep->module_id, bm->hdr.objkey_len,
				bm->hdr.objkey, &bm->body.dir.binding);
		}
		dsmcc_biop_free_binding(&bm->body.dir.binding);
	}

	cachep->curp += off;

	filecache->num_dirs--;
}

void
dsmcc_biop_process_file(struct biop_message *bm,struct cache_module_data *cachep, struct cache *filecache){
	int off = 0;
	unsigned char *data = cachep->data + cachep->curp;

	/* skip service contect count */
	off++;

	bm->body.file.msgbody_len = (data[off] << 24) | (data[off+1] << 16) |
        			     (data[off+2] << 8) | data[off+3];
	off+=4;


	bm->body.file.content_len = (data[off] << 24) | (data[off+1] << 16) |
      				     (data[off+2] << 8)  | data[off+3];
	off+=4;

	cachep->curp += off;

#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("File -> MsgBody Len = %ld\n",bm->body.file.msgbody_len);
	dsmcc_log("File -> Content Len = %ld\n", bm->body.file.content_len);
#endif
	dsmcc_cache_file(filecache, bm, cachep);

	cachep->curp += bm->body.file.content_len;
}

void
dsmcc_biop_process_data(struct cache *filecache, struct cache_module_data *cachep) {
	struct biop_message bm;
	struct descriptor *desc;
	int ret;
	unsigned int len;

	for(desc = cachep->descriptors; desc != NULL; desc=desc->next) {
		if(desc->tag == TS_CAROUSEL_DT_Compressed) { break; }
	}

	if(desc != NULL) {
		len = desc->data.compressed.original_size;
	} else {
		len = cachep->size;
	}

	cachep->curp = 0;

	dsmcc_log("[dsmcc-biop] Module (%d) size (uncompressed) = %d\n", cachep->module_id, len);

	/* Replace off with cachep->curp.... */
	while(cachep->curp < len) {
		dsmcc_log("Current %ld / Full %d\n", cachep->curp, len);

		/* Parse header */
		ret = dsmcc_biop_process_msg_hdr(&bm, cachep);
		if(ret < 0) {
			dsmcc_log("[dsmcc-biop] Invalid biop header, dropping rest of module\n");
			/* not valid, skip rest of data */
			break;
		}

		/* Handle each message type */

		if(strcmp(bm.hdr.objkind, "fil") == 0) {
			dsmcc_log("[dsmcc-biop] Processing file\n");
			dsmcc_biop_process_file(&bm, cachep, filecache);
		} else if(strcmp(bm.hdr.objkind, "dir") == 0) {
			dsmcc_log("[dsmcc-biop] Processing directory\n");
			dsmcc_biop_process_dir(&bm, cachep, filecache);
		} else if(strcmp(bm.hdr.objkind, "srg") == 0) {
			dsmcc_log("[dsmcc-biop] Processing gateway\n");
			dsmcc_biop_process_srg(&bm, cachep, filecache);
		} else if(strcmp(bm.hdr.objkind, "str") == 0) {
			dsmcc_log("[dsmcc-biop] Processing stream (todo)\n");
			/*dsmcc_biop_process_stream(&bm, cachep); */
		} else if(strcmp(bm.hdr.objkind, "ste") == 0) {
			dsmcc_log("[dsmcc-biop] Processing events (todo)\n");
			/*dsmcc_biop_process_event(&bm, cachep); */
		} else {
			/* Error */
		}

		if(bm.hdr.objkey){
			free(bm.hdr.objkey);
			bm.hdr.objkey = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> hdr->objkey");
#endif
		}
		if(bm.hdr.objkind){
			free(bm.hdr.objkind);
			bm.hdr.objkind = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> hdr->objkind");
#endif
		}
		if(bm.hdr.objinfo){
			free(bm.hdr.objinfo);
			bm.hdr.objinfo = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> hdr->objinfo");
#endif
		}
	}

}

int
dsmcc_biop_process_module_info(struct biop_module_info *modinfo, unsigned char *data) {
	int off, ret;

	modinfo->mod_timeout = (data[0] << 24 ) | (data[1] << 16) |
       			       (data[2] << 8 )  | data[3];

	modinfo->block_timeout = (data[4] << 24) | (data[5] << 16) |
				 (data[6] << 8) | data[7];

	modinfo->min_blocktime = (data[8] << 24) | (data[9] << 16) |
				 (data[10] << 8) | data[11];

	modinfo->taps_count = data[12];

#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("Module Info -> Taps Count = %d\n",modinfo->taps_count);
//	dsmcc_log("Module Info -> Mod Timeout = 0x%04X\n", modinfo->mod_timeout);
//	dsmcc_log("Module Info -> BLock Timeout = 0x%04X\n", modinfo->block_timeout);
//	dsmcc_log("Module Info -> Min Block Timeout = 0x%04X\n", modinfo->min_blocktime);
#endif

	off = 13;
	/* only 1 allowed TODO - may not be first though ? */
	ret = dsmcc_biop_process_tap(&modinfo->tap, data+off);
	if(ret > 0) { off += ret; } else { /* TODO error */ }

	modinfo->userinfo_len = data[off++];

#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("Module Info -> UserInfo Len = %d\n", modinfo->userinfo_len);
#endif

	if(modinfo->userinfo_len > 0) {
		modinfo->descriptors = 	dsmcc_desc_process(data+off,modinfo->userinfo_len,&off);
	} else {
		modinfo->descriptors = NULL;
	}

	return off;

}

int
dsmcc_biop_process_tap(struct biop_tap *tap, unsigned char *data) {
	int off=0;

	tap->id = (data[0] << 8) | data[1];
	off+=2;
	tap->use = (data[off] << 8) | data[off+1];
	off+=2;
	tap->assoc_tag = (data[off] << 8) | data[off+1];
	off+=2;
	tap->selector_len = data[off++];
	tap->selector_data = NULL;	
#ifdef DSMCC_PRINT_DEBUG	
	dsmcc_log("Tap -> ID = 0x%04X\n",tap->id);
	dsmcc_log("Tap -> Use = 0x%04X\n",tap->use);
	dsmcc_log("Tap -> assoc_tag = 0x%04X\n",tap->assoc_tag);
	dsmcc_log("Tap -> selector_len = %d\n",tap->selector_len);
#endif
	if(tap->use == 0x0016){
		tap->selector_type = (data[off] << 8) | data[off+1];
		off+=2;		

		tap->transaction_id = (data[off] << 24 ) | (data[off+1] << 16) |
						   (data[off+2] << 8 )	| data[off+3];
		off+=4;

		tap->timeout = (data[off] << 24 ) | (data[off+1] << 16) |
						   (data[off+2] << 8 )	| data[off+3];
		off+=4;
#ifdef DSMCC_PRINT_DEBUG	
		dsmcc_log("Tap -> selector_type = 0x%04X\n", tap->selector_type);
		dsmcc_log("Tap -> transaction_id = 0x%08X\n", tap->transaction_id);
		dsmcc_log("Tap -> timeout = 0x%08X\n", tap->timeout);
#endif
	}else{
		if(tap->selector_len > 0){
			tap->selector_data = (char *)calloc(sizeof(unsigned char), tap->selector_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] calloc -> tap->selector_data len : %d", tap->selector_len);
#endif
			memcpy(tap->selector_data, data+off, tap->selector_len);
			off+=tap->selector_len;
		}
	}

	return off;
}

int
dsmcc_biop_process_binder(struct biop_dsm_connbinder *binder, unsigned char *data) {
	int off = 0, ret;

	binder->component_tag = (data[0] << 24) | (data[1] << 16) |
				(data[2] << 8)  | data[3];
	off+=4;

	binder->component_data_len = data[off++];
	binder->taps_count = data[off++];

#ifdef DSMCC_PRINT_DEBUG	
	dsmcc_log("Binder -> component_tag = 0x%04X\n", binder->component_tag);
	dsmcc_log("Binder -> component_data_len = %d\n", binder->component_data_len);
	dsmcc_log("Binder -> taps_count = %d\n",binder->taps_count);
#endif

	/* UKProfile - only first tap read */
	ret = dsmcc_biop_process_tap(&binder->tap, data+off);
	if(ret > 0) { off += ret; } else { /* TODO error */ }

	return off;
}

int
dsmcc_biop_process_object(struct biop_obj_location *loc, unsigned char *data) {
	int off = 0;

	loc->component_tag = (data[0]<<24)|(data[1]<<16)|
			     (data[2]<<8)|data[3];
	off+=4;
	loc->component_data_len = data[off++];
	loc->carousel_id = (data[off]<<24)|(data[off+1]<<16)|
			   (data[off+2]<<8)|data[off+3];
	off+=4;
	loc->module_id = (data[off] << 8) | data[off+1];

	off+=2;
	loc->version_major = data[off++];
	loc->version_minor = data[off++];

	loc->objkey_len = data[off++]; /* <= 4 */
	if(loc->objkey_len > 0){
		loc->objkey = (char *)calloc(sizeof(unsigned char), loc->objkey_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> binding->ior.body.full.obj_loc.objkey len : %d", loc->objkey_len);
#endif
		memcpy(loc->objkey, data+off, loc->objkey_len);
		off+=loc->objkey_len;
	}

#ifdef DSMCC_PRINT_DEBUG	
	dsmcc_log("Object -> component_tag = 0x%04X\n",loc->component_tag);
	dsmcc_log("Object -> component_data_len = %d\n", loc->component_data_len);
	dsmcc_log("Object -> carousel_id = 0x%04X\n",loc->carousel_id);
	dsmcc_log("Object -> module_id = %d\n",loc->module_id);
	dsmcc_log("Object -> version.major = %d\n", loc->version_major);
	dsmcc_log("Object -> Version.minor = %d\n", loc->version_minor);
	dsmcc_log("Object -> Key Length = %d\n",loc->objkey_len );
#endif
	return off;
}


int
dsmcc_biop_process_lite(struct biop_profile_lite *lite, unsigned char *data) { 
	dsmcc_log("BiopLite - Not Implemented Yet");
	return 0; 
}

int
dsmcc_biop_process_body(struct biop_profile_body *body, unsigned char *data) {
	int off = 0, ret;

	body->data_len = (data[off]<<24)|(data[off+1]<<16)|
	 		 (data[off+2]<<8)|data[off+3];
	off+=4;

	body->byte_order = data[off++];
	body->lite_components_count = data[off++];

#ifdef DSMCC_PRINT_DEBUG	
	dsmcc_log("Body -> data_len = %ld\n", body->data_len);
	dsmcc_log("Body -> profile_data_byte_order = 0x%02X\n", body->byte_order);
	dsmcc_log("Body -> lite_components_count = 0x%02X\n", body->lite_components_count);
#endif

	ret = dsmcc_biop_process_object(&body->obj_loc, data+off);
	if(ret > 0) { off += ret; } else { /* TODO error */ }

	ret = dsmcc_biop_process_binder(&body->dsm_conn, data+off);
	if(ret > 0) { off += ret; } else { /* TODO error */ }

	/* UKProfile - ignore anything else */

	return off;
}

int
dsmcc_biop_process_ior(struct biop_ior *ior, unsigned char *data) {
	int off = 0, ret;

	ior->type_id_len = (data[0]<<24)|(data[1]<<16)|(data[2]<<8)|(data[3]);
	off+=4;

	if(ior->type_id_len){
		ior->type_id = (char *)calloc(sizeof(unsigned char), ior->type_id_len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> binding->ior.type_id len : %d", ior->type_id_len);
#endif
		memcpy(ior->type_id, data+off, ior->type_id_len);
		off+=ior->type_id_len;
	}
	
	ior->tagged_profiles_count = (data[off]<<24)|(data[off+1]<<16)|
       				     (data[off+2]<<8)|(data[off+3]);
	off+=4;

	ior->profile_id_tag = (data[off]<<24)|(data[off+1]<<16)|
      			      (data[off+2]<<8)|data[off+3];
	off+=4;

#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("New BIOP IOR\n");
	dsmcc_log("type_id_len = %ld\n", ior->type_id_len);
	dsmcc_log("type_id = %s\n", ior->type_id);
	dsmcc_log("Tagged Profiles Count = %ld\n", ior->tagged_profiles_count);
	dsmcc_log("Profile Id Tag = 0x%04X\n", ior->profile_id_tag);
	dsmcc_log("Profile Id Tag last = 0x%02X\n", (ior->profile_id_tag & 0xFF));
#endif
	if((ior->profile_id_tag & 0xFF) == 0x06) {
		ret = dsmcc_biop_process_body(&ior->body.full, data+off);
		if(ret > 0) { off += ret; } else { /* TODO error */ }
	} else if((ior->profile_id_tag & 0xFF) == 0x05) {
		ret = dsmcc_biop_process_lite(&ior->body.lite, data+off);
		if(ret > 0) { off += ret; } else { /* TODO error */ }
	}

	/* UKProfile - receiver may ignore other profiles */

	return off;
}

