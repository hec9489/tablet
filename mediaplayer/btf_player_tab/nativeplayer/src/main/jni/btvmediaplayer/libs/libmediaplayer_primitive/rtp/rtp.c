// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#include "rtp.h"

#include <dbg.h>
#include "dvb_primitive.h"
#include <RtpUtils.h>

#define F_CHECK_RECEIVED_LENGTH
#ifdef F_CHECK_RECEIVED_LENGTH
#include  <systemproperty.h>
#endif
void recvIptvData(dvb_player_t *player, int nDevice, RtpInfo_t *pRtpInfo, int *pFlag_find_first_iframe, int *pFlag_first_socket_read);
void feedData(dvb_player_t *player, int nDevice, RtpInfo_t *pRtpInfo, long long  buffering_usec, long long  *pLast_injection_usec, bool enableDelay, int *pFlag_first_feed_data_wait);

struct timeval tv_now;
struct timeval tv_last_injection;

int getrtp2(dvb_player_t *player, RtpInfo_t *pRtpInfo, int fd, struct rtpheader *rh, char **data, int *lengthData, int nDevice)
{
    *data = pRtpInfo->bufferTemp;
    if(player->hMlrClient != NULL && player->mlrFunction != NULL) getRtp2Buffer(fd, rh, *data, lengthData, nDevice, player->mlrFunction, player);
    else getRtp2Buffer(fd, rh, *data, lengthData, nDevice, NULL, player);
    //getrtp2Buffer(player, fd, rh, *data, lengthData, nDevice);
    return 0;
}

void *iptv_media_player_recv_thread(void *ThreadParam) {
    logLogCatKey_stdvb("++ %s start!!!!! iptv_media_player_recv_thread", __FUNCTION__);
    dvb_player_t *player = (dvb_player_t*) ThreadParam;

    int nDevice = player->idx_player;

    RtpInfo_t *pRtpInfo = gRtpInfo[nDevice];

#ifdef MEASURE_ZAPPING
    int flag_first_socket_read = 1;
#endif
    int flag_find_first_iframe = 0; //iframe찾기를 하려면 1로 세팅..

    //set_thread_priority(2);
    set_thread_priority(0);

    logLogCatKey_stdvb("++[rtp.c][DEVICE %d] recv data ready...", nDevice);

    while (!player->TerminateTuner) {
#ifdef MEASURE_ZAPPING
        recvIptvData(player, nDevice, pRtpInfo, &flag_find_first_iframe, &flag_first_socket_read);
#else
        recvIptvData(player, nDevice, pRtpInfo, &flag_find_first_iframe, NULL);
#endif
    } //while (!player->TerminateTuner)

    logLogCatKey_stdvb("++ %s terminate!!!!! iptv_media_player_recv_thread", __FUNCTION__);
    return 0;
}

void recvIptvData(dvb_player_t *player, int nDevice, RtpInfo_t *pRtpInfo, int *pFlag_first_iframe, int *pFlag_first_socket_read) {
    int ret     = 0;
    fd_set fds;
    struct timeval tv;

    char *buf;
    struct rtpheader rh;
    int lengthData = 0;
    int total_lengthData = 0;
    int remainBufferSize = 0;

    int nQiSendSize = 0;
    char *nBufQiPtr = NULL;

#ifdef F_CHECK_RECEIVED_LENGTH
    static int lengthCheck = 0;
    static int total_len = 0;
    int enable_log = 0;
    struct timeval tv_start;
    enable_log = get_pkt_logging_enable();
    if((enable_log == 1) && (lengthCheck == 0))
    {
      gettimeofday(&tv_start, NULL);
      lengthCheck = 1;
    }
#endif

    FD_ZERO(&fds);

    FD_SET(pRtpInfo->socketIn, &fds);

    tv.tv_sec = 1;  //
    tv.tv_usec = 0;

    ret = select(pRtpInfo->socketIn + 1, &fds, NULL, NULL, &tv);

    if (ret > 0 && FD_ISSET(pRtpInfo->socketIn, &fds)) {
        //read from socket
        if (pFlag_first_socket_read != NULL && *pFlag_first_socket_read) {
            *pFlag_first_socket_read = 0;
            log_info("[zapping] First read from socket");
            logLogCatKey_stdvb("++[rtp.c][DEVICE %d] *************************************************", nDevice);
            logLogCatKey_stdvb("++[rtp.c][DEVICE %d] First read from socket", nDevice);
        }

        getrtp2(player, pRtpInfo, pRtpInfo->socketIn, &rh, &buf, &lengthData, nDevice);
#ifdef F_CHECK_RECEIVED_LENGTH
        if(lengthCheck == 1)
        {
          total_len += lengthData;
        }
#endif
        if ((total_lengthData + lengthData) < SIZE && lengthData > 0) {
			
            if (player->cbCASDescramble	&& lengthData > 0) {
				lengthData = player->cbCASDescramble((void *)buf, lengthData, player->idx_player);
				//logLogCatKey_stdvb("++[rtp.c][DEVICE %d] cbCASDescramble lengthData2 [%d]", nDevice, lengthData);
            }

            nQiSendSize = lengthData;
            nBufQiPtr = buf;
            if (lengthData < 0) {
                logLogCatKey_stdvb("++[rtp.c][DEVICE %d] error, cas packet size : %d", nDevice, lengthData);
                return;
            }

            if(nQiSendSize >= 188*7 && (nQiSendSize%(188*7))==0) {
                //qi_calculator_scan((const unsigned char*)&msg_buf[0], QI_TS_DATAGRAM_SIZE,1, channel_ip[0]);
                if (player->cbQiCalcScan) player->cbQiCalcScan((const unsigned char *) nBufQiPtr,
                                                               (unsigned int) nQiSendSize,
                                                               (unsigned int) (nQiSendSize / (188 * 7)),
                                                               (unsigned int) pRtpInfo->ipAddr);
            }

            if(player->hAdsClient != NULL && player->adsFunction != NULL)  player->adsFunction(player, buf, &lengthData);
            //else logLogCatKey_stdvb("++Device[%d] >> player->hAdsClient:%p, player->adsFunction:%p", nDevice, player->hAdsClient, player->hAdsClient, player->adsFunction);

            remainBufferSize = (int) (pRtpInfo->buf_max - pRtpInfo->buf_writeptr);
            if(remainBufferSize < lengthData) {
                memcpy((void *)pRtpInfo->buf_writeptr, (void *)buf, (size_t) remainBufferSize);
                pRtpInfo->buf_writeptr = (volatile char *) pRtpInfo->buf_min;
                total_lengthData += remainBufferSize;
                buf += remainBufferSize;
                lengthData -= remainBufferSize;
            }

            memcpy((void *)pRtpInfo->buf_writeptr, (void *)buf, (size_t) lengthData);
            pRtpInfo->buf_writeptr += lengthData;
            total_lengthData += lengthData;
#ifdef USE_PIX_ADS
            if(player->hAdsClient != NULL)
                PIX_ADS_NotifyNextBufferStatus(player->hAdsClient, SIZE, total_lengthData);
#endif

            //logLogCatKey_stdvb("++[rtp.c][DEVICE %d] size : %d, writeprt:%x, readprt:%x", nDevice, lengthData, pRtpInfo->buf_writeptr, pRtpInfo->buf_readptr);

            pRtpInfo->buf_readOneOrMoreData = 1; // always set true and it's cleared by other thread, 데이터 수신되었는지 확인
        } else if((total_lengthData + lengthData) >= SIZE) {
            logLogCatKey_stdvb("++[rtp.c]Device[%d] dropped received packets", nDevice);
            usleep(1000 * 20); //jung2604 : 리시브에 이상이 있을경우 확인하자
            pRtpInfo->buf_writeptr = pRtpInfo->buf_readptr = (volatile char *) pRtpInfo->buf_min;
#ifdef USE_BTV_HAL_MANAGER
            HalBtv_FlushTS(player->idx_player);
#else
            AVP_FlushTS(player->avpInfo.playerHandle);
#endif
        }
        //usleep(1);
    } else logLogCatKey_stdvb("++Device[%d] socket timeout" , nDevice);

#ifdef F_CHECK_RECEIVED_LENGTH
    struct timeval tv_end;
    if(lengthCheck == 1)
    {
        gettimeofday(&tv_end, NULL);
        if(tv_end.tv_sec != tv_start.tv_sec)
        {
          logLogCatKey_stdvb("++[rtp.c]Device[%d] total_len %d" , nDevice, total_len);
          lengthCheck = 0;
          total_len = 0;
        }
    }
#endif
}

void *iptv_media_player_feed_thread(void *ThreadParam) {
    logLogCatKey_stdvb("++ %s start!!!!! iptv_media_player_feed_thread", __FUNCTION__);
    dvb_player_t *player = (dvb_player_t*) ThreadParam;

    int nDevice = player->idx_player;

    RtpInfo_t *pRtpInfo = gRtpInfo[nDevice];

    long long  buffering_usec=0;
    long long  now_usec = 0;
    long long  last_injection_usec = 0;

    //set_thread_priority(2);
    set_thread_priority(0);

    pthread_t this_thread = pthread_self();
    if(pthread_setname_np(this_thread, "iptvfeedthread") != 0)
        logLogCatKey_stdvb("pthread_setname_np fail");

    // jung2604 : 첫 데이터 FEED 체크용..
    int flag_first_feed_data_wait = 1;

    struct timeval tv_now;
    gettimeofday(&tv_now,NULL);
    long long  start_usec =  ( (tv_now.tv_sec << 20) + tv_now.tv_usec);
    long long  end_usec = start_usec;

    last_injection_usec = end_usec + ((BUFFERING_DURATION) * 2);
#if 0
    bool isFirstIframe = false;
    if(!isFirstIframe) LOG("DEMUX_CheckIFrame start");
    while (!player->TerminateTuner && !isFirstIframe && player->demuxHandle != NULL) {
        if (!player->isAvpStart) { //jung2604 ; isAvpStart -> iptv에서 ST_ChangeChannel할때 close와 open이 뒤로 빠지면서 데이터를 feed해주는것을 늦춰야한다.
            usleep(1000 << 2);
            continue;
        }

        int total_lengthData = (int)(pRtpInfo->buf_writeptr - pRtpInfo->buf_readptr);
        int minBuffer = 0;
        if(pRtpInfo->buf_writeptr < pRtpInfo->buf_readptr) {
            total_lengthData = (int) ((pRtpInfo->buf_max - pRtpInfo->buf_readptr) + (pRtpInfo->buf_writeptr - pRtpInfo->buf_min));
            //total_lengthData = (int) (pRtpInfo->buf_max - pRtpInfo->buf_readptr);
        }

        if(total_lengthData >= 188*7) {
            for(int i = 0 ; i < total_lengthData / 188 ; i++) {
                DEMUX_CheckIFrame(player->demuxHandle, (char *) pRtpInfo->buf_readptr, 188, &isFirstIframe);
                if (isFirstIframe) {
                    LOG("DEMUX_CheckIFrame isFirstIframe=%d\n", isFirstIframe);
                    DEMUX_ClearAllPesFilter(player->demuxHandle);
                    break;
                }

                pRtpInfo->buf_readptr += (188);
                if(pRtpInfo->buf_readptr >= pRtpInfo->buf_max) {
                    pRtpInfo->buf_readptr = pRtpInfo->buf_min;
                }
            }
        }
        usleep(1000);

        gettimeofday(&tv_now,NULL);
        end_usec = ( (tv_now.tv_sec << 20) + tv_now.tv_usec);

        if(end_usec - last_injection_usec > 1000000) { //1 초이상 지연시 그냥 통과
            LOG("DEMUX_CheckIFrame pass");
            break;
        }
    }
#endif

    int logTimeSec = tv_now.tv_sec;
    int total_lengthData = 0;

    while (!player->TerminateTuner) {
		// iptv 재생시 화면 재생 속도 향상을 위해 dvb_device_open( 보다 dvb_tuner_lock을 먼저 실행시켜서 
		// iptv 데이터를 먼저 받아온다	   
		// SOC player가  create 될때까지 feeddate  하지않고 기다린다. 
        if(!player->isAvpStart) {
			usleep(1000 <<2);
            continue;
        }

        gettimeofday(&tv_now,NULL);
        end_usec = ( (tv_now.tv_sec << 20) + tv_now.tv_usec);

        buffering_usec = end_usec - last_injection_usec;

        if(logTimeSec != tv_now.tv_sec) {
            logTimeSec = tv_now.tv_sec;

            if(pRtpInfo->buf_writeptr < pRtpInfo->buf_readptr) {
                total_lengthData = (int) ((pRtpInfo->buf_max - pRtpInfo->buf_readptr) + (pRtpInfo->buf_writeptr - pRtpInfo->buf_min));
            } else total_lengthData = (int)(pRtpInfo->buf_writeptr - pRtpInfo->buf_readptr);
            logLogCatKey_stdvb("++ remain buffer size : %d", total_lengthData);
        }

        feedData(player, nDevice, pRtpInfo, buffering_usec, &last_injection_usec, true, &flag_first_feed_data_wait);

        start_usec = end_usec;
    } //while (!player->TerminateTuner)

    logLogCatKey_stdvb("++ %s terminate!!!!! iptv_media_player_feed_thread", __FUNCTION__);
    return 0;
}

void feedData(dvb_player_t *player, int nDevice, RtpInfo_t *pRtpInfo, long long buffering_usec, long long *pLast_injection_usec, bool enableDelay, int *pFlag_first_feed_data_wait) {
    int total_lengthData = (int)(pRtpInfo->buf_writeptr - pRtpInfo->buf_readptr);

    int written = 0;

    long long start_usec = 0;
    long long now_usec = 0;
    struct timeval tv_now;
    long sleepTime = 0;

    bool enableFastFeed = false;

    //jung2604 : 버퍼의 사이즈를 기준으로 버퍼링..
    int wantFeedSize = BUFFERING_SIZE;

    gettimeofday(&tv_now,NULL);
    start_usec = ( (tv_now.tv_sec << 20) + tv_now.tv_usec);

    int minBuffer = 0;
    if(pRtpInfo->buf_writeptr < pRtpInfo->buf_readptr) {
        total_lengthData = (int) ((pRtpInfo->buf_max - pRtpInfo->buf_readptr) + (pRtpInfo->buf_writeptr - pRtpInfo->buf_min));
        minBuffer = (int) (pRtpInfo->buf_max - pRtpInfo->buf_readptr);
    }

    if( (buffering_usec >= BUFFERING_DURATION && total_lengthData > wantFeedSize)) {
		
        if (total_lengthData > 0 && !player->TerminateTuner) {
            if(minBuffer > 0) {
#ifdef USE_BTV_HAL_MANAGER
                written = HalBtv_InjectTS(player->idx_player, (void *) pRtpInfo->buf_readptr, minBuffer);
#else
                written = AVP_InjectTS(player->avpInfo.playerHandle, (void *) pRtpInfo->buf_readptr, minBuffer);
#endif
                //logLogCatKey_stdvb( "++Device[%d] minBuffer : %d, total_lengthData : %d, writen : %d, feedTime : %d", nDevice, minBuffer, total_lengthData, written);
            } else {
                //readbuffer는 별도로 락을 걸지 않는다
#ifdef USE_BTV_HAL_MANAGER
                written = HalBtv_InjectTS(player->idx_player, (void *) pRtpInfo->buf_readptr, total_lengthData);
#else
                written = AVP_InjectTS(player->avpInfo.playerHandle, (void *) pRtpInfo->buf_readptr, total_lengthData);
#endif
            }
             {
                gettimeofday(&tv_now,NULL);
                now_usec = ( (tv_now.tv_sec << 20) + tv_now.tv_usec);

                    int gap = now_usec -  start_usec;
                    if(gap > 10000)
                        logLogCatKey_stdvb("++Device[%d] AVP_InjectTS feed delay gap(%d) > 10ms, written : %d, total_lengthData: %d\n", nDevice, gap/1000, written, total_lengthData);

             }

            if(written > 0) {
                if(player->demuxHandle != NULL) {
                    DEMUX_FeedData(player->demuxHandle, (char *) pRtpInfo->buf_readptr, written);
                     {
                        gettimeofday(&tv_now,NULL);
                        now_usec = ( (tv_now.tv_sec << 20) + tv_now.tv_usec);

                        int gap = now_usec -  start_usec;
                        if(gap > 10000)
                            logLogCatKey_stdvb("++Device[%d] DEMUX_FeedData feed delay gap(%d) > 10ms)\n", nDevice, gap/1000);

                     }
                }
            }

            //jung2604 : 첫 데이터 FEED 용
            if(pFlag_first_feed_data_wait != NULL && *pFlag_first_feed_data_wait > 0) {
                *pFlag_first_feed_data_wait = 0;
                logLogCatKey_stdvb( "++Device[%d] first feed data > total_lengthData : %d, written : %d ", nDevice, total_lengthData, written);

                { // TODO : jung2604 : 20190429 : zapping time check용..
                    if ( 0 == clock_gettime( CLOCK_REALTIME, &player->tTempTimeVal)) {
                        player->tempTimeMs = (player->tTempTimeVal.tv_sec * 1000) + (player->tTempTimeVal.tv_nsec / 1000000);
                        player->timeCheckFirstFeedingTime = player->tempTimeMs;
                        logLogCatKey_stdvb("**_** first data feeding start : %ld ,start ~ now : %ld ms ", player->tempTimeMs, player->tempTimeMs - player->timeCheckStartTime);
                    }
                }
            }

            if (written > 0) {
                if(minBuffer > 0 && minBuffer <= written) {
                    pRtpInfo->buf_readptr = (volatile char *) (pRtpInfo->buf_min + (written - minBuffer));
                } else pRtpInfo->buf_readptr += written;
                if(pRtpInfo->buf_max <= (const char *) pRtpInfo->buf_readptr) pRtpInfo->buf_readptr = (volatile char *) pRtpInfo->buf_min;
                //jung2604 : 적은 양을 feed하였다면.. sleep속도를 빠르게 조정한다.
                if(written < total_lengthData && written <= (int) (pRtpInfo->buf_writeptr - pRtpInfo->buf_readptr))  enableFastFeed = true;


                //logLogCatKey_stdvb("++Device[%d] written : %d, total : %d, enableFastFeed : %d", nDevice, written, total_lengthData, enableFastFeed);
            } else {
                if(nDevice == 0) {
                    //logLogCatKey_stdvb("++Device[%d] failed to write to dvr. need to write %d bytes. trying..., error code : %d, total : %d, feedTime : %d", nDevice, size, written, total_lengthData, mid_usec - start_usec);
                }
                //logLogCatKey_stdvb("++Device[%d] failed to write to dvr. need to write %d bytes. trying...\n", nDevice, size);
            }
        } //if (size > 0 && !player->TerminateTuner)
    }

    gettimeofday(&tv_now,NULL);
    now_usec = ( (tv_now.tv_sec << 20) + tv_now.tv_usec);

    //jung2604 : 적은 양을 feed하였다면.. sleep속도를 빠르게 조정한다.
    if(enableFastFeed) {
        sleepTime = -1; //100;
        *pLast_injection_usec = start_usec - BUFFERING_DURATION;
    } else {
        sleepTime = BUFFERING_DURATION - ((now_usec -  start_usec) - BUFFERING_DURATION_CORRECTION);
        *pLast_injection_usec = start_usec;
    }
    {
        int gap = now_usec -  start_usec;
        if(gap > BUFFERING_DURATION)
            logLogCatKey_stdvb("++Device[%d] feed delay gap(%d) > BUFFERING_DURATION(%d)\n", nDevice, gap, BUFFERING_DURATION);

    }

    if(enableDelay) {
        //logLogCatKey_stdvb( "++Device[%d] sleepTIme : %d", nDevice, sleepTime);
        if (sleepTime > 0 && sleepTime <= (BUFFERING_DURATION) * 2) usleep((useconds_t) sleepTime);
    }
}

/*}}}*/
