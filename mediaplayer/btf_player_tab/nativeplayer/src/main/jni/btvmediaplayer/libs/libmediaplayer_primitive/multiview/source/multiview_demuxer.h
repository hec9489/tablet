// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifdef __cplusplus
extern "C" {
#endif

#ifndef WORK_FFMPEG_DEMUX_H
#define WORK_FFMPEG_DEMUX_H

#include <android/log.h>
#include <stdbool.h>

#include <hal/multiView/pix_hevc_merge_export.h>
#include <hal/multiView/pix_type.h>

#include <libavformat/avio.h>
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>

#include "multiview_define.h"
#include "multiview_util.h"


typedef int (*READ_STREAM_CB)(void* opaque, uint8_t* buf, int buf_size);

typedef struct _ST_FFMPEG_DEMUX_CTX{
	int                 channelIdx;         // channelId
	AVFormatContext     *fmt_ctx;           // input context

	AVCodecContext      *video_dec_ctx;
	AVCodecContext      *audio_dec_ctx;

	AVIOContext         *avio_ctx;

	enum AVPixelFormat  pix_fmt;
	int                 width;
	int                 height;

	int                 video_stream_idx;
	int                 audio_stream_idx;

	int                 pid;
	int                 vid;
	int                 aid;

	int64_t             pts_iframe;     // key frame 이 되는 iframe 에 대한 pts
	int64_t             dts_iframe;     // key frame 이 되는 iframe 에 대한 dts

	int64_t             pts_mod;        // channel 0 을 기준으로 한 조절값
	int64_t             dts_mod;        // channel 0 을 기준으로 한 조절값

//	bool                isFindIFrame;

	READ_STREAM_CB      cb;

	ST_QUEUE_MESSAGE    *message_queue;     // Muxer를 위한 Audio
	AVThreadMessageQueue *video_queue;      // 모든 채널이 Ready 되기 전에 생성된 AVPacket을 queue에 보관하기 위한 용도.


}ST_FFMPEG_DEMUX_CTX;

int multiview_create_demuxer_stream(ST_FFMPEG_DEMUX_CTX** ctx, int channelIdx, int pid, int vid, int aid, void* opaque, READ_STREAM_CB cb, int* terminate);

int multiview_demux_stream(ST_FFMPEG_DEMUX_CTX* ctx, h_pt_merge_hevc pt_ctx, int* terminate);

int multiview_destroy_demuxer(ST_FFMPEG_DEMUX_CTX** ctx);

int multiview_demuxer_init();

int multiview_demuxer_final();

#ifdef __cplusplus
} //extern "C"
#endif

#endif //WORK_FFMPEG_DEMUX_H
