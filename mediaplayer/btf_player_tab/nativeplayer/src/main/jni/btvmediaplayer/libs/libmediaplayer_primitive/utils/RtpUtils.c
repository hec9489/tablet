// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#include "RtpUtils.h"

#include <dbg.h>
#include <sched.h>
#include <pthread.h>

#include <stdlib.h>

RtpInfo_t *gRtpInfo[6];

int getRtp2Buffer(int fd, struct rtpheader *rh, char *data, int *lengthData, int nDevice, MLR_FUNCTION mlrFunction, void* argument) {
    unsigned int intP;
    char *charP = (char *)&intP;
    int headerSize;

    *lengthData = recv(fd, data, 1590, 0);

    if (*lengthData == 0) exit(1);
    if (*lengthData < 0) {
        fprintf(stderr, "socket read error\n");
        printf("getrtp2 failed: - %s\n", strerror (errno));
        exit(2);
    }

    if (*lengthData < 12) {
        fprintf(stderr, "packet too small (%d) to be an rtp frame (>12bytes)\n", *lengthData);
        exit(3);
    }

    bool isMlrSuccess = false;
    if(nDevice == 0 && mlrFunction != NULL) {
        mlrFunction(argument, data, lengthData);
    }

    if(isMlrSuccess == false && *lengthData > 0) {
        if(data[0]==0x47) {
            headerSize =0;
        } else if ((unsigned int)((data[0] >> 6) & 0x03)==2) { // Assuming it's rtp header
            rh->b.v = (unsigned int)((data[0] >> 6) & 0x03);
            rh->b.p = (unsigned int)((data[0] >> 5) & 0x01);
            rh->b.x = (unsigned int)((data[0] >> 4) & 0x01);
            rh->b.cc = (unsigned int)((data[0] >> 0) & 0x0f);
            rh->b.m = (unsigned int)((data[1] >> 7) & 0x01);
            rh->b.pt = (unsigned int)((data[1] >> 0) & 0x7f);
            intP = 0;
            memcpy(charP + 2, &data[2], 2);
            rh->b.sequence = ntohl(intP);
            intP = 0;
            memcpy(charP, &data[4], 4);
            rh->timestamp = ntohl(intP);
            headerSize = 12 + 4 * rh->b.cc;	/* in bytes */
            *lengthData -= headerSize;
            memcpy(data, &data[headerSize], *lengthData);
        } else { // what's this
            fprintf(stderr, "can't recognize header\n");
            exit(3);
        }
    }

    return 0;
}

void set_thread_priority(int idx) {
    int ret;
    char *policy_name[] = {"SCHED_OTHER","SCHED_RR","SCHED_FIFO"};
    int policy[] = {SCHED_OTHER, SCHED_RR, SCHED_FIFO};

    //int idx = 2;

    pthread_t this_thread = pthread_self();

    struct sched_param params;
    params.sched_priority = sched_get_priority_max(policy[idx]);

    ret = pthread_setschedparam(this_thread, policy[idx], &params);
    if (ret != 0) {
        log_info("Unsuccessful in setting thread prio");
        return;
    }
    int ret_policy = 0;
    ret = pthread_getschedparam(this_thread, &ret_policy, &params);
    if (ret != 0) {
        log_info("Couldn't retrieve real-time scheduling paramers");
        return;
    }

    if(ret_policy != policy[idx]) {
        log_info("SCHED policy is NOT %s!",policy_name[idx]);
    } else {
        log_info("SCHED policy = %s OK",policy_name[idx]);
    }

    log_info("Thread priority is %d" ,params.sched_priority );
}

/* create a sender socket. */
int makesocket(char *szAddr, unsigned short port, int TTL, struct sockaddr_in *sSockAddr) {
    int iRet, iLoop = 1;
    char cTtl = (char)TTL;
    char cLoop = 0;

    int iSocket = socket(AF_INET, SOCK_DGRAM, 0);

    if (iSocket < 0) {
        log_err("[%s] socket() failed.", __FUNCTION__);
		return -1;
    }

    sSockAddr->sin_family =  AF_INET;
    sSockAddr->sin_port =  htons(port);
    sSockAddr->sin_addr.s_addr = inet_addr(szAddr);

    iRet = setsockopt(iSocket, SOL_SOCKET, SO_REUSEADDR, &iLoop, sizeof(int));
    if (iRet < 0) {
		log_err("[%s] setsockopt SO_REUSEADDR failed", __FUNCTION__);
        return -1;
    }

    iRet = setsockopt(iSocket, IPPROTO_IP, IP_MULTICAST_TTL, &cTtl, sizeof(char));
    if (iRet < 0) {
		log_err("[%s] setsockopt IP_MULTICAST_TTL failed.  multicast in kernel?", __FUNCTION__);
        return -1;
    }

    cLoop = 1; /* !? */
    iRet = setsockopt(iSocket, IPPROTO_IP, IP_MULTICAST_LOOP, &cLoop, sizeof(char));
    if (iRet < 0) {
		log_err("[%s] setsockopt IP_MULTICAST_LOOP failed.  multicast in kernel?", __FUNCTION__);
        return -1;
    }

    return iSocket;
}

int stdvbplayer_makeclientsocket(char *szAddr, unsigned short port, int TTL, struct sockaddr_in *sSockAddr, struct ip_mreq *blub, int nDevice)
{
    int socket = makesocket(szAddr, port, TTL, sSockAddr);
    //struct ip_mreq blub;
    struct sockaddr_in sin;
    unsigned int tempaddr;
    sin.sin_family = AF_INET;
    sin.sin_port = htons(port);
    sin.sin_addr.s_addr = inet_addr(szAddr);

#ifdef STB_BMT_LEAVE_10MS_JOIN_TEST_BULLSHIT_NOSENSEINDOING
	if (gRtpInfo[nDevice]->socketBeforeIn >= 0) {
		//close(gRtpInfo[nDevice]->socketBeforeIn);
		
        cMulticast_leave(&gRtpInfo[nDevice]->socketBeforeIn, &gRtpInfo[nDevice]->blub);
        gRtpInfo[nDevice]->socketBeforeIn = -1;
	}
#endif

    if (bind(socket, (struct sockaddr *)&sin, sizeof(sin))) {
        logLogCatKey_stdvb("[rtp.c][DEVICE %d] bind failed", nDevice);
		log_err("[%s] bind failed", __FUNCTION__);
        return -1;
    }
    tempaddr = inet_addr(szAddr);
    if ((ntohl(tempaddr) >> 28) == 0xe) {
        blub->imr_multiaddr.s_addr = inet_addr(szAddr);
        blub->imr_interface.s_addr = 0;

		verbose("[%s] will multicast join in use set sock opt api", __FUNCTION__);

		logLogCatKey_stdvb("[rtp.c][DEVICE %d] befor call setsockopt IP_ADD_MEMBERSHIP (think join request send)", nDevice);

        if (setsockopt(socket, IPPROTO_IP, IP_ADD_MEMBERSHIP, blub/*&blub*/, sizeof(struct ip_mreq))) {
            logLogCatKey_stdvb("[%s] setsockopt IP_ADD_MEMBERSHIP failed , ip : %s, errno : %d(%s)", __FUNCTION__, szAddr, errno, strerror(errno));
			// error fix : after bind...  still live address binded socket
			close(socket);
            return -1;
        }

		logLogCatKey_stdvb("[rtp.c][DEVICE %d] after call setsockopt IP_ADD_MEMBERSHIP (think join request send)", nDevice);
    }

    int size = 3000000; //1<<20;

    logLogCatKey_stdvb("socket : %d", socket);
    logLogCatKey_stdvb("setsockopt SO_RCVBUF to %d bytes", size);

    if ( (setsockopt(socket, 0, SO_RCVBUF, &size ,sizeof(int)) ) < 0 )
        logLogCatKey_stdvb("[%s] setsockopt SO_RCVBUF failed (%d)", __FUNCTION__,size);

    return socket;
}

int cMulticast_leave(int *socket, struct ip_mreq *blub)
{
    if(setsockopt(*socket, IPPROTO_IP, IP_DROP_MEMBERSHIP, (void*) blub, sizeof(struct ip_mreq)) < 0) {
        fprintf( stderr, "cMulticast_leave : setsocketop() error !!!\n" );
    }

    close(*socket);
    *socket = -1;
    return 0;
}
