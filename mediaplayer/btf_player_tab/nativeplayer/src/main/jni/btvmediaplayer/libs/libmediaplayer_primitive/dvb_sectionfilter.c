// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <sys/poll.h>



#include "dvb_primitive.h"

//#define NDEBUG
#include <dbg.h>
#include "dvb_table.h"

//#define USE_SW_DEMUX

#define MAX_SECTION_SIZE 8192

#define READ_TIMEOUT_SEC  1
#define READ_TIMEOUT_USEC 500000

#define MIN_READ_TIMEOUT_SEC  0
#define MIN_READ_TIMEOUT_USEC 100000

#define MAX_TRY_CNT 3

int dvb_set_es( dvb_locator_t *locator, uint16_t pid, TABLE_StreamType_t type )
{
	if( locator == NULL ) {
		printf("NULL locator\n");
		return -1;
	}
	logLogCatKey_stdvb ("[SECTION] TYPE : %d", type);
	switch( type ) {
		case TABLE_STREAM_VIDEO_HEVCH265:
			logLogCatKey_stdvb ("[SECTION] VIDEO CODEC : %d(AVP_CODEC_VIDEO_H265), PID : %d", pid);
			locator->vid = pid;
			locator->vcodec = AVP_CODEC_VIDEO_H265;
			return 1;

        case TABLE_STREAM_AUDIO_EAC3:
			logLogCatKey_stdvb ("[SECTION] AUDIO CODEC : %d(AVP_CODEC_AUDIO_EAC3), PID : %d", pid);
			locator->aid = pid;
			locator->acodec = AVP_CODEC_AUDIO_EAC3;
			return 2;

		case TABLE_STREAM_VIDEO_MPEG4      :
			logLogCatKey_stdvb ("[SECTION] VIDEO CODEC : %d(AVP_CODEC_VIDEO_H264), PID : %d", pid);
//			if( locator->vid != pid ) {
				locator->vid = pid;
				locator->vcodec = AVP_CODEC_VIDEO_H264;
				return 1;
//			}
			break;
		case TABLE_STREAM_AUDIO_AC3        :
			printf("audio %d(0x%x) AC3\n", pid, pid);
			logLogCatKey_stdvb ("[SECTION] AUDIO CODEC : %d(AVP_CODEC_AUDIO_AC3), PID : %d", pid);
//			if( locator->aid != pid ) {
				locator->aid = pid;
				locator->acodec = AVP_CODEC_AUDIO_AC3;
				return 2;
//			}
			break;
		case TABLE_STREAM_AUDIO_AAC_ADTS   :
			printf("audio %d(0x%x) AAC\n", pid, pid);
			logLogCatKey_stdvb ("[SECTION] AUDIO CODEC : %d(AVP_CODEC_AUDIO_AAC), PID : %d", pid);
//			if( locator->aid != pid ) {
				locator->aid = pid;
				locator->acodec = AVP_CODEC_AUDIO_AAC;
				return 2;
//			}
			break;
		case TABLE_STREAM_RESERVED         :
		case TABLE_STREAM_VIDEO_MPEG1      :
		case TABLE_STREAM_VIDEO_MPEG2      :
		case TABLE_STREAM_AUDIO_MPEG1      :
		case TABLE_STREAM_AUDIO_MPEG2      :
		case TABLE_STREAM_PRIVATE_SECTIONS :
		case TABLE_STREAM_PRIVATE_PES      :
		case TABLE_STREAM_MHEG             :
		case TABLE_STREAM_DSM_CC           :
		case TABLE_STREAM_TYPE_H2221       :
		case TABLE_STREAM_TYPE_A           :
		case TABLE_STREAM_TYPE_B           :
		case TABLE_STREAM_TYPE_C           :
		case TABLE_STREAM_TYPE_D           :
		case TABLE_STREAM_TYPE_AUX         :
		case TABLE_STREAM_VIDEO_MPEG4P2    :
		case TABLE_STREAM_AUDIO_AAC_LATM   :
		case TABLE_STREAM_AUDIO_AAC_RAW1   :
		case TABLE_STREAM_AUDIO_AAC_RAW2   :
		case TABLE_STREAM_VIDEO_AVS        :
		case TABLE_STREAM_AUDIO_LPCM       :
		case TABLE_STREAM_AUDIO_DTS        :
		case TABLE_STREAM_AUDIO_MLP        :
		case TABLE_STREAM_AUDIO_DDPLUS     :
		case TABLE_STREAM_AUDIO_DTSHD      :
		case TABLE_STREAM_AUDIO_DTSHD_XLL  :
		case TABLE_STREAM_AUDIO_DDPLUS_2   :
		case TABLE_STREAM_AUDIO_DTSHD_2    :
		case TABLE_STREAM_VIDEO_VC1        :
		case TABLE_STREAM_AUDIO_WMA        :
			printf("unknown %d(0x%x)\n", pid, pid);
			break;
	}
	return 0;
}


int nCheckPID(uint8_t *pBuf, int nCheckPID) // if it's same pid then return 1... or retrun 0
{
    unsigned int 	pid = 0;
 
	if (pBuf == NULL) // broken memory case
	{
		logLogCatKey_stdvb ("[SECTION] buffer null");
		return 0;
	}

	// Retrieve the PID from transport packet
    pid = (pBuf[1] & 0x1F) << 8;
    pid |= (pBuf[2]);


	logLogCatKey_stdvb ("[SECTION] get PID [%d]", pid);

	if (pid == nCheckPID) // case match
		return 1;

	return 0; // case not match
}

int readSectionDataOneTs(FILE *Fp, int nPid, uint8_t *pBuf, int nBufSize) // will return payload data .. error : return -1 (blabla case)
{
	int nReadSize = 0;
	int nFindPID = 0;
	int nPayloadSize = 0;
	int nAdaptationField = 0;
	int nAdaptationFieldSize = 0;
	int nPayloadStart = 4;

	uint8_t pReadBuffer[192]; // ill make buffer size 192. for 4byte dummy

	logLogCatKey_stdvb ("[SECTION] readSectionDataOneTs called pid [%d]", nPid);

	if (nBufSize < 188) // small buffer case.. so error return
	{
		logLogCatKey_stdvb ("[SECTION] buffer small");
		return -1;
	}

	// get ts packet data
	while (nFindPID == 0)
	{
		nReadSize = fread (pReadBuffer, 1, 188, Fp);

		if (nReadSize != 188) // must be eof or error.. but i dnt care
		{
			logLogCatKey_stdvb ("[SECTION] fread error [%d]", nReadSize);
			break;
		}

		if( pReadBuffer[0] != 0x47 ) // shit!. TS sync byte was not matched.. must b broken ts file
		{
			logLogCatKey_stdvb ("[SECTION] TS syncbyte error");
        	break;
        }

		if (nCheckPID(pReadBuffer, nPid) == 1) // i gotcha baby ha.
		{
			logLogCatKey_stdvb ("[SECTION] found pid!");
			nFindPID = 1;
			break;
		}
	}

	if (nFindPID == 0) // darling.. you so ugly pid
		return -1;


	logLogCatKey_stdvb ("[SECTION] payload parsing start!");

	// parse payload data

	// 1. first check adaptation data 
	nAdaptationField = (pReadBuffer[3] & 0x30) >> 4;
	logLogCatKey_stdvb ("[SECTION] nAdaptationField [%d]", nAdaptationField);

	if (nAdaptationField == 2) // case no payload.. it will not happen'd in skt ts file
	{
		logLogCatKey_stdvb ("[SECTION] no payload data...");
		return -1;
	}

	if (nAdaptationField == 3) // case adaption field followed by payload ... so sexy.. case ID_main.ts file
	{
		nAdaptationFieldSize += pReadBuffer[4] + 1; // +1 means payload size field add
		logLogCatKey_stdvb ("[SECTION] adaptation field exsist...");
	}

	logLogCatKey_stdvb ("[SECTION] nAdaptationFieldSize [%d]...", nAdaptationFieldSize);

	// 2. catch payload data
	// I think skt ts file PMT/PAT has no continuity

	nPayloadSize = pReadBuffer[nPayloadStart + nAdaptationFieldSize + 3] + 3;
	logLogCatKey_stdvb ("[SECTION] nPayloadSize [%d]...", nPayloadSize);

	memcpy (pBuf, &pReadBuffer[nPayloadStart + nAdaptationFieldSize + 1], nPayloadSize);

	return nPayloadSize;
}

int dvb_get_pmt_pid_by_file(dvb_locator_t *locator, unsigned int *pmtPid)
{
	FILE *fp;

	pat_data_t pat;

	int DataLength=0;
	int remainData;

	uint8_t       buf[MAX_SECTION_SIZE];
	int i;

	logLogCatKey_stdvb ("[SECTION] dvb_get_pmt_pid_by_file start...");
	logLogCatKey_stdvb ("[SECTION] filename [%s]...", locator->tune_param.file.filename);

	fp = fopen64(locator->tune_param.file.filename,"rb");
	
	if (fp == NULL)
	{
		logLogCatKey_stdvb ("[SECTION] file open error!!");
		return -1;
	}

	while (1)
	{
		DataLength = readSectionDataOneTs (fp, 0, buf, sizeof (buf));
		logLogCatKey_stdvb ("[SECTION] DataLength : %d", DataLength);
		if (DataLength < 0)
		{
			logLogCatKey_stdvb ("[SECTION] read failed!!");

			fclose(fp);

			return -1;
		}
		else if (DataLength > 0)
		{
			int idx = 0;

			//#if 0
			logLogCatKey_stdvb ("[SECTION] data ======================== start");
			for(i = 0; i < DataLength; i++)
			{
				if(i && i%16 == 0)
					logLogCatKey_stdvb ("[SECTION] ---------------------");

				logLogCatKey_stdvb ("[SECTION] [%02x]", buf[i]);
			}
			logLogCatKey_stdvb ("[SECTION] data ======================== end");
			//#endif

			memset(&pat, 0, sizeof(pat_data_t));

			/*idx = 0*/
			pat.table_id = buf[idx++];

			/*idx = 1*/
			pat.section_syntax_indicator = (buf[idx] & 0x80) >> 7;

			/*idx = 1, idx = 2*/
			pat.section_length = ((buf[idx++] & 0xf) << 8);
			pat.section_length |= buf[idx++];

			/*idx = 3, idx = 4*/
			pat.transport_stream_id = (buf[idx++] << 8);
			pat.transport_stream_id |=  buf[idx++];

			/*idx = 5*/
			pat.version_number = (buf[idx] & 0x2e) >> 1;
			pat.current_next_indicator = buf[idx++] &0x1;

			/*idx = 6*/
			pat.section_number = buf[idx++];

			/*idx = 7*/
			pat.last_section_number = buf[idx++];

			printf( "\n#------------------------------------------#\n" );
			printf( "# Program Association Table\n" );
			printf( "#------------------------------------------#\n" );
			printf( " table_id = %#x\n", pat.table_id);
			printf( " section_syntax_indicator =%#x\n" , pat.section_syntax_indicator);
			printf( " section_length= %#x\n", pat.section_length);
			printf( " transport_stream_id = %#x\n" , pat.transport_stream_id);
			printf( " version_number = %#x\n" , pat.version_number);
			printf( " current_next_indicator = %#x\n" , pat.current_next_indicator);
			printf( " section_number = %#x\n" , pat.section_number);
			printf( " last_section_number = %#x\n" , pat.last_section_number);


			remainData = pat.section_length-5-4; /* 5 byes = from transport_stream_id to last_section_number, 4 bytes = CRC_32*/
			pat.program_count = remainData /4;

			printf( " program count = %d\n", pat.program_count);

			for(i = 0; i<pat.program_count; i++)
			{
				pat.pmt[i].program_number = (buf[idx++] << 8);
				pat.pmt[i].program_number |= (buf[idx++]);

				pat.pmt[i].pid = (buf[idx++] & 0x1f) << 8;
				pat.pmt[i].pid |= (buf[idx++]);

				printf( "\tprograme_number = %d\n" , pat.pmt[i].program_number);
				printf( "\tpid = %#x\n", pat.pmt[i].pid);

				if(pat.pmt[i].program_number != 0)
				{
					*pmtPid = pat.pmt[i].pid;
					printf( "#------------------------------------------#\n\n" );

					fclose(fp);
					logLogCatKey_stdvb ("[SECTION] success get pmtid [%d]", pat.pmt[i].pid);
					return 0;
				}
			}
		}

		logLogCatKey_stdvb ("[SECTION] must be error case .. next reading");
		usleep (1000); //FIXME,

	}

	logLogCatKey_stdvb ("[SECTION] must be error case .. not pmtid not found");
	fclose(fp);

	return 0;
}


int dvb_set_locator_info_by_file(dvb_locator_t *locator, unsigned int pmtPid)
{
	FILE *fp;

	int result;
	int vidSet = 0;
	int audSet = 0;
	int pcrSet = 0;
	int DataLength=0;
	uint8_t	buf[MAX_SECTION_SIZE];
	
	static uint16_t progNum;
	static uint16_t pcrPid;
	static TABLE_PMTElement_t pmt;
	static TABLE_Descriptor_t desc;
	
	logLogCatKey_stdvb ("[SECTION] dvb_set_locator_info_by_file start...");
	logLogCatKey_stdvb ("[SECTION] filename [%s]...", locator->tune_param.file.filename);

	fp = fopen64(locator->tune_param.file.filename,"rb");
	
	if (fp == NULL)
	{
		logLogCatKey_stdvb ("[SECTION] file open error!!");
		return -1;
	}
	
	
	// jung2604 : ???? ??? ??????? ??a???? ?????? ????? ??????..
	int retryCount = 0;
	while(retryCount < 5) { // while(1)
		retryCount++;
		DataLength = readSectionDataOneTs (fp, pmtPid, buf, sizeof (buf));
		
		logLogCatKey_stdvb ("[SECTION] DataLength : %d", DataLength);

		if (DataLength < 0)
		{
			logLogCatKey_stdvb ("[SECTION] read failed!!");

			fclose(fp);

			return -1;
		}
		else if (DataLength > 0) 
		{
			uint16_t      secLen = 0;
			uint16_t      progInfoLen = 0;
			uint16_t      esInfoLen = 0;
			uint8_t              version = 0;
			int   i = 0;
			int   j = 0;
			int   k = 0;
			int n = 0;
			int d = 0;
			int e = 0;
			int   numOfEntries = 0;
			int   numGenDesc = 0;
			int   numEsDesc = 0;

			logLogCatKey_stdvb ("[SECTION] data ======================== start");
			for(k = 0; k < DataLength; k++)
			{
				if(k && k % 16 == 0)
					logLogCatKey_stdvb ("[SECTION] ---------------------");

				logLogCatKey_stdvb ("[SECTION] [%02x]", buf[k]);
			}
			logLogCatKey_stdvb ("[SECTION] data ======================== end");
			
			if (DataLength < 3) {

				logLogCatKey_stdvb ("[SECTION] data too short");

				break;
			}
			secLen = (((buf[i+1]&0x0f)<<8)|(buf[i+2]&0xff))+3;
			printf( "section length = %d\n", secLen);
			
			progNum = ((buf[3])<<8)|(buf[4]);
			printf( "program number = %d\n", progNum);
			version = ((buf[5]&0x3e)>>1);
			printf( "version = %d\n", version);
			pcrPid = ((buf[8]&0x1f)<<8)|(buf[9]&0xff);
			printf( "pcr pid = %d\n", pcrPid);
			progInfoLen = (((buf[i+10]&0x0f)<<8)|(buf[i+11]&0xff));
			printf( "program info length = %d\n" , progInfoLen);
			
			// test es loop
			j = progInfoLen + 12;
			while (j < (secLen-4)) {
				esInfoLen = ((((buf[i+j+3]&0x0f)<<8))|(buf[i+j+4]&0xff));
				printf( "[%d] es info length = %d\n" , numOfEntries, esInfoLen);
				j += esInfoLen+5;
				numOfEntries++;
			}
			if (numOfEntries == 0) {
				logLogCatKey_stdvb ("[SECTION] PMT number of entries = 0");
				break;
			}
			
			j = 12;
			while (j < progInfoLen) {
				numGenDesc++;
				j += buf[i+j+13]+2; // descriptor length
			}
			
			j = 12;
			while (j < progInfoLen) {
				desc.Tag = buf[i+j+12];
				desc.Size = buf[i+j+13];
				desc.Data = NULL;
				j += desc.Size + 2;
				d++;
			}
			
			j = progInfoLen + 12;
			while (j < (secLen-4)) 
			{
				pmt.Type = buf[i+j];
				pmt.Pid = (((buf[i+j+1]&0x1F)<<8)|(buf[i+j+2]));
				pmt.NbDescriptors = 0;
				pmt.Descriptor = NULL;
				
				result = dvb_set_es( locator, pmt.Pid, pmt.Type );
				if(result == 1)
				{
					vidSet = 1;
				}
				else if (result == 2)
				{
					audSet = 1;
					break;
				}
				else
				{
					logLogCatKey_stdvb ("[SECTION] unsupported elementary stream type.");
				}
				
				esInfoLen = ((((buf[i+j+3]&0x0f)<<8))|(buf[i+j+4]&0xff));
				j += esInfoLen+5;
				n++;
			}

			// jung2604 : vidSet??? .. 
			// if( auSet )
			if( audSet && vidSet)
			{
				// jung2604 : ???? ?????? ??????? ???????? -> locator->vcodec = locator->vcodec;
				if(locator->vcodec == 0)  locator->vcodec = AVP_CODEC_VIDEO_H264;

				if( locator->pid != pcrPid ) 
				{
					logLogCatKey_stdvb ("[SECTION] found pid.: pcrPid %d", pcrPid);
					locator->vid = pcrPid;
					locator->pcr_pid = pcrPid; // TODO : jung2604
					pcrSet = 1;
				}

				fclose(fp);

				logLogCatKey_stdvb ("[SECTION] found and return.");

				return 0;
			}
	

		}
        usleep (100); //FIXME,	  
	}
// jung2604
	if( audSet ) {
        logLogCatKey_stdvb ("++[SECTION] last auSet .");
		// jung2604 : ???? ?????? ??????? ???????? -> locator->vcodec = locator->vcodec;
		locator->vcodec = AVP_CODEC_VIDEO_H264;

		if( locator->pid != pcrPid )
		{
			logLogCatKey_stdvb ("[SECTION] found pid.: pcrPid %d", pcrPid);
			locator->vid = pcrPid;
			locator->pcr_pid = pcrPid; // TODO : jung2604
			pcrSet = 1;
		}

		fclose(fp);

		logLogCatKey_stdvb ("[SECTION] found and return.");

		return 0;
	}

	logLogCatKey_stdvb ("[SECTION] must be error case...");

	fclose(fp);
	return 0;
}

int  dvb_set_stream_info(dvb_player_t *player, dvb_locator_t *locator)
{
	int result;
	unsigned int pmtPid;

	logLogCatKey_stdvb("[dvb_set_stream_info] before call dvb_get_pmt_pid_by_file");
	result = dvb_get_pmt_pid_by_file(locator, &pmtPid);
	logLogCatKey_stdvb("[dvb_set_stream_info] after call dvb_get_pmt_pid_by_file pmtPid [%d] result [%d]", pmtPid, result);
	
	if(result != 0)
	{
		printf( "Fail to get the PMT pid.\n" );
		logLogCatKey_stdvb( "Fail to get the PMT pid." );
		return -1;
	}

	logLogCatKey_stdvb("[dvb_set_stream_info] before call dvb_set_locator_info_by_file");
	result = dvb_set_locator_info_by_file(locator, pmtPid);
	logLogCatKey_stdvb("[dvb_set_stream_info] after call dvb_set_locator_info_by_file");
	
	if(result != 0)
    {
        printf("Fail to get the audio/vdieo ES info.\n");
		logLogCatKey_stdvb( "Fail to get the audio/vdieo ES info." );
		return -1;
    }
	
	return 0;
}

void DEMUX_Callback(void *handle, uint16_t pid, uint16_t tid,local_buffer* buffer, void *user_param){
	verbose("%s, pid:%d, tid:%d, data[0]:0x%02X, data_length:%d, user_param:%p",__FUNCTION__, pid, tid, buffer->buffer[0], buffer->length, user_param);

	dvb_player_t * player = (dvb_player_t*)user_param;
	uint8_t  *ptr = buffer->buffer;
	if(buffer->length <= 4096) {
		if(ptr[0] == tid){
			AVP_SectionFilterCallback callback = player->avpInfo.sectionFilterConfig.callback;
			if((tid == 0x3C) || (tid == 0x3B)){
				if(callback){
					callback(buffer->buffer, buffer->length, player);
				}
			}else{
				verbose("%s, pid:%d, tid:%d, data[0]:0x%02X, data_length:%d, avpInfo.isAcquireDone :%d",__FUNCTION__, pid, tid, buffer->buffer[0], buffer->length, player->avpInfo.isAcquireDone );
				if(player->avpInfo.isAcquireDone == false){
					player->avpInfo.isAcquireDone = true;				
					if(callback){
						callback(buffer->buffer, buffer->length, player);
					}
				}
			}
		}
	}
	verbose("%s end",__FUNCTION__);	
}

int dvb_set_section_filter(dvb_player_t *player, unsigned short pid, unsigned char tid, AVP_SectionFilterCallback callback)
{
	verbose("%s pid:%d, tid:%d,",__FUNCTION__, pid, tid);
	
	player->avpInfo.sectionFilterConfig.pid = pid;
	player->avpInfo.sectionFilterConfig.tid = tid;
	player->avpInfo.sectionFilterConfig.section_user_param = player;
	player->avpInfo.sectionFilterConfig.callback = callback;
	player->avpInfo.isAcquireDone = false;

#ifdef USE_SW_DEMUX
	if(player->demuxHandle != NULL) {
			section_filter sect;
            sect.pid = pid;
            sect.tid = tid;
            sect.callback= DEMUX_Callback;
            sect.user_param = player;
            DEMUX_SetSectionFilter(player->demuxHandle, &sect);
			return 0;
	}
#else
#ifdef USE_BTV_HAL_MANAGER
	if(HalBtv_SetSectionFilter(player->idx_player, &player->avpInfo.sectionFilterConfig) != 0)
		goto error;
#else
	if(AVP_SetSectionFilter(player->avpInfo.playerHandle, &player->avpInfo.sectionFilterConfig) != 0)
		goto error;
#endif
#endif

	log_info ("[dvb_set_section_filter] success");
	return 0;
	
error:
	log_info ("[dvb_set_section_filter] must be error");
	return -1;
}   

int dvb_clear_section_filter(dvb_player_t *player)
{
	verbose("%s",__FUNCTION__);
#ifdef USE_SW_DEMUX
	if(player->demuxHandle != NULL) {

		unsigned short pid = player->avpInfo.sectionFilterConfig.pid;
		unsigned char tid = player->avpInfo.sectionFilterConfig.tid;
		
		demux_context *demux =	(demux_context *)player->demuxHandle;
		section_filter sect ;		
		sect.pid = pid;
		sect.tid = tid;
		DEMUX_ClearSectionFilter(demux, &sect);
		return 0;
	}
#else
#ifdef USE_BTV_HAL_MANAGER
		if(HalBtv_ClearSectionFilter(player->idx_player) != 0) goto error;
#else
		if(AVP_ClearSectionFilter(player->avpInfo.playerHandle) != 0) goto error;
#endif
#endif		
	log_info ("[dvb_clear_section_filter] success");
	return 0;
		
error:
	log_info ("[dvb_clear_section_filter] must be error");
	return -1;

}

int dvb_set_dsmcc_section_filter(dvb_player_t *player, unsigned short pid, unsigned char tid, AVP_SectionFilterCallback callback)
{
	verbose("%s pid:%d, tid:%d,",__FUNCTION__, pid, tid);

    player->avpInfo.sectionFilterConfig.pid = pid;
    player->avpInfo.sectionFilterConfig.tid = tid;
    player->avpInfo.sectionFilterConfig.section_user_param = player;
    player->avpInfo.sectionFilterConfig.callback = callback;

#ifdef USE_SW_DEMUX
	if(player->demuxHandle != NULL) {
			section_filter sect;
            sect.pid = pid;
            sect.tid = tid;
            sect.callback= DEMUX_Callback;
            sect.user_param = player;
            DEMUX_SetSectionFilter(player->demuxHandle, &sect);
			return 0;
	}
	
#else
#ifdef USE_BTV_HAL_MANAGER
		if(HalBtv_SetSectionFilter(player->idx_player, &player->avpInfo.sectionFilterConfig) != 0)
			goto error;
#else
		if(AVP_SetSectionFilter(player->avpInfo.playerHandle, &player->avpInfo.sectionFilterConfig) != 0)
			goto error;
#endif
#endif

	log_info ("[dvb_set_dsmcc_section_filter] success");
	return 0;
	
error:
	log_info ("[dvb_set_dsmcc_section_filter] must be error");
	return -1;
}   

int dvb_clear_dsmcc_section_filter(dvb_player_t *player)
{
	verbose("%s",__FUNCTION__);
#ifdef USE_SW_DEMUX	
	if(player->demuxHandle != NULL) {
		unsigned short pid = player->avpInfo.sectionFilterConfig.pid;
		unsigned char tid = player->avpInfo.sectionFilterConfig.tid;
		
		demux_context *demux =	(demux_context *)player->demuxHandle;
		section_filter sect ;		
		sect.pid = pid;
		sect.tid = tid;
		DEMUX_ClearSectionFilter(demux, &sect);
		return 0;
	}
	
#else
#ifdef USE_BTV_HAL_MANAGER
	if(HalBtv_ClearSectionFilter(player->idx_player) != 0) goto error;
#else
	if(AVP_ClearSectionFilter(player->avpInfo.playerHandle) != 0) goto error;
#endif
#endif		
	log_info ("[dvb_clear_dsmcc_section_filter] success");
	return 0;
		
error:
	log_info ("[dvb_clear_dsmcc_section_filter] must be error");
	return -1;

}


#ifdef __cplusplus
} //extern "C"
#endif
