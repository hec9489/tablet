// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifdef __cplusplus
extern "C" {
#endif

#include <android/log.h>
#include "dsmcc.h"
#include "dsmcc-receiver.h"
#include "dsmcc-carousel.h"
#include <systemproperty.h>
//---------------------------------- LOGS ------------------------------------

void dsmcc_log_dump(const char* data, int lengths)
{
#ifdef DSMCC_DEBUG_LOG_ENABLE
	char szDebugTemp[4096] = {0,};

	if(data == NULL) {
		sprintf(szDebugTemp, "\n\n==========================================================\n");
		sprintf(szDebugTemp+strlen(szDebugTemp), ("----------------------------------------------------------\n"));
		sprintf(szDebugTemp+strlen(szDebugTemp), (">>>>>>>>>>>>>>>>>>>>DATA is Empty!!<<<<<<<<<<<<<<<<<<<<<<<\n"));	
		__android_log_print(ANDROID_LOG_DEBUG  , "[DSM_CC]", "%s", szDebugTemp);		
		return;
	}

	sprintf(szDebugTemp, "\n\n==========================================================\n");
	sprintf(szDebugTemp+strlen(szDebugTemp), "[size]:%d\n", lengths);
	sprintf(szDebugTemp+strlen(szDebugTemp), "----------------------------------------------------------\n");	

	int i     = 0;
	int j     = 0;
	int width = 16;

	for(i=0; i<lengths; ) {
		sprintf(szDebugTemp+strlen(szDebugTemp), "%04d", i);			

		// Print Hexa Code
		for(j=i; j<(i+width); j++) {
			if(!(j%(width/2))) {
				sprintf(szDebugTemp+strlen(szDebugTemp), " ");							
			}

			if(j >= lengths) {
				sprintf(szDebugTemp+strlen(szDebugTemp), "   ");											
			} else {
				sprintf(szDebugTemp+strlen(szDebugTemp), "%02x", (unsigned char)(data[j]));														
			}
		}
		sprintf(szDebugTemp+strlen(szDebugTemp), ": ");		

		// Print ASCII Code
		for(j=i; j<(i+width) && j<lengths; j++) {
			if(data[j] < 0x20 || data[j] > 0x70) {
				sprintf(szDebugTemp+strlen(szDebugTemp), ".");						
			} else {
				sprintf(szDebugTemp+strlen(szDebugTemp), "%c", data[j]);									
			}
		}

		i += width;
		if(i>lengths) {
			i = (int)lengths;
		}
		sprintf(szDebugTemp+strlen(szDebugTemp), "\n");											
	}
	
	sprintf(szDebugTemp+strlen(szDebugTemp), "===========================================================\n\n");												
	__android_log_print(ANDROID_LOG_DEBUG  , "[DVB_DSM_CC]", "%s", szDebugTemp);		
#endif	
}


void dsmcc_log(const char *format, ...)
{
#ifdef DSMCC_DEBUG_LOG_ENABLE
	struct 		timeval val;
	struct tm 	*ptm = NULL;
	va_list 	va;
	char szDebugTemp[4096] = {0,};
	int nHeaderLength;

	gettimeofday( &val, NULL );
	ptm = localtime( &val.tv_sec );

	sprintf(szDebugTemp, "[%02d/%02d %02d:%02d:%02d:%03d]\t%lld\t", ptm->tm_mon + 1, ptm->tm_mday
			,ptm->tm_hour, ptm->tm_min, ptm->tm_sec, (int)val.tv_usec / 1000, (long long) val.tv_sec*1000 + (long long)val.tv_usec/1000);

	nHeaderLength = strlen(szDebugTemp);

	va_start(va, format);
	vsprintf(szDebugTemp+strlen(szDebugTemp), format, va);
	va_end(va);

	__android_log_print(ANDROID_LOG_DEBUG  , "[DVB_DSM_CC]", "%s", szDebugTemp);
#endif	
}  

//---------------------------------- EVENT QUEUE------------------------------------

void dsmcc_lock_event_list(dsmcc_event_head_t *l)
{
    pthread_mutex_lock(&l->lock);
}

void dsmcc_unlock_event_list(dsmcc_event_head_t *l)
{
    pthread_mutex_unlock(&l->lock);
    pthread_cond_signal(&l->cond);
}

dsmcc_event_list_t* dsmcc_create_event_entry(int event, int param, void *data)
{
    dsmcc_event_list_t *c = calloc(sizeof(char), sizeof(dsmcc_event_list_t));
    if (!c)
        return 0;

    c->event = event;
    c->param = param;    
    c->data = data;
    c->next = 0;
    return c;
}

void dsmcc_destroy_event_entry(dsmcc_event_list_t* c)
{
    if(c!=0){
        free(c);
    }
}

dsmcc_event_list_t *dsmcc_dequeue_event(dsmcc_event_head_t *list)
{
    dsmcc_event_list_t *eptr;
    dsmcc_lock_event_list(list);
    eptr = list->head;
    if (eptr) {
        list->head = eptr->next;
        if (!list->head) {
            assert(list->last == eptr);
            list->last = 0;
        }
    }
    dsmcc_unlock_event_list(list);
    return eptr;
}

void dsmcc_queue_event(dsmcc_event_head_t *list, dsmcc_event_list_t *c, int add_to_front)
{
    c->next = 0;
    dsmcc_lock_event_list(list);
    /* appending a new entry to the back of the list */
    if (list->last) {
        assert(list->head);
        // List is not empty
        if (!add_to_front) {
            list->last->next = c;
            list->last = c;
        } else {
            c->next = list->head;
            list->head = c;
        }
    } else {
        // List is empty
        assert(!list->head);
        list->head = c;
        list->last = c;
    }
    dsmcc_unlock_event_list(list);
}

int dsmcc_queue_event_data(dsmcc_event_head_t *list, int event, int param, void *data)
{
    dsmcc_event_list_t *b  = dsmcc_create_event_entry(event, param, data);
    if (!b)
        return -1;
    dsmcc_queue_event(list, b, 0);
    return 0;
}

int dsmcc_queue_front_event_data(dsmcc_event_head_t *list, int event, int param, void *data)
{
    dsmcc_event_list_t *b  = dsmcc_create_event_entry(event, param, data);
    if (!b)
        return -1;
    dsmcc_queue_event(list, b, 1);
    return 0;
}


int dsmcc_remove_event(dsmcc_event_head_t *list)
{
    dsmcc_event_list_t *c = dsmcc_dequeue_event(list);
    if (!c) {
        return 0;
    }
    dsmcc_destroy_event_entry(c);
    return 1;
}

void dsmcc_free_events(dsmcc_event_head_t *list)
{
    while (dsmcc_remove_event(list))
        ;
}


//---------------------------------- HELPERS ------------------------------------

void dsmcc_free_section_data(dsmcc_section_data* section)
{
	if(section)
	{
		if(section->section_data)
			free(section->section_data);
		free(section);
	}
}

void dsmcc_callback_event(dsmcc_ctx *ctx, int event, char* path, void *data)
{
	if(ctx && ctx->callback){
		ctx->callback(ctx->handle, event, path, data);
	}
}

void dsmcc_post_application_data(dsmcc_ctx *ctx, dsmcc_application *data)
{
	if(ctx && !ctx->stop_requested){
		dsmcc_application* app = (dsmcc_application*) calloc(sizeof(unsigned char), sizeof(dsmcc_application));
		memcpy((void*)app, (void *)data, sizeof(dsmcc_application));
		dsmcc_queue_event_data(&ctx->dsmcc_event, DSMCC_EVENT_APPLICATION_DATA, 0, app);
	}
}

void dsmcc_post_section_filter_data(dsmcc_ctx *ctx, void *data, int size, void *user_param, dsmcc_section_done_callback callback)
{
	if(ctx && !ctx->stop_requested){
		dsmcc_section_data* section = (dsmcc_section_data*) calloc(sizeof(unsigned char), sizeof(dsmcc_section_data));
		section->section_data = (void *)calloc(sizeof(unsigned char), size);
		memcpy((void*)section->section_data, (void *)data, size);
		section->section_data_size = size;
		section->section_user_param = user_param;
		section->callback = callback;
		dsmcc_queue_event_data(&ctx->dsmcc_event, DSMCC_EVENT_SECTION_FILTER_DATA, 0, section);
	}
}

//------------------------------- PROCESSING --------------------------------

int dsmcc_check_utf_string(const char* bytes)
{
    const char* origBytes = bytes;
    if (bytes == NULL) {
        return -1;
    }
    while (*bytes != '\0') {
       unsigned char utf8 = *(bytes++);
        // Switch on the high four bits.
        switch (utf8 >> 4) {
            case 0x00:
            case 0x01:
            case 0x02:
            case 0x03:
            case 0x04:
            case 0x05:
            case 0x06:
            case 0x07: {
                // Bit pattern 0xxx. No need for any extra bytes.
                break;
            }
            case 0x08:
            case 0x09:
            case 0x0a:
            case 0x0b:
            case 0x0f: {
                /*printf("****JNI WARNING: illegal start byte 0x%x\n", utf8);*/
                return -1;
            }
            case 0x0e: {
                // Bit pattern 1110, so there are two additional bytes.
                utf8 = *(bytes++);
                if ((utf8 & 0xc0) != 0x80) {
                    /*printf("****JNI WARNING: illegal continuation byte 0x%x\n", utf8);*/
                    return -1;
                }
                // Fall through to take care of the final byte.
            }
            case 0x0c:
            case 0x0d: {
                // Bit pattern 110x, so there is one additional byte.
                utf8 = *(bytes++);
                if ((utf8 & 0xc0) != 0x80) {
                    /*printf("****JNI WARNING: illegal continuation byte 0x%x\n", utf8);*/
                    return -1;
                }
                break;
            }
        }
    }
    return 0;
}


void dsmcc_process_send_event_toapp(dsmcc_ctx *ctx, int eventType, int cached)
{
	struct obj_carousel* car;
	car = ctx->active_carousel;
	dsmcc_application *application = &car->application;
	int mainType;
	int path_len;

	dsmcc_sigevent event;

	memset(&event, 0x00, sizeof(dsmcc_sigevent));
	
	char *path = (char *) ctx->buffer;
	char dataPath[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
	if(application->appType == DSMCC_MUSIC_APP_TYPE){
		if(cached)
			mainType = INFO_MUSIC_CACHE;
		else
			mainType = INFO_MUSIC_DOWNLOAD;
		// path name pid/file_name	<== in Music case	

		path_len = sprintf(path, "%s%s/%d/%s", dataPath, DSMCC_ROOTPATH, application->dsmccPid, application->initialPath);
		path[path_len]='\0';
	}else{
		// path name pid <== in app case
		mainType = OC_DOWNLOAD_COMPLETE;
		path_len = sprintf(path, "%s%s/%d", dataPath, DSMCC_ROOTPATH, application->dsmccPid);
		path[path_len]='\0';
	}
	
	event.eventType = eventType;
	event.serviceId = application->channelNum;
	event.demux = 0;
	memcpy(event.uri, path, strlen(path));
	
	event.application.appType = application->appType;
	event.application.organizationId = application->organizationId;
	event.application.applicationId = application->applicationId;
	if(application->appType == DSMCC_MUSIC_APP_TYPE){
		// event dsmccPid value is Audio Pid in Music Case 
		event.application.dsmccPid = application->applicationId;
	}else{
		event.application.dsmccPid = application->dsmccPid;
	}
	event.application.isServiceBound = application->isServiceBound;
	event.application.controlCode = application->controlCode;
	memcpy(event.application.name, application->name, strlen(application->name));	
	event.application.priority = application->priority;
	memcpy(event.application.initialPath, application->initialPath, strlen(application->initialPath));		
	
	dsmcc_log( "[EVENT]#------------------------------------------#\n");				
	dsmcc_log( "[EVENT]\t mainType = %d\n" , mainType);	
	dsmcc_log( "[EVENT]\t rootPath = %s\n" , path);	
	dsmcc_log( "[EVENT]\t event.eventType = %d\n" , event.eventType);
	dsmcc_log( "[EVENT]\t event.serviceId = %d\n", event.serviceId);
	dsmcc_log( "[EVENT]\t event.demux = %d\n", event.demux);
	dsmcc_log( "[EVENT]\t event.uri = %s\n", event.uri);
	dsmcc_log( "[EVENT]\t event.application.appType = 0x%04X\n", event.application.appType);
	dsmcc_log( "[EVENT]\t event.application.organizationId = 0x%04X\n", event.application.organizationId);
	dsmcc_log( "[EVENT]\t event.application.applicationId = 0x%04X\n", event.application.applicationId);
	dsmcc_log( "[EVENT]\t event.application.dsmccPid = 0x%04X\n", event.application.dsmccPid);	
	dsmcc_log( "[EVENT]\t event.application.isServiceBound = 0x%04X\n", event.application.isServiceBound);
	dsmcc_log( "[EVENT]\t event.application.controlCode = 0x%04X\n", event.application.controlCode);	
	dsmcc_log( "[EVENT]\t event.application.name = %s\n", event.application.name);
	dsmcc_log( "[EVENT]\t event.application.priority = 0x%04X\n", event.application.priority);
	dsmcc_log( "[EVENT]\t event.application.initialPath = %s\n", event.application.initialPath);
	dsmcc_log( "[EVENT]#------------------------------------------#\n" );
	
	dsmcc_callback_event(ctx, mainType, path, &event);
}


//--------------------------------------------------------------------------------------

void dsmcc_process_section_filter_data(dsmcc_ctx *ctx, dsmcc_section_data *section)
{
	unsigned char* section_data = ((unsigned char*)section->section_data);
	unsigned int section_data_size = section->section_data_size;
	ctx->user_param = section->section_user_param;
	ctx->done_callback = section->callback;
	dsmcc_process_section(ctx, section_data, section_data_size);
}

void dsmcc_process_application(dsmcc_ctx *ctx, dsmcc_application *application)
{
	dsmcc_log( "[APPLICAITON]#------------------------------------------#\n");				
	dsmcc_log( "[APPLICAITON]\t appType = 0x%04X\n" , application->appType);
	dsmcc_log( "[APPLICAITON]\t organizationId = 0x%04X\n", application->organizationId);
	dsmcc_log( "[APPLICAITON]\t applicationId = 0x%04X\n", application->applicationId);
	dsmcc_log( "[APPLICAITON]\t dsmccPid = 0x%04X\n", application->dsmccPid);
	dsmcc_log( "[APPLICAITON]\t isServiceBound = 0x%04X\n", application->isServiceBound);
	dsmcc_log( "[APPLICAITON]\t controlCode = 0x%04X\n", application->controlCode);
	dsmcc_log( "[APPLICAITON]\t name = %s\n", application->name);
	dsmcc_log( "[APPLICAITON]\t priority = 0x%04X\n", application->priority);
	dsmcc_log( "[APPLICAITON]\t channelNum = %d\n", application->channelNum);	
	dsmcc_log( "[APPLICAITON]\t initialPath = %s\n", application->initialPath);
	dsmcc_log( "[APPLICAITON]#------------------------------------------#\n" );

	struct obj_carousel *cari, *cars;
	int found = 0;
	cari = ctx->carousels;
	for(; cari != NULL; cari = cari->next) {
		if(cari->application.dsmccPid == application->dsmccPid) {
			dsmcc_log("[libdsmcc] application dsmccPid (0x%04X) founded\n", application->dsmccPid);
			if(application->appType == DSMCC_MUSIC_APP_TYPE){
				// in Music Case => Update Audio Pid
				cari->application.applicationId = application->applicationId;
			}			
			cari->application.channelNum = application->channelNum; 
			found = 1;
			ctx->active_carousel = cari;
			break;
		}
	}
	if(found == 0){
		cari = (struct obj_carousel*) calloc(sizeof(unsigned char), sizeof(struct obj_carousel));
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> cari len : %d", sizeof(struct obj_carousel));
#endif

		cari->application.appType = application->appType;
		cari->application.organizationId = application->organizationId;
		cari->application.applicationId = application->applicationId;
		cari->application.dsmccPid = application->dsmccPid;
		cari->application.isServiceBound = application->isServiceBound;
		cari->application.controlCode = application->controlCode;
		if(dsmcc_check_utf_string(application->name) == 0)
			memcpy(cari->application.name, application->name, strlen(application->name));	
		else
			memcpy(cari->application.name, "app", strlen("app"));				
		cari->application.priority = application->priority;
		cari->application.channelNum = application->channelNum;		
		memcpy(cari->application.initialPath, application->initialPath, strlen(application->initialPath));
		cari->cache = NULL;
		cari->filecache = (struct cache*) calloc(sizeof(unsigned char), sizeof(struct cache));
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> cari->filecache len : %d", sizeof(struct cache));
#endif
		cari->gate = NULL;
		cari->id = 0;
		cari->flag = APP_NEW_DOWN;

		// directory name => dsmccPid
		char *buf = (char *) ctx->buffer;
		int buf_len = sprintf(buf, "%d", application->dsmccPid);
		buf[buf_len]='\0';

		dsmcc_cache_init(cari->filecache, buf);

		if(ctx->carousels == NULL) {
			ctx->carousels = cari;
		} else {
			for(cars=ctx->carousels;cars->next!=NULL;cars=cars->next){;}
			cars->next = cari;
		}
		cari->next = NULL;
		ctx->active_carousel = cari;
	}

}


int dsmcc_process(dsmcc_ctx *ctx)
{
	dsmcc_event_list_t *eptr;
	dsmcc_section_data *section;
	dsmcc_application  *application;	

	while ((eptr=dsmcc_dequeue_event(&ctx->dsmcc_event))) {
		switch(eptr->event) {
		case DSMCC_EVENT_APPLICATION_DATA:
			application = (dsmcc_application *)eptr->data;	
			if(!ctx->stop_requested)
				dsmcc_process_application(ctx, application);
			if(application) free(application);
			break;

		case DSMCC_EVENT_SECTION_FILTER_DATA:
			section = (dsmcc_section_data *)eptr->data;
			if(!ctx->stop_requested)
				dsmcc_process_section_filter_data(ctx, section);
			dsmcc_free_section_data(section);					
			break;
		}
		dsmcc_destroy_event_entry(eptr);
	}
	return 0;
}

void *dsmcc_thread_handle(void *arg )
{
	dsmcc_ctx *ctx = (dsmcc_ctx *)arg;
	dsmcc_log("dsmcc_thread_handle: started.. \n");	
	
	ctx->stop_requested = 0;
	dsmcc_log("initializing services\n");	
	
	while(!ctx->stop_requested) {
		pthread_mutex_lock(&ctx->dsmcc_event.lock);
		while(!ctx->dsmcc_event.head && !ctx->stop_requested) {
			pthread_cond_wait(&ctx->dsmcc_event.cond, &ctx->dsmcc_event.lock);
		}
		pthread_mutex_unlock(&ctx->dsmcc_event.lock);
		dsmcc_process(ctx);
	}
	dsmcc_log("dsmcc_thread_handle: ended.. \n");	
	dsmcc_finish(ctx);
	return 0;
}

dsmcc_ctx *dsmcc_init(void *handle, dsmcc_callback callback)
{
	int thread_id = 0;	
	dsmcc_log("initializing dsmcc handler\n");
	dsmcc_ctx *ctx = (dsmcc_ctx *) calloc(sizeof(char), sizeof(dsmcc_ctx));
	if (!ctx){
		dsmcc_log("ccx_dsmcc_init calloc ccx_dsmcc_ctx error not enough memory");
		return 0;
	}

	// Root Folder/dsmcc deleted...
	char *path = (char *) ctx->buffer;
	char dataPath[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);	
	strcat(dataPath, DSMCC_ROOTPATH);
	int path_len = sprintf(path, "rm -r %%ss", dataPath);	
	path[path_len]='\0';

	dsmcc_log("[libdsmcc] %s systemcall\n", path);
	system(path);

	mkdir(dataPath, 0777); 
	chmod(dataPath, 0777);
	
	ctx->stop_requested = 1;

	ctx->callback = callback;
	ctx->handle = handle;

	pthread_mutex_init(&ctx->dsmcc_event.lock, 0);
	pthread_cond_init(&ctx->dsmcc_event.cond, 0);

	pthread_attr_t attr_dsmcc_thread;
	/* create the signal handling thread */
	pthread_attr_init(&attr_dsmcc_thread);
	/* set the thread detach state */
	pthread_attr_setdetachstate(&attr_dsmcc_thread, PTHREAD_CREATE_DETACHED);
	
	thread_id = pthread_create( &ctx->dsmcc_thread, &attr_dsmcc_thread, dsmcc_thread_handle, ctx );
	if (thread_id < 0){
		dsmcc_log("pthread_create dsmcc_process returned: %d\n", thread_id);
	}
	dsmcc_log("initialized dsmcc handler\n");

	return ctx;
}

void dsmcc_destroy(dsmcc_ctx *ctx)
{
	if(!ctx) return;
	
	dsmcc_log("dsmcc_destroy: cleaning up\n");
	ctx->stop_requested = 1;

	pthread_cond_broadcast(&ctx->dsmcc_event.cond);

	dsmcc_log("dsmcc_destroy: destroyed...\n");
}

void dsmcc_finish(dsmcc_ctx *ctx)
{
	struct obj_carousel *car, *carnext;

	if(!ctx) return;

	dsmcc_log("dsmcc_finish: finishing\n");

	car = ctx->carousels;
	while(car){
		carnext = car->next;
		dsmcc_objcar_free(car);
		car = carnext;
	}
	ctx->carousels = NULL;

	dsmcc_free_events(&ctx->dsmcc_event);	
	
	pthread_mutex_destroy(&ctx->dsmcc_event.lock);
	pthread_cond_destroy(&ctx->dsmcc_event.cond);
	
	free(ctx);
	ctx = NULL;
	dsmcc_log("dsmcc_finish: finished...\n");
}


#ifdef __cplusplus
}
#endif

