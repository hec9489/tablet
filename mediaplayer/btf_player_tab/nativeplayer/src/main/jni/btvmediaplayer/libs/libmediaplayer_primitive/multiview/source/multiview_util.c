// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifdef __cplusplus
extern "C" {
#endif


#include <android/log.h>

#include <libavutil/timestamp.h>
#include <pthread.h>

#include "multiview_util.h"
#include "multiview_error.h"


ST_QUEUE_MESSAGE* g_messageQueue = NULL;
pthread_mutex_t   g_mvQueue_mutex = PTHREAD_MUTEX_INITIALIZER;;

#define MVQUEUE_MESSAGE_QUEUE_SIZE      1024        // size of queue


int resetRefCnt(ST_QUEUE_MESSAGE* msgQueue)
{
	pthread_mutex_lock(&g_mvQueue_mutex);
	msgQueue->refCount = 0;
	pthread_mutex_unlock(&g_mvQueue_mutex);
	return 0;
}

int addRefCnt(ST_QUEUE_MESSAGE* msgQueue)
{
	int result = 0;
	pthread_mutex_lock(&g_mvQueue_mutex);
	result = ++msgQueue->refCount;
	pthread_mutex_unlock(&g_mvQueue_mutex);
	return result;
}

int removeRefCnt(ST_QUEUE_MESSAGE* msgQueue)
{
	int result = 0;
	pthread_mutex_lock(&g_mvQueue_mutex);
	result = --msgQueue->refCount;
	pthread_mutex_unlock(&g_mvQueue_mutex);
	return result;
}

void mvQueue_freeCallback(void *msg)
{
	ST_MESSAGE* message = (ST_MESSAGE*)msg;
	av_packet_unref(&message->packet);
	LOGI("message free...  [%s:%d]", __FUNCTION__, __LINE__);
}

int mvQueue_init()
{
	pthread_mutex_init(&g_mvQueue_mutex, NULL);
	return 0;
}

int mvQueue_getInstance(ST_QUEUE_MESSAGE** instance)
{
	int result = 0;
	int refCount = 0;
	LOGI("mvQueue_getInstance start.  (%s : %d)", __FUNCTION__, __LINE__);
	if(instance == NULL){
		LOGE("parameter is invalid.");
		return FFMPEG_ERR_INVAIDDATA;
	}

	pthread_mutex_lock(&g_mvQueue_mutex);

	if(g_messageQueue != NULL)
	{
		pthread_mutex_unlock(&g_mvQueue_mutex);
		refCount = addRefCnt(g_messageQueue);
		*instance = g_messageQueue;
		LOGI("mvQueue instance ref count (%d)", refCount);
		return refCount;
	}

	g_messageQueue = (ST_QUEUE_MESSAGE*)av_malloc(sizeof(ST_QUEUE_MESSAGE));
	if(g_messageQueue == NULL)
	{
		LOGE("ST_QUEUE_MESSAGE alloc failed.");
		pthread_mutex_unlock(&g_mvQueue_mutex);
		return FFMPEG_ERR_NOT_ENOUGHT_MEMORY;
	}

	memset(g_messageQueue, 0x00, sizeof(ST_QUEUE_MESSAGE));

	result = av_thread_message_queue_alloc(&g_messageQueue->msg_queue,
	                              MVQUEUE_MESSAGE_QUEUE_SIZE,
                                  sizeof(ST_MESSAGE));
	if(result < 0)
	{
		LOGE("messagequeue alloc failed.  error(%d)", result);
		pthread_mutex_unlock(&g_mvQueue_mutex);
		return FFMPEG_ERR_NOT_ENOUGHT_MEMORY;
	}

	/**
    * Set the optional free message callback function which will be called if an
    * operation is removing messages from the queue.
    */
	av_thread_message_queue_set_free_func(g_messageQueue->msg_queue,
	                                           &mvQueue_freeCallback);

    pthread_mutex_unlock(&g_mvQueue_mutex);

//	g_mvQueue_mutex = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
	resetRefCnt(g_messageQueue);
	refCount = addRefCnt(g_messageQueue);

	*instance = g_messageQueue;


	LOGI("mvQueue instance ref count(%d) created.", refCount);
	return refCount;
}


int mvQueue_unRefInstance(ST_QUEUE_MESSAGE** queue)
{
	if(queue == NULL)
	{
		LOGE("mvQueue_unRefInstance parameter invalid. queue is null");
		return FFMPEG_ERR_INVAIDDATA;
	}

//	int refCount = --(*queue)->refCount;
	int refCount = removeRefCnt(*queue);
	if( refCount != 0)
	{
		LOGI("mvQueue refCount (%d)", refCount);
		if(refCount < 0)
			LOGE("mvQueue refCount ERROR!!");
		return refCount;
	}

	av_thread_message_queue_free(&(*queue)->msg_queue);

	av_free(*queue);
	*queue = NULL;
	g_messageQueue = NULL;
	LOGI("mvQueue message queue free success.");
	return refCount;
}

int mvQueue_pushMessage(ST_QUEUE_MESSAGE* pQueue, ST_MESSAGE* msg)
{
	if(pQueue == NULL || msg == NULL)
	{
		LOGE("invalid parameter.  queue(0x%x), msg(0x%x)", (unsigned int)pQueue, (unsigned int)msg);
		return FFMPEG_ERR_INVAIDDATA;
	}

//	int result = av_thread_message_queue_send(pQueue->msg_queue, msg, 0);        // blocking
	int result = av_thread_message_queue_send(pQueue->msg_queue, msg, AV_THREAD_MESSAGE_NONBLOCK);        // nonblocking

	// retry 를 해야 하는 경우 전달 처리.
	if (result == AVERROR(EAGAIN)) {
		result = av_thread_message_queue_send(pQueue->msg_queue, msg, 0);        // blocking 처리
		LOGI("av_thread_message_queue_send blocking proc.  result(%d), msg(%s)",
			     result, av_err2str(result));
	} else if (result == MVQUEUE_MSG_FREEZING){
		LOGE("MessageQueue Freezing... (%s:%d)", __FUNCTION__, __LINE__);
	}
	else if (result < 0){
		LOGE("av_thread_message_queue_send failed. error(%d), message(%s)", result,
		     av_err2str(result));
		return FFMPEG_ERR_INTERNAL;
	}

	return result;
}


int mvQueue_popMessage(ST_QUEUE_MESSAGE* pQueue, ST_MESSAGE* msg)
{
	if(pQueue == NULL || msg == NULL )
	{
		LOGE("invalid parameter.  queue(0x%x)", (unsigned int)pQueue);
		return FFMPEG_ERR_INVAIDDATA;
	}

//	int result = av_thread_message_queue_recv(pQueue->msg_queue, msg, 0);        // blocking
	int result = av_thread_message_queue_recv(pQueue->msg_queue, msg, AV_THREAD_MESSAGE_NONBLOCK);        // nonblocking
	if(result < 0)
	{
		if(result == MVQUEUE_MSG_FREEZING)
			LOGE("MessageQueue Freezing... (%s:%d)", __FUNCTION__, __LINE__);
//		else
//			LOGE("av_thread_message_queue_recv failed. error(%d)", result);
		return result;
	}

	return result;
}


int mvQueue_freezing(ST_QUEUE_MESSAGE* pQueue)
{
	if(pQueue == NULL)
		return FFMPEG_ERR_INVAIDDATA;

	av_thread_message_queue_set_err_recv(pQueue->msg_queue, MVQUEUE_MSG_FREEZING);
	av_thread_message_queue_set_err_send(pQueue->msg_queue, MVQUEUE_MSG_FREEZING);
	av_thread_message_flush(pQueue->msg_queue);

	return 0;
}

int mvQueue_unfreezing(ST_QUEUE_MESSAGE* pQueue)
{
	if(pQueue == NULL)
		return FFMPEG_ERR_INVAIDDATA;

	av_thread_message_queue_set_err_recv(pQueue->msg_queue, 0);
	av_thread_message_queue_set_err_send(pQueue->msg_queue, 0);

	return 0;
}

float getsecond(int64_t pts, int denominator){
	return pts / denominator;
}

void packet_check_pts(const AVPacket* pkt, int denominator ){
	static int64_t prev_pts[4] = {0,};
	int64_t ptsgap = pkt->pts - prev_pts[0];
	float second = getsecond(ptsgap, denominator);

	// 1초 이상 차이나는 경우
	if(second > 1){
		LOGE("[outStreamIdx : %d], second : %f, videoPrePts(%lld), prevPts(%lld) curPts(%lld)",
		     pkt->stream_index, second, prev_pts[0], prev_pts[pkt->stream_index], pkt->pts);
	}

	prev_pts[pkt->stream_index] = pkt->pts;

}


void log_packet(const AVFormatContext *fmt_ctx, const AVPacket *pkt)
{
	AVRational *time_base = &fmt_ctx->streams[pkt->stream_index]->time_base;
	LOGI("[outStreamIdx : %d] pts:%s pts_time:%s dts:%s dts_time:%s duration:%s duration_time:%s",
	     pkt->stream_index,
	     av_ts2str(pkt->pts), av_ts2timestr(pkt->pts, time_base),
	     av_ts2str(pkt->dts), av_ts2timestr(pkt->dts, time_base),
	     av_ts2str(pkt->duration), av_ts2timestr(pkt->duration, time_base)
	     );

//	packet_check_pts(pkt, time_base->den);
}

#ifdef __cplusplus
} //extern "C"
#endif