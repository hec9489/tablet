// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <sys/poll.h>
#include <systemproperty.h>

#if 1//def FEATURE_SOC_AMLOGIC
#include <android/native_window.h>
#endif

#include "dvb_primitive.h"

#include <dbg.h>
#include "dvb_table.h"

// FIX
//TODO: move to inside of player
//unsigned char       Data[4][BUFFER_SIZE+PADDING_LENGTH];       /* extra bytes for prepending headers */

AVP_PlayerConfig locator2plyaerConfig(dvb_locator_t locator) {
    AVP_PlayerConfig newPlayerConfig;
    newPlayerConfig.type = AVP_INPUT_MEMORY;

    if(locator.type == DVB_FILE) newPlayerConfig.clock_recovery_enable = true; //newPlayerConfig.mediaType = AVP_FILE;
    else if(locator.type == DVB_VOD) newPlayerConfig.clock_recovery_enable = true; //newPlayerConfig.mediaType = AVP_VOD;
    else newPlayerConfig.clock_recovery_enable = false; //newPlayerConfig.mediaType = AVP_IPTV;

    sprintf((char*) newPlayerConfig.url_address, "%s:%d", locator.tune_param.ip.ip, locator.tune_param.ip.port);
    newPlayerConfig.video_codec = (unsigned short) locator.vcodec;
    newPlayerConfig.audio_codec = (unsigned short) locator.acodec;
    newPlayerConfig.video_pid = (unsigned short) locator.vid;
    newPlayerConfig.audio_pid = (unsigned short) locator.aid;
    newPlayerConfig.pcr_pid = (unsigned short) locator.pcr_pid;
    newPlayerConfig.video_enable = (locator.vcodec != 0);
    newPlayerConfig.audio_enable = (locator.acodec != 0);

    printf("****************************************************************************************\n");
    printf("** url : %s \n", newPlayerConfig.url_address);
    printf("** video_codec:%d, audio_codec:%d, pcr_pid:%d, video_pid:%d, audio_pid:%d \n",
           newPlayerConfig.video_codec, newPlayerConfig.audio_codec,
           newPlayerConfig.pcr_pid, newPlayerConfig.video_pid, newPlayerConfig.audio_pid);
    printf("** video_enable:%d, audio_enable:%d \n", newPlayerConfig.video_enable, newPlayerConfig.audio_enable);
    printf("****************************************************************************************\n");

    //newPlayerConfig.clock_recovery_enable;
    //newPlayerConfig.playback_latency;
    //newPlayerConfig.display_first_frame_early;
    newPlayerConfig.keep_last_frame = (bool) locator.keep_last_frame;

    return newPlayerConfig;
}

int dvb_device_open(dvb_player_t *player, AVP_MediaCallback callback, dvb_locator_t *locator_head)
{
    verbose("%s",__FUNCTION__);
    //yiwoo 0415 skip avp_setsurface when channel change
    static int first_check = 0;
    // jung2604
    player->locator_head = locator_head;

    player->isAvpStart = false;
    player->isFirstFrameDisplayed = false;
    player->EOFReached = false;

    //TODO : create를 매번체크해서 처리한다...create를 한번만 해도 된다면......init으로 이동하자..
#ifdef USE_BTV_HAL_MANAGER
	player->avpInfo.createConfig.callback_user_param = player;
	player->avpInfo.createConfig.function = callback;
	player->avpInfo.createConfig.output = (player->idx_player == 0) ? AVP_VIDEO_MAIN : AVP_VIDEO_PIP;

	int device = player->idx_player;
	if(HalBtv_Create(device, &player->avpInfo.createConfig) != 0) goto error;


#if 1//def FEATURE_SOC_AMLOGIC
#if 1
    int videotunnel = 0;

    {
        //char value[128] = {0};
        //if (property_get("persist.vendor.media.skb.videotunnel", value, "1")) {
        //    if(strcmp(value, "0")==0) videotunnel = 0;
        //}
    }

    if(videotunnel != 1) {
    //if(first_check == 0) {
        //TODO : amlogic 용  --->
        log_info ("=================================================================================================");
        int width = ANativeWindow_getWidth((ANativeWindow *)player->m_pAnative); //1176;//
        int height = ANativeWindow_getHeight((ANativeWindow *)player->m_pAnative); //1920;//

        log_info("BTF|%s|%d| [device:%d] check!! From the ANativeWindow got window %p [%d x %d] \n", __FUNCTION__,__LINE__, device, player->m_pAnative, width, height);
        log_info ("=================================================================================================");


        HalBtv_SetSurface(player->idx_player, player->m_pAnative);  //TODO : amlogic 대응용 필요없을시 제거..
        // TODO : amlogic 용 <---
        first_check = 1;
    }
#endif
#endif
	
#else
    if(player->avpInfo.playerHandle == NULL) {
        player->avpInfo.createConfig.callback_user_param = player;
        player->avpInfo.createConfig.function = callback;
        player->avpInfo.createConfig.output = (player->idx_player == 0) ? AVP_VIDEO_MAIN : AVP_VIDEO_PIP;

        if(AVP_Create(&player->avpInfo.playerHandle, &player->avpInfo.createConfig) != 0) goto error;
    }
#endif

    player->avpInfo.playerConfig = locator2plyaerConfig(*player->locator_head);
#ifdef USE_BTV_HAL_MANAGER
    if(HalBtv_Start(device, &player->avpInfo.playerConfig) != 0) goto error;
#else
    if(AVP_Start(player->avpInfo.playerHandle, &player->avpInfo.playerConfig) != 0) goto error;
#endif

    log_info ("[dvb_device_open] mute in");
    HalBtv_SetAudioMute(player->idx_player, 1);
    log_info ("[dvb_device_open] mute out");

    player->isAvpStart = true;

    //TODO : AV SYNC 모드 설정해야한다!!
    //int ret = dvb_set_av_sync(player);

    //TODO : delay 설정해야한다.
    AVP_AVoffset offset;
    offset.type = AVP_PES_VIDEO;
    offset.delay_time_ms = player->videoDelayTimeMs;
#ifdef USE_BTV_HAL_MANAGER
    HalBtv_SetAVoffset(device, offset);
#else
    AVP_SetAVoffset(player->avpInfo.playerHandle, offset);
#endif

//TODO : AspectRatio 설정해야한다
#ifdef USE_BTV_HAL_MANAGER
	HalBtv_SetScaleRatioMode(player->settingsVideoScaleMode); //AspectRatio
#else
	VCOMPO_SetScaleRatioMode(player->avpInfo.vcompoHandle, player.settingsVideoScaleMode); //AspectRatio
#endif

    //TODO : dolby 설정해야한다.
    player->avpInfo.audioOutputMode = player->enableSettingsDolby;
    HalBtv_SetAudioOutputMode(player->avpInfo.audioOutputMode);

    if(player->idx_player == 0) {
#ifdef USE_BTV_HAL_MANAGER
		HalBtv_SetZorder(GFX_MAIN, GFX_PIP, VID_PIP, VID_MAIN);
#else
        VCOMPO_SetZorder(player->avpInfo.vcompoHandle, GFX_MAIN, GFX_PIP, VID_PIP, VID_MAIN);
#endif
    }

    // only the main player is applied.
    // Closed Caption
    if(player->idx_player == 0 && locator_head->type ==  DVB_IP && !player->isMultiview){
        ccx_dtvcc_init((void*)player);
    }

	log_info ("[dvb_device_open] success");
    return 0;

error:
	log_info ("[dvb_device_open] must be error");
    return -1;
}


int dvb_device_close(dvb_player_t *player)
{
    verbose("%s",__FUNCTION__);

    int ret;

    player->isAvpStart = false;

    // only the main player is applied.
    // Closed Caption
    if(player->idx_player == 0 && player->locator_head != NULL && player->locator_head->type ==  DVB_IP && !player->isMultiview){
        log_info ("[dvb_device_close] before ccx_dtvcc_destroy");
        if(player->dtvcc != NULL){
            ccx_dtvcc_destroy((void*)player);
            player->dtvcc = NULL;
        }
        log_info ("[dvb_device_close] after ccx_dtvcc_destroy");
    }

    player->locator_head = NULL;
    bool enableLastFrame = player->enableLastFrame;
    if(player->idx_player == 0) {
        char buff[256];
        log_info ("[dvb_device_close] before get_systemproperty PROPERTY__LASTFRAME old : %d", enableLastFrame);
        get_systemproperty(PROPERTY__LASTFRAME, buff, 20);
        enableLastFrame = (memcmp(buff, "1", 1) == 0);
        log_info ("[dvb_device_close] after get_systemproperty PROPERTY__LASTFRAME new : %d", enableLastFrame);
    }

    log_info ("[dvb_device_close] before AVP_Stop");
    HalBtv_SetCommand(player->idx_player, COMMAND_CHANGE_CONFIG_LASTFRAME, &enableLastFrame, NULL);

#ifdef USE_BTV_HAL_MANAGER
    long stopTimeMs = 0;
    if ( 0 == clock_gettime( CLOCK_REALTIME, &player->tTempTimeVal)) {
        stopTimeMs = (player->tTempTimeVal.tv_sec * 1000) + (player->tTempTimeVal.tv_nsec / 1000000);
    }
    HalBtv_Stop(player->idx_player);
     {
        if ( 0 == clock_gettime( CLOCK_REALTIME, &player->tTempTimeVal)) {
            player->tempTimeMs = (player->tTempTimeVal.tv_sec * 1000) + (player->tTempTimeVal.tv_nsec / 1000000);
            log_info("**_** fist HalBtv_Stop start ~ end : %ld ms ", player->tempTimeMs - stopTimeMs);
        }
    }
#else
    AVP_Stop(player->avpInfo.playerHandle);
#endif
    log_info ("[dvb_device_close] after AVP_Stop");

	log_info ("[dvb_device_close] success");
    return 0;

error:

	log_info ("[dvb_device_close] must be error");
    return -1;
}

int dvb_tuner_unlock(dvb_player_t *player, dvb_locator_t *locator)
{
    verbose("%s",__FUNCTION__);
    int ret=0;

    switch(locator->type)
    {
        case DVB_IP:
            ret = dvb_iptuner_unlock(player);      //TODO
            break;

        case DVB_FILE:
            ret = dvb_file_unlock(player);      //TODO: add argument
            break;
    }

    return ret;
}

int dvb_wait_tuner_unlock(dvb_player_t *player, dvb_locator_t *locator)
{
    verbose("%s",__FUNCTION__);
    int ret=0;

    switch(locator->type)
    {
        case DVB_IP:
            ret = dvb_wait_iptuner_unlock(player);      //TODO
            break;

        case DVB_FILE:
            ret = dvb_wait_file_unlock(player);      //TODO: add argument
            break;
    }

    return ret;
}

int dvb_tuner_lock(dvb_player_t *player, dvb_locator_t *locator)
{
    verbose("%s",__FUNCTION__);
    int ret=0;

    switch(locator->type)
    {
        case DVB_IP:
            ret = dvb_iptuner_lock(player,locator);
            check(!ret,"failed dvb_iptuner_lock()");
            break;

        case DVB_FILE:
            ret = dvb_file_lock(player,locator);
	   	    check(!ret,"failed dvb_iptuner_lock()");
            break;
    }

    return ret;

error:
    return -1;
    
}

#include <sys/syscall.h>

int dvb_wait_for_video_event_fistframe(dvb_player_t *player)
{
    struct video_event          VideoEvent;
    struct pollfd               PollFd[1];
	int nRet = 0;
	int nPollCount = 0;

#if 1 // TODO : jung2604 : 구현을 해보자.. 최종적으로는 callback에서 사용하는것으로 변경해야한다.
    //TODO : jung2604 : 콜백으로 구현해보자.. 현재는  player.isFirstFrameDisplayed의 값으로 판단한다.

    //player->isFirstFrameDisplayed = 0;

    while (player->nPollingRunFlag) {
        if(nPollCount > 0) usleep(20000);

        if (player->isFirstFrameDisplayed == 0) { // case time out
            nPollCount++;
            if (nPollCount >= 250 || player->avpInfo.isFeedBufferEOF == true || player->EOFReached) {
                log_info("[fistframe][%d] time out,  player : 0x%x", player->idx_player, player);
                return -1; // 10sec
            }
        } else {
            log_info("[fistframe][%d] got the first frame on decode, player : 0x%x", player->idx_player, player);

            if (player->locator_head != NULL && !player->save_mute_enable ) {
                int isUnmute = TRUE;

                if(player->locator_head->type != DVB_IP ) {
                    if (player->isRtspSeekMode) { //rtsp seek중일때는...소리를 켜지 않는다.
                        isUnmute = FALSE;
                    }
                }

                if(isUnmute) {
                    //usleep(100000);
                    log_info ("[iframe] mute in");
                    HalBtv_SetAudioMute(player->idx_player, 0);
                    log_info ("[iframe] mute out");
                }
            }
            return 2;
        }

    }

#else
#endif

    return -1;
}

int dvb_init_player(dvb_player_t *player, int idx_player,int jitter,int non_tunneled)
{
    verbose("%s",__FUNCTION__);

    player->idx_player  = idx_player;
    player->enable_audio   = true;
    player->enable_video   = true;
    player->enable_clock_recovery = true;

    player->cbHandler = NULL;
    player->cbCASDescramble = NULL;
    player->EOFReached = false;
    player->TerminateFilePlay = false;
    player->cbQiCalcScan = NULL;
    player->cbQiCalcOnJoinRequested = NULL;

    player->save_mute_enable = false;

    return 0;

error:
    return -1;
}


#ifdef __cplusplus
} //extern "C"
#endif
