// "ZooVodClientAPI.h"
// Copyright (c) 2008 Zooinnet, Ltd.  All rights reserved.
// Header file for using "zoovod" shared library API


#ifndef _ZOOVOD_CLIENT_API_H
#define _ZOOVOD_CLIENT_API_H


#include <stdint.h>

#include <stdio.h>
#include <assert.h>
#include <getopt.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <net/if.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <netdb.h>
#include <pthread.h>
#include <signal.h>
#include <string.h>

#include <sys/resource.h>

#include <syslog.h>
#include <sched.h>
#include <stdarg.h>
#include <sys/poll.h>
#include <sys/stat.h>

#include <errno.h>
#include <stdlib.h>
#include <time.h>


#ifdef __cplusplus
extern "C"
{
#endif


/////////////////////////////////
///////	Macro definitions ///////
/////////////////////////////////

// Log levels

#define LOGISQMS 		0    
#define LOGNONE			1
#define LOGWARNING		3
#define LOGERR			2
#define LOGNOTICE 		4
#define LOGDEBUG		5

// ISQMS MODE
#define SERVICE	0
#define DEBUG	1


//VoD macros

/*! \def TRANSPORT_SYNC_BYTE
    \brief MPEG2-TS의 동기화 Byte code 
 */
#define TRANSPORT_SYNC_BYTE		0x47

/*! \def RECEIVE_BUFFER_LEN
    \brief RTP데이터 수신을 위한 최대 버퍼 크기
 */
#define RECEIVE_BUFFER_LEN		20480

/*! \def MAX_IP_LEN
    \brief IP버퍼 크기
 */
#define MAX_IP_LEN				32

/*! \def RTSP_DEFAULT_PORT
    \brief RTSP기본 포트(554번) 
 */
#define RTSP_DEFAULT_PORT		554

/*! \def MAX_URL_LEN
    \brief URL버퍼 크기
 */
#define MAX_URL_LEN				256

/*! \def MPEG2_TS_SIZE
    \brief MPEG2-TS의 1개의 패킷화된 형태의 스트림 크기
 */
#define MPEG2_TS_SIZE           188L

/*! \def MAX_SDP_CONTROL_LEN
    \brief SDP Control길이
 */
#define MAX_SDP_CONTROL_LEN		10

/*! \def TINY_BUFFER_LEN
    \brief RTSP RR(Receiver report)를 전송하기 위한 주기(단위: 초) 
*/
#define TINY_BUFFER_LEN			512

/*! \def RTCP_RR_INTERVAL
    \brief RTSP RR(Receiver report)를 전송하기 위한 주기(단위: 초) 
 */
#define RTCP_RR_INTERVAL		5	// 5 seconds 

/*! \def READ_TIMEOUT
    \brief Socket read timeout
 */
#define READ_TIMEOUT			25


//Error Definitions
#define E_INVARG 		-201	// Invalid argument
#define E_VODCONNFAILED -202	// Connection with relay failed after time-out
#define E_UNKNOWN 		-1		// Unknown Error


/////////////////////////////////
///////	Type Definitions  ///////
/////////////////////////////////

//Session status information
typedef enum SESSION_STATUS
{
	STATUS_NONE			= 0x00, // 상태 없음.
	STATUS_PLAYING		= 0x01, // PLAY상태
	STATUS_PAUSE		= 0x02, // PAUSE상태
	STATUS_TEARDOWN		= 0x03, // TEARDOWN상태
	STATUS_FFFR			= 0x04,
	STATUS_CLOSED,				// SESSION CLOSED!			
} SESSION_STATUS;


//Event Code definition
typedef enum EVENT_CODE
{
	EVENT_NONE			= 0x00,	// 이벤트 없음.(다른 이벤트 처리 가능)
	EVENT_PAUSE			= 0x01, // PAUSE이벤트
	EVENT_CONTINUE		= 0x02, // CONTINUE이벤트 (SESSION_STATUS상태가 STATUS_PAUSE이어야 함)
	EVENT_TEARDOWN		= 0x03, // TEARDOWN이벤트 (중지) (SESSION_STATUS상태에 영향 받지 않음.)
	EVENT_RANDOM_SEEK	= 0x04, // RANDOM-SEEK이벤트 (SESSION_STATUS상태가 STATUS_PLAYING이어야 함)
	EVENT_FAST_FRWD		= 0x05,
	EVENT_FAST_RWD		= 0x06,
	EVENT_SPEED_SET		= 0x07,	
} EVENT_CODE;

//Return Code definition
typedef enum RETURN_CODE
{
	RET_SUCCESS			= 0,	
	RET_TEARDOWN		= 1, 	
	RET_RTCP_BYE		= 3,
	RET_URL_ERR			= 100,
	RET_SERVER_CONERR	= 101,
	RET_INCORRECT_FILE	= 102,
	RET_SOCKET_ERR		= 105,
	RET_INCORRECT_MSG	= 201,
	RET_INCORRECT_SCALE	= 205,
	RET_INCORRECT_TIME	= 206,
	RET_PLAY_ORDER_ERR	= 207,
	RET_PAUSE_ERR		= 208,
	RET_FF_FR_ERR		= 209,
	RET_UNKNOWN			= 300,
	RET_BAD_REQUEST_ERR		= 400, // TODO : jung2604 : 없어서 추가함..
	RET_AUTH_ERR		= 401,
	RET_ERR_SYNCBYTE	= 30101,
	RET_ERR_THREAD_DUPLICATION	= 30201,
} RETURN_CODE;

//Type to hold session details

typedef enum RTCP_PAYLOAD_TYPE  RTCP_PAYLOAD_TYPE;

typedef enum RTCP_PAYLOAD_TYPE
{
	RTCP_SENDER_REPORT 		= 200, // Sender report
	RTCP_RECEIVER_REPORT 	= 201, // Receiver report
	RTCP_SOURCE_DESCRIPTION = 202, // Source description
	RTCP_BYE 				= 203, // Bye
	RTCP_APPLICATION_SPECIFIC = 204, // Application specific
	RTCP_FR_END 			= 205,
	RTCP_CONSYNC_ERR 		= 206,
	RTCP_UNKNOWN 			= 207,
} TCP_PAYLOAD_TYPE;

typedef struct Content_info 	// pid, codec information
{
	int pid;
	int codex_id;
	//struct pes_info *next;	
} Content_info_t;


typedef struct _VodClient_Session_t VodClient_Session_t;

//=======================================================>
// ISQMS DATA INFORMATIONS (not used)
//=======================================================>

typedef enum MODE_SET_CODE
{
	SET_ALL_ENABLE			=0,
	SET_ALL_DISABLE			=1,
	SET_JITTER_ENABLE		=2,
	SET_JITTER_DISABLE		=3,
	SET_RTT_ENABLE			=4,
	SET_RTT_DISABLE			=5,
	SET_BITRATE_ENABLE		=6,
	SET_BITRATE_DISABLE		=7,
	SET_SCONSTATE_ENABLE	=8,
	SET_SCONSTATE_DISABLE	=9,
	SET_ERRREPORT_ENABLE	=10,
	SET_ERRREPORT_DISABLE	=11,
} SET_CODE;

typedef enum REPORT_COMMAND_CODE
{
	ZVOD_RTT					= 0,
	ZVOD_PRSERR_RTT_CMDERR		= 1,	
	ZVOD_SCONSTATE				= 2,
	ZVOD_PRSERR_SCON_URLERR		= 3,
	ZVOD_SCONSTATE_CONFULL		= 4,
	ZVOD_PRSERR_SCON_CMDERR		= 5,
	ZVOD_JITTER					= 6,
	ZVOD_NETERR_JITTER_CMDERR	= 7,
	ZVOD_NETERR_JITTER_DELAYERR	= 8,
	ZVOD_BITRATE				= 9,
	ZVOD_NETERR_BRATE_CRI		= 10,
	ZVOD_NETERR_BRATE_TCRI		= 11,
	ZVOD_NETERR_SDK_NETERR		= 12,
	ZVOD_CONDITION				= 13,
} REPORT_CODE;

/** Not Used
int znsa_init(VodClient_Session_t *_pSession,int mode);
int znsa_close();
int znsa_reset_session(VodClient_Session_t *_pSession);
int znsa_close_session();
int znsa_set_condition(SET_CODE all,SET_CODE jitter,SET_CODE rtt,SET_CODE bitrate,SET_CODE sconstate,SET_CODE errreport);
void znsa_get_rtt();
void znsa_get_jitter();
void znsa_get_con();
void znsa_get_stream_bitrate();
void znsa_get_error();
void znsa_get_condition();
void znsa_get_condition_by_buff(char *buff); // len value should be buff size, it must be 5
int znsa_set_RTTConfig(int iRTTtime, int iRTTCnt);
int znsa_set_SConConfig(int iSContime);
int znsa_set_JitterConfig(float fJitterThsd);
int znsa_set_BitrateConfig(int iConUnderlimit_sd, int iConUnderlimit_hd, int iConOverlimit_sd, int iConOverlimit_hd); // each limite value are MB/s
void znsa_get_RTTConfig(int *iRTTtime,int *iRTTCnt);
void znsa_get_SConConfig(int *iSContime);
void znsa_get_JitterConfig(float *fJitterThsd);
void znsa_get_BitrateConfig(int *iConUnderlimit_sd, int *iConOverlimit_sd, int *iConUnderlimit_hd, int *iConOverlimit_hd,float *fThreshold);
**/

//=======================================================>


struct _VodClient_Session_t
{
	int    	sockfd;					// socket id
	int    	fIdentifier;			// ssession id (개발자 임의 지정) 하나의 프로세스에서 다른 클라이언트 세션과 중복되면 안됨.	
	int    	session;				// 세션  id
	char   	ip[MAX_IP_LEN];			// Connent IP
	int    	port;					// Port Number
	char   	Test_SerIP[MAX_IP_LEN];	// Config file's Connent IP
	int    	Test_SerPort;			// Config file'sPort Numb
	int 	Test_SerSet;			//Config file exit checking
	char   	url[MAX_URL_LEN];		// rtsp url
	float 	fDuration;				// Play File Duration Time Value
	int    	fKeepAliveTime;			// Network keep alive time(단위: 초) PAUSE상태에서도 keep alive는 동작합니다. PAUSE가 keep alive time이상 지속되면 연결을 끊습니다.
	uint32_t senderSSRC;			// SSRC of Server(sender ssrc)
	unsigned long ulCSeq;			// RTSP를 위한 seq
	int    	fPkt_Recv_Timeout;		// Recv Time Out Value
	int    	fRecv_Timeout_During_Pause; // Pause Time Out Value
	int    	FileState;				// File Begin or End Info
	int	  	control_offset;			// Ctrl Offset
	int    	iPlayType;				// Real Time Only Trick Play 지원여부
	float  	fScale;					// Request Scale Real Time Only
	float  	fSpeed;					// Request speed B&P Only
	
	EVENT_CODE EventCode;			// Request Event Code	
	char       szEventSpec[1024]; 	// Event Message
	SESSION_STATUS sessionStatus;  	// session state	 
	
	//SDP info
	Content_info_t vc_info[8];		// Video pid, Codec Info
	Content_info_t ac_info[8];		// Audio pid, Codec Info
	int 	ifr_status;				// Index Status
	unsigned long long first_ptsinfo;	// First PTS
	
#ifdef PTS_DEBUG	
	//20130805 add for PTS Debug
	unsigned long long	nPTSValue;
	//
#endif

	int 	iDRM_BuffSize;			// DRM Size
	int	  	DRM_exist;				// DRM or no DRM
	unsigned char  drm_info[1024];	// DRM string

	pthread_mutex_t sync_mutex;		// Mutex Handler

	// Global info  SDK Internal use			
	int  	geventCode;				
	int	  	iNew_stream_info;
	int 	rtsp_msg_status;
	int 	RecvData_set;
	float	fSeek_time;
				
	// configuration data (zconfig.conf)
	int 	sock_recv_buffsize;		// Socket Receive Queue Size
	int  	iRecv_count;			// Packet Counter on time send to OnRecvData
	int	  	iRecv_sleep;			// usleep between RTP Receive Loop 
	int	  	TOS_set;				// TOS Set Flag
	int 	loglevel_set;			// Log Level
	int	  	iPause_counter;			// Pause timeout counter
	
	// CUG information
	int 	iServerType;			// CUG Server Type 

	// ISQMS Not Used
	int 	iRTTtime;
	int 	iRTTCnt;				// KJG zPlay Thread Running flag
	char 	cRTTTarget[30];
	int 	iConUnderlimit_sd;
	int 	iConUnderlimit_hd;
	int 	iConOverlimit_sd;
	int 	iConOverlimit_hd;
	int 	iBratetime;
	int 	iBitratecheckingclock;
	float 	fJitterThsd;
	int 	iSContime;
	int 	iJitterDBGReportTime;
	// ISQMS Not Used
		
	int (*OnRecvData)(const VodClient_Session_t * _pSession, const int fIdentifier, unsigned char *buffer, int n);	// RTP데이터가 도착하면 call-back		
	int (*OnRecvEvent)(const VodClient_Session_t * _pSession,EVENT_CODE eRetCode);			// RTCP Event Message call-back
	int (*OnPlayEvent)(const VodClient_Session_t * _pSession, const int fIdentifier);		// Play Event Message Eof or Bof
	int (*OnPlayReturn)(const VodClient_Session_t * _pSession, RETURN_CODE eRetCode); 		// Return Error or Closing Status Message 
	int (*OnFREND)(const VodClient_Session_t * _pSession, RTCP_PAYLOAD_TYPE eRetCode); 		// Return Error or Closing Status Message 
	int (*OnznsaReport)(REPORT_CODE Command,char *reportMSG);
};


////////////////////////////////////
///////	API Function definitions ///////
////////////////////////////////////

extern int InitClient(VodClient_Session_t* _pSession); 		// init session API
extern int Setup(VodClient_Session_t* _pSession); 			// begin API at first call
extern int Play(VodClient_Session_t* _pSession ,float _fStartTime ); // begin API at Second call
extern int Pause(VodClient_Session_t* _pSession ); 			// Nomal play, Trick-Play use
extern int Continue(VodClient_Session_t* _pSession ); 		// Nomal play, Trick-Play use
extern int Seek(VodClient_Session_t* _pSession ,float _fSeekTime ); // Seeking
extern int Slow(VodClient_Session_t* _pSession ,float _fSpeed ); 	// 0.1 <= _fSpeed <= 1
extern int FastTransmission(VodClient_Session_t* _pSession ,float _fSpeed ); // 1 <= _fSpeed <= 4
extern int Teardown(VodClient_Session_t* _pSession ); 		// Stop API
extern int time_FastForward( VodClient_Session_t* _pSession /*= NULL*/,float _fSpeed /*= 0*/,float _fSeekTime ); // _fSpeed support 4x , _fSeelTime is pts
extern int time_Rewind( VodClient_Session_t* _pSession 		/*= NULL*/,float _fSpeed /*= 0*/,float _fSeekTime ); 	// _fSpeed support 4x , _fSeelTime is pts
extern int RecvData_ON( VodClient_Session_t* _pSession);	// RTP Packet Receiving
extern int RecvData_OFF( VodClient_Session_t* _pSession);	// RTP Packet don't Receive

#ifdef __cplusplus
};
#endif

#endif

