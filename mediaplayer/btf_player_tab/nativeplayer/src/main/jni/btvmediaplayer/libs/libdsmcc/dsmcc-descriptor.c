// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#include <stdlib.h>
#include <string.h>
#include "dsmcc-descriptor.h"
#include "dsmcc.h"

void
dsmcc_desc_free(struct descriptor *desc) {
	switch(desc->tag) {
		case TS_CAROUSEL_DT_Type:
		{
			struct descriptor_type *type = &desc->data.type;
			if(type->text){
				free(type->text);
				type->text = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
				dsmcc_log("[MEMINFO] free   -> descriptor->type->text");
#endif
			}
		}
		break;
		case TS_CAROUSEL_DT_Name:
		{
			struct descriptor_name *name = &desc->data.name;
			if(name->text){
				free(name->text);				
				name->text = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
				dsmcc_log("[MEMINFO] free   -> descriptor->name->text");
#endif
			}
		}
		break;
		case TS_CAROUSEL_DT_Info:
		{
			struct descriptor_info *info = &desc->data.info;
			if(info->text){
				free(info->text);				
				info->text = NULL;				
#ifdef DSMCC_MALLOC_PRINT_DEBUG
				dsmcc_log("[MEMINFO] free   -> descriptor->info->text");
#endif
			}
		}
		break;
	}	
	free(desc);	
#ifdef DSMCC_MALLOC_PRINT_DEBUG
	dsmcc_log("[MEMINFO] free   -> descriptor");
#endif

}

void
dsmcc_desc_process_type(unsigned char *data, struct descriptor *desc) {
	struct descriptor_type *type = &desc->data.type;
	if(desc->len > 0){
		type->text = (char *)calloc(sizeof(unsigned char), desc->len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> descriptor->type->text len : %d", desc->len);
#endif
		memcpy(type->text, data, desc->len);
	}
}

void
dsmcc_desc_process_name(unsigned char *data, struct descriptor *desc) {
	struct descriptor_name *name = &desc->data.name;
	if(desc->len > 0){
		name->text = (char *)calloc(sizeof(unsigned char), desc->len);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> descriptor->name->text len : %d", desc->len);
#endif
		memcpy(name->text, data, desc->len);
	}
}

void
dsmcc_desc_process_info(unsigned char *data, struct descriptor *desc) {
	struct descriptor_info *info = &desc->data.info;

	memcpy(info->lang_code, data, 3);
	if((desc->len - 3) > 0){
		info->text = (char *)calloc(sizeof(unsigned char), desc->len - 3);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] calloc -> descriptor->info->text len : %d", desc->len - 3);
#endif
		memcpy(info->text, data+3, desc->len - 3);
	}
}

void
dsmcc_desc_process_modlink(unsigned char *data, struct descriptor *desc) {
	struct descriptor_modlink *modlink = &desc->data.modlink;

	modlink->position = data[0];
	modlink->module_id = (data[1] << 8) | data[2];

#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("Info -> Module -> Descriptor -> Modlink -> Position = %d\n", modlink->position);
	dsmcc_log("Info -> Module -> Descriptor -> Modlink -> Mod Id = %d\n", modlink->module_id);
#endif
}

void
dsmcc_desc_process_crc32(unsigned char *data, struct descriptor *desc) {
	struct descriptor_crc32 *crc32 = &desc->data.crc32;

	crc32->crc = (data[0] << 24) | (data[1] << 16) | (data[2] << 8) |
			data[3];

#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("Info -> Module -> Descriptor -> CRC32 -> CRC = %ld\n", crc32->crc);
#endif
}

void
dsmcc_desc_process_location(unsigned char *data, struct descriptor *desc) {
	struct descriptor_location *location = &desc->data.location;
	location->location_tag = data[0];
#ifdef DSMCC_PRINT_DEBUG	
	dsmcc_log("Info -> Module -> Descriptor -> Location -> Tag = %d\n", location->location_tag);
#endif
}

void
dsmcc_desc_process_dltime(unsigned char *data, struct descriptor *desc) {
	struct descriptor_dltime *dltime = &desc->data.dltime;
	dltime->download_time = (data[0] << 24) | (data[1] << 16) |
				(data[2] << 8 ) | data[3];
#ifdef DSMCC_PRINT_DEBUG	
	dsmcc_log("Info -> Module -> Descriptor -> Download -> Time = %ld\n", dltime->download_time);
#endif
}

void
dsmcc_desc_process_grouplink(unsigned char *data, struct descriptor *desc) {
	struct descriptor_grouplink *grouplink = &desc->data.grouplink;

	grouplink->position = data[0];
	grouplink->group_id = (data[1] << 24) | (data[2] << 16) |
			      (data[3] << 8 ) | data[4];

#ifdef DSMCC_PRINT_DEBUG	
	dsmcc_log("Info -> Module -> Descriptor -> Grouplink -> Position = %d\n", grouplink->position);
	dsmcc_log("Info -> Module -> Descriptor -> Grouplink -> Group Id = %ld\n", grouplink->group_id);
#endif

}

void
dsmcc_desc_process_compressed(unsigned char *data, struct descriptor *desc) {
	struct descriptor_compressed *compressed = &desc->data.compressed;

	compressed->method = data[0];
	compressed->original_size = (data[1] << 24) | (data[2] << 16) |
				    (data[3] << 8)  | data[4];

#ifdef DSMCC_PRINT_DEBUG	
	dsmcc_log("Info -> Module -> Descriptor -> Compressed -> Method = %d\n", compressed->method);
	dsmcc_log("Info -> Module -> Descriptor -> Compressed -> Size = %ld\n", compressed->original_size);
#endif

}

/*
void
dsmcc_desc_process_pirvate(unsigned char *data, struct descriptor *desc) {
	struct descriptor_private *private = &desc->data.private;

	private = (struct descriptor_private*)calloc(sizeof(unsigned char), sizeof(struct descriptor_private));

	private->descriptor = (char *)calloc(sizeof(unsigned char), desc->len);
	memcpy(private->descriptor, data, desc->len);

	return private;
}
*/

struct descriptor *
dsmcc_desc_process_one(unsigned char *data, int *offset) {
	struct descriptor *desc;
	int off=0;

	desc = (struct descriptor*)calloc(sizeof(unsigned char), sizeof(struct descriptor));
#ifdef DSMCC_MALLOC_PRINT_DEBUG
	dsmcc_log("[MEMINFO] calloc -> descriptor len : %d", sizeof(struct descriptor));
#endif

	desc->tag = data[0];
	desc->len = data[1];

	off += 2;
#ifdef DSMCC_PRINT_DEBUG
	dsmcc_log("Info -> Module -> Descriptor -> Tag = %d\n", desc->tag);
	dsmcc_log("Info -> Module -> Descriptor -> Length = %d\n", desc->len);
#endif
	switch(desc->tag) {
		case TS_CAROUSEL_DT_Type:
			dsmcc_desc_process_type(data+2, desc);
			break;
		case TS_CAROUSEL_DT_Name:
			dsmcc_desc_process_name(data+2, desc);
			break;
		case TS_CAROUSEL_DT_Info:
			dsmcc_desc_process_info(data+2, desc);
			break;
		case TS_CAROUSEL_DT_ModuleLink:
			dsmcc_desc_process_modlink(data+2, desc);
			break;
		case TS_CAROUSEL_DT_CRC32:
			dsmcc_desc_process_crc32(data+2, desc);
			break;
		case TS_CAROUSEL_DT_Location:
			dsmcc_desc_process_location(data+2, desc);
			break;
		case TS_CAROUSEL_DT_DownloadTime:
			dsmcc_desc_process_dltime(data+2, desc);
			break;
		case TS_CAROUSEL_DT_GroupLink:
			dsmcc_desc_process_grouplink(data+2, desc);
			break;
		case TS_CAROUSEL_DT_Compressed:
			dsmcc_desc_process_compressed(data+2, desc);
			break;
		default:
			break;
/*
			if(desc->tag >= 0x80 && 
					desc->tag <= 0xFF)
				dsmcc_desc_process_private(data+2, desc);
			}  else if( MHP tag ) 
*/
	}

	off+= desc->len;

	*offset += off;

	return desc;

}

struct descriptor *
dsmcc_desc_process(unsigned char *data, int data_len, int *offset){
	int index = 0;
	struct descriptor *desc, *list, *l;
	
	desc = list = NULL;

	while(data_len > index) {
#ifdef DSMCC_PRINT_DEBUG
		dsmcc_log("data_len %d Index %d\n", data_len, index);
#endif
		desc = dsmcc_desc_process_one(data+index, &index);
		if(list == NULL) {
			list = desc;
			list->next = NULL;
		} else {
			for(l=list;l->next!=NULL;l=l->next) { ; }
			l->next = desc;
			desc->next = NULL;
		}
	}

	*offset += index;

	return list;
}

