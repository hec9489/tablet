// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#include <pthread.h> 
#include "demux.h"

/******************************************************************************
 * DEFINE
 *****************************************************************************/

/******************************************************************************
 * LOCAL FUCTION  
 *****************************************************************************/

static void sDEMUX_SectionCompleted(demux_context *handle, section_filter *filter)
{    
    if (filter) {
        if (filter->buffer.curr_len >= filter->buffer.sect_len) {
            local_buffer buf;
            buf.buffer = &(filter->buffer.buf[0]);
            buf.length = filter->buffer.sect_len;

            LOG("sDEMUX_SectionCompleted : pid(%d) tid(%x) len(%d) cur_len(%d)\n", filter->pid, filter->tid,buf.length, filter->buffer.curr_len);
            filter->callback(handle, filter->pid, filter->tid, &buf, filter->user_param);
        }
    }
}

static uint32_t sDEMUX_ParseSectionHeader(uint8_t *data, section_header *sect_hdr)
{
    sect_hdr->table_id                 = data[0];
    sect_hdr->section_syntax_indicator = (data[1] >> 7) & 0x01;
    sect_hdr->private_indicator        = (data[1] >> 6) & 0x01;
    sect_hdr->section_length           = ((uint16_t)(data[1]&0x0f) << 8) | data[2];
    sect_hdr->transport_stream_id      = ((uint16_t)data[3] << 8) | data[4];
    sect_hdr->version_number           = (data[5] >> 1) & 0x1f;

    return SECTION_HDR_LEN;
}

static bool sDEMUX_WriteSectionData(demux_context *handle, section_filter *filter, 
                                  uint8_t *data, uint32_t data_size, uint8_t is_start)
{

    if(handle == NULL || filter == NULL){
        LOGE("handle or filter is NULL");
        return false; 
    }

    pthread_mutex_lock(&(handle->section_table_mutex));
    CHECK_DEMUX_ALIVE(handle);
    
    if (is_start) {
        memset((void*)&filter->buffer, 0x00, sizeof(section_buffer));
        if(data_size < SECTION_MAX_LEN){
            memcpy(&(filter->buffer.buf[0]), data, data_size);
            filter->buffer.curr_len = data_size;
		}
    }
    else {
        if (filter->buffer.curr_len > 0) {
            if(filter->buffer.curr_len + data_size < SECTION_MAX_LEN){			
                memcpy(&filter->buffer.buf[filter->buffer.curr_len], data, data_size);
                filter->buffer.curr_len += data_size;
          	}
        }
    }

    if (!filter->buffer.sect_len && (filter->buffer.curr_len >= SECTION_HDR_LEN)) {
        section_header sect_hdr;
        memset(&sect_hdr, 0x00, sizeof(section_header));
        sDEMUX_ParseSectionHeader(&filter->buffer.buf[0], &sect_hdr);         
        filter->buffer.sect_len = sect_hdr.section_length + 3;
    }       
    pthread_mutex_unlock(&(handle->section_table_mutex));

    sDEMUX_SectionCompleted(handle, filter);
    
    return true;
}

static uint16_t inline sDEMUX_Get16Bits(uint8_t *buffer, uint16_t mask)
{
    return (((((uint16_t) buffer[0]) << 8) + buffer[1]) & mask);
}

static bool inline sDEMUX_StreamType_IsAIT(uint8_t stream_type)
{
    return stream_type == 0x05;
}

static bool inline sDEMUX_StreamType_IsDataCarousel(uint8_t stream_type)
{
    return stream_type == 0x0b || stream_type == 0x0d;
}

static bool inline sDEMUX_StreamType_IsEventMessage(uint8_t stream_type)
{
    return stream_type == 0x0c || stream_type == 0x0d;
}

static bool inline sDEMUX_StreamType_IsMPE(uint8_t stream_type)
{
    return stream_type == 0x0a;
}

static bool inline sDEMUX_StreamType_IsObjectCarousel(uint8_t stream_type)
{
    switch (stream_type) {
        case 0x06:
        case 0x0a:
        case 0x0c:
        case 0x0d:
        case 0x7e:
            return true;
        default:
            return false;
    }
}

static bool inline sDEMUX_StreamType_IsVideo(uint8_t stream_type)
{
    switch (stream_type) {
        case 0x01: //MPEG-1 video
        case 0x02: //MPEG-2 video
        case 0x10: //MPEG-4 video
        case 0x1B: // H.264
        case 0x24 : // H.265
            return true;
        default:
            return false;
    }
}

/******************************************************************************
 * GLOBAL FUCTION  
 *****************************************************************************/
uint32_t DEMUX_SectionAddData(demux_context *handle, ts_header *ts_hdr, 
                           uint8_t *data, uint32_t size, section_filter *filter)
{
    uint32_t used = 0;
    uint8_t is_start = ts_hdr->payload_unit_start_indicator;

    if (is_start == 1) {
        uint8_t ptr_len = data[used++]; 
        if (ptr_len) {
            if (ptr_len > (size - used)) {
                LOGE("pointer_fileld(%d) > data length.(%d)\n", ptr_len, size);
                goto err;
            }
            sDEMUX_WriteSectionData(handle, filter, &data[used], ptr_len, !is_start);
            used += ptr_len;
        }
    }

    if (used < size) {
        sDEMUX_WriteSectionData(handle, filter, &data[used], (size-used), is_start);
        used += size;
    }
    return used;
err:
    return size;       
}

void DEMUX_PATCallback(void *handle, uint16_t pid, uint16_t tid,local_buffer* buf, void* user_parma){
    uint16_t          prog_number;
    uint16_t          section_len;
    uint16_t          offset;
    uint16_t          pmt_pid;  
    uint8_t           *data = buf->buffer;
    demux_context *demux =  (demux_context *)handle;
    section_filter      sect ;

    /* once parse PAT */    
    sect.pid = pid;
    sect.tid = tid;
    DEMUX_ClearSectionFilter(demux, &sect);    

    offset = 8;
    section_len = sDEMUX_Get16Bits(&(data[1]), SECTION_LEN_MASK) + 3 - (offset + 4);

    LOG("call DEMUX_PATCallback : pid=%d, tid=%x user_parma=%p\n",pid, tid, user_parma);
    while (section_len >= 4) {
        prog_number = sDEMUX_Get16Bits(&(data[offset]), 0xFFFF);
        if (prog_number != 0) {
            if (demux->program_count + 1 <= MAX_PROGRAM_COUNT) {

                pmt_pid = sDEMUX_Get16Bits(&(data[offset + 2]), 0x1FFF);
                demux->program[demux->program_count].program_number = prog_number;                
                demux->program[demux->program_count].pid = pmt_pid;                

                /* if pid is 0, then Network sevice id, else PMT pid */
                if(pmt_pid > 0){
                    section_filter sect; 
                    sect.pid = pmt_pid;
                    sect.tid = PMT_TID;
                    sect.callback= DEMUX_PMTCallback;
                    demux->pmt_count++;
                    DEMUX_SetSectionFilter(handle, &sect);
                }
                LOG("call DEMUX_PATCallback : program number=%d PMT pid=%d\n",prog_number, pmt_pid);                
                demux->program_count++;
            } else {
                LOGE("DEMUX_PATCallback: can not get PMT info");
                return;
            }
        }
        if (section_len >= 4) {
            offset      += 4;
            section_len -= 4;
        }
    }
}

void DEMUX_PMTCallback(void *handle, uint16_t pid, uint16_t tid,local_buffer* buf, void* user_parma){

    uint16_t           section_len;
    uint16_t           offset;  
    uint16_t           es_info_len;
    uint8_t             stream_type;    
    uint16_t           elementary_pid;  
    demux_context *demux =  (demux_context *)handle;
    uint8_t             *data = buf->buffer;

    LOG("call DEMUX_PMTCallback : pid=%d, tid=%x , user_parma=%p\n",pid, tid, user_parma);

    /* once parse PMT */
    if(demux->pmt_count > 0){
        section_filter sect ;
        sect.pid = pid;
        sect.tid = tid;  
        DEMUX_ClearSectionFilter(demux, &sect);
    }

    /* ES Info (ES pid/type)*/
    offset      = 12 + sDEMUX_Get16Bits(&(data[10]), 0x0FFF);
    section_len = sDEMUX_Get16Bits(&(data[1]), SECTION_LEN_MASK) + 3
                  - (offset + 4);

    while (section_len >= 5) {
        if (demux->es_count + 1 <= MAX_ES_COUNT) {
            stream_type = data[offset];
            demux->es_info[demux->es_count].stream_type = stream_type;
            elementary_pid = sDEMUX_Get16Bits(&(data[offset+1]), 0x1FFF);   
            demux->es_info[demux->es_count].elementary_pid = elementary_pid;            
            demux->es_count++;

            /* stream type */			
            if (sDEMUX_StreamType_IsDataCarousel(stream_type) ||
                   sDEMUX_StreamType_IsEventMessage(stream_type) ||
                   sDEMUX_StreamType_IsMPE(stream_type) ||
                   sDEMUX_StreamType_IsObjectCarousel(stream_type)) {
                   
                /* DSM-CC test */
#if 0				
#if 1               
                section_filter sect;
                sect.pid = elementary_pid;
                sect.tid = DSMCC2_TID;
                sect.callback= DEMUX_DSMCCCallback;
                DEMUX_SetSectionFilter(handle, &sect);
#else
                section_filter sect;
                sect.pid = elementary_pid;
                sect.tid = DSMCC3_TID;
                sect.callback= DEMUX_DSMCCCallback;
                DEMUX_SetSectionFilter(handle, &sect);
#endif
#endif
            }           
            else if(sDEMUX_StreamType_IsAIT(stream_type)){
#if 0
                section_filter sect;
                sect.pid = elementary_pid;
                sect.tid = AIT_TID;
                sect.callback= DEMUX_AITCallback;
                DEMUX_SetSectionFilter(handle, &sect);
#endif
            }
            else if(sDEMUX_StreamType_IsVideo(stream_type)){
                LOGE("DEMUX_PMTCallback: stream_type=%x\n",stream_type);
                demux->video_codec = stream_type;
                DEMUX_SetPesFilter(demux, VIDEO_FILTER, elementary_pid, stream_type);
            }
        }
        else {
            LOGE("DEMUX_PMTCallback: can not get ES info\n");
            return;
        }
        es_info_len  = sDEMUX_Get16Bits(&(data[offset+3]), 0x0FFF);
        if (section_len >= (5 + es_info_len)) {
            offset      += (5 + es_info_len);
            section_len -= (5 + es_info_len);
        } else {
            break;
        }
    }   
    
    LOG("DEMUX_PMTCallback: Done\n");
}

void DEMUX_AITCallback(void *handle, uint16_t pid, uint16_t tid,local_buffer* buf, void* user_parma){
    demux_context *demux =  (demux_context *)handle;
    section_filter sect ;       

    /* once parse AIT */    
    sect.pid = pid;
    sect.tid = tid;
    DEMUX_ClearSectionFilter(demux, &sect);
    
    LOG("call DEMUX_AITCallback pid=%d, tid=%x, len=%d, user_parma=%p\n", pid, tid, buf->length, user_parma);
}

void DEMUX_DSMCCCallback(void *handle, uint16_t pid, uint16_t tid,local_buffer* buf, void* user_parma){
    demux_context *demux =  (demux_context *)handle;    
    
    LOG("call DEMUX_DSMCCCallback pid=%d, tid=%x, len=%d, demux=%p, user_parma=%p\n", pid, tid, buf->length, demux, user_parma);
}

