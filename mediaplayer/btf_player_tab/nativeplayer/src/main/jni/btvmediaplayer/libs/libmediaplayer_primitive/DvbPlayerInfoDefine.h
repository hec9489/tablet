// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef __DVB_PLAYER_INFO_DEFINE______H_____
#define __DVB_PLAYER_INFO_DEFINE______H_____

#include "utils/RtpUtils.h"

#ifdef __cplusplus
extern "C" {
#endif

#include <btv_hal.h>
#include <demux.h>
#include "dtvcc.h"
#ifdef USE_MULTIVIEW
#include <hal/multiView/pix_type.h>
#endif
#include <pt_mlr_client.h>

#include <pt_ads.h>

#define USE_BTV_HAL_MANAGER

typedef enum dvb_locator_type_enum
{
    DVB_IP,
    DVB_FILE,
    DVB_VOD
} dvb_locator_type;

typedef struct dvb_param_ip_s
{
    char    ip[256];
    int     port;
} dvb_param_ip;

typedef struct dvb_param_file_s
{
    char    filename[256];
} dvb_param_file;

typedef struct dvb_locator_s
{
    dvb_locator_type type;

    char name[32];

    int vid;
    int pid;
    int aid;
    int vcodec;
    int acodec;
	int pcr_pid;

    int sid;
    int nid;
    int onid;

    union
    {
        dvb_param_ip   ip;
        dvb_param_file file;
    } tune_param;

	int keep_last_frame;

    struct dvb_locator_s *next;
} dvb_locator_t;

#define BLOCK_COUNT                             8

#define TP_PACKET_SIZE                          188
#define NUMBER_PACKETS                          (128) //(199*BLOCK_COUNT)
#define BUFFER_SIZE                             (TP_PACKET_SIZE*NUMBER_PACKETS)

typedef int 	(*TCASDemuxCB) (void *buffer, int size, int viewID);
typedef void	(*TQICalcScanCB) ( const unsigned char* stream_buf,
								  unsigned int buf_size,
								unsigned int packet_count, unsigned  int channel_ip);
typedef void	(*TQICalcOnJoinRequestedCB)(unsigned long ip,
											unsigned short port);


typedef struct TAudioConfig {
	int pid;
	int codec;
}t_AudioConfig;

// jung2604 : multiview 정보를 위해서 추가
typedef struct dvb_player_s dvb_player_t;

// jung2604 : multiview 정보를 위해서 추가
typedef struct STRUCT_MULTIVIEW_INFO {
	int viewNumber;
	int idx_player_for_multiview;
	dvb_locator_t tLocator;
	dvb_player_t *pPlayer;
	int readCnt;
	pthread_mutex_t multiview_thread_lock;
	pthread_cond_t multiview_thread_cond;
	int isTerminateComplete;
} t_MultiviewInfo, *pt_MultiviewInfo;

typedef struct TAG_AVP_INFO {
	AVP_HANDLE playerHandle;
	AVP_CreateConfig createConfig;
	AVP_PlayerConfig playerConfig;
	AVP_AudioOutputMode audioOutputMode;
	AVP_AVoffset avOffset;
	AVP_SectionFilterConfig sectionFilterConfig;

	VCOMPO_HANDLE vcompoHandle;
	AMIXER_HANDLE amixerHandle;

    bool isFeedBufferEOF; // 버퍼에 EOF를 세팅했는지 여부..
	bool isAcquireDone; // Sectionfilter AcquiredDone
} tAvpInfo;

typedef struct dvb_player_s {
    FILE *fStream;;

    bool enable_clock_recovery;

    bool enable_audio;
    bool enable_video;

    int idx_player;

	bool EOFReached;
	volatile int TerminateFilePlay;
	void *cbHandler;
    TCASDemuxCB cbCASDescramble;
	// qi calculator callback
	TQICalcScanCB 	cbQiCalcScan;
	TQICalcOnJoinRequestedCB cbQiCalcOnJoinRequested;
	// add stop action
	// this will not close device and other action will do
	int nStopFlag;
	int nPollingRunFlag;
	long nPuaseAtMs;

    unsigned int EOSVideo_Count;
    unsigned int EOSAudio_Count;
    bool AudioEventRecvd;
    bool VideoEventRecvd;

    //volatile int Terminate;
    volatile int TerminateTuner;

	volatile bool isAvpStart; //jung2604 ; iptv에서 ST_ChangeChannel할때 close와 open이 뒤로 빠지면서 데이터를 feed해주는것을 늦춰야한다.

    //bool calcBitrate;
	dvb_locator_t *locator_head;

	tAvpInfo avpInfo;
	bool rtsp_last_eos_status;
	long rtsp_last_pts_check_time;
	long rtsp_last_pts;
	bool save_mute_enable;
	int channel_num;
    void *pIptvRkPlayer;
	int isFirstFrameDisplayed;
	bool isMultiview;
#ifdef USE_MULTIVIEW
    int multiviewType;
	h_pt_merge_hevc hMultiView;
	t_MultiviewInfo pt_MultiviewInfo[4];
#endif

    MLR_HANDLE hMlrClient;
    MLR_FUNCTION mlrFunction;
	bool isMlrClientHangup;

	ADS_HANDLE hAdsClient;
	ADS_FUNCTION adsFunction;
	bool isAdsClientHangup;

	// Closed Caption
	cc_fcallback cc_callback;
	ccx_dtvcc_ctx *dtvcc;

	// DSMCC Control
	dsmcc_ctx *dsmcc;
    bool enableLastFrame;

	// Demux handle
	demux_context* demuxHandle;

    int videoDelayTimeMs;
	int enableGlobalMute;
	int lastMuteValueForGlobalMute;
	AVP_AudioOutputMode enableSettingsDolby; //돌비 관련 설정파일 상태..
	AVP_AspectRatioMode settingsVideoScaleMode; //화면 비율 설정
	int kec_mode; //

	bool isRtspSeekMode;

	// test용
    struct timespec tTempTimeVal;
    long tempTimeMs;
	long timeCheckStartTime;
    long timeCheckSocketBeforTime;
    long timeCheckSocketAfterTime;
    long timeCheckFirstFeedingTime;
    long timeCheckFirstIframeDisplayedTime;
    long timeCheckStopServiceBeforeTime;
    long timeCheckStopServiceAfterTime;
    long timeCheckCreateStartAfterTime;
    long timeCheckCreateEndAfterTime;

	int isFeedMultiviewData; //멀티뷰 feed감시용

	const void *m_pAnative; //anative 저장용

} dvb_player_t;

typedef struct
{
	int program_number;
	int pid;
}pmt_info_t;

typedef struct
{
	unsigned char table_id;
	unsigned char section_syntax_indicator;
	unsigned short section_length;
	unsigned short transport_stream_id;
	unsigned char version_number;
	unsigned char current_next_indicator;
	unsigned char section_number;
	unsigned char last_section_number;
	unsigned char program_count;
	pmt_info_t pmt[3];
} pat_data_t;

typedef enum dvb_event_type_enum
{
  SECTION_FILTER_EVENT = 4,
  PLAY_STATUS_EVENT,
  TUNE_EVENT,
  DISCONNECT_EVENT,
  CLOSE_CAPTION_DATA_EVENT,
  PID_CHANGED_EVENT,
  SEEK_COMPLETION_EVENT = 10,
  MAX_NUM_EVENT
} dvb_notify_event_type;

#ifdef __cplusplus
}
#endif

#endif //__DVB_PLAYER_INFO_DEFINE______H_____
