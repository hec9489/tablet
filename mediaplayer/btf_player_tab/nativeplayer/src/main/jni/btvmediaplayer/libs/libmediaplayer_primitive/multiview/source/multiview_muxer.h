// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifdef __cplusplus
extern "C" {
#endif

#ifndef WORK_FFMPEG_MUXER_H
#define WORK_FFMPEG_MUXER_H

#include <android/log.h>


#include <libavformat/avio.h>
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>

#include "multiview_define.h"
#include "multiview_demuxer.h"
#include "multiview_util.h"

typedef int (*WRITE_STREAM_CB)(void* opaque, uint8_t* buf, int buf_size);

typedef struct _ST_FFMPEG_MUX_CTX{
	AVFormatContext     *fmt_ctx;
	AVIOContext         *avio_ctx;

//	ST_FFMPEG_DEMUX_CTX ffmpeg_demux_ctx[MULTIVIEW_MUXER_CHANNEL];

	int                 video_stream_idx;
	int                 audio_stream_idx[MULTIVIEW_MUXER_CHANNEL];

	AVRational          video_time_base;
	AVRational          audio_time_base[MULTIVIEW_MUXER_CHANNEL];

	void*               player;             // player

	WRITE_STREAM_CB cb;

	ST_QUEUE_MESSAGE    *message_queue;
}ST_FFMPEG_MUX_CTX;

int multiview_create_muxer_stream(ST_FFMPEG_MUX_CTX** ctx, ST_FFMPEG_DEMUX_CTX** ffmpeg_demux_ctx, int demuxCnt,
                                  void* opaque, WRITE_STREAM_CB cb);

int multiview_mux_stream(ST_FFMPEG_MUX_CTX* ctx, h_pt_merge_hevc pt_ctx, int* terminate);

int multiview_destroy_muxer(ST_FFMPEG_MUX_CTX** ctx);

#ifdef __cplusplus
} //extern "C"
#endif

#endif //WORK_FFMPEG_MUXER_H
