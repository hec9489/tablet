// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#include <dvb_primitive.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef PLAY_RTSP_H_
#define PLAY_RTSP_H_

int  RTSP_Initialize();
void RTSP_Uninitialize();
void RTSP_Reset();

int  RTSP_Open(int mdaID, int nWindowType, char* url, char* cid, int nMillisecond, void* errCB, void* hIptvInstance);
int  RTSP_Close(int mdaID);


int  RTSP_Play(int mdaID, int nMillisecond, dvb_player_t *player);
int  RTSP_Stop(int mdaID);
int  RTSP_Pause(int mdaID);
int  RTSP_Resume(int mdaID);

int  RTSP_Seek(int mdaID, int nMillisecond);
int RTSP_SetPlaySpeed(int mdaID, float speed);
int  RTSP_Trick(int mdaID, int speed, int nCurMillisecond);

long long  RTSP_GetDuration(int mdaID);

int  RTSP_GetVideoPID(int mdaID);
int  RTSP_GetVideoCodec(int mdaID);
int  RTSP_GetAudioPID(int mdaID);
int  RTSP_GetAudioCodec(int mdaID);

long long  RTSP_GetFirstIFreamePTS(int mdaID);

int RTSP_GetIFRStatus(int mdaID);
int RTSP_GetSessionStatus(int mdaID);

int RTSP_SetWaitWriteDVR(int mdaID, int nFlag);
int RTSP_WaitWriteCheckDVR(int mdaID);
int RTSP_GetEOFBOFStatus(int mdaID);

//int    Callback_playstatus_event( int mdaID, int nWindowType, PlayerStatusCB p_stat);


#endif /* PLAY_RTSP_H_ */


#ifdef __cplusplus
}
#endif
