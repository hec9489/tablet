// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef ____RTP_UTILS____H____
#define ____RTP_UTILS____H____

#include "RtpInfoDefine.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef int (*MLR_FUNCTION)(void *player, char *buf, int *pLengthData);
typedef int (*ADS_FUNCTION)(void *player, char *buf, int *pLengthData);

int getRtp2Buffer(int fd, struct rtpheader *rh, char *data, int *lengthData, int nDevice, MLR_FUNCTION mlrFunction, void* argument);
int makesocket(char *szAddr, unsigned short port, int TTL, struct sockaddr_in *sSockAddr);
int cMulticast_leave(int *socket, struct ip_mreq *blub);
int stdvbplayer_makeclientsocket(char *szAddr, unsigned short port, int TTL, struct sockaddr_in *sSockAddr, struct ip_mreq *blub, int nDevice);
void set_thread_priority(int idx);

extern RtpInfo_t *gRtpInfo[6];

#ifdef __cplusplus
}
#endif

#endif
