// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifdef __cplusplus
extern "C" {
#endif

#include <pthread.h>

#include "ZooVodClientAPI.h"
#include "iptvmedia/util_logmgr.h"
#include "play_rtsp.h"

#include "../casdrmImpl/wise_skdrm.h"

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dvb_primitive.h"

#define RTSP_HEADER_SIZE 			(77)
#define RTSP_SKDRM_HEADER_TAG		"DHDR"
#define RTSP_PACKET_LEN			(9964)
#define RTSP_PACKET_COUNT		(2600)
#define RTSP_BUF_LEN				(RTSP_PACKET_LEN * RTSP_PACKET_COUNT) // Packet 1000 EA = (1000 * 9964)Byte
#define DIV_RATIO 					4

char					*GlobalBufferPointer = NULL;

typedef int (*TRTSPErrorCB) (int nErrorCode, char *szIp, int nPort, void *hInstance, int nWillClose);

typedef struct tag_RTSP_SESSION{
	VodClient_Session_t 	 VodClient;
	int	 					 mdaID;
	int 					 nWindowType;
	char                     content_id[64];
	volatile int 			 nBOFStatus; 		// 1:BOF set 0:None
	volatile int 			 nEOFStatus; 		// 1:EOF set 0:None
	volatile int 			 nStopflag; 		// 1:Stop set 0:None
	int 					 nStreamPause;		// 1:ON 0:OFF
	int 					 nFirstPacket;		// 1:Check set 0:ON
	unsigned long long		 uuReceiveByte;   	// receive streamData size
	unsigned long long		 uuReceiveBytePerTime;   	// receive streamData size
	struct  timeval 		 timeStart;

	EVENT_CODE				 eRegEventMsg;

	pthread_mutex_t     	 request_lock;
	pthread_cond_t        	 producer_cv;
	pthread_t 				 pRTSPThread;

	char					*szBuf;
	int						nPos;
	int 					 nTearDown; 		// 1:TearDowned set 0:Not

	TRTSPErrorCB errCB;
	void			*hIptvInstance;

	// For buffering implementation.
	char *buf_readptr;
	char *buf_writeptr;
	const char *buf_max;
	const char *buf_min;
	int is_trick_on;
	int is_fasttrans_enable;
	int			nPauseFlag;
	int			nBadRequestFeedPauseFlag; // TODO : jung2604 : seek중 400에러가 났을경우 죽는 문제를 해결하기 위해서, Seek중일때에는 data를 받지 않아본다..
	volatile int waitDvrWrite;
	volatile int waitDvrWriteCheck;
	volatile int *waitDvrBackWrite;

	// jung2604
	dvb_player_t *player;

	struct tag_RTSP_SESSION *pNext;
}RTSP_SESSION, *PRTSP_SESSION;

PRTSP_SESSION g_pVodSessionRoot = NULL;

#define DSRC_RTSP LOGSRC


// define api  prototype
int  Callback_OnRecvData(const VodClient_Session_t * _pSession, const int fIdentifier, unsigned char *buffer, int n);
int  Callback_OnRecvEvent(const VodClient_Session_t * _pSession,EVENT_CODE eRetCode);								///< RTCP Event Message call-back
int  Callback_OnPlayEvent(const VodClient_Session_t * _pSession, const int fIdentifier);								///< Play Event Message Eof or Bof
int  Callback_OnPlayReturn(const VodClient_Session_t * _pSession,RETURN_CODE eRetCode); ///< Return Error or Closing Status Message
int  Callback_OnFREND(const VodClient_Session_t * _pSession,RTCP_PAYLOAD_TYPE eRetCode); ///< Return Error or Closing Status Message

int  _add_Session(PRTSP_SESSION pSession);
int  _del_Session(int mdaID);
PRTSP_SESSION _find_Session(int mdaID);

int  Thread_moniteringBuf(void *arg);	// Monitering the buffer state and Controlling the stream.
int  Thread_writingToHAL(void *arg);	// Reading the buffer data and Writing to HAL.

void _clear_HalBuf(PRTSP_SESSION pSession, int nNonflush);
void _pause_Stream(PRTSP_SESSION pSession);
void _resume_Stream(PRTSP_SESSION pSession);

int  _create_SKDRM(PRTSP_SESSION pSession);
int  _delete_SKDRM(PRTSP_SESSION pSession);

int  _setRegFrameType(PRTSP_SESSION pSession, EVENT_CODE eRegEventMsg);
int  _IsFrameType(PRTSP_SESSION pSession, EVENT_CODE eRevcEventMsg);

int  _setRegFrameType(PRTSP_SESSION pSession, EVENT_CODE eRegEventMsg)
{
	int nRet = 0;
	pthread_mutex_lock(&pSession->request_lock);

	if(pSession->eRegEventMsg != eRegEventMsg){
		pSession->eRegEventMsg 		= eRegEventMsg;
		DSRC_RTSP("[RTSP] _setRegFrameType eRegEventMsg(%d)", eRegEventMsg);
	}

	pthread_mutex_unlock(&pSession->request_lock);

	return nRet;
}

int nGetRTSPBufferUsedCount(PRTSP_SESSION pSession)
{
	int i, nCount;

	if (pSession == NULL)
		return 0;

	char *tmpPoint = (char*)pSession->buf_min;
	
	for (i = 0, nCount = 0; i < RTSP_PACKET_COUNT; i++)
	{
		if (tmpPoint[i * RTSP_PACKET_LEN] == 0x47) // case data set!
		{
			nCount++;
		}
	}

	return nCount;
}

int showBufferData(PRTSP_SESSION pSession)
{
	int i;

	char *tmpPoint = (char*)pSession->buf_min;
	char *tmpPoint2 = (char*)pSession->buf_readptr;
	char *tmpPoint3 = (char*)pSession->buf_writeptr;
	
	printf("bullshit !!!!!!!!!! %s %d %x %x %x %x\n", __func__, __LINE__,  pSession->buf_readptr, pSession->buf_writeptr, pSession->buf_min, pSession->buf_max);

	for (i = 0; i < RTSP_PACKET_COUNT; i++)
	{
		if (i % 32 == 0)
			printf("\n"); 

		if (&tmpPoint[i * RTSP_PACKET_LEN] == tmpPoint2)
		{
			printf("*");
		}

		if (&tmpPoint[i * RTSP_PACKET_LEN] == tmpPoint3)
		{
			printf("X");
		}

		if (tmpPoint[i * RTSP_PACKET_LEN] == 0x47) // case data set!
		{
			printf("1 ");
		}
		else
		{
			printf("0 ");
		}
	}
	printf("\n");

	return 0;
}


int  _IsFrameType(PRTSP_SESSION pSession, EVENT_CODE eRevcEventMsg)
{
	int nRet = 0;
	pthread_mutex_lock(&pSession->request_lock);
	if(pSession->eRegEventMsg == eRevcEventMsg){
		nRet = 0;
	}
	else{
		nRet = -1;
	}
	pthread_mutex_unlock(&pSession->request_lock);

	return nRet;
}


int  _create_SKDRM(PRTSP_SESSION pSession)
{
	int nHeaderlength;
	int nRet = -1;

	if (0 == pSession->VodClient.DRM_exist)
	{
		DSRC_RTSP("[RTSP] 0 == pSession->VodClient.DRM_exist(%d)", pSession->VodClient.DRM_exist);
		return nRet;
	}

	WISE_SKDRM_Init();

	if(WISE_SKDRM_getSKDRMContentHandler( pSession->nWindowType ) != NULL){
		DSRC_RTSP("[RTSP] there exist SKDRM contentHandler so delete it");
		WISE_SKDRM_DeleteContentHandler( pSession->nWindowType );
	}

	nHeaderlength = ntohl(*((int*)(pSession->VodClient.drm_info + 4)));

	nRet = WISE_ZOO_SKDRM_CreatContentHandler ((char*) pSession->VodClient.drm_info , nHeaderlength , pSession->nWindowType , pSession->content_id );
	if (nRet == 0)
	{
		DSRC_RTSP("[RTSP] SUCCESS:SKDRM_CreatContentHandler");
	}
	else
	{	
		nRet = WISE_ZOO_SKDRM_CreatContentHandler ((char*) pSession->VodClient.drm_info , nHeaderlength , pSession->nWindowType , pSession->content_id );
		
		if (nRet == 0)
		{
			DSRC_RTSP("[RTSP] SUCCESS:SKDRM_CreatContentHandler");
		}
		else if (nRet == -0x00210002)
		{
			DSRC_RTSP("[RTSP] SKDRM Integrity ERROR. Delete files and RE-INIT. send 404 Error");
			WISE_SKDRM_Init();
			pSession->nStopflag = 1;
			if (pSession->errCB && pSession->hIptvInstance)
			{
				pSession->errCB(404, pSession->VodClient.ip, pSession->VodClient.port, pSession->hIptvInstance, 0);
			}
		}
		else if (nRet == -0x00200013)
		{
			DSRC_RTSP("[RTSP] SKDRM don't create HANDLER!! creating RO delayed !! send 403 error");
			pSession->nStopflag = 1;
			if (pSession->errCB && pSession->hIptvInstance)
			{
				pSession->errCB(403, pSession->VodClient.ip, pSession->VodClient.port, pSession->hIptvInstance, 0);
			}
		}
		else{
			DSRC_RTSP("[RTSP] ERROR:SKDRM_CreatContentHandler failed, nRet=%d", nRet);
			pSession->nStopflag = 1;
			if (pSession->errCB && pSession->hIptvInstance)
			{
				pSession->errCB(401, pSession->VodClient.ip, pSession->VodClient.port, pSession->hIptvInstance, 0);
			}
		}
	}

	return nRet;
}

int  _delete_SKDRM(PRTSP_SESSION pSession)
{
	if(WISE_SKDRM_getSKDRMContentHandler(pSession->nWindowType) != NULL){
		WISE_SKDRM_DeleteContentHandler(pSession->nWindowType);
	}

	return 0;
}


// api implement

int  _add_Session(PRTSP_SESSION pSession)
{
	if( NULL == g_pVodSessionRoot){
		g_pVodSessionRoot = pSession;
	}
	else{
		PRTSP_SESSION  pTemp = g_pVodSessionRoot;
		while(pTemp->pNext){
			pTemp = pTemp->pNext;
		}

		pTemp->pNext = pSession;
	}


	return 0;
}

int  _del_Session(int mdaID)
{
	PRTSP_SESSION  pTemp = NULL;
	PRTSP_SESSION  pPrev = NULL;
	PRTSP_SESSION  pDel  = NULL;
	int 		   nRet  = -1;
	if( NULL == g_pVodSessionRoot){
		return nRet;
	}

	pTemp = g_pVodSessionRoot;

	if(-1 == mdaID){
		do{
			pDel = pTemp;
			pTemp = pTemp->pNext;
			free(pDel);
		}while(pTemp);

		g_pVodSessionRoot = NULL;
		nRet = 0;
	}
	else{
		do{
			if(pTemp->mdaID == mdaID){
				if(g_pVodSessionRoot == pTemp){
					g_pVodSessionRoot = pTemp->pNext;
					free(pTemp);
					pTemp = NULL;
				}
				else{
					pPrev->pNext = pTemp->pNext;
					free(pTemp);
					pTemp = NULL;
				}
				nRet = 0;
				break;
			}
			pPrev = pTemp;
			pTemp = pTemp->pNext;
		}while(pTemp);
	}


	return nRet;
}

PRTSP_SESSION _find_Session(int mdaID)
{
	PRTSP_SESSION pTempSession = NULL;

	if( NULL == g_pVodSessionRoot){

		return pTempSession;
	}

	pTempSession = g_pVodSessionRoot;
	do{
		if(pTempSession->mdaID == mdaID){
			break;
		}

		pTempSession = pTempSession->pNext;
	}while(pTempSession);

	return pTempSession;
}

/* fldgo note : 2014.12.24 buffering 정책을 다시 확정한다.
 1. 구간은 4개 구간으로 나눈다. buffer free 율 기준이다.
	 -- 0% 이상 - 10% 미만	// 구간 LIMITED -> pause 시킨다.
	 -- 10% 이상 - 30% 미만	// 구간 FULL ->  pause 상태였으면 푼다. 0.5 배속
	 -- 30이상 - 95 미만	// 구간 NORMAL -> 1.0 배속
	 -- 95 이상				// 구간 LACK -> 4.0 배속
*/

// fldgo note : 3개의 poll 영역 구분자
#define FREE_BUFFERING_POLL_LIMITIED	(20.0f)
#define FREE_BUFFERING_POLL_FULL		(30.0f)
#define FREE_BUFFERING_POLL_LACK		(95.0f)

/* fldgo note : 2014.12.24 buffering 정책을 다시 확정한다.
 1. LIMITED , FULL, NORMAL 구간에서만 완충 영역을 설정한다.
     -- 5% 이상 ~ 10 %미만	// 구간 LIMITED : 내부의 해당 영역에서는 아무것도 하지 않는다. 
	 pause 를 풀게되면 많은 데이터 유입으로(0.5 배속이라도 쌓인다.) pause 재진입 반복현상
	 문제가 발생하므로 완충 지대를 둔다.
	 -- 20% 이상 - 30% 미만	// 구간 FULL : 내부의 해당 영역에서는 아무것도 하지 않는다. 0.5 배속 1배속 완충 구간이다.
	 -- 70 이상 - 95 미만	// 구간 NORMAL : 내부의 해당 영역에서는 아무것도 하지 않는다. 1 배속 4배속 완충 구간이다.
*/

// fldgo note : 각 구분자간의 공용 구역
#define FREE_BUFFERING_POLL_LIMITED_DIV	(15.0f)
#define FREE_BUFFERING_POLL_FULL_DIV	(25.0f)
#define FREE_BUFFERING_POLL_NORMAL_DIV	(70.0f)

void FreeBufferingPolicyLIMITIED(PRTSP_SESSION pSession, double bufFree)
{
	// 완충 지대 체크
	if (bufFree >= FREE_BUFFERING_POLL_LIMITED_DIV)
		return;

	printf("fldgo monitorThread bufFree goto limit will pause Free Buf [%f]\n", bufFree);
	if(0 == pSession->nStreamPause){
		//DSRC_RTSP("[RTSP] ++ FreeBufferingPolicyLIMITIED bufFree [%f]", bufFree);
		_pause_Stream(pSession);
	}
}

void FreeBufferingPolicyFULL(PRTSP_SESSION pSession, double bufFree)
{
	int nRet = -1;

	if (1 == pSession->nStreamPause)
	{
		printf("fldgo monitorThread will resume !! \n");
		_resume_Stream(pSession);
	}

	// 완충 지대 체크
	if (bufFree >= FREE_BUFFERING_POLL_FULL_DIV)
		return;
	
	if (pSession->is_fasttrans_enable==1 && pSession->VodClient.fSpeed != 0.5) 
	{
		printf("fldgo monitorThread SLOW CALLED!! \n");
		nRet = Slow(&pSession->VodClient, 0.5);
		printf("fldgo monitorThread SLOW CALLED [%d] [%f]!! \n", nRet, pSession->VodClient.fSpeed);
		pSession->is_fasttrans_enable = 0;	// FastTransmission 이 적용되기 전까지 중복 호출되는 것을 막기 위함.
	}
	
	if (pSession->VodClient.fSpeed == 0.5)
	{
		printf("fldgo monitorThread enable set SLOW!! \n");
		pSession->is_fasttrans_enable = 1;
	}
}

void FreeBufferingPolicyNORMAL(PRTSP_SESSION pSession, double bufFree)
{
	int nRet = -1;

	// 100ms over policy full not called case
	if (1 == pSession->nStreamPause)
	{
		printf("fldgo monitorThread will resume !! \n");
		_resume_Stream(pSession);
	}

	// 완충 지대 체크
	if (bufFree >= FREE_BUFFERING_POLL_NORMAL_DIV)
		return;

	if (pSession->is_fasttrans_enable==1 && pSession->VodClient.fSpeed != 1.0) 
	{
		printf("fldgo monitorThread NORMAL CALLED!! \n");
		nRet = FastTransmission(&pSession->VodClient, 1.0);
		printf("fldgo monitorThread NORMAL CALLED [%d] [%f]!! \n", nRet, pSession->VodClient.fSpeed);
		pSession->is_fasttrans_enable = 0;	// FastTransmission 이 적용되기 전까지 중복 호출되는 것을 막기 위함.
	}
	
	if (pSession->VodClient.fSpeed == 1.0)
	{
		printf("fldgo monitorThread enable set NORMAL!! \n");
		pSession->is_fasttrans_enable = 1;
	}
}

void FreeBufferingPolicyLACK(PRTSP_SESSION pSession, double bufFree)
{
	int nRet = -1;

	//rtsp 자체적으로 멈춤현상이 있다..
	if (pSession->waitDvrWrite != 1 && pSession->nPauseFlag != 1 && pSession->nStreamPause == 1) {
		//DSRC_RTSP("[RTSP] ++ test FreeBufferingPolicyLACK bufFree [%f], !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!resume!!!!!!!!!!!!!!!!!", bufFree);
		_resume_Stream(pSession);
	}

	if (pSession->is_fasttrans_enable==1 && pSession->VodClient.fSpeed < 4.0)
	{
		printf("fldgo monitorThread FAST CALLED!! \n");
		nRet = FastTransmission(&pSession->VodClient, 4.0);
		printf("fldgo monitorThread FAST CALLED [%d] [%f]!! \n", nRet, pSession->VodClient.fSpeed);
		pSession->is_fasttrans_enable = 0;	// FastTransmission 이 적용되기 전까지 중복 호출되는 것을 막기 위함.
	}
	
	if (pSession->VodClient.fSpeed == 4.0)
	{
		printf("fldgo monitorThread enable set FAST!! \n");	
		pSession->is_fasttrans_enable = 1;
	}
}

int Thread_moniteringBuf(void *arg)
{
	int nRet 	= -1;
	int nBufFreeSpace = 0;
	int nCount;
	int nnCount = 0;

	double bufFree = 0.0f;	// Percentage of free buffer remainder.

	PRTSP_SESSION pSession = (PRTSP_SESSION)arg;
	if (pSession == NULL){
		DSRC_RTSP("[RTSP] ERROR:Thread_moniteringBuf pSession == NULL");
		return nRet; // case none
	}

	while (0 == pSession->nStopflag) {
		nRet = 0;

		if (pSession->is_trick_on == 1) // trick 모드일때에는 모니터링 하지 않는다.
		{
			usleep(4000);
			continue;
		}

		if (pSession->nPauseFlag == 1) // pause 모드일때에는 모니터링 하지 않는다.
		{
			usleep(4000);
			continue;
		}

		if (pSession->nBadRequestFeedPauseFlag == 1) // jung2604 : seek중 400에러가 났을경우 죽는 문제를 해결하기 위해서, Seek중일때에는 data를 받지 않아본다..
		{
			usleep(4000);
			continue;
		}

		nnCount++;

		//nCount = nGetRTSPBufferUsedCount(pSession);
		//nBufFreeSpace = RTSP_BUF_LEN - nCount * RTSP_PACKET_LEN;
		if(pSession->buf_writeptr < pSession->buf_readptr) {
			nBufFreeSpace = RTSP_BUF_LEN - ((int) (pSession->buf_max - pSession->buf_readptr) + (int) (pSession->buf_writeptr - pSession->buf_min));
		} else nBufFreeSpace = RTSP_BUF_LEN - ((int) (pSession->buf_writeptr - pSession->buf_readptr));

		//버퍼가 꽉 찬다면(그럴일이 없어야하지만)..리드버프의 첫번째가 0인지 확인해본다. 0일경우 buf_writeptr을 처음으로 이동시켜준다.
		if(nBufFreeSpace == 0 && !pSession->buf_readptr[0] && pSession->buf_writeptr == pSession->buf_max) {
			pSession->buf_writeptr = pSession->buf_min;
			DSRC_RTSP("[RTSP] ++ Thread_moniteringBuf : buf_writeptr reset....nStreamPause[%d], nBufFreeSpace [%d], bufFree [%f], speed [%f], waitDvrWrite [%d], is_trick_on [%d], nPauseFlag [%d], nBadRequestFeedPauseFlag [%d], buf_readptr[0]=[%d]",
					  pSession->nStreamPause, nBufFreeSpace, bufFree, pSession->VodClient.fSpeed, pSession->waitDvrWrite,  pSession->is_trick_on,  pSession->nPauseFlag, pSession->nBadRequestFeedPauseFlag, pSession->buf_readptr[0]);

			if(pSession->nStreamPause) {
				DSRC_RTSP("[RTSP] ++ Thread_moniteringBuf : change stream pause to resume (nBufFreeSpace:%d)", nBufFreeSpace);
				_resume_Stream(pSession);
			}

			usleep(4000);
			continue;
		}

		bufFree = ((double)nBufFreeSpace / RTSP_BUF_LEN) * 100.0f;

		if (nnCount%10 == 0) {
			nnCount = 0;
			DSRC_RTSP("[RTSP] ++ Thread_moniteringBuf : pSession->nStreamPause[%d], nBufFreeSpace [%d], bufFree [%f], speed [%f], waitDvrWrite [%d], is_trick_on [%d], nPauseFlag [%d], nBadRequestFeedPauseFlag [%d], buf_readptr[0]=[%d]",
					  pSession->nStreamPause, nBufFreeSpace, bufFree, pSession->VodClient.fSpeed, pSession->waitDvrWrite,  pSession->is_trick_on,  pSession->nPauseFlag, pSession->nBadRequestFeedPauseFlag, pSession->buf_readptr[0]);

			if (pSession->nStreamPause == 1)
				printf("fldgo monitorThread %d bufFree [%f] [%f] [PAUSE]\n", __LINE__, bufFree, pSession->VodClient.fSpeed);
			else
				printf("fldgo monitorThread %d bufFree [%f] [%f] [RESUME]\n", __LINE__, bufFree, pSession->VodClient.fSpeed);
		}

		// jung2604 : 기존의 pause구간에 pause구간에서 다시 FREE_BUFFERING_POLL_FULL_DIV 구간을 지날때까지 pause를 유지한다.
		if (bufFree < FREE_BUFFERING_POLL_LIMITIED || (pSession->nStreamPause &&  bufFree < FREE_BUFFERING_POLL_FULL_DIV)) // 구간 LIMITED
		{
			FreeBufferingPolicyLIMITIED(pSession, bufFree);

		} 
		else if (bufFree >= FREE_BUFFERING_POLL_LIMITIED && bufFree < FREE_BUFFERING_POLL_FULL) // 구간 FULL
		{
			FreeBufferingPolicyFULL(pSession, bufFree);
		}

		else if(bufFree >= FREE_BUFFERING_POLL_FULL && bufFree < FREE_BUFFERING_POLL_LACK) // 구간 NORMAL
		{
			FreeBufferingPolicyNORMAL(pSession, bufFree);
		}
		else if (bufFree > 95.0f) // 구간 LACK
		{
			FreeBufferingPolicyLACK(pSession, bufFree);
		}

		usleep(50000);//usleep(100000);
	}

	return nRet;
}


int Thread_writingToHAL(void *arg) {
	int nRet = -1;
	int written = 0;
    int remain = 0;
	int total = 0;
    void *pReadBuf;
	void *pCheckReadBufAddr;
	int lastWaitStatus = 0;

	//int logCheckTime = 0; // TODO : jung2604 : test용

	PRTSP_SESSION pSession = (PRTSP_SESSION) arg;
	if (pSession == NULL) {
		return nRet;
	}

	int isFeedWaitBuffer = 1;

    int feedWaitBufferSize = 188*128;
	if(pSession->player && pSession->player->locator_head && pSession->player->locator_head->vcodec == AVP_CODEC_VIDEO_H265) { // UHD일경우 4M를 버퍼로 설정한다.
        feedWaitBufferSize = 4095392; //188*21784;//188*128;//188*128;//188*8169;//188*6807;////188*10892;//4982000; //188*5446; // //9964000; 3840*2160
	}


	long startTimeMs = 0l, currentTimeMs = 0l, totalWritten = 0l;
	struct timespec tTempTimeVal;
	if (0 == clock_gettime(CLOCK_REALTIME, &tTempTimeVal)) {
		startTimeMs = (tTempTimeVal.tv_sec * 1000) + (tTempTimeVal.tv_nsec / 1000000);
	}

	while (0 == pSession->nStopflag) {
		nRet = 0;
		if (pSession->waitDvrWrite == 1 || pSession->is_trick_on == 1)
		{
			if (pSession->waitDvrWrite == 1)
			{
				pSession->waitDvrWriteCheck = 1;
			}

            if(lastWaitStatus != 1) {
                DSRC_RTSP("[RTSP] ++ Thread_writingToHAL : waitDvrWrite[%d], is_trick_on [%d]", pSession->waitDvrWrite, pSession->is_trick_on);
			    lastWaitStatus = 1;
            }
			usleep(4000);
			isFeedWaitBuffer = 1;
			continue;
		}

		if (pSession->nPauseFlag == 1) // pause 모드일때에는 모니터링 하지 않는다.
		{
            if(lastWaitStatus != 2) {
                DSRC_RTSP("[RTSP] ++ Thread_writingToHAL pSession->nPauseFlag : %d", pSession->nPauseFlag);
			    lastWaitStatus = 2;
            }
			usleep(4000);
			//isFeedWaitBuffer = 1;
			continue;
		}

		if (pSession->nBadRequestFeedPauseFlag == 1) // jung2604 : seek중 400에러가 났을경우 죽는 문제를 해결하기 위해서, Seek중일때에는 data를 받지 않아본다..
		{
            if(lastWaitStatus != 3) {
                DSRC_RTSP("[RTSP] ++ Thread_writingToHAL pSession->nBadRequestFeedPauseFlag : %d", pSession->nBadRequestFeedPauseFlag);
                lastWaitStatus = 3;
            }
			usleep(4000);
			isFeedWaitBuffer = 1;
			continue;
		}

        if(lastWaitStatus != 3) lastWaitStatus = 0;
        
        //리드 버퍼가 MAX와 같다.
		if(pSession->buf_readptr >= pSession->buf_max) {
            //리드버퍼와 라이트버퍼가 다르다면..
            //버퍼의 포인트가 같다면, FULL, EMPTY 판단을 하기 위해서 첫번째 버퍼(buf_min[0])에 데이터가(0x47) 있는지 확인한다.
			if(pSession->buf_readptr != pSession->buf_writeptr || pSession->buf_min[0]) {
				DSRC_RTSP("[RTSP] ++  test change readptr min %p/%p ", pSession->buf_readptr , pSession->buf_writeptr);
				pSession->buf_readptr = (char*) pSession->buf_min;
			}

		}

		remain = RTSP_PACKET_LEN;

		//버퍼 사이즈 계산
		if(pSession->buf_writeptr < pSession->buf_readptr) {
			remain = ((int) (pSession->buf_max - pSession->buf_readptr));
			total = remain + (int) (pSession->buf_writeptr - pSession->buf_min);
			if(remain <= 0) {
				DSRC_RTSP("[RTSP] ++  test ERR> remain error reamin : %d, total : %d... init..", remain, total);
				pSession->buf_readptr = pSession->buf_min;
				remain = total;
			}
		} else {
			total = remain = (int) (pSession->buf_writeptr - pSession->buf_readptr);
		}

		pCheckReadBufAddr = pReadBuf = pSession->buf_readptr;

        //버퍼에 충분히 쌓기.
        if(isFeedWaitBuffer && !pSession->nEOFStatus) {
            if(remain > feedWaitBufferSize) {
                isFeedWaitBuffer = 0;
                DSRC_RTSP("[RTSP] ++ start buffer size : %d/%d", remain, feedWaitBufferSize);
            } else {
                DSRC_RTSP("[RTSP] ++ wait buffer size : %d/%d", remain, feedWaitBufferSize);
            }
        }

		if(remain > 0 && !isFeedWaitBuffer) {
            written = HalBtv_InjectTS(pSession->player->idx_player, pReadBuf, remain);
            if(written <= 0) DSRC_RTSP("[RTSP] written error  written(%d), buffer(%d)", written, total);
            else if(pSession->nEOFStatus && (total - written) <= 0) {
				DSRC_RTSP("[RTSP] ++rtsp EOF !!!!!!!");
				//3회 실패시 무조건 에러 처리
				for(int i = 0 ; i < 3 && 0 == pSession->nStopflag ; i++) {
					if(HalBtv_InjectCommand(pSession->player->idx_player, AVP_INJECT_EOS) == 0) break;

					usleep(100000); //wait 100ms
				}
				pSession->player->avpInfo.isFeedBufferEOF = true;
			}

            if (pSession->is_trick_on == 0 && written > 0 && pCheckReadBufAddr == pSession->buf_readptr) { // write 후 현재 상태가 trick 이 아닐 경우에만
                memset((void*) pSession->buf_readptr, 0x00, written);
                pSession->buf_readptr += written;
            }

        } else {
            written = 0;

			if(pSession->nEOFStatus && !pSession->player->avpInfo.isFeedBufferEOF) {
				DSRC_RTSP("[RTSP] ++rtsp EOF !!!!!!! - out total:0");
				//3회 실패시 무조건 에러 처리
				for(int i = 0 ; i < 3 && 0 == pSession->nStopflag ; i++) {
					if(HalBtv_InjectCommand(pSession->player->idx_player, AVP_INJECT_EOS) == 0) break;

					usleep(100000); //wait 100ms
				}
				pSession->player->avpInfo.isFeedBufferEOF = true;
			}

			//체크용..
			int nBufFreeSpace = 0;
			if(pSession->buf_writeptr < pSession->buf_readptr) {
				nBufFreeSpace = RTSP_BUF_LEN - ((int) (pSession->buf_max - pSession->buf_readptr) + (int) (pSession->buf_writeptr - pSession->buf_min));
			} else nBufFreeSpace = RTSP_BUF_LEN - ((int) (pSession->buf_writeptr - pSession->buf_readptr));

			if(nBufFreeSpace <= 0) {
				DSRC_RTSP("[RTSP] buffer full?? writeptr(%p), buf_readptr(%p), max(%p), min(%p)", pSession->buf_writeptr, pSession->buf_readptr, pSession->buf_max, pSession->buf_min);
			}
		}

		if(written > 0) totalWritten += written;
		else usleep(1000*250); // TODO : feed실패시 딜레이 발생!! 학인!!

		if (0 == clock_gettime(CLOCK_REALTIME, &tTempTimeVal)) {
			currentTimeMs = (tTempTimeVal.tv_sec * 1000) + (tTempTimeVal.tv_nsec / 1000000);
			if(currentTimeMs - startTimeMs >= 1000) {
				DSRC_RTSP("[RTSP] rtsp feed info : buffer(%d/%d), 1sec totalWritten(%d), readPtr(%p), eof(%d)", remain, total, totalWritten, pSession->buf_readptr, pSession->nEOFStatus);
				startTimeMs = currentTimeMs;
				totalWritten = 0;
			}
		}

		//usleep(1000);
	}

	return nRet;
}


int nFILEFD;


int nRecVVVV = 0;
int PreTime = 0;
int nLogCheckRecvDataCount = 0;
int  Callback_OnRecvData(const VodClient_Session_t * _pSession, const int fIdentifier, unsigned char *buffer, int n)
{
/*
	// TODO : jung2604 : 테스트용..
	nLogCheckRecvDataCount++;
	if(nLogCheckRecvDataCount > 100) {
		DSRC_RTSP("[RTSP] Callback_OnRecvData %d", n);
		nLogCheckRecvDataCount = 0;
	}
*/
	
	{
		//time handling
		struct timeval tvNow;
		gettimeofday(&tvNow, NULL);
		nRecVVVV += n;

		if (PreTime != tvNow.tv_sec) {
			printf("fldgo onrecv                                                %d Recv %f Mbps\n", nRecVVVV,
					(float) nRecVVVV * 8 / 1024 / 1024);
			nRecVVVV = 0;
			PreTime = tvNow.tv_sec;
		}
	}
	
	int nRet = 0;
	PRTSP_SESSION pSession = NULL;

    unsigned char decryptedBuffer[4096*3];
	
    int space = 0;

	pSession = _find_Session(_pSession->fIdentifier);
	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", _pSession->fIdentifier);
		return nRet;
	}

	if(1 == pSession->nStopflag){
		LOGSRC(" ");
		return nRet;
	}

	// jung2604 : seek일 경우 400에러가 발생하면 죽기때문에 seek중일때에는 데이터를 받지 않아본다.
	if(1 == pSession->nBadRequestFeedPauseFlag) {
		LOGSRC("++ bad request error");
		return nRet;
	}

	pSession->nEOFStatus = 0;

	if(NULL == buffer){
		DSRC_RTSP("[RTSP] ERROR:NULL == buffer n(%d)", n);
		return nRet;
	}

	if(RTSP_HEADER_SIZE >= n){
		return 0;
	}
	if (0 != _IsFrameType(pSession, _pSession->iNew_stream_info)){
		DSRC_RTSP("[RTSP] INFO:eRegEventMsg(%d) iNew_stream_info(%d)", pSession->eRegEventMsg, _pSession->iNew_stream_info);
		//return 0;
	}

	if (pSession->VodClient.DRM_exist) { // DRM flag check and decrypt
		int dec_size = 0;
		//dec_size = WISE_ZOO_SKDRM_DecryptTS(buffer, decryptedBuffer, n, 0);
		dec_size = WISE_ZOO_SKDRM_DecryptTS((char*)buffer, (char*)decryptedBuffer, n, 0);
		if (dec_size <= 0 || dec_size != n) {
			LOGSRC(	"WISE_ZOO_SKDRM_DecryptTS api return error very bad case DataSize:%d dec_size:%d!!!!!!",
				n, dec_size);
			return 0;
		}
		buffer = decryptedBuffer;
	}


	if (pSession->is_trick_on == 1) // trick 모드 일경우에는 buffering 을 하지 않는다.
	{
		while (pSession->waitDvrWrite == 1)
		{
			pSession->waitDvrWriteCheck = 1;
			//usleep(4000);	
			return 0;
		}

		int remain = n;
		int written = 0;
		void * pReadBuf = buffer;
		while(0 == pSession->nStopflag && remain > 0 ) {
#ifdef USE_BTV_HAL_MANAGER
			written = HalBtv_InjectTS(pSession->player->idx_player, pReadBuf, remain);
#else
			written = AVP_InjectTS(pSession->player->avpInfo.playerHandle, pReadBuf, remain);
#endif
			remain -= written;
			pReadBuf += written;

			if(remain <= 0) break;
			usleep(4000);
            //DSRC_RTSP("[RTSP] Callback_OnRecvData out");
		}

		return 0;
	}

	int nBufFreeSpace = 0;
	if(pSession->buf_writeptr < pSession->buf_readptr) {
		nBufFreeSpace = RTSP_BUF_LEN - ((int) (pSession->buf_max - pSession->buf_readptr) + (int) (pSession->buf_writeptr - pSession->buf_min));
	} else nBufFreeSpace = RTSP_BUF_LEN - ((int) (pSession->buf_writeptr - pSession->buf_readptr));

	//DSRC_RTSP("[RTSP] ++ nBufFreeSpace : %d", nBufFreeSpace);

	if (pSession->buf_writeptr >= pSession->buf_max) // reset buffer write ptr
	{
		pSession->buf_writeptr = (char*) pSession->buf_min;
	}

	// !0 이라는 이야기는 write 된 데이터가 아직 남아 있다는.. 즉 buffer full 상태라는 이야기가 된다.
	// 그 데이터는 항상 0x47 이다.
	// 하지만 drop 이 옳은지는 생각해야 한다.
	// 결국 깨지게되는데 이때에는 나중의 data 를 넣어두는게 낫다.

	// trick 모드에서 play 전환할때에는 시간차 때문에 중간에 발생가능하다 버그가 아니다.
	if (pSession->buf_writeptr[0] != 0) // case not put 
	{
		DSRC_RTSP("[RTSP] wooooops save data buffer was write by another thread or full");
		if(pSession->buf_writeptr < pSession->buf_readptr) {
			nBufFreeSpace = RTSP_BUF_LEN - ((int) (pSession->buf_max - pSession->buf_readptr) + (int) (pSession->buf_writeptr - pSession->buf_min));
		} else nBufFreeSpace = RTSP_BUF_LEN - ((int) (pSession->buf_writeptr - pSession->buf_readptr));

		DSRC_RTSP("[RTSP] wooooops nBufFreeSpace : %d", nBufFreeSpace);

	}

	memcpy((void*) pSession->buf_writeptr, (void*) buffer, n);

	pSession->buf_writeptr += n;

	return 0;
}


int  Callback_OnRecvEvent(const VodClient_Session_t * _pSession,EVENT_CODE eRetCode)///< RTCP Event Message call-back
{
    DSRC_RTSP("[RTSP] Callback_OnRecvEvent event:%d sessStatus:%d eRetCode:%d", _pSession->EventCode, _pSession->sessionStatus, eRetCode);
	return 0;
}

int  Callback_OnPlayEvent(const VodClient_Session_t * _pSession, const int fIdentifier)///< Play Event Message Eof or Bof
{
    DSRC_RTSP("[RTSP] Callback_OnPlayEvent event:%d sessStatus:%d", _pSession->EventCode, _pSession->sessionStatus);
	return 0;
}

int  Callback_OnPlayReturn(const VodClient_Session_t * _pSession, RETURN_CODE eRetCode)///< Return Error or Closing Status Message
{
    DSRC_RTSP("[RTSP] Callback_OnPlayReturn");

	int nRet = 0;
	PRTSP_SESSION pSession = NULL;

	LOGSRC("Callback_OnPlayReturn event:%d sessStatus:%d eRetCode:%d", _pSession->EventCode, _pSession->sessionStatus, eRetCode);
//	if (_pSession->sessionStatus == STATUS_CLOSED) {
//		DSRC_RTSP("[RTSP] session status closed. ignore~");
//		return nRet;
//	}

	pSession = _find_Session(_pSession->fIdentifier);
	if(NULL == pSession){
		//DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", _pSession->fIdentifier);
		return nRet;
	}

	if(1 == pSession->nStopflag){
		return 0;
	}

	if( eRetCode == RET_BAD_REQUEST_ERR ) {
		LOGSRC("++RET_BAD_REQUEST_ERR");
		pSession->nBadRequestFeedPauseFlag				= 1; // jung2604 : seek중 400에러가 났을경우 죽는 문제를 해결하기 위해서..
	}

	if( eRetCode == RET_SOCKET_ERR || eRetCode == RET_SERVER_CONERR
		|| eRetCode == RET_URL_ERR || eRetCode == RET_INCORRECT_FILE
		|| eRetCode == RET_UNKNOWN || eRetCode == RET_INCORRECT_MSG
		|| eRetCode == RET_PLAY_ORDER_ERR || eRetCode == RET_AUTH_ERR/* || eRetCode == RET_BAD_REQUEST_ERR*/){

		pSession->nStopflag = 1;
		pSession->nTearDown = 1;

		if (pSession->errCB && pSession->hIptvInstance) {
			pSession->errCB(eRetCode, _pSession->ip, _pSession->port, pSession->hIptvInstance, 1);
		}
	}

	if (eRetCode == RET_RTCP_BYE){
		LOGSRC("rtsp recv EOF !!!");

		pSession->nEOFStatus = 1;
		pSession->is_fasttrans_enable = 0;
	}
	else if(eRetCode == RET_ERR_SYNCBYTE){
        LOGSRC("rtsp recv BYE~ EOF !!!");
		pSession->nStopflag = 1;
		pSession->nTearDown = 1;
		if (pSession->errCB && pSession->hIptvInstance)
		{
			pSession->errCB(eRetCode, _pSession->ip, _pSession->port, pSession->hIptvInstance, 1);
		}
	}
	else if(eRetCode == RET_ERR_THREAD_DUPLICATION){

	}

	return 0;
}

int  Callback_OnFREND(const VodClient_Session_t * _pSession, RTCP_PAYLOAD_TYPE eRetCode) ///< Return Error or Closing Status Message
{
	LOGSRC("Callback_OnFREND");

	int nRet = 0;
	PRTSP_SESSION pSession = NULL;


	LOGSRC("Callback_OnFREND");
	pSession = _find_Session(_pSession->fIdentifier);
	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", _pSession->fIdentifier);
		return nRet;
	}

	if(1 != pSession->nBOFStatus){
		pSession->nBOFStatus = 1;
		//_pause_Stream(pSession);
	}
	return 0;
}

void RTSP_Reset()
{
	while(g_pVodSessionRoot){
		DSRC_RTSP("[RTSP] RTSP_Reset mdaID:%d", g_pVodSessionRoot->mdaID);
		RTSP_Close(g_pVodSessionRoot->mdaID);
	};

	_del_Session(-1);
	DSRC_RTSP("[RTSP] RTSP_Reset:");
}

void RTSP_Uninitialize()
{
	RTSP_Reset();
	return;
}

int  RTSP_Open(int mdaID, int nWindowType, char* url, char* cid, int nMillisecond, void* errCB, void* hIptvInstance)
{
	int nRet = -1;
	PRTSP_SESSION pNewSession = NULL;

	if(_find_Session(mdaID)){
		DSRC_RTSP("[RTSP] ERROR:exist session mdaID:%d", mdaID);
		return nRet;
	}

	if(NULL == url){
		DSRC_RTSP("[RTSP] ERROR:invalid url mdaID:%d", mdaID);
		return nRet;
	}


	pNewSession = malloc(sizeof(RTSP_SESSION));
	if(NULL == pNewSession){
		DSRC_RTSP("[RTSP] ERROR:malloc(%d) mdaID:%d", sizeof(RTSP_SESSION), mdaID);
		return nRet;
	}

	memset(pNewSession, 0x00, sizeof(RTSP_SESSION));

	pNewSession->errCB = (TRTSPErrorCB)errCB;
	pNewSession->hIptvInstance = hIptvInstance;


	LOG_TIME_STARTV("InitClient mdaID(%d) URL(%s)", mdaID, url);
	nRet = InitClient(&pNewSession->VodClient); //주인넷 초기화
	LOG_TIME_END("InitClient nRet(%d)", nRet);


	if(RET_SUCCESS != nRet){
		DSRC_RTSP("[RTSP] ERROR:InitClient mdaID:%d nRet%d", mdaID, nRet);
		if(nRet > 99 )
		{
			if (pNewSession->errCB && pNewSession->hIptvInstance)
			{
				pNewSession->errCB(nRet, pNewSession->VodClient.ip, pNewSession->VodClient.port, pNewSession->hIptvInstance, 0);
			}
		}

		free(pNewSession);
		return nRet;
	}

	pNewSession->mdaID 					= mdaID;
	pNewSession->nWindowType			= nWindowType;
	pNewSession->VodClient.fIdentifier 	= mdaID;
	pNewSession->VodClient.OnRecvData 	= Callback_OnRecvData;
	pNewSession->VodClient.OnRecvEvent 	= Callback_OnRecvEvent;
	pNewSession->VodClient.OnPlayEvent 	= Callback_OnPlayEvent;
	pNewSession->VodClient.OnPlayReturn = Callback_OnPlayReturn;
	pNewSession->VodClient.OnFREND 		= Callback_OnFREND;
	pNewSession->eRegEventMsg			= EVENT_NONE;

	// fldgo note : dvr write 잠시 delay flag  1: delay 0: write
	pNewSession->waitDvrWrite			= 0;
	pNewSession->nPauseFlag				= 0;
	pNewSession->nBadRequestFeedPauseFlag				= 0;

	strcpy(pNewSession->VodClient.url, url);
	if(cid != NULL){
		strcpy(pNewSession->content_id, cid);
	}

	nRet = _add_Session(pNewSession);
	if(RET_SUCCESS != nRet){
		DSRC_RTSP("[RTSP] ERROR:_add_Session mdaID:%d", mdaID);
		return nRet;
	}

	LOG_TIME_STARTV("Setup mdaID(%d)", mdaID);
	nRet = Setup(&pNewSession->VodClient);  //주인넷 setup
	LOG_TIME_END("Setup nRet(%d)", nRet);

	if(RET_SUCCESS != nRet){
		DSRC_RTSP("[RTSP] ERROR:Setup mdaID:%d nRet%d", mdaID, nRet);

		if(RET_ERR_THREAD_DUPLICATION != nRet){
			if(nRet > 99){
				if (pNewSession->errCB && pNewSession->hIptvInstance)
				{
					pNewSession->errCB(nRet, pNewSession->VodClient.ip, pNewSession->VodClient.port, pNewSession->hIptvInstance, 0);
				}
			}
		}

		_del_Session(mdaID);
		return nRet;
	}

	if(pNewSession->VodClient.DRM_exist){
		LOG_TIME_STARTV("_create_SKDRM mdaID(%d)", mdaID);
		nRet = _create_SKDRM(pNewSession);
		LOG_TIME_END("_create_SKDRM nRet:%d", nRet);
		if(0 != nRet){
			RTSP_Close(mdaID);
			return -1;
		}
	}
    //GlobalBufferPointer : RTSP 라이브러리에서 사용할 버퍼
	if (GlobalBufferPointer == NULL)
	{
		GlobalBufferPointer = (char *) malloc(RTSP_BUF_LEN);
	}

	memset(GlobalBufferPointer, 0x00, RTSP_BUF_LEN);

	pNewSession->szBuf = GlobalBufferPointer;
	pNewSession->nPos = 0;
	pNewSession->buf_readptr = &pNewSession->szBuf[0];
	pNewSession->buf_writeptr = &pNewSession->szBuf[0];
	pNewSession->buf_max = &pNewSession->szBuf[RTSP_BUF_LEN];
	pNewSession->buf_min = &pNewSession->szBuf[0];
	pNewSession->is_trick_on = 0;
	pNewSession->is_fasttrans_enable = 1;

	DSRC_RTSP("[RTSP] SUCCESS:RTSP_Open mdaID:%d", mdaID);

	return 0;
}

int Thread_teardown(void *arg) {
    int mdaID = -1;
    int nRet = -1;
    PRTSP_SESSION pSession = NULL;

//    if(arg == NULL) {
//        DSRC_RTSP("[RTSP] [%s] arg is null", __func__);
//        return 0;
//    }

    mdaID = (int) arg;

    DSRC_RTSP("[RTSP] [%s] mdaID:%d", __func__, mdaID);

    pSession = _find_Session(mdaID);
    if(NULL == pSession){
        DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
        return nRet;
    }

    pSession->nStopflag = 1;

    if (pSession->nTearDown != 1)
    {
        LOGSRC("Teardown sockfd:%d", pSession->VodClient.sockfd);
		nRet = Teardown(&pSession->VodClient);//fldgo note : will call
		LOGSRC("Teardown nRet(%d)", nRet);
    }

    if(RET_SUCCESS != nRet){
        LOGSRC("ERROR:Tear down failed");
    }

    DSRC_RTSP("[RTSP] INFO:mdaID(%d) ReceiveByte(%lld)", mdaID, pSession->uuReceiveByte);

    LOGSRC("_delete_SKDRM");
    _delete_SKDRM(pSession);
    LOGSRC("_delete_SKDRM");

    nRet = _del_Session(mdaID);
    if(RET_SUCCESS != nRet){
        DSRC_RTSP("[RTSP] ERROR:_del_Session mdaID:%d", mdaID);
        return nRet;
    }

    DSRC_RTSP("[RTSP] [%s] mdaID:%d", __func__, mdaID);

    return 0;
}

int  RTSP_Close(int mdaID)
{
	int nRet = -1;

	PRTSP_SESSION pSession = NULL;

	DSRC_RTSP("[RTSP] RTSP_Close mdaID:%d", mdaID);

	pSession = _find_Session(mdaID);
	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return nRet;
	}

	pSession->nStopflag = 1;

	if (pSession->nTearDown != 1)
	{
#if 1
		LOGSRC("Teardown sockfd:%d", pSession->VodClient.sockfd);
		nRet = Teardown(&pSession->VodClient);//fldgo note : will call
		LOGSRC("Teardown nRet(%d)", nRet);
#else
		pthread_attr_t teardownThreadAttr;
		pthread_attr_init(&teardownThreadAttr);
		pthread_attr_setdetachstate(&teardownThreadAttr, PTHREAD_CREATE_DETACHED);
		pthread_t           pTearDownThread = 0;

		if(pthread_create(&pTearDownThread, &teardownThreadAttr, (void*)Thread_teardown, (void*)mdaID) != 0) {
			LOGSRC("ERROR:Tear down thread fail.");
		}
#endif
	} else {
        if(RET_SUCCESS != nRet){
            LOGSRC("ERROR:Tear down failed");
        }

        DSRC_RTSP("[RTSP] INFO:mdaID(%d) ReceiveByte(%lld)", mdaID, pSession->uuReceiveByte);

        LOGSRC("_delete_SKDRM");
        _delete_SKDRM(pSession);
        LOGSRC("_delete_SKDRM");

        nRet = _del_Session(mdaID);
        if(RET_SUCCESS != nRet){
            DSRC_RTSP("[RTSP] ERROR:_del_Session mdaID:%d", mdaID);
            return nRet;
        }
    }

	DSRC_RTSP("[RTSP] SUCCESS:RTSP_Close mdaID:%d", mdaID);

	return 0;
}


int  RTSP_Play(int mdaID, int nMillisecond, dvb_player_t *player)
{
	int nRet = -1;
	PRTSP_SESSION pSession = NULL;

	pSession = _find_Session(mdaID);
	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return nRet;
	}

	pSession->player = player;

	pSession->nBOFStatus = 0;

	pSession->is_trick_on = 0;

	//if(STATUS_NONE == pSession->VodClient.sessionStatus && NULL == pSession->pRTSPThread){
	if((SESSION_STATUS)STATUS_NONE == (SESSION_STATUS)pSession->VodClient.sessionStatus && (int)NULL == pSession->pRTSPThread){
		_setRegFrameType(pSession, EVENT_CONTINUE);

		LOG_TIME_STARTV("Play mdaID(%d) %f", mdaID, (float)nMillisecond/1000);
		nRet = Play(&pSession->VodClient,(float)nMillisecond/1000);
		LOG_TIME_END("Play nRet(%d)", nRet);
		if(RET_SUCCESS != nRet){
			DSRC_RTSP("[RTSP] ERROR:Play mdaID:%d pos:%f", mdaID, (float)nMillisecond/1000);
			return nRet;
		}

		_clear_HalBuf(pSession, 1);

		int thrId;

		pthread_attr_t tattrMoniteringBuf;
		/* create the signal handling thread */
    	nRet = pthread_attr_init(&tattrMoniteringBuf);
		/* set the thread detach state */
    	nRet = pthread_attr_setdetachstate(&tattrMoniteringBuf, PTHREAD_CREATE_DETACHED);
		thrId = pthread_create(&pSession->pRTSPThread, &tattrMoniteringBuf, (void*)Thread_moniteringBuf, pSession);
    	if (thrId < 0)
    	{
        	DSRC_RTSP("[RTSP] daemon :Thread_moniteringBuf create error\n");
         	return -1;
    	}

		pthread_attr_t tattrWritingToHAL;
		/* create the signal handling thread */
    	nRet = pthread_attr_init(&tattrWritingToHAL);
		/* set the thread detach state */
    	nRet = pthread_attr_setdetachstate(&tattrWritingToHAL, PTHREAD_CREATE_DETACHED);
		thrId = pthread_create(&pSession->pRTSPThread, &tattrWritingToHAL, (void*)Thread_writingToHAL, pSession);
		if (thrId < 0)
    	{
        	DSRC_RTSP("[RTSP] daemon :Thread_writingToHAL create error\n");
         	return -1;
    	}

	}
	else{


#if FEATURE_TEST
		if(1 == pSession->nEOFStatus && nMillisecond+1*1000 > RTSP_GetDuration(mdaID)){
			DSRC_RTSP("[RTSP] WARNING:RTSP_Play mdaID:%d nMillisecond(%d)+1*1000", mdaID, nssecond);
			return -1;
		}
#endif

		_setRegFrameType(pSession, EVENT_RANDOM_SEEK);

		pSession->is_fasttrans_enable = 0;
		LOG_TIME_STARTV("Seek mdaID(%d) %f", mdaID, nMillisecond/1000);
		nRet = Seek(&pSession->VodClient, nMillisecond/1000);
		LOG_TIME_END("Seek nRet(%d)", nRet);

		pSession->is_fasttrans_enable = 1;
		if(RET_SUCCESS != nRet){
			pSession->nBadRequestFeedPauseFlag				= 0; // jung2604 : seek중 400에러가 났을경우 죽는 문제를 해결하기 위해서..
			DSRC_RTSP("[RTSP] ERROR:Seek mdaID:%d pos:%f", mdaID, nMillisecond/1000);
			return nRet;
		}

		// Buffer Reset
		memset(pSession->buf_min, 0x00, RTSP_BUF_LEN);
		pSession->buf_readptr = (char*)pSession->buf_min;
		pSession->buf_writeptr = (char*)pSession->buf_min;
		
		_clear_HalBuf(pSession, 0);
	}

//	nRet = FastTransmission(&pSession->VodClient, 4.0);
	DSRC_RTSP("[RTSP] SUCCESS:RTSP_Play mdaID:%d pos:%f", mdaID, (float)nMillisecond/1000);
	return 0;
}

int  RTSP_Stop(int mdaID)
{
	int nRet = -1;
	PRTSP_SESSION pSession = NULL;

	LOGSRC(" ");

	pSession = _find_Session(mdaID);
	if(NULL == pSession){
		LOGSRC(" ");
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return nRet;
	}

	pSession->nStopflag = 1;

	LOGSRC(" ");
	LOGSRC(" ");
	if(RET_SUCCESS != nRet){
		LOGSRC(" ");
		DSRC_RTSP("[RTSP] ERROR:Seek mdaID:%d pos:%f", mdaID, (float)0);
		return nRet;
	}

	LOGSRC(" ");
	pSession->is_fasttrans_enable = 0;
	nRet = Pause(&pSession->VodClient);
	LOGSRC(" ");
	if(RET_SUCCESS != nRet){
		LOGSRC(" ");
		DSRC_RTSP("[RTSP] ERROR:Pause mdaID:%d", mdaID);
		return nRet;
	}
 
	LOGSRC(" ");
	DSRC_RTSP("[RTSP] SUCCESS:RTSP_Stop mdaID:%d", mdaID);

	return 0;
}

int  RTSP_Pause(int mdaID)
{
	int nRet = -1;
	PRTSP_SESSION pSession = NULL;

	LOGSRC(" ");
	pSession = _find_Session(mdaID);
	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return nRet;
	}

    // jung2604
#ifdef USE_BTV_HAL_MANAGER
	HalBtv_SetPlaySpeed(pSession->player->idx_player, (float)abs(1));
#else
	AVP_SetPlaySpeed(pSession->player->avpInfo.playerHandle, (float)abs(1));
#endif

//#if FEATURE_TEST
	if(1 == pSession->nEOFStatus){
		DSRC_RTSP("[RTSP] WARNING:RTSP_Pause EOFStatus mdaID:%d", mdaID);
		return 0;
	}
//#endif //FEATURE_TEST


	LOGSRC(" ");
	pSession->is_fasttrans_enable = 0;
	nRet = Pause(&pSession->VodClient);
	if(RET_SUCCESS != nRet){
		DSRC_RTSP("[RTSP] ERROR:Pause mdaID:%d", mdaID);
		return nRet;
	}

	pSession->nPauseFlag = 1;	

	LOGSRC(" ");
	DSRC_RTSP("[RTSP] SUCCESS:RTSP_Pause mdaID:%d", mdaID);
	return 0;
}

int  RTSP_Resume(int mdaID)
{
	int nRet = -1;
	PRTSP_SESSION pSession = NULL;

	pSession = _find_Session(mdaID);
	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return nRet;
	}

    // jung2604
#ifdef USE_BTV_HAL_MANAGER
	HalBtv_SetPlaySpeed(pSession->player->idx_player, (float)abs(1));
#else
	AVP_SetPlaySpeed(pSession->player->avpInfo.playerHandle, (float)abs(1));
#endif
	
	pSession->nPauseFlag = 0;
	pSession->is_trick_on = 0;
	pSession->nBadRequestFeedPauseFlag				= 0; // jung2604 : seek중 400에러가 났을경우 죽는 문제를 해결하기 위해서..
//#if FEATURE_TEST
	if(1 == pSession->nEOFStatus){
		DSRC_RTSP("[RTSP] WARNING:RTSP_Resume EOFStatus mdaID:%d", mdaID);
		return 0;
	}
//#endif //FEATURE_TEST


	_setRegFrameType(pSession, EVENT_CONTINUE);
	pSession->is_fasttrans_enable = 1;

	LOG_TIME_STARTV("Continue mdaID(%d)", mdaID);
	nRet = Continue(&pSession->VodClient);
	LOG_TIME_END("Continue nRet(%d)", nRet);

	pSession->nBadRequestFeedPauseFlag				= 0; // jung2604 : seek중 400에러가 났을경우 죽는 문제를 해결하기 위해서..
	if(RET_SUCCESS != nRet){
		DSRC_RTSP("[RTSP] ERROR:Continue mdaID:%d", mdaID);
		return nRet;
	}

	if(EVENT_FAST_FRWD == pSession->eRegEventMsg
			|| EVENT_FAST_RWD == pSession->eRegEventMsg
			|| STATUS_FFFR == pSession->VodClient.sessionStatus){

//		HMedia_FlushRTSPHalBuffer();fldgo note : will call
		pSession->nFirstPacket = 1;
		pSession->uuReceiveByte	= 0;
		pSession->uuReceiveBytePerTime = 0;
		gettimeofday( &pSession->timeStart, NULL );
	}

	if (1 == pSession->nStreamPause)
	{
		printf("fldgo monitorThread will resume IN SEEK!! \n");
		_resume_Stream(pSession);
	}

	DSRC_RTSP("[RTSP] SUCCESS:RTSP_Resume mdaID:%d", mdaID);
	return 0;
}


int  RTSP_Seek(int mdaID, int nMillisecond)
{
	int nRet = -1;
	PRTSP_SESSION pSession = NULL;
	float seekTimeMs = 0.0;

	pSession = _find_Session(mdaID);
	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return nRet;
	}

	pSession->is_trick_on = 0;
#if FEATURE_TEST
	if(1 == pSession->nEOFStatus && nMillisecond+1*1000 > RTSP_GetDuration(mdaID)){
		DSRC_RTSP("[RTSP] WARNING:RTSP_Play mdaID:%d nMillisecond(%d)+1*1000", mdaID, nMillisecond);
		return -1;
	}
#endif

	pSession->nPauseFlag = 0;
	//TODO : jung2604 : seek하는도중 400에러로 인한 에러 방지용
	pSession->nBadRequestFeedPauseFlag				= 0; // jung2604 : seek중 400에러가 났을경우 죽는 문제를 해결하기 위해서..
	pSession->nBOFStatus = 0;
	_setRegFrameType(pSession, EVENT_RANDOM_SEEK);

	pSession->is_fasttrans_enable = 0;
	LOG_TIME_STARTV("Seek mdaID(%d) %f", mdaID, (float)nMillisecond/1000);

	// jung2604 : 버퍼 초기화 추가..seek하기전에 비워줘서 DATA_OFF가 발생하지 않도록 해보자
	memset(pSession->buf_min, 0x00, RTSP_BUF_LEN);
	pSession->buf_readptr = (char*)pSession->buf_min;
	pSession->buf_writeptr = (char*)pSession->buf_min;
/*
	if (1 == pSession->nStreamPause)
	{
		printf("fldgo monitorThread will resume IN SEEK!! \n");
		_resume_Stream(pSession);
	}
*/
    seekTimeMs = (float)nMillisecond/1000;
 	nRet = Seek(&pSession->VodClient, seekTimeMs);//(float)nMillisecond/1000);
 	LOG_TIME_END("Seek nRet(%d)", nRet);
 	pSession->is_fasttrans_enable = 1;

	if(RET_SUCCESS != nRet){
		pSession->nBadRequestFeedPauseFlag				= 0; // jung2604 : seek중 400에러가 났을경우 죽는 문제를 해결하기 위해서..
		DSRC_RTSP("[RTSP] ERROR:Seek mdaID:%d pos:%f", mdaID, seekTimeMs);
		return nRet;
	}

	memset(pSession->buf_min, 0x00, RTSP_BUF_LEN);
	pSession->buf_readptr = (char*)pSession->buf_min;
	pSession->buf_writeptr = (char*)pSession->buf_min;

	_clear_HalBuf(pSession, 0);

	DSRC_RTSP("[RTSP] SUCCESS:RTSP_Seek mdaID:%d pos:%f", mdaID, (float)nMillisecond/1000);

	return 0;
}

int RTSP_SetPlaySpeed(int mdaID, float speed) {
    DSRC_RTSP("[RTSP] ++RTSP_SetPlaySpeed session mdaID:%f, speed : %f", mdaID, speed);
    int nRet = -1;
    PRTSP_SESSION pSession = NULL;
    pSession = _find_Session(mdaID);
    if(NULL == pSession){
        DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%f", mdaID);
        return nRet;
    }

    if(speed >= 0.8 && speed <= 1.5) {
#ifdef USE_BTV_HAL_MANAGER
		HalBtv_SetPlaySpeed(pSession->player->idx_player, speed);
#else
		AVP_SetPlaySpeed(pSession->player->avpInfo.playerHandle, speed);
#endif
    }

    return  0;
}

int  RTSP_Trick(int mdaID, int speed, int nCurMillisecond)
{
	int nRet = -1;
	PRTSP_SESSION pSession = NULL;

	pSession = _find_Session(mdaID);
	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return nRet;
	}

	//	트릭모드일 때는 FastTransmission 호출되지 않도록 처리
	pSession->is_trick_on = 1;
	pSession->nBOFStatus = 0;
	pSession->nPauseFlag = 0;

	pSession->nBadRequestFeedPauseFlag				= 0; // jung2604 : seek중 400에러가 났을경우 죽는 문제를 해결하기 위해서..

	//_resume_Stream(pSession); // 일단 넣는다. 요한대리님 추후 수정요망 //////////////

	if (1 == pSession->nStreamPause)
	{
		printf("fldgo monitorThread will resume IN Trick!! \n");
		_resume_Stream(pSession);
	}

	if(speed > 0){
#if FEATURE_TEST
		if(1 == pSession->nEOFStatus){
			DSRC_RTSP("[RTSP] WARNING:RTSP_Trick EOFStatus mdaID:%d", mdaID);
			return -1;
		}
#endif //FEATURE_TEST

		_setRegFrameType(pSession, EVENT_FAST_FRWD);
		LOG_TIME_STARTV("time_FastForward mdaID(%d) speed(%d) %f", mdaID, speed, (float)nCurMillisecond/1000);
		nRet = time_FastForward(&pSession->VodClient, (float)speed, (float)nCurMillisecond/1000.0);
		LOG_TIME_END("time_FastForward nRet(%d)", nRet);
		if(RET_SUCCESS != nRet){
			DSRC_RTSP("[RTSP] ERROR:time_FastForward mdaID:%d speed:%d pos:%f", mdaID, (float)speed, (float)nCurMillisecond/1000);
			return nRet;
		}


	}
	else if(speed < 0){
		_setRegFrameType(pSession, EVENT_FAST_RWD);
		LOG_TIME_STARTV("time_Rewind mdaID(%d) speed(%d) %f", mdaID, speed, (float)nCurMillisecond/1000);
		nRet = time_Rewind(&pSession->VodClient, (float)speed, (float)nCurMillisecond/1000.0);
		LOG_TIME_END("time_Rewind nRet(%d)", nRet);
		if(RET_SUCCESS != nRet){
			DSRC_RTSP("[RTSP] ERROR:time_FastForward mdaID:%d speed:%d pos:%f", mdaID, (float)speed, (float)nCurMillisecond/1000);
			return nRet;
		}
	}
#ifdef USE_BTV_HAL_MANAGER
	HalBtv_SetPlaySpeed(pSession->player->idx_player, 1.0);
#else
	AVP_SetPlaySpeed(pSession->player->avpInfo.playerHandle, 1.0);
#endif
	// Buffer Reset
	memset(pSession->buf_min, 0x00, RTSP_BUF_LEN);
	pSession->buf_readptr = (char*)pSession->buf_min;
	pSession->buf_writeptr = (char*)pSession->buf_min;

	_clear_HalBuf(pSession, 0);

	DSRC_RTSP("[RTSP] SUCCESS:RTSP_Trick mdaID:%d speed:%f pos:%f", mdaID, (float)speed,(float)nCurMillisecond/1000);
	return 0;
}


long long  RTSP_GetDuration(int mdaID)
{
	int nRet = -1;
	PRTSP_SESSION pSession = NULL;

	pSession = _find_Session(mdaID);
	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return nRet;
	}

	//DSRC_RTSP("[RTSP] RTSP_GetDuration (int)%d (float)%f", (int)(pSession->VodClient.fDuration*1000), pSession->VodClient.fDuration);

	return (long long) (pSession->VodClient.fDuration*1000);
}

int  RTSP_Initialize()
{
	int nRet = -1;
	if(g_pVodSessionRoot){
		DSRC_RTSP("[RTSP] ERROR:already RTSP_Initialize");
		nRet = -1;
	}
	else{
		nRet = 0;
	}

	DSRC_RTSP("[RTSP] SUCCESS:RTSP_Initializes");
	return nRet;
}

int  RTSP_GetVideoPID(int mdaID)
{
	int nRet = 0;
	PRTSP_SESSION pSession = NULL;

	pSession = _find_Session(mdaID);
	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return nRet;
	}

	return pSession->VodClient.vc_info[0].pid;
}

int  RTSP_GetVideoCodec(int mdaID)
{
	int nRet = 0;
	PRTSP_SESSION pSession = NULL;

	pSession = _find_Session(mdaID);
	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return nRet;
	}

	return pSession->VodClient.vc_info[0].codex_id;
}

int  RTSP_GetAudioPID(int mdaID)
{
	int nRet = 0;
	PRTSP_SESSION pSession = NULL;

	pSession = _find_Session(mdaID);
	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return nRet;
	}

	return pSession->VodClient.ac_info[0].pid;
}

int  RTSP_GetAudioCodec(int mdaID)
{
	int nRet = 0;
	PRTSP_SESSION pSession = NULL;

	pSession = _find_Session(mdaID);
	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return nRet;
	}

#if FEATURE_SAMSUNG_STB
	nRet = pSession->VodClient.ac_info[0].codex_id;

	if (nRet == 0x06)
		nRet = 0x81;

	return nRet;
#else
	return pSession->VodClient.ac_info[0].codex_id;
#endif
}

long long  RTSP_GetFirstIFreamePTS(int mdaID)
{
	int nRet = -1;
	long long llFirstIFramePTSTime = 0;
	PRTSP_SESSION pSession = NULL;

	pSession = _find_Session(mdaID);
	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return nRet;
	}

	llFirstIFramePTSTime = (long long)(pSession->VodClient.first_ptsinfo);
	//LOGSRC("llFirstIFramePTSTime:[%lld] [%f]", llFirstIFramePTSTime, pSession->VodClient.first_ptsinfo);

	return llFirstIFramePTSTime;
}

void _clear_HalBuf(PRTSP_SESSION pSession, int nNonflush)
{
//	printf("fldgo _clear_HalBuf !!!!!!\n");

	pSession->nFirstPacket = 1;
	pSession->uuReceiveByte	= 0;
	pSession->uuReceiveBytePerTime = 0;
	gettimeofday( &pSession->timeStart, NULL );
	//pSession->nBOFStatus = 0;
	pSession->nEOFStatus = 0;

	pSession->nBadRequestFeedPauseFlag				= 0; // jung2604 : seek중 400에러가 났을경우 죽는 문제를 해결하기 위해서..
}

void _pause_Stream(PRTSP_SESSION pSession)
{
//	printf("fldgo _pause_Stream !!!!!!\n");

	pSession->nStreamPause = 1;
	//jung2604 : 2019.11.04 : vod 끊김현상으로 pause, continue를 사용하도록 가이드 되었음
	// RecvData_OFF(&pSession->VodClient); //fldgo note : will call
	Pause(&pSession->VodClient);
}

void _resume_Stream(PRTSP_SESSION pSession)
{
//	printf("fldgo _resume_Stream !!!!!!\n");

	pSession->nStreamPause = 0;
	//jung2604 : 2019.11.04 : vod 끊김현상으로 pause, continue를 사용하도록 가이드 되었음
	// RecvData_ON(&pSession->VodClient); //fldgo note : will call
	Continue(&pSession->VodClient);
}

/*
int    Callback_playstatus_event( int mdaID, int nWindowType, PlayerStatusCB p_stat)
{

	int nRet = -1;
	PRTSP_SESSION pSession = NULL;

	pSession = _find_Session(mdaID);
	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return nRet;
	}


	if(1 == pSession->nBOFStatus
	  && 1 == p_stat.nPlaySpeed  && HAL_PLAYING == p_stat.nPlayState){ //BOF

		pSession->nBOFStatus = 0;
		RTSP_Seek(mdaID, 0);
		DSRC_RTSP("[RTSP] BOF get stream!!");
		HMedia_Play( nWindowType );
	}
	else if( 1 == pSession->nEOFStatus && HAL_STOPPED == p_stat.nPlayState){ // EOF
		//RTSP_Close(mdaID);
		DSRC_RTSP("[RTSP] EOF RTSP Success!!");
	}

	return 0;
}
*/


int RTSP_GetIFRStatus(int mdaID)
{
	int nRet = -1;
	PRTSP_SESSION pSession = NULL;

	pSession = _find_Session(mdaID);
	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return nRet;
	}

	return pSession->VodClient.ifr_status;

}

int RTSP_GetSessionStatus(int mdaID)
{
	PRTSP_SESSION pSession = _find_Session(mdaID);

	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return -1;
	}

	return pSession->VodClient.sessionStatus;
}

int RTSP_SetWaitWriteDVR(int mdaID, int nFlag)
{
	PRTSP_SESSION pSession = _find_Session(mdaID);

	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return -1;
	}

	pSession->waitDvrWrite = nFlag;
	pSession->waitDvrWriteCheck = 0;

	return 0;
}

int RTSP_WaitWriteCheckDVR(int mdaID)
{
	PRTSP_SESSION pSession = _find_Session(mdaID);


	int nWaitCount = 0;

	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return -1;
	}


	if (pSession->waitDvrWrite == 1)
	{
		while (nWaitCount++ < 20) //  50ms 씩 20번1초간 체크
		{
			if (pSession->waitDvrWriteCheck == 1)
			{
				break;
			}

			usleep(50000);
		}
	}

	if (nWaitCount >= 20) // dvr write block 이 풀리지 않았거나 무언가오류가 발생했다.
	{
		DSRC_RTSP("[RTSP] WARNING:DVR WRITE check may be error");
		return -1;
	}

	return 0;
}

int RTSP_GetEOFBOFStatus(int mdaID)
{
	PRTSP_SESSION pSession = NULL;

	pSession = _find_Session(mdaID);
	if(NULL == pSession){
		DSRC_RTSP("[RTSP] WARNING:no exist session mdaID:%d", mdaID);
		return -1;
	}

	if(1 == pSession->nEOFStatus) {
		return 1;
	}

	if(1 == pSession->nBOFStatus) {
		return 2;
	}

	return 0;
}

#ifdef __cplusplus
} //extern "C"
#endif
