/*
 * pt_ads.h
 *
 * created by : hunchan.yu
 * Copyright (c) 2020 PIXTREE, Inc.
 * All rights reserved.
 *
 */
 
#ifndef __PT_ADS_CLIENT_H__
#define __PT_ADS_CLIENT_H__


#if _WIN32
#ifdef ADS_DLL_BUILD
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT __declspec(dllimport)
#endif
#else
#define DLL_EXPORT
#endif // ~_WIN32


typedef void* ADS_HANDLE;



#ifdef __cplusplus 
extern "C" {
#endif

DLL_EXPORT void PIX_ADS_GetVersion(int *pVersion);

DLL_EXPORT ADS_HANDLE PIX_ADS_Initialize(int iId, char *pMulticastIP, char *pMulticastIpPort, int iOpMode);

DLL_EXPORT void PIX_ADS_Uninitialize(ADS_HANDLE pHandle);
DLL_EXPORT int PIX_ADS_PutPackets(ADS_HANDLE pHandle, unsigned char *pPackets, unsigned int iPacketSize);
DLL_EXPORT int PIX_ADS_GetPackets(ADS_HANDLE pHandle, unsigned char *pPackets, unsigned int iPacketSize);
DLL_EXPORT void PIX_ADS_NotifyNextBufferStatus(ADS_HANDLE pHandle, int iBufferSize, int iFilledSize);
DLL_EXPORT int PIX_ADS_SetOperateMode(ADS_HANDLE pHandle, int iOpMode);
DLL_EXPORT int PIX_ADS_GetOperateMode(ADS_HANDLE pHandle);
DLL_EXPORT int PIX_ADS_GetLastError(ADS_HANDLE pHandle);


#ifdef __cplusplus 
}
#endif

#endif


