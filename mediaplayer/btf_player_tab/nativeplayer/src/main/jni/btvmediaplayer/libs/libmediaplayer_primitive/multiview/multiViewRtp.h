// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef _____________MULTIVIEW_RTP_H___________________
#define _____________MULTIVIEW_RTP_H___________________

#ifdef __cplusplus
extern "C" {
#endif

#include "utils/RtpInfoDefine.h"

void *multiview_recv_thread(void *ThreadParam);
void *multiview_feed_thread(void *ThreadParam);
void multiview_init();
void multiview_final();
//void multiview_selectChannel(dvb_player_t* player, int channelIdx);

#ifdef __cplusplus
}
#endif

#endif //_____________MULTIVIEW_RTP_H___________________
