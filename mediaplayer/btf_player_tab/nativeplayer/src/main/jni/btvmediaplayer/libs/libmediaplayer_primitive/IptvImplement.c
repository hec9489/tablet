// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <sys/poll.h>

#undef NDEBUG

#include "dvb_primitive.h"
#include <dbg.h>
#include <rtp.h>
	
#include "IptvImplement.h"
#include <systemproperty.h>
extern RtpInfo_t *gRtpInfo[];

#define NOT_USE_CHANGE_CHANNEL "/btf/btvmedia/log/iptvserver_nochange"

int gNotChangeChannel = 0; // change channel ???????? ????

#define FL_LOGCAT_OUT
#define FL_STDOUT_OUT

pthread_mutex_t  mutex = PTHREAD_MUTEX_INITIALIZER;

// 2014.04.30 st ??????? ????? ????
pthread_mutex_t  mutexMulti = PTHREAD_MUTEX_INITIALIZER;

// 2014.04.30 continuity check flag add
int nContinuityCheck = 0;

//////////// log part

#include <sys/syscall.h>

void string_version2() {
	char stVersion[256] ;
	sprintf(stVersion, "sptek__version__libmediaplayer_primitive : 0.9.91");
}

int Android_ThreadID(void)
{
	return (int)syscall(__NR_gettid); // android porting chojjj 20130131*/
}

void strPrintLog(const char *format, ...)
{
	char szDebugTemp[4096] = {0,};

	int nHeaderLength;
	
	struct 		timeval val;
	struct tm 	*ptm = NULL;
	va_list 	va;

	
	gettimeofday( &val, NULL );
	ptm = localtime( &val.tv_sec );
	sprintf(szDebugTemp, "[iptvimpl %02d/%02d %02d:%02d:%02d:%03d][%d] ", ptm->tm_mon + 1, ptm->tm_mday
			,ptm->tm_hour, ptm->tm_min, ptm->tm_sec, (int)val.tv_usec / 1000, Android_ThreadID());

	nHeaderLength = strlen(szDebugTemp);

	va_start(va, format);
	vsprintf(szDebugTemp+strlen(szDebugTemp), format, va);
	va_end(va);

#ifdef FL_LOGCAT_OUT
	verbose("%s", szDebugTemp);
#endif

#ifdef FL_STDOUT_OUT
	printf("%s\n", szDebugTemp);
#endif
}

void ST_InitPlayer(dvb_player_t *player, int idx_player, int jitter,int non_tunneled)
{
	strPrintLog("[impl.c]before call dvb_init_player idx_player [%d]", idx_player);
	dvb_init_player(player, idx_player, jitter, non_tunneled);
	strPrintLog("[impl.c]after call dvb_init_player");
	char dataPath[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
	strcat(dataPath, NOT_USE_CHANGE_CHANNEL);

	if (idx_player == 0) {
		if (0 == access(dataPath, F_OK)){
			gNotChangeChannel = 1;
		}
		else 
		{
			gNotChangeChannel = 0;
		}
	}

    #if 0//def USE_MLR
    if(idx_player == 0)
    {
      int mlr_ret=0;
      DIR* dir = NULL;
      char mlr_dirbuf[256];
      char mlr_dataPath[128];
      #define MLR_STORAGE_NAME "btf/btvmedia/mlr_client"
      get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, mlr_dataPath, 128);
      sprintf(mlr_dirbuf, "%s/%s", mlr_dataPath, MLR_STORAGE_NAME);
      dir = opendir(mlr_dirbuf);
      if(dir == NULL)
      { 
        strPrintLog("%s folder not exist.. mkdir ",MLR_STORAGE_NAME);
        mkdir(mlr_dirbuf, 0700); 
        chmod(mlr_dirbuf, 0700);
      }
      mlr_ret = MLR_set_storage_path(mlr_dirbuf);
      strPrintLog("MLR_set_storage_path ret = %d",mlr_ret);
    }
    #endif
}

void ST_SetWindowSize(dvb_player_t *player, int x, int y, int width, int height)
{
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	strPrintLog("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);

	strPrintLog("[impl.c]before call dvb_resize_video_plane device [%d] x[%d] y[%d] width[%d], height[%d]", player->idx_player, x, y, width, height);
	/* jung2604
	dvb_resize_video_plane(player, x, y, width, height);
	 */
#ifdef USE_BTV_HAL_MANAGER
	HalBtv_SetVideoPlaneSize(player->idx_player, x, y, width, height);		
#else
	AVP_SetVideoPlaneSize(player->avpInfo.playerHandle, x, y, width, height);
#endif
	strPrintLog("[impl.c]after call dvb_resize_video_plane");

	strPrintLog("[impl.c][DEVICE %d] %s END [%d]", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
}
 
int ST_setPlayerSurfaceView(dvb_player_t *player, int deviceID, uint32_t nativWindows)
{
	//Soc_SetPlayerSurfaceView(player, deviceID, nativWindows);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	strPrintLog("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] %s END [%d]", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	return 0;
}

int ST_setPlayerSurface(dvb_player_t *player, int deviceID, const void *m_pAnative)
{
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	strPrintLog("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);
	HalBtv_SetSurface(player->idx_player, m_pAnative);
	strPrintLog("[impl.c][DEVICE %d] %s END [%d]", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	return 0;
}

int ST_PlayerGetBufferStatus (dvb_player_t *player)
{
	int ret;
	RtpInfo_t *pRtpInfo;
    pRtpInfo = gRtpInfo[player->idx_player];

	if (pRtpInfo->buf_readOneOrMoreData == 0) // case not received data
	{
		return -1; // case bad
	}

	if(player->idx_player == 0 && player->isMultiview && player->isFeedMultiviewData != 0) {
		logLogCatKey_stdvb("[TEST] get isFeedMultiviewData = %d", player->isFeedMultiviewData);
		if(player->isFeedMultiviewData == 1) return -1;
		player->isFeedMultiviewData = 1;
	}

	// case good
	pRtpInfo->buf_readOneOrMoreData = 0;
	return 0;
}

int SPTEK_TuneMultiviewChannel(dvb_player_t *player, dvb_locator_t *ptLocators, AVP_MediaCallback pSoc_player_callback)
{
	int ret = 0, nErrorCode = 0;
	//dvb_locator_t *locator = &ptLocators[0];

	logLogCatKey_stdvb("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	logLogCatKey_stdvb("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);

	player->enable_clock_recovery = true;
	player->nStopFlag = 0; // reset to clear

	logLogCatKey_stdvb("[impl.c]before call dvb_device_open device [%d] line [%d]", player->idx_player, __LINE__);
	ret = dvb_device_open(player, pSoc_player_callback, &ptLocators[0]);
	logLogCatKey_stdvb("[impl.c]after call dvb_device_open");

	if (ret != 0) {
		dry_check(!ret,"failed dvb_device_open()");

		logLogCatKey_stdvb("[impl.c][DEVICE %d] %s ERROR END [%d]", player->idx_player, __func__, -1);
		logLogCatKey_stdvb("[impl.c][DEVICE %d] #########################################################", player->idx_player);

		return -1;
	}

	logLogCatKey_stdvb("[impl.c]before call pthread_mutex_lock [%d] line [%d]", player->idx_player, __LINE__);
	pthread_mutex_lock(&mutexMulti);
	logLogCatKey_stdvb("[impl.c]after call pthread_mutex_lock");

	logLogCatKey_stdvb("[impl.c]before call dvb_tuner_lock device [%d]", player->idx_player);
	ret = dvb_tuner_lock(player, ptLocators); // jung2604 : 소켓 생성 및 데이터 받아오는 스레드 동작시키는 함수
	logLogCatKey_stdvb("[impl.c]after call dvb_tuner_lock ret: [%d]", ret);

	if (ret != 0)
	{
		nErrorCode = -5;
		check(!ret,"failed dvb_tuner_lock()");
	}

	logLogCatKey_stdvb("[impl.c]before call pthread_mutex_unlock [%d] line [%d]", player->idx_player, __LINE__);
	pthread_mutex_unlock(&mutexMulti);
	logLogCatKey_stdvb("[impl.c]after call pthread_mutex_unlock");

	logLogCatKey_stdvb("[impl.c][DEVICE %d] %s SUCCESS END", player->idx_player, __func__);
	logLogCatKey_stdvb("[impl.c][DEVICE %d] #########################################################", player->idx_player);

	return 0;

error:

	strPrintLog("[impl.c]before call pthread_mutex_unlock [%d] line [%d]", player->idx_player, __LINE__);
	pthread_mutex_unlock(&mutexMulti);
	strPrintLog("[impl.c]after call pthread_mutex_unlock");

	strPrintLog("[impl.c]before call dvb_device_close device [%d] line [%d]", player->idx_player, __LINE__);
	dvb_device_close(player);
	strPrintLog("[impl.c]after call dvb_device_close");

	strPrintLog("[impl.c][DEVICE %d] %s ERROR END [%d]", player->idx_player, __func__, nErrorCode);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

	return nErrorCode;
}

int ST_TuneChannel(dvb_player_t *player, dvb_locator_t *locator_head, AVP_MediaCallback pSoc_player_callback)
{  
    int ret = 0, nErrorCode = 0;	
    dvb_locator_t *locator = locator_head;

	logLogCatKey_stdvb("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	logLogCatKey_stdvb("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);

	player->enable_clock_recovery = true;
	player->nStopFlag = 0; // reset to clear

	logLogCatKey_stdvb("[impl.c]before call pthread_mutex_lock [%d] line [%d]", player->idx_player, __LINE__);
	pthread_mutex_lock(&mutexMulti);
	logLogCatKey_stdvb("[impl.c]after call pthread_mutex_lock");

	logLogCatKey_stdvb("[impl.c]before call dvb_tuner_lock device [%d]", player->idx_player);
	ret = dvb_tuner_lock(player, locator); // jung2604 : 소켓 생성 및 데이터 받아오는 스레드 동작시키는 함수
	logLogCatKey_stdvb("[impl.c]after call dvb_tuner_lock ret: [%d]", ret);

	if (ret != 0)
	{
		nErrorCode = -5;
		check(!ret,"failed dvb_tuner_lock()");
	}

	logLogCatKey_stdvb("[impl.c]before call pthread_mutex_unlock [%d] line [%d]", player->idx_player, __LINE__);
	pthread_mutex_unlock(&mutexMulti);
	logLogCatKey_stdvb("[impl.c]after call pthread_mutex_unlock");

    ret = dvb_device_open(player, pSoc_player_callback, locator_head);
	logLogCatKey_stdvb("[impl.c]after call dvb_device_open");

	if (ret != 0)
	{
		dry_check(!ret,"failed dvb_device_open()");

		logLogCatKey_stdvb("[impl.c][DEVICE %d] %s ERROR END [%d]", player->idx_player, __func__, -1);
		logLogCatKey_stdvb("[impl.c][DEVICE %d] #########################################################", player->idx_player);

		return -1;		
	}

	logLogCatKey_stdvb("[impl.c][DEVICE %d] %s SUCCESS END", player->idx_player, __func__);
	logLogCatKey_stdvb("[impl.c][DEVICE %d] #########################################################", player->idx_player);

    return 0;

error:

	strPrintLog("[impl.c]before call pthread_mutex_unlock [%d] line [%d]", player->idx_player, __LINE__);
	pthread_mutex_unlock(&mutexMulti);
	strPrintLog("[impl.c]after call pthread_mutex_unlock");
	
	strPrintLog("[impl.c]before call dvb_device_close device [%d] line [%d]", player->idx_player, __LINE__);
	dvb_device_close(player);
	strPrintLog("[impl.c]after call dvb_device_close");

	strPrintLog("[impl.c][DEVICE %d] %s ERROR END [%d]", player->idx_player, __func__, nErrorCode);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	
    return nErrorCode;
}

int ST_ChangeChannel(dvb_player_t *player, dvb_locator_t *locator_head, AVP_MediaCallback pSoc_player_callback)
{  
    int ret = 0, nErrorCode = 0;
    dvb_locator_t *locator = locator_head;

	logLogCatKey_stdvb("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	logLogCatKey_stdvb("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);

	player->enable_clock_recovery = true;

/* TODO : jung2604 : 사용하지 않는듯...
	if (gNotChangeChannel)
	{
		ret = ST_CloseChannel(player, locator_head);
		if (ret != 0)
		{
			strPrintLog("[impl.c][DEVICE %d] %s ERROR END [%d]", player->idx_player, __func__, ret);
			strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
			return ret;
		}
	
		ret = ST_TuneChannel(player, locator_head, &pSoc_player_callback);
		strPrintLog("[impl.c][DEVICE %d] %s END [%d]", player->idx_player, __func__, ret);
		strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

		return ret;
	}
*/
	
	logLogCatKey_stdvb("[impl.c]before call pthread_mutex_lock [%d] line [%d]", player->idx_player, __LINE__);
	pthread_mutex_lock(&mutexMulti);
	logLogCatKey_stdvb("[impl.c]after call pthread_mutex_lock");

	if (player->nStopFlag == 0) // already not stopped
	{
#ifdef STB_BMT_LEAVE_10MS_JOIN_TEST_BULLSHIT_NOSENSEINDOING
		logLogCatKey_stdvb("[impl.c]before call dvb_iptuner_unlock_silent device [%d] line [%d]", player->idx_player, __LINE__);
		dvb_iptuner_unlock_silent(player);
		logLogCatKey_stdvb("[impl.c]after call dvb_iptuner_unlock_silent");

		logLogCatKey_stdvb("[impl.c]before call dvb_wait_iptuner_unlock_silent device [%d] line [%d]", player->idx_player, __LINE__);
		dvb_wait_iptuner_unlock_silent(player);
		logLogCatKey_stdvb("[impl.c]after call dvb_wait_iptuner_unlock_silent");
#else
		logLogCatKey_stdvb("[impl.c]before call dvb_tuner_unlock device [%d] line [%d]", player->idx_player, __LINE__);
		dvb_tuner_unlock(player, locator);
		logLogCatKey_stdvb("[impl.c]after call dvb_tuner_unlock");

		logLogCatKey_stdvb("[impl.c]before call dvb_wait_tuner_unlock device [%d] line [%d]", player->idx_player, __LINE__);
		dvb_wait_tuner_unlock(player, locator);
		logLogCatKey_stdvb("[impl.c]after call dvb_wait_tuner_unlock");
#endif
	}

	logLogCatKey_stdvb("[impl.c]before call dvb_tuner_lock device [%d] line [%d]", player->idx_player, __LINE__);
	ret = dvb_tuner_lock(player, locator);
	logLogCatKey_stdvb("[impl.c]after call dvb_tuner_lock");

    if (ret != 0)
    {
              nErrorCode = -6;
#ifdef STB_BMT_LEAVE_10MS_JOIN_TEST_BULLSHIT_NOSENSEINDOING
                dvb_close_silent_byerror(player); 
#endif
        check(!ret,"failed dvb_tuner_lock()");
	}

	logLogCatKey_stdvb("[impl.c]before call pthread_mutex_unlock [%d] line [%d]", player->idx_player, __LINE__);
	pthread_mutex_unlock(&mutexMulti);
	logLogCatKey_stdvb("[impl.c]after call pthread_mutex_unlock");

	dvb_device_close(player);

	player->nStopFlag = 0; // reset to clear

	ret = dvb_device_open(player, pSoc_player_callback, locator_head);

	if (ret != 0)
	{
		dry_check(!ret,"failed dvb_device_open()");

		logLogCatKey_stdvb("[impl.c][DEVICE %d] %s ERROR END [%d]", player->idx_player, __func__, -1);
		logLogCatKey_stdvb("[impl.c][DEVICE %d] #########################################################", player->idx_player);

		return -1;
	}

	logLogCatKey_stdvb("[impl.c][DEVICE %d] %s SUCCESS END", player->idx_player, __func__);
	logLogCatKey_stdvb("[impl.c][DEVICE %d] #########################################################", player->idx_player);
   
	return 0;
error:

	strPrintLog("[impl.c]before call pthread_mutex_unlock [%d] line [%d]", player->idx_player, __LINE__);
	pthread_mutex_unlock(&mutexMulti);
	strPrintLog("[impl.c]after call pthread_mutex_unlock");

	strPrintLog("[impl.c]before call dvb_device_close device [%d] line [%d]", player->idx_player, __LINE__);
	dvb_device_close(player);
	strPrintLog("[impl.c]after call dvb_device_close");

	strPrintLog("[impl.c][DEVICE %d] %s ERROR END [%d]", player->idx_player, __func__, nErrorCode);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
    
	return nErrorCode;
}

int ST_CloseChannel(dvb_player_t *player, dvb_locator_t *locator_head)
{
	int ret = 0, nErrorCode = 0;
	dvb_locator_t *locator = locator_head;

	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	strPrintLog("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);

	strPrintLog("[impl.c]before call pthread_mutex_lock [%d] line [%d]", player->idx_player, __LINE__);
	pthread_mutex_lock(&mutexMulti);
	strPrintLog("[impl.c]after call pthread_mutex_lock");

	if (player->nStopFlag == 0) // already not stopped
	{
		strPrintLog("[impl.c]before call dvb_tuner_unlock device [%d] line [%d]", player->idx_player, __LINE__);
		dvb_tuner_unlock(player, locator);
		strPrintLog("[impl.c]after call dvb_tuner_unlock");

        strPrintLog("[impl.c]before call dvb_wait_tuner_unlock device [%d] line [%d]", player->idx_player, __LINE__);
		dvb_wait_tuner_unlock(player, locator);
		strPrintLog("[impl.c]after call dvb_wait_tuner_unlock");
	}
	
	player->nStopFlag = 0; // reset to clear

	strPrintLog("[impl.c]before call pthread_mutex_unlock [%d] line [%d]", player->idx_player, __LINE__);
	pthread_mutex_unlock(&mutexMulti);
	strPrintLog("[impl.c]after call pthread_mutex_unlock");

	strPrintLog("[impl.c]before call dvb_device_close device [%d] line [%d]", player->idx_player, __LINE__);
	ret = dvb_device_close(player);
	strPrintLog("[impl.c]after call dvb_device_close");

	if (ret != 0)
	{
		nErrorCode = -3;
	    check(!ret,"failed dvb_device_close()");
	}

	ST_ReleasePlayer(player);

	strPrintLog("[impl.c][DEVICE %d] %s SUCCESS END", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

	return 0;
error:

	strPrintLog("[impl.c]before call pthread_mutex_unlock [%d] line [%d]", player->idx_player, __LINE__);
	pthread_mutex_unlock(&mutexMulti);
	strPrintLog("[impl.c]after call pthread_mutex_unlock");

	strPrintLog("[impl.c][DEVICE %d] %s ERROR END [%d]", player->idx_player, __func__, nErrorCode);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
        
    return nErrorCode;
}

int ST_StopChannel(dvb_player_t *player, dvb_locator_t *locator_head)
{  
    int ret = 0, nErrorCode = 0;
    dvb_locator_t *locator = locator_head;

	if (player->nStopFlag == 1) // already stopped
	{
		strPrintLog("[impl.c][DEVICE %d] already stopped!", player->idx_player);
		return 0;
	}

	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	strPrintLog("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);

	strPrintLog("[impl.c]before call pthread_mutex_lock [%d] line [%d]", player->idx_player, __LINE__);
	pthread_mutex_lock(&mutexMulti);
	strPrintLog("[impl.c]after call pthread_mutex_lock");

	strPrintLog("[impl.c]before call dvb_tuner_unlock device [%d] line [%d]", player->idx_player, __LINE__);
	dvb_tuner_unlock(player, locator);
	strPrintLog("[impl.c]after call dvb_tuner_unlock");

	strPrintLog("[impl.c]before call dvb_wait_tuner_unlock device [%d] line [%d]", player->idx_player, __LINE__);
	dvb_wait_tuner_unlock(player, locator);
	strPrintLog("[impl.c]after call dvb_wait_tuner_unlock");

	// will set stop flag
	player->nStopFlag = 1;

	strPrintLog("[impl.c]before call pthread_mutex_unlock [%d] line [%d]", player->idx_player, __LINE__);
	pthread_mutex_unlock(&mutexMulti);
	strPrintLog("[impl.c]after call pthread_mutex_unlock");

	strPrintLog("[impl.c][DEVICE %d] %s SUCCESS END", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
   
	return 0;
error:

	strPrintLog("[impl.c]before call pthread_mutex_unlock [%d] line [%d]", player->idx_player, __LINE__);
	pthread_mutex_unlock(&mutexMulti);
	strPrintLog("[impl.c]after call pthread_mutex_unlock");

	strPrintLog("[impl.c]before call dvb_device_close device [%d] line [%d]", player->idx_player, __LINE__);
	dvb_device_close(player);
	strPrintLog("[impl.c]after call dvb_device_close");

	strPrintLog("[impl.c][DEVICE %d] %s ERROR END [%d]", player->idx_player, __func__, nErrorCode);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
    
	return nErrorCode;
}

int ST_StopChannelSilent(dvb_player_t *player, dvb_locator_t *locator_head)
{

    logLogCatKey_stdvb("[impl.c][DEVICE %d] ST_StopChannelSilent", player->idx_player);
    int ret = 0, nErrorCode = 0;
    dvb_locator_t *locator = locator_head;

	if (player->nStopFlag == 1) // already stopped
	{
        logLogCatKey_stdvb("[impl.c][DEVICE %d] already stopped!", player->idx_player);
		return 0;
	}

    logLogCatKey_stdvb("[impl.c][DEVICE %d] #########################################################", player->idx_player);
    logLogCatKey_stdvb("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);

    logLogCatKey_stdvb("[impl.c]before call pthread_mutex_lock [%d] line [%d]", player->idx_player, __LINE__);
	pthread_mutex_lock(&mutexMulti);
    logLogCatKey_stdvb("[impl.c]after call pthread_mutex_lock");
	
#ifdef STB_BMT_LEAVE_10MS_JOIN_TEST_BULLSHIT_NOSENSEINDOING
	logLogCatKey_stdvb("[impl.c]before call dvb_iptuner_unlock_silent device [%d] line [%d]", player->idx_player, __LINE__);
	dvb_iptuner_unlock_silent(player);
	logLogCatKey_stdvb("[impl.c]after call dvb_iptuner_unlock_silent");
#else
	logLogCatKey_stdvb("[impl.c]before call dvb_tuner_unlock device [%d] line [%d]", player->idx_player, __LINE__);
	dvb_tuner_unlock(player, locator);
	logLogCatKey_stdvb("[impl.c]after call dvb_tuner_unlock");
#endif
			
#ifdef STB_BMT_LEAVE_10MS_JOIN_TEST_BULLSHIT_NOSENSEINDOING
	logLogCatKey_stdvb("[impl.c]before call dvb_wait_iptuner_unlock_silent device [%d] line [%d]", player->idx_player, __LINE__);
	dvb_wait_iptuner_unlock_silent(player);
	logLogCatKey_stdvb("[impl.c]after call dvb_wait_iptuner_unlock_silent");
#else
	logLogCatKey_stdvb("[impl.c]before call dvb_wait_tuner_unlock device [%d] line [%d]", player->idx_player, __LINE__);
	dvb_wait_tuner_unlock(player, locator);
	logLogCatKey_stdvb("[impl.c]after call dvb_wait_tuner_unlock");
#endif

	// will set stop flag
	player->nStopFlag = 1;

	strPrintLog("[impl.c]before call pthread_mutex_unlock [%d] line [%d]", player->idx_player, __LINE__);
	pthread_mutex_unlock(&mutexMulti);
	strPrintLog("[impl.c]after call pthread_mutex_unlock");

	strPrintLog("[impl.c][DEVICE %d] %s SUCCESS END", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
   
	return 0;
error:

	strPrintLog("[impl.c]before call pthread_mutex_unlock [%d] line [%d]", player->idx_player, __LINE__);
	pthread_mutex_unlock(&mutexMulti);
	strPrintLog("[impl.c]after call pthread_mutex_unlock");

	strPrintLog("[impl.c]before call dvb_device_close device [%d] line [%d]", player->idx_player, __LINE__);
	dvb_device_close(player);
	strPrintLog("[impl.c]after call dvb_device_close");

	strPrintLog("[impl.c][DEVICE %d] %s ERROR END [%d]", player->idx_player, __func__, nErrorCode);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
    
	return nErrorCode;
}

int ST_OpenTsFile(dvb_player_t *player, dvb_locator_t *locator_head, int nWillOpenDevice, AVP_MediaCallback pSoc_player_callback)
{
	int ret = 0, nErrorCode = 0;
	dvb_locator_t *locator = locator_head;

	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	strPrintLog("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);
	
	player->enable_clock_recovery = false;

	//CODEC 정보를 찾는다
	strPrintLog("[impl.c]before call dvb_set_stream_info device [%d] line [%d]", player->idx_player, __LINE__);
	ret = dvb_set_stream_info(player, locator);
	strPrintLog("[impl.c]after call dvb_set_stream_info");

	if (nWillOpenDevice == 1)
	{	
		strPrintLog("[impl.c]before call dvb_device_open device [%d] line [%d]", player->idx_player, __LINE__);
		ret = dvb_device_open(player, pSoc_player_callback, locator_head);
		strPrintLog("[impl.c]after call dvb_device_open");

		if (ret != 0)
		{
			dry_check(!ret,"failed dvb_device_open()");
			
			strPrintLog("[impl.c][DEVICE %d] %s ERROR END [%d]", player->idx_player, __func__, -1);
			strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

			return -1;		
		}
	}

	if (ret != 0)
	{
		nErrorCode = -3;
		check(!ret,"failed dvb_set_stream_info");
	}

	strPrintLog("[impl.c][DEVICE %d] %s SUCCESS END", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

    return 0;

error:

	strPrintLog("[impl.c]before call dvb_device_close device [%d] line [%d]", player->idx_player, __LINE__);
	dvb_device_close(player);
	strPrintLog("[impl.c]after call dvb_device_close");

	strPrintLog("[impl.c][DEVICE %d] %s ERROR END [%d]", player->idx_player, __func__, nErrorCode);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

    return nErrorCode;
}


int ST_OpenRTSP(dvb_player_t *player, dvb_locator_t *locator_head, int nWillOpenDevice, AVP_MediaCallback pSoc_player_callback)
{
	int ret = 0, nErrorCode = 0;
	dvb_locator_t *locator = locator_head;

	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	strPrintLog("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);

	player->enable_clock_recovery = false;

	if (nWillOpenDevice == 1)
	{	
		strPrintLog("[impl.c]before call dvb_device_open device [%d] line [%d]", player->idx_player, __LINE__);
		ret = dvb_device_open(player, pSoc_player_callback, locator_head);
		strPrintLog("[impl.c]after call dvb_device_open");

		if (ret != 0)
		{
			dry_check(!ret,"failed dvb_device_open()");

			strPrintLog("[impl.c][DEVICE %d] %s ERROR END [%d]", player->idx_player, __func__, -1);
			strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

			return -1;		
		}
	}

	strPrintLog("[impl.c][DEVICE %d] %s SUCCESS END", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

    return 0;

error:

	strPrintLog("[impl.c]before call dvb_device_close device [%d] line [%d]", player->idx_player, __LINE__);
	dvb_device_close(player);
	strPrintLog("[impl.c]after call dvb_device_close");

	strPrintLog("[impl.c][DEVICE %d] %s ERROR END [%d]", player->idx_player, __func__, nErrorCode);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

    return nErrorCode;
}

int ST_PlayStDvbBuffer(dvb_player_t *player, dvb_locator_t *locator_head)
{
	int ret = 0, nErrorCode = 0;
	dvb_locator_t *locator = locator_head;

	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	strPrintLog("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);

	strPrintLog("[impl.c]before call dvb_tuner_lock device [%d] line [%d]", player->idx_player, __LINE__);
	ret = dvb_tuner_lock(player, locator);
	strPrintLog("[impl.c]after call dvb_tuner_lock");

	if (ret != 0)
	{
		nErrorCode = -1;
		check(!ret,"failed dvb_tuner_lock()");
	}

	strPrintLog("[impl.c][DEVICE %d] %s SUCCESS END", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

    return 0;

error:

	strPrintLog("[impl.c][DEVICE %d] %s ERROR END [%d]", player->idx_player, __func__, nErrorCode);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	
	return nErrorCode;
}


int ST_StopStDvbBuffer(dvb_player_t *player, dvb_locator_t *locator_head)
{
	int ret = 0, nErrorCode = 0;
	dvb_locator_t *locator = locator_head;

	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	strPrintLog("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);

	strPrintLog("[impl.c]before call dvb_tuner_unlock device [%d] line [%d]", player->idx_player, __LINE__);
	ret = dvb_tuner_unlock(player, locator);
	strPrintLog("[impl.c]after call dvb_tuner_unlock");
	
	if (ret != 0)
	{
		nErrorCode = -1;
		check(!ret,"failed dvb_tuner_unlock()");
	}

    strPrintLog("[impl.c]before call dvb_wait_tuner_unlock device [%d] line [%d]", player->idx_player, __LINE__);
	dvb_wait_tuner_unlock(player, locator);
	strPrintLog("[impl.c]after call dvb_wait_tuner_unlock");

	strPrintLog("[impl.c][DEVICE %d] %s SUCCESS END", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

	return 0;

error:

	strPrintLog("[impl.c][DEVICE %d] %s ERROR END [%d]", player->idx_player, __func__, nErrorCode);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

    return nErrorCode;
 
}

int ST_PauseBuffer(dvb_player_t *player)
{
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	strPrintLog("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);

#ifdef USE_BTV_HAL_MANAGER
	if(HalBtv_Pause(player->idx_player) != 0) goto error;
#else
	if(AVP_Pause(player->avpInfo.playerHandle) != 0) goto error;
#endif
	strPrintLog("[impl.c][DEVICE %d] %s SUCCESS END", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

    return 0;

error:

	strPrintLog("[impl.c][DEVICE %d] %s ERROR END", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

	return -1;
 
}

int ST_ResumeBuffer(dvb_player_t *player)
{
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	strPrintLog("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);
#ifdef USE_BTV_HAL_MANAGER
	if(HalBtv_Resume(player->idx_player) != 0) goto error;
#else
	if(AVP_Resume(player->avpInfo.playerHandle) != 0) goto error;
#endif

	strPrintLog("[impl.c][DEVICE %d] %s SUCCESS END", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

    return 0;

error:

	strPrintLog("[impl.c][DEVICE %d] %s ERROR END [%d]", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

    return -1;
 
}

int ST_FlushAVBuffer(dvb_player_t *player)
{
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	strPrintLog("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);
#ifdef USE_BTV_HAL_MANAGER
	if(HalBtv_FlushTS(player->idx_player) != 0) goto error;
#else
	if(AVP_FlushTS(player->avpInfo.playerHandle) != 0) goto error;
#endif
	strPrintLog("[impl.c][DEVICE %d] %s END ", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

error:

	strPrintLog("[impl.c][DEVICE %d] %s ERROR END [%d]", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

	return -1;
}

int ST_CloseDevice(dvb_player_t *player)
{
	int ret;

	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	strPrintLog("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);


	strPrintLog("[impl.c]before call dvb_device_close [%d] line [%d]", player->idx_player, __LINE__);
	ret = dvb_device_close(player);
	ST_ReleasePlayer(player);
	strPrintLog("[impl.c]after call dvb_device_close");

	strPrintLog("[impl.c][DEVICE %d] %s END [%d]", player->idx_player, __func__, ret);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

	return ret;
}


int ST_ChangeAudio(dvb_player_t *player, dvb_locator_t *locator)
{
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	strPrintLog("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);

	AVP_AudioConfig audioConfig;
	player->avpInfo.playerConfig.audio_pid = audioConfig.pid = (uint16_t) locator->aid;
	player->avpInfo.playerConfig.audio_codec = audioConfig.codec = (AVP_Codec) locator->acodec;

#ifdef USE_BTV_HAL_MANAGER
	HalBtv_SetAudioStream(player->idx_player, &audioConfig);
#else
	AVP_SetAudioStream(player->avpInfo.playerHandle, &audioConfig);
#endif

	strPrintLog("[impl.c][DEVICE %d] %s SUCCESS END", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

    return 0;
}

int ST_IsFilePlayEOFReached(dvb_player_t *player)
{
	if (player->EOFReached == true)
	{
		player->EOFReached = false;
		return 1;
	}

	else 
		return 0;
}

long long ST_GetTimeStamp (dvb_player_t *player)
{
	int ret;

	long long pts = 0;
	if(player != NULL) {
#ifdef USE_BTV_HAL_MANAGER
    	if(HalBtv_GetVideoPTS(player->idx_player, &pts) == 0)  
#else
    	if(AVP_GetVideoPTS(player->avpInfo.playerHandle, &pts) == 0)  
#endif
		{
			//if(pts != 0) return pts;
			return pts;
		}
	}
	return 0LL;
}

int ST_ChangeGlobalAudioMute(dvb_player_t *player) {
	int ret = 0;

	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	strPrintLog("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);

	int isMute = player->enableGlobalMute;

	bool oldMuteState = false;
#ifdef USE_BTV_HAL_MANAGER
	ret = HalBtv_GetAudioMute(player->idx_player, &oldMuteState);
#else
	ret = AVP_GetAudioMute(player->avpInfo.playerHandle, &oldMuteState);
#endif
	if (0 == ret)  {
		// global mute가 1이면 무조건...Mute시킨다..
		if(player->enableGlobalMute == 1) {
			logLogCatKey_stdvb("++%s[%d] enableGlobalMute !!!!", __FUNCTION__, player->idx_player);
		}

		//unmute일경우 이전값이 unmute일때만 풀어준다.
		if(oldMuteState != player->enableGlobalMute && (player->enableGlobalMute || (player->enableGlobalMute == player->lastMuteValueForGlobalMute))) {
			//HRESULT ret = AMP_SND_SetMute(AMP_SND_PATH_71, isMute ? 1:0);
			//AMP_SND_SetMute(AMP_SND_PATH_HDMI, isMute ? 1:0); //HDMI mute
#ifdef USE_BTV_HAL_MANAGER
			ret = HalBtv_SetAudioMute(player->idx_player, isMute);
#else
			ret = AVP_SetAudioMute(player->avpInfo.playerHandle, isMute);
#endif
			if (0 == ret)  {
				logLogCatKey_stdvb("++%s[%d]  %s audio\n", __FUNCTION__, player->idx_player, isMute ? "mute" : "unmute");
			} else logLogCatKey_stdvb("++%s[%d]  failed to call AMP_SND_SetMute, r = 0x%x\n", __FUNCTION__, player->idx_player, ret);

			//if(!isMute) setHdmiFormat(player); //Mute가 풀릴때 설정해주자..Multiview에서 유용하게..쓰인다.

		} else logLogCatKey_stdvb("++%s[%d]  already is mute %s", __FUNCTION__, player->idx_player, isMute ? "enable" : "disable");
	} else logLogCatKey_stdvb("++%s[%d]  failed to call AMP_SND_GetMute, r = 0x%x\n", __FUNCTION__, player->idx_player, ret);

	if(player->idx_player == 0){
#ifdef USE_BTV_HAL_MANAGER
//		HalBtv_SetMasterAudioMute(isMute);
#else
		AMIXER_SetMasterAudioMute(player->avpInfo.amixerHandle, isMute);
#endif
	}

	strPrintLog("[impl.c][DEVICE %d] %s END [%d]", player->idx_player, __func__, ret);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

	return ret;
}

int ST_AudioMute(dvb_player_t *player, int nOnOff)
{
	int ret = 0;

    player->lastMuteValueForGlobalMute = nOnOff ? 1:0;

    nOnOff |= player->enableGlobalMute;
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	strPrintLog("[impl.c][DEVICE %d] %s START, value : %s, enableGlobalMute : %d", player->idx_player, __func__, nOnOff ? "mute":"unmute", player->enableGlobalMute);
#ifdef USE_BTV_HAL_MANAGER
	ret = HalBtv_SetAudioMute(player->idx_player, (nOnOff == 1));
#else
	ret = AVP_SetAudioMute(player->avpInfo.playerHandle, (nOnOff == 1));
#endif
	strPrintLog("[impl.c][DEVICE %d] %s END [%d]", player->idx_player, __func__, ret);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);

	return ret;
}

int ST_ChangeDolbyMode(dvb_player_t *player, AVP_AudioOutputMode mode) {
#ifdef USE_BTV_HAL_MANAGER
	return HalBtv_SetAudioOutputMode(mode);
#else
	return AMIXER_SetAudioOutputMode(player->avpInfo.amixerHandle, mode);
#endif
}

void ST_AspectRatioMode(dvb_player_t *player, AVP_AspectRatioMode mode) {
#ifdef USE_BTV_HAL_MANAGER
	HalBtv_SetScaleRatioMode(mode); //AspectRatio
#else
	VCOMPO_SetScaleRatioMode(player->avpInfo.vcompoHandle, mode); //AspectRatio
#endif
}

int ST_GetClientEvent(dvb_player_t *player)
{
	return dvb_wait_for_video_event_fistframe(player);
}

void ST_SetVideoDelay(dvb_player_t *player, AVP_AVoffset offset) {
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	strPrintLog("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);
#ifdef USE_BTV_HAL_MANAGER
	HalBtv_SetAVoffset(player->idx_player, offset);
#else
	AVP_SetAVoffset(player->avpInfo.vcompoHandle, offset);
#endif
	strPrintLog("[impl.c][DEVICE %d] %s END [%d]", player->idx_player, __func__);
	strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
}

void ST_SetAVOffset(dvb_player_t *player, AVP_AVoffset offset) {
	//strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
	strPrintLog("[impl.c][DEVICE %d] %s START", player->idx_player, __func__);
#ifdef USE_BTV_HAL_MANAGER
	HalBtv_SetAVoffset(player->idx_player, offset);
#else
	AVP_SetAVoffset(player->avpInfo.vcompoHandle, offset);
#endif
	strPrintLog("[impl.c][DEVICE %d] %s END", player->idx_player, __func__);
	//strPrintLog("[impl.c][DEVICE %d] #########################################################", player->idx_player);
}

void ST_ReleasePlayer(dvb_player_t *player)
{
#ifdef USE_BTV_HAL_MANAGER
	HalBtv_Destroy(player->idx_player);
#else
	if(player->avpInfo.playerHandle != NULL)
		AVP_Destroy(player->avpInfo.playerHandle);
	player->avpInfo.playerHandle = NULL;
#endif

}

#ifdef __cplusplus
}
#endif
