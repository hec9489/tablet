// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef _DVB_TABLE_H_
#define _DVB_TABLE_H_

/* C++ support */
/* ----------- */
#ifdef __cplusplus
extern "C" {
#endif

/* Includes */
/* -------- */
//#include <system.h>
//#include <traces.h>
#include "stddefs.h"

/* Traces Macros */
/* ------------- */
#define TRACE_API(fmt,...)     DVBTEST_TRACE(fmt, ##__VA_ARGS__)
#define TRACE_DBG(fmt,...)     DVBTEST_DEBUG(fmt, ##__VA_ARGS__)
#define TRACE_WRN(fmt,...)     DVBTEST_DEBUG(fmt, ##__VA_ARGS__)
#define TRACE_ERR(fmt,...)     DVBTEST_ERROR(fmt, ##__VA_ARGS__)
#define TRACE_COMPONENT_NAME "dvb_table"
  
/* Definitions */
/* ----------- */
#define TABLE_MAX_BUFFER_SIZE  256*1024   /*!< Maximum size of the section buffer reception                        */
#define TABLE_FILTER_DONT_CARE 0x80000000 /*!< Bit mask to take any value for this filter, don't care of the value */
#define TABLE_FILTER_AUTOMATIC 0x40000000 /*!< Bit mask to have the section coming in the right order              */

/* Defaults PIDs */
/* ------------- */
#define TABLE_PAT_PID 0x0000 /*!< This is the default pat pid */
#define TABLE_CAT_PID 0x0001 /*!< This is the default cat pid */
#define TABLE_NIT_PID 0x0010 /*!< This is the default nit pid */
#define TABLE_SDT_PID 0x0011 /*!< This is the default sdt pid */
#define TABLE_BAT_PID 0x0011 /*!< This is the default bat pid */
#define TABLE_EIT_PID 0x0012 /*!< This is the default eit pid */
#define TABLE_TDT_PID 0x0014 /*!< This is the default tdt pid */
#define TABLE_TOT_PID 0x0014 /*!< This is the default tot pid */

/* Tables IDs */
/* ---------- */
#define TABLE_PAT_TABLE_ID     0x00  /*!< Program Association Table                   */
#define TABLE_CAT_TABLE_ID     0x01  /*!< Conditional Access Table                    */
#define TABLE_PMT_TABLE_ID     0x02  /*!< Program Map Table                           */
#define TABLE_DSM_UN_TABLE_ID  0x3B  /*!< DSMCC U-N Messages Table                    */
#define TABLE_DSM_DL_TABLE_ID  0x3C  /*!< DSMCC Download Messages Table               */
#define TABLE_NIT_TABLE_ID     0x40  /*!< Network Info Table (actual transport)       */
#define TABLE_NITO_TABLE_ID    0x41  /*!< Network Info Table (other transport)        */
#define TABLE_SDT_TABLE_ID     0x42  /*!< Service Descriptor Table (actual transport) */
#define TABLE_SDTO_TABLE_ID    0x46  /*!< Service Descriptor Table (other transport)  */
#define TABLE_BAT_TABLE_ID     0x4a  /*!< Bouquet Association Table                   */
#define TABLE_EIT_TABLE_ID     0x4e  /*!< Event Information Table (actual transport)  */
#define TABLE_EITO_TABLE_ID    0x4f  /*!< Event Information Table (other transport)   */
#define TABLE_TDT_TABLE_ID     0x70  /*!< Time Date Table                             */
#define TABLE_RST_TABLE_ID     0x71  /*!< Running Status Table                        */
#define TABLE_ST_TABLE_ID      0x72  /*!< Stuffing Table                              */
#define TABLE_TOT_TABLE_ID     0x73  /*!< Time Offset Table                           */
#define TABLE_AIT_TABLE_ID     0x74  /*!< Application Information Table               */
#define TABLE_INVALID_TABLE    0x100 /*!< Invalid Table                               */

/*! Definition of stream types */
/* --------------------------- */
typedef enum
{
  TABLE_STREAM_RESERVED         = 0x00,
  TABLE_STREAM_VIDEO_MPEG1      = 0x01,
  TABLE_STREAM_VIDEO_MPEG2      = 0x02,
  TABLE_STREAM_AUDIO_MPEG1      = 0x03,
  TABLE_STREAM_AUDIO_MPEG2      = 0x04,
  TABLE_STREAM_PRIVATE_SECTIONS = 0x05,
  TABLE_STREAM_PRIVATE_PES      = 0x06,
  TABLE_STREAM_MHEG             = 0x07,
  TABLE_STREAM_DSM_CC           = 0x08,
  TABLE_STREAM_TYPE_H2221       = 0x09,
  TABLE_STREAM_TYPE_A           = 0x0A,
  TABLE_STREAM_TYPE_B           = 0x0B,
  TABLE_STREAM_TYPE_C           = 0x0C,
  TABLE_STREAM_TYPE_D           = 0x0D,
  TABLE_STREAM_TYPE_AUX         = 0x0E,
  TABLE_STREAM_AUDIO_AAC_ADTS   = 0x0F,
  TABLE_STREAM_VIDEO_MPEG4P2    = 0x10,
  TABLE_STREAM_AUDIO_AAC_LATM   = 0x11,
  TABLE_STREAM_AUDIO_AAC_RAW1   = 0x12,
  TABLE_STREAM_AUDIO_AAC_RAW2   = 0x13,
  TABLE_STREAM_VIDEO_MPEG4      = 0x1B,  
  TABLE_STREAM_AUDIO_BSAC		= 0x22, 
  TABLE_STREAM_VIDEO_HEVCH265	= 0x24,
  TABLE_STREAM_VIDEO_AVS        = 0x42,
  TABLE_STREAM_AUDIO_LPCM       = 0x80,
  TABLE_STREAM_AUDIO_AC3        = 0x81,
  TABLE_STREAM_AUDIO_DTS        = 0x82,
  TABLE_STREAM_AUDIO_MLP        = 0x83,
  TABLE_STREAM_AUDIO_DDPLUS     = 0x84,
  TABLE_STREAM_AUDIO_DTSHD      = 0x85,
  TABLE_STREAM_AUDIO_DTSHD_XLL  = 0x86,
  TABLE_STREAM_AUDIO_EAC3       = 0x87,
  TABLE_STREAM_AUDIO_DTS_HD     = 0x88,
  TABLE_STREAM_AUDIO_DDPLUS_2   = 0xA1,
  TABLE_STREAM_AUDIO_DTSHD_2    = 0xA2,
  TABLE_STREAM_VIDEO_VC1        = 0xEA,
  TABLE_STREAM_AUDIO_WMA        = 0xE6
} TABLE_StreamType_t;

/*! Definition of descriptor types */
/* ------------------------------- */
typedef enum
{
  TABLE_DESCRIPTOR_VIDEO_STREAM                 = 0x02,
  TABLE_DESCRIPTOR_AUDIO_STREAM                 = 0x03,
  TABLE_DESCRIPTOR_HIERARCHY                    = 0x04,
  TABLE_DESCRIPTOR_REGISTRATION                 = 0x05,
  TABLE_DESCRIPTOR_DATA_STREAM_ALIGNMENT        = 0x06,
  TABLE_DESCRIPTOR_TARGET_BACKGROUND_GRID       = 0x07,
  TABLE_DESCRIPTOR_VIDEO_WINDOW                 = 0x08,
  TABLE_DESCRIPTOR_CA                           = 0x09,
  TABLE_DESCRIPTOR_LANGUAGE_NAME                = 0x0A,
  TABLE_DESCRIPTOR_SYSTEM_CLOCK                 = 0x0B,
  TABLE_DESCRIPTOR_MULTIPLEX_BUFFER             = 0x0C,
  TABLE_DESCRIPTOR_COPYRIGHT                    = 0x0D,
  TABLE_DESCRIPTOR_MAX_BITRATE                  = 0x0E,
  TABLE_DESCRIPTOR_PRIVATE_DATA_INDICATOR       = 0x0F,
  TABLE_DESCRIPTOR_SMOOTHING_BUFFER             = 0x10,
  TABLE_DESCRIPTOR_STD                          = 0x11,
  TABLE_DESCRIPTOR_IBP                          = 0x12,
  TABLE_DESCRIPTOR_CAROUSEL_IDENTIFIER          = 0x13,
  TABLE_DESCRIPTOR_ASSOCIATION_TAG              = 0x14,
  TABLE_DESCRIPTOR_DEFERRED_ASSOCIATION_TAG     = 0x15,
  TABLE_DESCRIPTOR_RESERVED_16                  = 0x16,
  TABLE_DESCRIPTOR_NPT_REFERENCE                = 0x17,
  TABLE_DESCRIPTOR_NPT_ENDPOINT                 = 0x18,
  TABLE_DESCRIPTOR_STREAM_MODE                  = 0x19,
  TABLE_DESCRIPTOR_STREAM_EVENT                 = 0x1A,
  TABLE_DESCRIPTOR_MPEG4_VIDEO                  = 0x1B,
  TABLE_DESCRIPTOR_MPEG4_AUDIO                  = 0x1C,
  TABLE_DESCRIPTOR_MPEG4_IOD                    = 0x1D,
  TABLE_DESCRIPTOR_MPEG4_SL                     = 0x1E,
  TABLE_DESCRIPTOR_MPEG4_FMC                    = 0x1F,
  TABLE_DESCRIPTOR_MPEG4_EXTERNAL_ES_ID         = 0x20,
  TABLE_DESCRIPTOR_MPEG4_MUX_CODE               = 0x21,
  TABLE_DESCRIPTOR_MPEG4_FMX_BUFFER_SIZE        = 0x22,
  TABLE_DESCRIPTOR_MPEG4_MULTIPLEX_BUFFER       = 0x23,
  TABLE_DESCRIPTOR_MPEG4_CONTENT_LABELING       = 0x24,
  TABLE_DESCRIPTOR_TVA_METADATA_POINTER         = 0x25,
  TABLE_DESCRIPTOR_TVA_METADATA                 = 0x26,
  TABLE_DESCRIPTOR_TVA_METADATA_STD             = 0x27,
  TABLE_DESCRIPTOR_AVC_VIDEO                    = 0x28,
  TABLE_DESCRIPTOR_AVC_IPMP                     = 0x29,
  TABLE_DESCRIPTOR_AVC_TIMING_AND_HRD           = 0x2A,
  TABLE_DESCRIPTOR_NETWORK_NAME                 = 0x40,
  TABLE_DESCRIPTOR_SERVICE_LIST                 = 0x41,
  TABLE_DESCRIPTOR_STUFFING                     = 0x42,
  TABLE_DESCRIPTOR_SATELLITE_DELIVERY_SYSTEM    = 0x43,
  TABLE_DESCRIPTOR_CABLE_DELIVERY_SYSTEM        = 0x44,
  TABLE_DESCRIPTOR_VBI_DATA                     = 0x45,
  TABLE_DESCRIPTOR_VBI_TELETEXT                 = 0x46,
  TABLE_DESCRIPTOR_BOUQUET_NAME                 = 0x47,
  TABLE_DESCRIPTOR_SERVICE                      = 0x48,
  TABLE_DESCRIPTOR_COUNTRY_AVAIBILITY           = 0x49,
  TABLE_DESCRIPTOR_LINKAGE                      = 0x4A,
  TABLE_DESCRIPTOR_NVOD_REFERENCE               = 0x4B,
  TABLE_DESCRIPTOR_TIME_SHIFTED                 = 0x4C,
  TABLE_DESCRIPTOR_SHORT_EVENT                  = 0x4D,
  TABLE_DESCRIPTOR_EXTENDED_EVENT               = 0x4E,
  TABLE_DESCRIPTOR_TIME_SHIFTED_EVENT           = 0x4F,
  TABLE_DESCRIPTOR_COMPONENT                    = 0x50,
  TABLE_DESCRIPTOR_MOSAIC                       = 0x51,
  TABLE_DESCRIPTOR_STREAM_IDENTIFIER            = 0x52,
  TABLE_DESCRIPTOR_CA_IDENTIFIER                = 0x53,
  TABLE_DESCRIPTOR_CONTENT                      = 0x54,
  TABLE_DESCRIPTOR_PARENTAL_RATING              = 0x55,
  TABLE_DESCRIPTOR_TELETEXT                     = 0x56,
  TABLE_DESCRIPTOR_TELEPHONE                    = 0x57,
  TABLE_DESCRIPTOR_LOCAL_TIME_OFFSET            = 0x58,
  TABLE_DESCRIPTOR_SUBTITLING                   = 0x59,
  TABLE_DESCRIPTOR_TERRESTRIAL_DELIVERY_SYSTEM  = 0x5A,
  TABLE_DESCRIPTOR_MULTILINGUAL_NETWORK_NAME    = 0x5B,
  TABLE_DESCRIPTOR_MULTILINGUAL_BOUQUET_NAME    = 0x5C,
  TABLE_DESCRIPTOR_MULTILINGUAL_SERVICE_NAME    = 0x5D,
  TABLE_DESCRIPTOR_COMPONENT_SERVICE_NAME       = 0x5E,
  TABLE_DESCRIPTOR_PRIVATE_DATA_SPECIFIER       = 0x5F,
  TABLE_DESCRIPTOR_SERVICE_MOVE                 = 0x60,
  TABLE_DESCRIPTOR_SHORT_SMOOTHING_BUFFER       = 0x61,
  TABLE_DESCRIPTOR_FREQUENCY_LIST               = 0x62,
  TABLE_DESCRIPTOR_PARTIAL_TRANSPORT_STREAM     = 0x63,
  TABLE_DESCRIPTOR_DATA_BROADCAST               = 0x64,
  TABLE_DESCRIPTOR_CA_SYSTEM                    = 0x65,
  TABLE_DESCRIPTOR_DATA_BROADCAST_ID            = 0x66,
  TABLE_DESCRIPTOR_TRANSPORT_STREAM             = 0x67,
  TABLE_DESCRIPTOR_DSNG                         = 0x68,
  TABLE_DESCRIPTOR_PDC                          = 0x69,
  TABLE_DESCRIPTOR_AC3                          = 0x6A,
  TABLE_DESCRIPTOR_ANCILLARY_DATA               = 0x6B,
  TABLE_DESCRIPTOR_CELL_LIST                    = 0x6C,
  TABLE_DESCRIPTOR_CELL_FREQUENCY_LINK          = 0x6D,
  TABLE_DESCRIPTOR_ANNOUNCEMENT_SUPPORT         = 0x6E,
  TABLE_DESCRIPTOR_APPLICATION_SIGNALLING       = 0x6F,
  TABLE_DESCRIPTOR_ADAPTATION_FIELD_DATA        = 0x70,
  TABLE_DESCRIPTOR_SERVICE_IDENTIFIER           = 0x71,
  TABLE_DESCRIPTOR_SERVICE_AVAILABILITY         = 0x72,
  TABLE_DESCRIPTOR_TVA_DEFAULT_AUTHORITY        = 0x73,
  TABLE_DESCRIPTOR_TVA_RELATED_CONTENT          = 0x74,
  TABLE_DESCRIPTOR_TVA_ID                       = 0x75,
  TABLE_DESCRIPTOR_TVA_CONTENT_IDENTIFIER       = 0x76,
  TABLE_DESCRIPTOR_TIMESLICING_FEC_IDENTIFIER   = 0x77,
  TABLE_DESCRIPTOR_ECM_REPETITION_RATE          = 0x78,
  TABLE_DESCRIPTOR_S2_SATELLITE_DELIVERY_SYSTEM = 0x79,
  TABLE_DESCRIPTOR_ENHANCED_AC3                 = 0x7A,
  TABLE_DESCRIPTOR_DTS                          = 0x7B,
  TABLE_DESCRIPTOR_AAC                          = 0x7C,
  TABLE_DESCRIPTOR_XAIT_LOCATION                = 0x7D,
  TABLE_DESCRIPTOR_FTA_CONTANT_MANAGEMENT       = 0x7E,
  TABLE_DESCRIPTOR_EXTENSION                    = 0x7F,
  TABLE_DESCRIPTOR_LOGICAL_CHANNEL_NUMBER       = 0x83
} TABLE_DescriptorType_t;

/*! Definition of SDT status */
/* ------------------------- */
typedef enum
{
  TABLE_SDT_UNDEFINED            = 0,
  TABLE_SDT_NOT_RUNNING          = 1,
  TABLE_SDT_START_IN_FEW_SECONDS = 2,
  TABLE_SDT_PAUSING              = 3,
  TABLE_SDT_RUNNING              = 4,
  TABLE_SDT_RESERVED_5           = 5,
  TABLE_SDT_RESERVED_6           = 6,
  TABLE_SDT_RESERVED_7           = 7
} TABLE_SDTStatus_t;

/*! Definition of descriptor element */
/* --------------------------------- */
typedef struct
{
  TABLE_DescriptorType_t Tag;  /*!< [in] Tag descriptor type      */
  U8                     Size; /*!< [in] Size of the descriptor   */
  U8                    *Data; /*!< [in] Data array of descriptor */
} TABLE_Descriptor_t;

/*! Definition of PMT Element */
/* -------------------------- */
typedef struct
{
  U16                 Pid;           /*!< [in] Pid pmt value                      */
  TABLE_StreamType_t  Type;          /*!< [in] Stream type of this pmt element    */
  U32                 NbDescriptors; /*!< [in] Nb descriptors in this pmt element */
  TABLE_Descriptor_t *Descriptor;    /*!< [in] Array of descriptors               */
} TABLE_PMTElement_t;

/*! Definition of PAT Element */
/* -------------------------- */
typedef struct
{
  U16 ProgramNumber; /*!< [in] Program number of this pat element */
  U16 Pid;           /*!< [in] Pid value this pat element         */
} TABLE_PATElement_t;

/*! Definition of SDT Element */
/* -------------------------- */
typedef struct
{
  U16                 ServiceId;                 /*!< [in] Service identifier of this sdt element */
  BOOL                EIT_Schedule_Flag;         /*!< [in] Eit schedule flag                      */
  BOOL                EIT_PresentFollowing_Flag; /*!< [in] Eit current/next flag                  */
  TABLE_SDTStatus_t   Status;                    /*!< [in] Status of the eit                      */
  BOOL                Scrambled;                 /*!< [in] Is TRUE if program is scrambled        */
  U32                 NbDescriptors;             /*!< [in] Nb descriptors in this sdt element     */
  TABLE_Descriptor_t *Descriptor;                /*!< [in] Array of descriptors                   */
} TABLE_SDTElement_t;

/*! Definition of NIT Element */
/* -------------------------- */
typedef struct
{
  U16                 TransportStreamId; /*!< [in] Transport identifier of this nit element        */
  U16                 OriginalNetWorkId; /*!< [in] Original network identifier of this nit element */
  U32                 NbDescriptors;     /*!< [in] Nb descriptors in this nit element              */
  TABLE_Descriptor_t *Descriptor;        /*!< [in] Array of descriptors                            */
} TABLE_NITElement_t;

/*! Definition of EIT Element */
/* -------------------------- */
typedef struct
{
  U16                 EventId;                                                                  /*!< [in] Event identifier of this eit element */
  struct              {U8 Day;  U8 Month;  U16 Year; U8 Hour; U8 Minute; U8 Second;} StartTime; /*!< [in] Define start time of this event      */
  struct              {U8 Hour; U8 Minute; U8 Second;} Duration;                                /*!< [in] Define duration of this event        */
  TABLE_SDTStatus_t   Status;                                                                   /*!< [in] Status of this event                 */
  BOOL                Scrambled;                                                                /*!< [in] Is TRUE if program is scrambled      */
  U32                 NbDescriptors;                                                            /*!< [in] Nb descriptors in this eit element   */
  TABLE_Descriptor_t *Descriptor;                                                               /*!< [in] Array of descriptors                 */
} TABLE_EITElement_t;

/*! Definition of PAT section datas */
/* -------------------------------- */
typedef struct
{
  U16                 TransportStreamId; /*!< [in] Transport identifier of this pat data */
  U32                 NbElements;        /*!< [in] Nb elements in this pat data          */
  TABLE_PATElement_t *Element;           /*!< [in] Array of elements                     */
} TABLE_PATData_t;

/*! Definition of PMT section datas */
/* -------------------------------- */
typedef struct
{
  U16                 PcrPid;        /*!< [in] Pcr pid of this pmt data        */
  U16                 ProgramNumber; /*!< [in] Program number of this pmt data */
  U32                 NbDescriptors; /*!< [in] Nb descriptors in this pmt data */
  TABLE_Descriptor_t *Descriptor;    /*!< [in] Array of descriptors            */
  U32                 NbElements;    /*!< [in] Nb elements in this pmt data    */
  TABLE_PMTElement_t *Element;       /*!< [in] Array of elements               */
  BOOL                Scrambled;     /*!< [in] Is TRUE if program is scrambled */
} TABLE_PMTData_t;

/*! Definition of NIT section datas */
/* -------------------------------- */
typedef struct
{
  U16                 NetWorkId;     /*!< [in] Network identifier of this nit data */
  U32                 NbDescriptors; /*!< [in] Nb descriptors in this nit data     */
  TABLE_Descriptor_t *Descriptor;    /*!< [in] Array of descriptors                */
  U32                 NbElements;    /*!< [in] Nb elements in this nit data        */
  TABLE_NITElement_t *Element;       /*!< [in] Array of elements                   */
} TABLE_NITData_t;

/*! Definition of SDT section datas */
/* -------------------------------- */
typedef struct
{
  U16                 TransportStreamId;  /*!< [in] Network identifier of this sdt data          */
  U16                 OriginalNetWorkdId; /*!< [in] Original network identifier of this sdt data */
  U32                 NbElements;         /*!< [in] Nb elements in this sdt data                 */
  TABLE_SDTElement_t *Element;            /*!< [in] Array of elements                            */
} TABLE_SDTData_t;

/*! Definition of EIT section datas */
/* -------------------------------- */
typedef struct
{
  U16                 ServiceId;          /*!< [in] Service identifier of this eit data          */
  U16                 TransportStreamId;  /*!< [in] Network identifier of this eit data          */
  U16                 OriginalNetWorkdId; /*!< [in] Original network identifier of this eit data */
  U32                 NbElements;         /*!< [in] Nb elements in this eit data                 */
  TABLE_EITElement_t *Element;            /*!< [in] Array of elements                            */
} TABLE_EITData_t;

/*! Defintion of TDT section datas*/
/* ------------------------------- */
typedef struct
{
  U8  Day;    /*!< [in] Day value   [1-31] */
  U8  Month;  /*!< [in] Month value [1-12] */
  U16 Year;   /*!< [in] Year (ie : 2008)   */
  U8  Hour;   /*!< [in] Hour [0-23]        */
  U8  Minute; /*!< [in] Minute [0-59]      */
  U8  Second; /*!< [in] Minute [0-59]      */
} TABLE_TDTData_t;

/* Prototypes for table extraction */
/* ------------------------------- */
ST_ErrorCode_t TABLE_PAT_Acquire(char *Filename,U16 Pid,TABLE_PATData_t *TableData);
ST_ErrorCode_t TABLE_PMT_Acquire(char *Filename,U16 Pid,TABLE_PMTData_t *TableData);
ST_ErrorCode_t TABLE_SDT_Acquire(char *Filename,U16 Pid,TABLE_SDTData_t *TableData);
ST_ErrorCode_t TABLE_NIT_Acquire(char *Filename,U16 Pid,TABLE_NITData_t *TableData);
ST_ErrorCode_t TABLE_EIT_Acquire(char *Filename,U16 Pid,TABLE_EITData_t *TableData);
ST_ErrorCode_t TABLE_TDT_Acquire(char *Filename,U16 Pid,TABLE_TDTData_t *TableData);
ST_ErrorCode_t TABLE_PAT_Delete(TABLE_PATData_t *TableData);
ST_ErrorCode_t TABLE_PMT_Delete(TABLE_PMTData_t *TableData);
ST_ErrorCode_t TABLE_SDT_Delete(TABLE_SDTData_t *TableData);
ST_ErrorCode_t TABLE_NIT_Delete(TABLE_NITData_t *TableData);
ST_ErrorCode_t TABLE_EIT_Delete(TABLE_EITData_t *TableData);
ST_ErrorCode_t TABLE_TDT_Delete(TABLE_TDTData_t *TableData);

/* Generic function prototype to filter section */
/* -------------------------------------------- */
ST_ErrorCode_t TABLE_ReadSection(U8 *Filename,U16 Pid,U32 Table_Id,U32 Program_Number,U32 Section_Number,U32 Current_Not_Next,U8 *Buffer,U32 Buffer_Size,U32 *Read_Size);

/* Toolkit to manage descriptors and other stuffs */
/* ---------------------------------------------- */
ST_ErrorCode_t TABLE_ContentTypeToString           (TABLE_PMTElement_t *Element   ,U8  *ContentTypeString,U8 *Details);
ST_ErrorCode_t TABLE_DescriptorService             (TABLE_Descriptor_t *Descriptor,U8  *ProviderName,U8 *ServiceName,U8 *ServiceType);
ST_ErrorCode_t TABLE_DescriptorTeletext            (TABLE_Descriptor_t *Descriptor,U8  *Language,U8 *Type,U8 *Magazine,U8 *Page);
ST_ErrorCode_t TABLE_DescriptorSubtitle            (TABLE_Descriptor_t *Descriptor,U8  *Language,U8 *Type,U16 *Composition_Id,U16 *Ancillary_Id);
ST_ErrorCode_t TABLE_DescriptorLanguageName        (TABLE_Descriptor_t *Descriptor,U8  *Language);
ST_ErrorCode_t TABLE_DescriptorRegistration        (TABLE_Descriptor_t *Descriptor,U8  *FormatIdentifier);
ST_ErrorCode_t TABLE_DescriptorNetworkName         (TABLE_Descriptor_t *Descriptor,U8  *NetworkName);
ST_ErrorCode_t TABLE_DescriptorSatellite           (TABLE_Descriptor_t *Descriptor,U32 *Frequency,U8 *Modulation,U32 *SymbolRate,U8 *Polarization,U8 *Fec,U16 *OrbitalPosition,BOOL *WestNoEast);
ST_ErrorCode_t TABLE_DescriptorCable               (TABLE_Descriptor_t *Descriptor,U32 *Frequency,U8 *Modulation,U32 *SymbolRate,U8 *Fec_In,U8 *Fec_Out);
ST_ErrorCode_t TABLE_DescriptorTerrestrial         (TABLE_Descriptor_t *Descriptor,U32 *Frequency,U8 *Bandwith,U8 *Guard_Interval,U8 *Fec_HP,U8 *Fec_LP,U8 *Transmission_Mode);
ST_ErrorCode_t TABLE_DescriptorShortEvent          (TABLE_Descriptor_t *Descriptor,U8  *Language,U16 *EventNameCoding,U8 *EventName,U16 *EventDescriptionCoding,U8 *EventDescription);
ST_ErrorCode_t TABLE_DescriptorAC3                 (TABLE_Descriptor_t *Descriptor,U8  *ComponentType,U8 *MainId,U8 *Asvc,U8 *Bsid);
ST_ErrorCode_t TABLE_DescriptorEnhancedAC3         (TABLE_Descriptor_t *Descriptor,U8  *ComponentType,U8 *MainId,U8 *Asvc,U8 *Bsid,U8 *SubStream1,U8 *SubStream2,U8 *SubStream3);
ST_ErrorCode_t TABLE_DescriptorLogicalChannelNumber(TABLE_Descriptor_t *Descriptor,U32  Index,U32 *ServiceId,U32 *LogicalChannelNumber);
ST_ErrorCode_t TABLE_CopyPrintable                 (U8 *Destination,U8 *Source,U32 Max);

/* C++ support */
/* ----------- */
#ifdef __cplusplus
}
#endif
#endif
