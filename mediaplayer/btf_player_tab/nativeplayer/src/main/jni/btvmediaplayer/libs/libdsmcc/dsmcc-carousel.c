// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#include <stdlib.h>

#include "dsmcc-receiver.h"

void dsmcc_gateway_free(struct dsmcc_dsi *gateway) {
	/* Free gateway info */
	if(gateway != NULL) {
		if(gateway->user_data_len > 0){
			free(gateway->user_data);
			gateway->user_data = NULL; gateway->user_data_len = 0;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> gateway->user_data");
#endif
		}
		if(gateway->profile.type_id_len > 0){
			free(gateway->profile.type_id);
			gateway->profile.type_id = NULL; gateway->profile.type_id_len = 0;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> gateway->profile.type_id");
#endif
		}
		if(gateway->profile.body.full.obj_loc.objkey_len>0){
			free(gateway->profile.body.full.obj_loc.objkey);
			gateway->profile.body.full.obj_loc.objkey = NULL; gateway->profile.body.full.obj_loc.objkey_len = 0;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
			dsmcc_log("[MEMINFO] free   -> gateway->profile.body.full.obj_loc.objkey");
#endif
		}
		if(gateway->profile.body.full.dsm_conn.taps_count>0) {
			if(gateway->profile.body.full.dsm_conn.tap.selector_data){
				free(gateway->profile.body.full.dsm_conn.tap.selector_data);
				gateway->profile.body.full.dsm_conn.tap.selector_data = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
				dsmcc_log("[MEMINFO] free   -> gateway->profile.body.full.dsm_conn.tap.selector_data");
#endif
			}
		}
	}
}

void dsmcc_objcar_free(struct obj_carousel *obj) {
	struct cache_module_data *cachep, *cachepnext;

	/* Free gateway info */
	if(obj->gate != NULL) {
		dsmcc_gateway_free(obj->gate);
		free(obj->gate);
		obj->gate = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free   -> obj_carousel->gate");
#endif
	}

	/* Free cache_module_data info */
	cachep = obj->cache;
	while(cachep!=NULL) {
		cachepnext = cachep->next;
		dsmcc_cache_module_data_free(cachep);
		cachep = cachepnext;
	}
	
	/* Free cache info */
	if(obj->filecache != NULL) {
		dsmcc_cache_free(obj->filecache);
		free(obj->filecache);
		obj->filecache = NULL;
#ifdef DSMCC_MALLOC_PRINT_DEBUG
		dsmcc_log("[MEMINFO] free	-> obj_carousel->filecache");
#endif
	}
	
	free(obj);
#ifdef DSMCC_MALLOC_PRINT_DEBUG
	dsmcc_log("[MEMINFO] free   -> obj_carousel");
#endif

}
