// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef WORK_FFMPEG_ERROR_H
#define WORK_FFMPEG_ERROR_H

#include <android/log.h>

#define LOG_TAG     "MULTIVIEW"

#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define  LOGF(...)  __android_log_print(ANDROID_FATAL_ERROR,LOG_TAG,__VA_ARGS__)
#define  LOGS(...)  __android_log_print(ANDROID_SILENT_ERROR,LOG_TAG,__VA_ARGS__)



typedef enum _MULTIVIEW_ERROR{
	FFMPEG_SUCCESS = 0,
	FFMPEG_ERR_INVAIDDATA = -9999,
	FFMPEG_ERR_NOT_ENOUGHT_MEMORY,
	FFMPEG_ERR_INTERNAL
}MULTIVIEW_ERROR;


#endif //WORK_FFMPEG_ERROR_H
