// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifdef __cplusplus
extern "C" {
#endif

#include "iconv.h"
#include <systemproperty.h>

#include <hal/define_systempropertys.h>

#include <dbg.h>
#include "dvb_table.h"
#include <dvb_primitive.h>
#include <btv_hal.h>

#include "hal/psi.h"
#include "dtvcc.h"

//#define PROPERTY_NAME_HEAR		"vendor.sptek.hear" 	// Caption On/Off
//#define PROPERTY_NAME_CAPTION	"vendor.sptek.caption" 	// Caption Mode
//#define PROPERTY_NAME_EYE		"vendor.sptek.eye" 		// VI On/Off
//#define PROPERTY_NAME_LANGUAGE	"vendor.sptek.language" // language


static pthread_cond_t cc_cond = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t cc_lock = PTHREAD_MUTEX_INITIALIZER;

void ccx_dtvcc_request_dsmcc_data(ccx_dtvcc_ctx *ctx);
void ccx_dtvcc_stop_dsmcc_section_filter(ccx_dtvcc_ctx *ctx);
void ccx_dtvcc_tta_check_audio_setting_active(ccx_dtvcc_ctx *ctx);
void ccx_dtvcc_set_timer(ccx_dtvcc_ctx *ctx, int action);

#define CCX_SWAP_16(s) (((((s) & 0xff) << 8) | (((s) >> 8) & 0xff)))
#define CCX_SWAP_32(l) (((((l) & 0xff000000) >> 24) | (((l) & 0x00ff0000) >> 8)  | (((l) & 0x0000ff00) << 8)  | (((l) & 0x000000ff) << 24)))

uint16_t _dtvcc_pmt_proc_stream_descriptors_get_ca_pid( uint8_t *p_bfr, unsigned int size, int stream_num );

#define ADD_VIDEO_PID(P_INFO, PID, TYPE, PMT, PMTSIZE, INDEX) \
    do { \
    if( (P_INFO)->num_video_pids < TSPSIMGR_MAX_PIDS ) \
    { \
    ccx_dtvcc_log("  vpid[%d] 0x%x, type 0x%x", (P_INFO)->num_video_pids, (PID), (TYPE)); \
    (P_INFO)->video_pids[(P_INFO)->num_video_pids].pid = (PID); \
    (P_INFO)->video_pids[(P_INFO)->num_video_pids].codec = (TYPE); \
    (P_INFO)->video_pids[(P_INFO)->num_video_pids].ca_pid = _dtvcc_pmt_proc_stream_descriptors_get_ca_pid((PMT), (PMTSIZE), (INDEX)); \
    (P_INFO)->num_video_pids++; \
    } \
    } while (0)
#define ADD_AUDIO_PID(P_INFO, PID, TYPE, PMT, PMTSIZE, INDEX) \
    do { \
    if( (P_INFO)->num_audio_pids < TSPSIMGR_MAX_PIDS ) \
    { \
    ccx_dtvcc_log("  apid[%d] 0x%x, type 0x%x", (P_INFO)->num_audio_pids, (PID), (TYPE)); \
    (P_INFO)->audio_pids[(P_INFO)->num_audio_pids].pid = (PID); \
    (P_INFO)->audio_pids[(P_INFO)->num_audio_pids].codec = (TYPE); \
    (P_INFO)->audio_pids[(P_INFO)->num_audio_pids].ca_pid = _dtvcc_pmt_proc_stream_descriptors_get_ca_pid((PMT), (PMTSIZE), (INDEX)); \
    (P_INFO)->num_audio_pids++; \
    } \
    } while (0)
#define ADD_OTHER_PID(P_INFO, PID, TYPE, PMT, PMTSIZE, INDEX) \
    do { \
    if( (P_INFO)->num_other_pids < TSPSIMGR_MAX_PIDS ) \
    { \
    ccx_dtvcc_log("  opid[%d] 0x%x, type 0x%x", (P_INFO)->num_audio_pids, (PID), (TYPE)); \
    (P_INFO)->other_pids[(P_INFO)->num_other_pids].pid = (PID); \
    (P_INFO)->other_pids[(P_INFO)->num_other_pids].stream_type = (TYPE); \
    (P_INFO)->other_pids[(P_INFO)->num_other_pids].ca_pid = _dtvcc_pmt_proc_stream_descriptors_get_ca_pid((PMT), (PMTSIZE), (INDEX)); \
    (P_INFO)->num_other_pids++; \
    } \
    } while (0)


const char *DTVCC_COMMANDS_C0[32] =
{
	"NUL", // 0 = NUL
	NULL,  // 1 = Reserved
	NULL,  // 2 = Reserved
	"ETX", // 3 = ETX
	NULL,  // 4 = Reserved
	NULL,  // 5 = Reserved
	NULL,  // 6 = Reserved
	NULL,  // 7 = Reserved
	"BS",  // 8 = Backspace
	NULL,  // 9 = Reserved
	NULL,  // A = Reserved
	NULL,  // B = Reserved
	"FF",  // C = FF
	"CR",  // D = CR
	"HCR", // E = HCR
	NULL,  // F = Reserved
	"EXT1",// 0x10 = EXT1,
	NULL,  // 0x11 = Reserved
	NULL,  // 0x12 = Reserved
	NULL,  // 0x13 = Reserved
	NULL,  // 0x14 = Reserved
	NULL,  // 0x15 = Reserved
	NULL,  // 0x16 = Reserved
	NULL,  // 0x17 = Reserved
	"P16", // 0x18 = P16
	NULL,  // 0x19 = Reserved
	NULL,  // 0x1A = Reserved
	NULL,  // 0x1B = Reserved
	NULL,  // 0x1C = Reserved
	NULL,  // 0x1D = Reserved
	NULL,  // 0x1E = Reserved
	NULL,  // 0x1F = Reserved
};

struct CCX_DTVCC_S_COMMANDS_C1 DTVCC_COMMANDS_C1[32] =
{
	{CCX_DTVCC_C1_CW0, "CW0", "SetCurrentWindow0",     1},
	{CCX_DTVCC_C1_CW1, "CW1", "SetCurrentWindow1",     1},
	{CCX_DTVCC_C1_CW2, "CW2", "SetCurrentWindow2",     1},
	{CCX_DTVCC_C1_CW3, "CW3", "SetCurrentWindow3",     1},
	{CCX_DTVCC_C1_CW4, "CW4", "SetCurrentWindow4",     1},
	{CCX_DTVCC_C1_CW5, "CW5", "SetCurrentWindow5",     1},
	{CCX_DTVCC_C1_CW6, "CW6", "SetCurrentWindow6",     1},
	{CCX_DTVCC_C1_CW7, "CW7", "SetCurrentWindow7",     1},
	{CCX_DTVCC_C1_CLW, "CLW", "ClearWindows",          2},
	{CCX_DTVCC_C1_DSW, "DSW", "DisplayWindows",        2},
	{CCX_DTVCC_C1_HDW, "HDW", "HideWindows",           2},
	{CCX_DTVCC_C1_TGW, "TGW", "ToggleWindows",         2},
	{CCX_DTVCC_C1_DLW, "DLW", "DeleteWindows",         2},
	{CCX_DTVCC_C1_DLY, "DLY", "Delay",                 2},
	{CCX_DTVCC_C1_DLC, "DLC", "DelayCancel",           1},
	{CCX_DTVCC_C1_RST, "RST", "Reset",                 1},
	{CCX_DTVCC_C1_SPA, "SPA", "SetPenAttributes",      3},
	{CCX_DTVCC_C1_SPC, "SPC", "SetPenColor",           4},
	{CCX_DTVCC_C1_SPL, "SPL", "SetPenLocation",        3},
	{CCX_DTVCC_C1_RSV93, "RSV93", "Reserved",          1},
	{CCX_DTVCC_C1_RSV94, "RSV94", "Reserved",          1},
	{CCX_DTVCC_C1_RSV95, "RSV95", "Reserved",          1},
	{CCX_DTVCC_C1_RSV96, "RSV96", "Reserved",          1},
	{CCX_DTVCC_C1_SWA, "SWA", "SetWindowAttributes",   5},
	{CCX_DTVCC_C1_DF0, "DF0", "DefineWindow0",         7},
	{CCX_DTVCC_C1_DF1, "DF1", "DefineWindow1",         7},
	{CCX_DTVCC_C1_DF2, "DF2", "DefineWindow2",         7},
	{CCX_DTVCC_C1_DF3, "DF3", "DefineWindow3",         7},
	{CCX_DTVCC_C1_DF4, "DF4", "DefineWindow4",         7},
	{CCX_DTVCC_C1_DF5, "DF5", "DefineWindow5",         7},
	{CCX_DTVCC_C1_DF6, "DF6", "DefineWindow6",         7},
	{CCX_DTVCC_C1_DF7, "DF7", "DefineWindow7",         7}
};

//------------------------- DEFAULT AND PREDEFINED -----------------------------

ccx_dtvcc_service_descriptor ccx_dtvcc_default_caption_service_descrpitor =
{
	"kor",
	1,
	1,
	0,	
	0,
	0
};

ccx_dtvcc_pen_color ccx_dtvcc_default_pen_color =
{
	0x3f,
	0,
	0,
	0,
	0
};

ccx_dtvcc_pen_attribs ccx_dtvcc_default_pen_attribs =
{
	CCX_DTVCC_PEN_SIZE_STANDART,
	0,
	CCX_DTVCC_PEN_TEXT_TAG_UNDEFINED_12,
	0,
	CCX_DTVCC_PEN_EDGE_NONE,
	0,
	0
};

ccx_dtvcc_window_attribs ccx_dtvcc_predefined_window_styles[] =
{
	{0,0,0,0,0,0,0,0,0,0, 0}, // Dummy, unused (position 0 doesn't use the table)
	{ /* 1 - NTSC Style PopUp Captions */
		CCX_DTVCC_WINDOW_JUSTIFY_LEFT,
		CCX_DTVCC_WINDOW_PD_LEFT_RIGHT,
		CCX_DTVCC_WINDOW_SD_BOTTOM_TOP,
		0,
		CCX_DTVCC_WINDOW_SDE_SNAP,
		0,
		0,
		0,
		CCX_DTVCC_WINDOW_FO_SOLID,
		CCX_DTVCC_WINDOW_BORDER_NONE,
		0
	},
	{ /* 2 - PopUp Captions w/o Black Background */
		CCX_DTVCC_WINDOW_JUSTIFY_LEFT,
		CCX_DTVCC_WINDOW_PD_LEFT_RIGHT,
		CCX_DTVCC_WINDOW_SD_BOTTOM_TOP,
		0,
		CCX_DTVCC_WINDOW_SDE_SNAP,
		0,
		0,
		0,
		CCX_DTVCC_WINDOW_FO_TRANSPARENT,
		CCX_DTVCC_WINDOW_BORDER_NONE,
		0
	},
	{ /* 3 -> NTSC Style Centered PopUp Captions */
		CCX_DTVCC_WINDOW_JUSTIFY_CENTER,
		CCX_DTVCC_WINDOW_PD_LEFT_RIGHT,
		CCX_DTVCC_WINDOW_SD_BOTTOM_TOP,
		0,
		CCX_DTVCC_WINDOW_SDE_SNAP,
		0,
		0,
		0,
		CCX_DTVCC_WINDOW_FO_SOLID,
		CCX_DTVCC_WINDOW_BORDER_NONE,
		0
	},
	{ /* 4 -> NTSC Style RollUp Captions */
		CCX_DTVCC_WINDOW_JUSTIFY_LEFT,
		CCX_DTVCC_WINDOW_PD_LEFT_RIGHT,
		CCX_DTVCC_WINDOW_SD_BOTTOM_TOP,
		1,
		CCX_DTVCC_WINDOW_SDE_SNAP,
		0,
		0,
		0,
		CCX_DTVCC_WINDOW_FO_SOLID,
		CCX_DTVCC_WINDOW_BORDER_NONE,
		0
	},
	{ /* 5 -> RollUp Captions w/o Black Background */
		CCX_DTVCC_WINDOW_JUSTIFY_LEFT,
		CCX_DTVCC_WINDOW_PD_LEFT_RIGHT,
		CCX_DTVCC_WINDOW_SD_BOTTOM_TOP,
		1,
		CCX_DTVCC_WINDOW_SDE_SNAP,
		0,
		0,
		0,
		CCX_DTVCC_WINDOW_FO_TRANSPARENT,
		CCX_DTVCC_WINDOW_BORDER_NONE,
		0
	},
	{ /* 6 -> NTSC Style Centered RollUp Captions */
		CCX_DTVCC_WINDOW_JUSTIFY_CENTER,
		CCX_DTVCC_WINDOW_PD_LEFT_RIGHT,
		CCX_DTVCC_WINDOW_SD_BOTTOM_TOP,
		1,
		CCX_DTVCC_WINDOW_SDE_SNAP,
		0,
		0,
		0,
		CCX_DTVCC_WINDOW_FO_SOLID,
		CCX_DTVCC_WINDOW_BORDER_NONE,
		0
	},
	{ /* 7 -> Ticker tape */
		CCX_DTVCC_WINDOW_JUSTIFY_LEFT,
		CCX_DTVCC_WINDOW_PD_TOP_BOTTOM,
		CCX_DTVCC_WINDOW_SD_RIGHT_LEFT,
		0,
		CCX_DTVCC_WINDOW_SDE_SNAP,
		0,
		0,
		0,
		CCX_DTVCC_WINDOW_FO_SOLID,
		CCX_DTVCC_WINDOW_BORDER_NONE,
		0
	}
};

//---------------------------------- LOGS ------------------------------------

void ccx_dtvcc_log_dump(const char* data, int lengths)
{
#ifdef DEBUG_LOG_ENABLE
	char szDebugTemp[4096] = {0,};

	if(data == NULL) {
		sprintf(szDebugTemp, "\n\n==========================================================\n");
		sprintf(szDebugTemp+strlen(szDebugTemp), ("----------------------------------------------------------\n"));
		sprintf(szDebugTemp+strlen(szDebugTemp), (">>>>>>>>>>>>>>>>>>>>DATA is Empty!!<<<<<<<<<<<<<<<<<<<<<<<\n"));	
		__android_log_print(ANDROID_LOG_DEBUG  , "[CC_DTV]", "%s", szDebugTemp);		
		return;
	}

	sprintf(szDebugTemp, "\n\n==========================================================\n");
	sprintf(szDebugTemp+strlen(szDebugTemp), "[size]:%d\n", lengths);
	sprintf(szDebugTemp+strlen(szDebugTemp), "----------------------------------------------------------\n");	

	int i     = 0;
	int j     = 0;
	int width = 16;

	for(i=0; i<lengths; ) {
		sprintf(szDebugTemp+strlen(szDebugTemp), "%04d", i);			

		// Print Hexa Code
		for(j=i; j<(i+width); j++) {
			if(!(j%(width/2))) {
				sprintf(szDebugTemp+strlen(szDebugTemp), " ");							
			}

			if(j >= lengths) {
				sprintf(szDebugTemp+strlen(szDebugTemp), "   ");											
			} else {
				sprintf(szDebugTemp+strlen(szDebugTemp), "%02x", (unsigned char)(data[j]));														
			}
		}
		sprintf(szDebugTemp+strlen(szDebugTemp), ": ");		

		// Print ASCII Code
		for(j=i; j<(i+width) && j<lengths; j++) {
			if(data[j] < 0x20 || data[j] > 0x70) {
				sprintf(szDebugTemp+strlen(szDebugTemp), ".");						
			} else {
				sprintf(szDebugTemp+strlen(szDebugTemp), "%c", data[j]);									
			}
		}

		i += width;
		if(i>lengths) {
			i = (int)lengths;
		}
		sprintf(szDebugTemp+strlen(szDebugTemp), "\n");											
	}
	
	sprintf(szDebugTemp+strlen(szDebugTemp), "===========================================================\n\n");												
	__android_log_print(ANDROID_LOG_DEBUG  , "BOXKEY-DVB_DTV_CC", "%s", szDebugTemp);		
#endif	
}


void ccx_dtvcc_log(const char *format, ...)
{
#ifdef DEBUG_LOG_ENABLE
	struct 		timeval val;
	struct tm 	*ptm = NULL;
	va_list 	va;
	char szDebugTemp[4096] = {0,};
	int nHeaderLength;

	gettimeofday( &val, NULL );
	ptm = localtime( &val.tv_sec );

	sprintf(szDebugTemp, "[%02d/%02d %02d:%02d:%02d:%03d]\t%lld\t", ptm->tm_mon + 1, ptm->tm_mday
			,ptm->tm_hour, ptm->tm_min, ptm->tm_sec, (int)val.tv_usec / 1000, (long long) val.tv_sec*1000 + (long long)val.tv_usec/1000);

	nHeaderLength = strlen(szDebugTemp);

	va_start(va, format);
	vsprintf(szDebugTemp+strlen(szDebugTemp), format, va);
	va_end(va);

	__android_log_print(ANDROID_LOG_DEBUG  , "BOXKEY-DVB_DTV_CC", "%s", szDebugTemp);
#endif	
}  

//---------------------------------- UTILS ------------------------------------

int is_latin_character(unsigned short utf16_char)
{
	if (utf16_char > 0xA0 && utf16_char <= 0xFF) {
		return 1;
	}
	return 0;
}

int write_utf16_char(unsigned short utf16_char, char *out)
{
	int nbytes = 0;
	if ((utf16_char >> 8) != 0) {
		nbytes = 2;
		out[0] = (unsigned char)(utf16_char >> 8);
		out[1] = (unsigned char)(utf16_char & 0xff);
	} else {
		nbytes = 1;
		out[0] = (unsigned char)(utf16_char);
	}
	return nbytes;
}

int ucs2_to_utf8(unsigned short unicode, char *out)
{
	int nbytes = 0;
	if (unicode < 0x80) {
		nbytes = 1;
		out[0] = (unsigned char)((unicode >> 0 & 0x7F) | 0x00);
	} else if (unicode < 0x0800) {
		nbytes = 2;
		out[0] = (unsigned char)((unicode >> 6 & 0x1F) | 0xC0);
		out[1] = (unsigned char)((unicode >> 0 & 0x3F) | 0x80);
	} else {
		nbytes = 3;
		out[0] = (unsigned char)((unicode >> 12 & 0x0F) | 0xE0);
		out[1] = (unsigned char)((unicode >> 6 & 0x3F)  | 0x80);
		out[2] = (unsigned char)((unicode >> 0 & 0x3F)  | 0x80);
	}
	return nbytes;
}

unsigned int _dtvcc_sys_clock()
{
	struct timeval now;
	gettimeofday(&now, NULL);
	return (unsigned int)(((now.tv_sec) * 1000 + (now.tv_usec) / 1000));
}

void _dtvcc_pat_data_dump(TS_PSI_header *header)
{
	ccx_dtvcc_log( "[SECTION]#------------------------------------------#\n");
	ccx_dtvcc_log( "[SECTION]# Program Association Table\n" );
	ccx_dtvcc_log( "[SECTION]#------------------------------------------#\n" );
	ccx_dtvcc_log( "[SECTION] table_id = 0x%02X\n", header->table_id);
	ccx_dtvcc_log( "[SECTION] section_syntax_indicator =0x%02X\n" , header->section_syntax_indicator);
	ccx_dtvcc_log( "[SECTION] section_length= %#d\n", header->section_length);
	ccx_dtvcc_log( "[SECTION] transport_stream_id = 0x%04X\n" , header->table_id_extension);
	ccx_dtvcc_log( "[SECTION] version_number = 0x%02X\n" , header->version_number);
	ccx_dtvcc_log( "[SECTION] current_next_indicator = 0x%02X\n" , header->current_next_indicator);
	ccx_dtvcc_log( "[SECTION] section_number = 0x%02X\n" , header->section_number);
	ccx_dtvcc_log( "[SECTION] last_section_number = 0x%02X\n" , header->last_section_number);
}

void _dtvcc_pmt_data_dump(TS_PSI_Program *program_info)
{
	int i;

	ccx_dtvcc_log( "[SECTION]#------------------------------------------#\n");
	ccx_dtvcc_log( "[SECTION]# Program Map Table\n" );
	ccx_dtvcc_log( "[SECTION]#------------------------------------------#\n" );
	ccx_dtvcc_log( "[SECTION] Program number = 0x%04X\n", program_info->program_number);
	ccx_dtvcc_log( "[SECTION] Program version = 0x%04X\n" , program_info->version);
	ccx_dtvcc_log( "[SECTION] Program PCR_PID = 0x%04X\n", program_info->pcr_pid);
	ccx_dtvcc_log( "[SECTION] Program numOfVideoPID = 0x%04X\n" , program_info->num_video_pids);
	ccx_dtvcc_log( "[SECTION] Program numOfAudioPID = 0x%04X\n" , program_info->num_audio_pids);
	ccx_dtvcc_log( "[SECTION] Program numOfOtherPID = 0x%04X\n" , program_info->num_other_pids);
	
	i = 0;
	ccx_dtvcc_log( "[SECTION] --------- Video --------\n");
	for (i = 0; i < program_info->num_video_pids; i++)
	{
		ccx_dtvcc_log("[SECTION] Program %d'th VideoES_PID	 : 0x%04x\n", i, program_info->video_pids[i].pid);
		ccx_dtvcc_log("[SECTION] Program %d'th Video_Codec	 : 0x%04x\n", i, program_info->video_pids[i].codec);
		ccx_dtvcc_log("[SECTION] Program %d'th VideoCA_PID	 : 0x%04x\n", i, program_info->video_pids[i].ca_pid);		
	}

	i = 0;
	ccx_dtvcc_log( "[SECTION] --------- Audio --------\n");
	for (i = 0; i < program_info->num_audio_pids; i++)
	{
		ccx_dtvcc_log("[SECTION] Program %d'th AudioES_PID	 : 0x%04x\n", i, program_info->audio_pids[i].pid);
		ccx_dtvcc_log("[SECTION] Program %d'th Audio_Codec	 : 0x%04x\n", i, program_info->audio_pids[i].codec);
		ccx_dtvcc_log("[SECTION] Program %d'th AudioCA_PID	 : 0x%04x\n", i, program_info->audio_pids[i].ca_pid);		
	}

	i = 0;
	ccx_dtvcc_log( "[SECTION] --------- Others --------\n");
	for (i = 0; i < program_info->num_other_pids; i++)
	{
		ccx_dtvcc_log("[SECTION] Program %d'th OtherES_PID	 : 0x%04x\n", i, program_info->other_pids[i].pid);
		ccx_dtvcc_log("[SECTION] Program %d'th Stream_Type	 : 0x%04x\n", i, program_info->other_pids[i].stream_type);
		ccx_dtvcc_log("[SECTION] Program %d'th OtherCA_PID	 : 0x%04x\n", i, program_info->other_pids[i].ca_pid);		
	}
	
	ccx_dtvcc_log( "[SECTION]#------------------------------------------#\n");
}

void _dtvcc_ait_data_dump(TS_PSI_header *header)
{
	ccx_dtvcc_log( "[SECTION]#------------------------------------------#\n");
	ccx_dtvcc_log( "[SECTION]# Application Information Table\n" );
	ccx_dtvcc_log( "[SECTION]#------------------------------------------#\n" );
	ccx_dtvcc_log( "[SECTION] table_id = %d (0x%02X)\n", header->table_id, header->table_id);
	ccx_dtvcc_log( "[SECTION] section_syntax_indicator = %d (0x%02X)\n" , header->section_syntax_indicator, header->section_syntax_indicator);
	ccx_dtvcc_log( "[SECTION] section_length= %d (0x%04X)\n", header->section_length, header->section_length);
	ccx_dtvcc_log( "[SECTION] test_application_flag = %d (0x%02X)\n" , (header->table_id_extension>>15)&1, (header->table_id_extension>>15)&1);	
	ccx_dtvcc_log( "[SECTION] application_type = %d (0x%04X)\n" , header->table_id_extension&0x7FFF, header->table_id_extension&0x7FFF);
	ccx_dtvcc_log( "[SECTION] version_number = %d (0x%02X)\n" , header->version_number, header->version_number);
	ccx_dtvcc_log( "[SECTION] current_next_indicator = %d (0x%02X)\n" , header->current_next_indicator, header->current_next_indicator);
	ccx_dtvcc_log( "[SECTION] section_number = %d (0x%02X)\n" , header->section_number, header->section_number);
	ccx_dtvcc_log( "[SECTION] last_section_number = %d (0x%02X)\n" , header->last_section_number, header->last_section_number);
}


void _dtvcc_audio_descriptor_dump(ccx_dtvcc_audio_service_descriptor *descriptor)
{
	ccx_dtvcc_log( "[SECTION]#------------------------------------------#\n");
	ccx_dtvcc_log( "[SECTION]# Audio Descriptor\n" );
	ccx_dtvcc_log( "[SECTION]#------------------------------------------#\n" );
	if(descriptor->ac3_stream_descriptor.stream_type != 0x00){
		ccx_dtvcc_log("ATSC_AC3_Audio : stream_type (0x%02X)", 
				descriptor->ac3_stream_descriptor.stream_type);
		ccx_dtvcc_log("ATSC_AC3_Audio : pid (0x%04X)", 
				descriptor->ac3_stream_descriptor.pid);
			
		ccx_dtvcc_log("ATSC_AC3_Audio : sample_rate_code (0x%02X)", 
				descriptor->ac3_stream_descriptor.sample_rate_code);
		ccx_dtvcc_log("ATSC_AC3_Audio : bsid (0x%02X)", 
				descriptor->ac3_stream_descriptor.bsid);
			
		ccx_dtvcc_log("ATSC_AC3_Audio : bit_rate_code (0x%02X)", 
				descriptor->ac3_stream_descriptor.bit_rate_code);
		ccx_dtvcc_log("ATSC_AC3_Audio : surround_mode (0x%02X)", 
				descriptor->ac3_stream_descriptor.surround_mode);

		ccx_dtvcc_log("ATSC_AC3_Audio : bsmod (0x%02X)", 
				descriptor->ac3_stream_descriptor.bsmod);
		ccx_dtvcc_log("ATSC_AC3_Audio : num_channels (0x%02X)", 
				descriptor->ac3_stream_descriptor.num_channels);
		ccx_dtvcc_log("ATSC_AC3_Audio : full_svc (0x%02X)", 
				descriptor->ac3_stream_descriptor.full_svc);
			
		ccx_dtvcc_log("ATSC_AC3_Audio : langcod (0x%02X)", 
				descriptor->ac3_stream_descriptor.langcod);
			
		ccx_dtvcc_log("ATSC_AC3_Audio : langcod2 (0x%02X)", 
				descriptor->ac3_stream_descriptor.langcod2);
			
		if(descriptor->ac3_stream_descriptor.bsmod < 2){
			ccx_dtvcc_log("ATSC_AC3_Audio : mainid (0x%02X)", 
					descriptor->ac3_stream_descriptor.mainid);
			ccx_dtvcc_log("ATSC_AC3_Audio : priority (0x%02X)", 
							descriptor->ac3_stream_descriptor.priority);
		}else{
			ccx_dtvcc_log("ATSC_AC3_Audio : asvcflags (0x%02X)", 
					descriptor->ac3_stream_descriptor.asvcflags);
		}
		ccx_dtvcc_log("ATSC_AC3_Audio : text_code (0x%02X)", 
				descriptor->ac3_stream_descriptor.text_code);

		ccx_dtvcc_log("ATSC_AC3_Audio : text_desc (%s)", 
				descriptor->ac3_stream_descriptor.text_desc);

		ccx_dtvcc_log("ATSC_AC3_Audio : language (%s)", 
				descriptor->ac3_stream_descriptor.language);

		ccx_dtvcc_log("ATSC_AC3_Audio : language_2 (%s)",
				descriptor->ac3_stream_descriptor.language2);
	}		

	if(descriptor->language_descriptor.stream_type != 0x00){

		ccx_dtvcc_log("ISO 639 Audio : stream_type (0x%02X)", 
				descriptor->language_descriptor.stream_type);
		ccx_dtvcc_log("ISO 639 Audio : pid (0x%04X)", 
				descriptor->language_descriptor.pid);
		ccx_dtvcc_log("ISO 639 Language: language (%s)", 
				descriptor->language_descriptor.language);
		ccx_dtvcc_log("ISO 639 Language: audio_type (0x%02X)",
				descriptor->language_descriptor.audio_type);
	}			
	
	ccx_dtvcc_log( "[SECTION]#------------------------------------------#\n");
}

//TODO : 코덱 변환을 위해서..작업을 새로운 장비일경우 세팅해줘야한다.

int _dtvcc_convert_audio_codec(int streamtype)
{
	int audio_codec = AVP_CODEC_AUDIO_AAC;
	
	switch (streamtype)
	{
		case TS_PSI_ST_11172_3_Audio: /* MPEG-1 */
			audio_codec = AVP_CODEC_AUDIO_MPEG1;
			break;

		case TS_PSI_ST_13818_3_Audio: /* MPEG-2 */
			audio_codec = AVP_CODEC_AUDIO_MPEG2;
			break;

		case TS_PSI_ST_13818_7_AAC:   /* MPEG-2 AAC */
		case TS_PSI_ST_14496_3_Audio: /* MPEG-4 AAC */
			audio_codec = AVP_CODEC_AUDIO_AAC;//AMP_HE_AAC;
			break;

		case TS_PSI_ST_ATSC_AC3:	  /* ATSC AC-3 */
			audio_codec = AVP_CODEC_AUDIO_AC3;
			break;

		case TS_PSI_ST_ATSC_EAC3:	  /* ATSC Enhanced AC-3 */
		case TS_PSI_ST_13818_1_PrivateSection:
			audio_codec = AVP_CODEC_AUDIO_EAC3;
			break;

		case TS_PSI_ST_ATSC_DTS:	  /* ATSC DTS audio */
		case TS_PSI_ST_BD_DTS:			
			audio_codec = AVP_CODEC_AUDIO_DTS;
			break;

		case TS_PSI_ST_ATSC_DTS_HD:   /* ASTC DTS-UHD audio */
			audio_codec = AVP_CODEC_AUDIO_DTS;
			break;

		default:
			audio_codec = AVP_CODEC_AUDIO_AAC;//AMP_HE_AAC;
			break;
	}

	return audio_codec;
}

void _dtvcc_request_vi_audio_play(ccx_dtvcc_ctx *ctx, uint16_t audio_pid, uint16_t audio_codec)
{
	if(ctx->is_audio_play)	return;			

	dvb_player_t* player = (dvb_player_t*)ctx->handle;

	// backup main audio setting
	ctx->main_audio_pid = player->avpInfo.playerConfig.audio_pid;
	ctx->main_audio_codec = player->avpInfo.playerConfig.audio_codec;

	// PLAY Visual Impaired Audio Request
	ccx_dtvcc_log( "#_dtvcc_vi_audio_play: audio_codec[%d], audio_pid[0x%02X]\n", audio_codec, audio_pid);

	AVP_AudioConfig audioConfig;
	player->avpInfo.playerConfig.audio_pid = audioConfig.pid = (uint16_t) audio_pid;
	player->avpInfo.playerConfig.audio_codec = audioConfig.codec = (AVP_Codec) audio_codec;

#ifdef USE_BTV_HAL_MANAGER
	HalBtv_SetAudioStream(player->idx_player, &audioConfig);
#else
	AVP_SetAudioStream(player->avpInfo.playerHandle, &audioConfig);
#endif
	ctx->is_audio_play = 1;
}

void _dtvcc_request_main_audio_play(ccx_dtvcc_ctx *ctx)
{
	if(!ctx->is_audio_play) return;

	dvb_player_t* player = (dvb_player_t*)ctx->handle;

	// PLAY Main Audio Request
	ccx_dtvcc_log( "#_dtvcc_main_audio_play: audio_codec[%d], audio_pid[0x%02X]\n", ctx->main_audio_codec, ctx->main_audio_pid);

	AVP_AudioConfig audioConfig;
	player->avpInfo.playerConfig.audio_pid = audioConfig.pid = (uint16_t) ctx->main_audio_pid;
	player->avpInfo.playerConfig.audio_codec = audioConfig.codec = (AVP_Codec) ctx->main_audio_codec;

#ifdef USE_BTV_HAL_MANAGER
	HalBtv_SetAudioStream(player->idx_player, &audioConfig);
#else
	AVP_SetAudioStream(player->avpInfo.playerHandle, &audioConfig);
#endif
	ctx->is_audio_play = 0;
}

ccx_dtvcc_audio_service_descriptor* _dtvcc_tta_get_audio_service_descriptor(ccx_dtvcc_ctx *ctx, int select_type)
{
	int lang_match;
	int is_aac;
	int audio_type;
	int bsmod;
	int langflag;
	ccx_dtvcc_log("Searching audio_service_descriptor TYPE: %d", select_type);
	ccx_dtvcc_audio_service_descriptor *sptr = ctx->audio_descriptor_head;
	while (sptr) {

		_dtvcc_audio_descriptor_dump(sptr);

		if(sptr->ac3_stream_descriptor.stream_type == 0x00) 
			is_aac = 1;
		else
			is_aac = 0;
		
		if(select_type == CCX_DTVCC_AUDIO_KCM){
			lang_match = 0;
			if(sptr->language_descriptor.stream_type != 0x00){
				audio_type = sptr->language_descriptor.audio_type;
				if(!strcmp(sptr->language_descriptor.language, "kor")){
					lang_match = 1;
				}
				if(is_aac && lang_match && audio_type == 0x00){
					ccx_dtvcc_log("1.audio_service_descriptor CCX_DTVCC_AUDIO_KCM founded");
					return sptr;
				}
			}

			if(sptr->ac3_stream_descriptor.stream_type != 0x00){
				bsmod = sptr->ac3_stream_descriptor.bsmod;
				langflag = sptr->ac3_stream_descriptor.langflag;
				if(langflag){
					if(!strcmp(sptr->ac3_stream_descriptor.language, "kor")){
						lang_match = 1;
					}
				}else{
					if(!strcmp(sptr->ac3_stream_descriptor.text_desc, "ko")){
						lang_match = 1;
					}
				}
				if(bsmod == 0x00 && lang_match){
					ccx_dtvcc_log("2.audio_service_descriptor CCX_DTVCC_AUDIO_KCM founded");
					return sptr;
				}
			}

		}else if(select_type == CCX_DTVCC_AUDIO_KVI){
			lang_match = 0;
			if(sptr->language_descriptor.stream_type != 0x00){
				audio_type = sptr->language_descriptor.audio_type;
				if(!strcmp(sptr->language_descriptor.language, "kor")){
					lang_match = 1;
				}
				if(is_aac && lang_match && audio_type == 0x03){
					ccx_dtvcc_log("1.audio_service_descriptor CCX_DTVCC_AUDIO_KVI founded");
					return sptr;
				}
			}

			if(sptr->ac3_stream_descriptor.stream_type != 0x00){
				bsmod = sptr->ac3_stream_descriptor.bsmod;
				langflag = sptr->ac3_stream_descriptor.langflag;
				if(langflag){
					if(!strcmp(sptr->ac3_stream_descriptor.language, "kor")){
						lang_match = 1;
					}
				}else{
					if(!strcmp(sptr->ac3_stream_descriptor.text_desc, "ko")){
						lang_match = 1;
					}
				}
				if(bsmod == 0x02 && lang_match){
					ccx_dtvcc_log("2.audio_service_descriptor CCX_DTVCC_AUDIO_KVI founded");
					return sptr;
				}
			}

		}else if(select_type == CCX_DTVCC_AUDIO_ECM){
			lang_match = 0;
			if(sptr->language_descriptor.stream_type != 0x00){
				audio_type = sptr->language_descriptor.audio_type;
				if(!strcmp(sptr->language_descriptor.language, "eng")){
					lang_match = 1;
				}
				if(is_aac && lang_match && audio_type == 0x00){
					ccx_dtvcc_log("1.audio_service_descriptor CCX_DTVCC_AUDIO_ECM founded");
					return sptr;
				}
			}
		
			if(sptr->ac3_stream_descriptor.stream_type != 0x00){
				bsmod = sptr->ac3_stream_descriptor.bsmod;
				langflag = sptr->ac3_stream_descriptor.langflag;
				if(langflag){
					if(!strcmp(sptr->ac3_stream_descriptor.language, "eng")){
						lang_match = 1;
					}
				}else{
					if(!strcmp(sptr->ac3_stream_descriptor.text_desc, "en")){
						lang_match = 1;
					}
				}
 				if(bsmod == 0x00 && lang_match){
					ccx_dtvcc_log("2.audio_service_descriptor CCX_DTVCC_AUDIO_ECM founded");
					return sptr;
				}
			}

		}else if(select_type == CCX_DTVCC_AUDIO_EVI){
			lang_match = 0;
			if(sptr->language_descriptor.stream_type != 0x00){
				audio_type = sptr->language_descriptor.audio_type;
				if(!strcmp(sptr->language_descriptor.language, "eng")){
					lang_match = 1;
				}
				if(is_aac && lang_match && audio_type == 0x03){
					ccx_dtvcc_log("1.audio_service_descriptor CCX_DTVCC_AUDIO_EVI founded");
					return sptr;
				}
			}

			if(sptr->ac3_stream_descriptor.stream_type != 0x00){
				bsmod = sptr->ac3_stream_descriptor.bsmod;
				langflag = sptr->ac3_stream_descriptor.langflag;
				if(langflag){
					if(!strcmp(sptr->ac3_stream_descriptor.language, "eng")){
						lang_match = 1;
					}
				}else{
					if(!strcmp(sptr->ac3_stream_descriptor.text_desc, "en")){
						lang_match = 1;
					}
				}
 				if(bsmod == 0x02 && lang_match){
					ccx_dtvcc_log("2.audio_service_descriptor CCX_DTVCC_AUDIO_EVI founded");
					return sptr;
				}
			}
		}
		sptr = sptr->next;
	}
	return 0;
}

void _dtvcc_tta_request_audio_play(ccx_dtvcc_ctx *ctx, uint16_t audio_pid, uint16_t audio_codec)
{
	dvb_player_t* player = (dvb_player_t*)ctx->handle;

	ccx_dtvcc_log( "#_dtvcc_tta_request_audio_play: audio_codec[%d], audio_pid[0x%04X]\n", audio_codec, audio_pid);

	AVP_AudioConfig audioConfig;
	player->avpInfo.playerConfig.audio_pid =audioConfig.pid = (uint16_t) audio_pid;//ctx->main_audio_pid;
	player->avpInfo.playerConfig.audio_codec = audioConfig.codec = (AVP_Codec) audio_codec;//ctx->main_audio_codec;

#ifdef USE_BTV_HAL_MANAGER
	HalBtv_SetAudioStream(player->idx_player, &audioConfig);
#else
	AVP_SetAudioStream(player->avpInfo.playerHandle, &audioConfig);
#endif
}

void _dtvcc_tta_audio_descriptor_handle(ccx_dtvcc_ctx *ctx)
{
	ccx_dtvcc_audio_service_descriptor* descriptor;

	uint16_t audio_pid;
	uint16_t audio_codec;		
	int select_type;
	
	if(ctx->is_audio_active == 0){
		if(ctx->audio_language == 0){
			// In case KCM
			select_type = CCX_DTVCC_AUDIO_KCM;
		}else{
			// In case ECM
			select_type = CCX_DTVCC_AUDIO_ECM;
		}
	}else{
		if(ctx->audio_language == 0){
			// In case KVI
			select_type = CCX_DTVCC_AUDIO_KVI;
		}else{
			// In case EVI
			select_type = CCX_DTVCC_AUDIO_EVI;
		}
	}

	descriptor = _dtvcc_tta_get_audio_service_descriptor(ctx, select_type);
	if(!descriptor){
		ccx_dtvcc_log("audio_service_descriptor TYPE: %d not founded", select_type);
		if(select_type == CCX_DTVCC_AUDIO_KCM){
			descriptor = _dtvcc_tta_get_audio_service_descriptor(ctx, CCX_DTVCC_AUDIO_ECM);
			if(!descriptor) return;			
		}else if(select_type == CCX_DTVCC_AUDIO_ECM){
			descriptor = _dtvcc_tta_get_audio_service_descriptor(ctx, CCX_DTVCC_AUDIO_KCM);
			if(!descriptor) return;
		}else if(select_type == CCX_DTVCC_AUDIO_KVI){
			descriptor = _dtvcc_tta_get_audio_service_descriptor(ctx, CCX_DTVCC_AUDIO_KCM);
			if(!descriptor) return;
		}else if(select_type == CCX_DTVCC_AUDIO_EVI){
			descriptor = _dtvcc_tta_get_audio_service_descriptor(ctx, CCX_DTVCC_AUDIO_ECM);
			if(!descriptor) return;
		}
	}

	if(!descriptor) {
		descriptor = _dtvcc_tta_get_audio_service_descriptor(ctx, CCX_DTVCC_AUDIO_KCM);
		if(!descriptor) return;
	}
	
	if(descriptor->ac3_stream_descriptor.stream_type != 0x00){
		audio_pid = descriptor->ac3_stream_descriptor.pid;
		audio_codec = _dtvcc_convert_audio_codec(descriptor->ac3_stream_descriptor.stream_type);
		_dtvcc_tta_request_audio_play(ctx, audio_pid, audio_codec);
		return;
	}		

	if(descriptor->language_descriptor.stream_type != 0x00){
		audio_pid = descriptor->language_descriptor.pid;
		audio_codec = _dtvcc_convert_audio_codec(descriptor->language_descriptor.stream_type); 		
		_dtvcc_tta_request_audio_play(ctx, audio_pid, audio_codec);
		return;
	}
}


void _dtvcc_audio_descriptor_handle(ccx_dtvcc_ctx *ctx)
{
	uint16_t audio_pid;
	uint16_t audio_codec;		

	if(!ctx->is_audio_active) return; 

	ccx_dtvcc_audio_service_descriptor *descriptor = &ctx->audio_descriptor;
	
	if(descriptor->ac3_stream_descriptor.stream_type != 0x00){
		int bsmod = descriptor->ac3_stream_descriptor.bsmod;
		int full_svc =	descriptor->ac3_stream_descriptor.full_svc;
		if(bsmod == 0x02 && full_svc == 0x01){
			audio_pid = descriptor->ac3_stream_descriptor.pid;
			audio_codec = _dtvcc_convert_audio_codec(descriptor->ac3_stream_descriptor.stream_type);
			_dtvcc_request_vi_audio_play(ctx, audio_pid, audio_codec);
			return;
		}
	}		

	if(descriptor->language_descriptor.stream_type != 0x00){
		int audio_type = descriptor->language_descriptor.audio_type;
		if(audio_type == 0x03){
			audio_pid = descriptor->language_descriptor.pid;
			audio_codec = _dtvcc_convert_audio_codec(descriptor->language_descriptor.stream_type); 		
			_dtvcc_request_vi_audio_play(ctx, audio_pid, audio_codec);
			return;
		}
	}

	_dtvcc_request_main_audio_play(ctx);
}

void _dtvcc_kec_pid_handle(ccx_dtvcc_ctx *ctx, TS_PSI_Program *program_info)
{
	if(program_info->num_video_pids == 0 || program_info->num_audio_pids == 0 )
		return;

	char *buf = (char *) ctx->buffer;

	if(ctx->video_pid == 0) ctx->video_pid = program_info->video_pids[0].pid;
	
	if(ctx->video_pid != program_info->video_pids[0].pid){
		ccx_dtvcc_log("From [0x%04X] To [0x%04X] video_pid changed... ", ctx->video_pid, program_info->video_pids[0].pid);
		ctx->video_pid = program_info->video_pids[0].pid;

		int buf_len = sprintf(buf, "%d", program_info->video_pids[0].pid);
		buf[buf_len]='\0';
		ccx_dtvcc_log("CCX_EVENT_VPID_CHAGNED pid: [0x%04X] To event to app... ", program_info->video_pids[0].pid);
		ccx_dtvcc_pid_callback_event(ctx, CCX_EVENT_VPID_CHAGNED, buf);		
	}

	if(ctx->audio_pid == 0){
		ctx->audio_pid = program_info->audio_pids[0].pid;
		ctx->num_audio_pids = program_info->num_audio_pids;
	}
	
	if(ctx->audio_pid != program_info->audio_pids[0].pid){
		ccx_dtvcc_log("From [0x%04X] To [0x%04X] audio_pid changed... ", ctx->audio_pid, program_info->audio_pids[0].pid);
		ctx->audio_pid = program_info->audio_pids[0].pid;
		
		int buf_len = sprintf(buf, "%d", program_info->audio_pids[0].pid);
		buf[buf_len]='\0';
		ccx_dtvcc_log("CCX_EVENT_APID_CHAGNED pid: [0x%04X] To event to app... ", program_info->audio_pids[0].pid);
		ccx_dtvcc_pid_callback_event(ctx, CCX_EVENT_APID_CHAGNED, buf);		
	}

	if(ctx->num_audio_pids != program_info->num_audio_pids){
		ctx->num_audio_pids = program_info->num_audio_pids;
		uint16_t pid = 0;
		if(ctx->num_audio_pids >= 2){
			pid = program_info->audio_pids[1].pid;
		}
		int buf_len = sprintf(buf, "%d", pid);
		buf[buf_len]='\0';
		ccx_dtvcc_log("CCX_EVENT_NUM_APID_CHAGNED pid: [0x%04X] To event to app... ", pid);
		ccx_dtvcc_pid_callback_event(ctx, CCX_EVENT_NUM_APID_CHAGNED, buf);
	}
}


void _dtvcc_free_section_data(ccx_section_data* section)
{
	if(section)
	{
		if(section->section_data)
			free(section->section_data);
		free(section);
	}
}

void _dtvcc_free_dsmcc_section_data(ccx_dsmcc_section_data* section)
{
	if(section)
	{
		if(section->section_data)
			free(section->section_data);
		free(section);
	}
}

void _dtvcc_get_section_header(unsigned char *buf, TS_PSI_header *p_header )
{
	p_header->table_id = buf[TS_PSI_TABLE_ID_OFFSET];
	p_header->section_syntax_indicator = (buf[TS_PSI_SECTION_LENGTH_OFFSET]>>7)&1;
	p_header->private_indicator = (buf[TS_PSI_SECTION_LENGTH_OFFSET]>>6)&1;
	p_header->section_length = TS_PSI_GET_SECTION_LENGTH(buf);
	p_header->table_id_extension = (uint16_t)(TS_READ_16(&(buf)[TS_PSI_TABLE_ID_EXT_OFFSET] ) & 0xFFFF);
	p_header->version_number = (uint8_t)((buf[TS_PSI_CNI_OFFSET]>>1)&0x1F);
	p_header->current_next_indicator = buf[TS_PSI_CNI_OFFSET]&1;
	p_header->section_number = buf[TS_PSI_SECTION_NUMBER_OFFSET];
	p_header->last_section_number = buf[TS_PSI_LAST_SECTION_NUMBER_OFFSET];
	p_header->CRC_32 = TS_READ_32( &(buf[p_header->section_length+TS_PSI_SECTION_LENGTH_OFFSET-4]) );
}

int _dtvcc_pat_get_program_count(unsigned char *buf)
{
	return (TS_PSI_MAX_BYTE_OFFSET(buf)-(TS_PSI_LAST_SECTION_NUMBER_OFFSET+1))/4;
}

int _dtvcc_pat_get_program(unsigned char *buf, unsigned int size, int program_num, TS_PAT_program *program )
{
	int byteOffset = TS_PSI_LAST_SECTION_NUMBER_OFFSET+1;

	byteOffset += program_num*4;

	if( byteOffset >= TS_PSI_MAX_BYTE_OFFSET(buf) || byteOffset >= (int)size)
	{
		return -1;
	}

	program->program_number = TS_READ_16( &buf[byteOffset] );
	program->PID = (uint16_t)(TS_READ_16( &buf[byteOffset+2] ) & 0x1FFF);

	return 0;
}

uint8_t* _dtvcc_pmt_p_get_descriptor(uint8_t *buf, uint32_t descriptors_length, int descriptor_num )
{
	uint8_t *descriptor;
	int i;
	uint32_t offset = 0;

	descriptor = NULL;

	for( i = 0; offset < descriptors_length; i++ )
	{
		descriptor = &buf[offset];
		offset += buf[offset+1] + 2;

		if( i == descriptor_num )
			break;
		else
			descriptor = NULL;
	}

	return descriptor;
}

int _dtvcc_pmt_p_get_stream_offset(uint8_t *buf, unsigned int size, int stream_num )
{
	int offset;
	int i;

	/* After the last descriptor */
	offset = STREAM_BASE(buf);

	for (i=0; i < stream_num; i++)
	{
		if (offset >= (int)size || offset >= TS_PSI_MAX_BYTE_OFFSET(buf))
			return -1;
		offset += 5 + (TS_READ_16( &buf[offset+3] ) & 0xFFF);
	}

	return offset;
}

uint16_t _dtvcc_pmt_get_pcr_pid(unsigned char *buf, unsigned int size)
{
	if (size < TS_PSI_LAST_SECTION_NUMBER_OFFSET+3)
	{
		return 0x1FFF;
	}
	
	return (uint16_t)(TS_READ_16( &buf[TS_PSI_LAST_SECTION_NUMBER_OFFSET+1] ) & 0x1FFF);
}

uint8_t* _dtvcc_pmt_get_descriptor( uint8_t *buf, unsigned int size, int descriptor_num )
{
	uint8_t *descriptor_base = DESCRIPTOR_BASE(buf);
	uint32_t descriptors_length = PROGRAM_INFO_LENGTH(buf);

	/* Any time we dereference memory based on the contents of live data,
	we should check. */
	if (descriptor_base - buf >= (int)size) {
		return NULL;
	}
	size -= (descriptor_base - buf);
	if (size < descriptors_length) {
		return NULL;
	}
	return _dtvcc_pmt_p_get_descriptor(descriptor_base, descriptors_length, descriptor_num);
}


void _dtvcc_pmt_proc_program_descriptors( uint8_t *p_bfr, unsigned int size, TS_PSI_Program *prog_info )
{
	int i;
	uint8_t *descriptor;

	for( i = 0, descriptor = _dtvcc_pmt_get_descriptor( p_bfr, size, i );
		descriptor != NULL;
		i++, descriptor = _dtvcc_pmt_get_descriptor( p_bfr, size, i ) )
	{
		switch (descriptor[0])
		{
			case TS_PSI_DT_CA:
				prog_info->ca_pid = ((descriptor[4] & 0x1F) << 8) + descriptor[5];
			break;

		default:
			break;
		}
	}
}


int _dtvcc_pmt_proc_program_descriptors_get_caption( uint8_t *p_bfr, unsigned int size, ccx_dtvcc_service_descriptor *cc_descriptor, int active_service_num)
{
	int i;
	uint8_t *descriptor;
	int nb_service;
	int service_index;
	unsigned char *cc_service;
	ccx_dtvcc_service_descriptor caption_descriptor;

	ccx_dtvcc_log("program_descriptors_get_caption");

	for( i = 0, descriptor = _dtvcc_pmt_get_descriptor( p_bfr, size, i );
		descriptor != NULL;
		i++, descriptor = _dtvcc_pmt_get_descriptor( p_bfr, size, i ) )
	{
		switch (descriptor[0])
		{
		/* 0x86 : Caption Service Descriptor*/
		case TS_PSI_DT_ATSC_CaptionService:
			nb_service = descriptor[2] & 0x1F;
			cc_service = &descriptor[3];
			for (service_index = 0; service_index < nb_service; service_index++)
			{
				int service_number = cc_service[3] & 0x3F;
				if (service_number < 1 || service_number > CCX_DTVCC_MAX_SERVICES){
					ccx_dtvcc_log("Invalid service number (%d), valid range is 1-%d.", service_number, CCX_DTVCC_MAX_SERVICES);
					cc_service += 6;
					continue;
				}

				ccx_dtvcc_log("CC SERVICE[%d]: language (%c%c%c)", service_index,
					cc_service[0], cc_service[1], cc_service[2]);

				caption_descriptor.language[0] = cc_service[0];
				caption_descriptor.language[1] = cc_service[1];
				caption_descriptor.language[2] = cc_service[2];
				caption_descriptor.language[3] = 0x00;
					
				caption_descriptor.digital_cc = cc_service[3] >> 7;				
				caption_descriptor.caption_service_number = cc_service[3] & 0x3F;
				caption_descriptor.easy_reader = cc_service[4] >> 7;
				caption_descriptor.wide_aspect_ratio = (cc_service[4] >> 6) & 0x01;
				caption_descriptor.korean_code = (cc_service[4] >> 5) & 0x01;
				
				ccx_dtvcc_log("CC SERVICE[%d]: digital cc (0x%02X)", service_index,
					caption_descriptor.digital_cc);
				ccx_dtvcc_log("CC SERVICE[%d]: caption_service_number (0x%02X)", service_index,
					caption_descriptor.caption_service_number);
				ccx_dtvcc_log("CC SERVICE[%d]: easy_reader (0x%02X)", service_index,
					caption_descriptor.easy_reader);
				ccx_dtvcc_log("CC SERVICE[%d]: wide_aspect_ratio (0x%02X)", service_index,
					caption_descriptor.wide_aspect_ratio);
				ccx_dtvcc_log("CC SERVICE[%d]: korean_code (0x%02X)", service_index,
					caption_descriptor.korean_code);

				if(service_number == active_service_num){
					ccx_dtvcc_log("CC SERVICE: active_service_num found (0x%02X)", active_service_num);

					cc_descriptor->language[0] = cc_service[0];
					cc_descriptor->language[1] = cc_service[1];
					cc_descriptor->language[2] = cc_service[2];
					cc_descriptor->language[3] = 0x00;
					
					cc_descriptor->digital_cc = cc_service[3] >> 7; 			
					cc_descriptor->caption_service_number = cc_service[3] & 0x3F;
					cc_descriptor->easy_reader = cc_service[4] >> 7;
					cc_descriptor->wide_aspect_ratio = (cc_service[4] >> 6) & 0x01;
					cc_descriptor->korean_code = (cc_service[4] >> 5) & 0x01;
				}
				
				cc_service += 6;
			}
			return 1;
			break;

		default:
			ccx_dtvcc_log("Unsupported descriptor 0x%02x", descriptor[0]);			
			break;
		}
	}
	return 0;
}


int _dtvcc_pmt_get_stream_count( uint8_t *buf, unsigned int size)
{
	int offset;
	int i = 0;

	offset = STREAM_BASE(buf);

	while (offset < TS_PSI_MAX_BYTE_OFFSET(buf) && offset < (int)size)
	{
		offset += 5 + (TS_READ_16( &buf[offset+3] ) & 0xFFF);
		i++;
	}

	return i;
}

int _dtvcc_pmt_get_stream( uint8_t *buf, unsigned int size, int stream_num, TS_PMT_stream *p_stream )
{
	int offset;

	offset = _dtvcc_pmt_p_get_stream_offset( buf, size, stream_num );
	if (offset == -1)
		return 1;

	p_stream->stream_type = buf[offset];
	p_stream->elementary_PID = (uint16_t)(TS_READ_16( &buf[offset+1] ) & 0x1FFF);

	return 0;
}

uint8_t* _dtvcc_pmt_get_stream_descriptor( uint8_t *buf, unsigned int size, int stream_num, int descriptor_num )
{
	int offset;
	uint32_t descriptors_length;

	offset = _dtvcc_pmt_p_get_stream_offset( buf, size, stream_num );
	if (offset == -1)
		return NULL;

	descriptors_length = TS_READ_16(&buf[offset+3])&0xFFF;

	return (_dtvcc_pmt_p_get_descriptor(&buf[offset+5], descriptors_length, descriptor_num ));
}

uint16_t _dtvcc_pmt_proc_stream_descriptors_get_ca_pid( uint8_t *p_bfr, unsigned int size, int stream_num )
{
	int i;
	uint8_t *descriptor;

	for( i = 0, descriptor = _dtvcc_pmt_get_stream_descriptor( p_bfr, size, stream_num, i );
		descriptor != NULL;
		i++, descriptor = _dtvcc_pmt_get_stream_descriptor( p_bfr, size, stream_num, i ) )
	{
		switch (descriptor[0])
		{
		case TS_PSI_DT_CA:
			return ((descriptor[4] & 0x1F) << 8) + descriptor[5];
			break;

		default:
			break;
		}
	}
	return 0;
}

int _dtvcc_pmt_proc_stream_descriptors_get_audio( uint8_t *p_bfr, unsigned int size, int stream_num, 
					uint8_t stream_type, uint16_t pid, ccx_dtvcc_audio_service_descriptor *audio_descriptor)
{
	int i;
	uint8_t *descriptor;
	int num_channels;
	int bsmod;
	int lang_flag;	
	int lang_flag2;		
	int length;
	int text_len;
	int text_index;
	int desc_index;
	unsigned char *audio_service;

	ccx_dtvcc_log("stream_descriptors_get_audio");
	
	memset((void*)audio_descriptor, 0x00, sizeof(ccx_dtvcc_audio_service_descriptor));

	for( i = 0, descriptor = _dtvcc_pmt_get_stream_descriptor( p_bfr, size, stream_num, i );
		descriptor != NULL;
		i++, descriptor = _dtvcc_pmt_get_stream_descriptor( p_bfr, size, stream_num, i ) )
	{
        switch (descriptor[0])
		{
		/* 0x81 : AC-3 Audio Stream Descriptor */
		case TS_PSI_DT_ATSC_AC3_Audio:

			length = descriptor[1];
			audio_service = &descriptor[2];

#ifdef DEBUG_CC_PACKETS			
			ccx_dtvcc_log("[CEA-708] TS_PSI_DT_ATSC_AC3_Audio: descriptor data len:%d\n", length);
			ccx_dtvcc_log_dump((char*) audio_service, length);
#endif

			audio_descriptor->ac3_stream_descriptor.stream_type = stream_type;
			audio_descriptor->ac3_stream_descriptor.pid = pid;
#ifdef DTVCC_PRINT_DEBUG
			ccx_dtvcc_log("ATSC_AC3_Audio : stream_type (0x%02X)", 
						audio_descriptor->ac3_stream_descriptor.stream_type);
			ccx_dtvcc_log("ATSC_AC3_Audio : pid (0x%04X)", 
						audio_descriptor->ac3_stream_descriptor.pid);
#endif			
			desc_index = 0;
			audio_descriptor->ac3_stream_descriptor.sample_rate_code = (audio_service[desc_index] >> 5);
			audio_descriptor->ac3_stream_descriptor.bsid = audio_service[desc_index] & 0x1F;

#ifdef DTVCC_PRINT_DEBUG
			ccx_dtvcc_log("ATSC_AC3_Audio : sample_rate_code (0x%02X)", 
						audio_descriptor->ac3_stream_descriptor.sample_rate_code);
			ccx_dtvcc_log("ATSC_AC3_Audio : bsid (0x%02X)", 
						audio_descriptor->ac3_stream_descriptor.bsid);
#endif			
			desc_index++;
			audio_descriptor->ac3_stream_descriptor.bit_rate_code = (audio_service[desc_index] >> 2);
			audio_descriptor->ac3_stream_descriptor.surround_mode = audio_service[desc_index] & 0x03;
			
#ifdef DTVCC_PRINT_DEBUG		
			ccx_dtvcc_log("ATSC_AC3_Audio : bit_rate_code (0x%02X)", 
						audio_descriptor->ac3_stream_descriptor.bit_rate_code);
			ccx_dtvcc_log("ATSC_AC3_Audio : surround_mode (0x%02X)", 
						audio_descriptor->ac3_stream_descriptor.surround_mode);
#endif
			desc_index++;
			audio_descriptor->ac3_stream_descriptor.bsmod = bsmod = (audio_service[desc_index] >> 5);
			audio_descriptor->ac3_stream_descriptor.num_channels = num_channels = (audio_service[desc_index] >> 1) & 0x0F;			
			audio_descriptor->ac3_stream_descriptor.full_svc = audio_service[desc_index] & 0x01;	

#ifdef DTVCC_PRINT_DEBUG
			ccx_dtvcc_log("ATSC_AC3_Audio : bsmod (0x%02X)", 
						audio_descriptor->ac3_stream_descriptor.bsmod);
			ccx_dtvcc_log("ATSC_AC3_Audio : num_channels (%d)", 
						audio_descriptor->ac3_stream_descriptor.num_channels);
			ccx_dtvcc_log("ATSC_AC3_Audio : full_svc (0x%02X)", 
						audio_descriptor->ac3_stream_descriptor.full_svc);
#endif			
			desc_index++;
			audio_descriptor->ac3_stream_descriptor.langcod = audio_service[desc_index];

#ifdef DTVCC_PRINT_DEBUG			
			ccx_dtvcc_log("ATSC_AC3_Audio : langcod (0x%02X)", 
						audio_descriptor->ac3_stream_descriptor.langcod);
#endif			
			if(num_channels == 0){
				desc_index++;
				audio_descriptor->ac3_stream_descriptor.langcod2 = audio_service[desc_index];
#ifdef DTVCC_PRINT_DEBUG				
				ccx_dtvcc_log("ATSC_AC3_Audio : langcod2 (0x%02X)", 
							audio_descriptor->ac3_stream_descriptor.langcod2);
#endif
			}
			
			desc_index++;
			if(bsmod < 2){
				audio_descriptor->ac3_stream_descriptor.mainid = (audio_service[desc_index] >> 5);
				audio_descriptor->ac3_stream_descriptor.priority = (audio_service[desc_index] >> 3) & 0x03;	

#ifdef DTVCC_PRINT_DEBUG								
				ccx_dtvcc_log("ATSC_AC3_Audio : mainid (0x%02X)", 
							audio_descriptor->ac3_stream_descriptor.mainid);
				ccx_dtvcc_log("ATSC_AC3_Audio : priority (0x%02X)", 
							audio_descriptor->ac3_stream_descriptor.priority);
#endif				
			}else{
				audio_descriptor->ac3_stream_descriptor.asvcflags = audio_service[desc_index];			
#ifdef DTVCC_PRINT_DEBUG
				ccx_dtvcc_log("ATSC_AC3_Audio : asvcflags (0x%02X)", 
							audio_descriptor->ac3_stream_descriptor.asvcflags);
#endif
			}
			
			desc_index++;
			text_len = (audio_service[desc_index] >> 1);
			audio_descriptor->ac3_stream_descriptor.text_code = audio_service[desc_index] & 0x01;

#ifdef DTVCC_PRINT_DEBUG			
			ccx_dtvcc_log("ATSC_AC3_Audio : text_len (%d)", 
						text_len);
			ccx_dtvcc_log("ATSC_AC3_Audio : text_code (0x%02X)", 
						audio_descriptor->ac3_stream_descriptor.text_code);

			ccx_dtvcc_log("ATSC_AC3_Audio : desc_index (%d), length(%d)", 
						desc_index, length);
#endif			
			
			if(desc_index == length-1) break;
			
			desc_index++;			
			for (text_index = 0; text_index < text_len; text_index++)
			{
				audio_descriptor->ac3_stream_descriptor.text_desc[text_index] = audio_service[desc_index];
				desc_index++;
			}
#ifdef DTVCC_PRINT_DEBUG			
			ccx_dtvcc_log("ATSC_AC3_Audio : text_desc (%s)", 
							audio_descriptor->ac3_stream_descriptor.text_desc);
#endif
			lang_flag = (audio_service[desc_index] >> 7); 
			lang_flag2 = (audio_service[desc_index] >> 6) & 0x01; 

			audio_descriptor->ac3_stream_descriptor.langflag = lang_flag;
			desc_index++;
			
			audio_service = &audio_service[desc_index];
			if(lang_flag == 1){
#ifdef DTVCC_PRINT_DEBUG				
				ccx_dtvcc_log("ATSC_AC3_Audio : language (%c%c%c)", 
						audio_service[0], audio_service[1], audio_service[2]);
#endif
				audio_descriptor->ac3_stream_descriptor.language[0] = audio_service[0];
				audio_descriptor->ac3_stream_descriptor.language[1] = audio_service[1];
				audio_descriptor->ac3_stream_descriptor.language[2] = audio_service[2];
				audio_descriptor->ac3_stream_descriptor.language[3] = 0x00;
				audio_service += 3;
			}
			if(lang_flag == 2){
#ifdef DTVCC_PRINT_DEBUG				
				ccx_dtvcc_log("ATSC_AC3_Audio : language_2 (%c%c%c)",
						audio_service[0], audio_service[1], audio_service[2]);
#endif
				audio_descriptor->ac3_stream_descriptor.language2[0] = audio_service[0];
				audio_descriptor->ac3_stream_descriptor.language2[1] = audio_service[1];
				audio_descriptor->ac3_stream_descriptor.language2[2] = audio_service[2];
				audio_descriptor->ac3_stream_descriptor.language2[3] = 0x00;
			}
			break;

		/* 0x0A : ISO 639 Language Descriptor */
		case TS_PSI_DT_ISO_639_Language:
			length = descriptor[1];
			audio_service = &descriptor[2];
#ifdef DEBUG_CC_PACKETS						
			ccx_dtvcc_log("[CEA-708] TS_PSI_DT_ISO_639_Language: descriptor data len:%d\n", length);
			ccx_dtvcc_log_dump((char*) audio_service, length);
#endif
			audio_descriptor->language_descriptor.stream_type = stream_type;
			audio_descriptor->language_descriptor.pid = pid;				
			audio_descriptor->language_descriptor.language[0] = audio_service[0];
			audio_descriptor->language_descriptor.language[1] = audio_service[1];
			audio_descriptor->language_descriptor.language[2] = audio_service[2];
			audio_descriptor->language_descriptor.language[3] = 0x00;
			audio_descriptor->language_descriptor.audio_type  = audio_service[3];

#ifdef DTVCC_PRINT_DEBUG			
			ccx_dtvcc_log("ISO 639 Audio : stream_type (0x%02X)", 
					stream_type);
			ccx_dtvcc_log("ISO 639 Audio : pid (0x%04X)", 
					pid);
			ccx_dtvcc_log("ISO 639 Language: language (%c%c%c)", 
					audio_service[0], audio_service[1], audio_service[2]);
			ccx_dtvcc_log("ISO 639 Language: audio_type (0x%02X)",
					audio_service[3]);
#endif			
			break;

		default:
			ccx_dtvcc_log("Unsupported descriptor 0x%02x", descriptor[0]);			
			break;
		}
	}
	return 0;
}


int _dtvcc_pmt_proc_stream_descriptors_get_caption( uint8_t *p_bfr, unsigned int size, int stream_num, ccx_dtvcc_service_descriptor *cc_descriptor, int active_service_num)
{
	int i;
	uint8_t *descriptor;
	int nb_service;
	int service_index;
	unsigned char *cc_service;
	ccx_dtvcc_service_descriptor caption_descriptor;

	ccx_dtvcc_log("stream_descriptors_get_caption");

	for( i = 0, descriptor = _dtvcc_pmt_get_stream_descriptor( p_bfr, size, stream_num, i );
		descriptor != NULL;
		i++, descriptor = _dtvcc_pmt_get_stream_descriptor( p_bfr, size, stream_num, i ) )
	{
        switch (descriptor[0])
		{
		/* 0x86 : Caption Service Descriptor*/
		case TS_PSI_DT_ATSC_CaptionService:
			nb_service = descriptor[2] & 0x1F;
			cc_service = &descriptor[3];
			for (service_index = 0; service_index < nb_service; service_index++)
			{
				int service_number = cc_service[3] & 0x3F;
				if (service_number < 1 || service_number > CCX_DTVCC_MAX_SERVICES){
					ccx_dtvcc_log("Invalid service number (%d), valid range is 1-%d.", service_number, CCX_DTVCC_MAX_SERVICES);
					cc_service += 6;
					continue;
				}

				ccx_dtvcc_log("CC SERVICE[%d]: language (%c%c%c)", service_index,
					cc_service[0], cc_service[1], cc_service[2]);

				caption_descriptor.language[0] = cc_service[0];
				caption_descriptor.language[1] = cc_service[1];
				caption_descriptor.language[2] = cc_service[2];
				caption_descriptor.language[3] = 0x00;
					
				caption_descriptor.digital_cc = cc_service[3] >> 7;				
				caption_descriptor.caption_service_number = cc_service[3] & 0x3F;
				caption_descriptor.easy_reader = cc_service[4] >> 7;
				caption_descriptor.wide_aspect_ratio = (cc_service[4] >> 6) & 0x01;
				caption_descriptor.korean_code = (cc_service[4] >> 5) & 0x01;
				
				ccx_dtvcc_log("CC SERVICE[%d]: digital cc (0x%02X)", service_index,
					caption_descriptor.digital_cc);
				ccx_dtvcc_log("CC SERVICE[%d]: caption_service_number (0x%02X)", service_index,
					caption_descriptor.caption_service_number);
				ccx_dtvcc_log("CC SERVICE[%d]: easy_reader (0x%02X)", service_index,
					caption_descriptor.easy_reader);
				ccx_dtvcc_log("CC SERVICE[%d]: wide_aspect_ratio (0x%02X)", service_index,
					caption_descriptor.wide_aspect_ratio);
				ccx_dtvcc_log("CC SERVICE[%d]: korean_code (0x%02X)", service_index,
					caption_descriptor.korean_code);

				if(service_number == active_service_num){
					ccx_dtvcc_log("CC SERVICE: active_service_num found (0x%02X)", active_service_num);

					cc_descriptor->language[0] = cc_service[0];
					cc_descriptor->language[1] = cc_service[1];
					cc_descriptor->language[2] = cc_service[2];
					cc_descriptor->language[3] = 0x00;
					
					cc_descriptor->digital_cc = cc_service[3] >> 7; 			
					cc_descriptor->caption_service_number = cc_service[3] & 0x3F;
					cc_descriptor->easy_reader = cc_service[4] >> 7;
					cc_descriptor->wide_aspect_ratio = (cc_service[4] >> 6) & 0x01;
					cc_descriptor->korean_code = (cc_service[4] >> 5) & 0x01;
				}
				
				cc_service += 6;
			}
			return 1;
			break;

		default:
			ccx_dtvcc_log("Unsupported descriptor 0x%02x", descriptor[0]);			
			break;
		}
	}
	return 0;
}

int _dtvcc_pmt_proc_stream_descriptors_get_dsmcc( uint8_t *p_bfr, unsigned int size, int stream_num, 
					uint8_t stream_type, uint16_t pid, ccx_dtvcc_dsmcc_stream_service *dsmcc_stream)
{
	int i;
	uint8_t *descriptor;
	int length;
	int desc_index;
	uint32_t carousel_id;
	uint8_t format_id;
	uint16_t association_tag;	
	uint16_t use;		
	uint16_t data_broadcast_id;
	uint8_t component_tag;	
	unsigned char *service;

	memset((void*)dsmcc_stream, 0x00, sizeof(ccx_dtvcc_dsmcc_stream_service));
	dsmcc_stream->stream_type = stream_type;
	dsmcc_stream->pid = pid;

	for( i = 0, descriptor = _dtvcc_pmt_get_stream_descriptor( p_bfr, size, stream_num, i );
		descriptor != NULL;
		i++, descriptor = _dtvcc_pmt_get_stream_descriptor( p_bfr, size, stream_num, i ) )
	{
        switch (descriptor[0])
		{
		/* 0x13 : Carousel Identifier Descriptor */
		case TS_PSI_DT_13818_6_DSMCC_1:
			length = descriptor[1];
			service = &descriptor[2];
			
			ccx_dtvcc_log("DSM-CC Stream - DescriptorTag (0x%02X): Carousel Identifier Descriptor", 
					descriptor[0]);

			ccx_dtvcc_log("Descriptor_length: %d", length);
			desc_index = 0;

			carousel_id = (uint32_t)TS_READ_32( &(service)[desc_index] );
			desc_index += 4;

			format_id = service[desc_index];
			
			dsmcc_stream->carousel_id_descriptor.carousel_id = carousel_id;
			dsmcc_stream->carousel_id_descriptor.format_id = format_id;

			ccx_dtvcc_log("carousel_id: (0x%08X)", carousel_id);
			ccx_dtvcc_log("format_id: (0x%02X)", format_id);

			if(format_id == 0x00){
				ccx_dtvcc_log("Standard Boot");
			} else if(format_id == 0x01){
				ccx_dtvcc_log("Enhanced Boot");
			}
			break;

		/* 0x14 : Association Tag Descriptor */
		case TS_PSI_DT_13818_6_DSMCC_2:
			length = descriptor[1];
			service = &descriptor[2];
			ccx_dtvcc_log("DSM-CC Stream - DescriptorTag (0x%02X): Association Tag Descriptor", 
					descriptor[0]);

			ccx_dtvcc_log("Descriptor_length: %d", length);
			
			desc_index = 0;
			association_tag = (uint16_t)TS_READ_16( &(service)[desc_index] );
			desc_index += 2;
			use = (uint16_t)TS_READ_16( &(service)[desc_index] );
			desc_index += 2;

			dsmcc_stream->association_tag_descriptor.association_tag = association_tag;
			dsmcc_stream->association_tag_descriptor.use = use;

			if(use == 0x0000){
				int selector_length = service[desc_index];
				desc_index += 1;

				uint32_t transaction_id = (uint32_t)TS_READ_32( &(service)[desc_index] );
				desc_index += 4;

				uint32_t timeout = (uint32_t)TS_READ_32( &(service)[desc_index] );

				dsmcc_stream->association_tag_descriptor.transaction_id = transaction_id;
				dsmcc_stream->association_tag_descriptor.timeout = timeout;
			}

			ccx_dtvcc_log("association_tag: (0x%04X)", association_tag);
			ccx_dtvcc_log("use: (0x%04X)", use);
			ccx_dtvcc_log("transaction_id: (0x%08X)", 
					dsmcc_stream->association_tag_descriptor.transaction_id);
			ccx_dtvcc_log("timeout: (0x%08X)",
					dsmcc_stream->association_tag_descriptor.timeout);
			break;

		/* 0x66 : Data Broadcast ID Descriptor */
		case TS_PSI_DT_DVB_DataBroadcastID:
			length = descriptor[1];
			service = &descriptor[2];
			ccx_dtvcc_log("DSM-CC Stream - DescriptorTag (0x%02X): Data Broadcast ID Descriptor", 
					descriptor[0]);

			ccx_dtvcc_log("Descriptor_length: %d", length);
			
			data_broadcast_id = (uint16_t)TS_READ_16( &(service)[0] );
			dsmcc_stream->broadcast_id_descriptor.data_broadcast_id = data_broadcast_id;

			ccx_dtvcc_log("data_broadcast_id: (0x%04X)", data_broadcast_id);
			break;

		/* 0x52 : Stream ID Descriptor */
		case TS_PSI_DT_DVB_StreamIdentifier:
			length = descriptor[1];
			service = &descriptor[2];
			ccx_dtvcc_log("DSM-CC Stream - DescriptorTag (0x%02X): Stream ID Descriptor", 
					descriptor[0]);
			ccx_dtvcc_log("Descriptor_length: %d", length);

			component_tag = service[0];
			dsmcc_stream->stream_id_descriptor.component_tag = component_tag;
			ccx_dtvcc_log("component_tag: (0x%02X)", component_tag);
			break;

		default:
			ccx_dtvcc_log("Unsupported descriptor 0x%02x", descriptor[0]);			
			break;
		}
	}
	return 0;
}

int _dtvcc_pmt_proc_stream_descriptors_get_ait( uint8_t *p_bfr, unsigned int size, int stream_num, 
					uint8_t stream_type, uint16_t pid, ccx_dtvcc_ait_stream_service *ait_stream)
{
	int i;
	uint8_t *descriptor;
	int length;
	int desc_index;
	uint16_t application_type;	
	uint8_t ait_version;	
	unsigned char *service;

	memset((void*)ait_stream, 0x00, sizeof(ccx_dtvcc_ait_stream_service));
	ait_stream->stream_type = stream_type;
	ait_stream->pid = pid;

	for( i = 0, descriptor = _dtvcc_pmt_get_stream_descriptor( p_bfr, size, stream_num, i );
		descriptor != NULL;
		i++, descriptor = _dtvcc_pmt_get_stream_descriptor( p_bfr, size, stream_num, i ) )
	{
        switch (descriptor[0])
		{
		/* Applicaiton Signalling Descriptor */
		case TS_PSI_DT_DVB_ApplicationSignalling:
			length = descriptor[1];
			service = &descriptor[2];
			ccx_dtvcc_log("AIT Stream - DescriptorTag (0x%02X): Applicaiton Signalling Descriptor", 
					descriptor[0]);
			ccx_dtvcc_log("Descriptor_length: %d", length);
			
			desc_index = 0;
			
			application_type = TS_READ_16(&service[desc_index])&0x7FFF;
			desc_index += 2;
 
			ait_version = service[desc_index]&0x1F;
 
			ait_stream->signalling_descriptor.application_type = application_type;
			ait_stream->signalling_descriptor.ait_version = ait_version;
 				
			ccx_dtvcc_log("application_type: (0x%04X)", application_type);
			ccx_dtvcc_log("ait_version: (0x%02X)", ait_version);
		break;
 		default:
			ccx_dtvcc_log("Unsupported descriptor 0x%02x", descriptor[0]);			
			break;
		}
	}
	return 0;
}

void _dtvcc_insert_dsmcc_stream(ccx_dtvcc_ctx *ctx, ccx_dtvcc_dsmcc_stream *str)
{
    str->next = 0;
	/* appending a new entry to the back of the list */
	if (ctx->dsmcc_stream_head) {
		// List is not empty
		str->next = ctx->dsmcc_stream_head;
		ctx->dsmcc_stream_head = str;
	} else {
		// List is empty
		ctx->dsmcc_stream_head = str;
	}
}

void _dtvcc_free_dsmcc_stream(ccx_dtvcc_ctx *ctx)
{
	ccx_dtvcc_dsmcc_stream *sptr = ctx->dsmcc_stream_head;
	while(sptr){
		ctx->dsmcc_stream_head = sptr->next;
		free(sptr);
		sptr = ctx->dsmcc_stream_head;
	}
}

void _dtvcc_set_audio_descriptor(ccx_dtvcc_ctx *ctx, ccx_dtvcc_audio_service_descriptor *descriptor)
{
	ccx_dtvcc_audio_service_descriptor *target = &ctx->audio_descriptor;
	if(descriptor->ac3_stream_descriptor.stream_type != 0x00){
		int bsmod = descriptor->ac3_stream_descriptor.bsmod;
		int full_svc =	descriptor->ac3_stream_descriptor.full_svc;
		if(bsmod == 0x02 && full_svc == 0x01){
			memcpy(&target->ac3_stream_descriptor, &descriptor->ac3_stream_descriptor, sizeof(ccx_dtvcc_ac3_stream_descriptor));
		}
	}		

	if(descriptor->language_descriptor.stream_type != 0x00){
		int audio_type = descriptor->language_descriptor.audio_type;
		if(audio_type == 0x03){
			memcpy(&target->language_descriptor, &descriptor->language_descriptor, sizeof(ccx_dtvcc_iso_639_language_descriptor));
		}
	}
}


void _dtvcc_insert_audio_descriptor(ccx_dtvcc_ctx *ctx, ccx_dtvcc_audio_service_descriptor *descriptor)
{
	ccx_dtvcc_audio_service_descriptor *target = calloc(sizeof(char), sizeof(ccx_dtvcc_audio_service_descriptor));
	if (target){
		if(descriptor->ac3_stream_descriptor.stream_type != 0x00){
			memcpy(&target->ac3_stream_descriptor, &descriptor->ac3_stream_descriptor, sizeof(ccx_dtvcc_ac3_stream_descriptor));
		}		

		if(descriptor->language_descriptor.stream_type != 0x00){
			memcpy(&target->language_descriptor, &descriptor->language_descriptor, sizeof(ccx_dtvcc_iso_639_language_descriptor));
		}
		
		target->next = 0;
		/* appending a new entry to the back of the list */
		if (ctx->audio_descriptor_head) {
			// List is not empty
			target->next = ctx->audio_descriptor_head;
			ctx->audio_descriptor_head = target;
		} else {
			// List is empty
			ctx->audio_descriptor_head = target;
		}
		ctx->vi_audio_count++;
	}
}

void _dtvcc_free_audio_descriptor(ccx_dtvcc_ctx *ctx)
{
	ccx_dtvcc_audio_service_descriptor *sptr = ctx->audio_descriptor_head;
	while(sptr){
		ctx->audio_descriptor_head = sptr->next;
		free(sptr);
		sptr = ctx->audio_descriptor_head;
	}
}


void _dtvcc_pmt_get_program(ccx_dtvcc_ctx *ctx, void *pmt, unsigned int pmt_size, TS_PSI_Program *program_info)
{
    int i;
    int stream_count;
    TS_PMT_stream pmt_stream;
    TS_PSI_header header;
    ccx_dtvcc_audio_service_descriptor audio_descriptor;

    _dtvcc_get_section_header(pmt, &header );

    /* Store the main information about the program */
    program_info->program_number   = header.table_id_extension;
    program_info->version          = header.version_number;
    program_info->pcr_pid          = _dtvcc_pmt_get_pcr_pid(pmt, pmt_size);

    /* find and process Program descriptors */
    _dtvcc_pmt_proc_program_descriptors(pmt, pmt_size, program_info );

    /* find and process Program descriptors for caption service descriptor */
//    if (_dtvcc_pmt_proc_program_descriptors_get_caption(pmt, pmt_size, &ctx->cc_descriptor, ctx->active_service_num)) {
//        ccx_dtvcc_log("program descriptors caption info exist... ");					
//    }

    /* Find the video and audio pids... */
    program_info->num_video_pids   = 0;
    program_info->num_audio_pids   = 0;
    program_info->num_other_pids   = 0;

    stream_count = _dtvcc_pmt_get_stream_count(pmt, pmt_size);
    ccx_dtvcc_log("stream_count %d", stream_count);	
    for( i = 0; i < stream_count; i++ )
    {
        int desc_index = 0;
        if (_dtvcc_pmt_get_stream(pmt, pmt_size, i, &pmt_stream )) {
            continue;
        }

        ccx_dtvcc_log("stream_type 0x%02X", pmt_stream.stream_type);

        switch( pmt_stream.stream_type )
        {
        /* video formats */
        case TS_PSI_ST_11172_2_Video:  /* MPEG-1 */
        case TS_PSI_ST_ATSC_Video:   /* ATSC MPEG-2 */
        case TS_PSI_ST_13818_2_Video: /* MPEG-2 */
        case TS_PSI_ST_14496_2_Video: /* MPEG-4 Part 2 */
        case TS_PSI_ST_14496_10_Video: /* H.264/AVC */
        case TS_PSI_ST_14496_10_AnnexG_Video: /* H.264/SVC */
        case TS_PSI_ST_14496_10_AnnexH_Video: /* H.264/MVC */
        case TS_PSI_ST_23008_2_Video: /* H.265/HEVC */
        case TS_PSI_ST_AVS_Video: /* AVS */
            ccx_dtvcc_log("Video stream type 0x%02X", pmt_stream.stream_type);
            ADD_VIDEO_PID(program_info, pmt_stream.elementary_PID, pmt_stream.stream_type, pmt, pmt_size, i);
            if (_dtvcc_pmt_proc_stream_descriptors_get_caption(pmt, pmt_size, i, &ctx->cc_descriptor, ctx->active_service_num )) {
                ccx_dtvcc_log("stream descriptors caption info exist... ");
            }
            if(ctx->init_korean_code == 0){
                ctx->init_korean_code = 1;
                ctx->korean_code = ctx->cc_descriptor.korean_code;
                if(ctx->korean_code) {
                    ctx->check_unicode_routine = 1;
                    ctx->check_character_cnt = 0;
                }
            }
            break;
        /* audio formats */
        case TS_PSI_ST_11172_3_Audio: /* MPEG-1 */
        case TS_PSI_ST_13818_3_Audio: /* MPEG-2 */
        case TS_PSI_ST_13818_7_AAC:  /* MPEG-2 AAC */
        case TS_PSI_ST_14496_3_Audio: /* MPEG-4 AAC */
        case TS_PSI_ST_ATSC_AC3:      /* ATSC AC-3 */
        case TS_PSI_ST_ATSC_EAC3:     /* ATSC Enhanced AC-3 */
        case TS_PSI_ST_ATSC_DTS:      /* ATSC DTS audio */
        case TS_PSI_ST_ATSC_DTS_HD:   /* ASTC DTS-UHD audio */
        case TS_PSI_ST_AVS_Audio:     /* AVS */
        case TS_PSI_ST_DRA_Audio:     /* DRA */
            ccx_dtvcc_log("Audio stream type 0x%02X", pmt_stream.stream_type);			
            ADD_AUDIO_PID(program_info, pmt_stream.elementary_PID, pmt_stream.stream_type, pmt, pmt_size, i);
            _dtvcc_pmt_proc_stream_descriptors_get_audio(pmt, pmt_size, i, 
						pmt_stream.stream_type, pmt_stream.elementary_PID, &audio_descriptor );

#ifdef DTVCC_TTA_CERT_MODE
           if(ctx->kec_mode == 1)
           {
             _dtvcc_insert_audio_descriptor(ctx, &audio_descriptor);
           }
           else
           {
             _dtvcc_set_audio_descriptor(ctx, &audio_descriptor);

           }
#else
           _dtvcc_set_audio_descriptor(ctx, &audio_descriptor);
#endif
            break;

        /* video or audio */
        case TS_PSI_ST_13818_1_PrivatePES:  /* examine descriptors to handle private data */
            for (;;) {
                uint8_t *desc = _dtvcc_pmt_get_stream_descriptor(pmt, pmt_size, i, desc_index);
                if (desc == NULL) break;
				ccx_dtvcc_log("desc[%d]: 0x%02x", desc_index, desc[0]);
                desc_index++;

                switch(desc[0]) {
                /* video formats */
                case TS_PSI_DT_VideoStream:
                case TS_PSI_DT_MPEG4_Video:
                case TS_PSI_DT_AVC:
                case TS_PSI_DT_AVS_Video:
                    ADD_VIDEO_PID(program_info, pmt_stream.elementary_PID, pmt_stream.stream_type, pmt, pmt_size, i);
                    break;

                /* audio formats */
                case TS_PSI_DT_AudioStream:
                case TS_PSI_DT_MPEG2_AAC:
                case TS_PSI_DT_MPEG4_Audio:
                case TS_PSI_DT_DVB_AAC:
                case TS_PSI_DT_DVB_AC3:
                case TS_PSI_DT_DVB_EnhancedAC3:
                case TS_PSI_DT_DVB_DTS:;
                case TS_PSI_DT_DVB_DRA:
                    ADD_AUDIO_PID(program_info, pmt_stream.elementary_PID, pmt_stream.stream_type, pmt, pmt_size, i);
                    break;
                default:
                    ccx_dtvcc_log("Unsupported private descriptor 0x%x",desc[0]);
                    break;
                }
            }
            break;
        /* User Private AIT */
        case TS_PSI_ST_UserPrivate: /* AIT */
            ccx_dtvcc_log("AIT: stream type (0x%02X)", pmt_stream.stream_type);			
            ccx_dtvcc_log("AIT: pid (0x%04X)", pmt_stream.elementary_PID);				
            ADD_OTHER_PID(program_info, pmt_stream.elementary_PID, pmt_stream.stream_type, pmt, pmt_size, i);
 			_dtvcc_pmt_proc_stream_descriptors_get_ait(pmt, pmt_size, i, 
            pmt_stream.stream_type, pmt_stream.elementary_PID, &ctx->ait_stream );
            break;

        /* ISO/IEC 13818-6 type B */
        case TS_PSI_ST_13818_6_TypeB: /* DSM-CC Object Carousel */
            ccx_dtvcc_log("DSM-CC Object Carousel: stream type (0x%02X)", pmt_stream.stream_type);			
            ccx_dtvcc_log("DSM-CC Object Carousel: pid (0x%04X)", pmt_stream.elementary_PID);				
            ADD_OTHER_PID(program_info, pmt_stream.elementary_PID, pmt_stream.stream_type, pmt, pmt_size, i);
            _dtvcc_pmt_proc_stream_descriptors_get_dsmcc(pmt, pmt_size, i, 
            pmt_stream.stream_type, pmt_stream.elementary_PID, &ctx->dsmcc_stream );

			ccx_dtvcc_dsmcc_stream *str = calloc(sizeof(char), sizeof(ccx_dtvcc_dsmcc_stream));
			if (str){
				str->pid = pmt_stream.elementary_PID;
				str->assoc_tag = ctx->dsmcc_stream.association_tag_descriptor.association_tag;
				_dtvcc_insert_dsmcc_stream(ctx, str);
			}
            break;

        default:
            if( program_info->num_other_pids < TSPSIMGR_MAX_PIDS )
            {
                ADD_OTHER_PID(program_info, pmt_stream.elementary_PID, pmt_stream.stream_type, pmt, pmt_size, i);
            }
            break;
        }

    }

}

uint8_t* _dtvcc_ait_p_get_descriptor(uint8_t *buf, uint32_t descriptors_length, int descriptor_num )
{
	uint8_t *descriptor;
	int i;
	uint32_t offset = 0;

	descriptor = NULL;

	for( i = 0; offset < descriptors_length; i++ )
	{
		descriptor = &buf[offset];
		offset += buf[offset+1] + 2;

		if( i == descriptor_num )
			break;
		else
			descriptor = NULL;
	}

	return descriptor;
}

int _dtvcc_ait_p_get_application_offset(uint8_t *buf, unsigned int size, int application_num )
{
	int offset;
	int i;

	/* After the last descriptor */
	offset = APPLICATION_BASE(buf);

	for (i=0; i < application_num; i++)
	{
		if (offset >= (int)size || offset >= TS_PSI_MAX_BYTE_OFFSET(buf))
			return -1;
		offset += 9 + (TS_READ_16( &buf[offset+7] ) & 0xFFF);
	}

	return offset;
}


int _dtvcc_ait_get_application_count( uint8_t *buf, unsigned int size)
{
	int offset;
	int i = 0;

	offset = APPLICATION_BASE(buf);

	while (offset < TS_PSI_MAX_BYTE_OFFSET(buf) && offset < (int)size)
	{
		offset += 9 + (TS_READ_16( &buf[offset+7] ) & 0xFFF);
		i++;
	}

	return i;
}

int _dtvcc_ait_get_application( uint8_t *buf, unsigned int size, int appication_num, ccx_dtvcc_appication *p_appication )
{
	int offset;

	offset = _dtvcc_ait_p_get_application_offset( buf, size, appication_num );
	if (offset == -1)
		return 1;

	p_appication->organisation_id = (uint32_t)TS_READ_32( &buf[offset] );
	p_appication->application_id = (uint16_t)TS_READ_16( &buf[offset+4] );
	p_appication->application_control_code = buf[offset+6];
	p_appication->application_descriptor_loop_length = (uint16_t)TS_READ_16( &buf[offset+4] )&0x0FFF;

	return 0;
}

uint8_t* _dtvcc_ait_get_application_descriptor( uint8_t *buf, unsigned int size, int application_num, int descriptor_num )
{
	int offset;
	uint32_t descriptors_length;

	offset = _dtvcc_ait_p_get_application_offset( buf, size, application_num );
	if (offset == -1)
		return NULL;

	descriptors_length = TS_READ_16(&buf[offset+7])&0xFFF;

	return (_dtvcc_ait_p_get_descriptor(&buf[offset+9], descriptors_length, descriptor_num ));
}


uint8_t* _dtvcc_ait_get_descriptor( uint8_t *buf, unsigned int size, int descriptor_num )
{
	uint8_t *descriptor_base = COMMON_DESCRIPTOR_BASE(buf);
	uint32_t descriptors_length = COMMON_DESCRIPTOR_LENGTH(buf);

	/* Any time we dereference memory based on the contents of live data,
	we should check. */
	if (descriptor_base - buf >= (int)size) {
		return NULL;
	}
	size -= (descriptor_base - buf);
	if (size < descriptors_length) {
		return NULL;
	}
	return _dtvcc_ait_p_get_descriptor(descriptor_base, descriptors_length, descriptor_num);
}


void _dtvcc_ait_get_information(ccx_dtvcc_ctx *ctx, void *ait, unsigned int ait_size)
{
    int i;
	int index = 0;
    int application_count;
    ccx_dtvcc_appication* ait_application;
    TS_PSI_header header;

    _dtvcc_get_section_header(ait, &header );
	_dtvcc_ait_data_dump( &header );

    application_count = _dtvcc_ait_get_application_count(ait, ait_size);
    ccx_dtvcc_log("application_count : %d", application_count);	
    for( i = 0; i < application_count; i++ )
    {
        ait_application = &(ctx->application_info.application);

        int desc_index = 0;
        if (_dtvcc_ait_get_application(ait, ait_size, i, ait_application )) {
            continue;
        }

        ait_application->appication_type = header.table_id_extension&0x7FFF;
        ccx_dtvcc_log("appication_type          : 0x%04X", ait_application->appication_type);
        ccx_dtvcc_log("organisation_id          : 0x%08X", ait_application->organisation_id);
        ccx_dtvcc_log("application_id           : 0x%04X", ait_application->application_id);
        ccx_dtvcc_log("application_control_code : 0x%02X", ait_application->application_control_code);		
        ccx_dtvcc_log("application_descriptor_loop_length : %d", ait_application->application_descriptor_loop_length);				

		for (;;) {
			uint8_t *desc = _dtvcc_ait_get_application_descriptor(ait, ait_size, i, desc_index);
			if (desc == NULL) break;
			ccx_dtvcc_log("desc[%d]: 0x%02x", desc_index, desc[0]);
			desc_index++;
		
			switch(desc[0]) {
			/* Applicaiton Descriptor */
			case TS_PSI_DT_AIT_Application:
			{
				ccx_dtvcc_log("AIT-DescriptorTag (0x%02X): Applicaiton Descriptor", 
						desc[0]);
				
				ccx_dtvcc_application_descriptor *descriptor;	
				descriptor = &(ctx->application_info.application_descriptor);

				int descriptor_length = desc[1];
				ccx_dtvcc_log("Descriptor_length: %d", descriptor_length);
				index = 2;
				int profile_length = desc[index];
				ccx_dtvcc_log("application_profile_length: %d", profile_length);				
				index += 1;
				int loop_size = profile_length;
				while(loop_size > 0) {
					descriptor->applicaiton_profile = TS_READ_16(&desc[index]);
					descriptor->major = desc[index+2];
					descriptor->minor = desc[index+3];
					descriptor->micro = desc[index+4];
					ccx_dtvcc_log("    application_profile: (0x%04X)", descriptor->applicaiton_profile);
					ccx_dtvcc_log("    version.major: (0x%02X)", descriptor->major);
					ccx_dtvcc_log("    version.minor: (0x%02X)", descriptor->minor);
					ccx_dtvcc_log("    version.micro: (0x%02X)", descriptor->micro);
					index += 5;
					loop_size -= 5;
				}
				
				descriptor->service_bound_flag = (uint8_t)((desc[index]>>7)&1);
				descriptor->visibility = (uint8_t)((desc[index]>>5)&0x03);
				index += 1;
				descriptor->application_priority = desc[index];
				index += 1;
				descriptor->transport_protocol_label = desc[index];
				
				ccx_dtvcc_log("service_bound_flag: (0x%02X)", 
						descriptor->service_bound_flag);
				ccx_dtvcc_log("visibility: (0x%02X)", 
						descriptor->visibility);
				ccx_dtvcc_log("application_priority: (0x%02X)", 
						descriptor->application_priority);
				ccx_dtvcc_log("transport_protocol_label: (0x%02X)", 
						descriptor->transport_protocol_label);
			}
			break;

			/* Application Name Descriptor */
			case TS_PSI_DT_AIT_ApplicationName:
			{
				ccx_dtvcc_log("AIT-DescriptorTag (0x%02X): Applicaiton Name Descriptor", 
						desc[0]);

				int descriptor_length = desc[1];
				ccx_dtvcc_log("Descriptor_length: %d", descriptor_length);
				
				ccx_dtvcc_application_name_descriptor *descriptor;	
				descriptor = &(ctx->application_info.name_descriptor);

				int application_name_length;
				index = 2;				
				while(descriptor_length > 0) {
					
					descriptor->language[0] = desc[index];
					descriptor->language[1] = desc[index+1];
					descriptor->language[2] = desc[index+2];
					descriptor->language[3] = 0x00;

					index += 3;
					
					ccx_dtvcc_log("    ISO639_language_code: (%c%c%c)", 
						descriptor->language[0], descriptor->language[1], descriptor->language[2]);

					application_name_length = desc[index];
					index += 1;

					ccx_dtvcc_log("    application_name_length: %d", application_name_length);
					
					if(application_name_length > 0){
						memset(descriptor->application_name, 0x00, 64);
						memcpy(descriptor->application_name, &desc[index], application_name_length);
						ccx_dtvcc_log("    application_name: \"%s\"", descriptor->application_name);
					}
					
					index += application_name_length;
					descriptor_length -= (application_name_length + 4);
				}
			}				
			break;

			/* Transport Protocol Descriptor */
			case TS_PSI_DT_AIT_TransportProtocol:
			{
				ccx_dtvcc_log("AIT-DescriptorTag (0x%02X): Transport Protocol Descriptor", 
						desc[0]);

				int descriptor_length = desc[1];
				ccx_dtvcc_log("Descriptor_length: %d", descriptor_length);

				ccx_dtvcc_application_tranport_descriptor *descriptor;	
				descriptor = &(ctx->application_info.transport_descriptor);
				index = 2;
				
				descriptor->protocol_id = TS_READ_16(&desc[index]);
				index += 2;
				
				descriptor->transport_protocol_label = desc[index];
				index += 1;
				
				ccx_dtvcc_log("protocol_id: (0x%04X)", 
						descriptor->protocol_id);
				ccx_dtvcc_log("transport_protocol_label: (0x%02X)", 
						descriptor->transport_protocol_label);

				if(descriptor->protocol_id == 0x0001){
					descriptor->remote_connection = (uint8_t)((desc[index]>>7)&1);
					index += 1;
					
					if(descriptor->remote_connection == 1){
						/* skip original_network_id + transport_stream_id + service_id */
						index += 6;
						descriptor->component_tag = desc[index];						
					}else{
						descriptor->component_tag = desc[index];

					}
					ccx_dtvcc_log("remote_connection: (0x%02X)", 
							descriptor->remote_connection);
					ccx_dtvcc_log("component_tag: (0x%02X)", 
							descriptor->component_tag);
				}
			}
			break;
			
			/* DVB-J Application Descriptor */
			case TS_PSI_DT_AIT_DVBJApplication:
			{
				ccx_dtvcc_log("AIT-DescriptorTag (0x%02X): DVB-J Application Descriptor", 
						desc[0]);
				int parameter_length;
				char parameter[64];
				int descriptor_length = desc[1];
				ccx_dtvcc_log("Descriptor_length: %d", descriptor_length);
				index = 2;
				if (descriptor_length > 0) {
					parameter_length = desc[index];
					index += 1;

					ccx_dtvcc_log("    parameter_length: %d", parameter_length);
					
					if(parameter_length > 0){
						memset(parameter, 0x00, 64);
						memcpy(parameter, &desc[index], parameter_length);
						ccx_dtvcc_log("    parameter: \"%s\"", parameter);
					}
					
					index += parameter_length;
					descriptor_length -= (parameter_length + 1);
				}				
			}		
			break;

			/* DVB-J Application Location Descriptor */
			case TS_PSI_DT_AIT_DVBJApplicationLocation:
			{
				ccx_dtvcc_log("AIT-DescriptorTag (0x%02X): DVB-J Application Location Descriptor", 
						desc[0]);
				int descriptor_length = desc[1];
				ccx_dtvcc_log("Descriptor_length: %d", descriptor_length);
				index = 2;

				ccx_dtvcc_dvbj_application_location_descriptor *descriptor;	
				descriptor = &(ctx->application_info.dvbj_location_descriptor);

				int base_directory_length = desc[index];
				index += 1;
				ccx_dtvcc_log("base_directory_length: %d", base_directory_length);

				if(base_directory_length > 0){
					memset(descriptor->base_directory, 0x00, 64);					
					memcpy(descriptor->base_directory, &desc[index], base_directory_length);
					ccx_dtvcc_log("base_directory: \"%s\"", descriptor->base_directory);
					index += base_directory_length;					
				}else{
					ccx_dtvcc_log("base_directory: \"\"");
				}

				int classpath_extension_length = desc[index];
				index += 1;
				ccx_dtvcc_log("classpath_extension_length: %d", classpath_extension_length);
			
				if(classpath_extension_length > 0){
					memcpy(descriptor->classpath_extension, &desc[index], classpath_extension_length);
					ccx_dtvcc_log("classpath_extension: \"%s\"", descriptor->classpath_extension);
					index += classpath_extension_length; 				
				}else{
					ccx_dtvcc_log("classpath_extension: \"\"");
				}

				int initial_class_length = descriptor_length - (base_directory_length + classpath_extension_length + 2);
				if (initial_class_length > 0) {
					memcpy(descriptor->initial_class, &desc[index], initial_class_length);
					ccx_dtvcc_log("initial_class: \"%s\"", descriptor->initial_class);
				}else{
					ccx_dtvcc_log("initial_class: \"\"");
				}
			}		
			break;

			/* Application Storage Descriptor */
			case TS_PSI_DT_AIT_ApplicationStorage:
			{
				ccx_dtvcc_log("AIT-DescriptorTag (0x%02X): Application Storage Descriptor", 
						desc[0]);

				int descriptor_length = desc[1];
				ccx_dtvcc_log("Descriptor_length: %d", descriptor_length);

				ccx_dtvcc_application_storage_descriptor *descriptor;	
				descriptor = &(ctx->application_info.storage_descriptor);
				index = 2;
				
				descriptor->storage_property = desc[index];
				index += 1;
				ccx_dtvcc_log("storage_property: (0x%02X)", descriptor->storage_property);
				
				descriptor->not_launchable_from_broadcast = (desc[index]>>7)&1;
				descriptor->launchable_completely_from_cache = (desc[index]>>6)&1;
				descriptor->is_launchable_with_older_version = (desc[index]>>5)&1;				
				index += 1;

				ccx_dtvcc_log("not_launchable_from_broadcast: %d", descriptor->not_launchable_from_broadcast);
				ccx_dtvcc_log("launchable_completely_from_cache: %d", descriptor->launchable_completely_from_cache);
				ccx_dtvcc_log("is_launchable_with_older_version: %d", descriptor->is_launchable_with_older_version);						

				descriptor->version = TS_READ_32(&desc[index])&0x7FFFFFFFL;
				index += 4;
				ccx_dtvcc_log("version: (0x%08X)", descriptor->version);
				
				descriptor->priority = desc[index];
				ccx_dtvcc_log("priority: (0x%02X)", descriptor->priority);
			}				
			break;

			/* Simple Application Location Descriptor */
			case TS_PSI_DT_AIT_ApplicationLocation:
			{
				ccx_dtvcc_log("AIT-DescriptorTag (0x%02X): Simple Application Location Descriptor", 
						desc[0]);

				int descriptor_length = desc[1];
				ccx_dtvcc_log("Descriptor_length: %d", descriptor_length);

				ccx_dtvcc_application_location_descriptor *descriptor;	
				descriptor = &(ctx->application_info.location_descriptor);

				if(descriptor_length > 0){
					memcpy(descriptor->initial_path_bytes, &desc[2], descriptor_length);
					ccx_dtvcc_log("initial path: \"%s\"", 
							descriptor->initial_path_bytes);
				}
			}		
			break;
		
			default:
				ccx_dtvcc_log("Unsupported appication information descriptor:(0x%02x), length:%d",desc[0], desc[1]);
			break;
			}
		}
    }
}

//---------------------------------- EVENT QUEUE------------------------------------

void dtvcc_lock_event_list(ccx_event_head_t *l)
{
    pthread_mutex_lock(&cc_lock);//pthread_mutex_lock(&l->lock);
}

void dtvcc_unlock_event_list(ccx_event_head_t *l)
{
    pthread_mutex_unlock(&cc_lock);//pthread_mutex_unlock(&l->lock);
    pthread_cond_signal(&cc_cond);	//pthread_cond_signal(&l->cond);
}

ccx_event_list_t* dtvcc_create_event_entry(int event, int param, void *data)
{
    ccx_event_list_t *c = calloc(sizeof(char), sizeof(ccx_event_list_t));
    if (!c)
        return 0;

    c->event = event;
    c->param = param;    
    c->data = data;
    c->next = 0;
    return c;
}

void dtvcc_destroy_event_entry(ccx_event_list_t* c)
{
    if(c!=0){
        free(c);
    }
}

ccx_event_list_t *dtvcc_dequeue_event(ccx_event_head_t *list)
{
    ccx_event_list_t *eptr;

    dtvcc_lock_event_list(list);
    eptr = list->head;
    if (eptr) {
        list->head = eptr->next;
        if (!list->head) {
            assert(list->last == eptr);
            list->last = 0;
        }
    }
    dtvcc_unlock_event_list(list);
    return eptr;
}

void dtvcc_queue_event(ccx_event_head_t *list, ccx_event_list_t *c, int add_to_front)
{
    c->next = 0;
    dtvcc_lock_event_list(list);
    /* appending a new entry to the back of the list */
    if (list->last) {
        assert(list->head);
        // List is not empty
        if (!add_to_front) {
            list->last->next = c;
            list->last = c;
        } else {
            c->next = list->head;
            list->head = c;
        }
    } else {
        // List is empty
        assert(!list->head);
        list->head = c;
        list->last = c;
    }
    dtvcc_unlock_event_list(list);
}

int dtvcc_queue_event_data(ccx_event_head_t *list, int event, int param, void *data)
{
    ccx_event_list_t *b  = dtvcc_create_event_entry(event, param, data);
    if (!b)
        return -1;
    dtvcc_queue_event(list, b, 0);
    return 0;
}

int dtvcc_queue_front_event_data(ccx_event_head_t *list, int event, int param, void *data)
{
    ccx_event_list_t *b  = dtvcc_create_event_entry(event, param, data);
    if (!b)
        return -1;
    dtvcc_queue_event(list, b, 1);
    return 0;
}


int dtvcc_remove_event(ccx_event_head_t *list)
{
    ccx_event_list_t *c = dtvcc_dequeue_event(list);
    if (!c) {
        return 0;
    }
    dtvcc_destroy_event_entry(c);
    return 1;
}

void dtvcc_free_events(ccx_event_head_t *list)
{
    while (dtvcc_remove_event(list))
        ;
}


//---------------------------------- HELPERS ------------------------------------

void ccx_dtvcc_callback_event(ccx_dtvcc_ctx *ctx, int cw, int event, void *data)
{
	if (!ctx->is_active)
		return;

	int cw_event = ((cw & 0xFF) << 8) + (event & 0xFF);
	ctx->cc_callback(ctx->handle, CCX_WHAT_EVENT_CC, 0, cw_event, data);
}

void ccx_dtvcc_pid_callback_event(ccx_dtvcc_ctx *ctx, int event, void *data)
{
	ctx->cc_callback(ctx->handle, CCX_WHAT_EVENT_PID_CHAGNED, 0, event, data);
}

void ccx_dtvcc_post_picture_data(ccx_dtvcc_ctx *ctx, void *data, int size)
{
	if(ctx && !ctx->stop_requested){
		unsigned char* buffer = (unsigned char*)calloc(sizeof(unsigned char), size);
		memcpy((void *)buffer, (void *)data, size);
		dtvcc_queue_event_data(&ctx->dtvcc_event, CCX_DTVCC_EVENT_PICTURE_DATA, size, buffer);
	}
}

void ccx_dtvcc_post_section_filter_data(ccx_dtvcc_ctx *ctx, void *data, int size, void *user_param)
{
	if(ctx && !ctx->stop_requested){
		ccx_section_data* section = (ccx_section_data*) calloc(sizeof(unsigned char), sizeof(ccx_section_data));
		section->section_data = (void *)calloc(sizeof(unsigned char), size);
		memcpy((void*)section->section_data, (void *)data, size);
		section->section_data_size = size;
		section->section_user_param = user_param;
		dtvcc_queue_event_data(&ctx->dtvcc_event, CCX_DTVCC_EVENT_SECTION_FILTER_DATA, 0, section);
	}
}

void ccx_dtvcc_post_dsmcc_section_filter_data(ccx_dtvcc_ctx *ctx, void *data, int size, void *user_param)
{
	if(ctx && !ctx->stop_requested){
		ccx_dsmcc_section_data* section = (ccx_dsmcc_section_data*) calloc(sizeof(unsigned char), sizeof(ccx_dsmcc_section_data));
		section->section_data = (void *)calloc(sizeof(unsigned char), size);
		memcpy((void*)section->section_data, (void *)data, size);
		section->section_data_size = size;
		section->section_user_param = user_param;
		dtvcc_queue_event_data(&ctx->dtvcc_event, CCX_DTVCC_EVENT_DSMCC_SECTION_FILTER_DATA, 0, section);
	}
}

void ccx_dtvcc_clear_packet(ccx_dtvcc_ctx *ctx)
{
	ctx->start_packet_recved = 0;
	ctx->current_packet_length = 0;
	memset(ctx->current_packet, 0, CCX_DTVCC_MAX_PACKET_LENGTH * sizeof(unsigned char));
}

void ccx_dtvcc_clear_serivce_descriptor(ccx_dtvcc_ctx *ctx)
{
	ctx->cc_descriptor = ccx_dtvcc_default_caption_service_descrpitor;
}

void _dtvcc_color_to_hex(int color, unsigned *hR, unsigned *hG, unsigned *hB)
{
	*hR = (unsigned) (color >> 4);
	*hG = (unsigned) ((color >> 2) & 0x3);
	*hB = (unsigned) (color & 0x3);
//	ccx_dtvcc_log("[CEA-708] [CEA-708] Color: %d [%06x] %u %u %u\n", color, color, *hR, *hG, *hB);
}

void _dtvcc_change_pen_colors(ccx_dtvcc_ctx *dtvcc, dtvcc_tv_screen *tv, ccx_dtvcc_pen_color pen_color, int row_index, int column_index, size_t *buf_len, int open)
{
	char *buf = (char *)dtvcc->buffer;

	ccx_dtvcc_pen_color new_pen_color;
	if (column_index >= CCX_DTVCC_SCREENGRID_COLUMNS)
		new_pen_color = ccx_dtvcc_default_pen_color;
	else
		new_pen_color = tv->pen_colors[row_index][column_index];
	if ((pen_color.fg_color != new_pen_color.fg_color) || (pen_color.fg_opacity != new_pen_color.fg_opacity))
	{
		if (!open)			
		{
			(*buf_len) += sprintf(buf + (*buf_len), "</font>");	// should close older non-white color
//			ccx_dtvcc_log("[CEA-708] _dtvcc_write_screen_row index: %d, </font>\n", column_index);				
		}

		if (open)			
		{
			unsigned alpha, red, green, blue;
			_dtvcc_color_to_hex(new_pen_color.fg_color, &red, &green, &blue);
			switch (new_pen_color.fg_opacity)
			{
			case CCX_DTVCC_WINDOW_FO_SOLID:
			case CCX_DTVCC_WINDOW_FO_FLASH:				
				alpha = 255;
				break;
			case CCX_DTVCC_WINDOW_FO_TRANSLUCENT:
				alpha = 128;
				break;
			case CCX_DTVCC_WINDOW_FO_TRANSPARENT:
				alpha = 0;
				break;
			}
			red = (255 / 3) * red;
			green = (255 / 3) * green;
			blue = (255 / 3) * blue;
			(*buf_len) += sprintf(buf + (*buf_len), "<font color=\"%02x%02x%02x%02x\">", alpha, red, green, blue);
//			ccx_dtvcc_log("[CEA-708] _dtvcc_write_screen_row index: %d, <font color=\"%02x%02x%02x%02x\">\n", column_index, alpha, red, green, blue);
		}
	}
}

void _dtvcc_change_pen_bg_colors(ccx_dtvcc_ctx *dtvcc, dtvcc_tv_screen *tv, ccx_dtvcc_pen_color pen_color, int row_index, int column_index, size_t *buf_len, int open)
{
	char *buf = (char *)dtvcc->buffer;

	ccx_dtvcc_pen_color new_pen_color;
	if (column_index >= CCX_DTVCC_SCREENGRID_COLUMNS)
		new_pen_color = ccx_dtvcc_default_pen_color;
	else
		new_pen_color = tv->pen_colors[row_index][column_index];
	if ((pen_color.bg_color != new_pen_color.bg_color) || (pen_color.bg_opacity != new_pen_color.bg_opacity))
	{
		if (!open)			
		{
			(*buf_len) += sprintf(buf + (*buf_len), "</bg>");	// should close older non-white color
//			ccx_dtvcc_log("[CEA-708] _dtvcc_write_screen_row index: %d, </font>\n", column_index);				
		}

		if (open)			
		{
			unsigned alpha, red, green, blue;
			_dtvcc_color_to_hex(new_pen_color.bg_color, &red, &green, &blue);
			switch (new_pen_color.bg_opacity)
			{
			case CCX_DTVCC_WINDOW_FO_SOLID:
			case CCX_DTVCC_WINDOW_FO_FLASH:				
				alpha = 255;
				break;
			case CCX_DTVCC_WINDOW_FO_TRANSLUCENT:
				alpha = 128;
				break;
			case CCX_DTVCC_WINDOW_FO_TRANSPARENT:
				alpha = 0;
				break;
			}
			red = (255 / 3) * red;
			green = (255 / 3) * green;
			blue = (255 / 3) * blue;
			(*buf_len) += sprintf(buf + (*buf_len), "<bg color=\"%02x%02x%02x%02x\">", alpha, red, green, blue);
//			ccx_dtvcc_log("[CEA-708] _dtvcc_write_screen_row index: %d, <back color=\"%02x%02x%02x%02x\">\n", column_index, alpha, red, green, blue);
		}
	}
}


void _dtvcc_change_pen_attribs(ccx_dtvcc_ctx *dtvcc, dtvcc_tv_screen *tv, ccx_dtvcc_pen_attribs pen_attribs, int row_index, int column_index, size_t *buf_len, int open)
{
	char *buf = (char *)dtvcc->buffer;

	ccx_dtvcc_pen_attribs new_pen_attribs;
	if (column_index >= CCX_DTVCC_SCREENGRID_COLUMNS)
		new_pen_attribs = ccx_dtvcc_default_pen_attribs;
	else
		new_pen_attribs = tv->pen_attribs[row_index][column_index];
	if (pen_attribs.italic != new_pen_attribs.italic)
	{
		if (pen_attribs.italic && !open)
			(*buf_len) += sprintf(buf + (*buf_len), "</i>");
		if (!pen_attribs.italic && open)
			(*buf_len) += sprintf(buf + (*buf_len), "<i>");
	}
	if (pen_attribs.underline != new_pen_attribs.underline)
	{
		if (pen_attribs.underline && !open)
			(*buf_len) += sprintf(buf + (*buf_len), "</u>");
		if (!pen_attribs.underline && open)
			(*buf_len) += sprintf(buf + (*buf_len), "<u>");
	}
}


void _dtvcc_tv_clear(ccx_dtvcc_service_decoder *decoder)
{
	for (int i = 0; i < CCX_DTVCC_SCREENGRID_ROWS; i++)
		memset(decoder->tv->chars[i], 0, CCX_DTVCC_SCREENGRID_COLUMNS * sizeof(ccx_dtvcc_symbol));
};

int _dtvcc_decoder_has_visible_windows(ccx_dtvcc_service_decoder *decoder)
{
	for (int i = 0; i < CCX_DTVCC_MAX_WINDOWS; i++)
	{
		if (decoder->windows[i]->visible)
			return 1;
	}
	return 0;
}

void _dtvcc_window_clear_row(ccx_dtvcc_window *window, int row_index)
{
	if (window->memory_reserved)
	{
		memset(window->rows[row_index], 0, CCX_DTVCC_MAX_COLUMNS * sizeof(ccx_dtvcc_symbol));
		for (int column_index = 0; column_index < CCX_DTVCC_MAX_COLUMNS; column_index++)
		{
			window->pen_attribs[row_index][column_index] = ccx_dtvcc_default_pen_attribs;
			window->pen_colors[row_index][column_index] = ccx_dtvcc_default_pen_color;
		}
	}
}

void _dtvcc_window_clear_text(ccx_dtvcc_window *window)
{
	window->pen_color_pattern = ccx_dtvcc_default_pen_color;
	window->pen_attribs_pattern = ccx_dtvcc_default_pen_attribs;
	for (int i = 0; i < CCX_DTVCC_MAX_ROWS; i++)
		_dtvcc_window_clear_row(window, i);
	window->is_empty = 1;
}

void _dtvcc_window_clear(ccx_dtvcc_service_decoder *decoder, int window_id)
{
	_dtvcc_window_clear_text(decoder->windows[window_id]);
}

void _dtvcc_window_apply_style(ccx_dtvcc_window *window, ccx_dtvcc_window_attribs *style)
{
	window->attribs.border_color = style->border_color;
	window->attribs.border_type = style->border_type;
	window->attribs.display_effect = style->display_effect;
	window->attribs.effect_direction = style->effect_direction;
	window->attribs.effect_speed = style->effect_speed;
	window->attribs.fill_color = style->fill_color;
	window->attribs.fill_opacity = style->fill_opacity;
	window->attribs.justify = style->justify;
	window->attribs.print_direction = style->print_direction;
	window->attribs.scroll_direction = style->scroll_direction;
	window->attribs.word_wrap = style->word_wrap;
}

int _dtvcc_is_screen_row_empty(dtvcc_tv_screen *tv, int row_index)
{
	for (int j = 0; j < CCX_DTVCC_SCREENGRID_COLUMNS; j++)
	{
		if (CCX_DTVCC_SYM_IS_SET(tv->chars[row_index][j]))
			return 0;
	}
	return 1;
}

int _dtvcc_is_screen_empty(dtvcc_tv_screen *tv)
{
	for (int i = 0; i < CCX_DTVCC_SCREENGRID_ROWS; i++)
	{
		if (!_dtvcc_is_screen_row_empty(tv, i))
		{
			return 0;
		}
	}
	return 1;
}

void _dtvcc_get_screen_write_interval(dtvcc_tv_screen *tv, int row_index, int *first, int *last)
{
	for (*first = 0; *first < CCX_DTVCC_SCREENGRID_COLUMNS; (*first)++)
		if (CCX_DTVCC_SYM_IS_SET(tv->chars[row_index][*first]))
			break;
	for (*last = CCX_DTVCC_SCREENGRID_COLUMNS - 1; *last > 0; (*last)--)
		if (CCX_DTVCC_SYM_IS_SET(tv->chars[row_index][*last]))
			break;
}

void _dtvcc_write_screen_row(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_service_decoder *decoder, int row_index, int use_colors)
{
	dtvcc_tv_screen *tv = decoder->tv;
	char *cc_data = (char *) dtvcc->cc_data;
	if (!cc_data)
	{
		ccx_dtvcc_log("[CEA-708] _dtvcc_write_screen_row: cc_data not allocated skipped\n");
		return;
	}
	
	char *buf = (char *)dtvcc->buffer;
	if (!buf)
	{
		ccx_dtvcc_log("[CEA-708] _dtvcc_write_screen_row: buffer not allocated skipped\n");
		return;
	}
	
	size_t buf_len = 0;
	int first, last;
	int is_latin = 0;

	ccx_dtvcc_pen_color pen_color = ccx_dtvcc_default_pen_color;
	ccx_dtvcc_pen_attribs pen_attribs = ccx_dtvcc_default_pen_attribs;
	_dtvcc_get_screen_write_interval(tv, row_index, &first, &last);

	// set font color tag the first symbol in a row
	if (use_colors) _dtvcc_change_pen_bg_colors(dtvcc, tv, pen_color, row_index, first, &buf_len, 1);
	if (use_colors) _dtvcc_change_pen_colors(dtvcc, tv, pen_color, row_index, first, &buf_len, 1);
	_dtvcc_change_pen_attribs(dtvcc, tv, pen_attribs, row_index, first, &buf_len, 1);
	pen_color = tv->pen_colors[row_index][first];
	pen_attribs = tv->pen_attribs[row_index][first];

	for (int i = first; i <= last; i++)
	{
		_dtvcc_change_pen_attribs(dtvcc, tv, pen_attribs, row_index, i, &buf_len, 0);
		if (use_colors)	_dtvcc_change_pen_colors(dtvcc, tv, pen_color, row_index, i, &buf_len, 0);
		if (use_colors)	_dtvcc_change_pen_bg_colors(dtvcc, tv, pen_color, row_index, i, &buf_len, 0);
		if (use_colors)	_dtvcc_change_pen_bg_colors(dtvcc, tv, pen_color, row_index, i, &buf_len, 1);
		if (use_colors) _dtvcc_change_pen_colors(dtvcc, tv, pen_color, row_index, i, &buf_len, 1);
		_dtvcc_change_pen_attribs(dtvcc, tv, pen_attribs, row_index, i, &buf_len, 1);
		pen_color = tv->pen_colors[row_index][i];
		pen_attribs = tv->pen_attribs[row_index][i];

		int size = 0;

		if(dtvcc->korean_code){
			size = ucs2_to_utf8(tv->chars[row_index][i].sym, buf + buf_len);
		}else{
#ifdef DTVCC_TTA_CERT_MODE
            if(dtvcc->kec_mode == 1)
            {
    			if(is_latin_character(tv->chars[row_index][i].sym)){ 
    				size = ucs2_to_utf8(tv->chars[row_index][i].sym, buf + buf_len);
    				is_latin = 1;				
    			}else{
    				size = write_utf16_char(tv->chars[row_index][i].sym, buf + buf_len);
    			}
			}
			else
			{
			    size = write_utf16_char(tv->chars[row_index][i].sym, buf + buf_len);
			}
#else			
			size = write_utf16_char(tv->chars[row_index][i].sym, buf + buf_len);
#endif
		}
		buf_len += size;
	}

	// there can be unclosed tags or colors after the last symbol in a row
	_dtvcc_change_pen_attribs(dtvcc, tv, pen_attribs, row_index, CCX_DTVCC_SCREENGRID_COLUMNS, &buf_len, 0);
	if (use_colors) _dtvcc_change_pen_colors(dtvcc, tv, pen_color, row_index, CCX_DTVCC_SCREENGRID_COLUMNS, &buf_len, 0);
	if (use_colors) _dtvcc_change_pen_bg_colors(dtvcc, tv, pen_color, row_index, CCX_DTVCC_SCREENGRID_COLUMNS, &buf_len, 0);

	buf[buf_len]='\0';

	if(dtvcc->korean_code || is_latin){ // Unicode 
#ifdef DTVCC_CC_PRINT_DEBUG
		char logging[3072] = {0,};
		sprintf(logging, "[CAPTION]:");
		sprintf(logging+strlen(logging), "%s", buf);		
		ccx_dtvcc_log(logging);
#endif
		dtvcc->cc_data_size += sprintf(cc_data + dtvcc->cc_data_size, "%s", buf);
	}else{ // EUC-KR
		if (dtvcc->cd != (iconv_t) -1)
		{
			int encoded_buf_size = INITIAL_ENC_BUFFER_CAPACITY * sizeof(char);
			char *encoded_buf = (char *)dtvcc->encoded_buf;
			if (!encoded_buf){
				ccx_dtvcc_log("[CEA-708] _dtvcc_write_screen_row enough memory error alloc encoded_buf\n");
				return;
			}

			char *encoded_buf_start = encoded_buf;

			size_t in_bytes_left = buf_len;
			size_t out_bytes_left = encoded_buf_size;

			size_t result = iconv(dtvcc->cd, &buf, &in_bytes_left, &encoded_buf, &out_bytes_left);

			if (result == (size_t)-1){
				ccx_dtvcc_log("[CEA-708] _dtvcc_write_screen_row: " 			
						"conversion failed: %s\n", strerror(errno));
				return;
			}

			out_bytes_left = encoded_buf_size - out_bytes_left;
			encoded_buf_start[out_bytes_left]='\0';

#ifdef DTVCC_CC_PRINT_DEBUG
			char logging[3072] = {0,};
			sprintf(logging, "[CAPTION]:");
			sprintf(logging+strlen(logging), "%s", encoded_buf_start);		
			ccx_dtvcc_log(logging);
#endif
			dtvcc->cc_data_size += sprintf(cc_data + dtvcc->cc_data_size, "%s", encoded_buf_start);
		}
		else
		{
			ccx_dtvcc_log("[CEA-708] _dtvcc_write_screen_row: iconv has to be defined first\n");
		}
	}
}


void ccx_dtvcc_write_screen_caption(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_service_decoder *decoder)
{
	dtvcc_tv_screen *tv = decoder->tv;
	if (_dtvcc_is_screen_empty(tv))
		return;
	
	char *buf = (char *) dtvcc->cc_data;
	if (!buf)
	{
		ccx_dtvcc_log("[CEA-708] cc_data not allocated skipped\n");
		return;
	}
	
	dtvcc->cc_data_size = 0;
	for (int i = 0; i < CCX_DTVCC_SCREENGRID_ROWS; i++)
	{
		if (!_dtvcc_is_screen_row_empty(tv, i))
		{
			_dtvcc_write_screen_row(dtvcc, decoder, i, 1);
			dtvcc->cc_data_size += sprintf(buf + dtvcc->cc_data_size, "<br>" );			
		}
	}
	buf[dtvcc->cc_data_size]='\0';
	ccx_dtvcc_callback_event(dtvcc, decoder->current_window, CCX_DTVCC_TV_EVENT_CAPTION_DATA, buf);
}


#ifdef DTVCC_PRINT_DEBUG

int _dtvcc_is_win_row_empty(ccx_dtvcc_window *window, int row_index)
{
	for (int j = 0; j < CCX_DTVCC_MAX_COLUMNS; j++)
	{
		if (CCX_DTVCC_SYM_IS_SET(window->rows[row_index][j]))
			return 0;
	}
	return 1;
}

void _dtvcc_get_win_write_interval(ccx_dtvcc_window *window, int row_index, int *first, int *last)
{
	for (*first = 0; *first < CCX_DTVCC_MAX_COLUMNS; (*first)++)
		if (CCX_DTVCC_SYM_IS_SET(window->rows[row_index][*first]))
			break;
	for (*last = CCX_DTVCC_MAX_COLUMNS - 1; *last > 0; (*last)--)
		if (CCX_DTVCC_SYM_IS_SET(window->rows[row_index][*last]))
			break;
}

void _dtvcc_window_dump(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_window *window)
{
	char logging[1024] = {0,};
	sprintf(logging, "[CEA-708] Window %d dump:\n", window->number);

	if (!window->is_defined)
		return;

	char sym_buf[10];

	for (int i = 0; i < CCX_DTVCC_MAX_ROWS; i++)
	{
		if (!_dtvcc_is_win_row_empty(window, i))
		{
			int first, last;
			ccx_dtvcc_symbol sym;
			_dtvcc_get_win_write_interval(window, i, &first, &last);
			for (int j = first; j <= last; j++)
			{
				sym = window->rows[i][j];
				int len = 0;
				if(dtvcc->korean_code){
					len = ucs2_to_utf8(sym.sym, sym_buf);
				}else{
					len = write_utf16_char(sym.sym, sym_buf);
				}
				for (int index = 0; index < len; index++)
					sprintf(logging+strlen(logging), "%c", sym_buf[index]);
			}
			sprintf(logging+strlen(logging), "\n");
		}
	}
	ccx_dtvcc_log(logging);
	ccx_dtvcc_log("[CEA-708] Window %d Dump done\n", window->number);
}

void ccx_dtvcc_write_screen_debug(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_service_decoder *decoder)
{
	dtvcc_tv_screen *tv = decoder->tv;

	char logging[1024] = {0,};
	sprintf(logging, "[CEA-708] ccx_dtvcc_write_screen_debug dump:\n");
	char sym_buf[10];

	for (int i = 0; i < CCX_DTVCC_SCREENGRID_ROWS; i++)
	{
		if (!_dtvcc_is_screen_row_empty(tv, i))
		{
			int first, last;
			ccx_dtvcc_symbol sym;
			_dtvcc_get_screen_write_interval(tv, i, &first, &last);
			for (int j = first; j <= last; j++)
			{
				sym = tv->chars[i][j];
				int len = 0;
				if(dtvcc->korean_code){
					len = ucs2_to_utf8(sym.sym, sym_buf);
				}else{
					len = write_utf16_char(sym.sym, sym_buf);
				}
				for (int index = 0; index < len; index++)
					sprintf(logging+strlen(logging), "%c", sym_buf[index]);
			}
			sprintf(logging+strlen(logging), "\n");			
		}
	}
}

void _dtvcc_window_data_dump(ccx_dtvcc_window *window)
{
	char logging[2048] = {0,};
	sprintf(logging, "[CEA-708] Window[%d] dump:\n", window->number);	
	sprintf(logging+strlen(logging), "[CEA-708] window : %p \n", window);	
	sprintf(logging+strlen(logging), "[CEA-708] window->number: %d \n", window->number);
	sprintf(logging+strlen(logging), "[CEA-708] window->is_defined: %d \n", window->is_defined);
	sprintf(logging+strlen(logging), "[CEA-708] window->is_empty: %d \n", window->is_empty);
	sprintf(logging+strlen(logging), "[CEA-708] window->commands: [0x%02X:0x%02X:0x%02X:0x%02X:0x%02X:0x%02X] \n",
		window->commands[0], window->commands[1], window->commands[2], window->commands[3], window->commands[4], window->commands[5]);	
	
	sprintf(logging+strlen(logging), "[CEA-708] window->memory_reserved: %d \n", window->memory_reserved);

	ccx_dtvcc_log(logging);
	ccx_dtvcc_log("[CEA-708] Window[%d] Dump done\n", window->number);
}

#endif

void ccx_dtvcc_windows_reset(ccx_dtvcc_service_decoder *decoder)
{
	if(!decoder) return; 
	
	for (int j = 0; j < CCX_DTVCC_MAX_WINDOWS; j++)
	{
		ccx_dtvcc_window *window = decoder->windows[j];
		if(window){
			_dtvcc_window_clear_text(window);
			window->is_defined = 0;
			window->visible = 0;
			memset(window->commands, 0, sizeof(window->commands));
		}
	}
	decoder->current_window = -1;
	_dtvcc_tv_clear(decoder);
}

void _dtvcc_decoders_reset(ccx_dtvcc_ctx *dtvcc)
{
	ccx_dtvcc_log("[CEA-708] _dtvcc_decoders_reset: Resetting all decoders\n");

	for (int i = 0; i < CCX_DTVCC_MAX_SERVICES; i++)
	{
		ccx_dtvcc_windows_reset(dtvcc->decoders[i]);
	}

	ccx_dtvcc_clear_packet(dtvcc);

	dtvcc->last_sequence = CCX_DTVCC_NO_LAST_SEQUENCE;
}

int _dtvcc_compare_win_priorities(const void *a, const void *b)
{
	ccx_dtvcc_window *w1 = *(ccx_dtvcc_window **)a;
	ccx_dtvcc_window *w2 = *(ccx_dtvcc_window **)b;
	return (w1->priority - w2->priority);
}


void _dtvcc_window_copy_to_screen(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_service_decoder *decoder, ccx_dtvcc_window *window)
{
	ccx_dtvcc_log("[CEA-708] _dtvcc_window_copy_to_screen: W-%d\n", window->number);
	int top, left;
	// For each window we calculate the top, left position depending on the
	// anchor
	switch (window->anchor_point)
	{
		case CCX_DTVCC_ANCHOR_POINT_TOP_LEFT:
			top = window->anchor_vertical;
			left = window->anchor_horizontal;
			break;
		case CCX_DTVCC_ANCHOR_POINT_TOP_CENTER:
			top = window->anchor_vertical;
			left = window->anchor_horizontal - window->col_count / 2;
			break;
		case CCX_DTVCC_ANCHOR_POINT_TOP_RIGHT:
			top = window->anchor_vertical;
			left = window->anchor_horizontal - window->col_count;
			break;
		case CCX_DTVCC_ANCHOR_POINT_MIDDLE_LEFT:
			top = window->anchor_vertical - window->row_count / 2;
			left = window->anchor_horizontal;
			break;
		case CCX_DTVCC_ANCHOR_POINT_MIDDLE_CENTER:
			top = window->anchor_vertical - window->row_count / 2;
			left = window->anchor_horizontal - window->col_count / 2;
			break;
		case CCX_DTVCC_ANCHOR_POINT_MIDDLE_RIGHT:
			top = window->anchor_vertical - window->row_count / 2;
			left = window->anchor_horizontal - window->col_count;
			break;
		case CCX_DTVCC_ANCHOR_POINT_BOTTOM_LEFT:
			top = window->anchor_vertical - window->row_count;
			left = window->anchor_horizontal;
			break;
		case CCX_DTVCC_ANCHOR_POINT_BOTTOM_CENTER:
			top = window->anchor_vertical - window->row_count;
			left = window->anchor_horizontal - window->col_count / 2;
			break;
		case CCX_DTVCC_ANCHOR_POINT_BOTTOM_RIGHT:
			top = window->anchor_vertical - window->row_count;
			left = window->anchor_horizontal - window->col_count;
			break;
		default: // Shouldn't happen, but skip the window just in case
			return;
			break;
	}
	ccx_dtvcc_log("[CEA-708] For window %d: Anchor point -> %d, size %d:%d, real position %d:%d\n",
								 window->number, window->anchor_point, window->row_count, window->col_count,
								 top, left
	);

	ccx_dtvcc_log(
			"[CEA-708] we have top [%d] and left [%d]\n", top, left);

	top = top < 0 ? 0 : top;
	left = left < 0 ? 0 : left;

	int copyrows = top + window->row_count >= CCX_DTVCC_SCREENGRID_ROWS ?
				   CCX_DTVCC_SCREENGRID_ROWS - top : window->row_count;
	int copycols = left + window->col_count >= CCX_DTVCC_SCREENGRID_COLUMNS ?
				   CCX_DTVCC_SCREENGRID_COLUMNS - left : window->col_count;

	ccx_dtvcc_log(
			"[CEA-708] %d*%d will be copied to the TV.\n", copyrows, copycols);

	dtvcc_tv_screen *tv = decoder->tv;
	if(!tv){
		ccx_dtvcc_log("[CEA-708] Error: dtvcc_tv_screen was not memory allocated");
		return;
	}
	
	for (int j = 0; j < copyrows; j++)
	{
		memcpy(tv->chars[top + j], window->rows[j], copycols * sizeof(ccx_dtvcc_symbol));
		for (int col = 0; col < CCX_DTVCC_SCREENGRID_COLUMNS; col++)
		{
			tv->pen_attribs[top + j][col] = window->pen_attribs[j][col];
			tv->pen_colors[top + j][col] = window->pen_colors[j][col];
		}
	}

#ifdef DTVCC_PRINT_DEBUG
	_dtvcc_window_dump(dtvcc, window);
#endif

	char *buf = (char *) dtvcc->cc_data;
	if (buf)
	{
		dtvcc->cc_data_size = 0;
		for (int j = 0; j < copyrows; j++)
		{
			if (!_dtvcc_is_screen_row_empty(tv, top + j)){
				_dtvcc_write_screen_row(dtvcc, decoder, top + j, 1);				
			}
			dtvcc->cc_data_size += sprintf(buf + dtvcc->cc_data_size, "<br>" );
		}
		buf[dtvcc->cc_data_size]='\0';
		ccx_dtvcc_callback_event(dtvcc, window->number, CCX_DTVCC_TV_EVENT_CAPTION_DATA, buf);
	}
	_dtvcc_tv_clear(decoder);
}

void _dtvcc_screen_print(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_service_decoder *decoder)
{
	ccx_dtvcc_log("[CEA-708] _dtvcc_screen_print\n");

	//TODO use priorities to solve windows overlap
	//qsort(wnd, visible, sizeof(ccx_dtvcc_window *), _dtvcc_compare_win_priorities);

#ifdef DTVCC_PRINT_DEBUG
	ccx_dtvcc_log("[CEA-708] TV dump:\n");
	ccx_dtvcc_write_screen_debug(dtvcc, decoder);
#endif

	ccx_dtvcc_write_screen_caption(dtvcc, decoder);
	_dtvcc_tv_clear(decoder);
}

void _dtvcc_process_hcr(ccx_dtvcc_service_decoder *decoder)
{
	if (decoder->current_window == -1)
	{
		ccx_dtvcc_log("[CEA-708] _dtvcc_process_hcr: Window has to be defined first\n");
		return;
	}

	ccx_dtvcc_window *window = decoder->windows[decoder->current_window];
	window->pen_column = 0;
	window->real_col_count = window->col_count;
	_dtvcc_window_clear_row(window, window->pen_row);
}

void _dtvcc_process_ff(ccx_dtvcc_service_decoder *decoder)
{
	if (decoder->current_window == -1)
	{
		ccx_dtvcc_log("[CEA-708] _dtvcc_process_ff: Window has to be defined first\n");
		return;
	}
	
	ccx_dtvcc_log("[CEA-708] _dtvcc_process_ff \n");	
	ccx_dtvcc_window *window = decoder->windows[decoder->current_window];
	window->pen_column = 0;
	window->pen_row = 0;
	window->real_col_count = window->col_count;	
	//CEA-708-D doesn't say we have to clear neither window text nor text line,
	//but it seems we have to clean the line
	_dtvcc_window_clear_text(window);
}

void _dtvcc_process_etx(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_service_decoder *decoder)
{
	if (decoder->current_window == -1)
	{
		ccx_dtvcc_log("[CEA-708] _dtvcc_process_etx: Window has to be defined first\n");
		return;
	}

	ccx_dtvcc_window *window = decoder->windows[decoder->current_window];
	if (window->is_defined)
	{
		ccx_dtvcc_log("[CEA-708] _dtvcc_process_etx \n");
		_dtvcc_window_copy_to_screen(dtvcc, decoder, window);
		dtvcc->screen_content_changed = 0;		
	}
}

void _dtvcc_process_bs(ccx_dtvcc_service_decoder *decoder)
{
	if (decoder->current_window == -1)
	{
		ccx_dtvcc_log("[CEA-708] _dtvcc_process_bs: Window has to be defined first\n");
		return;
	}

	ccx_dtvcc_log("[CEA-708] _dtvcc_process_bs \n");	

	int cw = decoder->current_window;
	ccx_dtvcc_window *window = decoder->windows[cw];

	switch (window->attribs.print_direction)
	{
	case CCX_DTVCC_WINDOW_PD_RIGHT_LEFT:
		if (window->pen_column + 1 < window->col_count)
			window->pen_column++;
		break;
	case CCX_DTVCC_WINDOW_PD_LEFT_RIGHT:
		if (window->pen_column > 0)
			window->pen_column--;
		break;
	case CCX_DTVCC_WINDOW_PD_BOTTOM_TOP:
		if (window->pen_row + 1 < window->row_count)
			window->pen_row++;
		break;
	case CCX_DTVCC_WINDOW_PD_TOP_BOTTOM:
		if (window->pen_row > 0)
			window->pen_row--;
		break;
	default:
		ccx_dtvcc_log("[CEA-708] _dtvcc_process_character: unhandled branch (%02d)\n",
			window->attribs.print_direction);
		break;
	}
}

void _dtvcc_window_rolldown(ccx_dtvcc_window *window)
{
	for (int i = window->row_count - 1; i > 0; i--)
	{
		memcpy(window->rows[i], window->rows[i - 1], CCX_DTVCC_MAX_COLUMNS * sizeof(ccx_dtvcc_symbol));
		for (int z = 0; z < CCX_DTVCC_MAX_COLUMNS; z++)
		{
			window->pen_colors[i][z] = window->pen_colors[i - 1][z];
			window->pen_attribs[i][z] = window->pen_attribs[i - 1][z];
		}
	}

	_dtvcc_window_clear_row(window, 0);
}


void _dtvcc_window_rollup(ccx_dtvcc_window *window)
{
	for (int i = 0; i < window->row_count - 1; i++)
	{
		memcpy(window->rows[i], window->rows[i + 1], CCX_DTVCC_MAX_COLUMNS * sizeof(ccx_dtvcc_symbol));
		for (int z = 0; z < CCX_DTVCC_MAX_COLUMNS; z++)
		{
			window->pen_colors[i][z] = window->pen_colors[i + 1][z];
			window->pen_attribs[i][z] = window->pen_attribs[i + 1][z];
		}
	}

	_dtvcc_window_clear_row(window, window->row_count - 1);
}

void _dtvcc_process_cr(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_service_decoder *decoder)
{
	if (decoder->current_window == -1)
	{
		ccx_dtvcc_log("[CEA-708] _dtvcc_process_cr: Window has to be defined first\n");
		return;
	}

	ccx_dtvcc_window *window = decoder->windows[decoder->current_window];

	int rollup_required = 0;
	int rolldown_required = 0;

	window->real_col_count = window->col_count;

	switch (window->attribs.scroll_direction)
	{
		case CCX_DTVCC_WINDOW_SD_TOP_BOTTOM:
			window->pen_column = 0;
			if (window->pen_row - 1 >= 0)
				window->pen_row--;
			else rolldown_required = 1;  
			break;
		case CCX_DTVCC_WINDOW_SD_BOTTOM_TOP:
			window->pen_column = 0;
			if (window->pen_row + 1 < window->row_count)
				window->pen_row++;
			else rollup_required = 1;
			break;
		default:
			window->pen_column = 0;
			if (window->pen_row + 1 < window->row_count)
				window->pen_row++;
			else rollup_required = 1;
			break;
	}

	if (window->is_defined)
	{
		if (rollup_required)
		{
			_dtvcc_window_rollup(window);
		}
		
		if (rolldown_required)
		{
			_dtvcc_window_rolldown(window);
		}
		
		ccx_dtvcc_log("[CEA-708] _dtvcc_process_cr: rolling up\n");					
		
		if(dtvcc->rollup_screen_update){
			_dtvcc_window_copy_to_screen(dtvcc, decoder, window);
			dtvcc->screen_content_changed = 0;
		}else{
			dtvcc->screen_content_changed = 1;
		}
	}
}

void _dtvcc_process_character(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_service_decoder *decoder, ccx_dtvcc_symbol symbol)
{
	ccx_dtvcc_log("[CEA-708] _dtvcc_process_character Window: %d\n", decoder->current_window);

	int cw = decoder->current_window;
	ccx_dtvcc_window *window = decoder->windows[cw];

	if(window == NULL) {
        ccx_dtvcc_log("[CEA-708] _dtvcc_process_character: Window: %d is null \n", cw);
	} else {
        ccx_dtvcc_log(
                "[CEA-708] _dtvcc_process_character: "
                        "%c [%04X]  - Window: %d %s, Pen: %d:%d\n",
                CCX_DTVCC_SYM(symbol), symbol.sym,
                cw, window->is_defined ? "[OK]" : "[undefined]",
                cw != -1 ? window->pen_row : -1, cw != -1 ? window->pen_column : -1
        );
	}


	if (cw == -1 || !window->is_defined) // Writing to a non existing window, skipping
		return;

	int col_count = window->col_count;
	int row_count = window->row_count;

#ifdef DTVCC_TTA_CERT_MODE	
    if(dtvcc->kec_mode == 1)
    {
    	if(dtvcc->cc_descriptor.korean_code){ // Unicode 
    		if( (0x1100 <= symbol.sym && symbol.sym < 0x1200) ||
    			(0x2113 <= symbol.sym && symbol.sym < 0x2127) ||
    			(0x2E80 <= symbol.sym && symbol.sym < 0xA500) ||
    			(0xAC00 <= symbol.sym && symbol.sym < 0xD800) ||
    			(0xF900 <= symbol.sym && symbol.sym < 0xFB00) ||
    			(0xFE30 <= symbol.sym && symbol.sym < 0xFE50) )

    			if(window->real_col_count > window->col_count/2)
    				window->real_col_count--;
    	}else{
    		if( (0xA2DE <= symbol.sym && symbol.sym < 0xA2E5) ||
    			(0xA4A1 <= symbol.sym && symbol.sym < 0xA4FE) ||
    			(0xA7A1 <= symbol.sym && symbol.sym < 0xA7F0) ||
    			(0xA8B1 <= symbol.sym && symbol.sym < 0xA8CD) ||
    			(0xA9B1 <= symbol.sym && symbol.sym < 0xA9CD) ||
    			(0xAAA1 <= symbol.sym && symbol.sym < 0xAAF4) ||
    			(0xABA1 <= symbol.sym && symbol.sym < 0xABF7) ||
    			(0xB000 <= symbol.sym ) )

    			if(window->real_col_count > window->col_count/2)
    				window->real_col_count--;
    	}

    	ccx_dtvcc_log("[CEA-708] _dtvcc_process_character real_col_count: %d\n", window->real_col_count);
    	col_count = window->real_col_count;
	}
#endif

	window->is_empty = 0;
	window->rows[window->pen_row][window->pen_column] = symbol;
	window->pen_attribs[window->pen_row][window->pen_column] = window->pen_attribs_pattern;		// "Painting" char by pen - attribs
	window->pen_colors[window->pen_row][window->pen_column] = window->pen_color_pattern;		// "Painting" char by pen - colors
	switch (window->attribs.print_direction)
	{
		case CCX_DTVCC_WINDOW_PD_LEFT_RIGHT:
			if (window->pen_column + 1 < col_count)
				window->pen_column++;
			break;
		case CCX_DTVCC_WINDOW_PD_RIGHT_LEFT:
			if (window->pen_column > 0)
				window->pen_column--;
			break;
		case CCX_DTVCC_WINDOW_PD_TOP_BOTTOM:
			if (window->pen_row + 1 < row_count)
				window->pen_row++;
			break;
		case CCX_DTVCC_WINDOW_PD_BOTTOM_TOP:
			if (window->pen_row > 0)
				window->pen_row--;
			break;
		default:
			ccx_dtvcc_log("[CEA-708] _dtvcc_process_character: unhandled branch (%02d)\n",
				window->attribs.print_direction);
			break;
	}
	ccx_dtvcc_log("[CEA-708] _dtvcc_process_character Pen: %d:%d end\n", window->pen_row, window->pen_column);
}

//---------------------------------- COMMANDS ------------------------------------

void dtvcc_handle_CWx_SetCurrentWindow(ccx_dtvcc_service_decoder *decoder, int window_id)
{
	ccx_dtvcc_log(
			"[CEA-708] dtvcc_handle_CWx_SetCurrentWindow: [%d]\n", window_id);

	ccx_dtvcc_window *window = decoder->windows[window_id];
	
	if (window->is_defined)
		decoder->current_window = window_id;
	else
		ccx_dtvcc_log("[CEA-708] dtvcc_handle_CWx_SetCurrentWindow: "
										   "window [%d] is not defined\n", window_id);
}

void dtvcc_handle_CLW_ClearWindows(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_service_decoder *decoder, int windows_bitmap)
{
	char logging[1024] = {0,};
	sprintf(logging, "[CEA-708] dtvcc_handle_CLW_ClearWindows: windows: ");

	if (windows_bitmap == 0)
		sprintf(logging+strlen(logging), "none\n");
	else
	{
		for (int i = 0; i < CCX_DTVCC_MAX_WINDOWS; i++)
		{
			if (windows_bitmap & 1)
			{
				sprintf(logging+strlen(logging), "[Window %d] ", i);
				_dtvcc_window_clear(decoder, i);

				ccx_dtvcc_window *window = decoder->windows[i];
				if (!window->is_defined)
				{
					ccx_dtvcc_log("[CEA-708] Error: window %d was not defined", i);
					continue;
				}
				if (window->visible)
				{
					_dtvcc_window_copy_to_screen(dtvcc, decoder, window);
					dtvcc->screen_content_changed = 0;						
				}
			}
			windows_bitmap >>= 1;
		}
		sprintf(logging+strlen(logging), "\n");
	}
	ccx_dtvcc_log(logging);
}

void dtvcc_handle_DSW_DisplayWindows(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_service_decoder *decoder, int windows_bitmap)
{
	char logging[1024] = {0,};
	sprintf(logging, "[CEA-708] dtvcc_handle_DSW_DisplayWindows: windows: ");

	if (windows_bitmap == 0)
		sprintf(logging+strlen(logging), "none\n");
	else
	{
		for (int i = 0; i < CCX_DTVCC_MAX_WINDOWS; i++)
		{
			if (windows_bitmap & 1)
			{
				ccx_dtvcc_window *window = decoder->windows[i];
				sprintf(logging+strlen(logging), "[Window %d] ", i);
				if (!window->is_defined)
				{
					ccx_dtvcc_log("[CEA-708] Error: window %d was not defined", i);
					continue;
				}
				if (!window->visible)
				{
					window->visible = 1;
					if (!window->is_empty){
						_dtvcc_window_copy_to_screen(dtvcc, decoder, window);
						dtvcc->screen_content_changed = 0;						
					}
				}
			}
			windows_bitmap >>= 1;
		}
		sprintf(logging+strlen(logging), "\n");
	}
	ccx_dtvcc_log(logging);	
}


void dtvcc_handle_HDW_HideWindows(ccx_dtvcc_ctx *dtvcc,
								  ccx_dtvcc_service_decoder *decoder,
								  int windows_bitmap)
{
	char logging[1024] = {0,};
	sprintf(logging, "[CEA-708] dtvcc_handle_HDW_HideWindows: windows: ");

	if (windows_bitmap == 0)
		sprintf(logging+strlen(logging), "none\n");
	else
	{
		for (int i = 0; i < CCX_DTVCC_MAX_WINDOWS; i++)
		{
			if (windows_bitmap & 1)
			{
				ccx_dtvcc_window *window = decoder->windows[i];			
				sprintf(logging+strlen(logging), "[Window %d] ", i);
				if (window->visible)
				{
					window->visible = 0;
					if (!window->is_empty){
						ccx_dtvcc_callback_event(dtvcc, window->number, CCX_DTVCC_TV_EVENT_CLEAR_WINDOW, 0);
					}	
				}
			}
			windows_bitmap >>= 1;
		}
		sprintf(logging+strlen(logging), "\n");
	}
	ccx_dtvcc_log(logging);	
}

void dtvcc_handle_TGW_ToggleWindows(ccx_dtvcc_ctx *dtvcc,
									ccx_dtvcc_service_decoder *decoder,
									int windows_bitmap)
{
	char logging[1024] = {0,};
	sprintf(logging, "[CEA-708] dtvcc_handle_TGW_ToggleWindows: windows: ");

	if (windows_bitmap == 0)
		sprintf(logging+strlen(logging), "none\n");
	else
	{
		for (int i = 0; i < CCX_DTVCC_MAX_WINDOWS; i++)
		{
			ccx_dtvcc_window *window = decoder->windows[i];
			if ((windows_bitmap & 1) && (window->is_defined))
			{
				sprintf(logging+strlen(logging), "[W-%d: %d->%d]", i, window->visible, !window->visible);			
				window->visible = !window->visible;
				if (window->visible)
				{
					if (!window->is_empty)
					{
						_dtvcc_window_copy_to_screen(dtvcc, decoder, window);
						dtvcc->screen_content_changed = 0;
					}
				}
				else
				{
					ccx_dtvcc_callback_event(dtvcc, window->number, CCX_DTVCC_TV_EVENT_CLEAR_WINDOW, 0);
				}
			}
			windows_bitmap >>= 1;
		}
		sprintf(logging+strlen(logging), "\n");
	}
	ccx_dtvcc_log(logging);	
}


void dtvcc_handle_DFx_DefineWindow(ccx_dtvcc_ctx *dtvcc, 
						ccx_dtvcc_service_decoder *decoder, 
						int window_id, 
						unsigned char *data)
{
	ccx_dtvcc_log("[CEA-708] dtvcc_handle_DFx_DefineWindow: "
			"W[%d], attributes: \n", window_id);

	ccx_dtvcc_window *window = decoder->windows[window_id];
/*
#ifndef DTVCC_TTA_CERT_MODE
	if ((window->is_defined) && !memcmp(window->commands, data + 1, 6))
	{
		// When a decoder receives a DefineWindow command for an existing window, the
		// command is to be ignored if the command parameters are unchanged from the
		// previous window definition.
		decoder->current_window = window_id;
		ccx_dtvcc_log("[CEA-708] dtvcc_handle_DFx_DefineWindow: Repeated window definition, ignored\n");
		return;
	}
#endif
*/
	window->number = window_id;

	int priority = (data[1]) & 0x7;
	int col_lock = (data[1] >> 3) & 0x1;
	int row_lock = (data[1] >> 4) & 0x1;
	int visible  = (data[1] >> 5) & 0x1;
	int anchor_vertical = data[2] & 0x7f;
	int relative_pos = data[2] >> 7;
	int anchor_horizontal = data[3];
	int row_count = (data[4] & 0xf) + 1; //according to CEA-708-D
	int anchor_point = data[4] >> 4;
	int col_count = (data[5] & 0x3f) + 1; //according to CEA-708-D
	int pen_style = data[6] & 0x7;
	int win_style = (data[6] >> 3) & 0x7;

	int do_clear_window = 0;

	ccx_dtvcc_log("[CEA-708] Visible: [%s]\n", visible ? "Yes" : "No");
	ccx_dtvcc_log("[CEA-708] Priority: [%d]\n", priority);
	ccx_dtvcc_log("[CEA-708] Row count: [%d]\n", row_count);
	ccx_dtvcc_log("[CEA-708] Column count: [%d]\n", col_count);
	ccx_dtvcc_log("[CEA-708] Anchor point: [%d]\n", anchor_point);
	ccx_dtvcc_log("[CEA-708] Anchor vertical: [%d]\n", anchor_vertical);
	ccx_dtvcc_log("[CEA-708] Anchor horizontal: [%d]\n", anchor_horizontal);
	ccx_dtvcc_log("[CEA-708] Relative pos: [%s]\n", relative_pos ? "Yes" : "No");
	ccx_dtvcc_log("[CEA-708] Row lock: [%s]\n", row_lock ? "Yes" : "No");
	ccx_dtvcc_log("[CEA-708] Column lock: [%s]\n", col_lock ? "Yes" : "No");
	ccx_dtvcc_log("[CEA-708] Pen style: [%d]\n", pen_style);
	ccx_dtvcc_log("[CEA-708] Win style: [%d]\n", win_style);

	if(relative_pos){
		anchor_vertical = (int) ((anchor_vertical *  CCX_DTVCC_SCREENGRID_ROWS) / 100);
		anchor_horizontal = (int) ((anchor_horizontal *  CCX_DTVCC_SCREENGRID_COLUMNS) / 100);		
		ccx_dtvcc_log("[CEA-708] Anchor vertical: [%d]\n", anchor_vertical);
		ccx_dtvcc_log("[CEA-708] Anchor horizontal: [%d]\n", anchor_horizontal);
	}

	if (anchor_vertical > CCX_DTVCC_SCREENGRID_ROWS - row_count)
		anchor_vertical = CCX_DTVCC_SCREENGRID_ROWS - row_count;
	if (anchor_horizontal > CCX_DTVCC_SCREENGRID_COLUMNS - col_count)
		anchor_horizontal = CCX_DTVCC_SCREENGRID_COLUMNS - col_count;

	window->priority = priority;
	window->col_lock = col_lock;
	window->row_lock = row_lock;
	window->visible = visible;
	window->anchor_vertical = anchor_vertical;
	window->relative_pos = relative_pos;
	window->anchor_horizontal = anchor_horizontal;
	window->row_count = row_count;
	window->anchor_point = anchor_point;
	window->col_count = col_count;
	window->real_col_count = col_count;

	int top, left;
	top = window->anchor_vertical;
	left = window->anchor_horizontal;

	ccx_dtvcc_log("[CEA-708] we have top [%d] and left [%d]\n", top, left);
	
	char *buf = (char *) dtvcc->buffer;
	if (buf)
	{
		int buf_len = sprintf(buf, "%d", window->anchor_point);
		buf[buf_len]='\0';
		ccx_dtvcc_callback_event(dtvcc, window->number, CCX_DTVCC_TV_EVENT_WINDOW_ANCHOR, buf);

		buf_len = sprintf(buf, "%d:%d", top, left);
		buf[buf_len]='\0';
		ccx_dtvcc_callback_event(dtvcc, window->number, CCX_DTVCC_TV_EVENT_WINDOW_POINT, buf);
	}

	// If changing the style of an existing window delete contents
	if (win_style > 0 && window->is_defined && window->win_style != win_style)
		do_clear_window = 1;

	/* If the window doesn't exist and win style==0 then default to win_style=1 */
	if (win_style == 0 && !window->is_defined) {
		win_style = 1;
	}
	/* If the window doesn't exist and pen style==0 then default to pen_style=1 */
	if (pen_style == 0 && !window->is_defined) {
		pen_style = 1;
	}

	//Apply windows attribute presets
	if (win_style > 0 && win_style < 8)

		window->win_style = win_style; {
		window->attribs.border_color = ccx_dtvcc_predefined_window_styles[win_style].border_color;
		window->attribs.border_type = ccx_dtvcc_predefined_window_styles[win_style].border_type;
		window->attribs.display_effect = ccx_dtvcc_predefined_window_styles[win_style].display_effect;
		window->attribs.effect_direction = ccx_dtvcc_predefined_window_styles[win_style].effect_direction;
		window->attribs.effect_speed = ccx_dtvcc_predefined_window_styles[win_style].effect_speed;
		window->attribs.fill_color = ccx_dtvcc_predefined_window_styles[win_style].fill_color;
		window->attribs.fill_opacity = ccx_dtvcc_predefined_window_styles[win_style].fill_opacity;
		window->attribs.justify = ccx_dtvcc_predefined_window_styles[win_style].justify;
		window->attribs.print_direction = ccx_dtvcc_predefined_window_styles[win_style].print_direction;
		window->attribs.scroll_direction = ccx_dtvcc_predefined_window_styles[win_style].scroll_direction;
		window->attribs.word_wrap = ccx_dtvcc_predefined_window_styles[win_style].word_wrap;
	}

	if (pen_style > 0)
	{
		//TODO apply static pen_style preset
		window->pen_style = pen_style;
	}

	if (!window->is_defined)
	{
		// If the window is being created, all character positions in the window
		// are set to the fill color and the pen location is set to (0,0)
		window->pen_column = 0;
		window->pen_row = 0;
		if (!window->memory_reserved)
		{
			for (int i = 0; i < CCX_DTVCC_MAX_ROWS; i++)
			{
				window->rows[i] = (ccx_dtvcc_symbol *) calloc(CCX_DTVCC_MAX_COLUMNS, sizeof(ccx_dtvcc_symbol));
				if (!window->rows[i])
					ccx_dtvcc_log("[CEA-708] enough memory alloc window row dtvcc_handle_DFx_DefineWindow");
			}
			window->memory_reserved = 1;
		}
		window->is_defined = 1;
		_dtvcc_window_clear_text(window);

		//Accorgind to CEA-708-D if window_style is 0 for newly created window , we have to apply predefined style #1
		if (window->win_style == 0)
		{
			_dtvcc_window_apply_style(window, &ccx_dtvcc_predefined_window_styles[0]);
		}
		else if (window->win_style <= 7)
		{
			_dtvcc_window_apply_style(window, &ccx_dtvcc_predefined_window_styles[window->win_style]);
		}
		else
		{
			ccx_dtvcc_log("[CEA-708] dtvcc_handle_DFx_DefineWindow: "
											   "invalid win_style num %d\n", window->win_style);
			_dtvcc_window_apply_style(window, &ccx_dtvcc_predefined_window_styles[0]);
		}
	}
	else
	{
		if (do_clear_window)
			_dtvcc_window_clear_text(window);
	}
	// ...also makes the defined windows the current window
	dtvcc_handle_CWx_SetCurrentWindow(decoder, window_id);
	memcpy(window->commands, data + 1, 6);

	if (window->is_defined){
		char *buf = (char *) dtvcc->buffer;
		if (buf)
		{
			int buf_len = sprintf(buf, "%d:%d", row_count, col_count);
			buf[buf_len]='\0';
			ccx_dtvcc_callback_event(dtvcc, window_id, CCX_DTVCC_TV_EVENT_WINDOW_ROW_COL, buf);

			unsigned alpha, red, green, blue;
			_dtvcc_color_to_hex(window->attribs.fill_color, &red, &green, &blue);
			switch (window->attribs.fill_opacity)
			{
			case CCX_DTVCC_WINDOW_FO_SOLID:
			case CCX_DTVCC_WINDOW_FO_FLASH: 			
				alpha = 255;
				break;
			case CCX_DTVCC_WINDOW_FO_TRANSLUCENT:
				alpha = 128;
				break;
			case CCX_DTVCC_WINDOW_FO_TRANSPARENT:
				alpha = 0;
				break;
			}
			
			red = (255 / 3) * red;
			green = (255 / 3) * green;
			blue = (255 / 3) * blue;
			
			buf_len = sprintf(buf, "#%02x%02x%02x%02x", alpha, red, green, blue);
			buf[buf_len]='\0';
			ccx_dtvcc_callback_event(dtvcc, window_id, CCX_DTVCC_TV_EVENT_WINDOW_COLOR, buf);
			
			buf_len = sprintf(buf, "%d", window->attribs.justify);
			buf[buf_len]='\0';
			ccx_dtvcc_callback_event(dtvcc, window_id, CCX_DTVCC_TV_EVENT_WINDOW_JUSTIFY, buf);
		}
	}
}


void dtvcc_handle_SWA_SetWindowAttributes(ccx_dtvcc_ctx *dtvcc,
									ccx_dtvcc_service_decoder *decoder,
									unsigned char *data)
{
	ccx_dtvcc_log("[CEA-708] dtvcc_handle_SWA_SetWindowAttributes: attributes: \n");

	int fill_color    = (data[1]     ) & 0x3f;
	int fill_opacity  = (data[1] >> 6) & 0x03;
	int border_color  = (data[2]     ) & 0x3f;
	int border_type01 = (data[2] >> 6) & 0x03;
	int justify       = (data[3]     ) & 0x03;
	int scroll_dir    = (data[3] >> 2) & 0x03;
	int print_dir     = (data[3] >> 4) & 0x03;
	int word_wrap     = (data[3] >> 6) & 0x01;
	int border_type   = ((data[3] >> 5) & 0x04)| border_type01;
	int display_eff   = (data[4]     ) & 0x03;
	int effect_dir    = (data[4] >> 2) & 0x03;
	int effect_speed  = (data[4] >> 4) & 0x0f;

	ccx_dtvcc_log("       Fill color: [%d]     Fill opacity: [%d]    Border color: [%d]  Border type: [%d]\n",
			fill_color, fill_opacity, border_color, border_type01);
	ccx_dtvcc_log("          Justify: [%d]       Scroll dir: [%d]       Print dir: [%d]    Word wrap: [%d]\n",
			justify, scroll_dir, print_dir, word_wrap);
	ccx_dtvcc_log("      Border type: [%d]      Display eff: [%d]      Effect dir: [%d] Effect speed: [%d]\n",
			border_type, display_eff, effect_dir, effect_speed);

	if (decoder->current_window == -1)
	{
		ccx_dtvcc_log("[CEA-708] dtvcc_handle_SWA_SetWindowAttributes: "
										   "Window has to be defined first\n");
		return;
	}

	ccx_dtvcc_window *window = decoder->windows[decoder->current_window];

	window->attribs.fill_color = fill_color;
	window->attribs.fill_opacity = fill_opacity;
	window->attribs.border_color = border_color;
	window->attribs.justify = justify;
	window->attribs.scroll_direction = scroll_dir;
	window->attribs.print_direction = print_dir;
	window->attribs.word_wrap = word_wrap;
	window->attribs.border_type = border_type;
	window->attribs.display_effect = display_eff;
	window->attribs.effect_direction = effect_dir;
	window->attribs.effect_speed = effect_speed;

	{
		char *buf = (char *) dtvcc->buffer;
		if (buf)
		{
			unsigned alpha, red, green, blue;
			_dtvcc_color_to_hex(fill_color, &red, &green, &blue);
			switch (fill_opacity)
			{
			case CCX_DTVCC_WINDOW_FO_SOLID:
			case CCX_DTVCC_WINDOW_FO_FLASH: 			
				alpha = 255;
				break;
			case CCX_DTVCC_WINDOW_FO_TRANSLUCENT:
				alpha = 128;
				break;
			case CCX_DTVCC_WINDOW_FO_TRANSPARENT:
				alpha = 0;
				break;
			}
			
			red = (255 / 3) * red;
			green = (255 / 3) * green;
			blue = (255 / 3) * blue;
			
			int buf_len = sprintf(buf, "#%02x%02x%02x%02x", alpha, red, green, blue);
			buf[buf_len]='\0';
			ccx_dtvcc_log("[CEA-708] dtvcc_handle_SWA_SetWindowAttributes fill_color color = #%02x%02x%02x%02x\n", alpha, red, green, blue);
			ccx_dtvcc_callback_event(dtvcc, window->number, CCX_DTVCC_TV_EVENT_WINDOW_COLOR, buf);
	
			buf_len = sprintf(buf, "%d", window->attribs.justify);
			buf[buf_len]='\0';
			ccx_dtvcc_callback_event(dtvcc, window->number, CCX_DTVCC_TV_EVENT_WINDOW_JUSTIFY, buf);
		}
	}
}

void dtvcc_handle_DLW_DeleteWindows(ccx_dtvcc_ctx *dtvcc,
									ccx_dtvcc_service_decoder *decoder,
									int windows_bitmap)
{
	char logging[1024] = {0,};
	sprintf(logging, "[CEA-708] dtvcc_handle_DLW_DeleteWindows: windows: ");

	if (windows_bitmap == 0)
		sprintf(logging+strlen(logging), "none\n");
	else
	{
		for (int i = 0; i < CCX_DTVCC_MAX_WINDOWS; i++)
		{
			if (windows_bitmap & 1)
			{
				ccx_dtvcc_window *window = decoder->windows[i];
				sprintf(logging+strlen(logging), "Deleting [W-%d] ", i);
				ccx_dtvcc_callback_event(dtvcc, i, CCX_DTVCC_TV_EVENT_DELETE_WINDOW, 0);
				window->is_defined = 0;
				window->visible = 0;
				if (i == decoder->current_window)
				{
					// If the current window is deleted, then the decoder's current window ID
					// is unknown and must be reinitialized with either the SetCurrentWindow
					// or DefineWindow command.
					decoder->current_window = -1;
				}
			}
			windows_bitmap >>= 1;
		}
		sprintf(logging+strlen(logging), "\n");
	}
	ccx_dtvcc_log(logging);
}

void dtvcc_handle_SPA_SetPenAttributes(ccx_dtvcc_ctx *dtvcc,
									   ccx_dtvcc_service_decoder *decoder, 
									   unsigned char *data)
{
	ccx_dtvcc_log("[CEA-708] dtvcc_handle_SPA_SetPenAttributes: attributes: \n");

	int pen_size  = (data[1]     ) & 0x3;
	int offset    = (data[1] >> 2) & 0x3;
	int text_tag  = (data[1] >> 4) & 0xf;
	int font_tag  = (data[2]     ) & 0x7;
	int edge_type = (data[2] >> 3) & 0x7;
	int underline = (data[2] >> 6) & 0x1;
	int italic    = (data[2] >> 7) & 0x1;

	ccx_dtvcc_log("       Pen size: [%d]     Offset: [%d]  Text tag: [%d]   Font tag: [%d]\n",
			pen_size, offset, text_tag, font_tag);
	ccx_dtvcc_log("      Edge type: [%d]  Underline: [%d]    Italic: [%d]\n",
			edge_type, underline, italic);

	if (decoder->current_window == -1)
	{
		ccx_dtvcc_log("[CEA-708] dtvcc_handle_SPA_SetPenAttributes: "
										   "Window has to be defined first\n");
		return;
	}

	ccx_dtvcc_window *window = decoder->windows[decoder->current_window];

	if (window->pen_row == -1)
	{
		ccx_dtvcc_log("[CEA-708] dtvcc_handle_SPA_SetPenAttributes: "
										   "can't set pen attribs for undefined row\n");
		return;
	}

	ccx_dtvcc_pen_attribs *pen = &window->pen_attribs_pattern;

	pen->pen_size = pen_size;
	pen->offset = offset;
	pen->text_tag = text_tag;
	pen->font_tag = font_tag;
	pen->edge_type = edge_type;
	pen->underline = underline;
	pen->italic = italic;

	char *buf = (char *) dtvcc->buffer;
	if (buf)
	{
		int buf_len = sprintf(buf, "%d", pen_size);
		buf[buf_len]='\0';
		ccx_dtvcc_callback_event(dtvcc, window->number, CCX_DTVCC_TV_EVENT_FONT_SIZE, buf);
	}
}

void dtvcc_handle_SPC_SetPenColor(ccx_dtvcc_ctx *dtvcc, 
								  ccx_dtvcc_service_decoder *decoder, 
								  unsigned char *data)
{
	ccx_dtvcc_log("[CEA-708] dtvcc_handle_SPC_SetPenColor: attributes: \n");

	int fg_color   = (data[1]     ) & 0x3f;
	int fg_opacity = (data[1] >> 6) & 0x03;
	int bg_color   = (data[2]     ) & 0x3f;
	int bg_opacity = (data[2] >> 6) & 0x03;
	int edge_color = (data[3]     ) & 0x3f;

	ccx_dtvcc_log("      Foreground color: [%d]     Foreground opacity: [%d]\n",
			fg_color, fg_opacity);
	ccx_dtvcc_log("      Background color: [%d]     Background opacity: [%d]\n",
			bg_color, bg_opacity);
	ccx_dtvcc_log("            Edge color: [%d]\n",
			edge_color);

	if (decoder->current_window == -1)
	{
		ccx_dtvcc_log("[CEA-708] dtvcc_handle_SPC_SetPenColor: "
										   "Window has to be defined first\n");
		return;
	}

	ccx_dtvcc_window *window = decoder->windows[decoder->current_window];

	if (window->pen_row == -1)
	{
		ccx_dtvcc_log("[CEA-708] dtvcc_handle_SPA_SetPenAttributes: "
										   "can't set pen color for undefined row\n");
		return;
	}

	ccx_dtvcc_pen_color *color = &window->pen_color_pattern;

	color->fg_color = fg_color;
	color->fg_opacity = fg_opacity;
	color->bg_color = bg_color;
	color->bg_opacity = bg_opacity;
	color->edge_color = edge_color;

}

void dtvcc_handle_SPL_SetPenLocation(ccx_dtvcc_ctx *dtvcc, 
									 ccx_dtvcc_service_decoder *decoder, 
									 unsigned char *data)
{
	ccx_dtvcc_log("[CEA-708] dtvcc_handle_SPL_SetPenLocation: attributes: \n");

	int row = data[1] & 0x0f;
	int col = data[2] & 0x3f;

	ccx_dtvcc_log("      row: [%d]     Column: [%d]\n", row, col);

	if (decoder->current_window == -1)
	{
		ccx_dtvcc_log("[CEA-708] dtvcc_handle_SPL_SetPenLocation: "
										   "Window has to be defined first\n");
		return;
	}

	ccx_dtvcc_window *window = decoder->windows[decoder->current_window];
	window->pen_row = row;
	window->pen_column = col;
	window->real_col_count = window->col_count;

	ccx_dtvcc_symbol sym;
	CCX_DTVCC_SYM_SET(sym, 0x20);

	for (int i = 0; i < col; i++)
	{
		if(CCX_DTVCC_SYM_IS_EMPTY(window->rows[row][i])){
			window->rows[row][i] = sym;
			window->pen_colors[row][i] = window->pen_color_pattern;
			window->pen_attribs[row][i] = window->pen_attribs_pattern;
		}
	}

	char *buf = (char *) dtvcc->buffer;
	if (buf)
	{
		int buf_len = sprintf(buf, "%d:%d", row, col);
		buf[buf_len]='\0';
		ccx_dtvcc_callback_event(dtvcc, window->number, CCX_DTVCC_TV_EVENT_WINDOW_POSITON, buf);
	}
}

void dtvcc_handle_RST_Reset(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_service_decoder *decoder)
{
	ccx_dtvcc_windows_reset(decoder);
	ccx_dtvcc_callback_event(dtvcc, CCX_DTVCC_WINDOW_INIT, CCX_DTVCC_TV_EVENT_CLEAR_WINDOW, 0);
}

//------------------------- SYNCHRONIZATION COMMANDS -------------------------

void dtvcc_handle_DLY_Delay(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_service_decoder *decoder, int tenths_of_sec)
{
	ccx_dtvcc_log("[CEA-708] dtvcc_handle_DLY_Delay: "
			"delay for [%d] tenths of second", tenths_of_sec);

	dtvcc->delay_usec = tenths_of_sec*100*1000;
	if(dtvcc->screen_content_changed)
	{
		ccx_dtvcc_window *window = decoder->windows[decoder->current_window];
		if (window->is_defined && window->visible)
		{
			_dtvcc_window_copy_to_screen(dtvcc, decoder, window);
			dtvcc->screen_content_changed = 0;
		}
	}

}

void dtvcc_handle_DLC_DelayCancel(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_service_decoder *decoder)
{
	ccx_dtvcc_log("[CEA-708] dtvcc_handle_DLC_DelayCancel");
	dtvcc->delay_usec = 0;

}

//------------------------------- TRANSCODE ----------------------------------

/********************************************************
00-1F -> Characters that are in the G2 group in 20-3F,
         except for 06, which is used for the closed captions
         sign "CC" which is defined in group G3 as 00. (this
         is by the article 33).
20-7F -> Group G0 as is - corresponds to the ASCII code
80-9F -> Characters that are in the G2 group in 60-7F
         (there are several blank characters here, that's OK)
A0-FF -> Group G1 as is - non-English characters and symbols
*/

unsigned char dtvcc_get_internal_from_G0(unsigned char g0_char)
{
	return g0_char;
}

unsigned char dtvcc_get_internal_from_G1(unsigned char g1_char)
{
	if (g1_char == 0xA0) // The NBS (non-breaking space), so we return a blank space
		return 0x20;
	return g1_char;
}

// G2: Extended Control Code Set 1
unsigned char dtvcc_get_internal_from_G2(unsigned char g2_char)
{
	if (g2_char >= 0x20 && g2_char <= 0x3F)
		return g2_char - (unsigned char)0x20;
	if (g2_char >= 0x60 && g2_char <= 0x7F)
		return g2_char + (unsigned char)0x20;
	// Rest unmapped, so we return a blank space
	return 0x20;
}

// G3: Future Characters and Icon Expansion
unsigned char dtvcc_get_internal_from_G3(unsigned char g3_char)
{
	if (g3_char == 0xA0) // The "CC" (closed captions) sign
		return 0x06;
	// Rest unmapped, so we return a blank space
	return 0x20;
}


//-------------------------- CHARACTERS AND COMMANDS -------------------------
int _dtvcc_handle_unicode_check_routine(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_service_decoder *decoder, unsigned char *data)
{
	if(data[0] != 0x00){
		if( !(data[1]>0xa0 && data[1]<0xff))
			dtvcc->check_unicode_routine = 0;
		else
			dtvcc->check_character_cnt++;

		if(dtvcc->check_character_cnt > 5)
		{
			dtvcc->check_unicode_routine = 0;
			dtvcc->korean_code = 0;
			dtvcc->check_character_cnt = 0;
		}
	}
	return 3;
}

int _dtvcc_handle_C0_P16(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_service_decoder *decoder, unsigned char *data) //16-byte chars always have 2 bytes
{
	if (decoder->current_window == -1) {
		ccx_dtvcc_log("[CEA-708] _dtvcc_handle_C0_P16: Window has to be defined first\n");
		return 3;
	}

	ccx_dtvcc_symbol sym;

	if (data[0])
	{
		CCX_DTVCC_SYM_SET_16(sym, data[0], data[1]);
	}
	else
	{
		if(data[1] == 0x00){
			CCX_DTVCC_SYM_SET(sym, 0x20);
		}else{
			CCX_DTVCC_SYM_SET(sym, data[1]);
		}
	}
	ccx_dtvcc_log("[CEA-708] _dtvcc_handle_C0_P16: [%04X]\n", sym.sym);
	_dtvcc_process_character(dtvcc, decoder, sym);
	return 3;
}

// G0 - Code Set - ASCII printable characters
int _dtvcc_handle_G0(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_service_decoder *decoder, unsigned char *data, int data_length)
{
	if (decoder->current_window == -1)
	{
		ccx_dtvcc_log("[CEA-708] _dtvcc_handle_G0: Window has to be defined first\n");
		return data_length;
	}

	unsigned char c = data[0];
	ccx_dtvcc_log("[CEA-708] G0: [%02X]  (%c)\n", c, c);
	ccx_dtvcc_symbol sym;
	if (c == 0x7F) {	// musical note replaces the Delete command code in ASCII
		if(dtvcc->cc_descriptor.korean_code){
			CCX_DTVCC_SYM_SET(sym, CCX_DTVCC_MUSICAL_NOTE_UNICODE_CHAR);
		}else{
			CCX_DTVCC_SYM_SET(sym, CCX_DTVCC_MUSICAL_NOTE_EUCKR_CHAR);
		}
	} else {
		unsigned char uc = dtvcc_get_internal_from_G0(c);
		CCX_DTVCC_SYM_SET(sym, uc);
	}
	_dtvcc_process_character(dtvcc, decoder, sym);
	return 1;
}

// G1 Code Set - ISO 8859-1 LATIN-1 Character Set
int _dtvcc_handle_G1(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_service_decoder *decoder, unsigned char *data, int data_length)
{
	ccx_dtvcc_log("[CEA-708] G1: [%02X]  (%c)\n", data[0], data[0]);
	unsigned char c = dtvcc_get_internal_from_G1(data[0]);
	ccx_dtvcc_symbol sym;
	CCX_DTVCC_SYM_SET(sym, c);
	_dtvcc_process_character(dtvcc, decoder, sym);
	return 1;
}

int _dtvcc_handle_C0(ccx_dtvcc_ctx *dtvcc,
					 ccx_dtvcc_service_decoder *decoder,
					 unsigned char *data,
					 int data_length)
{
	unsigned char c0 = data[0];
	const char *name = DTVCC_COMMANDS_C0[c0];
	if (name == NULL)
		name = "Reserved";

	ccx_dtvcc_log("[CEA-708] C0: [%02X]  (%d)   [%s]\n", c0, data_length, name);

	int len = -1;
	// These commands have a known length even if they are reserved.
	if (c0 <= 0xF)
	{
		switch (c0)
		{
			case CCX_DTVCC_C0_NUL:
				// ASCII say it's "do nothing"
				break;
			case CCX_DTVCC_C0_CR:
				_dtvcc_process_cr(dtvcc, decoder);
				break;
			case CCX_DTVCC_C0_HCR:
				_dtvcc_process_hcr(decoder);
				break;
			case CCX_DTVCC_C0_FF:
				_dtvcc_process_ff(decoder);
				break;
			case CCX_DTVCC_C0_ETX:
				_dtvcc_process_etx(dtvcc, decoder);
				break;
			case CCX_DTVCC_C0_BS:
				_dtvcc_process_bs(decoder);
				break;
			default:
				ccx_dtvcc_log("[CEA-708] _dtvcc_handle_C0: unhandled branch\n");
				break;
		}
		len = 1;
	}
	else if (c0 >= 0x10 && c0 <= 0x17)
	{
		// Note that 0x10 is actually EXT1 and is dealt with somewhere else. Rest is undefined as per
		// CEA-708-D
		len = 2;
	}
	else if (c0 >= 0x18 && c0 <= 0x1F)
	{
		if (c0 == CCX_DTVCC_C0_P16) // PE16
		{
			if(dtvcc->check_unicode_routine){
				_dtvcc_handle_unicode_check_routine(dtvcc, decoder, data + 1);
			}else{
				_dtvcc_handle_C0_P16(dtvcc, decoder, data + 1);
				dtvcc->screen_content_changed = 1;
			}
		}
		len = 3;
	}
	if (len == -1)
	{
		ccx_dtvcc_log("[CEA-708] _dtvcc_handle_C0: impossible len == -1");
		return -1;
	}
	if (len > data_length)
	{
		ccx_dtvcc_log("[CEA-708] _dtvcc_handle_C0: "
				"command is %d bytes long but we only have %d\n", len, data_length);
		return -1;
	}
	return len;
}

// C1 Code Set - Captioning Commands Control Codes
int _dtvcc_handle_C1(ccx_dtvcc_ctx *dtvcc,
					 ccx_dtvcc_service_decoder *decoder,
					 unsigned char *data,
					 int data_length)
{
	struct CCX_DTVCC_S_COMMANDS_C1 com = DTVCC_COMMANDS_C1[data[0] - 0x80];

	ccx_dtvcc_log("[CEA-708] C1: [%02X]  [%s] [%s] (%d)\n",
			data[0], com.name, com.description, com.length);

	if (com.length > data_length)
	{
		ccx_dtvcc_log("[CEA-708] C1: Warning: Not enough bytes for command.\n");
		return -1;
	}

	switch (com.code)
	{
		case CCX_DTVCC_C1_CW0: /* SetCurrentWindow */
		case CCX_DTVCC_C1_CW1:
		case CCX_DTVCC_C1_CW2:
		case CCX_DTVCC_C1_CW3:
		case CCX_DTVCC_C1_CW4:
		case CCX_DTVCC_C1_CW5:
		case CCX_DTVCC_C1_CW6:
		case CCX_DTVCC_C1_CW7:
			dtvcc_handle_CWx_SetCurrentWindow(decoder, com.code - CCX_DTVCC_C1_CW0); /* Window 0 to 7 */
			break;
		case CCX_DTVCC_C1_CLW:
			dtvcc_handle_CLW_ClearWindows(dtvcc, decoder, data[1]);
			break;
		case CCX_DTVCC_C1_DSW:
			dtvcc_handle_DSW_DisplayWindows(dtvcc, decoder, data[1]);
			break;
		case CCX_DTVCC_C1_HDW:
			dtvcc_handle_HDW_HideWindows(dtvcc, decoder, data[1]);
			break;
		case CCX_DTVCC_C1_TGW:
			dtvcc_handle_TGW_ToggleWindows(dtvcc, decoder, data[1]);
			break;
		case CCX_DTVCC_C1_DLW:
			dtvcc_handle_DLW_DeleteWindows(dtvcc, decoder, data[1]);
			break;
		case CCX_DTVCC_C1_DLY:
			dtvcc_handle_DLY_Delay(dtvcc, decoder, data[1]);
			break;
		case CCX_DTVCC_C1_DLC:
			dtvcc_handle_DLC_DelayCancel(dtvcc, decoder);
			break;
		case CCX_DTVCC_C1_RST:
			dtvcc_handle_RST_Reset(dtvcc, decoder);
			break;
		case CCX_DTVCC_C1_SPA:
			dtvcc_handle_SPA_SetPenAttributes(dtvcc, decoder, data);
			break;
		case CCX_DTVCC_C1_SPC:
			dtvcc_handle_SPC_SetPenColor(dtvcc, decoder, data);
			break;
		case CCX_DTVCC_C1_SPL:
			dtvcc_handle_SPL_SetPenLocation(dtvcc, decoder, data);
			break;
		case CCX_DTVCC_C1_RSV93:
		case CCX_DTVCC_C1_RSV94:
		case CCX_DTVCC_C1_RSV95:
		case CCX_DTVCC_C1_RSV96:
			ccx_dtvcc_log("[CEA-708] Warning, found Reserved codes, ignored.\n");
			break;
		case CCX_DTVCC_C1_SWA:
			dtvcc_handle_SWA_SetWindowAttributes(dtvcc, decoder, data);
			break;
		case CCX_DTVCC_C1_DF0:
		case CCX_DTVCC_C1_DF1:
		case CCX_DTVCC_C1_DF2:
		case CCX_DTVCC_C1_DF3:
		case CCX_DTVCC_C1_DF4:
		case CCX_DTVCC_C1_DF5:
		case CCX_DTVCC_C1_DF6:
		case CCX_DTVCC_C1_DF7:
			dtvcc_handle_DFx_DefineWindow(dtvcc, decoder, com.code - CCX_DTVCC_C1_DF0, data); /* Window 0 to 7 */
			break;
		default:
			ccx_dtvcc_log("[CEA-708] BUG: Unhandled code in _dtvcc_handle_C1.\n");
			break;
	}

	return com.length;
}

// C2: Extended Miscellaneous Control Codes
int _dtvcc_handle_C2(unsigned char *data)
{
	if (data[0] <= 0x07) // 00-07...
		return 1; // ... Single-byte control bytes (0 additional bytes)
	else if (data[0] <= 0x0f) // 08-0F ...
		return 2; // ..two-byte control codes (1 additional byte)
	else if (data[0] <= 0x17)  // 10-17 ...
		return 3; // ..three-byte control codes (2 additional bytes)
	return 4; // 18-1F => four-byte control codes (3 additional bytes)
}

int _dtvcc_handle_C3(unsigned char *data)
{
	if (data[0] < 0x80 || data[0] > 0x9F)
		ccx_dtvcc_log("[CEA-708] Entry in _dtvcc_handle_C3 with an out of range value.");
	if (data[0] <= 0x87) // 80-87...
		return 5; // ... Five-byte control bytes (4 additional bytes)
	else if (data[0] <= 0x8F) // 88-8F ...
		return 6; // ..Six-byte control codes (5 additional byte)
	// If here, then 90-9F ...
	
	return 0;
}

// This function handles extended codes (EXT1 + code), from the extended sets
// G2 (20-7F) => Mostly unmapped, except for a few characters.
// G3 (A0-FF) => A0 is the CC symbol, everything else reserved for future expansion in EIA708-B
// C2 (00-1F) => Reserved for future extended misc. control and captions command codes
// Returns number of used bytes, usually 1 (since EXT1 is not counted).
int _dtvcc_handle_extended_char(ccx_dtvcc_ctx *dtvcc, ccx_dtvcc_service_decoder *decoder, unsigned char *data, int data_length)
{
	int used;
	ccx_dtvcc_log("[CEA-708] In _dtvcc_handle_extended_char, "
			"first data code: [%c], length: [%u]\n", data[0], data_length);
	unsigned char c = 0x20; // Default to space
	unsigned char code = data[0];
	if (/* data[i]>=0x00 && */ code <= 0x1F) // Comment to silence warning
	{
		used = _dtvcc_handle_C2(data);
	}
		// Group G2 - Extended Miscellaneous Characters
	else if (code >= 0x20 && code <= 0x7F)
	{
		c = dtvcc_get_internal_from_G2(code);
		used = 1;
		ccx_dtvcc_symbol sym;
		CCX_DTVCC_SYM_SET(sym, c);
		_dtvcc_process_character(dtvcc, decoder, sym);
	}
		// Group C3
	else if (code>= 0x80 && code <= 0x9F)
	{
		used = _dtvcc_handle_C3(data);
		// TODO: Something
	}
		// Group G3
	else
	{
		c = dtvcc_get_internal_from_G3(code);
		used = 1;
		ccx_dtvcc_symbol sym;
		CCX_DTVCC_SYM_SET(sym, c);
		_dtvcc_process_character(dtvcc, decoder, sym);
	}
	return used;
}

void _dtvcc_decoder_init(ccx_dtvcc_ctx *ctx, int index)
{
	ctx->decoders[index] = (ccx_dtvcc_service_decoder *) calloc(sizeof(char), sizeof(ccx_dtvcc_service_decoder));
	ccx_dtvcc_service_decoder *decoder = ctx->decoders[index];
	
	if(!decoder){ 
		ccx_dtvcc_log("[CEA-708] ccx_dtvcc_init not enough memory erro alloc decoder"); 		
		return;
	}
	
	decoder->tv = (dtvcc_tv_screen *) calloc(sizeof(char), sizeof(dtvcc_tv_screen));
	if (!decoder->tv){
		ccx_dtvcc_log("[CEA-708] ccx_dtvcc_init not enough memory erro alloc decoder->tv");
		return;
	}else{
		decoder->tv->service_number = index + 1;
	}
	
	for (int j = 0; j < CCX_DTVCC_MAX_WINDOWS; j++){
		decoder->windows[j] = (ccx_dtvcc_window *) calloc(sizeof(char), sizeof(ccx_dtvcc_window));
		ccx_dtvcc_window *window = decoder->windows[j];
		if(!window){ 
			ccx_dtvcc_log("[CEA-708] ccx_dtvcc_init not enough memory erro alloc window");		
			continue;
		}
		window->memory_reserved = 0;
	}
	ccx_dtvcc_windows_reset(decoder);
}

//------------------------------- PROCESSING --------------------------------

void ccx_dtvcc_section_filter_callback(const unsigned char *section, const unsigned int length, void *section_user_param)
{
#ifdef DEBUG_CC_PACKETS
	ccx_dtvcc_log("[CEA-708] ccx_dtvcc_section_filter_callback...section[%p], length[%d]\n", section, length);
//	ccx_dtvcc_log_dump((char*)section, length);
#endif

	dvb_player_t * player = (dvb_player_t*)section_user_param;
	ccx_dtvcc_ctx* ctx = player->dtvcc;
	ccx_dtvcc_post_section_filter_data(ctx, (void *)section, length, (void *)section_user_param);
}

void ccx_dsmcc_section_done_callback(int done_section, void *section_user_param)
{
	dvb_player_t * player = (dvb_player_t*)section_user_param;
	if(player == NULL) {
	    ccx_dtvcc_log("[CEA-708] section_user_param is null\n");
	    return;
	}

	if(player->dtvcc == NULL) {
	    ccx_dtvcc_log("[CEA-708] player->dtvcc is null\n");
	    return;
	}

	ccx_dtvcc_ctx* ctx = player->dtvcc;
	ccx_dtvcc_stop_dsmcc_section_filter(ctx);
	switch(done_section) {
	case TABLE_DSM_UN_TABLE_ID:
		ccx_dtvcc_request_dsmcc_data(ctx);	
		break;
	
	case TABLE_DSM_DL_TABLE_ID:
		break;
	}
}

void ccx_dsmcc_section_filter_callback(const unsigned char *section, const unsigned int length, void *section_user_param)
{
#ifdef DEBUG_CC_PACKETS
	ccx_dtvcc_log("[CEA-708] ccx_dsmcc_section_filter_callback...section[%p], length[%d]\n", section, length);
//	ccx_dtvcc_log_dump((char*)section, length);
#endif

	dvb_player_t * player = (dvb_player_t*)section_user_param;
//	ccx_dtvcc_ctx* ctx = player->dtvcc;
//	ccx_dtvcc_post_dsmcc_section_filter_data(ctx, (void *)section, length, (void *)userParam);

	//  direct passing to dsmmcc lib
	dsmcc_post_section_filter_data(player->dsmcc, (void *)section, length, 
		(void *)player, ccx_dsmcc_section_done_callback);
	
}

void ccx_dtvcc_request_pat_data(ccx_dtvcc_ctx *ctx)
{
	ccx_dtvcc_log("[CEA-708] ccx_dtvcc_request_pat_data..");	
	dvb_player_t* player = (dvb_player_t*)ctx->handle;

	// set Section Filter
	// PAT section filter ==> PAT table id : 0x00
	dvb_set_section_filter(player, 0, TABLE_PAT_TABLE_ID, ccx_dtvcc_section_filter_callback);
	ctx->on_request_section_filter = 1;	
}


void ccx_dtvcc_request_pmt_data(ccx_dtvcc_ctx *ctx)
{
	if(!ctx->pid) return;
	
	ccx_dtvcc_log("[CEA-708] ccx_dtvcc_request_pmt_data..");	
	dvb_player_t* player = (dvb_player_t*)ctx->handle;

	if(ctx->on_request_section_filter){
		ccx_dtvcc_log("[CEA-708] on_request_wait_pmt_data..");			
		return;
	}

	_dtvcc_free_dsmcc_stream(ctx);
#ifdef DTVCC_TTA_CERT_MODE	
    if(ctx->kec_mode == 1)
    {
	    _dtvcc_free_audio_descriptor(ctx);
	}
#endif
	// PMT section filter ==> PMT table id : 0x02
	dvb_set_section_filter(player, ctx->pid, TABLE_PMT_TABLE_ID, ccx_dtvcc_section_filter_callback);
	ctx->on_request_section_filter = 1;
}

void ccx_dtvcc_request_ait_data(ccx_dtvcc_ctx *ctx, unsigned short pid)
{
	ccx_dtvcc_log("[CEA-708] ccx_dtvcc_request_ait_data.. pid(0x%02X)", pid);	
	dvb_player_t* player = (dvb_player_t*)ctx->handle;

	// set Section Filter
	// AIT section filter ==> AIT table id : 0x74
	dvb_set_section_filter(player, pid, TABLE_AIT_TABLE_ID, ccx_dtvcc_section_filter_callback);
	ctx->on_request_section_filter = 1;	
}

void ccx_dtvcc_stop_section_filter(ccx_dtvcc_ctx *ctx)
{
	if(ctx->on_request_section_filter){
		ccx_dtvcc_log("[CEA-708] ccx_dtvcc_stop_section_filter.."); 
		dvb_player_t* player = (dvb_player_t*)ctx->handle;
		dvb_clear_section_filter(player);
		ctx->on_request_section_filter = 0;
	}
}

void ccx_dtvcc_request_dsmcc_indication(ccx_dtvcc_ctx *ctx)
{
	ccx_dtvcc_log("[CEA-708] ccx_dtvcc_request_dsmcc_indication..");	
	dvb_player_t* player = (dvb_player_t*)ctx->handle;

	// DSMCC section filter ==> DSMCC Indication table id : 0x3B
	dvb_set_dsmcc_section_filter(player, ctx->dsmcc_pid, TABLE_DSM_UN_TABLE_ID, ccx_dsmcc_section_filter_callback);
	ctx->on_dsmcc_request_section_filter = 1;	
}

void ccx_dtvcc_request_dsmcc_data(ccx_dtvcc_ctx *ctx)
{
	ccx_dtvcc_log("[CEA-708] ccx_dtvcc_request_dsmcc_data..");	
	dvb_player_t* player = (dvb_player_t*)ctx->handle;

	// DSMCC section filter ==> DSMCC Data table id : 0x3C
	dvb_set_dsmcc_section_filter(player, ctx->dsmcc_pid, TABLE_DSM_DL_TABLE_ID, ccx_dsmcc_section_filter_callback);
	ctx->on_dsmcc_request_section_filter = 1;
}

void ccx_dtvcc_stop_dsmcc_section_filter(ccx_dtvcc_ctx *ctx)
{
	if(ctx->on_dsmcc_request_section_filter){
		ccx_dtvcc_log("[CEA-708] ccx_dtvcc_stop_dsmcc_section_filter..");	
		dvb_player_t* player = (dvb_player_t*)ctx->handle;
		dvb_clear_dsmcc_section_filter(player);
		ctx->on_dsmcc_request_section_filter = 0;	
	}
}

void ccx_dtvcc_check_audio_time(ccx_dtvcc_ctx *ctx)
{
	unsigned int now, elapsed;
	
	now = _dtvcc_sys_clock();
	elapsed = (now - ctx->time_ms_prev);
	ctx->time_ms_prev = now;

	if(!ctx->is_audio_active) return; 
	
	if (elapsed >= CCX_DTVCC_CHECK_TIME_MS) {
		ccx_dtvcc_request_pmt_data(ctx);		
	}
}

void ccx_dtvcc_timer_handler( int sig, siginfo_t *siginfo, void *context )
{
	ccx_dtvcc_ctx *ctx;
	ctx = siginfo->si_value.sival_ptr;

	if(ctx->expired_action == CCX_DTVCC_EXPIRED_ACTION_CLEAR_WINDOW){
		ccx_dtvcc_log("[CEA-708] timer_expired clear window \n"); 	
		ccx_dtvcc_callback_event(ctx, CCX_DTVCC_WINDOW_INIT, CCX_DTVCC_TV_EVENT_CLEAR_WINDOW, 0);
		_dtvcc_decoders_reset(ctx); 		
	}else if(ctx->expired_action == CCX_DTVCC_EXPIRED_ACTION_DSMCC_REQUEST){
		ccx_dtvcc_log("[CEA-708] timer_expired dsmcc_request \n");	
		ctx->dsmcc_pid = 0;
		ccx_dtvcc_request_pmt_data(ctx);
	}else if(ctx->expired_action == CCX_DTVCC_EXPIRED_ACTION_KEC_PMT_REQUEST){
		ccx_dtvcc_log("[CEA-708] timer_expired pmt_request \n");	
		ccx_dtvcc_request_pmt_data(ctx);
	}else if(ctx->expired_action == CCX_DTVCC_EXPIRED_ACTION_AUDIO_CHECK_REQUEST){
		ccx_dtvcc_log("[CEA-708] timer_expired audio_check_request \n");	
		ccx_dtvcc_tta_check_audio_setting_active(ctx);
		ccx_dtvcc_set_timer(ctx, CCX_DTVCC_EXPIRED_ACTION_AUDIO_CHECK_REQUEST);
	}
	

}

void ccx_dtvcc_set_timer(ccx_dtvcc_ctx *ctx, int action)
{
	struct itimerspec its;

	if(!ctx->timer_id) return;

	ctx->expired_action = action;

	its.it_interval.tv_sec = 0;
	its.it_interval.tv_nsec = 0L;
	if(action == CCX_DTVCC_EXPIRED_ACTION_CLEAR_WINDOW){
		its.it_value.tv_sec = CCX_DTVCC_TIME_OUT_SEC;
	}else if(action == CCX_DTVCC_EXPIRED_ACTION_DSMCC_REQUEST){
		its.it_value.tv_sec = CCX_DSMCC_TIME_OUT_SEC;
	}else if(action == CCX_DTVCC_EXPIRED_ACTION_KEC_PMT_REQUEST){
		its.it_value.tv_sec = CCX_DTVCC_TIME_OUT_1_SEC;
	}else if(action == CCX_DTVCC_EXPIRED_ACTION_AUDIO_CHECK_REQUEST){
		its.it_value.tv_sec = CCX_DTVCC_TIME_OUT_1_SEC;
	}
	its.it_value.tv_nsec = 0L;
	timer_settime(ctx->timer_id, 0, &its, NULL);
}

void ccx_dtvcc_unset_timer(ccx_dtvcc_ctx *ctx)
{
	struct itimerspec its;
	
	if(!ctx->timer_id) return;

	its.it_interval.tv_sec = 0;
	its.it_interval.tv_nsec = 0L;
	its.it_value.tv_sec = 0;
	its.it_value.tv_nsec = 0L;
	timer_settime(ctx->timer_id, 0, &its, NULL);
}

void ccx_dtvcc_create_timer(ccx_dtvcc_ctx *ctx)
{
	struct sigevent  te;
	struct sigaction sa;
	int              sig_no = SIGRTMIN;

	/* Set up signal handler. */
	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = ccx_dtvcc_timer_handler;
	sigemptyset(&sa.sa_mask);
	if (sigaction(sig_no, &sa, NULL) == -1)
	{
		ccx_dtvcc_log("[CEA-708] ccx_dtvcc_create_timer failed to setup signal handling");
		return;
	}
	/* Set and enable alarm */
	te.sigev_notify = SIGEV_SIGNAL;
	te.sigev_signo = sig_no;
	te.sigev_value.sival_ptr = ctx;
	timer_create(CLOCK_REALTIME, &te, &ctx->timer_id);

	ccx_dtvcc_log("[CEA-708] ccx_dtvcc_create_timer success %d\n", ctx->timer_id);
}

void ccx_dtvcc_remove_timer(ccx_dtvcc_ctx *ctx)
{
	if(!ctx->timer_id) return;

	ccx_dtvcc_unset_timer(ctx);
	timer_delete(ctx->timer_id);
	ctx->timer_id = 0;
}

void ccx_dtvcc_setting_korean_code(ccx_dtvcc_ctx *ctx)
{
	if (ctx->cd != (iconv_t) -1) return;

	char charset[20]= {0,};	
	sprintf(charset, "%s", "EUC-KR");	
	
	ccx_dtvcc_log("[CEA-708] iconv_open for charset \"%s\"\n", charset);

	ctx->cd = iconv_open("UTF-8", charset);
	if (ctx->cd == (iconv_t) -1)
	{
		ccx_dtvcc_log("[CEA-708] dtvcc_init: "
											 "can't create iconv for charset \"%s\": %s\n",
									 charset, strerror(errno));
	}
	ccx_dtvcc_log("[CEA-708] iconv_open success %d\n", ctx->cd);
}

void ccx_dtvcc_get_tta_caption_enable(ccx_dtvcc_ctx *ctx) {
	int is_active = 0;
    char retBuf[128] = {0};

    if(get_systemproperty(PROPERTY_NAME_HEAR, retBuf, 128) == 0) {
        is_active = atoi(retBuf);
    }

	if(is_active == 1) {
		ctx->is_active = 1;		
	}else{
		if(ctx->is_active == 1){
			ccx_dtvcc_log("[CEA-708] ccx_dtvcc_get_tta_caption_enable set 0 \n");				
			ccx_dtvcc_unset_timer(ctx);
			ccx_dtvcc_callback_event(ctx, CCX_DTVCC_WINDOW_INIT, CCX_DTVCC_TV_EVENT_CLEAR_WINDOW, 0);
			ccx_dtvcc_log("[CEA-708] clear window \n");		
			_dtvcc_decoders_reset(ctx);
		}
		ctx->is_active = 0;		
	}
}

void ccx_dtvcc_get_tta_caption_mode(ccx_dtvcc_ctx *ctx) {
	int active_service_num = 1;
    char retBuf[128] = {0};

    if(get_systemproperty(PROPERTY_NAME_CAPTION, retBuf, 128) == 0) {
        active_service_num = atoi(retBuf);
    }

	if(active_service_num != ctx->active_service_num){
		ccx_dtvcc_log("[CEA-708] ccx_dtvcc_get_tta_caption_mode different \n");				
		ccx_dtvcc_unset_timer(ctx);
		ccx_dtvcc_callback_event(ctx, CCX_DTVCC_WINDOW_INIT, CCX_DTVCC_TV_EVENT_CLEAR_WINDOW, 0);
		ccx_dtvcc_log("[CEA-708] clear window \n"); 	
		_dtvcc_decoders_reset(ctx);
	}
	ctx->active_service_num = active_service_num;
}

void ccx_dtvcc_get_tta_vision_inpaired_enable(ccx_dtvcc_ctx *ctx) {
	int is_audio_active = -1;
    char retBuf[128] = {0};

    if(get_systemproperty(PROPERTY_NAME_EYE, retBuf, 128) == 0) {
        is_audio_active = atoi(retBuf);
    }

	if(!ctx->initial_internal){
		ctx->is_audio_active = is_audio_active;
		return;
	}

	if(is_audio_active == 1) {
		if(ctx->is_audio_active == 0 && !ctx->stop_requested){
			ccx_dtvcc_log("[CEA-708] ccx_dtvcc_get_tta_vision_inpaired_enable set 1 \n");
			ctx->is_audio_active = 1;
			_dtvcc_tta_audio_descriptor_handle(ctx);
 		}
	}else{
		if(ctx->is_audio_active == 1){
			ccx_dtvcc_log("[CEA-708] ccx_dtvcc_get_tta_vision_inpaired_enable set 0 \n");
			ctx->is_audio_active = 0;		
			_dtvcc_tta_audio_descriptor_handle(ctx);
 		}
	}
}

void ccx_dtvcc_get_tta_vi_language_mode(ccx_dtvcc_ctx *ctx) {
	int audio_language = 0;
    char retBuf[128] = {0};

    if(get_systemproperty(PROPERTY_NAME_LANGUAGE, retBuf, 128) == 0) {
        audio_language = atoi(retBuf);
    }

	if(!ctx->initial_internal){
		ctx->audio_language = audio_language;
		return;
	}
	
	if(audio_language == 1) {
		if(ctx->audio_language == 0 && !ctx->stop_requested){
			ccx_dtvcc_log("[CEA-708] ccx_dtvcc_get_tta_vi_language_mode set 1 \n");
			ctx->audio_language = 1;
			_dtvcc_tta_audio_descriptor_handle(ctx);
 		}
	}else{
		if(ctx->audio_language == 1){
			ccx_dtvcc_log("[CEA-708] ccx_dtvcc_get_tta_vi_language_mode set 0 \n");
			ctx->audio_language = 0;		
			_dtvcc_tta_audio_descriptor_handle(ctx);
 		}
	}
}

void ccx_dtvcc_tta_check_audio_setting_active(ccx_dtvcc_ctx *ctx)
{
	ccx_dtvcc_get_tta_vision_inpaired_enable(ctx);
	ccx_dtvcc_get_tta_vi_language_mode(ctx);
}

void ccx_dtvcc_tta_check_caption_active(ccx_dtvcc_ctx *ctx)
{
	unsigned int now, elapsed;
	
	now = _dtvcc_sys_clock();
	elapsed = (now - ctx->time_ms_check);
	
	if (elapsed <= CCX_DTVCC_ACTIVCE_CHECK_TIME_MS) {
		return;		
	}
	
	ctx->time_ms_check = now;

	ccx_dtvcc_get_tta_caption_enable(ctx);
	ccx_dtvcc_get_tta_caption_mode(ctx);
}


void ccx_dtvcc_check_setting_active(ccx_dtvcc_ctx *dtvcc)
{
	unsigned int now, elapsed;
	
	now = _dtvcc_sys_clock();
	elapsed = (now - dtvcc->time_ms_check);
	
	if (elapsed <= CCX_DTVCC_ACTIVCE_CHECK_TIME_MS) {
		return;		
	}
	
	dtvcc->time_ms_check = now;

	char value[20];
	get_systemproperty(PROPERTY__HEARING_IMPAIRED, value, 20);
	if(strcmp(value, "true") == 0) {
		dtvcc->is_active = 1;		
	}else{
		if(dtvcc->is_active == 1){
			ccx_dtvcc_log("[CEA-708] ccx_dtvcc_check_setting_active set 0 \n");				
			ccx_dtvcc_unset_timer(dtvcc);
			ccx_dtvcc_callback_event(dtvcc, CCX_DTVCC_WINDOW_INIT, CCX_DTVCC_TV_EVENT_CLEAR_WINDOW, 0);
			ccx_dtvcc_log("[CEA-708] clear window \n");		
			_dtvcc_decoders_reset(dtvcc);
		}
		dtvcc->is_active = 0;		
	}

	get_systemproperty(PROPERTY__VISION_IMPAIRED, value, 20);
	if(strcmp(value, "true") == 0) {
		if(dtvcc->is_audio_active == 0 && !dtvcc->stop_requested){
			ccx_dtvcc_log("[CEA-708] ccx_dtvcc_check_setting_audio_active set 1 \n");
			ccx_dtvcc_request_pmt_data(dtvcc);			
 		}
		dtvcc->is_audio_active = 1;
	}else{
		if(dtvcc->is_audio_active == 1){
			ccx_dtvcc_log("[CEA-708] ccx_dtvcc_check_setting_audio_active set 0 \n");
			_dtvcc_request_main_audio_play(dtvcc);
 		}
		dtvcc->is_audio_active = 0;		
	}
}

void ccx_dtvcc_process_service_block(ccx_dtvcc_ctx *dtvcc,
									 ccx_dtvcc_service_decoder *decoder,
									 unsigned char *data,
									 int data_length)
{
#ifdef DEBUG_CC_PACKETS
	ccx_dtvcc_log("[CEA-708] ccx_dtvcc_process_service_block: block data\n");
	ccx_dtvcc_log_dump((char*) data, data_length);
#endif
	int i = 0;
	dtvcc->screen_content_changed = 0;
	while (i < data_length)
	{
		int used = -1;
		if (data[i] != CCX_DTVCC_C0_EXT1)
		{
			if (data[i] <= 0x1F){
				used = _dtvcc_handle_C0(dtvcc, decoder, data + i, data_length - i);
			}else if (data[i] >= 0x20 && data[i] <= 0x7F){
				used = _dtvcc_handle_G0(dtvcc, decoder, data + i, data_length - i);
				dtvcc->screen_content_changed = 1;
			}else if (data[i] >= 0x80 && data[i] <= 0x9F){
				used = _dtvcc_handle_C1(dtvcc, decoder, data + i, data_length - i);
			}else{
				used = _dtvcc_handle_G1(dtvcc, decoder, data + i, data_length - i);
				dtvcc->screen_content_changed = 1;
			}

			if (used == -1)
			{
				ccx_dtvcc_log("[CEA-708] ccx_dtvcc_process_service_block: "
						"There was a problem handling the data. Reseting service decoder\n");
				// TODO: Not sure if a local reset is going to be helpful here.
				//ccx_dtvcc_windows_reset(decoder);
				return;
			}
		}
		else // Use extended set
		{
			used = _dtvcc_handle_extended_char(dtvcc, decoder, data + i + 1, data_length - 1);
			used++; // Since we had CCX_DTVCC_C0_EXT1
			dtvcc->screen_content_changed = 1;
		}
		i += used;
	}

	if(dtvcc->delay_usec){
		usleep(dtvcc->delay_usec);
		dtvcc->delay_usec = 0;
	}

	if (decoder->current_window != -1){
		if(dtvcc->screen_content_changed)
		{
			ccx_dtvcc_window *window = decoder->windows[decoder->current_window];
			if (window->is_defined && window->visible)
			{
				_dtvcc_window_copy_to_screen(dtvcc, decoder, window);
			}
		}
	}

}


void ccx_dtvcc_process_current_packet(ccx_dtvcc_ctx *dtvcc, int cc_type)
{
	if (dtvcc->current_packet_length == 0)
		return;

	/* packet header */
	int seq = (dtvcc->current_packet[0] & 0xC0) >> 6; // Two most significants bits
	int len = dtvcc->current_packet[0] & 0x3F; // 6 least significants bits

	/* packet data sequence */	
	if (len == 0) // This is well defined in TTAK.KO-07.0093R1;
		len = CCX_DTVCC_MAX_PACKET_LENGTH;
	else
		len = (len * 2);

	if(dtvcc->current_packet_length < len)
	{ 
		if(cc_type == CCX_DTVCC_PACKET_START)
			ccx_dtvcc_clear_packet(dtvcc);
		return;
	}

	// Note that len here is the length including the header
#ifdef DEBUG_CC_PACKETS
	ccx_dtvcc_log("[CEA-708] dtvcc_process_current_packet: "
									   "Sequence: %d, packet length: %d\n", seq, len);
#endif

	if (dtvcc->last_sequence != CCX_DTVCC_NO_LAST_SEQUENCE &&
			(dtvcc->last_sequence + 1) % 4 != seq)
	{
		ccx_dtvcc_log("[CEA-708] ccx_dtvcc_process_current_packet: "
											 "Unexpected sequence number, it is [%d] but should be [%d]\n",
				seq, (dtvcc->last_sequence + 1 ) % 4);
		//WARN: if we reset decoders here, buffer will not be written
		//WARN: resetting decoders breaks some samples
		//_dtvcc_decoders_reset(dtvcc);
		//return;
	}
	
	dtvcc->last_sequence = seq;

	unsigned char *pos = dtvcc->current_packet + 1;

	while (pos < dtvcc->current_packet + len)
	{
		int service_number = (pos[0] & 0xE0) >> 5; // 3 more significant bits
		int block_length = (pos[0] & 0x1F); // 5 less significant bits

		if (service_number == 7) // There is an extended header
		{
			pos++;
			service_number = (pos[0] & 0x3F); // 6 more significant bits

			ccx_dtvcc_log("[CEA-708] ccx_dtvcc_process_current_packet: Extended header: "
							"Service number: [%d]\n", service_number);
		}

		if (service_number == 0 && block_length == 0) // Null Service Header
		{
//			ccx_dtvcc_log("[CEA-708] dtvcc_process_current_packet: Null Service Header\n");		
		}

		pos++; // Move to service data
		if (service_number == 0 && block_length != 0) // Illegal, but specs say what to do...
		{
			ccx_dtvcc_log("[CEA-708] ccx_dtvcc_process_current_packet: "
					"Data received for service 0, skipping rest of packet.");
			pos = dtvcc->current_packet + len; // Move to end
			break;
		}

		if (block_length != 0)
		{
			ccx_dtvcc_log("[CEA-708] ccx_dtvcc_process_current_packet: "
							"Service number: [%d] Block length: [%d]\n", service_number, block_length);
		
			if (service_number > 0){

				if(dtvcc->active_service_num != service_number){
					ccx_dtvcc_log("[CEA-708] ccx_dtvcc_process_current_packet: "
									"Service number: [%d] active_service_num: [%d]\n", service_number, dtvcc->active_service_num);
					ccx_dtvcc_log("[CEA-708] ccx_dtvcc_process_current_packet: "
							"If it is not a active service, skip it,-> skipping rest of packet.");

					pos = dtvcc->current_packet + len; // Move to end => only active_service_num is handled
					break;
				}

				ccx_dtvcc_set_timer(dtvcc, CCX_DTVCC_EXPIRED_ACTION_CLEAR_WINDOW);

				if(!dtvcc->decoders[service_number - 1]){
					_dtvcc_decoder_init(dtvcc, service_number - 1);
				}
				ccx_dtvcc_service_decoder *decoder = dtvcc->decoders[service_number - 1];
				if(decoder){
					ccx_dtvcc_process_service_block(dtvcc, decoder, pos, block_length);
				}
			}
		}

		pos += block_length; // Skip data
	}

	ccx_dtvcc_clear_packet(dtvcc);

	if (pos != dtvcc->current_packet + len) // For some reason we didn't parse the whole packet
	{
		ccx_dtvcc_log("[CEA-708] ccx_dtvcc_process_current_packet:"
				" There was a problem with this packet, reseting\n");
//		_dtvcc_decoders_reset(dtvcc);
	}

	if (len < 128 && *pos) // Null header is mandatory if there is room
	{
		ccx_dtvcc_log("[CEA-708] ccx_dtvcc_process_current_packet: "
				"Warning: Null header expected but not found.\n");
	}
}

void ccx_dtvcc_process_data(ccx_dtvcc_ctx *dtvcc, 
							const unsigned char *data,
							int data_length)
{
	/*
	 * Note: the data has following format:
	 * 1 byte for cc_valid
	 * 1 byte for cc_type
	 * 2 bytes for the actual data
	 */
	 
	for (int i = 0; i < data_length; i += 4)
	{
		unsigned char cc_valid = data[i];
		unsigned char cc_type = data[i + 1];

		switch (cc_type)
		{
			case CCX_DTVCC_PACKET_DATA:
				if (!dtvcc->is_active) return;
				if (cc_valid == 0) // This ends the previous packet
				{
					ccx_dtvcc_process_current_packet(dtvcc, CCX_DTVCC_PACKET_DATA);
#ifdef DTVCC_TTA_CERT_MODE
                    if(dtvcc->kec_mode == 1)
                    {
					    ccx_dtvcc_tta_check_caption_active(dtvcc);
					}
					else
					{
					    ccx_dtvcc_check_setting_active(dtvcc);					
					}
#else
					ccx_dtvcc_check_setting_active(dtvcc);					
#endif
				}
				else
				{
					if (dtvcc->current_packet_length > CCX_DTVCC_MAX_PACKET_LENGTH - 1)
					{
						ccx_dtvcc_log("[CEA-708] ccx_dtvcc_process_data: "
								"Warning: Legal packet size exceeded, data not added.\n");
					}
					else
					{
						if(dtvcc->start_packet_recved){
							dtvcc->current_packet[dtvcc->current_packet_length++] = data[i + 2];
							dtvcc->current_packet[dtvcc->current_packet_length++] = data[i + 3];
						}
					}
				}
				break;
				
			case CCX_DTVCC_PACKET_START:
				if (cc_valid)
				{
#ifdef DTVCC_TTA_CERT_MODE
                    if(dtvcc->kec_mode == 1)
                    {
					    ccx_dtvcc_tta_check_caption_active(dtvcc);
					}
					else
					{
    					ccx_dtvcc_check_setting_active(dtvcc);
    					ccx_dtvcc_check_audio_time(dtvcc);
					}
#else
					ccx_dtvcc_check_setting_active(dtvcc);
					ccx_dtvcc_check_audio_time(dtvcc);
#endif
					if (!dtvcc->is_active) return;
					ccx_dtvcc_process_current_packet(dtvcc, CCX_DTVCC_PACKET_START);

					dtvcc->start_packet_recved = 1;
					dtvcc->current_packet[dtvcc->current_packet_length++] = data[i + 2];
					dtvcc->current_packet[dtvcc->current_packet_length++] = data[i + 3];
				}
				break;
			default:
				ccx_dtvcc_log("[CEA-708] dtvcc_process_data: "
						"shouldn't be here - cc_type: %d\n", cc_type);
		}
	}
}

unsigned char * ccx_dtvcc_find_data(unsigned char * ccdp_atom_content, unsigned int *len, unsigned int *cc_count)
{
	unsigned char *data = ccdp_atom_content;
	unsigned char  process_cc_data_flag = 0;

	process_cc_data_flag = (unsigned char)((data[0] & 0x40)>>6);	
	*cc_count = (unsigned int) (data[0] & 0x1F);

//	ccx_dtvcc_log("[CEA-708] ccx_dtvcc_find_data: "
//					   "process_cc_data_flag: %d, cc_count: %d\n", process_cc_data_flag, *cc_count);

	data += sizeof(unsigned char)*2;  // reserved + process_cc_data_flag + zero_bit + cc_count + reserved
	*len = (2 + ((*cc_count) * 3)) + 1; // cc_data + marker_bits

	if (process_cc_data_flag != 0x01)
	{
		ccx_dtvcc_log("[CEA-708] ccx_dtvcc_find_data : process_cc_data_flag not 1 ==> don't handling\n");
		return NULL;
	}

	return data;
}

//--------------------------------------------------------------------------------------

void ccx_dtvcc_process_picture_data(ccx_dtvcc_ctx *ctx, unsigned char * data, unsigned int data_size)
{
	unsigned int 	atom_start = 0;
	unsigned int 	mpeg_offset = 0;
	unsigned int 	h264_offset = 0;
				
	unsigned int	user_data_start_code = 0;
	unsigned int	user_identifier = 0;
	unsigned char	user_data_type_code = 0;
				
	unsigned char	itu_t_t35_country_code = 0;
	unsigned short	itu_t_t35_provider_code = 0;
	unsigned int	ATSC_user_identifier = 0;

	while (atom_start < data_size)
	{
		unsigned int user_data_length = 0;
		unsigned char *buffer = data + atom_start;
					
		mpeg_offset = 0;
		memcpy((void*)&user_identifier, (void*)&(buffer[mpeg_offset]), sizeof(unsigned int));
		user_identifier = CCX_SWAP_32(user_identifier);
		mpeg_offset +=  sizeof(unsigned int);
		
#if 0
		memcpy((void*)&user_data_start_code, (void*)&(buffer[mpeg_offset]), sizeof(unsigned int));
		user_data_start_code = CCX_SWAP_32(user_data_start_code);
		mpeg_offset +=  sizeof(unsigned int);
					
		memcpy((void*)&user_identifier, (void*)&(buffer[mpeg_offset]), sizeof(unsigned int));
		user_identifier = CCX_SWAP_32(user_identifier);
		mpeg_offset +=  sizeof(unsigned int);

		h264_offset = 0;
		memcpy((void*)&itu_t_t35_country_code, (void*)&(buffer[h264_offset]), sizeof(unsigned char));
		h264_offset +=  sizeof(unsigned char);

		memcpy((void*)&itu_t_t35_provider_code, (void*)&(buffer[h264_offset]), sizeof(unsigned short));
		itu_t_t35_provider_code = CCX_SWAP_16(itu_t_t35_provider_code);
		h264_offset +=  sizeof(unsigned short);				
					
		memcpy((void*)&ATSC_user_identifier, (void*)&(buffer[h264_offset]), sizeof(unsigned int));
		ATSC_user_identifier = CCX_SWAP_32(ATSC_user_identifier);				
		h264_offset +=  sizeof(unsigned int);

//		ccx_dtvcc_log("[CEA-708] AVP_PICTURE_USER_DATA: user_data_start_code=0x%08x, user_identifier=0x%08x, itu_t_t35_country_code=0x%02x, itu_t_t35_provider_code=0x%04x, ATSC_user_identifier=0x%08x\n",
//			user_data_start_code, user_identifier, itu_t_t35_country_code, itu_t_t35_provider_code, ATSC_user_identifier);

		// MPEG -2 CC Data
		if((CCX_USERDATA_STARTCODE_MPEG2 == user_data_start_code)
			&&(CCX_USER_IDENTIFIER == user_identifier))
		{
//			ccx_dtvcc_log("[CEA-708] AVP_PICTURE_USER_DATA : MPEG -2 CC Data!!!...\n");
			memcpy((void*)&user_data_type_code, (void*)&(buffer[mpeg_offset]), sizeof(unsigned char));
			mpeg_offset +=  sizeof(unsigned char);
			atom_start += mpeg_offset;
			buffer += mpeg_offset;

		}
		// AVC/H.264 CC Data
		else if((CCX_ITU_T35_CONTRY_CODE == itu_t_t35_country_code)
			&&(CCX_ITU_T35_PROVIDER_CODE == itu_t_t35_provider_code)
			&&(CCX_USER_IDENTIFIER == ATSC_user_identifier))
		{
//			ccx_dtvcc_log("[CEA-708] AVP_PICTURE_USER_DATA :AVC/H.264 CC Data!!!...\n");
			memcpy((void*)&user_data_type_code, (void*)&(buffer[h264_offset]), sizeof(unsigned char));	
			h264_offset +=  sizeof(unsigned char);						
			atom_start += h264_offset;						
			buffer += h264_offset;						
		}
#endif		
		if(CCX_USER_IDENTIFIER == user_identifier)
		{
//			ccx_dtvcc_log("[CEA-708] AVP_PICTURE_USER_DATA : MPEG -2 CC Data!!!...\n");
			memcpy((void*)&user_data_type_code, (void*)&(buffer[mpeg_offset]), sizeof(unsigned char));
			mpeg_offset +=	sizeof(unsigned char);
			atom_start += mpeg_offset;
			buffer += mpeg_offset;
		}
		else
		{
			ccx_dtvcc_log("[CEA-708] AVP_PICTURE_USER_DATA skipped invalid data!!!...\n");
			break;
		}

		if(CCX_USERDATA_TYPE_CODE != user_data_type_code){
			ccx_dtvcc_log("[CEA-708] AVP_PICTURE_USER_DATA cc not data!!!...\n");
			break;
		}

		unsigned int cc_count;
		unsigned char *cc_data = ccx_dtvcc_find_data((unsigned char *) buffer, &user_data_length, &cc_count);					
					
		atom_start += user_data_length;
		if (!cc_data)
		{
			ccx_dtvcc_log("[CEA-708] AVP_PICTURE_USER_DATA: cc data skipped\n");
			continue;
		}
#if 0 //yiwoo dump picture raw data
{
  unsigned char temp_buf[512];
  int len;
  ccx_dtvcc_log("==========================");
  ccx_dtvcc_log("data_size : %d ",data_size);
  memset(temp_buf, 0x00, sizeof(temp_buf));
  for(unsigned char j=0;j<data_size;j++)
  {
    sprintf(temp_buf + (j*3), "%02x ",data[j]);
  }
  ccx_dtvcc_log("yiwoo %s",temp_buf);
  ccx_dtvcc_log("==========================");
}
#endif
		unsigned char temp[4];
		for (unsigned int cc_i = 0; cc_i < cc_count; cc_i++, cc_data += 3)
		{
			unsigned char cc_info = cc_data[0];
			unsigned char cc_valid = (unsigned char) ((cc_info & 4) >> 2);
			unsigned char cc_type = (unsigned char) (cc_info & 3);
					
			temp[0] = cc_valid;
			temp[1] = cc_type;
			temp[2] = cc_data[1];
			temp[3] = cc_data[2];

//			ccx_dtvcc_log("[CEA-708] cc_valid = 0x%02x, cc_type =0x%02x, cc_data1 = 0x%02x, cc_data2 = 0x%02x \n", 
//				cc_valid, cc_type, cc_data[1], cc_data[2]);
			if (cc_type < CCX_DTVCC_PACKET_DATA)
			{
//				ccx_dtvcc_log("[CEA-708] cc skipped (cc_type < CC_DTVCC_PACKET_DATA)\n");
				continue;
			}
			
			ccx_dtvcc_process_data(ctx, (unsigned char *) temp, 4);
			
		}
	}
}


void ccx_dtvcc_process_section_filter_data(ccx_dtvcc_ctx *ctx, ccx_section_data *section)
{
	unsigned char* section_data = ((unsigned char*)section->section_data);
	unsigned int section_data_size = section->section_data_size;
	dvb_player_t* player = (dvb_player_t*)section->section_user_param;
	
	ccx_dtvcc_log("[SECTION] ccx_dtvcc_process_section_filter_data\n");
	unsigned char table_id = section_data[0];	
	
	dvb_clear_section_filter(player);
	ctx->on_request_section_filter = 0;

	switch( table_id ) {
		case TABLE_PAT_TABLE_ID:
			{
				int program_count, i, rc;
				TS_PSI_header header;
				TS_PAT_program program;
				_dtvcc_get_section_header(section_data, &header);
				_dtvcc_pat_data_dump(&header);
				program_count = _dtvcc_pat_get_program_count(section_data);
				ccx_dtvcc_log( "[SECTION] program count = %d\n", program_count);			
				for (i = 0; i < program_count; i++)
				{
					rc = _dtvcc_pat_get_program(section_data, section_data_size, i,	&program);
					if (!rc && program.program_number) {
						ccx_dtvcc_log( "[SECTION]#------------------------------------------#\n");				
						ccx_dtvcc_log( "[SECTION]\tprogram_number[%d] = 0x%04X\n" , i, program.program_number);
						ccx_dtvcc_log( "[SECTION]\tpid[%d] = 0x%04X\n", i, program.PID);
						ccx_dtvcc_log( "[SECTION]#------------------------------------------#\n" ); 			

						ctx->time_ms_prev = _dtvcc_sys_clock() - CCX_DTVCC_CHECK_GAP_TIME_MS;

						ctx->pid = program.PID;
						ccx_dtvcc_request_pmt_data(ctx);
					}
				}		
			}
			break;
			
		case TABLE_PMT_TABLE_ID:
			{
				TS_PSI_Program program_info;
				memset(&program_info, 0, sizeof(program_info));
				memset(&ctx->audio_descriptor, 0, sizeof(ccx_dtvcc_audio_service_descriptor));
				
				_dtvcc_pmt_get_program(ctx, section_data, section_data_size, &program_info);
				_dtvcc_pmt_data_dump(&program_info);
#ifdef DTVCC_TTA_CERT_MODE					
                if(ctx->kec_mode == 1)
                {
    				_dtvcc_tta_audio_descriptor_handle(ctx);
    				if(ctx->vi_audio_count >= 2){
    					ccx_dtvcc_set_timer(ctx, CCX_DTVCC_EXPIRED_ACTION_AUDIO_CHECK_REQUEST);
    				}
				}
				else
				{
    				_dtvcc_audio_descriptor_dump(&ctx->audio_descriptor);
    				_dtvcc_audio_descriptor_handle(ctx);
				}
#else
				_dtvcc_audio_descriptor_dump(&ctx->audio_descriptor);
				_dtvcc_audio_descriptor_handle(ctx);
#endif
				ccx_dtvcc_setting_korean_code(ctx);

				if(ctx->kec_mode){
					_dtvcc_kec_pid_handle(ctx, &program_info);
					ccx_dtvcc_set_timer(ctx, CCX_DTVCC_EXPIRED_ACTION_KEC_PMT_REQUEST);
				}

#ifdef ENABLE_DSMCC_LOGIC
				if(!ctx->dsmcc_pid){ // if dsmcc_pid is empty
					ccx_dtvcc_ait_stream_service *ait_stream = &ctx->ait_stream;
					if(ait_stream->stream_type == TS_PSI_ST_UserPrivate){
						ccx_dtvcc_request_ait_data(ctx, ait_stream->pid);
					}

					if(ait_stream->stream_type == 0){
						ccx_dtvcc_dsmcc_stream *sptr = ctx->dsmcc_stream_head;
						if(sptr){ // Music Channel
							ccx_dtvcc_log("find Music Handling DSMCC"
									"pid: (0x%04X)\n", sptr->pid);
							ctx->dsmcc_pid = sptr->pid;
							ccx_dtvcc_request_dsmcc_indication(ctx);
							// passing Application infomation to dsmcc module
							{
								ccx_dtvcc_audio_service_descriptor *descriptor = &ctx->audio_descriptor;
								dsmcc_application application;
								memset((void*)&application, 0x00, sizeof(dsmcc_application));
								application.appType = DSMCC_MUSIC_APP_TYPE; 						
								application.organizationId = 0;
								application.applicationId = descriptor->ac3_stream_descriptor.pid; // Audio Pid
								application.dsmccPid = sptr->pid;
								application.isServiceBound = 0;
								memcpy(application.name, "music", strlen("music"));
								application.controlCode = 0;
								application.priority = 0;
								application.channelNum = player->channel_num;
								memcpy(application.initialPath, "music.dat", strlen("music.dat"));

								dsmcc_post_application_data(player->dsmcc, &application);
								
								// dsmcc set timer => Music channel update check
								ccx_dtvcc_set_timer(ctx, CCX_DTVCC_EXPIRED_ACTION_DSMCC_REQUEST);
							}
						}
					}
				}
#endif				
			}
			break;
			
		case TABLE_AIT_TABLE_ID:
			{
				TS_PSI_Program program_info;
				memset(&program_info, 0, sizeof(program_info));
				_dtvcc_ait_get_information(ctx, section_data, section_data_size);

				ccx_dtvcc_log("Searching DSMCC assoc_tag");
				ccx_dtvcc_dsmcc_stream *sptr = ctx->dsmcc_stream_head;
				while (sptr) {
					ccx_dtvcc_log("pid: pid: (0x%04X) assoc_tag (0x%04X)", sptr->pid, sptr->assoc_tag);
					if(sptr->assoc_tag == 
						ctx->application_info.transport_descriptor.component_tag)
					{
						ccx_dtvcc_log("find assoc_tag (0x%04X) Handling DSMCC"
								"pid: (0x%04X)\n", sptr->assoc_tag, sptr->pid);
						ctx->dsmcc_pid = sptr->pid;
						ccx_dtvcc_request_dsmcc_indication(ctx);

						// passing Application infomation to dsmcc module
						{
							ccx_dtvcc_appication* ait_application;
							ait_application = &(ctx->application_info.application);

							ccx_dtvcc_application_name_descriptor *name_descriptor;	
							name_descriptor = &(ctx->application_info.name_descriptor);

							ccx_dtvcc_application_descriptor *application_descriptor;	
							application_descriptor = &(ctx->application_info.application_descriptor);
							
							ccx_dtvcc_application_location_descriptor *location_descriptor;		
							location_descriptor = &(ctx->application_info.location_descriptor);

							dsmcc_application application;
							memset((void*)&application, 0x00, sizeof(dsmcc_application));							
							application.appType = ait_application->appication_type;							
							application.organizationId = ait_application->organisation_id;
							application.applicationId = ait_application->application_id;
							application.dsmccPid = sptr->pid;
							application.isServiceBound = application_descriptor->service_bound_flag;
							memcpy(application.name, name_descriptor->application_name, strlen(name_descriptor->application_name));
							application.controlCode = ait_application->application_control_code;
							application.priority = application_descriptor->application_priority;
							application.channelNum = player->channel_num;							
							memcpy(application.initialPath, location_descriptor->initial_path_bytes, strlen(location_descriptor->initial_path_bytes));
							
							dsmcc_post_application_data(player->dsmcc, &application);
						}
						break;
					}
					sptr = sptr->next;
				}
			}
			break;

		default:
			ccx_dtvcc_log("[SECTION] ccx_dtvcc_process_section_filter_data: "
					"don't handle - table_id: %#x\n", table_id);
			break;
	}
}

void ccx_dtvcc_process_dsmcc_section_filter_data(ccx_dtvcc_ctx *ctx, ccx_dsmcc_section_data *section)
{
	unsigned char* section_data = ((unsigned char*)section->section_data);
	unsigned int section_data_size = section->section_data_size;
	dvb_player_t* player = (dvb_player_t*)section->section_user_param;
	
//	ccx_dtvcc_log("[SECTION] ccx_dtvcc_process_dsmcc_section_filter_data\n");
	unsigned char table_id = section_data[0];	

	switch( table_id ) {
		case TABLE_DSM_UN_TABLE_ID:
			{
//				ccx_dtvcc_log("[SECTION] dsmcc_indication Handling\n");
				dsmcc_post_section_filter_data(player->dsmcc, (void*)section_data, section_data_size, 
					(void*)player, ccx_dsmcc_section_done_callback);
			}
			break;

		case TABLE_DSM_DL_TABLE_ID:
			{
//				ccx_dtvcc_log("[SECTION] dsmcc_data Handling\n");
				dsmcc_post_section_filter_data(player->dsmcc, (void*)section_data, section_data_size, 
					(void*)player, ccx_dsmcc_section_done_callback);
			}
			break;

		default:
			ccx_dtvcc_log("[SECTION] ccx_dtvcc_process_dsmcc_section_filter_data: "
					"don't handle - table_id: %#x\n", table_id);
			break;
	}
}


int ccx_dtvcc_process(ccx_dtvcc_ctx *ctx)
{
	ccx_event_list_t *eptr;
	ccx_section_data *section;
	ccx_dsmcc_section_data *dsmcc_section;	
	unsigned char* handleData = 0;
	unsigned int size = 0;

	while ((eptr=dtvcc_dequeue_event(&ctx->dtvcc_event))) {
		switch(eptr->event) {
		case CCX_DTVCC_EVENT_PICTURE_DATA:
			handleData = (unsigned char *)eptr->data;	
			size = (unsigned int)eptr->param;
			if(!ctx->stop_requested)
				ccx_dtvcc_process_picture_data(ctx, handleData, size);
			if(handleData) free(handleData);
			break;

		case CCX_DTVCC_EVENT_SECTION_FILTER_DATA:
			section = (ccx_section_data *)eptr->data;
			if(!ctx->stop_requested)
				ccx_dtvcc_process_section_filter_data(ctx, section);
			_dtvcc_free_section_data(section);					
			break;
			
		case CCX_DTVCC_EVENT_DSMCC_SECTION_FILTER_DATA:
			dsmcc_section = (ccx_dsmcc_section_data *)eptr->data;
			if(!ctx->stop_requested)
				ccx_dtvcc_process_dsmcc_section_filter_data(ctx, dsmcc_section);
			_dtvcc_free_dsmcc_section_data(dsmcc_section);					
			break;
			
		}
		dtvcc_destroy_event_entry(eptr);
	}
	return 0;
}

void *ccx_dtvcc_thread_handle(void *arg )
{
	ccx_dtvcc_ctx *ctx = (ccx_dtvcc_ctx *)arg;
	dvb_player_t* player = (dvb_player_t*)ctx->handle;	
	
	ccx_dtvcc_log("[CEA-708] ccx_dtvcc_thread_handle: started.. \n");	

	ctx->rollup_screen_update = 0;	
	ctx->last_sequence = CCX_DTVCC_NO_LAST_SEQUENCE;
	ctx->cd = (iconv_t) -1;
	
	ctx->stop_requested = 0;
	ccx_dtvcc_log("[CEA-708] initializing services\n");	
	// initial one-time call
	ccx_dtvcc_init_internal(ctx);
	
	while(!ctx->stop_requested) {
        // static으로 변경
		pthread_mutex_lock(&cc_lock); //pthread_mutex_lock(&ctx->dtvcc_event.lock);
		while(!ctx->dtvcc_event.head && !ctx->stop_requested) {
			pthread_cond_wait(&cc_cond, &cc_lock);//pthread_cond_wait(&ctx->dtvcc_event.cond, &ctx->dtvcc_event.lock);
		}
		pthread_mutex_unlock(&cc_lock);//pthread_mutex_unlock(&ctx->dtvcc_event.lock);
		ccx_dtvcc_process(ctx);
	}
	ccx_dtvcc_finish(ctx);
	return 0;
}

void ccx_dtvcc_init_internal(ccx_dtvcc_ctx *ctx)
{
	if(ctx->initial_internal) return;

	ctx->active_service_num = 1;
	ctx->init_korean_code = 0;
	ctx->korean_code = 0;

	int kecReference = 0;
	if(ctx->handle != NULL) kecReference = ((dvb_player_t*)ctx->handle)->kec_mode;
	
	ccx_dtvcc_log("+============================+\n");
	ccx_dtvcc_log("|  KEC_REFERENCE : %d\n", kecReference);
	ccx_dtvcc_log("+============================+\n" );
	ctx->kec_mode = kecReference;
	
	ccx_dtvcc_log("[CEA-708] ccx_dtvcc_init_internal\n");
	ccx_dtvcc_clear_serivce_descriptor(ctx);
	ccx_dtvcc_create_timer(ctx);
#ifdef DTVCC_TTA_CERT_MODE
    if(ctx->kec_mode == 1)
    {
    	ccx_dtvcc_callback_event(ctx, 0, CCX_DTVCC_TV_EVENT_TTA_CERT_MODE, 0);
    	ccx_dtvcc_tta_check_caption_active(ctx);
    	ccx_dtvcc_tta_check_audio_setting_active(ctx);
	}
	else
	{
    	char value[20];
    	get_systemproperty(PROPERTY__HEARING_IMPAIRED, value, 20);
    	if(strcmp(value, "true") != 0) {
    		ccx_dtvcc_log("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    		ccx_dtvcc_log("<<<<<<< Systemproperty Close Caption is InActive <<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    		ccx_dtvcc_log("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    	}
    	ccx_dtvcc_check_setting_active(ctx);
	}
#else
	char value[20];
	get_systemproperty(PROPERTY__HEARING_IMPAIRED, value, 20);
	if(strcmp(value, "true") != 0) {
		ccx_dtvcc_log("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		ccx_dtvcc_log("<<<<<<< Systemproperty Close Caption is InActive <<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		ccx_dtvcc_log("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
	}
	ccx_dtvcc_check_setting_active(ctx);
#endif
	ccx_dtvcc_request_pat_data(ctx);

	ctx->initial_internal = 1;
}

void ccx_dtvcc_finish_internal(ccx_dtvcc_ctx *ctx)
{
	ccx_dtvcc_log("[CEA-708] ccx_dtvcc_finish_internal: finishing\n");

	ccx_dtvcc_stop_section_filter(ctx);	
	ccx_dtvcc_stop_dsmcc_section_filter(ctx);
	
	ccx_dtvcc_callback_event(ctx, CCX_DTVCC_WINDOW_INIT, CCX_DTVCC_TV_EVENT_CLEAR_WINDOW, 0);	
	ccx_dtvcc_remove_timer(ctx);

	if (ctx->cd != (iconv_t) -1)
		iconv_close(ctx->cd);
	
}

void ccx_dtvcc_init(void *handle)
{
	dvb_player_t* player = (dvb_player_t*)handle;
	ccx_dtvcc_ctx *ctx = player->dtvcc;

	if(ctx){ 
		ccx_dtvcc_log("[CEA-708] already initialized dtvcc decoder\n");
		ccx_dtvcc_log("[CEA-708] force destroying dtvcc decoder\n");
		ccx_dtvcc_destroy(handle);		
	}

	int thread_id = 0;	
	ccx_dtvcc_log("[CEA-708] initializing dtvcc decoder\n");
	ctx = (ccx_dtvcc_ctx *) calloc(sizeof(char), sizeof(ccx_dtvcc_ctx));
	if (!ctx){
		ccx_dtvcc_log("[CEA-708] ccx_dtvcc_init calloc ccx_dtvcc_ctx error not enough memory");
		return;
	}
	
	player->dtvcc = ctx;

	ctx->audio_pid = 0;
	ctx->video_pid = 0;

	ctx->cc_callback = player->cc_callback;
	ctx->stop_requested = 1;
	ctx->handle = handle;

/* static 으로 변경
	pthread_mutex_init(&ctx->dtvcc_event.lock, 0);
	pthread_cond_init(&ctx->dtvcc_event.cond, 0);
*/

	pthread_attr_t attr_dtvcc_thread;
	/* create the signal handling thread */
	pthread_attr_init(&attr_dtvcc_thread);
	/* set the thread detach state */
	pthread_attr_setdetachstate(&attr_dtvcc_thread, PTHREAD_CREATE_DETACHED);
	
	thread_id = pthread_create( &ctx->ccx_dtvcc_thread, &attr_dtvcc_thread, ccx_dtvcc_thread_handle, ctx );
	if (thread_id < 0){
		ccx_dtvcc_log("[CEA-708] pthread_create ccx_dtvcc_process returned: %d\n", thread_id);
	}
	ccx_dtvcc_log("[CEA-708] initialized dtvcc decoder\n");
	
#ifdef DTVCC_TTA_CERT_MODE
	// settings log
    char retBuf[128] = {0};
    //int retLen = get_systemproperty(PROPERTY_NAME_HEAR, retBuf, "-1");
    int retLen = get_systemproperty(PROPERTY_NAME_HEAR, retBuf, 128);
    if(retLen > 0 || strcmp(retBuf, "-1") == 0){
		ccx_dtvcc_log("[CEA-708] PROPERTY_NAME_HEAR(vendor.sptek.hear) is null \n");
    }else{
		ccx_dtvcc_log("[CEA-708] PROPERTY_NAME_HEAR(vendor.sptek.hear) : %s \n", retBuf);
    }
	
    //retLen = get_systemproperty(PROPERTY_NAME_CAPTION, retBuf, "-1");
    retLen = get_systemproperty(PROPERTY_NAME_CAPTION, retBuf, 128);
    if(retLen > 0 || strcmp(retBuf, "-1") == 0){
		ccx_dtvcc_log("[CEA-708] PROPERTY_NAME_CAPTION (vendor.sptek.caption) is null \n");
    }else{
		ccx_dtvcc_log("[CEA-708] PROPERTY_NAME_CAPTION (vendor.sptek.caption) : %s \n", retBuf);
    }
	
	//retLen = property_get(PROPERTY_NAME_EYE, retBuf, "-1");
	retLen = get_systemproperty(PROPERTY_NAME_EYE, retBuf, 128);
    if(retLen > 0 || strcmp(retBuf, "-1") == 0){
		ccx_dtvcc_log("[CEA-708] PROPERTY_NAME_EYE (vendor.sptek.eye) is null \n");
    }else{
		ccx_dtvcc_log("[CEA-708] PROPERTY_NAME_EYE (vendor.sptek.eye) : %s \n", retBuf);
    }
	
	//retLen = property_get(PROPERTY_NAME_LANGUAGE, retBuf, "-1");
	retLen = get_systemproperty(PROPERTY_NAME_LANGUAGE, retBuf, 128);
    if(retLen > 0 || strcmp(retBuf, "-1") == 0){
		ccx_dtvcc_log("[CEA-708] PROPERTY_NAME_LANGUAGE (vendor.sptek.language) is null \n");
    }else{
		ccx_dtvcc_log("[CEA-708] PROPERTY_NAME_LANGUAGE (vendor.sptek.language) : %s \n", retBuf);
    }
#endif
}

void ccx_dtvcc_destroy(void *handle)
{
	dvb_player_t* player = (dvb_player_t*)handle;
	ccx_dtvcc_ctx *ctx = player->dtvcc;

	if(!ctx) return;
	
	ccx_dtvcc_log("[CEA-708] ccx_dtvcc_destroy: cleaning up\n");
	ctx->stop_requested = 1;

	pthread_cond_broadcast(&cc_cond);//pthread_cond_broadcast(&ctx->dtvcc_event.cond);

	player->dtvcc = NULL;
	ccx_dtvcc_log("[CEA-708] ccx_dtvcc_destroy: destroyed...\n");
}

void ccx_dtvcc_finish(ccx_dtvcc_ctx *ctx)
{
	if(!ctx) return;
	
	ccx_dtvcc_log("[CEA-708] ccx_dtvcc_finish: finishing\n");

	ccx_dtvcc_finish_internal(ctx);

	_dtvcc_free_dsmcc_stream(ctx);

#ifdef DTVCC_TTA_CERT_MODE
    if(ctx->kec_mode == 1)
    {
    	_dtvcc_free_audio_descriptor(ctx);
	}
#endif

	dtvcc_free_events(&ctx->dtvcc_event);	

/* static으로 변경
	pthread_mutex_destroy(&ctx->dtvcc_event.lock);
	pthread_cond_destroy(&ctx->dtvcc_event.cond);
*/

	for (int i = 0; i < CCX_DTVCC_MAX_SERVICES; i++){
		ccx_dtvcc_service_decoder *decoder = ctx->decoders[i];
		if(!decoder) continue;

		if (decoder->tv){
			free(decoder->tv);
			decoder->tv = NULL;
		}
		
		for (int j = 0; j < CCX_DTVCC_MAX_WINDOWS; j++){
			ccx_dtvcc_window *window = decoder->windows[j];			
			if (window){
				if (window->memory_reserved){
					for (int k = 0; k < CCX_DTVCC_MAX_ROWS; k++) {
						if (window->rows[k]) {
							free(window->rows[k]);
							window->rows[k] = NULL; 				
						}
					}
					window->memory_reserved = 0;
				}
				free(window);
				window = NULL;
			}
		}
		
		free(decoder);
		decoder = NULL;
		
	}
	
	free(ctx);
	ctx = NULL;
	ccx_dtvcc_log("[CEA-708] ccx_dtvcc_finish: finished...\n");
}


#ifdef __cplusplus
}
#endif

