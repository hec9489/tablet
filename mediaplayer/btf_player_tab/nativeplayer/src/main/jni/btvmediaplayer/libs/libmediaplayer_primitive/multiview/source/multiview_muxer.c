// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <libavutil/timestamp.h>
#include "multiview_muxer.h"
#include "multiview_error.h"

#define MUXER_STREAM_COUNT          4
static int64_t *g_packetPts;
static bool    *g_printPts;

void init_muxer()
{
	g_packetPts = av_malloc(sizeof(int64_t) * MUXER_STREAM_COUNT);
	g_printPts = av_malloc(sizeof(bool) * MUXER_STREAM_COUNT);

	for(int i = 0; i < 4 ; i++) {
		g_packetPts[i] = -1;
		g_printPts[i] = false;
	}

}


int multiview_create_muxer_stream(ST_FFMPEG_MUX_CTX** ctx, ST_FFMPEG_DEMUX_CTX** ffmpeg_demux_ctx, int demuxCnt,
                                  void* opaque, WRITE_STREAM_CB cb) {
	int result = 0;
	uint8_t *buffer = NULL;
	AVFormatContext *fmt_ctx = NULL;

	LOGI("multiview_create_muxer_stream start. (%s : %d)", __FUNCTION__, __LINE__);
	if (ctx == NULL)
	{
		LOGE("ST_FFMPEG_MUX_CTX is NULL");
		return FFMPEG_ERR_INVAIDDATA;
	}

	if(ffmpeg_demux_ctx == NULL || demuxCnt != MULTIVIEW_MUXER_CHANNEL)
	{
		LOGE("demux context is invalid.  ffmpeg_demux_ctx(0x%08x), demuxCnt (%d)", (unsigned int)ffmpeg_demux_ctx, demuxCnt);
		LOGE("demuxCnt must be (%d) now.", MULTIVIEW_MUXER_CHANNEL);
		return FFMPEG_ERR_INVAIDDATA;
	}

//	av_register_all();

	*ctx = (ST_FFMPEG_MUX_CTX *) av_malloc(sizeof(ST_FFMPEG_MUX_CTX));
	if (*ctx == NULL)
		return FFMPEG_ERR_NOT_ENOUGHT_MEMORY;

	/**
	 * avformat_alloc_output_context2 에서 중복 할당되어 삭제함.
	fmt_ctx = avformat_alloc_context();
	if (fmt_ctx == NULL) {
		LOGE("fail to alloc avformat context");
		return FFMPEG_ERR_INTERNAL;
	}
	*/

//	result = avformat_alloc_output_context2(&fmt_ctx, fmt_ctx->oformat,
//	                                   "mpegts", NULL);
	result = avformat_alloc_output_context2(&fmt_ctx, NULL,
	                                   "mpegts", NULL);

	if(result < 0)
	{
		LOGE("avformat_alloc_output_context2 failed.");
		av_free(*ctx);
		*ctx = NULL;
		return FFMPEG_ERR_INTERNAL;
	}

	buffer = av_malloc(AVIO_BUFFER_SIZE);
	if (!buffer) {
		LOGE("fail to alloc av_buffer.");
		avformat_free_context(fmt_ctx);
		av_free(*ctx);
		*ctx = NULL;
		return FFMPEG_ERR_NOT_ENOUGHT_MEMORY;
	}

	AVIOContext *avioContext = avio_alloc_context(buffer, AVIO_BUFFER_SIZE, 1,
	                                              opaque, NULL, cb, NULL);
	if (avioContext == NULL) {
		LOGE("avio_alloc_context fail");
		av_free(buffer);
		avformat_free_context(fmt_ctx);
		av_free(*ctx);
		*ctx = NULL;
		return FFMPEG_ERR_INTERNAL;
	}

	fmt_ctx->pb = avioContext;
	fmt_ctx->flags = AVFMT_FLAG_CUSTOM_IO;

	AVStream* out_stream = NULL;
	int videoStreamIdx = ffmpeg_demux_ctx[0]->video_stream_idx;
	int audioStreamIdx = 0;
	int streamidx = 0;
	(*ctx)->video_stream_idx = streamidx++;

	// create video stream
	out_stream = avformat_new_stream(fmt_ctx, NULL);
	if(out_stream == NULL)
	{
		LOGE("filed allocating output stream");
		av_free(buffer);
		if((*ctx)->fmt_ctx != NULL)
            av_free((*ctx)->fmt_ctx->pb);
		avformat_free_context(fmt_ctx);
		av_free(*ctx);
		*ctx = NULL;
		return FFMPEG_ERR_NOT_ENOUGHT_MEMORY;
	} else{
		result = avcodec_parameters_copy(out_stream->codecpar,
		                                 ffmpeg_demux_ctx[0]->fmt_ctx->streams[videoStreamIdx]->codecpar);
		if(result < 0){
			LOGE("Failed to copy codec paramters");
			av_free(buffer);
			if((*ctx)->fmt_ctx != NULL)
                av_free((*ctx)->fmt_ctx->pb);
			avformat_free_context(fmt_ctx);
			av_free(*ctx);
			*ctx = NULL;
			return FFMPEG_ERR_INTERNAL;
		}
		(*ctx)->video_stream_idx = 0;       // 0 으로 고정
		(*ctx)->video_time_base = ffmpeg_demux_ctx[0]->fmt_ctx->streams[videoStreamIdx]->time_base;
		out_stream->id =  ffmpeg_demux_ctx[0]->vid;
		LOGI("video stream id (%d)", out_stream->id);

	}

	// create audio stream
	for(int i = 0; i < MULTIVIEW_MUXER_CHANNEL ; i++){
		audioStreamIdx = ffmpeg_demux_ctx[i]->audio_stream_idx;

		out_stream = avformat_new_stream(fmt_ctx, NULL);
		if(out_stream == NULL){
			LOGE("filed allocating output stream");
			av_free(buffer);
            if((*ctx)->fmt_ctx != NULL)
                av_free((*ctx)->fmt_ctx->pb);
			avformat_free_context(fmt_ctx);
			av_free(*ctx);
			*ctx = NULL;
			return FFMPEG_ERR_NOT_ENOUGHT_MEMORY;
		}

		result = avcodec_parameters_copy(out_stream->codecpar,
		                                 ffmpeg_demux_ctx[i]->fmt_ctx->streams[audioStreamIdx]->codecpar);
		if(result < 0) {
			LOGE("Failed to copy codec paramters. index(%d)", i);
			av_free(buffer);
            if((*ctx)->fmt_ctx != NULL)
                av_free((*ctx)->fmt_ctx->pb);
			avformat_free_context(fmt_ctx);
			av_free(*ctx);
			*ctx = NULL;
			return FFMPEG_ERR_INTERNAL;
		}

		(*ctx)->audio_stream_idx[i] = streamidx++;          // 1부터 값 설정.
		(*ctx)->audio_time_base[i] = ffmpeg_demux_ctx[i]->fmt_ctx->streams[audioStreamIdx]->time_base;
		out_stream->id =  ffmpeg_demux_ctx[i]->aid;

		LOGI("create audio streamIdx(%d) id (%d), streamCount(%d)", i, out_stream->id, fmt_ctx->nb_streams);
	}


	result = avformat_write_header(fmt_ctx, NULL);
	if(result < 0){
		LOGE("avformat_write_header failed.");
		av_free(avioContext->buffer);
		if((*ctx)->fmt_ctx != NULL)
		    av_free((*ctx)->fmt_ctx->pb);
		avformat_free_context(fmt_ctx);
		av_free(*ctx);
		*ctx = NULL;
		return FFMPEG_ERR_INTERNAL;
	}
//	LOGI("avformat_write_header success.");

	result = mvQueue_getInstance(&(*ctx)->message_queue);
	if(result < 0)
	{
		LOGE("get queue instance failed.");
		av_free(avioContext->buffer);
		if((*ctx)->fmt_ctx != NULL)
		    av_free((*ctx)->fmt_ctx->pb);
		avformat_free_context(fmt_ctx);
		av_free(*ctx);
		*ctx = NULL;

		return result;
	}

	(*ctx)->fmt_ctx = fmt_ctx;
	(*ctx)->avio_ctx = avioContext;
//	av_dict_free(&d);

	init_muxer();

	return result;
}

void messagequeue_finalize(ST_QUEUE_MESSAGE* queue)
{
	ST_MESSAGE message;
	int result = 0;

/*
	while(mvQueue_popMessage(queue, &message) >= 0){
		av_packet_unref(&message.packet);
	}
*/

    result = mvQueue_freezing(queue);
    if(result < 0)
    {
        LOGE("mvQueue_fresszing error.  result(%d)", result);
    }

	result = mvQueue_unRefInstance(&queue);
	LOGI("muxer mvQueueRefCount(%d), (%s:%d)", result, __FUNCTION__, __LINE__);

	if(g_packetPts != NULL) av_free(g_packetPts);
	if(g_printPts != NULL) av_free(g_printPts);
	g_packetPts = NULL;
	g_printPts = NULL;

	LOGI("muxer mesasgequeue finalize..");
}

// 처음 입력된 packet에 대한 packet interval 확인.-- test용 코드
void checkPacketTime(AVPacket* packet)
{
	AVRational avRational;
	avRational.den = 90000;
	avRational.num = 1;

	int64_t interval = 0;
	if(g_packetPts[packet->stream_index] == -1)
	{
		g_packetPts[packet->stream_index] = packet->pts;
	}

	if(packet->stream_index == 0)  // video 일 경우.
		return;

	// current audio Packet - base video packet
	interval = g_packetPts[packet->stream_index] - g_packetPts[0];

	if(g_packetPts[0] != -1 && g_packetPts[packet->stream_index] != -1 && g_printPts[packet->stream_index] == false)
	{
		LOGI("MUXER -- streamIdx(%d) first video pts (%lld) first audio pts(%lld), intervalPts(%lld), intervalTime(%s)",
		     packet->stream_index, g_packetPts[0], g_packetPts[packet->stream_index], interval, av_ts2timestr(interval, &avRational));
		g_printPts[packet->stream_index] = true;
	}
}

int write_packet(AVFormatContext *fmt_ctx, const AVRational *time_base, AVStream *st, AVPacket *pkt)
{
	/* rescale output packet timestamp values from codec to stream timebase */
	av_packet_rescale_ts(pkt, *time_base, st->time_base);

	return av_interleaved_write_frame(fmt_ctx, pkt);
}

int multiview_mux_stream(ST_FFMPEG_MUX_CTX* ctx, h_pt_merge_hevc pt_ctx, int* terminate)
{
	int result = 0;

	AVPacket    *packet = NULL;
	pt_hevc_au_t *ptHevc = NULL;
	ST_MESSAGE  msg;
	void         *avBuf = NULL;

	if(ctx == NULL || pt_ctx == NULL || terminate == NULL)
	{
		LOGE("paramter is invalid.   ctx(0x%x), pt_ctx(0x%x), terminate(0x%x)",
		     (unsigned int)ctx, (unsigned int)pt_ctx, (unsigned int)terminate);
		return FFMPEG_ERR_INVAIDDATA;
	}

	ptHevc = (pt_hevc_au_t*)av_malloc(sizeof(pt_hevc_au_t));

	packet = av_packet_alloc();

	while (*terminate == 0) {

		av_init_packet(packet);

		// for video stream
		result = PIX_GetMergeHevcEs(pt_ctx, ptHevc);
		if (result == PIX_SUCCESS) {
			// 정상적으로 MergeFrame을 얻은 경우
//			saveMultiviewRawData(ptHevc->pStream, ptHevc->iStreamLength);
//			LOGW("PIX_GetMergeHevcEs,  pts(%lld), dts(%lld) streamIdx(%d)", ptHevc->ulPts, ptHevc->ulDts, packet.stream_index);

			avBuf = av_malloc(ptHevc->iStreamLength);
			if(!avBuf)
			{
				LOGE("not enough memory.  size(%d)", ptHevc->iStreamLength);
				continue;
			}

			// av_malloc 으로 할당한 버퍼를 packet 에 넣어야 문제발생안됨.
			// 그렇지 않으면 내부적으로 av_packet_unref를 호출할때 종료됨.
			memcpy(avBuf, ptHevc->pStream, ptHevc->iStreamLength);
			result = av_packet_from_data(packet, avBuf, ptHevc->iStreamLength);
			if(result >= 0) {
				if(packet->buf == NULL)
					LOGE("packet buf is NULL..  streamBuf(0x%x), length(%d)", (unsigned int)ptHevc->pStream, ptHevc->iStreamLength);

				packet->stream_index = ctx->video_stream_idx;
				packet->pts = ptHevc->ulPts;
				packet->dts = ptHevc->ulDts;
				packet->duration = 0;

				packet->pos = -1;
//				LOGI("MUXER video device(%d), pts(%lld), dts(%lld)", msg.streamId, packet.pts, packet.dts);
//				log_packet(ctx->fmt_ctx, packet);      // for test
				checkPacketTime(packet); // for test

				// write video frame
				result = write_packet(ctx->fmt_ctx, &ctx->video_time_base, ctx->fmt_ctx->streams[packet->stream_index], packet);
//				result = av_interleaved_write_frame(ctx->fmt_ctx, packet);
				if (result < 0)
					LOGE("write video frame failed. func(%s:%d) channelIdx(%d), error(%d : %s)",
					     __FUNCTION__, __LINE__, msg.streamId, result, av_err2str(result));

				av_packet_unref(packet);       // pthevc stream을 이용하는 것이라 unref 하지 않음.
			}
			memset(ptHevc, 0, sizeof(pt_hevc_au_t));
		}

		// for audio stream
		result = mvQueue_popMessage(ctx->message_queue, &msg);
		if(result >= 0)
		{
			// channelIdx 는 InputStream 에 대한 ID 라 TS Stream id 의 값과는 다르다.
			// 따라서 Audio Stream ID 인 1, 2, 3 으로 설정하도록 +1을 한다.
			msg.packet.stream_index = ctx->audio_stream_idx[msg.streamId];
			msg.packet.duration = 0;
			msg.packet.pos = -1;

//			LOGI("MUXER audio device(%d), pts(%lld), dts(%lld)", msg.streamId, msg.packet.pts, msg.packet.dts);
//			log_packet(ctx->fmt_ctx, &msg.packet);      // for test
			checkPacketTime(&msg.packet); // for test

			if(msg.packet.buf == NULL || msg.packet.pts < 0 || msg.packet.dts < 0 )
			{
				LOGE("parameter is invalid buf(0x%x), pts(%lld), dts(%lld)",
				     msg.packet.buf, msg.packet.pts, msg.packet.dts);
			}

			if(ctx->fmt_ctx->nb_streams != (MULTIVIEW_MUXER_CHANNEL + 1))
			{
				LOGE("stream index is invalid.   streamCnt(%d)", ctx->fmt_ctx->nb_streams);
			}

			result = write_packet(ctx->fmt_ctx, &ctx->audio_time_base[msg.packet.stream_index-1],
								  ctx->fmt_ctx->streams[msg.packet.stream_index], &msg.packet);
//			result = av_interleaved_write_frame(ctx->fmt_ctx, &msg.packet);
			if (result < 0)
			{
				LOGE("write audio frame failed. func(%s:%d) channelIdx(%d), error(%d : %s)",
				     __FUNCTION__, __LINE__, msg.streamId, result, av_err2str(result));
			}/* else
				LOGI("[AUDIO] write audio frame success.  channelIDX(%d)", msg.streamId);*/
			av_packet_unref(&msg.packet);
			
			if (result < 0) {
				LOGE("%s:%d, multiview error break!!, result : %d", __FUNCTION__, __LINE__, result);
				break;
			}
		}

		usleep(MULTIVIEW_USLEEP_TIME);
	} //while (!*Terminate )

	LOGI("terminated muxer.");
	av_packet_free(packet);
	av_free(ptHevc);

	return 0;
}


int multiview_destroy_muxer(ST_FFMPEG_MUX_CTX** ctx){
	int result = 0;

	LOGI("multiview_destroy_muxer start. (%s : %d)", __FUNCTION__, __LINE__);

	if(ctx == NULL || *ctx == NULL){
		LOGE("invalid ctx.  ctx is invalid");
		return FFMPEG_ERR_INVAIDDATA;
	}

	messagequeue_finalize((*ctx)->message_queue);

    for(int i = 0; i <= MULTIVIEW_MUXER_CHANNEL ; i++)
    {
        avcodec_close((*ctx)->fmt_ctx->streams[i]->codec);
        avcodec_free_context(&(*ctx)->fmt_ctx->streams[i]->codec);
    }

	av_free((*ctx)->avio_ctx->buffer);
	av_free((*ctx)->avio_ctx);
//	av_free((*ctx)->fmt_ctx->pb);
	avformat_free_context((*ctx)->fmt_ctx);
//    avformat_close_input(&(*ctx)->fmt_ctx);

	av_free(*ctx);
	*ctx = NULL;

	LOGI("MuxerContext destroy.");

	return 0;
}

#ifdef __cplusplus
} //extern "C"
#endif