// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef WORK_MULTIVIEW_DEFINE_H
#define WORK_MULTIVIEW_DEFINE_H


#define AVIO_BUFFER_SIZE 7*188*10        // ffmpeg internal read buffer
#define MULTIVIEW_MUXER_CHANNEL     3
#define MULTIVIEW_USLEEP_TIME       1000




#endif //WORK_MULTIVIEW_DEFINE_H
