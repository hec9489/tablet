// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef DSMCC_UTIL_H
#define DSMCC_UTIL_H

#include <stdint.h>

unsigned long dsmcc_crc32 (unsigned char *data, int len);

#endif

