// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef ____RTP_INFO_DEFINE___H____
#define ____RTP_INFO_DEFINE___H____

#include <arpa/inet.h>
#include <pt_mlr_client.h>
#ifdef USE_MULTIVIEW
#include <hal/multiView/pix_hevc_merge_export.h>
#endif

/* buffer size - should be a multiple of 7 and
188 (7 tp pkts in a rtp pkt) */
#define SIZE (7 * 188 * 1024 * 16)

//#define BUFFERING_DURATION (32<<12) //usec
#define BUFFERING_DURATION 20000 //usec
#define BUFFERING_DURATION_CORRECTION 0 //usec 보정용
#define BUFFERING_DURATION_FIRST 20000 //500000 //usec
// jung2604
#define BUFFERING_SIZE (188 * 7 * 20)
#define BUFFERING_FEED_WRITE_MIN_SIZE (7 * 188 * 100)
#define BUFFERING_FIRST_SIZE (7 * 188 * 100)

struct rtpbits {
    unsigned int v:2;   /* version: 2 */
    unsigned int p:1;   /* is there padding appended: 0 */
    unsigned int x:1;   /* number of extension headers: 0 */
    unsigned int cc:4;  /* number of CSRC identifiers: 0 */
    unsigned int m:1;   /* marker: 0 */
    unsigned int pt:7;  /* payload type: 33 for MPEG2 TS - RFC 1890 */
    unsigned int sequence:16;   /* sequence number: random */
};

struct rtpheader {    /* in network byte order */
    struct rtpbits b;
    int timestamp;    /* start: random */
    int ssrc;         /* random */
    int type;         /* RTP or UDP */
};

typedef struct RtpInfo_s {
    int socketIn;
    volatile char *buf_readptr;
    volatile char *buf_writeptr;

	volatile int buf_readOneOrMoreData;
    const char *buf_max;
    const char *buf_min;
    char buffer[SIZE];
	char bufferTemp[SIZE];
    int connected;
    pthread_t ThreadReader;
    pthread_t ThreadReaderFeed;
#ifdef USE_MULTIVIEW
    pt_hevc_au_t inputHevc;
    pt_hevc_au_t outputHevc;
#endif

    //jung2604 : 추가
	unsigned long ipAddr; //uint32_t ipAddr;
    unsigned short ipPort;
	struct ip_mreq blub;
} RtpInfo_t;

#endif
