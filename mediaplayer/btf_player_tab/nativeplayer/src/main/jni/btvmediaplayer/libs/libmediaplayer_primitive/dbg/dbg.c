// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#include <stdbool.h>
#include <sys/time.h>

static bool dbg_print=0;

void dbg_set_dbg_print(bool enabled)
{
    dbg_print = enabled;
}

bool dbg_check_dbg_print()
{
    return dbg_print;
}

long get_current_date_in_sec()
{
    struct timeval tv_date;
    
    gettimeofday(&tv_date,NULL);
  
    return  (tv_date.tv_sec);

}

long get_current_date_in_usec()
{
    struct timeval tv_date;
    
    gettimeofday(&tv_date,NULL);
  
    return  (tv_date.tv_sec * 1000000 + tv_date.tv_usec);

}

