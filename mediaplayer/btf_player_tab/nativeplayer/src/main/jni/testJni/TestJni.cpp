#define LOG_TAG "Test-JNI"
#include <ALog.h>
#include <jni.h>

#include <BtvMediaPlayerService.h>

namespace android {
}

using namespace android;

static jint tuneTv(JNIEnv *env, jobject thiz, jstring xml) {
    ALOGD("[%s] called", __FUNCTION__);

    if(xml == NULL) {
        ALOGD("invalid argument is NULL ");
        return -1;
    }

    const char *tmpXml = env->GetStringUTFChars(xml, NULL);
    if (tmpXml == NULL)
        return -1;

    auto btvmedia = BtvMediaPlayerService::getInstance();
    btvmedia->tuneTV(0, tmpXml);

	return 1;
}

// ------------------------------------------------------------------------------------------------

// FIXME : java 패키지 수정
static const char *classJavaPackage = "sptek/sptek_media_player/MainActivity";

# define NELEM(x) ((int) (sizeof(x) / sizeof((x)[0])))

// FIMXME : 사용할 API 정의
static JNINativeMethod gMethods[] = {
        {"tuneTv", "(Ljava/lang/String;)I", (void*)tuneTv},
};

jint JNI_OnLoad(JavaVM* vm, void* /* reserved */)
{
    JNIEnv* env = NULL;
    jint result = -1;

    if (vm->GetEnv((void**)&env, JNI_VERSION_1_4) != JNI_OK)
    {
        ALOGD("[%s][%d] ERROR: GetEnv falied", __func__, __LINE__);
        return JNI_FALSE;
    }

    jclass clazz = env->FindClass(classJavaPackage);
    if (clazz == NULL) {
        ALOGD("[%s][%d] ERROR: FindClass failed", __func__, __LINE__);
        return JNI_FALSE;
    }

    if (env->RegisterNatives(clazz, gMethods, NELEM(gMethods)) < 0) {
        ALOGD("[%s][%d] ERROR: native registration failed", __func__, __LINE__);
        return JNI_FALSE;
    }

    result = JNI_VERSION_1_4;
    return result;
}
