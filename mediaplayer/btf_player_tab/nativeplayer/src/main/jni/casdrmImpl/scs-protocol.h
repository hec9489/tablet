
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/time.h>
#include <time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <fcntl.h>


#ifndef SCS_PROTOCOL_H
#define SCS_PROTOCOL_H

#ifdef __cplusplus
extern "C" {
#endif

#define BYTE                unsigned char
#define __int32             int
#define __int16             short

/* Error Code */
#define RTN_NO_ERR                          0x00
#define RTN_ERR_BAD_PKT                     0x01
#define RTN_ERR_NOT_CMD                     0x02
#define RTN_ERR_PDU_ERR                     0x03
#define RTN_ERR_CRC                         0x04
#define RTN_ERR_STB_PINNUM                  0x11
#define RTN_ERR_STB_MAC_ADDRESS             0x12
#define RTN_ERR_STB_ID                      0x13
#define RTN_ERR_CTS_NO_PERMIT               0x21
#define RTN_ERR_CTS_EXPIRE                  0x22
#define RTN_ERR_CTS_NOT_FOUND               0x23
#define RTN_ERR_CTS_INVALID_ID              0x24
#define RTN_ERR_NOT_SERVICE_ARREAR          0x25
#define RTN_ERR_NOT_SERVICE_TERMINATION     0x26
#define RTN_ERR_NOT_SERVICE_PAUSE           0x27
#define RTN_ERR_CDR_LOG_FAIL                0x4D
#define RTN_ERR_SYSTEM_ERR                  0x31
#define RTN_ERR_VERSION_ERR                 0x05
/* Error Code */



#define ENC_BF                              0x03
#define ENC_PLAIN                           0x01

#define MSG_REQ                             0x01
#define MSG_RSP                             0x10


#define HEADER_DEFAULT_STARTCODE    0xFF
#define HEADER_DEFAULT_VERSION      0x0C
#define HEADER_DEFAULT_VERSION_LGS  0x01
#define HEADER_DEFAULT_SVCCODE      0x01
#define HEADER_DEFAULT_MSGTYPE      MSG_REQ
#define HEADER_DEFAULT_ENCTYPE      ENC_BF
#define HEADER_DEFAULT_CRCCHECK     0x00

/* Packet Structure */
#pragma pack(1)
struct _Header
{
    unsigned char   nStartCode;
    unsigned char   nVersion;
    unsigned char   nSvcCode;
    unsigned char   nMsgType;
    unsigned char   nEncType;
    unsigned char   nCmdCode;
    unsigned char   nErrCode;

    unsigned char   byStbMsc[6];
    __int32         dTimeStamp;
    __int16         nPDUSize;
    unsigned char   nCrcCheck;
};

#define MAX_BUFFER_SIZE	2048
typedef struct TPacket
{
    struct _Header  header;
    char            pdu[MAX_BUFFER_SIZE];
} tpacket;

#pragma pack()
/* Packet Structure */

int scs_init(char *hostname, int port);
void scs_close(int fd);
int scs_recv( int fs, void *buffer, int nlen);
int scs_send(int fs, void *buffer, int nlen);
int scs_recvEx( int fs, void *buffer, int nlen);
int scs_sendEx(int fs, void *buffer, int nlen);


int set_default_header( tpacket   *packet, int ver, int enc );
int set_packet_cmd( tpacket *packet, unsigned char cmd);
int set_packet_err( tpacket *packet, unsigned char err);
int set_packet_mac( tpacket *packet );
int set_packet_time( tpacket *packet );
int clear_packet( tpacket *packet);
int copy_packet_data( tpacket *packet, void* data, int len);
int set_packet_data( tpacket *packet, char* data);
int append_packet_data( tpacket *packet, char* data);
int get_packet_data_length( tpacket *packet);
int get_packet_free_length( tpacket *packet);
int  get_interface_info( const char *eth, int infotype, char *rtnval);

int ScsTriggerRequestHandling ( char *pcData, int iBufLen, unsigned char ucCommand  );

#ifdef __cplusplus
}
#endif

#endif /* HANA_PRTOCOL_H */
