#ifndef _DMUX_DEFINE_H_______________
#define _DMUX_DEFINE_H_______________

#ifdef __cplusplus
extern "C" {
#endif

/***************************************************************************/
/**                                                                		  **/
/** MODULES USED                                                          **/
/**                                                                       **/
/***************************************************************************/
#include "cust_Feature.h"
#include "mcas_types.h"

#include "mcas_iptv.h"
#include <sys/socket.h>



/**************************************************************
type define
****************************************************************/
#define CAS_TIME_OUT  		(50000)  //ms
// fldgo note : auth server 접속 시간은 5초 따로 둔다.
// 최초 booting 시 main 에서 auth 접속시간이 50초이면 너무 오래걸리고 media api call 오류가 발생한다.
// 반대로 api 로 auth 체크 할 경우 정상이지만 서버가 바쁠 경우 5초룰에 걸릴 가능성도 존재한다.
// 하지만 이때에는 app이 다시 호출할 것이다.
#define CAS_CERT_AUTH_CONNECT_TIME_OUT  		(5000)  //ms
#define TRUE                            1
#define FALSE                           0

#define TS_PACKET_LEN                   (188)              // TS RDSIZE is fixed !!
#define TS_SYNC_BYTE                    (0x47)             // SyncByte fuer TS  ISO 138181-1

//#define EMM_MULTICAST_CONF_FILE         SKAF_HOME_DIR "conf/iptv_emm_multicast.conf"
#define EMM_MULTICAST_CONF_FILE   "/btf/btvmedia/cas/iptv_emm_multicast.conf"

#if 0
#define EMM_MULTICAST_IP                "239.192.60.100"
#else
#define EMM_MULTICAST_IP                "239.192.63.100"   // SAK - 13Jan2010 - Changed as per SKBM request.
#endif /* #if 0 */
#define EMM_MULTICAST_PORT              49220


#define MAXIMUM_AV_PID                  20
#define TS_PACKET_LEN                   (188)              /* TS RDSIZE is fixed !! */
#define TS_SYNC_BYTE                    (0x47)             /* SyncByte fuer TS  ISO 138181-1 */
#define RM_ERROR                        9
#define RM_OK                           6
#define TRUE                            1
#define FALSE                           0
#define RMstatus                        int
#define RMint64                         long long
#define RMuint64                        unsigned long long
#define RMbool                          unsigned char

typedef unsigned long              RMuint32;
typedef unsigned short             RMuint16;
typedef unsigned char              RMuint8;
typedef char                        RMascii;


/*
	typedef struct _group_source_req
    {
        // Interface index.
        //uint32_t gsr_interface;
		uint32 gsr_interface;

        // Group address.
        struct sockaddr_storage gsr_group;

        // Source address.
        struct sockaddr_storage gsr_source;
    } group_source_req;
*/
  // "...has a previous declaration here..." 에러 때문에 아래와 같이 수정.
    typedef struct _group_source_req
        {
            // Interface index.
            //uint32_t gsr_interface;
    		uint32 gsr_interface;

            // Group address.
            struct sockaddr_storage gsr_group;

            // Source address.
            struct sockaddr_storage gsr_source;
    } group_source_req_ex;


    /** type used to manipulate socket */
    typedef struct _RMsocket *RMsocket;


    /* sundara - added to manipulate Join */
    /* sundara - added to hold group struct */
    typedef struct
    {
        RMsocket group_sock;
        /* AF_INET6  = IPPROTO_IPV6,  AF_INET = IPPROTO_IP */
        int level;

        RMascii * psource_ip;
        RMascii * pgroup_ip;

        /* memory has to be allocated by usr calling the lib */
        //group_source_req *pJoinReq;
        group_source_req_ex *pJoinReq;
    } WISE_GROUP_STRUCT;


    typedef  struct _TSBuffer
	{
	        BYTE			buffer[131072]; // 128K
	        int32              sp;             // buffer start pos;
	        int32              ep;             // buffer end pos
	        int32              cp;             // buffer current pos;
	        uint32             len;            // buffer len;
	        struct _TSBuffer    *next;
	} TSBufferType;

	typedef enum
	{
	        Unknown,
	        AudioPID,
	        VideoPID,
	        ECMPID,
	        PATPID,
	        PMTPID,
	} PidType;


	typedef struct _pidTab
	    {
	        uint32     ecmpid;
	        uint32     audiopid;
	        uint32     videopid;
	    } pidTableType;

	typedef int32 (*sectionFilter)( PidType type, uint32, BYTE *buf, int viewID );


	typedef struct _demux
	{
	        pidTableType        pidTab;
	        sectionFilter       ECMFilterCB;
	        sectionFilter       AVFilterCB;
	        TSBufferType        inBuffer;
	        int32              	TSFound;
	} SWDemux;

    struct PMTInfo_type;


#ifdef __cplusplus
}
#endif
#endif /*_DMUX_DEFINE_H_______________ */

