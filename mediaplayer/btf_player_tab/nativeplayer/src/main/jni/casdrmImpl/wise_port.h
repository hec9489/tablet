#ifndef _WISEMBED_PORTING_INTEFACE_H_
#define _WISEMBED_PORTING_INTEFACE_H_

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <libgen.h>
#include <stdarg.h>
#include <time.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef __cplusplus
extern "C" {
#endif

#define FH5000_DRM_SECURE_PATH "/DATA/SKAF/skaf_home/DATA/CAS_DRM_Secure"

#define DRM_RO_PATH "/Data"
#define DRM_DB_PATH "/DB"

/**
 * SK DRM/CAS/PKI Agent porting layer common interface
 * using prefix wisembed porting interface 'wpi'
 */
#define WPI_VOID		void
#define WPI_PVOID		void*
#define 	WPI_INT32		int
#define WPI_UINT32	unsigned int
#define WPI_CHAR		char
#define WIPI_PCHAR	char*
#define WPI_UCHAR	unsigned char
#define WPI_PUCHAR	unsigned char*
#define WPI_BOOL		int
#define WPI_LONG		long
#define WPI_DOUBLE double

/*
 * String Handling Function
 */
#ifdef USE_INLINE_DEFINE
#define wpi_strtok		strtok
#define wpi_atoi			atoi
#define wpi_atof			atof
#define wpi_sprintf		sprintf
#else
WPI_CHAR	*wpi_strtok( WPI_CHAR *str, const WPI_CHAR *delimit);
WPI_INT32 wpi_atoi( const WPI_CHAR *str);
WPI_DOUBLE wpi_atof( const WPI_CHAR *str);
WPI_INT32 wpi_sprintf( WPI_CHAR *buffer, const WPI_CHAR *format, ...);
#endif
/**
 * memory related function
 */
#ifdef USE_INLINE_DEFINE
#define wpi_malloc malloc
#define wpi_free 	free
#define wpi_realloc realloc
#else
WPI_VOID* wpi_malloc( WPI_INT32 size);
WPI_VOID wpi_free( WPI_VOID *ptr);
WPI_VOID* wpi_realloc( WPI_VOID *block, WPI_INT32 size);
#endif

/**
 * File I/O related function
 */
#ifdef USE_INLINE_DEFINE
#define wpi_mkdir		mkdir
#define wpi_close		close
#define wpi_remove	remove
#define wpi_read		read
#define wpi_write		write
#define wpi_flush 		fsync
#define wpi_rename	rename
#define wpi_stat			stat
#define wpi_rmdir		rmdir
#else
WPI_INT32 wpi_mkdir( WPI_CHAR *name);
WPI_INT32 wpi_rmdir( WPI_CHAR *name);
WPI_VOID wpi_close(WPI_INT32 handle);
WPI_INT32 wpi_remove( WPI_CHAR *name);
WPI_INT32 wpi_read( WPI_INT32 handle, WPI_VOID * pBuf, WPI_INT32 size);
WPI_INT32 wpi_write( WPI_INT32 handle, WPI_VOID *pBuf, WPI_INT32 size);
WPI_INT32 wpi_flush( WPI_INT32 handle);
WPI_INT32 wpi_rename( WPI_CHAR *oldName, WPI_CHAR *newName);
WPI_INT32 wpi_stat(WPI_CHAR *filename, struct stat *fileinfo);
#endif
WPI_INT32 wpi_mkdir_p( WPI_CHAR *name);
WPI_INT32 wpi_open( WPI_CHAR *name, WPI_INT32 mode);
WPI_INT32 wpi_open2( WPI_CHAR *name, WPI_CHAR *mode);
WPI_INT32 wpi_close2(WPI_INT32 handle);
WPI_UINT32 wpi_size(WPI_CHAR *name);
WPI_INT32 wpi_seek( WPI_INT32 handle, WPI_INT32 offset, WPI_INT32 origin);
WPI_INT32 wpi_tell( WPI_INT32 handle);
WPI_BOOL wpi_exist( WPI_CHAR *fileName);
WPI_INT32 wpi_remove_rf( WPI_CHAR *path);
WPI_INT32 wpi_list(WPI_CHAR *dir, WPI_CHAR *buffer, WPI_INT32 size);
WPI_INT32 wpi_GetContentWithFormat (
										WPI_CHAR *fmt_data,
										WPI_CHAR *DesPtr,
										WPI_CHAR *FormatSearch,
										WPI_CHAR delimt );


/**
 * System/Time Releated function
 */
WPI_INT32 wpi_time( WPI_VOID);
/**
 * System Network information function
 */
int wpi_get_interface_info( const char *eth, int infotype, char *rtnval, int fmttype );

/**
 * for testing
 */
int wpi_is_test_mode(void);
int wpi_GetScsHostDetailes (char *filename, char *ScsHostAddr, unsigned short	 *pi32scsPort, signed short *pi8ScsVertion );

#ifdef __cplusplus
}
#endif

#endif /*_WISEMBED_PORTING_INTEFACE_H_*/
