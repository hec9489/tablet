
/*
 * (C)opyright 2008-2009, Wisembed Co., LTD. All rights reserved.
 *
 * MCAS Porting Layer Implemenation.
 *
 */

#include "mcas_port.h"
#include "wise_port.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sched.h>
#include <pthread.h>
#include <stdarg.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <time.h>
#include <sys/time.h>

#include "util_drm_logmgr.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "Cust_Feature.h"
#include <systemproperty.h>

#define IPTV_CAS_ROOT_FOLDER  "/btf/btvmedia/cas/"

// TODO : jung2604 : 기존 mcas_type.h 에서 가져옴
typedef enum
{
    DCPLI_EFILESEEK_SET = 0,
    // �뚯?˯߽�泥?ЬӬ��湲곗��?ѬĈ offset 留뚰겿�쀫Í
            DCPLI_EFILESEEK_CUR,
    // �뚯?˯߽��꾩옱 �꾩?çќ�湲곗��쇰줿offset 留뚰겿�쀫Í
            DCPLI_EFILESEEK_END
    // �뚯?˯߽��?ڬӣ 湲곗��?ѬĈ offset 留뚰겿�쀫Í
} EFILESEEK;


/* SAK - added */
MCAS_DBG_LEVEL g_level = MCAS_DBG_ALL;

typedef struct _SemaphoreType {
    pthread_mutex_t     mutex;
    pthread_cond_t      cond;
    int                 count;
    int                 bCreated; /* to check whether the semaphore is created or not.
                                     If  bCreated  is FALSE, then semaphore is not created */
} SemaphoreType;

typedef struct _MessageType {
    unsigned int        task_id;
    MCAS_MESSAGE    *message;
    struct _MessageType *next;
} MessageType;

static SemaphoreType			CSemaphore[3] = {0};
static MessageType				*MessageRoot=0;
static pthread_mutex_t			message_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t			message_cond_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t			message_cond  = PTHREAD_COND_INITIALIZER;



/////////////////////////////////////////////
///////////////////CAS///////////////////////
/////////////////////////////////////////////
#define MAX_CAS_VIEW_COUNT	2

	// SAK - 18June2009 - Added for Predefined PMT handling
	int 				gPredefinedPMTChannelZapStatus[2] = {0,};
	int				    gMulticastServiceSwitched[2] = {0,};
	unsigned int		gMCASTChannelAudioPid[2] = {0,};
	unsigned int		gMCASTChannelVideoPid[2] = {0,};
	unsigned char 		bStopEMMDataSending2CAS[2] = {0,};

	int 				gECMCAllbackSetByCAS[2] = {0,};

	int				    gBoxBootup[2] = {0,};
	/* TODO :jung2604
    char 				i8CurrPMTVersionNo[2] = {-1,};
	unsigned char 		ui8CurrPMTVersionNoChanged[2] = {0,};
	 */
	BYTE 				gCAdes = 0;

	// SAK - 16May2009 - To identify the current channel window id
	int 				gCurrentChannelWindowId = 0;

	// SAK - 15Oct2009 - Used to store the ECM_Pid value set by CAS lib in Filter_set_pid().
	// And this value is assigned in SWDemux
	unsigned short		gECMPidSetByCAS[2] = {0,};
	
	// This variable is used to monitor the status of EMM pid filter, which is set by CAS library
	unsigned char 		bEMMPidFilterStatus[2] = {0,};

	pthread_t           TaskID = 0xFFFFFFFF;
	int 		        g_nTaskRun = 0;

// jung2604
//	WISE_GROUP_STRUCT   stGroupSt[2];
	pthread_mutex_t	pmt2cas_lock = PTHREAD_MUTEX_INITIALIZER;

	void SKCAS_SetPredefinedPMTChannelZapStatus ( unsigned long WindowId, int PredefinedPMTChannelZapStatus )
	{
	    gPredefinedPMTChannelZapStatus[WindowId] = PredefinedPMTChannelZapStatus;
	}

	int SKCAS_GetPredefinedPMTChannelZapStatus ( unsigned long WindowId )
	{
	    return gPredefinedPMTChannelZapStatus[WindowId];
	}

	void SKCAS_SetMulticastServiceSwitchedStatus ( unsigned long WindowId, int MulticastServiceSwitchedStatus )
	{
	    gMulticastServiceSwitched[WindowId] = MulticastServiceSwitchedStatus;
	}

	int SKCAS_GetMulticastServiceSwitchedStatus ( unsigned long WindowId )
	{
	    return gMulticastServiceSwitched[WindowId];
	}

	void SKCAS_SetCurrentChannelWindowID ( int CurrentWindowID )
	{
	    gCurrentChannelWindowId = CurrentWindowID;
	}

	int SKCAS_GetCurrentChannelWindowID ( void )
	{
	    return gCurrentChannelWindowId;
	}

	void SKCAS_SetMCASTChannelAudioPid ( int WindowID, unsigned int AudioPid )
	{
	    gMCASTChannelAudioPid[WindowID] = AudioPid;
	}

	unsigned int SKCAS_GetMCASTChannelAudioPid ( int WindowID )
	{
	    return gMCASTChannelAudioPid[WindowID];
	}

	void SKCAS_SetMCASTChannelVideoPid ( int WindowID, unsigned int VideoPid )
	{
	    gMCASTChannelVideoPid[WindowID] = VideoPid;
	}

	unsigned int SKCAS_GetMCASTChannelVideoPid ( int WindowID )
	{
	    return gMCASTChannelVideoPid[WindowID];
	}

	void SKCAS_SetStopEMMDataSending2CASStatus ( int WindowID, unsigned char StopEMMDataSending2CAS )
	{
	    bStopEMMDataSending2CAS[WindowID] = StopEMMDataSending2CAS;
	}

	unsigned char SKCAS_GetStopEMMDataSending2CASStatus ( int WindowID )
	{
	    return bStopEMMDataSending2CAS[WindowID];
	}
	
	void SKCAS_ECMPidSetByCAS (int WindowID, unsigned short ECMPid )
	{
			gECMPidSetByCAS[WindowID] = ECMPid;
	}
	
	unsigned short SKCAS_GetECMPidSetByCAS (int WindowID)
	{
			return gECMPidSetByCAS[WindowID];
	}

	void SKCAS_ECMCallbackSetByCAS ( int WindowID, int ECMCallbackStatus )
	{
	    gECMCAllbackSetByCAS[WindowID] = ECMCallbackStatus;
	}

	int SKCAS_GetECMCallbackSetByCAS ( int WindowID )
	{
	    return gECMCAllbackSetByCAS[WindowID];
	}
/////////////////////////////////////////////
/////////////////////////////////////////////




MessageType *findMessage(OS_DRV_TASK_ID TaskId);
void listMessage(void);

/* SAK - added */
//	int close(int fd);
//	int write(int fd, void* buf, int size);
//	int read(int fd, void* buf, int size);
//	int lseek(int fd, long offset, int pos);

#define FLASH_SIZE	(1024 * 1024)

/* SAK - 02June2009 - Changed the directory path, as per request by INNOACE */
#if 1
#define NVRAM_FILE      "/btf/btvmedia/cas/NVRAM.data"
#define NVRAM_DIR       "/btf/btvmedia/cas"
#define FLASH_FILE      "/btf/btvmedia/cas/Flash.data"
#define FLASH_DIR       "/btf/btvmedia/cas"
#else
#define NVRAM_FILE      "/DATA/SKAF/skaf_home/DATA/CAS/NVRAM.data"
#define NVRAM_DIR       "/DATA/SKAF/skaf_home/DATA/CAS"
#define FLASH_FILE      "/DATA/SKAF/skaf_home/DATA/CAS/Flash.data"
#define FLASH_DIR       "/DATA/SKAF/skaf_home/DATA/CAS"
#endif /* #if 0 */

int CreateDir(char *name);
int32 DHgetfilesize(char *szFileName);


/*************************************************************
    OS Functions
**************************************************************/

OS_DRV_TASK_ID DHTaskCreate(VOID(*MyTask)(VOID * arg), INT32 iPriority, INT32 iStackSize)
{
    pthread_t           pthread_id = 0;
    pthread_attr_t      *attr = (pthread_attr_t*) DHmalloc(sizeof(pthread_attr_t));
    struct sched_param  *sched = (struct sched_param*) DHmalloc(sizeof(struct sched_param));

//    sched->__sched_priority = iPriority; fldgo note : change for android sched

	sched->sched_priority = iPriority;
    pthread_attr_init(attr);
    pthread_attr_setstacksize(attr, iStackSize);
    pthread_attr_setschedparam(attr, sched);

    if (pthread_create(&pthread_id, attr, (void* (*)(void*))MyTask, NULL) == 0) {
#ifdef USE_TASK_DETACH
        pthread_detach(pthread_id);
#endif
        return pthread_id;
    }
    else
        return -1;
}

void DHTaskDelay(int32 iDelayTime)
{
    //LOGSRC("%s %d> DHTaskDelay Stub api", __FILE__, __LINE__);
    usleep(iDelayTime);
}

INT64 DHtime( VOID )
{
  //int64 timer = 0;
  time_t t;
  t = time(NULL);
  return t;//time(&timer);
}

#ifdef EXIT_SUPPORT
void DHSleep(uint32 iSleepTime)
{
   usleep(iSleepTime);
}
#endif

INT32 DHMessageSend(OS_DRV_TASK_ID TaskId, MCAS_MESSAGE *pstMsg)
{
    MessageType	*p;

    pthread_mutex_lock(&message_mutex);
    if (!MessageRoot) {


        MessageRoot = (MessageType*)malloc(sizeof(MessageType));
        MessageRoot->task_id = TaskId;
#ifdef MCAS_MESSAGE_LOCAL_COPY
        MessageRoot->message = (IPTV_CAS_MESSAGE*)malloc(sizeof(IPTV_CAS_MESSAGE));
        memcpy(&MessageRoot->message, pstMsg, sizeof(IPTV_CAS_MESSAGE));
#else
        MessageRoot->message = pstMsg;
#endif
        MessageRoot->next = 0;

    } else {
        p = MessageRoot;
        while (p->next) p = p->next;



        MessageType *MessageItem = (MessageType*)malloc(sizeof(MessageType));
        MessageItem->task_id = TaskId;
#ifdef MCAS_MESSAGE_LOCAL_COPY
        MessageItem->message = (IPTV_CAS_MESSAGE *)malloc(sizeof(IPTV_CAS_MESSAGE));
        memcpy(&MessageItem->message, pstMsg, sizeof(IPTV_CAS_MESSAGE ));
#else
        MessageItem->message = pstMsg;
        MessageItem->next = 0;
#endif
        p->next =  MessageItem;
    }
    pthread_mutex_unlock(&message_mutex);
    pthread_cond_broadcast(&message_cond);


    return 1;
}

MCAS_MESSAGE* DHMessageRecv(OS_DRV_TASK_ID TaskId)
{
    MessageType	*p=NULL;
    int 	iCond_status;
    struct	timespec timeout;

    //listMessage();
    pthread_mutex_lock(&message_cond_lock);
    while ((p=findMessage(TaskId)) == NULL) {


        /* 
         * SAK - 01July2009 
         * Added for testing the channel zap delay problem and to avoid continuous  
         * <Not Found Matching Message> debug message printed . 
         * Disabled the  pthread_cond_broadcast() in find message and changed pthread_cond_wait() 
         * to pthread_cond_timedwait() to avoid deadlock case 
         */
#if 1			
        pthread_cond_wait(&message_cond, &message_cond_lock);
#else
        clock_gettime(CLOCK_REALTIME, &timeout);
        timeout.tv_sec += 5;
        timeout.tv_nsec = 0;
        iCond_status = pthread_cond_timedwait(&message_cond, &message_cond_lock, &timeout);
        if (iCond_status == ETIMEDOUT) {


        } else if (iCond_status != 0) {

        }
#endif /* #if 0 */			

    }

    pthread_mutex_unlock(&message_cond_lock);

    return p->message;
}

void listMessage()
{
    MessageType *p = MessageRoot;

    while (p) {

        p = p->next;
    }
}

MessageType *findMessage(OS_DRV_TASK_ID TaskId)
{
    MessageType *root = MessageRoot;
    MessageType *p, *q = 0;

    p = root;
    pthread_mutex_lock(&message_mutex);
    while (p) {
        if (p->task_id == TaskId) {
            if (q)
                q->next = p->next;
            else
                MessageRoot = p->next;



            pthread_mutex_unlock(&message_mutex);
            if (MessageRoot)
                pthread_cond_broadcast(&message_cond);
            return p;

        } else {

        }
        q = p;
        p = p->next;
    }


    pthread_mutex_unlock (&message_mutex);
    if (MessageRoot) {
        //			pthread_cond_broadcast( &message_cond);  // SAK - 01July2009 - for testing channel zap delay
        //			pthread_cond_signal (&message_cond);
    }
    return NULL;
}

OS_DRV_SEMAPHORE DHSemaphoreCreate(uint32 ulInitialCount)
{
    /* SAK - added */
    int return_value, Semp_index=0;

    /* Check for which index the semaphore is to be created */
    for (Semp_index=0; Semp_index<3; Semp_index++) {
        if (CSemaphore[Semp_index].bCreated == FALSE) {

            break;
        }
    }

    if (Semp_index >= 3) {


    }

    return_value = pthread_mutex_init(&CSemaphore[Semp_index].mutex, NULL);
    if (return_value != 0) {

        return return_value;
    }

    pthread_cond_init(&CSemaphore[Semp_index].cond, NULL);
    CSemaphore[Semp_index].count = ulInitialCount;
    CSemaphore[Semp_index].bCreated = TRUE;   /* Mark this semaphore as created */

    return  Semp_index; /* return the semaphore id like 0, 1,2 for the respective 3 semaphores */
}

void DHSemaphoreSignal(OS_DRV_SEMAPHORE Semaphore)
{
    if (CSemaphore[Semaphore].bCreated == FALSE) {
        return;
    }

    pthread_mutex_lock(&CSemaphore[Semaphore].mutex);
    CSemaphore[Semaphore].count++;
    pthread_cond_signal(&CSemaphore[Semaphore].cond);
    pthread_mutex_unlock(&CSemaphore[Semaphore].mutex);

}

void DHSemaphoreWait(OS_DRV_SEMAPHORE Semaphore)
{
    pthread_mutex_lock( &CSemaphore[Semaphore].mutex);
    CSemaphore[Semaphore].count--;
    while (CSemaphore[Semaphore].count < 0)
	{
        pthread_cond_wait( &CSemaphore[Semaphore].cond, &CSemaphore[Semaphore].mutex);
		break;
	}
    pthread_mutex_unlock( &CSemaphore[Semaphore].mutex);

}

#ifdef EXIT_SUPPORT
void DHSemaphoreClose(OS_DRV_SEMAPHORE Semaphore)
{
    pthread_mutex_destroy( &CSemaphore[Semaphore].mutex);
    pthread_cond_destroy( &CSemaphore[Semaphore].cond);
    CSemaphore[Semaphore].count = 0;
    CSemaphore[Semaphore].bCreated = FALSE;  /* When the semaphore is destroyed,
                                                initialise to zero, so that sempahore 
                                                for that index can be created again */


}
#endif

void DHTaskClose(OS_DRV_TASK_ID task_id)
{

#ifndef USE_TASK_DETACH
    pthread_join( task_id, NULL);
#endif
}


/*****************************************************************
  File I/O functions
******************************************************************/
FILE_HANDLE DHFileOpen(char const *szFileName, EFILEOPEN eFileOpenMode)
{
    int 	mode = 0;

    switch (eFileOpenMode) {
        case DCPLI_EFILEOPEN_EXIST_READONLY:
            mode = O_RDONLY;
            break;
        case DCPLI_EFILEOPEN_EXIST_WRITEONLY:
            mode = O_WRONLY;
            break;
        case DCPLI_EFILEOPEN_EXIST_READWRITE:
            mode = O_RDWR;
            break;
        case DCPLI_EFILEOPEN_TRUNC_WRITEONLY:
            mode = O_WRONLY | O_TRUNC | O_CREAT;
            break;
        case DCPLI_EFILEOPEN_TRUNC_READWRITE:
            mode = O_RDWR | O_TRUNC | O_CREAT;
            break;
        case DCPLI_EFILEOPEN_NEW_WRITEONLY:
            mode = O_WRONLY | O_CREAT;
            break;
        case DCPLI_EFILEOPEN_NEW_READWRITE:
            mode = O_RDWR | O_CREAT;
            break;
    }
    return wpi_open((char*)szFileName, mode);
}

int32 DHFileClose(FILE_HANDLE handle)
{
    return wpi_close2(handle);
}

int32 DHFileRemove(char const *szFileName)
{
    return wpi_remove((char*)szFileName);
}

int32 DHFileRead(FILE_HANDLE handle, byte* pbBuffer, int32 isize)
{
    return wpi_read(handle, pbBuffer, isize);
}

int32 DHFileWrite(FILE_HANDLE handle, byte* pbBuffer, int32 isize)
{
    return wpi_write(handle, pbBuffer, isize);
}

int32 DHFileSeek(FILE_HANDLE handle, int32 ioffset, EFILESEEK whence)
{
    return wpi_seek(handle, ioffset, whence);
}

int32 DHFileTell(FILE_HANDLE handle)
{
    return wpi_tell(handle);
}

int32 DHmkdir(char *dirname)
{
    return wpi_mkdir_p(dirname);
}

int32 DHrmdir(char *dirname)
{
    return wpi_rmdir(dirname);
}

int32 DHgetfilesize(char *szFileName)
{
    return wpi_size(szFileName);
}

int32 DHFileFlush(FILE_HANDLE handle)
{
    return wpi_flush(handle);
}

int32 DHFileCheck(char *filename)
{
    return wpi_exist(filename);
}

int32 DHFileRename(char *oldfilename, char *newfilename)
{
    return wpi_rename(oldfilename, newfilename);
}

int CreateDir(char *name)
{
    int     status = 0;
    char    *p, *q, *path = strdup(name);
    struct stat     st = {0,};

    q = p = 0;
    do {
        q = p;

        if ((status=mkdir(path, 0755)) < 0) {
            if (errno == ENOENT) {
                p = strrchr(path, '/');
                if(p)
                    *p = '\0';
                if(q)
                    *q = '/';
            } else if (errno == EEXIST) {
                if (q && *q !='/')
                    *q = '/';
                else
                    break;
            }
        } else {
            if (q)
                *q = '/';
            else
                break;
        }
    } while(1);

    free(path);

    if (stat(name, &st) == 0) {

        return 0;
    } else {

        return -1;
    }
}


/* SAK -added the following api's to store and retrieve the data from flash and NVRAM */
/*************************************************************************
  NVRAM & Flash Memory Access Functions
**************************************************************************/

/*
 *	NVRAM ==> /DATA/CAS/NVRAM  file
 */
BOOL DHNVRAMRead(byte *read_buf, uint16 offset, uint16 length)
{
    int fp;
	
	char dataPath[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
	strcat(dataPath, NVRAM_FILE);	

    fp = open(dataPath, O_RDONLY);
    if (fp == -1) {

        return 0;
    }

    lseek(fp,offset, DCPLI_EFILESEEK_SET);
    read(fp,read_buf,length);
    close(fp);


    return 1;
}

BOOL DHNVRAMWrite(byte *write_buf, uint16 offset,uint16 length)
{
    int fp;

	char dataPath[128];
	char dataDir[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
	strcat(dataPath, NVRAM_FILE);
	sprintf(dataDir, "%s%s", dataPath, NVRAM_DIR);
	
    fp = open(dataPath, O_WRONLY);
    if (fp == -1) {

    	// 2012.06.26. 파일 생성시 권한을 줌
        fp = open(dataPath, O_WRONLY | O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
        if (fp == -1) {

            CreateDir(dataDir);
            fp = open(dataPath, O_WRONLY | O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);

            if (fp != -1) {
                lseek(fp, FLASH_SIZE,DCPLI_EFILESEEK_SET);
                write(fp, "0", 1);

            } else {

                return 0; /* Check whether we need to return here or not */
            }
        }
    }

    lseek(fp, offset, DCPLI_EFILESEEK_SET);
    write(fp, write_buf, length);
    close(fp);


    return 1;
}

/*
 * FLASH ==> /DATA/CAS/Flash
 */
BOOL DHFlashRead(uint32 address, byte *read_buf, uint32 length)
{
    int fp;
    uint32 Offset;

	
	char dataPath[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
	strcat(dataPath, FLASH_FILE);	

    Offset = address - 0x800000;
    fp = open(dataPath, O_RDONLY);
    if (fp == -1) {

        return 0;
    }

    lseek(fp, Offset, DCPLI_EFILESEEK_SET);
    read(fp, read_buf, length);
    close(fp);


    return 1;
}

BOOL DHFlashWrite(uint32 address, byte *write_buf, uint32 length)
{
    int fp;
    uint32 Offset;

	char dataPath[128];
	char dataDir[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
	strcat(dataPath, FLASH_FILE);
	sprintf(dataDir, "%s%s", dataPath, FLASH_DIR);

    Offset = address - 0x800000;

    fp = open(dataPath, O_WRONLY);
    if (fp == -1) {

    	// 2012.06.26. namsj 권한 설정
        fp = open(dataPath, O_WRONLY | O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
        if (fp == -1) {

            CreateDir(dataDir);
            fp = open(dataPath, O_WRONLY | O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);

            if (fp != -1) {
                lseek(fp, FLASH_SIZE,DCPLI_EFILESEEK_SET);
                write(fp, "0", 1);

            } else {

                return 0;
            }
        }
    }

    lseek(fp, Offset, DCPLI_EFILESEEK_SET);
    write(fp, write_buf, length);
    close(fp);


    return 1;
}

/*
 * Base Flash Addr use fixed value 0x800000
 * size => +1MB (1024*1024)
 */
BOOL DHFlashGetAddress(uint32 *paddr, uint32 *size)
{
    *paddr = (uint32) 0x800000;
    *size = (1024 * 1024);		/*1MB*/
    return 1;
}

void *DHmalloc(uint32 ulSize)
{
    return wpi_malloc(ulSize);
}

void  DHfree(void* pvMemory)
{
    wpi_free(pvMemory);
}

VOID DBGMSG(MCAS_DBG_LEVEL DbgLevel, const CHAR * szFmt, ...)
{
#if 1
	 // cas 라이브러리 로그 출력
	 va_list  ap;
	 char buffer[1024];

	 va_start( ap, szFmt);
	 vsprintf( buffer, szFmt, ap);
	 va_end(ap);

	 buffer[strlen(buffer)-1] = '\0';
	 DPRINTSRC("DBGMSG level(%d) %s", DbgLevel, buffer);
	 printf("DBGMSG level(%d) %s\n", DbgLevel, buffer);
#endif
}


/**************************************************************
  check leap year
***************************************************************/

/* SAK - 29092008 - Added this api, to check the given input year is leap year or not */
unsigned char checkIsLeapYear (unsigned short yr)
{
    if ((yr%4==0 && yr%100!=0) || yr%400==0) {

        return 1;
    } else {

        return 0;
    }
}

/* SAK - 29092008 - Added this api, to returns the total no. of seconds between  0 hour, January 1st, 1970 (base date) and the MCAS generic time */
uint32 DHmcasmktime(MCAS_GenericTime *tm)
{
    int leap_year_days[] = { 31,29,31,30,31,30,31,31,30,31,30,31 }; /* no of days in a month */
    int non_leap_year_days[] = { 31,28,31,30,31,30,31,31,30,31,30,31 }; /* no of days in a month */

    uint32 seconds = 0;
    unsigned short start_year = 1970,
                   end_year,
                   start_month = 01,
                   end_month;
    unsigned char   start_date = 01,
                    end_date,
                    month=12,
                    date,
                    i =0;



    /* Base date is : January 1st,1970 (01/01/1970) */

    end_year = tm->nYear;
    end_month = tm->nMonth;
    end_date = tm->nDay;

    while ( start_year <= end_year ) {
        /* Calculate no. of seconds for the current year */
        if ( start_year == end_year ) {
            i =0;

            /* Check the input variable datatype */
            if (checkIsLeapYear ( start_year )) {
                for (  i = 0; i < (end_month -start_month); i++) {
                    /* Calculate no. of seconds in year containing leap year */
                    seconds += leap_year_days[i] * 24 * 60 * 60;
                }
            } else {
                for (i = 0; i < (end_month -start_month); i++) {
                    /* Calculate no. of seconds in year not containing leap year */
                    seconds += non_leap_year_days[i] * 24 * 60 * 60;
                }
            }

            /* Calculate no. of seconds for the no. of  days in the current month which is given as input*/
            date = end_date - start_date;
            seconds += date * 24 * 60 * 60;
        } else {
            /* Calculate no. of seconds in other than the current year */

            i =0;
            if (checkIsLeapYear ( start_year )) {
                for (i = 0; i < (month -1); i++) {
                    /* Calculate no. of seconds in year containing leap year */
                    seconds += leap_year_days[i] * 24 * 60 * 60;
                }
            } else {
                for (i = 0; i < (month -1); i++) {
                    /* Calculate no. of seconds in year not containing leap year */
                    seconds += non_leap_year_days[i] * 24 * 60 * 60;
                }
            }
        }
        start_year++;
    }

    /* Calculate no. of seconds for the current hr, min and seconds given as input */
    seconds += ((tm->nHour * 60 * 60) + (tm->nMin * 60) + tm->nSec);




    return seconds;
}

INT32 DHsprintf(BYTE *buffer, const CHAR *format, ...)
{
    int 	n=0;
    va_list  ap;

    va_start( ap, format);
    n = vsprintf( (char*)buffer, format, ap);
    va_end(ap);
    return n;
}

//STBID means STB's HW MAC Adress 6bytes
int32 DHGetSTB_ID(byte *recv_buf, int32 *length)
{
    int 		fd;
    struct ifreq ifrq;

    *length = 0;

    if ((fd=socket(AF_INET, SOCK_DGRAM, 0)) < 0)
        return fd;
    strcpy(ifrq.ifr_name, "eth0"); //default eth0
    if (ioctl(fd, SIOCGIFHWADDR, &ifrq) < 0)
        return -1;
    memcpy(recv_buf, ifrq.ifr_addr.sa_data, 6);
    *length = 6;
    return 0;
}

// for EMM reply using TCP channel
int32 DHSocketDirectSend (char *hostname, int16 port, byte *send_buf,
        int32 send_len, byte *recv_buf, int32 *recv_len)
{
    int fd = -1, read_len;
    struct sockaddr_in pin = {0,};
    struct hostent *hp = NULL;

    if ((fd = socket( AF_INET, SOCK_STREAM, 0)) < 0)
        return fd;
	// 2012.02.10 namsj gethostbyname() 이 실패할 경우가 있다.
	// 인터넷에 찾아 보면 대략 실제 호스트가 많을 경우 그럴 때가 있다는 것 같다.
	// gethostbyname2()를 사용하게 되면 잘 얻어 진다.
    //if ((hp = gethostbyname(hostname)) == NULL)
    if ((hp = gethostbyname2(hostname, AF_INET)) == NULL)
        return -1;

    pin.sin_family = AF_INET;
    pin.sin_port = htons(port);
    pin.sin_addr.s_addr = ((struct in_addr*)(hp->h_addr))->s_addr;
    if (connect(fd, (struct sockaddr *)&pin, sizeof(pin)) < 0) {
        close( fd);
        return -1;
    }

    if (send(fd, send_buf, send_len, 0) < 0) {
        close(fd);
        return -1;
    }

    if ((read_len = recv(fd, recv_buf, *recv_len, 0)) < 0) {
        close(fd);
        return -1;
    }
    *recv_len = read_len;

    close(fd);
    return 0;
}


INT16 Filter_Init(VOID)
{
	//LOGSRC("STUB API : Filter_Init");
	//printf("STUB API : Filter_Init\n");
	return 0;
}

void SetEMMPidFilterStatus ( unsigned char EMMPidFilterStatus, int WindowId )
{
    bEMMPidFilterStatus[WindowId] = EMMPidFilterStatus;
}

unsigned char GetEMMPidFilterStatus ( int WindowId )
{
	return bEMMPidFilterStatus[WindowId];
}

VOID filter_set_pid(BYTE emm_ecm, UINT16 view_id, UINT16 pid)
{
	//printf("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n");
	//printf("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n");
	//printf("STUB API : filter_set_pid emm_ecm(%d) pid(%d) view_id(%d)\n", emm_ecm, pid, view_id);
	//printf("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n");
	//printf("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n");

        //LOGSRC("Pid[%d : 0x%04X] Type[%X]\n", pid, pid, emm_ecm);

	// The first argument "emm_ecm" value is only used incase of h/w section filter.
	//  In case of s/w section filter, this argument is not used
	//  Check here, if the PID passed is EMM or ECM.
	//  The EMM PID will be a fixed one.
	//  So, if PID is EMM go for s/w section filtering.
	//  If the PID is ECM, the  go for h/w section filtering */

	if(view_id >= MAX_CAS_VIEW_COUNT) return;

        if (  pid == 5000 && emm_ecm == IB_EMM ){
		SetEMMPidFilterStatus ( TRUE , view_id );
		SKCAS_SetStopEMMDataSending2CASStatus ( view_id , FALSE );
        }
        else if ( emm_ecm == IB_ECM ){
		SKCAS_ECMPidSetByCAS ( view_id , pid );
		SKCAS_ECMCallbackSetByCAS ( view_id , TRUE);
    }

    return;
}


VOID filter_unset_pid(BYTE emm_ecm, UINT16 view_id, UINT16 pid)
    {
	//printf("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
	//printf("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
	//printf("STUB API : filter_unset_pid emm_ecm(%d) pid(%d) view_id(%d)\n", emm_ecm, pid, view_id);
	//printf("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
	//printf("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");

	if(view_id >= MAX_CAS_VIEW_COUNT) return;

	if ( pid == 5000 ){
		SetEMMPidFilterStatus ( FALSE , view_id );
		SKCAS_SetStopEMMDataSending2CASStatus ( view_id , TRUE );
        }
        else {/* ECM - pid */
            /* When filter_unset_pid () is called  from the CAS library, 
             * initialise the ecm_pid and the ECMCallback function */		
		SKCAS_ECMPidSetByCAS ( view_id , 0x1fff );
		SKCAS_ECMCallbackSetByCAS ( view_id , FALSE );
    }

    return;	
}

BOOL SXPLI_GetStoragePath( CHAR *path, UINT32 *path_len, UINT32 buf_len ) {
    //TODO : 안드로이드 세탑의 위치 char rootDir[] = "/data/skb_data/DATA/cas";
    //char rootDir[] = "/data/user/0/com.skb.framework.myapplication/btf/btvmedia/cas/";
    char dataPath[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
	
    char rootDir[256]; 
	sprintf(rootDir, "%s%s", dataPath, IPTV_CAS_ROOT_FOLDER);
    *path_len = strlen(rootDir);
    memcpy(path, rootDir, *path_len);

    DPRINTSRC("SXPLI_GetStoragePath %s", path);
    return TRUE;
}

#ifdef __cplusplus
}
#endif
