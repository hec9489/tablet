#ifndef _______UTIL_DRM_LOGMGR_H__________
#define _______UTIL_DRM_LOGMGR_H__________

#define ENABLE_DEBUG_PRINT

#ifdef ENABLE_DEBUG_PRINT

#include <android/log.h>

#define DEBUG_PRINT(__X__)	__X__
#define DPRINTSRC(args...) __android_log_print(ANDROID_LOG_DEBUG  , "BOXKEY-DRM", ##args)
#define DPRINTSOCKET DPRINTSRC
#define LOGSRC DPRINTSRC
#define DPRINTMEM(...)
//#define DPRINTSRC(a, b, c)

//define	LOG_TIME_STARTV DPRINTSRC
//#define	LOG_TIME_START DPRINTSRC
//#define LOG_TIME_END  DPRINTSRC

#define LOG_TIME_LOG(args...) DPRINTSRC(##args)
#define LOG_TIME(time, args...) DPRINTSRC((int)time, ##args)

#define	LOG_TIME_STARTV(args...)	\
{ \
	struct timeval time; \
	long lStart, lEnd; \
	gettimeofday(&time, NULL);\
	LOG_TIME((-1), ##args); \
	lStart = time.tv_sec*1000 + time.tv_usec/1000;

#define	LOG_TIME_START()	\
{ \
	struct timeval time; \
	long lStart, lEnd; \
	gettimeofday(&time, NULL);\
	lStart = time.tv_sec*1000 + time.tv_usec/1000;

#define LOG_TIME_END(args...)	\
	gettimeofday(&time, NULL);\
	lEnd = time.tv_sec*1000 + time.tv_usec/1000;\
	LOG_TIME((lEnd -lStart), ##args); \
}
#else
#define DEBUG_PRINT(__X__)
#define DPRINTSRC
#define DPRINTMEM(...)
#endif


#endif //_______UTIL_DRM_LOGMGR_H__________
