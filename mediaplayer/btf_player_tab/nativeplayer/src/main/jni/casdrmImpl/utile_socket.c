/*
 * utile_socket.c
 *
 *  Created on: 2011. 8. 25.
 *      Author: alexchoi
 */
#include <sys/time.h>
#include "include/utile_socket.h"
#include "util_drm_logmgr.h"

long _getStartTime()
{
	struct timeval time;
	gettimeofday(&time, NULL);
	return ( time.tv_sec*1000 + time.tv_usec/1000 );
}

int _checkIsTimeout(int nTimeout, long lStart, int *pnSpare)
{
	struct timeval time;
	long lEnd = 0L;
	int nTurnaround = 0;
	int nSpare = 0;

	gettimeofday(&time, NULL);
	lEnd = time.tv_sec*1000 + time.tv_usec/1000;
	nTurnaround = (int)(lEnd - lStart);
	nSpare = nTimeout - nTurnaround;
	if (nSpare < 0) {
		nSpare = 0;
	}

	*pnSpare = nSpare;

	return 0;
}

int _setRevcTimeout(int sockfd, int nTimeout_msec);
int _setSendTimeout(int sockfd, int nTimeout_msec);

int _setSendTimeout(int sockfd, int nTimeout_msec)
{
	int 	nRet = 0;
	struct 	timeval tval;

	if(0 >=  nTimeout_msec){
		nTimeout_msec = TIMEOUT_DEFAULT_MS;
	}

	tval.tv_sec     = nTimeout_msec/1000;
	tval.tv_usec    = (nTimeout_msec%1000)*1000;


	nRet = setsockopt( sockfd, SOL_SOCKET, SO_SNDTIMEO, &tval, sizeof( tval ));
	if(0 != nRet){
		DPRINTSOCKET("ERROR:setsockopt sockfd(%d) nRet(%d)", sockfd, nRet);
		return nRet;
	}

	return nRet;
}

int _setRevcTimeout(int sockfd, int nTimeout_msec)
{
	int 	nRet = 0;
	struct 	timeval tval;

	if(0 >=  nTimeout_msec){
		nTimeout_msec = TIMEOUT_DEFAULT_MS;
	}

	tval.tv_sec     = nTimeout_msec/1000;
	tval.tv_usec    = (nTimeout_msec%1000)*1000;

	nRet = setsockopt( sockfd, SOL_SOCKET, SO_RCVTIMEO, &tval, sizeof( tval ));
	if(0 != nRet){
		DPRINTSOCKET("ERROR:setsockopt sockfd(%d) nRet(%d)", sockfd, nRet);
		return nRet;
	}

	return nRet;
}

int 	bindEx(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
	int nRet = -1;
	nRet = bind(sockfd, addr, addrlen);
	return nRet;
}
int 	listenEx(int sockfd, int backlog)
{
	int nRet = -1;
	nRet = listen(sockfd, backlog);
	return nRet;
}

int 	acceptEx(int sockfd, struct sockaddr *cliaddr, socklen_t *addrlen, int nTimeout_msec)
{
	int nRet = -1;

	nRet = _setRevcTimeout(sockfd, nTimeout_msec);
	if(0 != nRet){
		return nRet;
	}

	nRet = accept(sockfd, cliaddr, addrlen);
	return nRet;
}

int  connectEx(int sockfd, const struct sockaddr *serv_addr, socklen_t addrlen, int nTimeout_msec)
{
	int nRet = -1;

	if(0 > nTimeout_msec){
		// Ÿ�� �ƿ� ��� ���� �ʴ� ���
		nRet = connect(sockfd, serv_addr, addrlen);
		return nRet;
	}
	else{
		int orgSockStat;
		fd_set  rset, wset;
		struct timeval tval;
		struct timeval *to;

		int error = 0;
		int esize;

		nRet = fcntl(sockfd, F_GETFL, NULL);
		if(0 > nRet){
			DPRINTSOCKET("F_GETFL error");
			return EACCES;
		}

		orgSockStat = nRet;
		nRet |= O_NONBLOCK;

		// Non blocking ���·� �����.
		nRet = fcntl(sockfd, F_SETFL, nRet);
		if(0 > nRet){
			DPRINTSOCKET("F_SETLF error");
			return EACCES;
		}

		// ������ ��ٸ���.
		// Non blocking �����̹Ƿ� �ٷ� �����Ѵ�.
		nRet = connect(sockfd, serv_addr, addrlen);
		if(0 > nRet){
			if (errno != EINPROGRESS){
				fcntl(sockfd, F_SETFL, orgSockStat);
				return nRet;
			}
		}

		// ��� ������ �������� ��� ������ ���� ���·� �ǵ����� �����Ѵ�.
		if (0 == nRet){
			DPRINTSOCKET("Connect Success");
			fcntl(sockfd, F_SETFL, orgSockStat);
			return nRet;
		}

		FD_ZERO(&rset);
		FD_SET(sockfd, &rset);
		wset = rset;

		tval.tv_sec     = nTimeout_msec/1000;
		tval.tv_usec    = (nTimeout_msec%1000)*1000;
		to = &tval;

		nRet = select(sockfd+1, &rset, &wset, NULL, to);
		fcntl(sockfd, F_SETFL, orgSockStat);
		if(0 == nRet){
			// timeout
			// fldgo note : ��ܿ��� errno �ٷ� üũ���� �ʰ� �ִ�. �ٸ� thread �� errno �� ������ �� ���ɼ� �ִ�.
			// errno = ETIMEDOUT; 
			DPRINTSOCKET("Connect timeout");
			// return ETIMEDOUT; 
			// fldgo note : ETIMEDOUT value �� ����̴�. 
			// ��ü������ �ҽ� �帧�� ������ ��쿡�� ����üũ�� �����Ǿ� �ִ�. 
			// connectEX api ������ -1 �� �����ϴ°� �����ϴ�.
			return -1; 
		}

		// �аų� �� �����Ͱ� �ִ��� �˻��Ѵ�.
		nRet = -1;
		// fldgo note: �ϱ� ��ƾ�� time out �� �ƴ� connect fail �� send/recv ��𿡼� ���� ������ ������ �߻��ߴ��� 
		// error ������ ������ errno �� set �ϴ� ��ƾ�̴�. 
		// �ݵ�� �ʿ��� ������ �ƴ����� ���ܵд�.
		if (FD_ISSET(sockfd, &rset) || FD_ISSET(sockfd, &wset) ){
			esize = sizeof(int);
			nRet = getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &error, (socklen_t *)&esize);
		}
		else{
			return -1;
		}


		if(error){
			// fldgo note : ��ܿ��� errno �ٷ� üũ���� �ʰ� �ִ�. �ٸ� thread �� errno �� ������ �� ���ɼ� �ִ�.
			//errno = error;
			return -1;
		}

		DPRINTSOCKET("Connect Success");
		return 0;
	}
}


int  recvfromEx(int sockfd, void *buf, int len, int flags, struct sockaddr *from, socklen_t *fromlen, int nTimeout_msec)
{
	int nRet = -1;


	nRet = _setRevcTimeout(sockfd, nTimeout_msec);
	if(0 != nRet){
		return nRet;
	}

	nRet = recvfrom(sockfd, buf, len, flags, from, fromlen);

	return nRet;
}
int  sendtoEx(int  sockfd,  const  void *buf, int len, int flags, const struct sockaddr *to, socklen_t tolen, int nTimeout_msec)
{
	int nRet = -1;


	nRet = _setSendTimeout(sockfd, nTimeout_msec);
	if(0 != nRet){
		return nRet;
	}

	nRet = sendto(sockfd,  buf, len, flags, to, tolen);

	return nRet;
}

int	 recvEx(int sockfd, char *buffer, int buffer_length, int flags, int nTimeout_msec)
{
	int nRet = -1;


	nRet = _setRevcTimeout(sockfd, nTimeout_msec);
	if(0 != nRet){
		return nRet;
	}

	nRet = recv(sockfd, buffer, buffer_length, flags);
	return nRet;
}

int	 recvDaemon(int sockfd, char *buffer, int buffer_length, int flags, int nTimeout_msec)
{
	long lStart = 0L;
	int nTotals = 0;
	int nTimeout = 0;
	int nLen = 0;
	char *p = buffer;


	// timeout �̼��� �� default ������ ����.
	if(0 >=  nTimeout_msec) {
		nTimeout = TIMEOUT_DEFAULT_MS;
	}
	else {
		nTimeout = nTimeout_msec;
	}

	do
	{
		// socket timeout ����
		if ( 0 != _setRevcTimeout(sockfd, nTimeout) ) {
			return -1; // T_E_ERROR
		}

		// recv
		//DPRINTSOCKET("[fd=%d] nTimeout=%d buffer_length=%d\n", sockfd, nTimeout, buffer_length);
		lStart = _getStartTime();
		nLen = recv(sockfd, p, buffer_length, flags);
		if (nLen == 0) {
			DPRINTSOCKET("client[fd=%d]: lost connection to daemon manager", sockfd);
			return -1; // T_E_ERROR
		}

		// recv error
		if (nLen < 0) {
			int nSpare = 0;
			_checkIsTimeout(nTimeout, lStart, &nSpare);
			nTimeout = nSpare;
			// timeout (errno üũ �κ� ���� ���� �;���)
			if (nTimeout <= 0) {
				DPRINTSOCKET("client[fd=%d]: timeout!!!!!!!!!!!!!!!!", sockfd);
				return -20; // T_E_TIMEOUT
			}

			// other interrupt
			if (errno == EAGAIN || errno == EINTR) {
				DPRINTSOCKET("client[fd=%d]: (errno == EAGAIN || errno == EINTR) errno=%d", sockfd, errno);
				continue;
			}

			DPRINTSOCKET("client[fd=%d]: failed to write data to daemon manager (errno:%d)", sockfd, errno);
			return -1;
		}

		// recv ok
		buffer += nLen;
		buffer_length -= nLen;
		nTotals += nLen;
	}
	while (buffer_length > 0);

	return nTotals;
}

int	 sendEx(int sockfd, char *buffer, int buffer_length, int flags, int nTimeout_msec)
{
	int nRet = -1;


	nRet = _setSendTimeout(sockfd, nTimeout_msec);
	if(0 != nRet){
		return nRet;
	}

	nRet = send(sockfd, buffer, buffer_length, flags);

	return nRet;
}

int  shutdownEx(int sockfd)
{
	return close(sockfd);

}
