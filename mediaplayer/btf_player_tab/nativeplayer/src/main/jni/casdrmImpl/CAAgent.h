/*
*       infomation      : MCurix Inc. HanaTV CA Agent Library
*       version         : 0.9.0.1
*       creator         : hmkang@mcurix.com
*       create date     : 2008.01.05
*       modifier        : 2008.09.12
*       lastmodify      :
*/

#ifndef _DRM_CA_AGENT_
#define _DRM_CA_AGENT_
#endif

#include <sys/types.h>
#include "mcas_types.h"

#ifdef __cplusplus
extern "C" {
#endif

//Service ID
#define DCA_SERVICEID_DRM               0x1000

// Cert Type
#define DCA_CERT_DRM                    71
#define DCA_CERT_CAS                    72

#define DCA_UID_CAS                     "CAS"
#define DCA_UID_DRM                     "DRM"

#define MAX_UID_LEN                     32

typedef struct _CIS_USER_INFO
{
        int     size;           // CIS_USER_INFO structure size
        int     CertType;
        char*   UserID;
        int     otp;
        unsigned char*  pVID;   // Unused
        int     len_vid;        // Unused
} CIS_USER_INFO;

typedef void (*dca_RESULTCB)(int resultCode, void* param);
int dca_RequestUserCert(unsigned int ServiceID, CIS_USER_INFO* pUserInfo,
                         char* szCAUrl, dca_RESULTCB cb, void* param);

int dca_CertVerify(UINT32 nSID, CHAR* szUserID, INT32 currDateTime);

int dca_CertStorageInit(UINT32 ServiceID);
#ifdef __cplusplus
}
#endif
