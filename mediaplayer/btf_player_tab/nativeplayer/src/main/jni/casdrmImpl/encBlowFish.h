////////////////////////////////////////////////////////////////////
/*  Progrmer : Ha, Sun Yong
    last-date : 2006.12.21
	Company   : Hana-Media  */
////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifndef __ENCBLOWFISH_H__
#define __ENCBLOWFISH_H__
#define BLOWFISH_ENCODE	1
#define BLOWFISH_DECODE	2


struct t0 {
  unsigned long P[16+2];
  unsigned long S[4][256];
};

typedef struct t0 BLOWFISH_CTX;

int BlowFish_MakeKey(char * Request_Header,unsigned char *key);
unsigned long BlowFish_Func (unsigned long x);
void Blowfish_Encrypt (unsigned long *xl,unsigned long *xr);
void Blowfish_Decrypt (unsigned long *xl,unsigned long *xr);
void Blowfish_Init (unsigned char *key,int keyLen);
void Blowfish_Encode(char *szString, unsigned int strLen);
void Blowfish_Decode(char * szString, unsigned int size);
unsigned char *Blowfish_Base64Encode(const unsigned char *str, int length, int *ret_length);
unsigned char *Blowfish_Base64Decode(const unsigned char *str, int length, int *ret_length) ;
void do_blowfish( unsigned char type, tpacket *packet );

#endif /* __ENCBLOWFISH_H__ */

