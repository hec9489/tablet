# Copyright 2019 SK Broadband Co., LTD.
# Author: Kwon Soon Chan (sysc0507@sk.com)

LOCAL_PATH:= $(call my-dir)

##############################
include $(CLEAR_VARS)
LOCAL_MODULE := libdrm
LOCAL_SRC_FILES := ./lib/drm/release/libdrm.a
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_SUFFIX := .a
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
include $(PREBUILT_STATIC_LIBRARY)

##############################
include $(CLEAR_VARS)
LOCAL_MODULE := libDCSDB
LOCAL_SRC_FILES := ./lib/drm/release/libDCSDB.a
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_SUFFIX := .a
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
include $(PREBUILT_STATIC_LIBRARY)

##############################
include $(CLEAR_VARS)
LOCAL_MODULE := libDRMCrypto
LOCAL_SRC_FILES := ./lib/drm/CALibrary/libDRMCrypto.a
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_SUFFIX := .a
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
include $(PREBUILT_STATIC_LIBRARY)


##############################
#include $(CLEAR_VARS)
#LOCAL_MODULE := libcurl
#LOCAL_SRC_FILES := ./lib/curl/$(TARGET_ARCH_ABI)/libcurl.a
#LOCAL_MODULE_TAGS := optional
#LOCAL_MODULE_SUFFIX := .a
#LOCAL_MODULE_CLASS := STATIC_LIBRARIES
#include $(PREBUILT_STATIC_LIBRARY)

##############################
include $(CLEAR_VARS)
LOCAL_MODULE := libcurl
LOCAL_SRC_FILES := ../libs/libcurl.so
LOCAL_MODULE_TAGS := optional
include $(PREBUILT_SHARED_LIBRARY)

##############################
include $(CLEAR_VARS)
LOCAL_MODULE := systemproperty
LOCAL_SRC_FILES := ../libs/libsystemproperty.so
LOCAL_MODULE_TAGS := optional
include $(PREBUILT_SHARED_LIBRARY)


###########################################################################################
# libskcasdrm
###########################################################################################
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_CFLAGS:= -DEXIT_SUPPORT
APP_LDFLAGS := -latomic -Wl,--allow-multiple-definition

LOCAL_SRC_FILES:= \
	./utile_socket.c \
	./drmp_port_impl.c \
	./wise_port_impl.c \
	./drm_crypto_impl.c \
	./mcas-port-impl.c \
	./encBlowFish.c \
	./wise_skdrm.c \
	./scs-trigger-handler.c \
	./jsmn.c \
	./utile_wscs.c \
	./json.c \


LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/lib/curl/include \
	$(LOCAL_PATH)/libs/casdrmImpl	\
	$(LOCAL_PATH)/include \
	$(LOCAL_PATH)/../systemproperty

LOCAL_CPP_INCLUDES := \
	$(LOCAL_PATH)/../systemproperty

LOCAL_SHARED_LIBRARIES := \
        libsystemproperty \
        libcurl

LOCAL_WHOLE_STATIC_LIBRARIES := \
        libdrm \
	libDCSDB \
	libDRMCrypto


LOCAL_LDLIBS := -llog

LOCAL_MODULE := libskcasdrm

#LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_SHARED_LIBRARY)
