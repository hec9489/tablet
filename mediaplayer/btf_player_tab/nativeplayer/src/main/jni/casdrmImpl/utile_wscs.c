/*
 * utile_wscs.c
 *
 *  Created on: 2015. 05. 07.
 *      Author: JIMIN
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <curl/curl.h>
#include "json.h"
#include "scs-protocol.h"
#include "utile_wscs.h"

#include <utile_wscs.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <time.h>
#include <systemproperty.h>
#if 0
#include <setjmp.h>
#include <pthread.h>
#endif
//#include "SKAF_typedef.h"
//#include "hal/skaf_hsys.h"

#include "util_drm_logmgr.h"

FILE* file = NULL;

typedef unsigned char uint8;
#ifndef  uint32
typedef unsigned int  uint32;
#endif
typedef unsigned char  TByte;

//sha256
#define GET_UINT32(n,b,i)                       \
{                                               \
    (n) = ( (uint32) (b)[(i)    ] << 24 )       \
        | ( (uint32) (b)[(i) + 1] << 16 )       \
        | ( (uint32) (b)[(i) + 2] <<  8 )       \
        | ( (uint32) (b)[(i) + 3]       );      \
}

#define PUT_UINT32(n,b,i)                       \
{                                               \
    (b)[(i)    ] = (uint8) ( (n) >> 24 );       \
    (b)[(i) + 1] = (uint8) ( (n) >> 16 );       \
    (b)[(i) + 2] = (uint8) ( (n) >>  8 );       \
    (b)[(i) + 3] = (uint8) ( (n)       );       \
}


typedef struct
{
	unsigned long int total[2];
	unsigned long int state[8];
	unsigned char buffer[64];
}
sha256_context;

/**
@brief  sha256
*/
void sha256_starts( sha256_context *ctx )
{
    ctx->total[0] = 0;
    ctx->total[1] = 0;

    ctx->state[0] = 0x6A09E667;
    ctx->state[1] = 0xBB67AE85;
    ctx->state[2] = 0x3C6EF372;
    ctx->state[3] = 0xA54FF53A;
    ctx->state[4] = 0x510E527F;
    ctx->state[5] = 0x9B05688C;
    ctx->state[6] = 0x1F83D9AB;
    ctx->state[7] = 0x5BE0CD19;
}

void sha256_process( sha256_context *ctx, unsigned char data[64] )
{
	unsigned long int temp1, temp2, W[64];
	unsigned long int A, B, C, D, E, F, G, H;

    GET_UINT32( W[0],  data,  0 );
    GET_UINT32( W[1],  data,  4 );
    GET_UINT32( W[2],  data,  8 );
    GET_UINT32( W[3],  data, 12 );
    GET_UINT32( W[4],  data, 16 );
    GET_UINT32( W[5],  data, 20 );
    GET_UINT32( W[6],  data, 24 );
    GET_UINT32( W[7],  data, 28 );
    GET_UINT32( W[8],  data, 32 );
    GET_UINT32( W[9],  data, 36 );
    GET_UINT32( W[10], data, 40 );
    GET_UINT32( W[11], data, 44 );
    GET_UINT32( W[12], data, 48 );
    GET_UINT32( W[13], data, 52 );
    GET_UINT32( W[14], data, 56 );
    GET_UINT32( W[15], data, 60 );

#define  SHR(x,n) ((x & 0xFFFFFFFF) >> n)
#define ROTR(x,n) (SHR(x,n) | (x << (32 - n)))

#define S0(x) (ROTR(x, 7) ^ ROTR(x,18) ^  SHR(x, 3))
#define S1(x) (ROTR(x,17) ^ ROTR(x,19) ^  SHR(x,10))

#define S2(x) (ROTR(x, 2) ^ ROTR(x,13) ^ ROTR(x,22))
#define S3(x) (ROTR(x, 6) ^ ROTR(x,11) ^ ROTR(x,25))

#define F0(x,y,z) ((x & y) | (z & (x | y)))
#define F1(x,y,z) (z ^ (x & (y ^ z)))

#define R(t)                                    \
(                                               \
    W[t] = S1(W[t -  2]) + W[t -  7] +          \
           S0(W[t - 15]) + W[t - 16]            \
)

#define P(a,b,c,d,e,f,g,h,x,K)                  \
{                                               \
    temp1 = h + S3(e) + F1(e,f,g) + K + x;      \
    temp2 = S2(a) + F0(a,b,c);                  \
    d += temp1; h = temp1 + temp2;              \
}

    A = ctx->state[0];
    B = ctx->state[1];
    C = ctx->state[2];
    D = ctx->state[3];
    E = ctx->state[4];
    F = ctx->state[5];
    G = ctx->state[6];
    H = ctx->state[7];

    P( A, B, C, D, E, F, G, H, W[ 0], 0x428A2F98 );
    P( H, A, B, C, D, E, F, G, W[ 1], 0x71374491 );
    P( G, H, A, B, C, D, E, F, W[ 2], 0xB5C0FBCF );
    P( F, G, H, A, B, C, D, E, W[ 3], 0xE9B5DBA5 );
    P( E, F, G, H, A, B, C, D, W[ 4], 0x3956C25B );
    P( D, E, F, G, H, A, B, C, W[ 5], 0x59F111F1 );
    P( C, D, E, F, G, H, A, B, W[ 6], 0x923F82A4 );
    P( B, C, D, E, F, G, H, A, W[ 7], 0xAB1C5ED5 );
    P( A, B, C, D, E, F, G, H, W[ 8], 0xD807AA98 );
    P( H, A, B, C, D, E, F, G, W[ 9], 0x12835B01 );
    P( G, H, A, B, C, D, E, F, W[10], 0x243185BE );
    P( F, G, H, A, B, C, D, E, W[11], 0x550C7DC3 );
    P( E, F, G, H, A, B, C, D, W[12], 0x72BE5D74 );
    P( D, E, F, G, H, A, B, C, W[13], 0x80DEB1FE );
    P( C, D, E, F, G, H, A, B, W[14], 0x9BDC06A7 );
    P( B, C, D, E, F, G, H, A, W[15], 0xC19BF174 );
    P( A, B, C, D, E, F, G, H, R(16), 0xE49B69C1 );
    P( H, A, B, C, D, E, F, G, R(17), 0xEFBE4786 );
    P( G, H, A, B, C, D, E, F, R(18), 0x0FC19DC6 );
    P( F, G, H, A, B, C, D, E, R(19), 0x240CA1CC );
    P( E, F, G, H, A, B, C, D, R(20), 0x2DE92C6F );
    P( D, E, F, G, H, A, B, C, R(21), 0x4A7484AA );
    P( C, D, E, F, G, H, A, B, R(22), 0x5CB0A9DC );
    P( B, C, D, E, F, G, H, A, R(23), 0x76F988DA );
    P( A, B, C, D, E, F, G, H, R(24), 0x983E5152 );
    P( H, A, B, C, D, E, F, G, R(25), 0xA831C66D );
    P( G, H, A, B, C, D, E, F, R(26), 0xB00327C8 );
    P( F, G, H, A, B, C, D, E, R(27), 0xBF597FC7 );
    P( E, F, G, H, A, B, C, D, R(28), 0xC6E00BF3 );
    P( D, E, F, G, H, A, B, C, R(29), 0xD5A79147 );
    P( C, D, E, F, G, H, A, B, R(30), 0x06CA6351 );
    P( B, C, D, E, F, G, H, A, R(31), 0x14292967 );
    P( A, B, C, D, E, F, G, H, R(32), 0x27B70A85 );
    P( H, A, B, C, D, E, F, G, R(33), 0x2E1B2138 );
    P( G, H, A, B, C, D, E, F, R(34), 0x4D2C6DFC );
    P( F, G, H, A, B, C, D, E, R(35), 0x53380D13 );
    P( E, F, G, H, A, B, C, D, R(36), 0x650A7354 );
    P( D, E, F, G, H, A, B, C, R(37), 0x766A0ABB );
    P( C, D, E, F, G, H, A, B, R(38), 0x81C2C92E );
    P( B, C, D, E, F, G, H, A, R(39), 0x92722C85 );
    P( A, B, C, D, E, F, G, H, R(40), 0xA2BFE8A1 );
    P( H, A, B, C, D, E, F, G, R(41), 0xA81A664B );
    P( G, H, A, B, C, D, E, F, R(42), 0xC24B8B70 );
    P( F, G, H, A, B, C, D, E, R(43), 0xC76C51A3 );
    P( E, F, G, H, A, B, C, D, R(44), 0xD192E819 );
    P( D, E, F, G, H, A, B, C, R(45), 0xD6990624 );
    P( C, D, E, F, G, H, A, B, R(46), 0xF40E3585 );
    P( B, C, D, E, F, G, H, A, R(47), 0x106AA070 );
    P( A, B, C, D, E, F, G, H, R(48), 0x19A4C116 );
    P( H, A, B, C, D, E, F, G, R(49), 0x1E376C08 );
    P( G, H, A, B, C, D, E, F, R(50), 0x2748774C );
    P( F, G, H, A, B, C, D, E, R(51), 0x34B0BCB5 );
    P( E, F, G, H, A, B, C, D, R(52), 0x391C0CB3 );
    P( D, E, F, G, H, A, B, C, R(53), 0x4ED8AA4A );
    P( C, D, E, F, G, H, A, B, R(54), 0x5B9CCA4F );
    P( B, C, D, E, F, G, H, A, R(55), 0x682E6FF3 );
    P( A, B, C, D, E, F, G, H, R(56), 0x748F82EE );
    P( H, A, B, C, D, E, F, G, R(57), 0x78A5636F );
    P( G, H, A, B, C, D, E, F, R(58), 0x84C87814 );
    P( F, G, H, A, B, C, D, E, R(59), 0x8CC70208 );
    P( E, F, G, H, A, B, C, D, R(60), 0x90BEFFFA );
    P( D, E, F, G, H, A, B, C, R(61), 0xA4506CEB );
    P( C, D, E, F, G, H, A, B, R(62), 0xBEF9A3F7 );
    P( B, C, D, E, F, G, H, A, R(63), 0xC67178F2 );

    ctx->state[0] += A;
    ctx->state[1] += B;
    ctx->state[2] += C;
    ctx->state[3] += D;
    ctx->state[4] += E;
    ctx->state[5] += F;
    ctx->state[6] += G;
    ctx->state[7] += H;
}

void sha256_update( sha256_context *ctx, unsigned char *input, unsigned long int length )
{
	unsigned long int left, fill;

    if( ! length ) return;

    left = ctx->total[0] & 0x3F;
    fill = 64 - left;

    ctx->total[0] += length;
    ctx->total[0] &= 0xFFFFFFFF;

    if( ctx->total[0] < length )
        ctx->total[1]++;

    if( left && length >= fill )
    {
        memcpy( (void *) (ctx->buffer + left),
                (void *) input, fill );
        sha256_process( ctx, ctx->buffer );
        length -= fill;
        input  += fill;
        left = 0;
    }

    while( length >= 64 )
    {
        sha256_process( ctx, input );
        length -= 64;
        input  += 64;
    }

    if( length )
    {
        memcpy( (void *) (ctx->buffer + left),
                (void *) input, length );
    }
}

static unsigned char sha256_padding[64] =
{
 0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

void sha256_finish( sha256_context *ctx, unsigned char digest[32] )
{
	unsigned long int last, padn;
	unsigned long int high, low;
    unsigned char msglen[8];

    high = ( ctx->total[0] >> 29 )
         | ( ctx->total[1] <<  3 );
    low  = ( ctx->total[0] <<  3 );

    PUT_UINT32( high, msglen, 0 );
    PUT_UINT32( low,  msglen, 4 );

    last = ctx->total[0] & 0x3F;
    padn = ( last < 56 ) ? ( 56 - last ) : ( 120 - last );

    sha256_update( ctx, sha256_padding, padn );
    sha256_update( ctx, msglen, 8 );

    PUT_UINT32( ctx->state[0], digest,  0 );
    PUT_UINT32( ctx->state[1], digest,  4 );
    PUT_UINT32( ctx->state[2], digest,  8 );
    PUT_UINT32( ctx->state[3], digest, 12 );
    PUT_UINT32( ctx->state[4], digest, 16 );
    PUT_UINT32( ctx->state[5], digest, 20 );
    PUT_UINT32( ctx->state[6], digest, 24 );
    PUT_UINT32( ctx->state[7], digest, 28 );
}

void SKBTSHA256Encrypt(char *stb_id, char *output, size_t out_size)
{
	int i = 0;
	sha256_context ctx;
	unsigned char sha256sum[32];

	sha256_starts(&ctx);
	sha256_update(&ctx, (uint8 *) stb_id, strlen(stb_id));
	sha256_finish(&ctx, sha256sum);

	memset(output, 0L, out_size);

	for (i = 0; i < 32; i++)
	{
		sprintf(output + i * 2, "%02x", sha256sum[i]);
	}
}

// add by ksyoon gateway token �߱� encrypt �Լ� �߰� [20190220]
void SKBTSHA256EncryptNoHax(char *stb_id, char *output, size_t out_size)
{
	int i = 0;
	sha256_context ctx;
	unsigned char sha256sum[32];

	sha256_starts(&ctx);
	sha256_update(&ctx, (uint8 *) stb_id, strlen(stb_id));
	sha256_finish(&ctx, sha256sum);

	memset(output, 0L, out_size);

	for (i = 0; i < 32; i++)
	{
		sprintf(output + i , "%c", sha256sum[i]);
	}
}

static const char MimeBase64[] = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
    'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
    'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
    'w', 'x', 'y', 'z', '0', '1', '2', '3',
    '4', '5', '6', '7', '8', '9', '+', '/'
};


typedef union bf{
    struct{
        unsigned char c1,c2,c3;
    } A;
    struct{
        unsigned int e1:6,e2:6,e3:6,e4:6;
    } B;
} BF;

void Pbase64encode(TByte *src, char *result, int length)
{
    int i, j = 0;
    BF temp;

    for(i = 0 ; i < length ; i = i+3, j = j+4)
    {
        temp.A.c3 = src[i];
        if((i+1) >= length) temp.A.c2 = 0x00;
        else temp.A.c2 = src[i+1];
        if((i+2) >= length) temp.A.c1 = 0x00;
        else temp.A.c1 = src[i+2];

        result[j]   = MimeBase64[temp.B.e4];
        result[j+1] = MimeBase64[temp.B.e3];
        result[j+2] = MimeBase64[temp.B.e2];
        result[j+3] = MimeBase64[temp.B.e1];

        if((i+1) >= length) result[j+2] = '=';
        if((i+2) >= length) result[j+3] = '=';
    }
}

void fileOpen() {
	if (!file) {
		file = fopen("/DATA/mediamanager.log", "a");
	}
}

void fileClose() {
	if (file) {
		fclose(file);
		file = NULL;
	}
}

void FILELOG(const char *fmt, ...) {
#if 0
	fileOpen();
	char *szDebugTemp;
	va_list va;
	va_start(va, fmt);
	vasprintf(&szDebugTemp, fmt, va);
	va_end(va);
	fprintf(file, "%s\n", szDebugTemp);
	free(szDebugTemp);
	fileClose();
#endif
}

static void *myrealloc(void *ptr, size_t size) {
	if (ptr) {
		return realloc(ptr, size);
	} else {
		return malloc(size);
	}
}

void getTimeString(char* timeStr) {
	struct tm *t;
	time_t timer;
	timer = time(NULL);
	t = localtime(&timer);
	sprintf(timeStr, "%04d%02d%02d%02d%02d%02d", t->tm_year + 1900, t->tm_mon + 1, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec);
}

int JSONParse(char* json, char* name, char* value) {
	json_value *json_value = json_parse_easy(json, strlen(json));

	if (json_value->type == json_object) {
		int objectLength = json_value->u.object.length;

		for (int i = 0; i < objectLength; i++) {
			if (strcmp(json_value->u.object.values[i].name, name) == 0) {
				strcpy(value, json_value->u.object.values[i].value->u.string.ptr);
				json_value_free(json_value);
				return WSCS_OK;
			}
		}
	}

	json_value_free(json_value);
	return WSCS_ERROR;
}

int JSONParseAlloc(char* json, char* name, char** value, uint16* length) {
	json_value *json_value = json_parse_easy(json, strlen(json));
	*length = 0;

	if (json_value->type == json_object) {
		int objectLength = json_value->u.object.length;

		for (int i = 0; i < objectLength; i++) {
			if (strcmp(json_value->u.object.values[i].name, name) == 0) {
				if (json_value->u.object.values[i].value->u.string.length != 0) {
					*length = asprintf(value, "%s", json_value->u.object.values[i].value->u.string.ptr);
				}
				json_value_free(json_value);
				return WSCS_OK;
			}
		}
	}

	json_value_free(json_value);
	return WSCS_ERROR;
}

int get_data_from_url(char *url, char *server, char *proto, char *path) {
	int count = 0;
	char protoname[MAX_PROTO_LENGTH] = { 0, };
	char hostname[MAX_URL_LENGTH] = { 0, };
	char pathname[MAX_URL_LENGTH] = { 0, };

	if (2 > sscanf(url, "%31[^\n:]://%1023[^\n/]%1023[^\n]", protoname, hostname, pathname)) {
		if (1 > sscanf(url, "%1023[^\n/]%1023[^\n]", hostname, pathname))
			return 0;
	} else if (proto != NULL) {
		strcpy(proto, protoname);
		count++;
	}

	if (server != NULL) {
		strcpy(server, hostname);
		count++;
	}
	if (path != NULL) {
		strcpy(path, pathname);
		count++;
	}

	return count;
}


#if 0
jmp_buf env;

// 시그널 핸들러, env로 값 1을 가지고 jump한다.
void sighandler(int signum)
{
	printf("longjmp\n");
	longjmp(env, 1);
}

// 시그널 핸들러, env로 값 1을 가지고 jump한다.
void TestSighandler(int signum) {
	printf("TestSighandler\n");

}
pthread_t threadTest;

typedef struct TAG_MyAddrInfo {
	char serverName[256];
	char serverIp[256];
}tMyAddrInfo;

void * testThread(void *ThreadParam) {
#if 0
	tMyAddrInfo *pTest = (tMyAddrInfo*) ThreadParam;

	int error = 0;

	struct addrinfo hints, *result = NULL;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET; //AF_INET6

	//signal(SIGALRM, TestSighandler);
	LOGSRC("[TEST] testThread 1 ");
	alarm(1);
	LOGSRC("[TEST] testThread 2 ");
	error = getaddrinfo(pTest->serverName, NULL, &hints, &result);
	LOGSRC("[TEST] testThread 3");
	signal(SIGALRM, SIG_DFL);     // SIGALRM 시그널을 기본설정으로 한다.
	LOGSRC("[TEST] testThread 4 ");
	alarm(0);
	LOGSRC("[TEST] testThread 5 ");
	pthread_kill(threadTest, SIGUSR2);
	LOGSRC("[TEST] testThread 5 5");

	if (error) {
		LOGSRC("[TEST] error getaddrinfo, %d", error);
		return 0;
	}
	LOGSRC("[TEST] testThread 6 ");
	sprintf(pTest->serverIp, "%s",   inet_ntoa(((struct sockaddr_in*)result->ai_addr)->sin_addr));
	LOGSRC("[TEST] testThread %s ", pTest->serverIp);
	freeaddrinfo(result);
#else
    tMyAddrInfo *pTest = (tMyAddrInfo*) ThreadParam;

    int error = 0;

    struct addrinfo hints, *result = NULL;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET; //AF_INET6

    LOGSRC("[TEST] [%s] testThread 1 ", __FUNCTION__);
    error = getaddrinfo(pTest->serverName, NULL, &hints, &result);

    if (error) {
        LOGSRC("[TEST] [%s]  error getaddrinfo, %d",__FUNCTION__, error);
        return 0;
    }
    LOGSRC("[TEST] [%s]  testThread 6 ", __FUNCTION__);
    sprintf(pTest->serverIp, "%s",   inet_ntoa(((struct sockaddr_in*)result->ai_addr)->sin_addr));
    LOGSRC("[TEST] [%s]  testThread %s ", __FUNCTION__, pTest->serverIp);
    freeaddrinfo(result);
#endif
	return 0;

}

int dnslookup3 (char *server, char* ipaddr) {
#if 0
	tMyAddrInfo test;
	memset(&test, 0, sizeof(tMyAddrInfo));
	memcpy(test.serverName, server, strlen(server));

	sigset_t mask,oldmask;
	sigemptyset(&mask);
	sigaddset(&mask,SIGALRM);
	pthread_sigmask(SIG_BLOCK, &mask, &oldmask);

	if (pthread_create(&threadTest, NULL, testThread, &test) == 0) {
		LOGSRC("[TEST] [%s] testStart pthread_create ", __FUNCTION__);
		LOGSRC("[TEST] [%s] pthread_detach before ");
		pthread_detach(threadTest);
		LOGSRC("[TEST] [%s] pthread_detach after ");
		int signo;
		LOGSRC("[TEST] [%s] sigwait before ");
		sigwait(&mask, &signo);
		LOGSRC("[TEST] [%s] sigwait after ");

		//threadTest = NULL;
		if(strlen(test.serverIp) > 0) {
			memcpy(ipaddr, test.serverIp, strlen(test.serverIp));
			LOGSRC("[TEST] 3333 getaddrinfo : %s", ipaddr);
			return 0;
		}
	}
#else
    tMyAddrInfo test;
    memset(&test, 0, sizeof(tMyAddrInfo));
    memcpy(test.serverName, server, strlen(server));

    signal(SIGALRM, sighandler);

    if(setjmp(env) == 0) {
        alarm(1);
        //LOGSRC("[TEST] [%s] testStart pthread_create ", __FUNCTION__);
        int result = pthread_create(&threadTest, NULL, testThread, &test);
        LOGSRC("[TEST] [%s] pthread_join before ", __FUNCTION__);
        if(result == 0) pthread_join(threadTest, NULL);
        LOGSRC("[TEST] [%s] pthread_join after ", __FUNCTION__);

        signal(SIGALRM, SIG_DFL);     // SIGALRM 시그널을 기본설정으로 한다.
        alarm(0);

        //threadTest = NULL;
        if(strlen(test.serverIp) > 0) {
            memcpy(ipaddr, test.serverIp, strlen(test.serverIp));
            LOGSRC("[TEST] [%s] getaddrinfo SUCCESS: %s", __FUNCTION__, ipaddr);
            return 0;
        }
        LOGSRC("[TEST] [%s] getaddrinfo ERROR", __FUNCTION__);
        return 1;
    } else {
        LOGSRC("[TEST] [%s] getaddrinfo TIME OUT", __FUNCTION__);
        LOGSRC("[TEST] [%s] pthread_detach before ", __FUNCTION__);
        pthread_detach(threadTest);
        LOGSRC("[TEST] [%s] pthread_detach after ", __FUNCTION__);
    }

    LOGSRC("[TEST] [%s]  getaddrinfo TIME OUT", __FUNCTION__);
#endif
	return 1;
}

int dnslookup2 (char *server, char* ipaddr) {
	int error = 0;

	struct addrinfo hints, *result = NULL;

	signal(SIGALRM, sighandler);

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET; //AF_INET6

	if(setjmp(env) == 0) {
		alarm(1);
		error = getaddrinfo(server, NULL, &hints, &result);
		signal(SIGALRM, SIG_DFL);     // SIGALRM 시그널을 기본설정으로 한다.
		alarm(0);

		if (error) {
			LOGSRC("[TEST] error getaddrinfo, %d", error);
			return error;
		}
		sprintf(ipaddr, "%s",   inet_ntoa(((struct sockaddr_in*)result->ai_addr)->sin_addr));
		freeaddrinfo(result);
		return error;
	}

	LOGSRC("[TEST] error getaddrinfo time out");
	return 2604;
}

struct hostent* dnslookup(char *request) {
	struct hostent *host = NULL;
	signal(SIGALRM, sighandler);

	// 1초동안 응답이 없으면 시그널 핸들러가 실행된다.
	// 시그널 핸들러는 값 1을 가지고 env로 점프하기 때문에
	// else 문이 실행된다.
	if(setjmp(env) == 0) {
		alarm(1);
		host = gethostbyname2(request, AF_INET);
		signal(SIGALRM, SIG_DFL);     // SIGALRM 시그널을 기본설정으로 한다.
		alarm(0);
		return host;
	}

	return NULL;
}
#endif

int get_ip_from_url(char* url, char* resolving_url) {
	char proto[MAX_PROTO_LENGTH + 3] = { 0, };
	char hostname[MAX_URL_LENGTH] = { 0, };
	char path[MAX_URL_LENGTH] = { 0, };
	char ipaddr[MAX_IP_LENGTH] = { 0, };
	char *server = NULL;
	char *userid = NULL;
	char *port = NULL;

	struct hostent *myent;
	struct in_addr myen;
	long int *addr;

	if (get_data_from_url(url, hostname, proto, path) >= 2) {
		if ((server = strchr(hostname, '@')) != NULL) {
			*server = 0;
			server++;
			userid = hostname;
		} else {
			server = hostname;
		}

		if ((port = strchr(server, ':')) != NULL) {
			*port = 0;
			port++;
		}

#if 0 //ip로 변환하지 않고 url자체 넘겨본다.
#if 1
		myent = gethostbyname2(server, AF_INET);
		if (myent == NULL || *myent->h_addr_list == NULL) {
			return WSCS_ERROR;
		}

		addr = (long int *) *myent->h_addr_list;
		myen.s_addr = *addr;
		sprintf(ipaddr, "%s", inet_ntoa(myen));
#else
		//myent = dnslookup(server);;
		//if(dnslookup2(server, ipaddr) != 0) return WSCS_ERROR;
		if(dnslookup3(server, ipaddr) != 0) return WSCS_ERROR;

		LOGSRC("[TEST] getaddrinfo : %s", ipaddr);
#endif

		/*sprintf(resolving_url, "%s%s%s%s%s%s%s%s", (proto) ? proto : "", (proto) ? "://" : "", (userid) ? userid : "", (userid) ? "@" : "", ipaddr, (port) ? ":" : "",
                (port) ? port : "", path);*/
		sprintf(resolving_url, "%s%s%s%s%s%s%s%s", proto, "://", (userid) ? userid : "", (userid) ? "@" : "", ipaddr, (port) ? ":" : "",
				(port) ? port : "", path);
#else
		//ip로 변환하지 않고 utl자체 넘겨본다.
		sprintf(resolving_url, "%s%s%s%s%s%s%s%s", proto, "://", (userid) ? userid : "", (userid) ? "@" : "", server, (port) ? ":" : "",
				(port) ? port : "", path);

#endif
		return WSCS_OK;
	}

	return WSCS_ERROR;
}

size_t WriteDataCallback(void *ptr, size_t size, size_t nmemb, void *data) {
	  size_t realsize = size * nmemb;
	  ResponseData *responseData = (ResponseData *)data;

	  responseData->buffer = (char *)myrealloc(responseData->buffer, responseData->buffer_size + realsize + 1);

	  if (responseData->buffer) {
	    memcpy(&(responseData->buffer[responseData->buffer_size]), ptr, realsize);
	    responseData->buffer_size += realsize;
	    responseData->buffer[responseData->buffer_size] = 0;
	  }

	  return realsize;
}

int http_post(char *cpURL, char *requestData, ResponseData *responseData, int tries, char *apiKey) {
	void *curl_handle;
	CURLcode rc = CURLE_OK;
	int count = 0;

	struct curl_slist *headers=NULL;
	char gwParamClientID[64] = {0,};
	char gwParamClientIP[64] = {0,};

	char tmpGwheaderStr[1024] = {0, };
	char gwParamAuthVal_temp[256] = {0,};
	char gwParamTimeStamp[64] = {0,};
	char sha256Str[4096] = {0,};
	char retData[4096] = {0,};

	time_t	            time_current;
	struct timeval tv;
	struct tm	        time_info;

	char resolving_url[MAX_URL_LENGTH] = { 0, };

	char gwtoken[64] = {0,};

	responseData->buffer = NULL;
	responseData->buffer_size = 0;

	if(get_systemproperty("STBID", gwParamClientID, 40) != 0 || strlen(gwParamClientID) == 0) {
		DPRINTSRC( "!!!! ERROR : STBID IS NULL !!!!!!");
		return WSCS_ERROR;
	}

	if(get_systemproperty("GW_TOKEN", gwtoken, 40) != 0 || strlen(gwtoken) == 0) {
		DPRINTSRC( "!!!! ERROR : GW_TOKEN IS NULL !!!!!!");
		return WSCS_ERROR;
	}

	if(get_ip_from_url(cpURL, resolving_url) != WSCS_OK) {
		return WSCS_ERROR;
	}

	curl_global_init(CURL_GLOBAL_ALL);

	curl_handle = curl_easy_init();

	if (strstr(cpURL, "https") != NULL) {
		curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYHOST, 0L);
	}

	curl_easy_setopt(curl_handle, CURLOPT_URL, resolving_url);
	curl_easy_setopt(curl_handle, CURLOPT_POST, 1L);
	curl_easy_setopt(curl_handle, CURLOPT_POSTFIELDS, requestData);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteDataCallback);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *) responseData);
	curl_easy_setopt(curl_handle, CURLOPT_CONNECTTIMEOUT_MS, 2900);
	curl_easy_setopt(curl_handle, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl_handle, CURLOPT_MAXREDIRS, 3);
	curl_easy_setopt(curl_handle, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl_handle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	curl_easy_setopt(curl_handle, CURLOPT_FAILONERROR, 1);
	curl_easy_setopt(curl_handle, CURLOPT_TCP_NODELAY, 1);

	int ret;

	headers = curl_slist_append(headers, "content-type:application/json;charset=utf8");

	sprintf(tmpGwheaderStr, "Client_ID:%s", gwParamClientID);

	headers = curl_slist_append(headers, tmpGwheaderStr);
	gettimeofday(&tv, NULL);

	time_current = time(NULL);
	time_info = *localtime(&time_current);

	sprintf(gwParamTimeStamp,"%04d%02d%02d%02d%02d%02d.%03d",time_info.tm_year+1900, time_info.tm_mon+1,
			time_info.tm_mday,time_info.tm_hour, time_info.tm_min, time_info.tm_sec, tv.tv_usec / 1000);
	sprintf(tmpGwheaderStr, "TimeStamp:%s", gwParamTimeStamp);

	headers = curl_slist_append(headers, tmpGwheaderStr);

	get_interface_info("eth0", SIOCGIFADDR, gwParamClientIP);
	//get_systemproperty("IP", gwParamClientIP, 20);
	sprintf(tmpGwheaderStr, "Client_IP:%s", gwParamClientIP);

	headers = curl_slist_append(headers, tmpGwheaderStr);

	sprintf(gwParamAuthVal_temp, "%s%s",gwtoken, gwParamTimeStamp);

	SKBTSHA256EncryptNoHax(gwParamAuthVal_temp, sha256Str, 128);
	Pbase64encode((TByte *)sha256Str, retData, strlen((char *)sha256Str));
	sprintf(tmpGwheaderStr, "Auth_Val:%s",(char *)retData);

	headers = curl_slist_append(headers, tmpGwheaderStr);
	if(apiKey != NULL) {
		sprintf(tmpGwheaderStr, "Api_Key:%s", apiKey);   // real l7xx851d12cc66dc4d2e86a461fb5a530f4a
	} else {
		DPRINTSRC( "!!!! ERROR : API KEY IS NULL !!!!!!");
	}

	headers = curl_slist_append(headers, tmpGwheaderStr);
	headers = curl_slist_append(headers, "Trace:IPTV");

	curl_easy_setopt(curl_handle, CURLOPT_HTTPHEADER, headers);

	for (count = 0; count < tries; count++) {
		rc = curl_easy_perform(curl_handle);
		if (rc == CURLE_OK) {
			break;
		}
	}

	curl_easy_getinfo(curl_handle, CURLINFO_RESPONSE_CODE, &responseData->response_code);
	curl_easy_cleanup(curl_handle);

	if(rc != CURLE_OK) {
		rc = WSCS_ERROR;
	}

	curl_global_cleanup();

	return rc;
}


int http_get_gw(char *cpURL, ResponseData *responseData, int tries, char *apiKey) {
	void *curl_handle;
	CURLcode rc = CURLE_OK;
	int count = 0;

	struct curl_slist *headers=NULL;
	char gwParamClientID[64] = {0,};
	char gwParamClientIP[64] = {0,};

	char tmpGwheaderStr[1024] = {0, };
	char gwParamAuthVal_temp[256] = {0,};
	char gwParamTimeStamp[64] = {0,};
	char sha256Str[4096] = {0,};
	char retData[4096] = {0,};

	time_t	            time_current;
	struct timeval tv;
	struct tm	        time_info;

	char resolving_url[MAX_URL_LENGTH] = { 0, };

	char gwtoken[64] = {0,};

	responseData->buffer = NULL;
	responseData->buffer_size = 0;

	if(get_systemproperty("STBID", gwParamClientID, 40) != 0 || strlen(gwParamClientID) == 0) {
		DPRINTSRC( "!!!! ERROR : STBID IS NULL !!!!!!");
		return WSCS_ERROR;
	}

	if(get_systemproperty("GW_TOKEN", gwtoken, 40) != 0 || strlen(gwtoken) == 0) {
		DPRINTSRC( "!!!! ERROR : GW_TOKEN IS NULL !!!!!!");
		return WSCS_ERROR;
	}

	if(get_ip_from_url(cpURL, resolving_url) != WSCS_OK) {
		return WSCS_ERROR;
	}

    DPRINTSRC( "cpURL : %s", cpURL );

	curl_global_init(CURL_GLOBAL_ALL);

	curl_handle = curl_easy_init();

	if (strstr(cpURL, "https") != NULL) {
        DPRINTSRC( "https!!!!!!!!!!", cpURL );
		curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYHOST, 0L);
	}

	curl_easy_setopt(curl_handle, CURLOPT_URL, resolving_url);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteDataCallback);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *) responseData);
	curl_easy_setopt(curl_handle, CURLOPT_CONNECTTIMEOUT_MS, 2900);
	curl_easy_setopt(curl_handle, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl_handle, CURLOPT_MAXREDIRS, 3);
	curl_easy_setopt(curl_handle, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl_handle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	curl_easy_setopt(curl_handle, CURLOPT_FAILONERROR, 1);
	curl_easy_setopt(curl_handle, CURLOPT_TCP_NODELAY, 1);

	int ret;

	headers = curl_slist_append(headers, "content-type:application/json;charset=utf8");

	sprintf(tmpGwheaderStr, "Client_ID:%s", gwParamClientID);


    DPRINTSRC( "headers : %s", tmpGwheaderStr );
	headers = curl_slist_append(headers, tmpGwheaderStr);
	gettimeofday(&tv, NULL);

	time_current = time(NULL);
	time_info = *localtime(&time_current);

	sprintf(gwParamTimeStamp,"%04d%02d%02d%02d%02d%02d.%03d",time_info.tm_year+1900, time_info.tm_mon+1,
			time_info.tm_mday,time_info.tm_hour, time_info.tm_min, time_info.tm_sec, tv.tv_usec / 1000);
	sprintf(tmpGwheaderStr, "TimeStamp:%s", gwParamTimeStamp);

    DPRINTSRC( "headers : %s", tmpGwheaderStr );
	headers = curl_slist_append(headers, tmpGwheaderStr);

	get_interface_info("eth0", SIOCGIFADDR, gwParamClientIP);
	//get_systemproperty("IP", gwParamClientIP, 20);
	sprintf(tmpGwheaderStr, "Client_IP:%s", gwParamClientIP);

    DPRINTSRC( "headers : %s", tmpGwheaderStr );
	headers = curl_slist_append(headers, tmpGwheaderStr);

	sprintf(gwParamAuthVal_temp, "%s%s",gwtoken, gwParamTimeStamp);

	SKBTSHA256EncryptNoHax(gwParamAuthVal_temp, sha256Str, 128);
	Pbase64encode((TByte *)sha256Str, retData, strlen((char *)sha256Str));
	sprintf(tmpGwheaderStr, "Auth_Val:%s",(char *)retData);

    DPRINTSRC( "headers : %s", tmpGwheaderStr );
	headers = curl_slist_append(headers, tmpGwheaderStr);

	if(apiKey != NULL) {
		sprintf(tmpGwheaderStr, "Api_Key:%s", apiKey);   // real l7xx851d12cc66dc4d2e86a461fb5a530f4a
	} else {
		DPRINTSRC( "!!!! ERROR : API KEY IS NULL !!!!!!");
	}

    DPRINTSRC( "headers : %s", tmpGwheaderStr );
	headers = curl_slist_append(headers, tmpGwheaderStr);
	headers = curl_slist_append(headers, "Trace:IPTV");

	curl_easy_setopt(curl_handle, CURLOPT_HTTPHEADER, headers);

	for (count = 0; count < tries; count++) {
		rc = curl_easy_perform(curl_handle);
		if (rc == CURLE_OK) {
			break;
		}

        DPRINTSRC( "rererererererererererererererererererere", cpURL );
	}

	curl_easy_getinfo(curl_handle, CURLINFO_RESPONSE_CODE, &responseData->response_code);
	curl_easy_cleanup(curl_handle);

	if(rc != CURLE_OK) {
		rc = WSCS_ERROR;
	}

	curl_global_cleanup();

	return rc;
}
struct MemoryStruct {
    char *memory;
    size_t size;
};

static size_t
WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    size_t realsize = size * nmemb;
    struct MemoryStruct *mem = (struct MemoryStruct *)userp;

    char *ptr = realloc(mem->memory, mem->size + realsize + 1);
    if(ptr == NULL) {
        /* out of memory! */
        printf("not enough memory (realloc returned NULL)\n");
        return 0;
    }

    mem->memory = ptr;
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

int test(void)
{
    CURL *curl_handle;
    CURLcode res;

    struct MemoryStruct chunk;

    chunk.memory = malloc(1);  /* will be grown as needed by the realloc above */
    chunk.size = 0;    /* no data at this point */

    curl_global_init(CURL_GLOBAL_ALL);

    /* init the curl session */
    curl_handle = curl_easy_init();

    /* specify URL to get */
    curl_easy_setopt(curl_handle, CURLOPT_URL, "https://www.naver.com/");

    /* send all data to this function  */
    //curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

    /* we pass our 'chunk' struct to the callback function */
    //curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);

    /* some servers don't like requests that are made without a user-agent
       field, so we provide one */
    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");

    /* get it! */
    res = curl_easy_perform(curl_handle);

    /* check for errors */
    if(res != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));
    }
    else {
        /*
         * Now, our chunk.memory points to a memory block that is chunk.size
         * bytes big and contains the remote file.
         *
         * Do something nice with it!
         */

        printf("%lu bytes retrieved\n", (unsigned long)chunk.size);
    }

    /* cleanup curl stuff */
    curl_easy_cleanup(curl_handle);

    free(chunk.memory);

    /* we're done with libcurl, so clean it up */
    curl_global_cleanup();

    return 0;
}