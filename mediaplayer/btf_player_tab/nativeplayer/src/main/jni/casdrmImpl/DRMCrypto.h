#ifndef __DRMCrypto_h
#define __DRMCrypto_h

#if defined(__cplusplus)
extern "C"
{
#endif


////
#include <stdint.h>

#ifndef CHAR
typedef char            CHAR;
#endif
#ifndef INT32
typedef int               INT32;
#endif

#ifndef INT32_MAX
#define  INT32_MAX        	2147483647L
#endif
#ifndef INT32_MIN
#define  INT32_MIN        	(-2147483647L - 1)
#endif
#ifndef UINT32_MAX
#define UINT32_MAX			(4294967295UL)
#endif
#ifndef INT64_MAX
#define INT64_MAX 			(9223372036854775807LL)
#endif
#ifndef INT64_MIN
#define INT64_MIN 			(-9223372036854775807LL-1)
#endif
#ifndef UINT64_MAX
#define UINT64_MAX 			(18446744073709551615ULL)
#endif


typedef int __compile_time_test__if_int_is_large_enough_for_32_bits_or_not[((int)INT32_MAX == INT32_MAX) ? 1 : -1];


#if !defined(__boolean_t_DEFINED)
typedef int boolean_t;
#define __boolean_t_DEFINED
#endif

#if !defined(FALSE)
#define FALSE ((boolean_t)(0 != 0))
#endif

#if !defined(TRUE)
#define TRUE (!FALSE)
#endif


#if !defined(__gmtime64_t_DEFINED)
typedef int64_t gmtime64_t;			// unix time (UTC)
#define __gmtime64_t_DEFINED

//#define GMTIME64_MAX INT32_MAX
//#define GMTIME64_MIN INT32_MIN
#define GMTIME64_MAX INT64_MAX
#define GMTIME64_MIN INT64_MIN

#endif


////


#define DCI_API

#define DCI_IN
#define DCI_OUT
#define DCI_INOUT
#define DCI_PTR

#define DCI_UNIQUE struct { int nothing; } *


////


typedef DCI_UNIQUE CIR;

#define DCI_FAILED(r) ((intptr_t)((0 != 0) ? CIR_ERROR : (r)) < 0)

#define CIR_OK                  ((CIR)   0)

#define CIR_ERROR               ((CIR)  -1)
#define CIR_INVALID_ALGO        ((CIR)  -2)
#define CIR_INVALID_CB_TYPE     ((CIR)  -3)
#define CIR_ARGUMENTS_BAD       ((CIR)  -4)
#define CIR_CB                  ((CIR)  -5)
#define CIR_INVALID_INFO        ((CIR)  -6)
#define CIR_RO_INFO             ((CIR)  -7)
#define CIR_INVALID_ENC         ((CIR)  -8)
#define CIR_BUFFER_TOO_SMALL    ((CIR)  -9)
#define CIR_INVALID_KEY         ((CIR) -10)
#define CIR_INVALID_OPTION      ((CIR) -11)
#define CIR_INVALID_PAD         ((CIR) -12)
#define CIR_INVALID_CERTYPE     ((CIR) -13)
#define CIR_NOT_FOUND           ((CIR) -14)
#define CIR_INVALID_EXT         ((CIR) -15)
#define CIR_NOT_EXIST           ((CIR) -16)
#define CIR_NOT_SUCCESSFUL_ORES ((CIR) -17)
#define CIR_INVALID_INPUT_LEN   ((CIR) -18)
#define CIR_NOT_SUPPORTED       ((CIR) -19)

#define CIR_LIB_NOT_INITIALIZED ((CIR)-101)
#define CIR_INVALID_HANDLE      ((CIR)-102)
#define CIR_INVALID_DATA        ((CIR)-103)
#define CIR_INVALID_CERT        ((CIR)-104)
#define CIR_INVALID_KEY_PAIR    ((CIR)-105)
#define CIR_VERIFY_FAIL         ((CIR)-106)
#define CIR_CERT_EXT_KEYUSAGE   ((CIR)-107)
#define CIR_CERT_REVOKED        ((CIR)-108)
#define CIR_CERT_EXPIRED        ((CIR)-109)

#define CIR_CONNECT_FAIL        ((CIR)-121)
#define CIR_CERT_ISSUE_FAIL     ((CIR)-122)

#define CIR_FILE_OPEN_FAIL      ((CIR)-131)
#define CIR_FILE_READ_FAIL      ((CIR)-132)
#define CIR_FILE_WRITE_FAIL     ((CIR)-133)
#define CIR_FILE_ENC_FAIL       ((CIR)-134)

#define CIR_INSTALLCERT_INVALID_DATA	((CIR)	-300)
#define CIR_INSTALLCERT_INVALID_NONCE	((CIR)	-301)
#define CIR_INSTALLCERT_INVALID_VERSION	((CIR)	-302)
#define CIR_INSTALLCERT_INVALID_CERT	((CIR)	-303)

#define CIR_INSTALLCERT_EMPTY_NONCE		((CIR)	-311)
#define CIR_INSTALLCERT_EMPTY_VERSION	((CIR)	-312)
#define CIR_INSTALLCERT_EMPTY_CERT		((CIR)	-313)

#define CIR_INSTALLCERT_STROAGE_ERROR	((CIR)	-321)




#define CIR_VALIFY_FAIL CIR_VERIFY_FAIL		// for backward compatibility


////


typedef DCI_UNIQUE CIH_BIGNUM;
typedef DCI_UNIQUE CIH_HASH;
typedef DCI_UNIQUE CIH_HMAC;
typedef DCI_UNIQUE CIH_ENCRYPT;
typedef DCI_UNIQUE CIH_DECRYPT;
typedef DCI_UNIQUE CIH_SIGN;
typedef DCI_UNIQUE CIH_VERIFY;
typedef DCI_UNIQUE CIH_PUBKEY;
typedef DCI_UNIQUE CIH_PRIKEY;
typedef DCI_UNIQUE CIH_CERT;
typedef DCI_UNIQUE CIH_OCSPREQ;
typedef DCI_UNIQUE CIH_OCSPRES;
typedef DCI_UNIQUE CIH_CERTID;
typedef DCI_UNIQUE CIH_NEWUCNK;

typedef DCI_UNIQUE CIH_TSD;
////


typedef DCI_UNIQUE CIC_ALGO_PBKC;
typedef DCI_UNIQUE CIC_ALGO_ESGN;
typedef DCI_UNIQUE CIC_ALGO_MDGT;
typedef DCI_UNIQUE CIC_ALGO_BLKC;

#define CIC_ALGO_UNKNOWN      0
#define CIC_ALGO_ECC          ((CIC_ALGO_PBKC) 1)		// public key cryptography
#define CIC_ALGO_RSA          ((CIC_ALGO_PBKC) 2)		// public key cryptography
#define CIC_ALGO_SHA1_RSA     ((CIC_ALGO_ESGN) 3)		// electronic signature
#define CIC_ALGO_MD5_RSA      ((CIC_ALGO_ESGN) 4)		// electronic signature
#define CIC_ALGO_SHA1         ((CIC_ALGO_MDGT) 7)		// message digest
#define CIC_ALGO_MD5          ((CIC_ALGO_MDGT) 8)		// message digest
#define CIC_ALGO_3DES         ((CIC_ALGO_BLKC) 9)		// block cipher
#define CIC_ALGO_SEED         ((CIC_ALGO_BLKC)10)		// block cipher
#define CIC_ALGO_AES128       ((CIC_ALGO_BLKC)11)		// block cipher
#define CIC_ALGO_AES192       ((CIC_ALGO_BLKC)12)		// block cipher
#define CIC_ALGO_AES256       ((CIC_ALGO_BLKC)13)		// block cipher
#define	CIC_ALGO_SHA1_RSA_PSS ((CIC_ALGO_ESGN)17)		// electronic signature


////


typedef struct
{
	uint8_t *pData;
	int length;
}
CIS_BIN;


////


typedef struct
{
	int8_t major;
	int8_t minor;
	int8_t revision;
	int8_t rebuild;
}
CIS_LIB_INFO;


DCI_API CIR dci_InitializeLib(DCI_IN const void *args);
// ^ 'args' can be 'NULL'.

DCI_API CIR dci_ReleaseLib(DCI_IN const void *args);
// ^ 'args' can be 'NULL'.

DCI_API CIR dci_GetLibInfo(DCI_OUT CIS_LIB_INFO *p_libInfo);


////


DCI_API CIR dci_GenerateRandom(DCI_OUT uint8_t *out, int outLength);


////


DCI_API CIR dci_BNEncode(CIH_BIGNUM hBignum, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.

DCI_API CIR dci_BNDecode(DCI_IN const uint8_t *in, int inLength, DCI_OUT CIH_BIGNUM *p_hBignum);
// ^ '*p_hBignum' will be 'NULL' on error.
// ^ '*p_hBignum' must be freed by 'dci_BNFree'.

DCI_API CIR dci_BNGetSize(CIH_BIGNUM hBignum);

DCI_API CIR dci_BNFree(CIH_BIGNUM hBignum);
// ^ 'hBignum' can be 'NULL'.


////


DCI_API CIR dci_HashInit(CIC_ALGO_MDGT algoID, DCI_OUT CIH_HASH *p_hHash);
// ^ '*p_hHash' will be 'NULL' on error.
// ^ '*p_hHash' must be freed by 'dci_HashFinal' or 'dci_HashStop'.

DCI_API CIR dci_HashUpdate(CIH_HASH hHash, DCI_IN const uint8_t *in, int inLength);

DCI_API CIR dci_HashFinal(CIH_HASH hHash, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.

DCI_API CIR dci_HashStop(CIH_HASH hHash);
// ^ 'hHash' can be 'NULL'.

DCI_API CIR dci_HashLength(CIC_ALGO_MDGT algoID);


////


DCI_API CIR dci_HMACInit(CIC_ALGO_MDGT algoID, DCI_IN const uint8_t *key, int keyLength, DCI_OUT CIH_HMAC *p_hHmac);
// ^ '*p_hHmac' will be 'NULL' on error.
// ^ '*p_hHmac' must be freed by 'dci_HmacFinal' or 'dci_HmacStop'.

DCI_API CIR dci_HMACUpdate(CIH_HMAC hHmac, DCI_IN const uint8_t *in, int inLength);

DCI_API CIR dci_HMACFinal(CIH_HMAC hHmac, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.

DCI_API CIR dci_HMACStop(CIH_HMAC hHmac);
// ^ 'hHmac' can be 'NULL'.


////


typedef DCI_UNIQUE CIC_CMODE;

#define CIC_CMODE_CBC ((CIC_CMODE)1)
#define CIC_CMODE_ECB ((CIC_CMODE)2)
#define CIC_CMODE_CTR ((CIC_CMODE)3)


typedef DCI_UNIQUE CIC_CPAD;

#define CIC_CPAD_PKCS5 ((CIC_CPAD)1)
#define CIC_CPAD_NONE  ((CIC_CPAD)2)
#define CIC_CPAD_CTS   ((CIC_CPAD)3)


typedef struct
{
	CIC_CMODE mode;
	CIC_CPAD padding;
	uint8_t *key;
	uint8_t *iv;
}
CIS_BLOCK_CIPHER_PARAM;


typedef DCI_UNIQUE CIC_CIPHER_TYPE;

#define CIC_CIPHER_TYPE_BLOCK  ((CIC_CIPHER_TYPE)1)
#define CIC_CIPHER_TYPE_STREAM ((CIC_CIPHER_TYPE)2)


typedef struct
{
	CIC_CIPHER_TYPE type;
	int keysize;
	int blocksize;
	int ivSize;
}
CIS_CIPHER_INFO;


DCI_API CIR dci_EncryptInit(CIC_ALGO_BLKC algoID, DCI_IN const CIS_BLOCK_CIPHER_PARAM *param, DCI_OUT CIH_ENCRYPT *p_hEncrypt);

DCI_API CIR dci_EncryptUpdate(CIH_ENCRYPT hEncrypt, DCI_IN const uint8_t *in, int inLength, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.

DCI_API CIR dci_EncryptFinal(CIH_ENCRYPT hEncrypt, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.

DCI_API CIR dci_EncryptStop(CIH_ENCRYPT hEncrypt);
// ^ 'hEncrypt' can be 'NULL'.


DCI_API CIR dci_DecryptInit(CIC_ALGO_BLKC algoID, DCI_IN const CIS_BLOCK_CIPHER_PARAM *param, DCI_OUT CIH_DECRYPT *p_hDecrypt);

DCI_API CIR dci_DecryptUpdate(CIH_DECRYPT hDecrypt, DCI_IN const uint8_t *in, int inLength, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.

DCI_API CIR dci_DecryptFinal(CIH_DECRYPT hDecrypt, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.

DCI_API CIR dci_DecryptStop(CIH_DECRYPT hDecrypt);
// ^ 'hDecrypt' can be 'NULL'.


DCI_API CIR dci_CipherAlgoInfo(CIC_ALGO_BLKC algoID, DCI_OUT CIS_CIPHER_INFO *p_algoInfo);


////


DCI_API CIR dci_AESKeyWrap(CIC_ALGO_BLKC algoID, DCI_IN const uint8_t *kek, DCI_IN const uint8_t *in, int inLength, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.

DCI_API CIR dci_AESKeyUnwrap(CIC_ALGO_BLKC algoID, DCI_IN const uint8_t *kek, DCI_IN const uint8_t *in, int inLength, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.


////


DCI_API CIR dci_SignInit(CIC_ALGO_ESGN algoID, CIH_PRIKEY hPrikey, DCI_OUT CIH_SIGN *p_hSign);
// ^ '*p_hSign' will be 'NULL' on error.
// ^ '*p_hSign' must be freed by 'dci_SignFinal' or 'dci_SignStop'.

DCI_API CIR dci_SignUpdate(CIH_SIGN hSign, DCI_IN const uint8_t *in, int inLength);

DCI_API CIR dci_SignFinal(CIH_SIGN hSign, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.

DCI_API CIR dci_SignStop(CIH_SIGN hSign);
// ^ 'hSign' can be 'NULL'.


DCI_API CIR dci_VerifyInit(CIC_ALGO_ESGN algoID, CIH_PUBKEY hPubkey, DCI_OUT CIH_VERIFY *p_hVerify);
// ^ '*p_hVerify' will be 'NULL' on error.
// ^ '*p_hVerify' must be freed by 'dci_VerifyFinal' or 'dci_VerifyStop'.

DCI_API CIR dci_VerifyUpdate(CIH_VERIFY hVerify, DCI_IN const uint8_t *in, int inLength);

DCI_API CIR dci_VerifyFinal(CIH_VERIFY hVerify, DCI_IN const uint8_t *sig, int sigLength);

DCI_API CIR dci_VerifyStop(CIH_VERIFY hVerify);
// ^ 'hVerify' can be 'NULL'.


////


typedef DCI_UNIQUE CIC_RSA_PAD;

#define CIC_RSA_PAD_PKCS1 ((CIC_RSA_PAD)1)
#define CIC_RSA_PAD_OAEP  ((CIC_RSA_PAD)2)
#define CIC_RSA_PAD_NONE  ((CIC_RSA_PAD)3)


DCI_API CIR dci_RSAEncrypt(CIH_PUBKEY hPubkey, CIC_RSA_PAD padding, DCI_IN const uint8_t *in, int inLength, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.

DCI_API CIR dci_RSADecrypt(CIH_PRIKEY hPrikey, CIC_RSA_PAD padding, DCI_IN const uint8_t *in, int inLength, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.


////


typedef DCI_UNIQUE CIC_PUBENC;

#define CIC_PUBENC_BITSTRING ((CIC_PUBENC)1)
#define CIC_PUBENC_X509      ((CIC_PUBENC)4)


typedef struct
{
	CIC_ALGO_PBKC algID;
	int param;
	int size;
}
CIS_GENERAL_KEY_INFO;


typedef struct
{
	int modulus;
	int exponent;
}
CIS_PUBKEY_ELEMENT;


DCI_API CIR dci_PubKeyEncode(CIH_PUBKEY hPubkey, CIC_PUBENC encType, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.

DCI_API CIR dci_PubKeyDecode(DCI_IN const uint8_t *in, int inLength, CIC_PUBENC encType, DCI_OUT CIH_PUBKEY *p_hPubkey);
// ^ '*p_hPubkey' will be 'NULL' on error.
// ^ '*p_hPubkey' must be freed by 'dci_PubKeyFree'.

DCI_API CIR dci_PubKeyGetHandle(CIH_CERT hCert, DCI_OUT CIH_PUBKEY *p_hPubkey);
// ^ '*p_hPubkey' will be 'NULL' on error.
// ^ '*p_hPubkey' must be freed by 'dci_PubKeyFree'.

DCI_API CIR dci_PubKeyFree(CIH_PUBKEY hPubkey);
// ^ 'hPubkey' can be 'NULL'.

DCI_API CIR dci_PubKeyGetInfo(CIH_PUBKEY hPubkey, DCI_OUT CIS_GENERAL_KEY_INFO *p_keyInfo);

DCI_API CIR dci_PubKeyGetKeyElement(CIH_PUBKEY hPubkey, DCI_OUT CIS_PUBKEY_ELEMENT *p_keyElement, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.


DCI_API CIR dci_PriKeyDecode(DCI_IN const uint8_t *in, int inLength, DCI_IN const char *password, DCI_OUT CIH_PRIKEY *p_hPrikey);
// ^ '*p_hPrikey' will be 'NULL' on error.
// ^ '*p_hPrikey' must be freed by 'dci_PriKeyFree'.

DCI_API CIR dci_PriKeyFree(CIH_PRIKEY hPrikey);
// ^ 'hPrikey' can be 'NULL'.

DCI_API CIR dci_PriKeyGetInfo(CIH_PRIKEY hPrikey, DCI_OUT CIS_GENERAL_KEY_INFO *p_keyInfo);


DCI_API CIR dci_KeyPairMatch(CIH_PUBKEY hPubkey, CIH_PRIKEY hPrikey);

DCI_API CIR dci_KeyPairGenerate(DCI_OUT CIH_PUBKEY *p_hPubkey, DCI_OUT CIH_PRIKEY *p_hPrikey);
// ^ '*p_hPubkey' will be 'NULL' on error.
// ^ '*p_hPubkey' must be freed by 'dci_PubKeyFree'.
// ^ '*p_hPrikey' will be 'NULL' on error.
// ^ '*p_hPrikey' must be freed by 'dci_PriKeyFree'.


////


typedef struct
{
	int Version;
	int KeySize;
	CIC_ALGO_PBKC PubKeyAlgo;
	CIC_ALGO_ESGN SignAlgo;
	gmtime64_t NotBefore;
	gmtime64_t NotAfter;
	char szNotBefore[20 + 1];		// null-terminated string
	char szNotAfter[20 + 1];		// null-terminated string
}
CIS_CERT_INFO;


DCI_API CIR dci_CertEncode(CIH_CERT hCert, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.

DCI_API CIR dci_CertDecode(DCI_IN const uint8_t *in, int inLength, DCI_OUT CIH_CERT *p_hCert);
// ^ '*p_hCert' will be 'NULL' on error.
// ^ '*p_hCert' must be freed by 'dci_CertFree'.

DCI_API CIR dci_CertFree(CIH_CERT hCert);
// ^ 'hCert' can be 'NULL'.

DCI_API CIR dci_CertGetSubjectDN(CIH_CERT hCert, DCI_OUT char *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.

DCI_API CIR dci_CertGetIssuerDN(CIH_CERT hCert, DCI_OUT char *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.

DCI_API CIR dci_CertGetSerial(CIH_CERT hCert, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.

DCI_API CIR dci_CertGetInfo(CIH_CERT hCert, DCI_OUT CIS_CERT_INFO *p_info);

DCI_API CIR dci_CertSignVerify(CIH_CERT hCert, CIH_CERT hIssuerCert);

DCI_API CIR dci_CertDateVerify(CIH_CERT hCert, gmtime64_t now);


typedef DCI_UNIQUE CIC_X509_EXT;

#define CIC_X509_EXT_BASIC_CONSTRAINTS ((CIC_X509_EXT) 1)
#define CIC_X509_EXT_KEY_USAGE         ((CIC_X509_EXT) 2)
#define CIC_X509_EXT_EXT_KEY_USAGE     ((CIC_X509_EXT) 3)
#define CIC_X509_EXT_SUBJECT_ALT_NAME  ((CIC_X509_EXT) 4)
#define CIC_X509_EXT_ISSUER_ALT_NAME   ((CIC_X509_EXT) 5)
#define CIC_X509_EXT_AUTH_INFO_ACCESS  ((CIC_X509_EXT) 6)
#define CIC_X509_EXT_AUTH_KEY_ID       ((CIC_X509_EXT) 7)
#define CIC_X509_EXT_SUBJECT_KEY_ID    ((CIC_X509_EXT) 8)
#define CIC_X509_EXT_CERT_POLICIES     ((CIC_X509_EXT) 9)
#define CIC_X509_EXT_CRLDPS            ((CIC_X509_EXT)10)


typedef struct
{
	boolean_t bCA;
	int ulPathLen;
}
CIS_BASIC_CONSTRAINTS;


typedef uint32_t CIC_KU;

#define CIC_KU_DIGITAL_SIGN      ((CIC_KU)0x0001)
#define CIC_KU_NON_REPUDIATION   ((CIC_KU)0x0002)
#define CIC_KU_KEY_ENCIPHERMENT  ((CIC_KU)0x0004)
#define CIC_KU_DATA_ENCIPHERMENT ((CIC_KU)0x0008)
#define CIC_KU_KEY_AGREEMENT     ((CIC_KU)0x0010)
#define CIC_KU_KEY_CERT_SIGN     ((CIC_KU)0x0020)
#define CIC_KU_CRL_SIGN          ((CIC_KU)0x0040)
#define CIC_KU_ENCIPHER_ONLY     ((CIC_KU)0x0080)
#define CIC_KU_DECIPHER_ONLY     ((CIC_KU)0x0100)

#define CIC_KU_SIGN CIC_KU_DIGITAL_SIGN
#define CIC_KU_ENCRYPT CIC_KU_KEY_ENCIPHERMENT


typedef uint32_t CIC_EKU;

#define CIC_EKU_SERVER_AUTH      ((CIC_EKU)0x0001)
#define CIC_EKU_CLIENT_AUTH      ((CIC_EKU)0x0002)
#define CIC_EKU_CODE_SIGN        ((CIC_EKU)0x0004)
#define CIC_EKU_EMAIL_PROTECTION ((CIC_EKU)0x0008)
#define CIC_EKU_TIME_STAMP       ((CIC_EKU)0x0010)
#define CIC_EKU_OCSP_SIGN        ((CIC_EKU)0x0800)
#define CIC_EKU_DRM_AGENT        ((CIC_EKU)0x1000)
#define CIC_EKU_RIGHTS_ISSUER    ((CIC_EKU)0x2000)


typedef DCI_UNIQUE CIC_GN_TYPE;

#define CIC_GN_TYPE_NOTHING            ((CIC_GN_TYPE)  0)
#define CIC_GN_TYPE_EMAIL              ((CIC_GN_TYPE)  1)
#define CIC_GN_TYPE_DNS                ((CIC_GN_TYPE)  2)
#define CIC_GN_TYPE_URI                ((CIC_GN_TYPE)  3)
#define CIC_GN_TYPE_IPADDR             ((CIC_GN_TYPE)  4)
#define CIC_GN_TYPE_DIR_NAME           ((CIC_GN_TYPE)  5)
#define CIC_GN_TYPE_OTHER_IDENTIFYDATA ((CIC_GN_TYPE)  6)
#define CIC_GN_TYPE_RID                ((CIC_GN_TYPE)  7)
#define CIC_GN_TYPE_UNSUPPORTED        ((CIC_GN_TYPE)255)


typedef struct
{
	char *realName;
	char *addInfo;
}
CIS_ID_DATA;

typedef struct
{
	CIC_GN_TYPE type;

	union
	{
		char *strname;
		CIS_ID_DATA iddata;
	}
	name;
}
CIS_GENERAL_NAME;

typedef struct
{
	int n;								// length of 'names'
	CIS_GENERAL_NAME *names;
}
CIS_ALT_NAMES;


typedef DCI_UNIQUE CIC_ACCMETH;

#define CIC_ACCMETH_CAISSUER    ((CIC_ACCMETH)1)
#define CIC_ACCMETH_OCSP        ((CIC_ACCMETH)2)
#define CIC_ACCMETH_UNSUPPORTED ((CIC_ACCMETH)3)


typedef struct
{
	CIC_ACCMETH	accMeth;
	CIS_GENERAL_NAME Location;
}
CIS_ACCESS_DESC;

typedef struct
{
	int n;								// length of 'pAccessDesc'
	CIS_ACCESS_DESC *pAccessDesc;
}
CIS_AUTH_INFO_ACCESS;


typedef struct
{
	CIS_BIN keyid;

	struct
	{
		int n;							// length of 'issuer'
		CIS_GENERAL_NAME *issuer;
	}
	issuernames;

	CIS_BIN serial;
}
CIS_AUTH_KEY_IDENTIFIER;


typedef CIS_BIN	CIS_KEY_IDENTIFIER;


typedef DCI_UNIQUE CIC_POLICY_QUALIFIER;

#define CIC_POLICY_QUALIFIER_NOTHING     ((CIC_POLICY_QUALIFIER)0)
#define CIC_POLICY_QUALIFIER_USER_NOTICE ((CIC_POLICY_QUALIFIER)1)
#define CIC_POLICY_QUALIFIER_CPS_URI     ((CIC_POLICY_QUALIFIER)2)
#define CIC_POLICY_QUALIFIER_UNSUPPORTED ((CIC_POLICY_QUALIFIER)3)


typedef struct
{
	char *organization;
	int n;								// length of 'noticeNumbers'
	int *noticeNumbers;
}
CIS_NOTICE_REFERENCE;

typedef struct
{
	CIS_NOTICE_REFERENCE noticeRef;
	char *explicitText;
}
CIS_USER_NOTICE;

typedef struct
{
	CIC_POLICY_QUALIFIER type;

	union
	{
		char *CPSuri;
		CIS_USER_NOTICE userNotice;
	}
	d;
}
CIS_POLICY_QUALIFIER_INFO;

typedef struct
{
	char *policyid;
	int nQualifier;						// length of 'qualifiers'
	CIS_POLICY_QUALIFIER_INFO *qualifiers;
}
CIS_POLICY_INFO;

typedef struct
{
	int n;								// length of 'policyinfo'
	CIS_POLICY_INFO *policyinfo;
}
CIS_CERT_POLICIES;


typedef DCI_UNIQUE CIC_CRL_DP_TYPE;

#define CIC_CRL_DP_TYPE_NONE     ((CIC_CRL_DP_TYPE)1)
#define CIC_CRL_DP_TYPE_FULLNAME ((CIC_CRL_DP_TYPE)2)
#define CIC_CRL_DP_TYPE_RELATIVE ((CIC_CRL_DP_TYPE)3)


typedef uint32_t CIC_CRLR;

#define CIC_CRLR_UNUSED             ((CIC_CRLR)0x0001)
#define CIC_CRLR_KEYCOMPROMISE      ((CIC_CRLR)0x0002)
#define CIC_CRLR_CACOMPROMISE       ((CIC_CRLR)0x0004)
#define CIC_CRLR_AFFILIATIONCHANGED ((CIC_CRLR)0x0008)
#define CIC_CRLR_SUPERSEDED         ((CIC_CRLR)0x0010)
#define CIC_CRLR_CESSATIONOFOP      ((CIC_CRLR)0x0020)
#define CIC_CRLR_CERTHOLD           ((CIC_CRLR)0x0040)


typedef struct
{
	CIC_CRL_DP_TYPE dpnType;

	union
	{
		struct
		{
			int n;						// length of 'generalNames'
			CIS_GENERAL_NAME *generalNames;
		}
		fullName;

		char *relativeName;
	}
	dpn;

	CIC_CRLR reasons;					// combination of 'CIC_CRLR_*'

	struct
	{
		int n;							// length of 'issuerName'
		CIS_GENERAL_NAME *issuerName;
	}
	crlIssuer;
}
CIS_CRL_DIST_POINT;

typedef struct
{
	int n;								// length of 'distPoint'
	CIS_CRL_DIST_POINT *distPoint;
}
CIS_CRL_DIST_POINTS;


typedef union
{
	CIS_BASIC_CONSTRAINTS BasicConstraints;
	CIC_KU KeyUsage;					// combination of 'CIC_KU_*'
	CIC_EKU ExtKeyUsage;				// combination of 'CIC_EKU_*'
	CIS_ALT_NAMES SubjectAltNames;
	CIS_ALT_NAMES IssuerAltNames;
	CIS_AUTH_INFO_ACCESS AIA;
	CIS_AUTH_KEY_IDENTIFIER AKID;
	CIS_KEY_IDENTIFIER SKID;
	CIS_CERT_POLICIES certPolicies;
	CIS_CRL_DIST_POINTS CRLDPs;
}
CIS_CERT_EXTENSION;


DCI_API CIR dci_CertGetExtension(CIH_CERT hCert, CIC_X509_EXT extID, DCI_OUT CIS_CERT_EXTENSION *p_certExt, DCI_OUT boolean_t *p_critical);
// ^ 'p_critical' can be 'NULL'.
// ^ '*p_critical' will be true when the 'critical' field is present and true.


////


// OCSP response status
#define CIC_ORES_SUCCESSFUL   0
#define CIC_ORES_MALFORMEDREQ 1
#define CIC_ORES_INTERNALERR  2
#define CIC_ORES_TRYLATER     3
#define CIC_ORES_SIGREQUIRED  5
#define CIC_ORES_UNAUTHORIZED 6


typedef DCI_UNIQUE CIC_OCSP_EXT;

#define CIC_OCSP_EXT_NONCE ((CIC_OCSP_EXT)1)


typedef CIS_BIN	CIS_OCSP_NONCE;

typedef union
{
	CIS_OCSP_NONCE nonce;
}
CIS_OCSP_EXTENSION;


typedef DCI_UNIQUE CIC_REVR;

#define CIC_REVR_UNSPECIFIED        ((CIC_REVR)0)
#define CIC_REVR_KEYCOMPROMISE      ((CIC_REVR)1)
#define CIC_REVR_CACOMPROMISE       ((CIC_REVR)2)
#define CIC_REVR_AFFILIATIONCHANGED ((CIC_REVR)3)
#define CIC_REVR_SUPERSEDED         ((CIC_REVR)4)
#define CIC_REVR_CESSATIONOFOP      ((CIC_REVR)5)
#define CIC_REVR_CERTHOLD           ((CIC_REVR)6)
#define CIC_REVR_REMOVEFROMCRL      ((CIC_REVR)8)


typedef struct
{
	gmtime64_t revocationTime;
	CIC_REVR revocationReason;
}
CIS_OCSP_REVOKED_INFO;


typedef DCI_UNIQUE CIC_OCSP_CERT;

#define CIC_OCSP_CERT_GOOD    ((CIC_OCSP_CERT)0)
#define CIC_OCSP_CERT_REVOKED ((CIC_OCSP_CERT)1)
#define CIC_OCSP_CERT_UNKNOWN ((CIC_OCSP_CERT)2)


typedef struct
{
	CIC_OCSP_CERT certStatus;
	gmtime64_t thisUpdate;				// unlike what is described in the documentation, it is not local time.
	gmtime64_t nextUpdate;				// unlike what is described in the documentation, it is not local time.
	CIS_OCSP_REVOKED_INFO revokedInfo;
}
CIS_OCSP_CERT_STATUS;


DCI_API CIR dci_OCSPReqCreate(DCI_OUT CIH_OCSPREQ *p_hOcspreq);
// ^ '*p_hOcspreq' will be 'NULL' on error.
// ^ '*p_hOcspreq' must be freed by 'dci_OCSPReqFree'.

DCI_API CIR dci_OCSPReqFree(CIH_OCSPREQ hOcspreq);
// ^ 'hOcspreq' can be 'NULL'.

DCI_API CIR dci_OCSPReqAddSingleRequest(CIH_OCSPREQ hOcspreq, CIH_CERT hTargetCert, CIH_CERT hIssuerCert);

DCI_API CIR dci_OCSPReqAddExt(CIH_OCSPREQ hOcspreq, CIC_OCSP_EXT extID, DCI_IN const CIS_OCSP_EXTENSION *ext);

DCI_API CIR dci_OCSPReqEncode(CIH_OCSPREQ hOcspreq, CIH_CERT hRequesterCert, CIH_PRIKEY hRequesterPrikey, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.


typedef DCI_UNIQUE CIC_OCSP_RESPID;

#define CIC_OCSP_RESPID_BYNAME ((CIC_OCSP_RESPID)1)
#define CIC_OCSP_RESPID_BYKEY  ((CIC_OCSP_RESPID)2)


typedef struct
{
	CIC_OCSP_RESPID type;
	CIS_BIN	value;
}
CIS_OCSP_RESPONDER_ID;


DCI_API CIR dci_OCSPResDecode(DCI_IN const uint8_t *in, int inLength, DCI_OUT CIH_OCSPRES *p_hOcspres);
// ^ '*p_hOcspres' will be 'NULL' on error.
// ^ '*p_hOcspres' must be freed by 'dci_OCSPResFree'.

DCI_API CIR dci_OCSPResFree(CIH_OCSPRES hOcspres);
// ^ 'hOcspres' can be 'NULL'.

DCI_API CIR dci_OCSPResStatus(CIH_OCSPRES hOcspres);

DCI_API CIR dci_OCSPResVerify(CIH_OCSPRES hOcspres, CIH_PUBKEY hSignerPubkey, DCI_IN const uint8_t *nonce);
// ^ 'hSignerPubkey' can be 'NULL'.

DCI_API CIR dci_OCSPResGetResponderID(CIH_OCSPRES hOcspres, DCI_OUT CIS_OCSP_RESPONDER_ID *p_respID);

DCI_API CIR dci_OCSPResGetProduceAt(CIH_OCSPRES hOcspres);

DCI_API CIR dci_OCSPResGetExt(CIH_OCSPRES hOcspres, CIC_OCSP_EXT extID, DCI_OUT CIS_OCSP_EXTENSION *p_ext);

DCI_API CIR dci_OCSPResGetCertStatus(CIH_OCSPRES hOcspres, CIH_CERTID hCertid, DCI_OUT CIS_OCSP_CERT_STATUS *p_status);

DCI_API CIR dci_OCSPResGetCertCount(CIH_OCSPRES hOcspres);

DCI_API CIR dci_OCSPResGetCert(CIH_OCSPRES hOcspres, int index, DCI_OUT CIH_CERT *p_hCert);
// ^ '*p_hCert' will be 'NULL' on error.
// ^ '*p_hCert' must be freed by 'dci_CertFree'.


DCI_API CIR dci_CertIDCreate(CIH_CERT hTargetCert, CIH_CERT hIssuerCert, DCI_OUT CIH_CERTID *p_hCertid);
// ^ '*p_hCertid' will be 'NULL' on error.
// ^ '*p_hCertid' must be freed by 'dci_CertIDFree'.

DCI_API CIR dci_CertIDFree(CIH_CERTID hCertid);
// ^ 'hCertid' can be 'NULL'.


////


DCI_API CIR dci_GetUserCertNKey(uint32_t serviceID, DCI_IN const char *userID, DCI_OUT CIH_CERT *p_hCert, DCI_OUT CIH_PRIKEY *p_hPrikey);
// ^ 'p_hCert' can be 'NULL'.
// ^ '*p_hCert' will be 'NULL' on error.
// ^ '*p_hCert' must be freed by 'dci_CertFree'.
// ^ 'p_hPrikey' can be 'NULL'.
// ^ '*p_hPrikey' will be 'NULL' on error.
// ^ '*p_hPrikey' must be freed by 'dci_PriKeyFree'.

DCI_API CIR dci_DeleteUserCertNKey(uint32_t serviceID, DCI_IN const char *userID);

DCI_API CIR dci_SaveUserCertNKey(uint32_t serviceID, DCI_IN const char *userID, CIH_CERT hCert, CIH_PRIKEY hPrikey, boolean_t asNewFormat);

DCI_API CIR dci_GetIssuerCert(CIH_CERT hCert, DCI_OUT CIH_CERT *p_hIssuerCert);
// ^ '*p_hIssuerCert' will be 'NULL' on error.
// ^ '*p_hIssuerCert' must be freed by 'dci_CertFree'.

DCI_API CIR dci_CertChainVerify(int certChainLength, DCI_IN const CIH_CERT *certChain, CIH_OCSPRES hOcspres, DCI_IN const uint8_t *nonce);
// ^ 'hOcspres' can be 'NULL'.
// ^ 'nonce' can be 'NULL' if and only if '(hOcspres == NULL)'.

DCI_API CIR dci_CreateCertChain(CIH_CERT hCert, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.

DCI_API CIR dci_SaveOtherCert(DCI_IN const char *label, CIH_CERT hCert);

DCI_API CIR dci_GetOtherCert(DCI_IN const char *label, DCI_OUT CIH_CERT *p_hCert);
// ^ '*p_hCert' will be 'NULL' on error.
// ^ '*p_hCert' must be freed by 'dci_CertFree'.

DCI_API CIR dci_DeleteOtherCert(DCI_IN const char *label);


////


DCI_API char *dci_GetErrorMessage();


////


// direct
DCI_API CIR dci_Base64Encode(DCI_IN const uint8_t *in, int inLength, DCI_OUT char *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.

// direct
DCI_API CIR dci_Base64Decode(DCI_IN const char *in, int inLength, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength);
// ^ 'out' can be 'NULL'.


////


// direct
DCI_API CIR dci_NewUserCertNKey_Begin(uint32_t serviceID, DCI_IN const char *userID, DCI_OUT CIH_NEWUCNK *p_hNewucnk);
// ^ '*p_hNewucnk' will be 'NULL' on error.
// ^ '*p_hNewucnk' must be freed by 'dci_NewUserCertNKeyEnd'.
// ^ '*p_req' will be 'NULL' on error.
// ^ '*p_req' should not be freed separately.

// direct
DCI_API CIR dci_NewUserCertNKey_End(CIH_NEWUCNK hNewucnk);
// ^ 'hNewucnk' can be 'NULL'.


// direct
DCI_API CIR dci_NewUserCertNKey_GetRequest(CIH_NEWUCNK hNewucnk, DCI_OUT char **p_req);
// ^ '*p_req' will be 'NULL' on error.
// ^ '*p_req' should not be freed. It will be freed by 'dci_NewUserCertNKey_End'.

// direct
DCI_API CIR dci_NewUserCertNKey_PutResponse(CIH_NEWUCNK hNewucnk, DCI_IN const char *res);


////


// direct
DCI_API CIR dci_CertStorageInit(uint32_t serviceID);


////


typedef uint32_t CIC_CERT_VERIFY;

#define CIC_CERT_VERIFY_SIGN ((CIC_CERT_VERIFY)0x0001)


// direct
DCI_API CIR dci_CertVerify(gmtime64_t now, CIC_CERT_VERIFY flags);


////


DCI_API char *dci_DirectGetErrorMessage();

DCI_API void dci_DirectClearErrorMessage();


////


// key length in octets
#define CIC_KEY_SIZE_128 16		// 128 bits
#define CIC_KEY_SIZE_192 24		// 192 bits
#define CIC_KEY_SIZE_256 32		// 256 bits

// IV length in octets
#define LEN_IV_3DES  8
#define LEN_IV_SEED 16
#define LEN_IV_AES  16

// message digest length
#define LEN_HASH_SHA1 20
#define LEN_HASH_MD5  16


// service id
#define DCA_SERVICEID_DRM 0x1000

// certificate type
#define DCA_CERT_DRM 71
#define DCA_CERT_CAS 72

// user id
#define DCA_UID_CAS "CAS"
#define DCA_UID_DRM "DRM"


////


#if defined(__cplusplus)
}
#endif


#endif
