#include "drm_crypto_port.h"
#include "wise_port.h"
#include <errno.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdarg.h>
#include "systemproperty.h"
#include "DRMCrypto.h"

#include "util_drm_logmgr.h"

#ifdef __cplusplus
extern "C" {
#endif

//	extern int 				errno;
	int _srand_init = 0;

	/**
	 * memory-related functions
	 */
	void *mcu_Malloc ( INT32 size)
	{
		void *ptr = NULL;
		ptr =  wpi_malloc (size);
		return ptr;
	}

	void mcu_Free ( void *ptr)
	{

		if ( NULL == ptr )
		{
			return;
		}
		wpi_free (ptr);
	}

	void* mcu_Realloc ( void *p, INT32 size)
	{

		return wpi_realloc (p,size);
	}

	/**
	 * File I/O related functions
	 */
	HFILE	mcu_FileOpen ( CHAR *fileName, CHAR*mode)
	{
		int iFd = -1;

		if ( !fileName)
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 4 illigal argument \n" ) );
			return MCU_E_INVALID_PARAMETER;
		}
		iFd = wpi_open2 ( fileName, mode);
		if ( iFd < 0 )
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 4 Failed File handler [ %d ] \n", iFd ) );
		}
		else
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 4 Success File handler [ %d ] \n", iFd ) );
		}
		return iFd;
	}

	INT32 	mcu_FileClose ( HFILE hFile)
	{
		if( hFile < 0 )
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 5 illigal argument \n" ) );
			return MCU_E_INVALID_PARAMETER;
		}
		DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 5 [%d] \n", hFile ) );
		return wpi_close2 (hFile);
	}

	INT32 	mcu_FileRead ( HFILE hFile, BYTE *buff, INT32 size)
	{
		if( hFile <= 0 || !buff || size <= 0)
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 6 illigal argument \n" ) );
			return MCU_E_INVALID_PARAMETER;
		}
		DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 6 \n" ) );
		return wpi_read( hFile, buff, size);
	}

	INT32 	mcu_FileWrite ( HFILE hFile, BYTE *buff, INT32 size)
	{
		if( hFile <= 0 || !buff || size <= 0)
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 7 illigal argument \n" ) );
			return MCU_E_INVALID_PARAMETER;
		}
		DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 7 \n" ) );
		return wpi_write ( hFile, buff, size);
	}

	INT32 	mcu_FileSeek ( HFILE hFile, INT32 offset, INT32 opt)
	{
		if( hFile <=0 )
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 8 illigal argument \n" ) );
			return MCU_E_INVALID_PARAMETER;
		}
		if ( 0 > wpi_seek ( hFile, offset, opt) )
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 8 Failed \n" ) );
			return -1;
		}
		else
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 8 Success \n" ) );
			return 0;
		}
	}

	INT32 	mcu_FileTell ( HFILE hFile)
	{
		if( hFile <= 0)
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 9 illigal argument \n" ) );
			return MCU_E_INVALID_PARAMETER;
		}

		DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 9 \n" ) );
		return wpi_tell( hFile);
	}

	INT32 	mcu_FileFlush ( HFILE hFile)
	{
		if( hFile <= 0)
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 10 illigal argument \n" ) );
			return MCU_E_INVALID_PARAMETER;
		}
		DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 10 \n" ) );
		return wpi_flush (hFile);
	}

	INT32 	mcu_FileRemove ( CHAR *fileName)
	{
		INT32 i32Return = -1;
		if( !fileName)
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 11 illigal argument \n" ) );
			return MCU_E_INVALID_PARAMETER;
		}

		if ( 0 == wpi_exist ( fileName ) )
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 11 NotEXIST \n" ) );
			return MCU_E_OK;
		}
		i32Return = wpi_remove ( fileName);
		if ( 0 > i32Return )
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 11 Failed [ %s ]\n", fileName ) );
		}
		else
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 11 success [ %s ] \n", fileName ) );
		}
		return i32Return;
	}

	INT32 	mcu_FileRename( CHAR *oldName, CHAR *newName)
	{
		if( !oldName || !newName)
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 12 illigal argument \n" ) );
			return MCU_E_INVALID_PARAMETER;
		}

		if ( 0 == wpi_exist ( oldName ) )
		{
			return MCU_E_NOT_EXIST;
		}

		DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 12 \n" ) );
		return wpi_rename( oldName, newName);
	}

	INT32	mcu_FileIsExist ( CHAR *fileName)
	{
		if( !fileName)
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 13 illigal argument \n" ) );
			return MCU_E_INVALID_PARAMETER;
		}
		if ( wpi_exist ( fileName) )
		{

			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 13 Success, file Exist [ %s ] \n", fileName ) );
			return 0;
		}
		else
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 13 Fail file Exist [ %s ] \n", fileName ) );
			return MCU_E_NOT_EXIST;
		}
	}

	INT32 	mcu_FileList ( CHAR *folderName, CHAR *buff, INT32 size)
	{
		if( !folderName || !buff || size <= 0)
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 14 illigal argument \n" ) );
			return MCU_E_INVALID_PARAMETER;
		}
		DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 14 \n" ) );
		return wpi_list ( folderName, buff, size);
	}

	INT32 	mcu_FileInfo ( CHAR *fileName, MCU_FILE_INFO *fileInfo)
	{
		struct stat st = {0,};


		if(!fileInfo || !fileName)
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 15 illigal argument \n" ) );
			return MCU_E_INVALID_PARAMETER;
		}
		DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 15 File info [ %s ]\n", fileName ) );
		if ( 0 == wpi_exist ( fileName ) )
		{
			return MCU_E_NOT_EXIST;
		}

		bzero( fileInfo, sizeof(MCU_FILE_INFO));
		if( wpi_stat( fileName, &st) == 0)
		{
			fileInfo->attrib = S_ISDIR(st.st_mode);
			fileInfo->creationTime = (UINT32)st.st_mtime;
			fileInfo->size = (unsigned int)st.st_size;

			return MCU_E_OK;
		}



		return MCU_E_ERROR;
	}

	INT32 	mcu_MkDir ( CHAR *dirName)
	{
		INT32 i32Return = 0;
		struct stat 	st = {0,};
		if( stat( dirName, &st) == 0)
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 16 EXIST [ %s ]\n", dirName ) );
			return MCU_E_OK;
		}
		i32Return = wpi_mkdir_p (dirName);

		if ( 0 > i32Return )
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 16 Failed [ %s ]\n", dirName ) );
			return -1;
		}
		else
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 16 success [ %s ]\n", dirName ) );
			return 0;
		}
	}

	INT32 	mcu_RmDir( CHAR *dirName)
	{
		INT32 i32Return = 0;
		struct stat 	st = {0,};
		if( stat( dirName, &st ) < 0)
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 17 Not Exist \n" ) );
			return MCU_E_OK;
		}
		i32Return = wpi_rmdir(dirName);
		if ( 0 > i32Return )
		{
			if (( errno == ENOTEMPTY ) || ( errno == EEXIST))
			{
				DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 17 ENOTEMPTY \n" ) );
				return MCU_E_EXIST;
			}
			else
			{
				DEBUG_PRINT ( fprintf ( stderr, "DRM_CRYPTO_IMPL.C> API 17 Failed [ %s ]\n", dirName ) );
				return -1;
			}
		}
		else
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRM.CRYPTO.IMPL.C> API 17 Success [ %s ]\n", dirName ) );
			return 0;
		}
	}

	/**
	 * not implemented yet
	 * @TODO  change for ver 0.2
	 */
	INT32 	mcu_GetDeviceID( BYTE *buff, INT32 size)
	{
		bzero(buff, size);

		get_systemproperty(PROPERTY__STB_ID, buff, size);

		return strlen(buff);
	}


	/**
	 * @TODO  change for ver 0.2
	 */
	INT32	mcu_RandomNumber( CHAR *buff, INT32 size)
	{
		int 	pos = 0;

		if( ( buff ) && ( size > 0 ) )
		{
			while( pos < size )
			{
				buff[pos] = (unsigned char) (random()%256);
				pos++;
			}
			DEBUG_PRINT ( fprintf ( stderr, "\n DRM_CRYPTO_IMPL.C> API 19 \n" ) );
			return 0;
		}
		else
		{
			DEBUG_PRINT ( fprintf ( stderr, "\n DRM_CRYPTO_IMPL.C> API 19 illigal argument \n" ) );
			return -1;
		}
	}


	/**
	 * maby return type must be UINT64
	 * update change ver 0.2
	 */
	int64_t	mcu_GetCurrentTime (void)
	{
		time_t now = time(NULL);
		mcu_DebugPrint("mcu_GetCurrentTime : %d\n", now);

		if (now > GMTIME64_MAX) return (int)MCU_E_ERROR;

		mcu_DebugPrint("mcu_GetCurrentTime ret : %d\n", now);

		return (gmtime64_t)now;
	}

	/**
	 * update change ver 0.2
	 */
	INT32 mcu_GetStoragePath (CHAR *buff, INT32 size)
	{
		struct stat		st = {0};

		if(buff != NULL) {
			char dataPath[128];
		    get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
		    strcat(dataPath, CRYPTO_STORAGE_PATH);
			strcpy( buff, dataPath );
			if( wpi_stat( dataPath, &st) < 0)
				wpi_mkdir_p(dataPath );

		} else {
			return 0;
		}
		return strlen( buff );
	}

	INT32 mcu_GetStoragePath2 (CHAR *buff, INT32 size){
		struct stat		st = {0};

		char dataPath[128];
		get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
		strcat(dataPath, CRYPTO_STORAGE_PATH);

		strcpy( buff, dataPath );
		if( wpi_stat( dataPath, &st) < 0)
			wpi_mkdir_p(dataPath );

		return strlen( buff );
	}

	void mcu_DebugPrint( CHAR *format, ...)
	{

		char	buffer[512] = {0};
		va_list  ap;
		va_start( ap, format);
		vsprintf( buffer, (const char *)format, ap);
		va_end(ap);
		fprintf(stderr, "mcu_Debug:%s", buffer);

	}

#ifdef __cplusplus
}
#endif

