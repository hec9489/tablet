/**
 * DRMP Porting Impl
 */
#include "mcas_port.h"
#include "wise_skdrm.h"
#include <unistd.h>
#include "drmp_port.h"
#include "CAAgent.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#include <utile_wscs.h>
// cas portting add by ksyoon 20180725
#include <jsmn.h>
#include "scs-protocol.h"
#include "utile_socket.h"
#include "encBlowFish.h"

#include "util_drm_logmgr.h"

#include "dmux_define.h"
#include "utile_wscs.h"
#include <systemproperty.h>

#ifdef __cplusplus
extern "C" {
#endif

static DC_BOOL 		gsbSKDrmInitFlag 										= FALSE;
static DRMP_CHD 	*gpDRMContentHandler [ MAX_WINDOW_SUPPORT ]		= { 0 };
static DC_INT32		sgi32FileHeaderLeg [ MAX_WINDOW_SUPPORT ]			= { 0 };
DC_CHAR 			*gpcLocReadBuf [ MAX_WINDOW_SUPPORT ]				= { 0};
DC_INT32			gi32SyncFlag	[ MAX_WINDOW_SUPPORT ]				= { 0 };

extern unsigned long long WISE_GetTaskTime ( void );


extern int wpi_is_test_mode(void);
extern int wpi_get_interface_info( const char *eth, int infotype, char *rtnval, int fmttype );
//extern KEY_PLAY_MODE whp_getPlayMode ( int iWindowId );
extern unsigned long long  RMGetTimeInMicroSeconds(void);
extern DC_INT16  process_cert_update(void);

// cas portting add by ksyoon 20180725
static int jsoneq(const char *json, jsmntok_t *tok, const char *s){
	if(tok->type == JSMN_STRING && (int) strlen(s) == tok->end - tok->start &&
		strncmp(json + tok->start, s, tok->end - tok->start) == 0 ){
		return 0;
	}
	return -1;
}

int remove_directory(const char *path) {
	DIR *d = opendir(path);
	size_t path_len = strlen(path);
	int r = -1;

	if (d) {
		struct dirent *p;
		r = 0;

		while (!r && (p = readdir(d))) {
			int r2 = -1;
			char *buf;
			size_t len;

			/* Skip the names "." and ".." as we don't want to recurse on them. */
			if (!strcmp(p->d_name, ".") || !strcmp(p->d_name, "..")) {
				continue;
			}

			len = path_len + strlen(p->d_name) + 2;
			buf = malloc(len);

			if (buf) {
				struct stat statbuf;

				snprintf(buf, len, "%s/%s", path, p->d_name);

				if (!stat(buf, &statbuf)) {
					if (S_ISDIR(statbuf.st_mode)) {
						r2 = remove_directory(buf);
					} else {
						r2 = unlink(buf);
					}
				}

				free(buf);
			}

			r = r2;
		}

		closedir(d);
	}

	if (!r) {
		r = rmdir(path);
	}

	return r;
}

static char content_ip[32];
static int content_port;

void setContentAddressInfo(char *ip, int port) {
	memset(content_ip, 0L, 32);
	sprintf(content_ip, "%s", ip);
	content_port = port;
}

void clearContentAddressInfo() {
	memset(content_ip, 0L, 32);
	content_port = 0;
}

DC_INT32 WISE_SKDRM_Init( DC_VOID) {
	DC_INT32 i32ErrorCode = -1, retry_count = 0;
	DC_INT32 i32InitFlag	= 1;

	DPRINTSRC("INFO:[%s]", __func__);
	if (TRUE == WISE_SKDRM_getInitFlag()) {
		LOGSRC("!!!!!!!!!!!!!!!!!!!!!!!! %d ", __LINE__);
		WISE_SKDRM_Destroy();
	}
	LOGSRC("!!!!!!!!!!!!!!!!!!!!!!!! %d ", __LINE__);

	drm_init:
	/* Initializ the SKDRM */
	if ( DRMP_NO_ERROR == (i32ErrorCode = DRMP_Command(WISE_SKDRM_INIT, NULL))) {
		DPRINTSRC("[%s] WISE_SKDRM_INIT Success!!!!", __func__);
		WISE_SKDRM_setInitFlag(TRUE);

		gpcLocReadBuf[0] = WISE_SKDRM_MALLOC(WISE_SKDRM_DECRYPT_LOC_BUFFER_SIZE);
		if (NULL == gpcLocReadBuf[0]) {
			DPRINTSRC("ERROR:[%s]", __func__);
			return -1;
		}

		gpcLocReadBuf[1] = WISE_SKDRM_MALLOC(WISE_SKDRM_DECRYPT_LOC_BUFFER_SIZE);
		if ( NULL == gpcLocReadBuf[1]) {
			WISE_SKDRM_Free(gpcLocReadBuf[0]);
			DPRINTSRC("ERROR:[%s]", __func__);
			return -1;
		}

		DPRINTSRC("INFO:[%s]", __func__);
		return DRMP_NO_ERROR;
	} else {
		DPRINTSRC("[%s] ERROR: WISE_SKDRM_INIT : i32ErrorCode=%d", __func__, i32ErrorCode);
		if (( ERROR_FILE_FAIL_OPEN_CERTIFICATE == i32ErrorCode || -0x00150075 == i32ErrorCode) && (retry_count < 3) /*&& ( DRMP_NO_ERROR == process_reg_confirm())*/) {
			//PENVI_INFO pEnviConfig = ENVI_GetConfig();

			//if (pEnviConfig) pEnviConfig->nAuthenticated = 0;

			retry_count++;
			goto drm_init;

		} else if ((i32ErrorCode >= ERROR_DBMGR_DAMAGED_DATA) && (i32ErrorCode <= ERROR_DBMGR_ABORT) && (retry_count < 3)) {
			// DB File이 깨진 경우의 에러 처리
			// DRMPDB.dat를 지우고 DRM Init를 다시 해야 한다.
			DPRINTSRC("[%s] ERROR: remove %s", __func__, DRMPDB_PATH);
			DPRINTSRC("[%s] Re Initialize SKDRM....", __func__);
			remove(DRMPDB_PATH);
			//drm_error_report(i32ErrorCode, content_ip, content_port);

			retry_count++;
			goto drm_init;

		} else if((i32ErrorCode == -0x00030009 || i32ErrorCode == 0x00150025 || i32ErrorCode == -0x00160108 || i32ErrorCode == -0x00160108 ||
				i32ErrorCode == -0x00160210 || i32ErrorCode == -0x00060001 || i32ErrorCode == -0x00160209) && retry_count < 3) {
			/*
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMP_PATH);
			remove_directory(DRMP_PATH);
			*/
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
			remove(DRMPDB_PATH);
			//drm_error_report(i32ErrorCode, content_ip, content_port);

			retry_count++;
			goto drm_init;
		}

		//drm_error_report(i32ErrorCode, content_ip, content_port);
		WISE_SKDRM_setInitFlag(FALSE);
		DPRINTSRC("ERROR:[%s]", __func__);
		return -1;
	}

	DPRINTSRC("INFO:[%s]", __func__);
	return DRMP_NO_ERROR;
}

DC_INT32 WISE_SKDRM_Destroy( DC_VOID) {
	DC_INT32 i32Return;

	DPRINTSRC("INFO:[%s]", __func__);
	if (WISE_SKDRM_getInitFlag()) {
		DC_INT8 i8Loop;

		for (i8Loop = 0; i8Loop < MAX_WINDOW_SUPPORT; i8Loop++) {
			if ( NULL != gpDRMContentHandler[i8Loop]) {
				/*If we have got the content handler, Should delete the content handler while go to destroy SKDRM module*/
				i32Return = WISE_SKDRM_DeleteContentHandler(i8Loop);
				if ( DRMP_NO_ERROR != i32Return) {
					DPRINTSRC("[%s] ERROR:%d", __func__, i32Return);
					return -1;
				}
			}
		}
		/*Destroy the SKDRM Module*/
		i32Return = DRMP_Command( WISE_SKDRM_DESTROY, NULL);
		if ( DRMP_NO_ERROR == i32Return) {
			WISE_SKDRM_setInitFlag(FALSE);
			WISE_SKDRM_Free(gpcLocReadBuf[0]);
			WISE_SKDRM_Free(gpcLocReadBuf[1]);
			DPRINTSRC("INFO:[%s]", __func__);
			return DRMP_NO_ERROR;
		} else {
			DPRINTSRC("[%s] ERROR:%d", __func__, i32Return);
			// DB File이 깨진 경우의 에러 처리
			// DRMPDB.dat를 지우고 DRM Init를 다시 해야 한다.
			if ((i32Return >= ERROR_DBMGR_DAMAGED_DATA) && (i32Return <= ERROR_DBMGR_ABORT)) {
				DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
				remove(DRMPDB_PATH);
				WISE_SKDRM_Init();
			}
			else if(i32Return == -0x00030009 || i32Return == 0x00150025 || i32Return == -0x00160108 ||
						i32Return == -0x00160108 || i32Return == -0x00160210 || i32Return == -0x00060001 || i32Return == -0x00160209) {
				/*
				DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMP_PATH);
				remove_directory(DRMP_PATH);
				*/
				DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
				remove(DRMPDB_PATH);
				WISE_SKDRM_Init();
			}

			//drm_error_report(i32Return, content_ip, content_port);
			return -1;
		}
	}

	DPRINTSRC("[%s] SUCCESS", __func__);
	return -1;
}

DC_VOID WISE_SKDRM_setInitFlag(DC_BOOL bFlag) {
	gsbSKDrmInitFlag = bFlag;
}

DC_BOOL WISE_SKDRM_getInitFlag( DC_VOID) {
	return gsbSKDrmInitFlag;
}

DC_INT32 WISE_SKDRM_BigToLittleEndian( DC_INT32 i32LegInBigEndian) {
	int i32LegInLittleEndian;
	char *cLittle = (char*) &i32LegInLittleEndian;
	char *cBig = (char*) &i32LegInBigEndian;
	cLittle[0] = cBig[3];
	cLittle[1] = cBig[2];
	cLittle[2] = cBig[1];
	cLittle[3] = cBig[0];

	return i32LegInLittleEndian;
}

/*For check this stream have SKDRM data or not */
DC_BOOL WISE_SKDRM_IsThisSKDRMData( DC_INT32 i32fd) {
	DC_INT32	i32BytesRead = 0;
	DC_CHAR 	cHeaderTag [ SKDRM_HEADER_TAG_LENGTH  + 1 ] = { 0 };

	DPRINTSRC("INFO:[%s]", __func__);

	i32BytesRead = lseek ( i32fd, 0, SEEK_SET);
	i32BytesRead = read( i32fd , cHeaderTag, SKDRM_HEADER_TAG_LENGTH ); 	/*4byte*/

	cHeaderTag [ SKDRM_HEADER_TAG_LENGTH ] = '\0';
	if ( !( strcmp ( cHeaderTag, SKDRM_HEADER_TAG ) ) ){
		DPRINTSRC("INFO:[%s]", __func__);
		return TRUE;
	} else{
		lseek( i32fd, 0, SEEK_SET );
		DPRINTSRC("INFO:[%s]", __func__);
		return FALSE;
	}
}

/*Stream have header length in big endian formate, So we want to converted to little endian form*/
DC_INT32 WISE_SKDRM_getDRMHeaderLengthInLittleEndian( DC_INT32 i32fd) {
	DC_INT32 i32BytesRead = 0, i32LegInBigEndian = 0, i32LegInLittleEndian = 0;
	DC_CHAR cHeaderLen[SKDRM_HEADER_LENGTH_FILED + 1] = { 0 };

	DPRINTSRC("INFO:[%s]", __func__);
	i32BytesRead = lseek(i32fd, 4, SEEK_SET);
	DEBUG_PRINT ( fprintf ( stderr, "WISE-SKDRM.C> Moved file pointer for < %d >bytes \n", i32BytesRead ) );
	i32BytesRead = read(i32fd, cHeaderLen, SKDRM_HEADER_LENGTH_FILED); /*4byte*/
	if ( SKDRM_HEADER_LENGTH_FILED != i32BytesRead) {
		DEBUG_PRINT ( fprintf ( stderr, "WISE-SKDRM.C> Header length field read failed \n" ) );
		return 0;
	}
	DEBUG_PRINT ( fprintf ( stderr, "WISE-SKDRM.C> Read the < %d >bytes of header len \n", i32BytesRead ) );
	cHeaderLen[ SKDRM_HEADER_LENGTH_FILED] = '\0';

	/*Conver header length bigendian to little endian form*/
	i32LegInBigEndian = *(( DC_INT32 *) cHeaderLen);
	i32LegInLittleEndian = WISE_SKDRM_BigToLittleEndian(i32LegInBigEndian);

	DPRINTSRC("INFO:[%s]", __func__);
	return i32LegInLittleEndian;
}

DC_INT32 WISE_SKDRM_CreatContentHandler( DC_INT32 i32fd, DC_INT32 i32WindowID, DC_CHAR *ContentID) {
	DPRINTSRC("INFO:[%s]", __func__);
#if 0 /* to remove warnings - blocked by chyi, 08/17/2009 */
	DRMP_COMMAND_PARAM stCommandParam = { 0 };
#else
	DRMP_COMMAND_PARAM stCommandParam;
#endif
	DRMP_CHD *pDRMHandler = NULL;
	DC_INT32 i32LegInLittleEndian = 0, i32BytesRead = 0, i32Return = 0;
	DC_CHAR *pcSKDRMHeaderData = NULL;

#if 1 /* by chyi, 08/17/2009 */
	memset(&stCommandParam, 0, sizeof(DRMP_COMMAND_PARAM));
#endif

	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, drmpdb_path_, 128);
	strcat(drmpdb_path_, DRMPDB_PATH);

	if ( NULL != gpDRMContentHandler[i32WindowID]) {
		i32Return = WISE_SKDRM_DeleteContentHandler(i32WindowID);
		if ( DRMP_NO_ERROR != i32Return) {
			DPRINTSRC("[%s] ERROR:%d", __func__, i32Return);
			return -1;
		}
	}

	if (TRUE != WISE_SKDRM_IsThisSKDRMData(i32fd)) {
		DPRINTSRC("[%s] ERROR: WISE_SKDRM_IsThisSKDRMData FALSE", __func__);
		return -2;
	}
	gi32SyncFlag[i32WindowID] = 0;
	/*File Formate
	 *	---------------------------------------------------------------------------------
	 *	|Header tag (4 byte) | Header length field (4 byte) | Header data |	Data.........				|
	 *	---------------------------------------------------------------------------------
	 */
	i32LegInLittleEndian = WISE_SKDRM_getDRMHeaderLengthInLittleEndian(i32fd);
	if (0 >= i32LegInLittleEndian) {
		DPRINTSRC("[%s] ERROR:i32LegInLittleEndiam = %d", __func__, i32LegInLittleEndian);
		return -1;
	}

	sgi32FileHeaderLeg[i32WindowID] = i32LegInLittleEndian;

	pcSKDRMHeaderData = WISE_SKDRM_MALLOC(i32LegInLittleEndian);
	if ( NULL == pcSKDRMHeaderData) {
		DPRINTSRC("[%s] ERROR:pcSKDRMHeaderData=NULL", __func__);
		return -1;
	}
	i32BytesRead = lseek(i32fd, 0, SEEK_SET);
	/*Read the header data with header tag and header length field*/
	i32BytesRead = read(i32fd, pcSKDRMHeaderData, i32LegInLittleEndian);
	if (i32BytesRead != i32LegInLittleEndian) {
		DPRINTSRC("[%s] ERROR: i32BytesRead=%d, i32LegInLittleEndian=%d", __func__, i32BytesRead, i32LegInLittleEndian);
		return -1;
	}

	memset(&stCommandParam, 0, sizeof(DRMP_COMMAND_PARAM));
	stCommandParam.param1 = (DC_PVOID) pcSKDRMHeaderData;
	stCommandParam.param2 = (DC_PVOID) i32LegInLittleEndian;
	stCommandParam.param3 = (DC_PVOID) &pDRMHandler;
	stCommandParam.param4 = (DC_PVOID) ContentID; //TODO

	if ( DRMP_NO_ERROR == (i32Return = DRMP_Command( WISE_SKDRM_CREATE_CONTENT_HANDLER, &stCommandParam))) {
		DPRINTSRC("[%s] WISE_SKDRM_CREATE_CONTENT_HANEDLER Success", __func__);
		gpDRMContentHandler[i32WindowID] = pDRMHandler;
		WISE_SKDRM_Free(pcSKDRMHeaderData);
		i32Return = WISE_SKDRM_IsROExist(pDRMHandler);
		if ( DRMP_NO_ERROR != i32Return) {
			DPRINTSRC("[%s] ERROR: WISE_SKDRM_IsROExist => %d", __func__, i32Return);
/*			
			// DB File이 깨진 경우의 에러 처리
			// DRMPDB.dat를 지우고 DRM Init를 다시 해야 한다.
			if ((i32Return >= ERROR_DBMGR_DAMAGED_DATA) && (i32Return <= ERROR_DBMGR_ABORT))
			{
				DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
				remove(DRMPDB_PATH);
				WISE_SKDRM_Init();
			}
*/			
			return -1;
		}

		DPRINTSRC("INFO:[%s]", __func__);
		return DRMP_NO_ERROR;
	} else {
		WISE_SKDRM_Free(pcSKDRMHeaderData);
		DPRINTSRC("[%s] ERROR:%d", __func__, i32Return);
		// DB File이 깨진 경우의 에러 처리
		// DRMPDB.dat를 지우고 DRM Init를 다시 해야 한다.
		if ((i32Return >= ERROR_DBMGR_DAMAGED_DATA) && (i32Return <= ERROR_DBMGR_ABORT))
		{
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
			remove(DRMPDB_PATH);
			WISE_SKDRM_Init();
		}
		else if(i32Return == -0x00030009 || i32Return == 0x00150025 || i32Return == -0x00160108 ||
				i32Return == -0x00160108 || i32Return == -0x00160210 || i32Return == -0x00060001 || i32Return == -0x00160209)
		{
			/*
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMP_PATH);
			remove_directory(DRMP_PATH);
			*/
			
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
			remove(DRMPDB_PATH);
			WISE_SKDRM_Init();
		}

		//drm_error_report(i32Return, content_ip, content_port);
		return -1;
	}
}

DC_INT32 WISE_SKDRM_CheckRO( DC_INT32 i32fd, DC_CHAR *ContentID)
{
	DRMP_COMMAND_PARAM stCommandParam;
	DRMP_CHD *pDRMHandler = NULL;
	DC_INT32 i32LegInLittleEndian = 0, i32BytesRead = 0, i32Return = 0;
	DC_CHAR *pcSKDRMHeaderData = NULL;

	DPRINTSRC("[%s] INFO", __func__);
	memset(&stCommandParam, 0, sizeof(DRMP_COMMAND_PARAM));

	if (TRUE != WISE_SKDRM_IsThisSKDRMData(i32fd)) {
		DPRINTSRC("ERROR:[%s]", __func__);
		return -1;
	}

	/*File Formate
	 *	---------------------------------------------------------------------------------
	 *	|Header tag (4 byte) | Header length field (4 byte) | Header data |	Data.........				|
	 *	---------------------------------------------------------------------------------
	 */
	i32LegInLittleEndian = WISE_SKDRM_getDRMHeaderLengthInLittleEndian(i32fd);
	if (0 >= i32LegInLittleEndian) {
		DPRINTSRC("[%s] ERROR: i32LegInLittleendian=%d", __func__, i32LegInLittleEndian);
		return -1;
	}

	pcSKDRMHeaderData = WISE_SKDRM_MALLOC(i32LegInLittleEndian);
	if ( NULL == pcSKDRMHeaderData) {
		DPRINTSRC("[%s] ERROR: pcSKDRMHeaderData=NULL", __func__);
		return -1;
	}
	i32BytesRead = lseek(i32fd, 0, SEEK_SET);
	/*Read the header data with header tag and header length field*/
	i32BytesRead = read(i32fd, pcSKDRMHeaderData, i32LegInLittleEndian);
	if (i32BytesRead != i32LegInLittleEndian) {
		DPRINTSRC("[%s] ERROR: i32BytesRead=%d, i32LegInLittleEndian=%d", __func__, i32BytesRead, i32LegInLittleEndian);
		return -1;
	}

	memset(&stCommandParam, 0, sizeof(DRMP_COMMAND_PARAM));
	stCommandParam.param1 = (DC_PVOID) pcSKDRMHeaderData;
	stCommandParam.param2 = (DC_PVOID) i32LegInLittleEndian;
	stCommandParam.param3 = (DC_PVOID) &pDRMHandler;
	stCommandParam.param4 = (DC_PVOID) ContentID;

	if ( DRMP_NO_ERROR == (i32Return = DRMP_Command( WISE_SKDRM_CREATE_CONTENT_HANDLER, &stCommandParam))) {
		WISE_SKDRM_Free(pcSKDRMHeaderData);

		i32Return = WISE_SKDRM_IsROExist ( pDRMHandler );
		if ( DRMP_NO_ERROR != i32Return){
			DPRINTSRC("[%s] ERROR: i32Return", __func__);
			return -1;
		}

		DPRINTSRC("INFO:[%s]", __func__);
		return DRMP_NO_ERROR;
	} else{
		WISE_SKDRM_Free ( pcSKDRMHeaderData );
		DPRINTSRC("[%s] ERROR: WISE_SKDRM_CREATE_CONTENT_HANDLER => %d", __func__, i32Return);
		// DB File이 깨진 경우의 에러 처리
		// DRMPDB.dat를 지우고 DRM Init를 다시 해야 한다.
		if ((i32Return >= ERROR_DBMGR_DAMAGED_DATA) && (i32Return <= ERROR_DBMGR_ABORT))
		{
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
			remove(DRMPDB_PATH);
			WISE_SKDRM_Init();
		}
		else if(i32Return == -0x00030009 || i32Return == 0x00150025 || i32Return == -0x00160108 ||
				i32Return == -0x00160108 || i32Return == -0x00160210 || i32Return == -0x00060001 || i32Return == -0x00160209)
		{
			/*
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMP_PATH);
			remove_directory(DRMP_PATH);
			*/
			
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
			remove(DRMPDB_PATH);
			WISE_SKDRM_Init();
		}

		//drm_error_report(i32Return, content_ip, content_port);
		return -1;
	}
}

DC_INT32 WISE_SKDRM_IsROExist(DRMP_CHD *pDRMHandler) {
	DRMP_COMMAND_PARAM stCommandParam;
	DC_PCHAR pcTriggeredData = NULL;
	DC_INT32 i32Return = 0, i32DataLen = 0;
	DC_CHAR acData[WISE_TIGGER_DATA_LEN] = { 0 };
	DC_BYTE cCommandID = 0, cCommandType1 = 0, cCommandType2 = 0;
	DC_INT32 retry_count = 0;

	DPRINTSRC("INFO:[%s]", __func__);
	memset(&stCommandParam, 0, sizeof(DRMP_COMMAND_PARAM));


	LOGSRC("@@@@@@@@@@@@@@@@@@@@@@@@@@@ %d ", __LINE__);
	while (TRUE) {
		memset(acData, 0, sizeof(acData));
		stCommandParam.param1 = pDRMHandler;
		stCommandParam.param2 = &cCommandID;
		stCommandParam.param3 = &cCommandType1;
		stCommandParam.param4 = &cCommandType2;
		stCommandParam.param5 = acData;

		if ( DRMP_NO_ERROR == (i32Return = DRMP_Command( WISE_SKDRM_RO_EXIST, &stCommandParam))) {
			DPRINTSRC("INFO:[%s]", __func__);
			LOGSRC("@@@@@@@@@@@@@@@@@@@@@@@@@@@ %d ", __LINE__);
			break;
		}

		try_third:
		/*Request the trigger to SCS*/
		i32Return = WISE_SKDRM_getTrigger(cCommandID, cCommandType1, cCommandType2, acData, &pcTriggeredData, &i32DataLen);

		LOGSRC("@@@@@@@@@@@@@@@@@@@@@@@@@@@ %d ", __LINE__);
		if (i32Return != 0) // bb 매니저님 요청사항으로 바로 error return
		{
			LOGSRC("@@@@@@@@@@@@@@@@@@@@@@@@@@@ %d ", __LINE__);
			return -1;
		}

		DPRINTSRC("[%s] i32DataLen = %d\n", __func__, i32DataLen);

		/*Send tiggered data to DRM agent*/
		memset(&stCommandParam, 0, sizeof(DRMP_COMMAND_PARAM));
		stCommandParam.param1 = pcTriggeredData;
		stCommandParam.param2 = (DC_PVOID) i32DataLen;

		LOGSRC("@@@@@@@@@@@@@@@@@@@@@@@@@@@ %d ", __LINE__);
		i32Return = DRMP_Command ( WISE_SKDRM_PROCESS_ROAP, &stCommandParam );

		DPRINTSRC("[%s] i32Return = %d\n", __func__, i32Return);

		if ( DRMP_NO_ERROR != i32Return)
		{
			LOGSRC("@@@@@@@@@@@@@@@@@@@@@@@@@@@ %d ", __LINE__);
			DPRINTSRC("[%s] ERROR: WISE_SKDRM_PROCESS_ROAP =>%d", __func__, i32Return);
			if (pcTriggeredData) {
				WISE_SKDRM_Free(pcTriggeredData);
				pcTriggeredData = NULL;
			}

			LOGSRC("@@@@@@@@@@@@@@@@@@@@@@@@@@@ %d ", __LINE__);
			if (( ERROR_ROAPMGR_SOCKET_ERROR == i32Return) && (3 > ++retry_count)) {
				DPRINTSRC("ERROR:[%s] ERROR_ROAPMGR_SOCKET_ERROR", __func__);
				goto try_third;
			}
			else if((i32Return == -0x00030009 || i32Return == 0x00150025 || i32Return == -0x00160108 || i32Return == -0x00160108
					|| i32Return == -0x00160210 || i32Return == -0x00060001 || i32Return == -0x00160209)) {
				/*
				DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMP_PATH);
				remove_directory(DRMP_PATH);
				*/
				
				DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
				remove(DRMPDB_PATH);
			
				WISE_SKDRM_Init();
				//drm_error_report(i32Return, content_ip, content_port);
				return i32Return;
			} else {
				LOGSRC("@@@@@@@@@@@@@@@@@@@@@@@@@@@ %d ", __LINE__);
				// DB File이 깨진 경우의 에러 처리
				// DRMPDB.dat를 지우고 DRM Init를 다시 해야 한다.
				if ((i32Return >= ERROR_DBMGR_DAMAGED_DATA) && (i32Return <= ERROR_DBMGR_ABORT))
				{
					DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
					remove(DRMPDB_PATH);
					WISE_SKDRM_Init();
				}

				// 2012.02.01
				// 인증서 만료 : 40023, 인증서 없음:150075 에러일 경우 인증서를 다시 받음
				// R401 에러를 올린 후 리턴... 즉, 추가 시나리오는 없음.....

				//drm_error_report(i32Return, content_ip, content_port);
				DPRINTSRC("ERROR:[%s]", __func__);
				return i32Return;
			}
		}

		LOGSRC("@@@@@@@@@@@@@@@@@@@@@@@@@@@ %d ", __LINE__);
		if (pcTriggeredData) {
			WISE_SKDRM_Free(pcTriggeredData);
			pcTriggeredData = NULL;
		}
	}

	LOGSRC("@@@@@@@@@@@@@@@@@@@@@@@@@@@ %d ", __LINE__);
	DPRINTSRC("INFO:[%s]", __func__);
	return DRMP_NO_ERROR;
}

DRMP_CHD* WISE_SKDRM_getSKDRMContentHandler( DC_INT32 i32WindowID) {
	return gpDRMContentHandler[i32WindowID];
}

DC_INT32 WISE_SKDRM_DeleteContentHandler( DC_INT32 i32WindowID) {
	DRMP_COMMAND_PARAM stCommandParam;
	DC_INT32 i32Return;

	DPRINTSRC("INFO:[%s]", __func__);
	memset(&stCommandParam, 0, sizeof(DRMP_COMMAND_PARAM));

	stCommandParam.param1 = gpDRMContentHandler[i32WindowID];
	i32Return = DRMP_Command( WISE_SKDRM_DELETE_CONTENT_HANDLER, &stCommandParam);
	if ( DRMP_NO_ERROR == i32Return) {
		gpDRMContentHandler[i32WindowID] = NULL;
		DPRINTSRC("INFO:[%s]", __func__);
		return DRMP_NO_ERROR;
	} else {
		DPRINTSRC("[%s] ERROR: WISE_SKDRM_DELETE_CONTENT_HANDER => %d", __func__, i32Return);
		// DB File이 깨진 경우의 에러 처리
		// DRMPDB.dat를 지우고 DRM Init를 다시 해야 한다.
		if ((i32Return >= ERROR_DBMGR_DAMAGED_DATA) && (i32Return <= ERROR_DBMGR_ABORT)) {
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
			remove(DRMPDB_PATH);
			WISE_SKDRM_Init();
		} else if(i32Return == -0x00030009 || i32Return == 0x00150025 || i32Return == -0x00160108 ||
				i32Return == -0x00160108 || i32Return == -0x00160210 || i32Return == -0x00060001 || i32Return == -0x00160209) {
			/*
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMP_PATH);
			remove_directory(DRMP_PATH);
			*/
			
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
			remove(DRMPDB_PATH);
			WISE_SKDRM_Init();
		}

		//drm_error_report(i32Return, content_ip, content_port);
		return i32Return;
	}
}

DC_INT32 WISE_SKDRM_DecryptTS ( DC_INT32 i32fd, DC_CHAR *pcOutBuf, DC_INT32 i32BufLen, DC_INT32 i32WindowID ) {
	DRMP_COMMAND_PARAM		stCommandParam;
	DC_INT32 	i32DecBufCount 			= 0;
	DC_INT32	i32Count 					= 0,
				i32Return					= 0,
//				CurOffset				= 0,
				i32NoOfDecTpCount 		= { 0 },
				i32NoOfByteInSingleRead	= WISE_SKDRM_DECRYPT_LOC_BUFFER_SIZE;
	DC_UINT32	CurOffset = 0;
	DC_CHAR	*pcLocBuf 				= NULL;

	DPRINTSRC("INFO:[%s]", __func__);

	memset(&stCommandParam, 0, sizeof(DRMP_COMMAND_PARAM));

	/*Skip the file header data, Because we doesn't need to decrypt the file header data*/
	CurOffset = lseek64(i32fd, 0, SEEK_CUR);
	if (CurOffset < (DC_UINT32) sgi32FileHeaderLeg[i32WindowID]) {
		lseek64(i32fd, sgi32FileHeaderLeg[i32WindowID], SEEK_SET);
	}

	pcLocBuf = gpcLocReadBuf[i32WindowID];

#if 0
	enPlayMode = whp_getPlayMode (i32WindowID);

	if ( ( gi32SyncFlag [ i32WindowID ] < 4 ) || ( KEY_REW_X2_INDEX == enPlayMode ) ||( KEY_REW_X4_INDEX == enPlayMode ) ||
			( KEY_REW_X8_INDEX == enPlayMode ) || ( KEY_REW_X16_INDEX == enPlayMode ) )
#endif /* BOGUS */
	{
		if (TRUE != WISE_FindingSyncByte(i32fd, pcLocBuf, i32NoOfByteInSingleRead)) {
			gi32SyncFlag[i32WindowID] = 0;
			DPRINTSRC("ERROR:[%s]", __func__);
			return -1;
		}
		//		gi32SyncFlag [ i32WindowID ] ++;
	}

	/* Read the encrypted data from file and decrypte the data then stored in outbuffer */
	while (TRUE) {
		if ((i32DecBufCount + i32NoOfByteInSingleRead) > i32BufLen) {
			i32NoOfByteInSingleRead = i32BufLen - i32DecBufCount;
			if ( WISE_SKDRM_TP_LEN > i32NoOfByteInSingleRead) {
				DPRINTSRC("INFO:[%s]", __func__);
				break;
			}

			i32NoOfByteInSingleRead = (i32NoOfByteInSingleRead / WISE_SKDRM_TP_LEN) * WISE_SKDRM_TP_LEN;
		}

		/*Read the encrypted data as like multiple of 188*/
		i32Count = read(i32fd, pcLocBuf, i32NoOfByteInSingleRead);
		DPRINTSRC("INFO:[%s]", __func__);
		if (i32DecBufCount == 0 && i32Count == 0) {
			DPRINTSRC("[%s] ERROR: i32DecBufCount == 0 && i32Count == 0", __func__);
			return 0;
		}

		stCommandParam.param1 = gpDRMContentHandler[i32WindowID];
		stCommandParam.param2 = pcLocBuf;
		stCommandParam.param3 = pcOutBuf + i32DecBufCount;
		stCommandParam.param4 = (DC_PVOID) (i32Count / WISE_SKDRM_TP_LEN);
		stCommandParam.param5 = (DC_PVOID) &i32NoOfDecTpCount;
		//		ptime1 = 	RMGetTimeInMicroSeconds ( ); //WISE_GetTaskTime (  );
		if ( DRMP_NO_ERROR != (i32Return = DRMP_Command( WISE_SKDRM_DECRYPT_TS, &stCommandParam))) {
			DPRINTSRC("[%s] ERROR: WISE_SKDRM_DECRYPT_TS => %d", __func__, i32Return);
			// DB File이 깨진 경우의 에러 처리
			// DRMPDB.dat를 지우고 DRM Init를 다시 해야 한다.
			if ((i32Return >= ERROR_DBMGR_DAMAGED_DATA) && (i32Return <= ERROR_DBMGR_ABORT))
			{
				DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
				remove(DRMPDB_PATH);
				WISE_SKDRM_Init();
			}
			else if(i32Return == -0x00030009 || i32Return == 0x00150025 || i32Return == -0x00160108 ||
					i32Return == -0x00160108 || i32Return == -0x00160210 || i32Return == -0x00060001 || i32Return == -0x00160209)
			{
				/*
				DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMP_PATH);
				remove_directory(DRMP_PATH);
				*/
				
				DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
				remove(DRMPDB_PATH);
				WISE_SKDRM_Init();
			}

			//drm_error_report(i32Return, content_ip, content_port);
			break;
		}

		i32DecBufCount += i32Count;
	}

	DPRINTSRC("INFO:[%s]", __func__);
	return i32DecBufCount;
}

char g_xbuf[188 * 500];
static int remove_tsh( DC_CHAR *i_buf, DC_INT32 tp_count)
{
#if 0
	DC_CHAR *xbuf = malloc( tp_count * 188 );
#else
	DC_CHAR *xbuf = g_xbuf;
#endif
	DC_INT32 loop_count = 0;

	memcpy(xbuf, i_buf, tp_count * 188);

	while (tp_count > 0)
	{

		memcpy(i_buf + loop_count * 184, xbuf + loop_count * 188 + 4, 184);
		//		fprintf( stderr, "[%2X]", *(xbuf + loop_count * 188 ) );

		tp_count--;
		loop_count++;
	}

#if 0
	free( xbuf );
#endif

	return 0;
}

/* for melon service. */
DC_INT32 WISE_SKDRM_DecryptTS2 ( DC_INT32 i32fd, DC_CHAR *pcOutBuf, DC_INT32 i32BufLen, DC_INT32 i32WindowID ) {
	DRMP_COMMAND_PARAM		stCommandParam;
	DC_INT32 	i32DecBufCount 			= 0;
	DC_INT32 	i32DecTpCount 			= 0;
	DC_INT32	i32Count 					= 0,
				i32Return					= 0,
				CurOffset				= 0,
				i32NoOfDecTpCount 		= { 0 },
				i32NoOfByteInSingleRead	= WISE_SKDRM_DECRYPT_LOC_BUFFER_SIZE;
	DC_CHAR	*pcLocBuf 				= NULL;

	DPRINTSRC("INFO:[%s]", __func__);
	memset(&stCommandParam, 0, sizeof(DRMP_COMMAND_PARAM));

	/*Skip the file header data, Because we doesn't need to decrypt the file header data*/
	CurOffset = lseek(i32fd, 0, SEEK_CUR);

	if ( CurOffset < sgi32FileHeaderLeg [ i32WindowID ] ){
		lseek ( i32fd, sgi32FileHeaderLeg [ i32WindowID ], SEEK_SET );
	}

	if( ( CurOffset - sgi32FileHeaderLeg [ i32WindowID ] )%188 != 0 ){

		CurOffset -= sgi32FileHeaderLeg [ i32WindowID ];
		CurOffset = CurOffset / 188 * 188;
		CurOffset += sgi32FileHeaderLeg[i32WindowID];
		lseek(i32fd, CurOffset, SEEK_SET);
	}

	pcLocBuf = gpcLocReadBuf[i32WindowID];

	/* Read the encrypted data from file and decrypte the data then stored in outbuffer */
	while ( TRUE ) {
		if ( ( i32DecBufCount + i32NoOfByteInSingleRead ) > i32BufLen ){
			i32NoOfByteInSingleRead = i32BufLen - i32DecBufCount;
			if ( WISE_SKDRM_TP_LEN > i32NoOfByteInSingleRead ){
				break;
			}

			i32NoOfByteInSingleRead = (i32NoOfByteInSingleRead / WISE_SKDRM_TP_LEN) * WISE_SKDRM_TP_LEN;
		}

		/*Read the efncrypted data as like multiple of 188*/
		i32Count = read ( i32fd, pcLocBuf, i32NoOfByteInSingleRead );
		if( i32Count <= 0 ) {
			usleep( 200000 );
			DPRINTSRC("ERROR:[%s]", __func__);
			return -1;
		}


		stCommandParam .param1	= gpDRMContentHandler [ i32WindowID ];
		stCommandParam .param2	= pcLocBuf;
		stCommandParam .param3 	= pcOutBuf + i32DecBufCount;
		stCommandParam .param4 	= (DC_PVOID)(i32Count/WISE_SKDRM_TP_LEN);
		stCommandParam .param5 	= (DC_PVOID)&i32NoOfDecTpCount;
		if ( DRMP_NO_ERROR != ( i32Return = DRMP_Command ( WISE_SKDRM_DECRYPT_TS, &stCommandParam ) ) ) {
			DPRINTSRC("[%s] ERROR: WISE_SKDRM_DECRYPT_TS => %d", __func__, i32Return);
			// DB File이 깨진 경우의 에러 처리
			// DRMPDB.dat를 지우고 DRM Init를 다시 해야 한다.
			if ((i32Return >= ERROR_DBMGR_UPDATE) && (i32Return <= ERROR_DBMGR_ABORT))
			{
				DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
				remove(DRMPDB_PATH);
				WISE_SKDRM_Init();
			}
			else if(i32Return == -0x00030009 || i32Return == 0x00150025 || i32Return == -0x00160108 ||
					i32Return == -0x00160108 || i32Return == -0x00160210 || i32Return == -0x00060001 || i32Return == -0x00160209)
			{
				/*
				DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMP_PATH);
				remove_directory(DRMP_PATH);
				*/
				
				DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
				remove(DRMPDB_PATH);
				WISE_SKDRM_Init();
			}

			//drm_error_report(i32Return, content_ip, content_port);
			break;
		}

		i32DecBufCount += i32Count;
		i32DecTpCount += i32NoOfDecTpCount;
		break;
	}

	//	fprintf( stderr, "tpcount[%d]\n", i32DecTpCount );
#if 1
	remove_tsh(pcOutBuf, i32DecTpCount);

	DPRINTSRC("INFO:[%s]", __func__);
	return i32DecTpCount * 184;
	//return i32DecBufCount;
#else
	return i32DecBufCount;
#endif	
}

DC_BOOL WISE_FindingSyncByte( DC_INT32 i32fd, DC_CHAR *pcLocBuf, DC_INT32 i32NoOfByteInSingleRead) {
	//	DC_INT32 	CurOffset 		= 0,
	DC_INT32 i32Count = 0, i32TempCount = 0, i32SyncCount = 0, i32SyncOffset = 0;

	DC_UINT32 CurOffset = 0;

	DPRINTSRC("INFO:[%s]", __func__);
	CurOffset = lseek64(i32fd, 0, SEEK_CUR);
	i32Count = read(i32fd, pcLocBuf, i32NoOfByteInSingleRead);

	if( i32Count == 0 ){
		// it's EOF.
		DPRINTSRC("INFO:[%s]", __func__);
		return TRUE;
	}

	/*Sync byte verification - Move to Sync byte*/
	while ( TRUE ) {
	    while ( pcLocBuf [ i32TempCount ] != WISE_SYNC_BYTE ) {
			++i32TempCount;
			if ( i32TempCount >= i32Count) {
				DPRINTSRC("ERROR:[%s]", __func__);
				return FALSE;
	       	}
	    }

	    i32SyncOffset = i32TempCount;
		while ( pcLocBuf [ i32TempCount ] == WISE_SYNC_BYTE ) {
			i32TempCount += 188;
			i32SyncCount++;
			if ( i32SyncCount >= 7 ) {
				DPRINTSRC("INFO:[%s]", __func__);
				lseek64 ( i32fd, ( CurOffset + i32SyncOffset ), SEEK_SET );
				return TRUE;
			}

			if ( i32TempCount >= i32Count) {
				DPRINTSRC("ERROR:[%s]", __func__);
				return FALSE;
			}
		}
		i32SyncCount = 0;
	}

	DPRINTSRC("INFO:[%s]", __func__);
}

DC_VOID* WISE_SKDRM_Malloc( DC_INT32 size) {
	if (size > 0) return malloc(size);

	return 0;
}

DC_VOID WISE_SKDRM_Free( DC_VOID *ptr) {
	if (ptr) {
		free(ptr);
	} else {
		DEBUG_PRINT ( fprintf ( stderr, "WISE-SKDRM.C> illigal memory address \n" ) );
	}
}

// WSCS SKCASDRM JIMIN ADD
// IPTV DRM_Programming Guide_20090413.doc document required reading.
int WISE_SKDRM_getTrigger(unsigned char commandID, unsigned char cmd1, unsigned char cmd2, char *in, DC_PCHAR *out, unsigned int *outLen) {
	ResponseData responseData;
	JSONBuf jsonBuf;

	char wscs[20];
	char url[128];
	int port;
	int ret = WSCS_OK;

	char apiKey[256] = { 0 };

	int m1, m2, m3, m4, m5, m6;
	char mac_addr[32];

	char *cmdAppendStr = NULL;

	responseData.buffer = NULL;
	responseData.data = NULL;
	jsonBuf.buf = NULL;

	get_wscs_server_address_from_xml(wscs, &port, apiKey);

	sprintf(url, WSCS_DRM_TRIGGER_URL, wscs, port);

    if(get_systemproperty("STBID", jsonBuf.stb_id, 39) != 0 || strlen(jsonBuf.stb_id) == 0) {
        DPRINTSRC( "!!!! ERROR : STBID IS NULL !!!!!!");
        return WSCS_ERROR;
    }

    if(get_systemproperty(PROPERTY__ETHERNET_MAC, jsonBuf.mac_addr, 18) != 0 || strlen(jsonBuf.mac_addr) == 0) {
        DPRINTSRC( "!!!! ERROR : MAC IS NULL !!!!!!");
        return WSCS_ERROR;
    }

    getTimeString(jsonBuf.time_str);

    sscanf(jsonBuf.mac_addr, "%02X:%02X:%02X:%02X:%02X:%02X", &m1, &m2, &m3, &m4, &m5, &m6);
    sprintf(mac_addr, "%x:%x:%x:%x:%x:%x", (unsigned char) m1, (unsigned char) m2, (unsigned char) m3, (unsigned char) m4, (unsigned char) m5, (unsigned char) m6);

	asprintf(&jsonBuf.buf, WSCS_SKCASDRM_TRIGGER_JSON, "IF-SCS-GWSVC-UI5-006", "1.0", jsonBuf.stb_id, mac_addr, cmd1, cmd2, in, jsonBuf.time_str, "post");  // by ksyoon modify UI5.0 SCS 규격수정

	DPRINTSRC("[%s] jsonBuf.buf = %s", __func__, jsonBuf.buf);

	ret = http_post(url, jsonBuf.buf, &responseData, NORMAL_HTTP_RETRY_COUNT, apiKey);

	DPRINTSRC("[%s] ret = %d, %s", __func__, ret, (ret != WSCS_ERROR) ? "SUCCESS":"ERROR");
	DPRINTSRC("[%s] responseData.response_code = %d", __func__, responseData.response_code);
	if (responseData.buffer != NULL && strlen(responseData.buffer) != 0 && ret != WSCS_ERROR && responseData.response_code < 400)
	{
		jsmn_parser pr;
		int res_parse;
		jsmntok_t  json_res[64];
		int i = 0;

		jsmn_init(&pr);

		res_parse = jsmn_parse(&pr, responseData.buffer, responseData.buffer_size, json_res, sizeof(json_res)/sizeof(json_res[0]));

		if(res_parse < 0){
			DPRINTSRC("[%s] IF-SCS-012 responseData.buffer parse error", __func__);
			ret = WSCS_ERROR;
			goto finish;
		}

		if(res_parse < 1 || json_res[0].type != JSMN_OBJECT){
			DPRINTSRC("[%s] IF-SCS-012 responseData.buffer not json response error", __func__);
			ret = WSCS_ERROR;
			goto finish;
		}

		memset(responseData.result, 0, sizeof(char)*128);

		for(i=1; i < res_parse; i++){
			if(jsoneq(responseData.buffer, &json_res[i], "result") == 0){
				strncpy(responseData.result, responseData.buffer+json_res[i+1].start,
					(size_t)(json_res[i+1].end-json_res[i+1].start));
			}
			else if(jsoneq(responseData.buffer, &json_res[i], "command_type1") == 0){
				memset(responseData.cmd1, 0, sizeof(responseData.cmd1));
				strncpy(responseData.cmd1, responseData.buffer+json_res[i+1].start,
					(size_t)(json_res[i+1].end-json_res[i+1].start));
			}
			else if(jsoneq(responseData.buffer, &json_res[i], "command_type2") == 0){
				memset(responseData.cmd2, 0, sizeof(responseData.cmd2));
				strncpy(responseData.cmd2, responseData.buffer+json_res[i+1].start,
					(size_t)(json_res[i+1].end-json_res[i+1].start));
			}
			else if(jsoneq(responseData.buffer, &json_res[i], "data") == 0){
				responseData.data = calloc( (json_res[i+1].end-json_res[i+1].start)+1, sizeof(char));
				strncpy(responseData.data, responseData.buffer+json_res[i+1].start,
					(size_t)(json_res[i+1].end-json_res[i+1].start));
				responseData.data_length = json_res[i+1].end-json_res[i+1].start;
			}
		}

		DPRINTSRC("[%s] parse data ok", __func__);

		if (/*responseData.result == NULL || */strlen(responseData.result) == 0 || strcmp(responseData.result, "0000") != 0)  // by ksyoon modify UI5.0 SCS 규격수정
		{
			DPRINTSRC("[%s]  IF-SCS-012 responseData.result error : %s", __func__, responseData.result);
			ret = WSCS_ERROR;
			goto finish;
		}

	}
	else
	{
		DPRINTSRC("[%s]  IF-SCS-012 responseData.buffer error", __func__);
		ret = WSCS_ERROR;
		goto finish;
	}

	if (responseData.data != NULL && responseData.data_length != 0)
	{
		responseData.data_length += 2;

		*out = (unsigned char *) WISE_SKDRM_MALLOC(responseData.data_length);
		if (NULL == *out)
		{
			DPRINTSRC("[%s]  IF-SCS-012 out buffer malloc fail", __func__);
			ret = WSCS_ERROR;
			goto finish;
		}

		cmdAppendStr = (unsigned char *) WISE_SKDRM_MALLOC(responseData.data_length);
		if (NULL == cmdAppendStr)
		{
			DPRINTSRC("[%s]  IF-SCS-012 cmdAppendStr buffer malloc fail", __func__);
			ret = WSCS_ERROR;
			goto finish;
		}

		cmdAppendStr[0] = strtol(responseData.cmd1, NULL, 16);
		cmdAppendStr[1] = strtol(responseData.cmd2, NULL, 16);
		memcpy(cmdAppendStr + 2, responseData.data, responseData.data_length - 2);

		memcpy(*out, cmdAppendStr, responseData.data_length);
		*outLen = responseData.data_length;
	}
	else
	{
		DPRINTSRC("[%s] IF-SCS-012 responseData.data error", __func__);
		ret = WSCS_ERROR;
	}

	finish:

	if (responseData.buffer)
	{
		free(responseData.buffer);
	}

	if (responseData.data)
	{
		free(responseData.data);
	}

	if (cmdAppendStr != NULL)
	{
		free(cmdAppendStr);
	}

	free(jsonBuf.buf);

	return ret;
}

int RequestMSG(char *hostName, unsigned short port, unsigned char *sendMsg, unsigned int sendMsgLen, unsigned char **recvMsg, unsigned int *recvMsgLen) {
	struct hostent *_he;
	struct sockaddr_in _saddr;
	int _ret = 0;
	int _sock = 0;
	int ndx = 0;
	unsigned int _addr;
	unsigned char *_msgBuf = NULL;
	struct _Header header;		// = {0, };
	int conn_timeout = 3000;
	int sr_timeout = 5000;

	DPRINTSRC("INFO:[%s]", __func__);
	if (NULL == hostName || NULL == recvMsg || NULL == recvMsgLen) {
		DPRINTSRC("ERROR:[%s]", __func__);
		return -1;
	}


	// 2012.02.10 namsj gethostbyname() 이 실패할 경우가 있다.
	// 인터넷에 찾아 보면 대략 실제 호스트가 많을 경우 그럴 때가 있다는 것 같다.
	// gethostbyname2()를 사용하게 되면 잘 얻어 진다.
	//_he = gethostbyname(hostName);
	_he = gethostbyname2(hostName, AF_INET);

	for (ndx = 0; ndx < 2; ndx++)
	{

		if (_he)
		{
			if (_he->h_addr_list[ndx] != NULL)
			{
				_addr = inet_addr(inet_ntoa(*((struct in_addr *) _he->h_addr_list[ndx])));
			}
			else
			{
				return -4;
			}
		}
		else
		{
			_addr = inet_addr(hostName);
			if (_addr == -1)
			{
				return -4;
			}
		}

		_sock = socket(AF_INET, SOCK_STREAM, 0);
		if (0 == _sock)
		{
			DPRINTSRC("ERROR:[%s]", __func__);
			return -3;
		}

		memset(&_saddr, 0, sizeof(_saddr));

		_saddr.sin_addr.s_addr = _addr;
		_saddr.sin_family = AF_INET;
		_saddr.sin_port = htons(port);

		LOG_TIME_STARTV("connectEx sockfd(%d) ScsHostAddr(%s) PORT(%d)", _sock, hostName, port);
		_ret = connectEx(_sock, (struct sockaddr *) &_saddr, sizeof(_saddr), conn_timeout);
		LOG_TIME_END("connectEx sockfd(%d) _ret(%d)", _sock, _ret);
		if (0 != _ret)
		{
			shutdownEx(_sock);
			DPRINTSRC("ERROR:RequestMSG connectEx");
			continue;
		}

		LOG_TIME_START();
		DPRINTMEM(sendMsg, sendMsgLen);
		_ret = sendEx(_sock, sendMsg, sendMsgLen, 0, sr_timeout);
		//_ret = scs_sendEx(_sock, sendMsg, sendMsgLen);
		if (0 > _ret)
		{
			shutdownEx(_sock);
			DPRINTSRC("ERROR:RequestMSG scs_sendEx");
			continue;
		}
		LOG_TIME_END("scs_send sockfd(%d) sendMsgLen(%d) _ret(%d)", _sock, sendMsgLen, _ret);

		memset(&header, 0, sizeof(struct _Header));

		// receive hearder
		LOG_TIME_START();
		_ret = recvEx(_sock, (char*) &header, sizeof(struct _Header), 0, sr_timeout);
		LOG_TIME_END("recvEx sockfd(%d) _ret(%d)", _sock, _ret);
		if (_ret != sizeof(struct _Header))
		{
			shutdownEx(_sock);
			DPRINTSRC("ERROR:RequestMSG recvEx _ret");
			continue;
		}

		DPRINTMEM(&header, sizeof(struct _Header));

		if ( NULL != *recvMsg)
		{
			WISE_SKDRM_Free(*recvMsg);
			*recvMsg = NULL;
		}

		// _msgBuf malloc
		DPRINTSRC("header.nPDUSize(%d)", header.nPDUSize);
		_msgBuf = (unsigned char *) WISE_SKDRM_Malloc(_ret + header.nPDUSize + 1);
		if (NULL == _msgBuf)
		{
			shutdownEx(_sock);
			DPRINTSRC("ERROR:RequestMSG WISE_SKDRM_Malloc(%d)", _ret);
			return -4;
		}

		// receive nPDU
		LOG_TIME_START();
		_ret = recvEx(_sock, (char*) _msgBuf + sizeof(struct _Header), header.nPDUSize, 0, sr_timeout);
		LOG_TIME_END("recvEx sockfd(%d) header.nPDUSize(%d) _ret(%d)", _sock, header.nPDUSize, _ret);
		if (_ret != header.nPDUSize)
		{
			shutdownEx(_sock);
			DPRINTSRC("ERROR:RequestMSG recvEx header");
			continue;
		}

		DPRINTMEM(_msgBuf, sizeof(struct _Header)+ header.nPDUSize);
		shutdownEx(_sock);

		memcpy(_msgBuf, (char*) &header, sizeof(struct _Header));

		*recvMsg = _msgBuf;
		*recvMsgLen = header.nPDUSize;

		DPRINTMEM(*recvMsg, sizeof(struct _Header)+ header.nPDUSize);

		return 0;
	}

	return -4;
}

int requestResponse(char *hostName, unsigned short port, unsigned char *sendMsg, unsigned int sendMsgLen, unsigned char **recvMsg, unsigned int *recvMsgLen) {
	struct hostent *_he;
	struct sockaddr_in _saddr;
	int _ret = 0;
	int _err;
	int _msgLen = 0;
	unsigned int _cur;
	int _retSize;
	int _sock = 0;
	unsigned int _addr;
	unsigned char *_msgBuf = NULL;
	unsigned char *_msgBuf_temp = NULL;
	unsigned char _buf[HDR_SIZE + 1];
#ifdef WIN32
	WSADATA _wsaData;
#endif

	DPRINTSRC("INFO:[%s]", __func__);

	if (NULL == hostName || NULL == recvMsg || NULL == recvMsgLen) {
		DPRINTSRC("ERROR:[%s]", __func__);
		return -1;
	}

	if ( NULL != *recvMsg) {
		WISE_SKDRM_Free(*recvMsg);
	}

#ifdef WIN32
	_ret = WSAStartup(MAKEWORD(2, 2), &_wsaData);
	if(0 != _ret) {
		return -2;
	}
#endif
	_sock = socket(AF_INET, SOCK_STREAM, 0);
	if (0 == _sock) {
		DPRINTSRC("ERROR:[%s]", __func__);
		return -3;
	}


	// 2012.02.10 namsj gethostbyname() 이 실패할 경우가 있다.
	// 인터넷에 찾아 보면 대략 실제 호스트가 많을 경우 그럴 때가 있다는 것 같다.
	// gethostbyname2()를 사용하게 되면 잘 얻어 진다.
	//_he = gethostbyname(hostName);
	_he = gethostbyname2(hostName, AF_INET);
	if (_he)
		_addr = inet_addr(inet_ntoa(*((struct in_addr *) _he->h_addr)));  //(unsigned int)(((struct in_addr *)(_he->h_addr))->s_addr);
		else
		_addr = inet_addr(hostName);

		DPRINTSRC("[%s] INFO: hostName=%s", __func__, hostName);

	memset(&_saddr, 0, sizeof(_saddr));
	_saddr.sin_addr.s_addr = _addr;
	_saddr.sin_family = AF_INET;
	_saddr.sin_port = htons(port);
	_ret = connect((int) _sock, (struct sockaddr *) &_saddr, sizeof(_saddr));
	if (0 != _ret) {
		DPRINTSRC("ERROR:[%s]", __func__);
		_ret = -4;
		goto end;
	}

	for (_cur = 0; _cur < sendMsgLen; _cur += _retSize) {
		_retSize = send(_sock, (const char *) (sendMsg + _cur), sendMsgLen - _cur, 0);
	}

	{
		int _totalSize, _reqSize, _err;

		_retSize = 0;
		_totalSize = HDR_SIZE;
		_err = 1;

		while (_retSize < _totalSize && (_err > 0)) {
			_reqSize = _totalSize - _retSize;

			_err = recv(_sock, &_buf[_retSize], _reqSize, 0);

			if (_err > 0)
				_retSize += _err;
		}

		DPRINTSRC("[%s] INFO: _retSize=%d, _totalSize=%d", __func__, _retSize, _totalSize);

		if (_retSize >= _totalSize) {
			_retSize = 0;
			_msgLen = _totalSize = _buf[17] | (_buf[18] << 8);
			_err = 1;

			_msgBuf_temp = (unsigned char *) realloc(_msgBuf, _totalSize + HDR_SIZE);
			if (NULL == _msgBuf_temp) {
				_ret = -5;
				goto end;
			} else {
				_msgBuf = _msgBuf_temp;
			}
			
			memset(_msgBuf, 0, _totalSize + HDR_SIZE);

			while ((_retSize < _totalSize) && (_err > 0)) {
				_reqSize = _totalSize - _retSize;
				_err = recv(_sock, &_msgBuf[HDR_SIZE + _retSize], _reqSize, 0);

				if (_err > 0)
					_retSize += _err;

			} 
			DPRINTSRC("[%s] INFO: _retSize=%d, _totalSize=%d", __func__, _retSize, _totalSize);

			if (_retSize < _totalSize) {
				_ret = -6;
				goto end;
			}

			memcpy(_msgBuf, _buf, HDR_SIZE);
			_ret = 0;
		} else {
			_ret = -7;
			goto end;
		}
	}

	if (0 < _msgLen) {
		*recvMsg = _msgBuf;
		*recvMsgLen = _msgLen;
		_msgBuf = NULL;
	}

end:
	if(0 != _sock) {
		_err = shutdown(_sock, 2);
		_err = close(_sock);
#ifdef WIN32
		_err = WSACleanup();
#endif
	}

	free(_msgBuf);

	DPRINTSRC("INFO:[%s]", __func__);
	return _ret;
}

DC_INT32 WISE_SKDRM_DeleteUselessRO( void) {
	int ret_v = 0;

	DPRINTSRC("INFO:[%s]", __func__);
	ret_v = DRMP_Command( WISE_SKDRM_DELETE_USELESS_RO, NULL);

	if ( DRMP_NO_ERROR == ret_v) {
		DPRINTSRC("INFO:[%s]", __func__);
		return DRMP_NO_ERROR;
	} else {
		DPRINTSRC("[%s] ERROR: WISE_SKDRM_DELETE_USELESS_RO => %d", __func__, ret_v);
		// DB File이 깨진 경우의 에러 처리
		// DRMPDB.dat를 지우고 DRM Init를 다시 해야 한다.
		if ((ret_v >= ERROR_DBMGR_DAMAGED_DATA) && (ret_v <= ERROR_DBMGR_ABORT)) {
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
			remove(DRMPDB_PATH);
			WISE_SKDRM_Init();
		} else if(ret_v == -0x00030009 || ret_v == 0x00150025 || ret_v == -0x00160108 ||
				ret_v == -0x00160108 || ret_v == -0x00160210 || ret_v == -0x00060001 || ret_v == -0x00160209) {
			/*
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMP_PATH);
			remove_directory(DRMP_PATH);
			*/
			
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
			remove(DRMPDB_PATH);
			WISE_SKDRM_Init();
		}

		//drm_error_report(ret_v, content_ip, content_port);
		return -1;
	}
}

DC_INT32 WISE_SKDRM_DeleteRObyContentID( DC_CHAR *ContentID) {
	int ret_v = 0;
	DRMP_COMMAND_PARAM stCommandParam;

	DPRINTSRC("INFO:[%s]", __func__);
	memset(&stCommandParam, 0, sizeof(DRMP_COMMAND_PARAM));

	stCommandParam.param1 = ContentID;

	ret_v = DRMP_Command( WISE_SKDRM_DELETE_RO_BYCID, &stCommandParam);

	if ( DRMP_NO_ERROR == ret_v) {
		DPRINTSRC("INFO:[%s]", __func__);
		return DRMP_NO_ERROR;
	} else {
		DPRINTSRC("[%s] ERROR: WISE_SKDRM_DELETE_RO_BYCID => %d", __func__, ret_v);
		// DB File�� ���� ����� ���� ó��
		// DRMPDB.dat�� ����� DRM Init�� �ٽ� �ؾ� �Ѵ�.
		if ((ret_v >= ERROR_DBMGR_DAMAGED_DATA) && (ret_v <= ERROR_DBMGR_ABORT)) {
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
			remove(DRMPDB_PATH);
			WISE_SKDRM_Init();
		} else if(ret_v == -0x00030009 || ret_v == 0x00150025 || ret_v == -0x00160108 ||
				ret_v == -0x00160108 || ret_v == -0x00160210 || ret_v == -0x00060001 || ret_v == -0x00160209) {
			/*
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMP_PATH);
			remove_directory(DRMP_PATH);
			*/
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
			remove(DRMPDB_PATH);
			WISE_SKDRM_Init();
		}

		//drm_error_report(ret_v, content_ip, content_port);
		return -1;
	}
}

DC_INT32 WISE_SKDRM_CleanupROStorage( void) {
	int ret_v = 0;

	DPRINTSRC("INFO:[%s]", __func__);
	ret_v = DRMP_Command( WISE_SKDRM_CLEANUP_RO_STORAGE, NULL);
	if ( DRMP_NO_ERROR == ret_v) {
		DPRINTSRC("INFO:[%s]", __func__);
		return DRMP_NO_ERROR;
	} else {
		DPRINTSRC("[%s] ERROR: WISE_SKDRM_CLEANUP_RO_STORAGE => %d", __func__, ret_v);
		// DB File이 깨진 경우의 에러 처리
		// DRMPDB.dat를 지우고 DRM Init를 다시 해야 한다.
		if ((ret_v >= ERROR_DBMGR_DAMAGED_DATA) && (ret_v <= ERROR_DBMGR_ABORT)) {
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
			remove(DRMPDB_PATH);
			WISE_SKDRM_Init();
		} else if(ret_v == -0x00030009 || ret_v == 0x00150025 || ret_v == -0x00160108 ||
				ret_v == -0x00160108 || ret_v == -0x00160210 || ret_v == -0x00060001 || ret_v == -0x00160209) {
			/*
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMP_PATH);
			remove_directory(DRMP_PATH);
			*/
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
			remove(DRMPDB_PATH);
			WISE_SKDRM_Init();
		}

		//drm_error_report(ret_v, content_ip, content_port);
		return -1;
	}
}

DC_INT32 WISE_ZOO_SKDRM_CreatContentHandler(DC_CHAR * HeaderData, DC_INT32 i32LegInLittleEndian, DC_INT32 i32WindowID, DC_CHAR* ContentID) {
	DRMP_COMMAND_PARAM stCommandParam;
	DRMP_CHD *pDRMHandler = NULL;
	DC_INT32 i32Return = 0;
	DC_CHAR *pcSKDRMHeaderData = NULL;

	memset(&stCommandParam, 0, sizeof(DRMP_COMMAND_PARAM));
	if ( NULL != gpDRMContentHandler[i32WindowID]) {
		i32Return = WISE_SKDRM_DeleteContentHandler(i32WindowID);
		if ( DRMP_NO_ERROR != i32Return) {
			DPRINTSRC("[%s] ERROR: %d", __func__, i32Return);
			return -1;
		}
	}

	gi32SyncFlag[i32WindowID] = 0;
	/*File Formate
	 *  ---------------------------------------------------------------------------------
	 *  |Header tag (4 byte) | Header length field (4 byte) | Header data | Data.........               |
	 *  ---------------------------------------------------------------------------------
	 */

	pcSKDRMHeaderData = WISE_SKDRM_MALLOC(i32LegInLittleEndian);
	if ( NULL == pcSKDRMHeaderData) {
		DPRINTSRC("[%s] ERROR:pcSKDRMHeaderData==NULL", __func__);
		return -1;
	}

	memset(pcSKDRMHeaderData, 0, i32LegInLittleEndian);
	memcpy(pcSKDRMHeaderData, HeaderData, i32LegInLittleEndian);

	memset(&stCommandParam, 0, sizeof(DRMP_COMMAND_PARAM));
	//DC_CHAR cid[1024] = "{8A3882A7-AE75-11DF-82C2-672F3C754514}";
	stCommandParam.param1 = (DC_PVOID) pcSKDRMHeaderData;
	stCommandParam.param2 = (DC_PVOID) i32LegInLittleEndian;
	stCommandParam.param3 = (DC_PVOID) &pDRMHandler;
	stCommandParam.param4 = (DC_PVOID) ContentID; //TODO
	//stCommandParam .param4 = (DC_PVOID) cid ;//TODO

		LOGSRC("!!!!!!!!!!!!!!!!!!!!!!!! %d ", __LINE__);
	if ( DRMP_NO_ERROR == (i32Return = DRMP_Command( WISE_SKDRM_CREATE_CONTENT_HANDLER, &stCommandParam))) {
		//yeongjin 11.04.11 RO file
		int roreturn;

		 	LOGSRC("!!!!!!!!!!!!!!!!!!!!!!!! %d ", __LINE__);
		gpDRMContentHandler[i32WindowID] = pDRMHandler;
		WISE_SKDRM_Free(pcSKDRMHeaderData);
			LOGSRC("!!!!!!!!!!!!!!!!!!!!!!!! %d ", __LINE__);
		if ( DRMP_NO_ERROR != (roreturn = WISE_SKDRM_IsROExist(pDRMHandler))) {
			DPRINTSRC("[%s] ERROR: WISE_SKDRM_CREATE_CONTENT_HANDLER => %d", __func__, roreturn);
			return roreturn;
		}

		DPRINTSRC("[%s] INFO: DRMP_NO_ERROR", __func__);
		return DRMP_NO_ERROR;
	} else {
		WISE_SKDRM_Free(pcSKDRMHeaderData);
			LOGSRC("!!!!!!!!!!!!!!!!!!!!!!!! %d ", __LINE__);
		DPRINTSRC("[%s] ERROR:WISE_SKDRM_CREATE_CONTENT_HANDLER i32Return=%d", __func__, i32Return);
		// DB File이 깨진 경우의 에러 처리
		// DRMPDB.dat를 지우고 DRM Init를 다시 해야 한다.
			LOGSRC("!!!!!!!!!!!!!!!!!!!!!!!! %d ", __LINE__);
		if ((i32Return >= ERROR_DBMGR_DAMAGED_DATA) && (i32Return <= ERROR_DBMGR_ABORT)) {
				LOGSRC("!!!!!!!!!!!!!!!!!!!!!!!! %d ", __LINE__);
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
			remove(DRMPDB_PATH);
			WISE_SKDRM_Init();
		} else if(i32Return == -0x00030009 || i32Return == 0x00150025 || i32Return == -0x00160108 ||
					i32Return == -0x00160108 || i32Return == -0x00160210 || i32Return == -0x00060001 || i32Return == -0x00160209) {
			/*
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMP_PATH);
			remove_directory(DRMP_PATH);
			*/
			
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
			remove(DRMPDB_PATH);
			WISE_SKDRM_Init();
		}

		//drm_error_report(i32Return, content_ip, content_port);
		return i32Return;
	}
}

DC_INT32 WISE_ZOO_SKDRM_DecryptTS( DC_CHAR *pBuf, DC_CHAR *pOutBuf, DC_INT32 i32BufLen, DC_INT32 i32WindowID) {
	DRMP_COMMAND_PARAM stCommandParam;
	DC_INT32 i32Return = 0, i32NoOfDecTpCount = 0;

	//DPRINTSRC("INFO:[%s]", __func__);
	memset(&stCommandParam, 0, sizeof(DRMP_COMMAND_PARAM));

	/* Read the encrypted data from file and decrypte the data then stored in outbuffer */
	stCommandParam.param1 = gpDRMContentHandler[i32WindowID];
	stCommandParam.param2 = pBuf;
	stCommandParam.param3 = pOutBuf;
	stCommandParam.param4 = (DC_PVOID) (i32BufLen / WISE_SKDRM_TP_LEN);
	stCommandParam.param5 = (DC_PVOID) &i32NoOfDecTpCount;

    if ( DRMP_NO_ERROR != ( i32Return = DRMP_Command ( WISE_SKDRM_DECRYPT_TS, &stCommandParam ) ) ) {
    	DPRINTSRC("[%s] ERROR: WISE_SKDRM_DECRYPT_TS => %d ", __func__, i32Return);
		// DB File이 깨진 경우의 에러 처리
		// DRMPDB.dat를 지우고 DRM Init를 다시 해야 한다.
		if ((i32Return >= ERROR_DBMGR_DAMAGED_DATA) && (i32Return <= ERROR_DBMGR_ABORT))
		{
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
			remove(DRMPDB_PATH);
			WISE_SKDRM_Init();
		} else if(i32Return == -0x00030009 || i32Return == 0x00150025 || i32Return == -0x00160108 ||
					i32Return == -0x00160108 || i32Return == -0x00160210 || i32Return == -0x00060001 || i32Return == -0x00160209) {
			/*
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMP_PATH);
			remove_directory(DRMP_PATH);
			*/
			
			DPRINTSRC("[%s] ERROR: remove %s, SKDRM Reinit", __func__, DRMPDB_PATH);
			remove(DRMPDB_PATH);
			WISE_SKDRM_Init();
		}

		//drm_error_report(i32Return, content_ip, content_port);
		return -1;
	}

	//DPRINTSRC("INFO:[%s]", __func__);
	return i32NoOfDecTpCount * WISE_SKDRM_TP_LEN;
}

#ifdef __cplusplus
}
#endif

