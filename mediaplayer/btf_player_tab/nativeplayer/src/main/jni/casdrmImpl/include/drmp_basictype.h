/*
-----------------------------------------------------------------------------
******************************* C HEADER FILE *******************************
-----------------------------------------------------------------------------
**                                                                         **
** PROJECT	: HanaTV DRM - DRM Client                                      **
** filename	: drm_basictype.h                                              **
** version	: 1                                                            **
** date		: 2007-01-11                                                   **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2007, DigiCAPS Inc.    All rights reserved.               **
**                                                                         **
*****************************************************************************
VERSION HISTORY:
----------------
Version	 Date        	Revised by	Description
1        2007-01-11  				Initial version.
*****************************************************************************
*/
#ifndef _DRMP_BASICTYPE_INCLUDED
#define _DRMP_BASICTYPE_INCLUDED

#include <stdint.h>
/****************************************************************************/
/**                                                                        **/
/** DEFINITIONS AND MACROS                                                 **/
/**                                                                        **/
/****************************************************************************/
#ifndef FALSE
#define FALSE		0
#endif

#ifndef	TRUE
#define	TRUE		1
#endif

#define	DC_FALSE	0
#define	DC_TRUE		1

#define DC_SWAP16(num) ((DC_UINT16)((((num) & 0xFF00) >> 8) | \
							 (((num) & 0x00FF) << 8)))

#define DC_SWAP24(num) ((DC_UINT32)((((num) & 0x00FF0000) >> 16) | \
							 ((num) & 0x0000FF00)| \
							 (((num) & 0x000000FF) << 16)))

#define DC_SWAP32(num) ((DC_UINT32)((((num) & 0xFF000000) >> 24) | \
							 (((num) & 0x00FF0000) >> 8) | \
							 (((num) & 0x0000FF00) << 8) | \
							 (((num) & 0x000000FF) << 24)))

#define DC_IS_ALPHA(x) (DC_IS_LOWALPHA(x) || DC_IS_UPALPHA(x))
#define DC_IS_LOWALPHA(x) (((x) >= 'a') && ((x) <= 'z'))
#define DC_IS_UPALPHA(x) (((x) >= 'A') && ((x) <= 'Z'))
#define DC_IS_DIGIT(x) (((x) >= '0') && ((x) <= '9'))
#define DC_IS_ALPHANUM(x) (DC_IS_ALPHA(x) || DC_IS_DIGIT(x))

/****************************************************************************/
/**                                                                        **/
/** TYPEDEFS AND STRUCTURES                                                **/
/**                                                                        **/
/****************************************************************************/
#ifndef		_DRM_BASIC_TYPE_
#define 	_DRM_BASIC_TYPE_

#ifndef NULL
#define NULL ((void*)0)
#endif

#ifndef DC_VOID
#define DC_VOID void
#endif

#ifndef DC_PVOID
#define DC_PVOID void*
#endif

#ifndef DC_BIT1
#define DC_BIT1 unsigned char
#endif

#ifndef DC_BIT7
#define DC_BIT7 unsigned char
#endif


#ifndef DC_BYTE
#define DC_BYTE unsigned char
#endif
/*
typedef		unsigned char	DC_BYTE;
*/
typedef		DC_BYTE*		DC_PBYTE;
typedef		DC_BYTE			DC_UCHAR;

typedef		DC_BYTE			DC_BOOL;

typedef		DC_BYTE			DC_UINT8;
#ifndef DC_UINT16
#define DC_UINT16 unsigned short
#endif
typedef		DC_UINT16*		DC_PUINT16;


#ifndef DC_UINT32
#define DC_UINT32 unsigned long
#endif
typedef		DC_UINT32*		DC_PUINT32;

#ifndef DC_ULONG
#define DC_ULONG DC_UINT32
#endif

//typedef		struct
//{
//	DC_UINT32 h; /* 64bit 데이타 중 상위 32bit 값을 가짐 */
//	DC_UINT32 l; /* 64bit 데이타 중 하위 32bit 값을 가짐 */
//} DC_UINT64;

#ifndef DC_INT8
#define DC_INT8 signed char
#endif

typedef		DC_INT8*			DC_PINT8;
#ifndef DC_INT16
#define DC_INT16 signed short
#endif

#ifndef DC_INT32
#define DC_INT32 signed int
#endif

typedef		DC_INT32*			DC_PINT32;

#ifndef DC_INT64
#define DC_INT64 int64_t
#endif

#ifndef DC_UINT64
#define DC_UINT64 uint64_t
#endif


#ifndef DC_LONG
#define DC_LONG signed long
#endif

typedef		DC_LONG*			DC_PLONG;

#ifndef DC_CHAR
#define DC_CHAR char
#endif

typedef		DC_CHAR*			DC_PCHAR;

#ifndef DC_DOUBLE
#define DC_DOUBLE double
#endif

typedef struct
{
	DC_PVOID	value;
	DC_PVOID	next;
} DC_ValueList; //ValueList

#endif		/* _DRM_BASIC_TYPE_ */

#endif		/* _DRM_BASICTYPE_INCLUDED */
/****************************************************************************/
/**                                                                        **/
/** EOF                                                                    **/
/**                                                                        **/
/****************************************************************************/

