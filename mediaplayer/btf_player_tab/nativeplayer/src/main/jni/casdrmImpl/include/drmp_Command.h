/*
-----------------------------------------------------------------------------
******************************* C SOURCE FILE *******************************
-----------------------------------------------------------------------------
**                                                                         **
** PROJECT	: HanaTV DRM - DRM Client                                  **
** FILENAME  	: drmp_Command.c                                           **
** VERSION   	: 1                                                        **
** DATE      	: 2008-08-14                                               **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2008, DigiCAPS Inc.    All rights reserved.	           **
**                                                                         **
*****************************************************************************
VERSION HISTORY:
----------------
Version	 Date        	Revised by	Description
         2008-08-14
*****************************************************************************
*/
#ifndef _DRMP_COMMAND
#define _DRMP_COMMAND

#ifdef __cplusplus
extern "C" {
#endif
/***************************************************************************/
/**                                                             	  **/
/** MODULES USED                                                          **/
/**                                                                       **/
/***************************************************************************/
#include "drmp_basictype.h"

/***************************************************************************/
/**                                                                       **/
/** DEFINITIONS AND MACROS                                                **/
/**                                                                       **/
/***************************************************************************/
/***************************************************************************/
/** DEFINITIONS AND MACROS : Error                                        **/
/***************************************************************************/
/***************************************************************************/
/** DEFINITIONS AND MACROS : ETC                                          **/
/***************************************************************************/

typedef enum
{
	DRMP_CMD_InitDRMLib,
	DRMP_CMD_DestroyDRMLib,
	DRMP_CMD_CreateContentHandler,
	DRMP_CMD_GetContentInfoAll,
	DRMP_CMD_DecryptTS,
	DRMP_CMD_DeleteContentHandler,
	DRMP_CMD_CleanupContentInfo,
	DRMP_CMD_CheckRO,
	DRMP_CMD_GetExpireDate,
	DRMP_CMD_GetROList,
	DRMP_CMD_GetROInfo,
	DRMP_CMD_GetROInfoByHandler,
	DRMP_CMD_DeleteRO,
	DRMP_CMD_DeleteROByHandler,
	DRMP_CMD_CleanupROStorage,
	DRMP_CMD_CleanupROInfo,
	DRMP_CMD_GetAvailableRights,
	DRMP_CMD_GetAvailableRightsByHandler,
	DRMP_CMD_CleanupRightsInfo,
	DRMP_CMD_ProcessROAP,
	DRMP_CMD_GetPolicyVersion,
	DRMP_CMD_SetPolicy,	
	DRMP_CMD_DeleteUselessRO, 
	DRMP_CMD_DeleteROByCid
} DRMP_COMMAND_TYPE;

typedef struct
{
	DC_PVOID	param1;
	DC_PVOID	param2;
	DC_PVOID	param3;
	DC_PVOID	param4;
	DC_PVOID	param5;
} DRMP_COMMAND_PARAM;
/***************************************************************************/
/**                                                                       **/
/** TYPEDEFS AND STRUCTURES                                               **/
/**                                                                       **/
/***************************************************************************/
/***************************************************************************/
/**                                                                       **/
/** EXPORTED VARIABLES                                                    **/
/**                                                                       **/
/***************************************************************************/
DC_INT32 DRMP_Command(DC_INT32 op, DRMP_COMMAND_PARAM *param);
/***************************************************************************/
/**                                                                       **/
/** EXPORTED FUNCTIONS                                                    **/
/**                                                                       **/
/***************************************************************************/

/***************************************************************************/
/**                                                                       **/
/** EOF                                                                   **/
/**                                                                       **/
/***************************************************************************/
#ifdef __cplusplus
}
#endif

#endif
