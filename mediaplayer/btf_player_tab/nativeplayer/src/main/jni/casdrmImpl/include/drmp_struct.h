/*
-----------------------------------------------------------------------------
******************************* C HEADER FILE *******************************
-----------------------------------------------------------------------------
**                                                                         **
** PROJECT		: SKB IPTV DRM - DRM Client                                **
** FILENAME		: drmp_struct.h		                                       **
** VERSION		: 1                                                        **
** DATE			: 2009-09-03                                               **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2009, DigiCAPS Inc.    All rights reserved.               **
**                                                                         **
*****************************************************************************
VERSION HISTORY:
----------------
Version	 Date        	Revised by	Description
1        2009-09-03  				Initial version.
*****************************************************************************
*/
#ifndef _DRMP_STRUCT_INCLUDED
#define _DRMP_STRUCT_INCLUDED

/***************************************************************************/
/**                                                                		  **/
/** MODULES USED                                                          **/
/**                                                                       **/
/***************************************************************************/
#include "drmp_basictype.h"

/***************************************************************************/
/**                                                                       **/
/** DEFINITIONS AND MACROS                                                **/
/**                                                                       **/
/***************************************************************************/



/***************************************************************************/
/**                                                                       **/
/** TYPEDEFS AND STRUCTURES                                               **/
/**                                                                       **/
/***************************************************************************/

typedef enum
{
	DT_NOPROXY		= 0, 
	DT_PROXY		= 1
} DRMP_DevType;

typedef enum
{
	RO_DEVICE		= 0, 
	RO_CHILD		= 1,
	RO_PARENT		= 2
} DRMP_ROType;

typedef struct
{
	DC_PCHAR	ContentID;
} DRMP_CON_INFO;

typedef DC_PVOID			DRMP_CHD;

typedef enum
{
	P_NONE = 0, P_PLAY, P_DISPLAY, P_EXECUTE, P_PRINT, P_EXPORT
} DRMP_PERMTYPE;

typedef struct
{
	//"oma-dd:timed-count"
	DC_LONG		count;
	DC_INT32	timer;
} DRMP_TIMEDCOUNT;

typedef struct
{
	//"o-dd:datetime"
	DC_LONG		start;	//CCYY-MM-DDThh:mm:ssZ의 형태로 시간 값을 가짐, 초 값이 들어감
	DC_LONG		end;	//CCYY-MM-DDThh:mm:ssZ의 형태로 시간 값을 가짐, 초 값이 들어감
} DRMP_DATETIME;

typedef struct
{
	//"o-ex:context"
	DC_PCHAR		version;	//0 or 1개 존재
	DC_ValueList	*uid;
	//DC_PVOID		next;
} DRMP_INDIVIDUAL;

typedef struct
{
	//"o-ex:context"
	DC_PCHAR		version;	//0 or 1개 존재
	DC_ValueList	*uid;
	//DC_PVOID		next;
} DRMP_SYSTEM;

typedef struct
{
	DC_PCHAR	RightsObjectID;
	DC_PCHAR	RightsIssuerID;
	DC_BOOL		Stateful;
	DC_BOOL		DomainRO;
} DRMP_RO_INFO;


typedef struct
{
	DC_BOOL					Unlimited;		// 무제한인지 0, 1
	DC_LONG					Count;			// 사용 가능 횟수 %d
	//DC_LONG					Interval;
	DC_PCHAR				Interval;		// (현재 상황에서 재생할 수 있는)yyyy-mm-ddTtt:mm:ssZ
	DC_LONG					Accumulated;	// 초 단위의 누적 사용 시간 %d
	/* DateTime */
	DC_PCHAR				StartDateTime;	// yyyy-mm-ddTtt:mm:ssZ
	DC_PCHAR				EndDateTime;	// yyyy-mm-ddTtt:mm:ssZ

	/* Timed Count */
	DC_LONG					TimedCount;		// Timer의 횟수 %d
	DC_INT32				Timer;			// Timer의 초 단위의 시간 %d

	DRMP_INDIVIDUAL			*Individual;
	DRMP_SYSTEM				*System;
} DRMP_CONSTRAINT_INFO;

typedef struct
{
	DC_PCHAR				RightsObjectID;
	DC_PCHAR				ContentID;
	DRMP_PERMTYPE			Permission;
	DRMP_CONSTRAINT_INFO	*Constraint;
} DRMP_RIGHTS_INFO;

#endif

/***************************************************************************/
/**                                                                       **/
/** EOF                                                                   **/
/**                                                                       **/
/***************************************************************************/


