/*
-----------------------------------------------------------------------------
******************************* C HEADER FILE *******************************
-----------------------------------------------------------------------------
**                                                                         **
** PROJECT		: CAS Project for Device                                   **
** FILENAME		: mcas_types.h                                             **
** VERSION		: 1.0.1                                                    **
** DATE			: 2006-04-12                                               **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2005, DigiCAPS Inc.    All rights reserved.               **
**                                                                         **
****************************************************************************/
#ifndef _MCAS_TYPES_H_
#define _MCAS_TYPES_H_

#ifdef __cplusplus
extern "C" {
#endif 

#ifdef WIN32
#include <windows.h>
#endif

/***************************************************************************/
/**                                                                       **/
/** DEFINITIONS AND MACROS (Common Data Type Definition)                  **/
/**                                                                       **/
/***************************************************************************/
#ifndef NO_ERROR
#define NO_ERROR	(0)
#endif

#ifndef VOID
#define VOID   void
#endif

#ifndef INT32
#define INT32  int
#endif

#ifndef INT16
#define INT16  short
#endif

#ifndef CHAR
#define CHAR   char
#endif

#ifdef WIN32

#ifndef INT64
#define INT64 __int64
#endif  

#ifndef UINT64
#define UINT64 unsigned __int64
#endif  

#else //ifndef WIN32
#ifndef INT64
#define INT64 long long
#endif  

#ifndef UINT64
#define UINT64 unsigned long long
#endif  

#endif //ifndef __WIN32

#ifndef UINT32
#define UINT32 unsigned int
#endif

#ifndef UINT16
#define UINT16 unsigned short
#endif

#ifndef WORD
#define WORD   unsigned short
#endif

#ifndef DWORD
#define DWORD unsigned long
#endif
#ifndef BYTE
#define BYTE   unsigned char
#endif

#ifndef DCBYTE
#define DCBYTE   unsigned char
#endif

#ifndef PBYTE
#define PBYTE  unsigned char *
#endif

#ifndef PVOID
#define PVOID  VOID *
#endif

#ifndef TRUE
#define TRUE   1
#endif

#ifndef FALSE
#define FALSE  0
#endif

#ifndef NULL
#define NULL   0
#endif

#ifndef int32
#define int32 int
#endif

#ifndef int16
#define int16  short
#endif


#ifdef WIN32

#ifndef int64
#define int64 __int64
#endif  

#ifndef uint64
#define uint64 unsigned __int64
#endif  

#else //ifndef WIN32
#ifndef int64
#define int64 long long
#endif  

#ifndef uint64
#define uint64 unsigned long long
#endif  

#endif //ifndef __WIN32

#ifndef uint32
#define uint32 unsigned int
#endif

#ifndef uint16
#define uint16 unsigned short
#endif

#ifndef byte
#define byte   unsigned char
#endif

#ifndef pbyte
#define pbyte  unsigned char *
#endif

#ifndef pvoid
#define pvoid  void *
#endif

#ifndef null
#define null   0
#endif

#ifndef WIN32
#ifndef BOOL
#define BOOL	unsigned char
#endif
#ifndef bool 
#define bool unsigned char
#endif
#endif

/*************************************************************
* Task 간의 통신을 위한 Message Struct
*************************************************************/
typedef struct
{
    uint16 type;
    uint16 length;
    byte *msg;
    byte *msg2;
} MCAS_MESSAGE;

typedef enum
{
	// 파일을 읽기 위해 연다. 파일이 없다면 에러
	DCPLI_EFILEOPEN_EXIST_READONLY = 0,		
	// 파일을 쓰기 위해 연다. 파일이 없다면 에러	
	DCPLI_EFILEOPEN_EXIST_WRITEONLY,
	// 파일을 읽고 쓰기 위해 연다. 파일이 없다면 에러
	DCPLI_EFILEOPEN_EXIST_READWRITE,
	// 파일을 비우고 (크기 0) 쓰기 위해 연다. 파일이 없다면 에러
	DCPLI_EFILEOPEN_TRUNC_WRITEONLY,
	// 파일을 비우고 읽고 쓰기 위해 연다. 파일이 없다면 에러
	DCPLI_EFILEOPEN_TRUNC_READWRITE,
	// 파일을 처음 생성하고 쓰기 위함. 파일이 이미 존재한다면 에러
	DCPLI_EFILEOPEN_NEW_WRITEONLY,
	// 파일을 처음 생성하고 읽고 쓰기 위함. 파일이 이미 존재한다면 에러
	DCPLI_EFILEOPEN_NEW_READWRITE
} EFILEOPEN;


#ifdef WIN32
//#define FILE_HANDLE HANDLE
#define FILE_HANDLE int32
#else
#define FILE_HANDLE int32
#endif

#ifdef __cplusplus
}
#endif

#endif //define _MCAS_TYPES_H_