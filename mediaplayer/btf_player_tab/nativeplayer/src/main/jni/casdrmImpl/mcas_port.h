/*
 -----------------------------------------------------------------------------
 ******************************* C HEADER FILE *******************************
 -----------------------------------------------------------------------------
 **                                                                         **
 ** PROJECT		: CAS Project for Device                                   **
 ** FILENAME		: mcas_port                                                **
 ** VERSION		: 1.1.0                                                    **
 ** DATE			: 2006-04-12                                               **
 **                                                                         **
 *****************************************************************************
 **                                                                         **
 ** Copyright (c) 2005, DigiCAPS Inc.    All rights reserved.               **
 **                                                                         **
 *****************************************************************************/

#ifndef _MCAS_PORT_H_
#define _MCAS_PORT_H_

#ifdef __cplusplus
extern "C" {
#endif 

/***************************************************************************/
/**                                                                		  **/
/** MODULES USED                                                          **/
/**                                                                       **/
/***************************************************************************/
#include "mcas_iptv.h"


/***********************************************************************
 * Memory Management
 ***********************************************************************/
/**
 *	@fn 		*DHmalloc
 *	@brief		malloc()
 */
VOID *DHmalloc(UINT32 size);

/**
 *	@fn 		DHfree
 *	@brief		free()
 */
VOID DHfree(VOID* ptr);

/*************************************************************************
 * File IO
 ***********************************************************************/
/**
 *	@fn 		DHFileOpen
 *	@brief		MCAS를 위한 데이터 파일을 Open 하여 File Descriptor 를 가져옴 ( fopen )
 *	@param[in]	szFileName		Data file Name (절대경로)
 *	@param[in]	eFileOpenMode	File Open Option (mcas_types 참조)
 */
FILE_HANDLE DHFileOpen(char const *szFileName, EFILEOPEN eFileOpenMode);

/**
 *	@fn 		DHFileClose
 *	@brief		해당 File Descriptor Close 
 *	@param[in]	handle			File Handle
 */
INT32 DHFileClose(FILE_HANDLE handle);

/**
 *	@fn 		DHFileRemove
 *	@brief		해당 데이터 파일을 삭제한다
 *	@param[in]	szFileName		Data file Name (절대경로)
 */
INT32 DHFileRemove(char const *szFileName);

/**
 *	@fn 		DHFileRead
 *	@brief		해당 데이터 파일의 Data을 Read
 *	@param[in]	handle		File Handle
 *	@param[out]	pbBuffer	데이터를 읽기 위해 할당된 데이터 버퍼의 포인터
 *	@param[in]	len 		읽을 데이터 버퍼의 길이
 */
INT32 DHFileRead(FILE_HANDLE handle, BYTE* pbBuffer, INT32 len);

/**
 *	@fn 		DHFileWrite
 *	@brief		해당 데이터 파일의 Data을 Write
 *	@param[in]	handle		File Handle
 *	@param[in]	pbBuffer	파일에 쓰기 위한 데이터 버퍼의 포인터
 *	@param[in]	len 		쓰기 위한 데이터 버퍼의 길이
 */
INT32 DHFileWrite(FILE_HANDLE handle, BYTE* pbBuffer, INT32 len);

/***********************************************************************
 * Task Management
 ***********************************************************************/
/**
 *	@fn 		DHTaskCreate
 *	@brief		Create Task that have priority
 *	@param[in]	* MyTask	Function pointer
 *	@param[in]	iPriority	Task priority
 *	@param[in]	iStackSize	Stack size
 *	@return 				Task ID
 */
OS_DRV_TASK_ID DHTaskCreate(VOID(*MyTask)(VOID * arg), INT32 iPriority, INT32 iStackSize);

/**
 *	@fn 		DHTaskClose
 *	@brief		Terminate Task
 *	@param[in]	task_id		 Task ID
 */
VOID DHTaskClose( OS_DRV_TASK_ID task_id );

/***********************************************************************
 * Inter-Process Communication
 ***********************************************************************/
/**
 *	@fn 		DHMessageSend
 *	@brief		Send MCAS Message to other Task
 *	@param[in]	TaskId 	Task ID
 *	@param[in]	pstMsg 		SAC message
 *	@return 				True/False
 */
INT32 DHMessageSend(OS_DRV_TASK_ID TaskId, MCAS_MESSAGE *pstMsg);

/**
 *	@fn 		*DHMessageRecv
 *	@brief		Receive MCAS Message.
 *				Blocking function, and return when had message.
* @param[in] task_id TaskId
* @return MCAS_MESSAGE
*/
MCAS_MESSAGE* DHMessageRecv(OS_DRV_TASK_ID TaskId);

/***********************************************************************
 * Semaphore Management
 ***********************************************************************/
/**
 *	@fn 		DHSemaphoreCreate
 *	@brief		Create semaphore that have counter
 *	@param[in]	ulInitialCount	Initial value of semaphore counter 
 *	@return 					Semaphore ID (OS_DRV_SEMAPHORE)
 */
OS_DRV_SEMAPHORE DHSemaphoreCreate(UINT32 ulInitialCount);

/**
 *	@fn 		DHSemaphoreClose
 *	@brief		Close semaphore
 *	@param[in]	Semaphore	Semaphore ID
 */
VOID DHSemaphoreClose(OS_DRV_SEMAPHORE Semaphore);

/**
 *	@fn 		DHSemaphoreSignal
 *	@brief		Increase semaphore counter (+1)
 *	@param[in]	Semaphore	Semaphore ID
 */
VOID DHSemaphoreSignal(OS_DRV_SEMAPHORE Semaphore);

/**
 *	@fn 		DHSemaphoreWait
 *	@brief		Decrease semaphore counter (-1)
 *				While counter is 0, function becomes blocking
 *	@param[in]	Semaphore	Semaphore ID
 */
VOID DHSemaphoreWait(OS_DRV_SEMAPHORE Semaphore);

/***********************************************************************
 * Smart Card Management (OPTIONAL)
 ***********************************************************************/

typedef INT32(* SC_ATR_Notify) (BYTE *bCardNumber,BYTE bStatus, BYTE *pATR, BYTE bProtocol);

/**
 *	@fn 		SC_DRV_Init
 *	@brief		CAS에 Smart Card ATR Notification 콜백함수 등록
 *	@param[in]	SC_ATR_Notify	Notification 함수 포인터
 */
BYTE SC_DRV_Init( SC_ATR_Notify notification );

/**
 *	@fn 		SC_DRV_PowerDown
 *	@brief		Smart Card Destroy
 */
VOID SC_DRV_PowerDown( VOID );

/**
 *	@fn 		SC_DRV_ResetCard
 *	@brief		Smart Card Reset
 *	@param[in]	bCardNumber		Smart Card ID
 */
VOID SC_DRV_ResetCard( BYTE bCardNumber );

/**
 *	@fn 		SC_DRV_DataSend
 *	@brief		Send Request To Smart Card
 *	@param[in]		bCardNumber		Smart Card ID
 *	@param[in]		bLength			요청 Msg Length (Header + Data)
 *	@param[in]		pstHeader		Command Header
 *	@param[in]		pabTxData		Request Data
 *	@param[out]		pbSW1			SW1
 *	@param[out]		pbSW2			SW2
 */
BYTE SC_DRV_DataSend( BYTE bCardNumber, UINT16 bLength, BYTE *pstHeader, BYTE *pabTxData, BYTE *pbSW1, BYTE *pbSW2 );

/**
 *	@fn 		SC_DRV_DataRecv
 *	@brief		Receive Response From Smart Card
 *	@param[in]		bCardNumber		Smart Card ID
 *	@param[in]		bLength			Command Header Length
 *	@param[in]		pstHeader		Command Header
 *	@param[out]		pabTxData		Response Data
 *	@param[out]		pbSW1			SW1
 *	@param[out]		pbSW2			SW2
 */
BYTE SC_DRV_DataRecv( BYTE bCardNumber, BYTE bLength, BYTE *pstHeader, BYTE *pabRxData, BYTE *pbSW1, BYTE *pbSW2 );

/***********************************************************************
 * System Information Function
 ***********************************************************************/
/**
 *	@fn 		SXPLI_GetStoragePath
 *	@brief		Get Storage Root Path
 *	@param[out]	path		directory path
 *	@param[out]	path_len	length of <path>
 *	@param[in]	buf_len 	Usable maximum size of <path> buffer
 *	@return 				TRUE/FALSE
 */
BOOL SXPLI_GetStoragePath( CHAR *path, UINT32 *path_len, UINT32 buf_len );

/***********************************************************************
 * Debug Function
 ***********************************************************************/
VOID DBGMSG(MCAS_DBG_LEVEL DbgLevel, const CHAR * szFmt, ...);

/***********************************************************************
 * Utility Function
 ***********************************************************************/
/**
 *	@fn 		DHmcasmktime
 *	@brief		MCAS에서 정의한 MCAS_GenericTime 구조체의 시간 정보를 Unix Time으로 반환
 *	@param[in]		tm		MCAS Time Struct
 */
UINT32 DHmcasmktime(MCAS_GenericTime *tm);

/**
 *	@fn 	DHtime
 *	@brief	현재 Unix Time Stamp
 */
INT64 DHtime( VOID );

/**
 *	@fn 		DHSleep
 *	@brief		sleep
 */
VOID DHSleep(UINT32 iSleepTime);

/**
 *	@fn 		DHsprintf
 *	@brief		sprintf
 */
INT32 DHsprintf(BYTE *buf, const CHAR *format, ...);

/***********************************************************************
 * TS Filter Management
 ***********************************************************************/
/**
 *	@fn 		Filter_Init
 *	@brief		
 */
INT16 Filter_Init(VOID);

/**
 *	@fn 		filter_set_pid
 *	@brief		CA Message에 대해 Filtering 요청 (동일한 타입에 대해 중복 요청 가능)
 *	@param[in]	emm_ecm		TS 타입 (IB_EMM or IB_ECM)
 *	@param[in]	view_id 	Filtering 요청 View ID
 *	@param[in]	pid 		Filtering 요청 PID
 */
VOID filter_set_pid(BYTE emm_ecm, UINT16 view_id, UINT16 pid);

/**
 *	@fn 		filter_unset_pid
 *	@brief		
 *	@param[in]	emm_ecm		TS 타입 (IB_EMM or IB_ECM)
 *	@param[in]	view_id 	Filtering 요청 View ID
 *	@param[in]	pid 		Filtering 해제 PID
 */
VOID filter_unset_pid(BYTE emm_ecm, UINT16 view_id, UINT16 pid);

/***********************************************************************
 * Descrambler PID Managerment (OPTIONAL)
 ***********************************************************************/
/**
 *	@fn 	Descrambler_set_pid
 *	@brief	H/W Descramble을 위한 PID 설정
 *	@param[in]	pid 		descrambling에 필요한 elementary pid
 */
VOID Descrambler_set_pid( UINT16 pid );

/**
 *	@fn 	Descrambler_unset_pid
 *	@brief	H/W Descramble을 위한 PID 해제
 *	@param[in]	pid 		descrambling에 해제될 elementary pid
 */
VOID Descrambler_unset_pid( UINT16 pid );

/***********************************************************************
 * Descrambler Key Managerment (OPTIONAL)
 ***********************************************************************/
/**
 *	@fn 	set_descrambler_cw
 *	@brief	H/W Descramble을 위한 Key 설정
 *	@param[in]	even_odd	Control Word가 even key(EVEN:0) or odd key(ODD:1)
 *	@param[in]	pid 		Control Word를 설정할 elementary pid
 *	@param[in]	cw 			pid의 descrambling에 필요한 Control word
 */
VOID set_descrambler_cw( BYTE even_odd, UINT16 pid, BYTE cw[8] );

/*************************************************************************
 * NVRAM & Flash Memory Access (OPTIONAL)
 ***********************************************************************/
BOOL DHNVRAMRead(BYTE *read_buf, UINT16 offset, UINT16 length);
BOOL DHNVRAMWrite(BYTE *write_buf, UINT16 offset,UINT16 length);
BOOL DHFlashRead(UINT32 address, BYTE *read_buf, UINT32 length);
BOOL DHFlashWrite(UINT32 address, BYTE *write_buf, UINT32 length);
BOOL DHFlashGetAddress(UINT32 *paddr, UINT32 *size);

#ifdef __cplusplus
}
#endif 

#endif //define _MCAS_H_
