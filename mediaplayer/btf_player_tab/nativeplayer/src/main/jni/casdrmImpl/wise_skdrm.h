#ifndef _SKDRM_APIS
#define _SKDRM_APIS

#include "drmp_Command.h" 
#include "drmp_struct.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "Cust_Feature.h"

/****************************** DEFINITIONS AND MACROS***********************************/
#define DRMP_NO_ERROR								0
#define SKDRM_HEADER_TAG_LENGTH 					4
#define SKDRM_HEADER_LENGTH_FILED				4
#define SKDRM_HEADER_TAG 							"DHDR"	/*String value is use to identify SKDRM data */
#define WISE_SKDRM_NO_OF_TP_READ_ONCE			350
#define WISE_SKDRM_DECRYPT_LOC_BUFFER_SIZE 	(188*WISE_SKDRM_NO_OF_TP_READ_ONCE)
#define WISE_SYNC_BYTE								0x47
#define WISE_SKDRM_TP_LEN							188
#define WISE_TIGGER_DATA_LEN						1024
#define SCS_IP_PORT 								"scs.hanafostv.com:30000" 	//"218.237.55.125:30000" 
#define MAX_WINDOW_SUPPORT						2
#define WISE_STBID_PREFIX_LEN_7					7
#define WISE_PDUDATA_PREFIX_LEN_9					9
#define SCS_SERVER_PROTOCOL_HEADER_LEN_20_AND_CMD_TYPE_LEN_2		22
#define ERROR_ROAPMGR_SOCKET_ERROR			-0x00200012
#define ERROR_FILE_FAIL_OPEN_CERTIFICATE			-0x00040023

// 2012.02.16. namsj DRMPDB ?�러 define
#define ERROR_DBMGR_ABORT								(-0x00210001)
#define ERROR_DBMGR_INIT_DB							(-0x00210002)
#define ERROR_DBMGR_CREATE_TABLE				(-0x00210003)
#define ERROR_DBMGR_OPEN_TABLE					(-0x00210004)
#define ERROR_DBMGR_INSERT							(-0x00210005)
#define ERROR_DBMGR_SELECT							(-0x00210006)
#define ERROR_DBMGR_DELETE							(-0x00210007)
#define ERROR_DBMGR_UPDATE							(-0x00210008)
#define ERROR_DBMGR_DAMAGED_DATA				(-0x00210009)



#define RMint32 	signed long
#define RMbool  	unsigned char
#define SWAP16(num) 									((DC_UINT16)((((num) & 0xFF00) >> 8) | (((num) & 0x00FF) << 8)))
#define SIZE_BUF		(1024*1)
#define HDR_SIZE	(20)
#define WISE_SKDRM_MALLOC(X)						WISE_SKDRM_Malloc ( X )

// 2012.02.16. namsj DRMPDB.dat�?지?�기 ?�해
#define DRMPDB_PATH		"//btf/btvmedia/drm/DB/DRMPDB.dat"

/*----------SKDRM COMMANDS------------*/

#define WISE_SKDRM_INIT 							0
#define WISE_SKDRM_DESTROY						1
#define WISE_SKDRM_CREATE_CONTENT_HANDLER		2
#define WISE_SKDRM_RO_EXIST						7
#define WISE_SKDRM_PROCESS_ROAP					19
#define WISE_SKDRM_DELETE_USELESS_RO			22
#define WISE_SKDRM_DELETE_CONTENT_HANDLER		5
#define WISE_SKDRM_DECRYPT_TS					4

/* 2009.05.29 09:59:33 Kyung-Doc, You <youkd@wisembed.com>	
 * SKAF ?袁⑥�?�븍�?筌욊?�六?�빳?�곕????.\./...
 */
#define WISE_SKDRM_CLEANUP_RO_STORAGE			14
#define WISE_SKDRM_DELETE_RO_BYCID				23

/*************************FUNCTION PROTOTYPES******************************/

DC_INT32	WISE_SKDRM_Init ( DC_VOID );
DC_INT32 	WISE_SKDRM_Destroy (  DC_VOID );
DC_VOID  	WISE_SKDRM_setInitFlag ( DC_BOOL bFlag );
DC_BOOL 	WISE_SKDRM_getInitFlag ( DC_VOID );
DC_INT32 	WISE_SKDRM_BigToLittleEndian ( DC_INT32 i32LegInBigEndian );
DC_BOOL	WISE_SKDRM_IsThisSKDRMData ( DC_INT32 i32fd );
DC_INT32 	WISE_SKDRM_getDRMHeaderLengthInLittleEndian ( DC_INT32 i32fd );

/* 2009.05.29 10:39:22 Kyung-Doc, You <youkd@wisembed.com> ContentID �곕???�뤿�?? */
DC_INT32 WISE_SKDRM_CreatContentHandler ( DC_INT32 i32fd, DC_INT32 i32WindowID, DC_CHAR *ContentID );

/* 2009.05.29 15:36:24 Kyung-Doc, You <youkd@wisembed.com> i32WindowID ??�� */
#if 0
DC_INT32 WISE_SKDRM_IsROExist ( DRMP_CHD 	*pDRMHandler, DC_INT32  i32WindowID );
#else
DC_INT32 WISE_SKDRM_IsROExist ( DRMP_CHD 	*pDRMHandler );
#endif

DC_INT32 WISE_ZOO_SKDRM_CreatContentHandler (DC_CHAR * HeaderData , DC_INT32 i32LegInLittleEndian, DC_INT32 i32WindowID, DC_CHAR* ContentID );
DC_INT32 WISE_ZOO_SKDRM_DecryptTS ( DC_CHAR *pBuf, DC_CHAR *pOutBuf, DC_INT32 i32BufLen, DC_INT32 i32WindowID );
	

DRMP_CHD*	WISE_SKDRM_getSKDRMContentHandler ( DC_INT32 i32WindowID );
DC_INT32 	WISE_SKDRM_DeleteContentHandler (	 DC_INT32 i32WindowID );
DC_INT32 	WISE_SKDRM_DecryptTS ( DC_INT32 i32fd, DC_CHAR *pcOutBuf, DC_INT32 i32BufLen, DC_INT32 i32WindowID );
DC_INT32 	WISE_SKDRM_DecryptTS2( DC_INT32 i32fd, DC_CHAR *pcOutBuf, DC_INT32 i32BufLen, DC_INT32 i32WindowID );
DC_VOID* 	WISE_SKDRM_Malloc ( DC_INT32 size );
DC_VOID 	WISE_SKDRM_Free ( DC_VOID *ptr );
DC_BOOL WISE_FindingSyncByte ( DC_INT32 i32fd, DC_CHAR *pcLocBuf, DC_INT32 i32NoOfByteInSingleRead );
int WISE_SKDRM_getTrigger( 
										unsigned char commandID, 
										unsigned char commandType1, 
										unsigned char commandType2, 
										char *in,
										DC_PCHAR	*out, 
										unsigned int *outLen );
int requestResponse(char *hostName, unsigned short port, 
					unsigned char *sendMsg, unsigned int sendMsgLen, 
					unsigned char **recvMsg, unsigned int *recvMsgLen);

int RequestMSG(char *hostName, unsigned short port,
					unsigned char *sendMsg, unsigned int sendMsgLen,
					unsigned char **recvMsg, unsigned int *recvMsgLen);



DC_VOID WISE_SKDRM_TsDump( DC_CHAR *pcOutBuf, DC_INT32 i32DecBufCount );

/**
 * ?�꿸?�而숋쭕��┷??RO��??���뺣�?
 * 
 * @return	?源껊�???DRMP_NO_ERROR(0), ?�?�� ?�살�??��
 */								
DC_INT32 WISE_SKDRM_DeleteUselessRO( void );

/**
 * CID��?紐꾩?�嚥�RO��??���뺣�?
 *
 * @param[in]	ContentID	�뚢뫂�??���꿃.
 * @return	?源껊�???DRMP_NO_ERROR(0), ?�?�� ?�살�??��
 */								
DC_INT32 WISE_SKDRM_DeleteRObyContentID( DC_CHAR *ContentID );

/**
 * ?�?��??�뚢뫂�??��?�롮�?RO��筌ｋ?�寃�???�륁??�� ?�놁몵筌�?�밴??�뺣�?
 *
 * @param[in]	i32fd		�뚢뫂�??��??���???���???�용�?���?
 * @param[in]	ContentID	�뚢뫂�??��?ID.
 * @return	?源껊�???DRMP_NO_ERROR(0), ?�?�� ?�살�??��
 */								
DC_INT32 WISE_SKDRM_CheckRO ( DC_INT32 i32fd, DC_CHAR *ContentID );

/**
 * ?��?�쇳???筌뤴뫀�?��RO??�폫O DB?�곷?????���뺣�?
 * @return	?源껊�???DRMP_NO_ERROR(0), ?�?�� ?�살�??��
 */
DC_INT32 WISE_SKDRM_CleanupROStorage( void );

/***************************************************************************/
#ifdef __cplusplus
}
#endif
#endif /*_SKDRM_APIS */

