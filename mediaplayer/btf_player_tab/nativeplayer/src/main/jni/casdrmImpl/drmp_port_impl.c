/**
 * DRMP Porting Impl
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <libgen.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include "drmp_port.h"
#include <systemproperty.h>
#include "DRMCrypto.h"
#include "drm_crypto_port.h"
//extern int errno;

#include "util_drm_logmgr.h"


#ifdef __cplusplus
extern "C" {
#endif

void remove_folder_recursive ( char*, int );
extern DC_INT32 wpi_GetContentWithFormat (
										DC_CHAR *fmt_data,
										DC_CHAR *DesPtr,
										DC_CHAR *FormatSearch,
										DC_CHAR delimt );
extern WPI_INT32 wpi_GetFileOpenMode ( DC_FHANDLE_ACCESS_MODE mode );

#define DRM_FOLDER_NAME "/btf/btvmedia/drm"

/*
 *======================================== String Handling Function ======================================
 */
DC_CHAR *pli_strtok ( DC_CHAR *str, const DC_CHAR *delimit)
{
	if ( delimit )
	{
		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 1  \n" ) );

		return wpi_strtok ( str, delimit );
	}
	else
	{
		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 1 illigal Argument , Delimit is NULL \n" ) );
		return NULL;
	}
}

DC_INT32 pli_atoi ( const DC_CHAR *str)
{
	DEBUG_PRINT ( fprintf ( stderr,  "\n DRMP_PORT_IMPL.C> API 2 \n" ) );
	return wpi_atoi (str);
}

DC_INT32 pli_atof ( const DC_CHAR *str)
{
	DEBUG_PRINT ( fprintf ( stderr,  "DRMP_PORT_IMPL.C> API 3 \n" ));
	return wpi_atof (str);
}

DC_INT32 pli_sprintf ( DC_CHAR *buffer, const DC_CHAR *format, ...)
{
	int 			n=0;
	va_list  		ap;

	va_start ( ap, format);
	n = vsprintf ( buffer, format, ap);
	va_end (ap);

	return n;
#if 0
	if ( buffer && format )
	{
		DC_INT32 		n=0;
		va_list 		ap;
		
		va_start ( ap, format);
		n = vsprintf ( buffer, format, ap);
		va_end (ap);
		return n;
	}
	else
	{
		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 4 illigal Argument \n" ) );
		return -1;
	}
#endif
}
         
DC_VOID pli_debugPrint(DC_CHAR *str)
{
	printf("DRM LOG : %s", str);
}

/*
 *========================================File I/O Handling Functions ======================================
 */
DC_INT32 pli_fsCreateDir ( DC_CHAR *name)
{
	DC_INT32 i32Return = 0;

	if ( name )
	{
		DEBUG_PRINT ( fprintf ( stderr,  "DRMP_PORT_IMPL.C> API 5 Dir Creation === [ %s ] \n", name ) );
		if ( 0 == (i32Return = wpi_mkdir_p (name) ) )	//ensure
		{
			DEBUG_PRINT ( fprintf ( stderr,  "DRMP_PORT_IMPL.C> API 5 Success Dir Creation  [ %d ] \n", i32Return ) );
		}
		else
		{
			DEBUG_PRINT ( fprintf ( stderr,  "DRMP_PORT_IMPL.C> API 5 Filed Dir Creation [%d] \n", i32Return ) );
		}
		return i32Return;
	}
	else
	{
		DEBUG_PRINT ( fprintf ( stderr,  "DRMP_PORT_IMPL.C> API 5 illigal Argument  \n" ) );
		return -1;
	}
}

DC_INT32 pli_fsOpen ( DC_CHAR *name, DC_FHANDLE_ACCESS_MODE mode)
{
	DC_INT32 	i32Fd 		= 0,
				i32FileMode	= 0;
	DC_CHAR	*cTempChar	= NULL;

	DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 6  File open \n" ) );

	cTempChar = strrchr( name, '/');
	DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 6  File open1 \n" ) );
#if 0
	if ( cTempChar )
	{
		*cTempChar = '\0';
		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 6  File open2 \n" ) );
		wpi_mkdir_p ( name );
		*cTempChar = '/';
	}
#endif

	DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 6  File open3\n" ) );
	i32FileMode = wpi_GetFileOpenMode ( mode );
	if ( name && ( -1 != i32FileMode ) ) {

		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 6  File open 4\n" ) );
		
		if ( 0 < (i32Fd = wpi_open ( name,  i32FileMode ) ) )
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 6 Success File open [ %s ] [ %d ]  [ %d ] \n", name, i32Fd, mode ) );
		} //else  DEBUG_PRINT("RMP_PORT_IMPL.C> API 7 open fail, ret : %d", i32Fd);
		return i32Fd;
#if 0
		else
		{
			if ( 0 < (i32Fd = wpi_open ( name, O_CREAT |i32FileMode ) ) )
			{
				DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 6 Success File open [ %s ] [ %d ] [ %d ] \n",  name, i32Fd, mode ) );
				return i32Fd;
			}
			else
			{
				DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C>API 6 Failed File open [%s] [ %d ] [ %d ] \n", name, i32Fd, mode ) );
				return i32Fd;
			}
		}
#endif
	}
	else
	{
		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C>API 6 illigal Argument \n" ) );
		return -1;
	}
}

DC_VOID pli_fsClose (DC_INT32 handle)
{
	if( handle >0)
	{
		DEBUG_PRINT ( fprintf ( stderr,  "DRMP_PORT_IMPL.C> API 7 [ %d ] \n", handle ) );
		close ( handle);
	}
	else
	{
		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 7 illigal Argument [ %d ] \n", handle ) );
	}
}

DC_INT32 pli_fsRemove ( DC_CHAR *name)
{
	if( name)
	{
		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 8 \n" ) );
		return wpi_remove (name);
	}
	else
	{
		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 8 illigal Argument \n" ) );
		return -1;
	}
}

DC_INT32 pli_fsRead ( DC_INT32 handle, DC_VOID * pBuf, DC_INT32 size)
{
	DC_INT32 	i32ReturnVal 	= 0,
				i32CurPos	= 0,
				i32EndPos	= 0;

	DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 9  \n" ) );

	if( handle > 0 && pBuf && size > 0)
	{

		i32ReturnVal = read ( handle, pBuf, size);

		if ( 0 <= i32ReturnVal )
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 9 Success File Reading [ %d ] Req size [ %d ]  \n", i32ReturnVal, size ));
		}
		else
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 9 Failed File Reading [ %d ] [ %d ] Req size [ %d ] \n", handle,  i32ReturnVal,  size ) );
		}
		return i32ReturnVal;
	}
	else
	{
		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 9 illigal Argument \n" ) );
		return -1;
	}
}

DC_INT32 pli_fsWrite ( DC_INT32 handle, DC_VOID *pBuf, DC_INT32 size)
{
	DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 10  \n" ) );

	if( handle > 0 && pBuf && size > 0)
	{
		DC_INT32 i32Return = -1;

		i32Return = wpi_write ( handle, pBuf, size);
		if ( 0 > i32Return )
		{
			DEBUG_PRINT ( fprintf ( stderr, "\n DRMP_PORT_IMPL.C> API 10 Fail Handle[ %d ] BufSize[ %d] Write data Size [%d ] \n", handle, size, i32Return ) );
		}
		else
		{
			DEBUG_PRINT ( fprintf ( stderr, "\n DRMP_PORT_IMPL.C> API 10  Success Handle[ %d ] BufSize[ %d] Write data Size [%d ] \n", handle, size, i32Return ) );
		}
		return i32Return;
	}
	else
	{
		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 10 illigal Argument \n" ) );
		return -1;
	}
}

DC_INT32 pli_fsSize (DC_CHAR *name)
{
	DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 11 \n" ) );

	return wpi_size ( name);
}

DC_INT32 pli_fsSeek ( DC_INT32 handle, DC_INT32 offset, DC_FHANDLE_SET_POSITION origin)
{
	DC_INT32 i32Return = -1;

	DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 12  \n" ) );

	i32Return =  wpi_seek ( handle, offset, origin);
	if ( 0 > i32Return )
	{
		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 12 Fail handle [ %d  ] offset [ %d ] pos [ %d ] ret [ %d ] \n", handle, offset, origin, i32Return ) );
	}
	else
	{
		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 12 Success handle [ %d  ] offset [ %d ] pos [ %d ] ret [ %d ] \n", handle, offset, origin, i32Return ) );
	}
	return i32Return;
}

DC_INT32 pli_fsTell ( DC_INT32 handle)
{
	DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 13 \n" ) );

	return wpi_tell ( handle);
	//return wpi_tell ( (FILE *) handle);
}

DC_INT32 pli_fsFlush ( DC_INT32 handle)
{
	DC_INT32 i32Return = 0;

	DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 14  \n" ) );
	if( handle > 0)
	{
// 2012.02.26 namsj 삼성의 경우 fsync()를 사용하지 않는다.
#ifdef __SKAF_SAMSUNG_STB__
		i32Return = 0;
#else
		i32Return = wpi_flush ( handle);		//ensure
#endif
		if ( 0 > i32Return )
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 14 Failed - Flush data [ %d ] \n", i32Return ) );
		}
		else
		{
			DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 14 Success - Flush data [ %d ] \n", i32Return ) );
		}
		return i32Return;
	}
	else
	{
		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 14 illigal Argument \n" ));
		return -1;
	}
}

DC_BOOL pli_fsIsExist ( DC_PCHAR fileName)
{
	DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 15  [ %s ] \n", fileName ) );

	return wpi_exist (fileName);
}

DC_INT32 pli_fsRename ( DC_CHAR *oldName, DC_CHAR *newName)
{
	DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 16 \n" ) );

	return wpi_rename ( oldName, newName);
}

DC_INT32 pli_fsRemoveAll ( DC_CHAR *path)
{
	DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 17 \n" ) );

	return wpi_remove_rf (path); //ensure
}

/*
 *======================================== Memory Handling Functions ======================================
 */
DC_VOID* pli_malloc ( DC_INT32 size)
{
//	DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 18 \n" ) );
	return wpi_malloc (size);
}

DC_VOID pli_free ( DC_VOID *ptr)
{
	if ( NULL != ptr )
	{
//		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 19 \n" ) );
		wpi_free (ptr);
	}
	else
	{
		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 19 illigal Argument \n" ) );
	}
}

DC_VOID* pli_realloc ( DC_VOID *block, DC_INT32 size)
{
	DEBUG_PRINT ( fprintf ( stderr, "\n DRMP_PORT_IMPL.C> API 20 \n" ) );
	return wpi_realloc ( block,size);
}

/*
 *======================================== Time Handling Function ======================================
 */
DC_INT64 pli_time ( DC_VOID)
{
	DEBUG_PRINT ( fprintf ( stderr, "\n DRMP_PORT_IMPL.C> API 21 \n" ) );

	time_t now = time(NULL);


	if (now > GMTIME64_MAX) return -1;


	return (gmtime64_t)now;
}

/*
 *========================================  Random Number Function ======================================
 */
DC_INT32 pli_randBytes ( DC_BYTE *buf, DC_INT32 num)
{
	int 	pos = 0;

	DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 22  \n" ) );
	if( ( buf ) && ( num > 0 ) )
	{
		while( pos < num)
		{
			buf[pos] = (unsigned char) (random()%256);
			pos++;
		}
		DEBUG_PRINT ( fprintf ( stderr, "\n DRMP_PORT_IMPL.C> API 22 \n" ) );

		return 1;
	}
	else
	{
		DEBUG_PRINT ( fprintf ( stderr, "\n DRMP_PORT_IMPL.C> API 22 illigal argument \n" ) );
		return 0;
	}
}

// 임의로 만든다.
void get_system_stbid(DC_BYTE *value)
{

	//if (value)
	//	sprintf(value, "{9C74B5C1-DABB-11E2-B7DA-E3BB7105C3CF}");

	if (value) {
		//get_systemproperty(DS_STR_STB_ID, value, 64);
		get_systemproperty(PROPERTY__STB_ID, value, 64);
	}
	
}

/**
 *========================================  Device Info Funtions ======================================
 */
DC_INT32 pli_getDeviceInfo ( DC_BYTE *type, DC_BYTE *value )
{
    	FILE 		*fpDisp 		= NULL;
	DC_CHAR 	acReadBuf [ READ_BUF_SIZE ] 	= { 0 },
				acTempBuf [ READ_BUF_SIZE ]	= { 0 };

	if ( ( NULL == type ) || ( NULL == value ) )
	{
		DEBUG_PRINT ( fprintf ( stderr, "\n DRMP_PORT_IMPL.C> API 23 illigal argument  \n"   ) );
	}

	DEBUG_PRINT ( fprintf ( stderr, "\n DRMP_PORT_IMPL.C> API 23 Device info Type [ %s ] \n", type  ) );

	if ( 0 != strcmp( type, "STBID" ) )
	{
		DEBUG_PRINT ( fprintf ( stderr, "\n DRMP_PORT_IMPL.C> Failed ID \n"  ) );

		return 0;
	}
#if 1
	get_system_stbid( value );
	return 0;
#else
	fpDisp = fopen( "/mnt/config/REGINFO", "r" );

	if ( NULL == fpDisp )
	{
		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> File open is failed \n" ) );
		return -1;
	}

	while ( !feof (fpDisp) )
	{
		memset(acReadBuf, 0, sizeof(acReadBuf));	
		fgets ( acReadBuf, READ_BUF_SIZE, fpDisp );

		if (acReadBuf[0] == '#' || acReadBuf[0] == ' ' ||acReadBuf[0] == '\n' || acReadBuf[0] == '\r' ||strlen(acReadBuf) < 6) 
		{
			memset(acReadBuf, 0, sizeof(acReadBuf));
			continue;
		}		

		if ( wpi_GetContentWithFormat ( acReadBuf, acTempBuf, DEVICE_INFO, '}' ) )
		{
			DC_INT32 i32Len = 0;

			i32Len = strlen ( acTempBuf );
			acTempBuf[i32Len] = '}';
			acTempBuf[i32Len+1] = '\0';
			
			if ( i32Len > 0 )
			{
				strcpy ( value, acTempBuf ); //{30229B26-7B07-491C-A712-F5864E4871D4}
				fclose ( fpDisp );

				( fprintf ( stderr, "DRMP_PORT_IMPL.C> Device Info Value STBID : [%s] [%d] \n", value, strlen(value) ) );

				return 0;
			}
			else
			{
				fclose ( fpDisp );

				return -1;
			}
		}
	}
#endif

	return -1;
}

DC_INT32 pli_getDrmSecureDirPath ( DC_CHAR *path)
{

	if ( path )
	{
#if 0
		strcpy( path, "/VOD");
#else /* 2009.06.01 10:52:18 Kyung-Doc, You <youkd@wisembed.com> */
		//strcpy( path, FH5000_DRM_SECURE_PATH );
		//strcpy( path, "/data/user/0/com.skb.framework.myapplication/btf/btvmedia/drm");
		char dataPath[128];
		get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
		strcat(dataPath, DRM_FOLDER_NAME);
		strcpy( path, dataPath);
#endif
		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 24 valid [ %s ] \n", path ) );
		return 0;
	}
	else
	{
		DEBUG_PRINT ( fprintf ( stderr, "\n DRMP_PORT_IMPL.C> API 24 illigal path \n" ) );
		return -1;
	}

}

// 2018.01.25 jyjung add
// changed by new drm porting guide
DC_INT32 pli_getDrmSecureDirPath2 ( DC_CHAR *path)
{

	if ( path )
	{
		//strcpy( path, "/data/user/0/com.skb.framework.myapplication/files/btf/btvmedia/drm");
		char dataPath[128];
		get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
		strcat(dataPath, DRM_FOLDER_NAME);
		strcpy(path, dataPath);
		DEBUG_PRINT ( fprintf ( stderr, "DRMP_PORT_IMPL.C> API 24 valid [ %s ] \n", path ) );
		return 0;
	}
	else
	{
		DEBUG_PRINT ( fprintf ( stderr, "\n DRMP_PORT_IMPL.C> API 24 illigal path \n" ) );
		return -1;
	}
	
	return 0;
}
/*
 *========================================   Socket Function  ======================================
 */
DC_INT32 pli_sOpen( DC_INT32 type)
{
	DEBUG_PRINT ( fprintf ( stderr, "\n DRMP_PORT_IMPL.C> API 25 \n" ) );

	return socket ( AF_INET, type, 0 );
}

DC_INT32 pli_sClose ( DC_INT32 s)
{
	DEBUG_PRINT ( fprintf ( stderr, "\n DRMP_PORT_IMPL.C> API 26 \n" ) );

	return close ( s);
}

DC_INT32 pli_sConnect( DC_INT32 s, DC_UINT16 port, DC_UINT32 addr)
{
	struct sockaddr_in pin = {0,};

	DEBUG_PRINT ( fprintf ( stderr, "\n DRMP_PORT_IMPL.C> API 27 \n" ) );
	pin.sin_family = AF_INET;
	pin.sin_addr.s_addr = addr;
	pin.sin_port = htons(port);

	return connect( s,	 (struct sockaddr *)&pin, sizeof(pin));
}

DC_INT32 pli_sRecv ( DC_INT32 s, DC_BYTE *buf, DC_INT32 len)
{
	DEBUG_PRINT ( fprintf ( stderr, "\n DRMP_PORT_IMPL.C> API 28 \n" ) );

	return recv ( s, buf, len, 0);
}

DC_INT32 pli_sSend ( DC_INT32 s, DC_BYTE *buf, DC_INT32 len)
{
	DEBUG_PRINT ( fprintf ( stderr, "\n DRMP_PORT_IMPL.C> API 29 \n" ) );

	return send ( s, buf, len, 0);
}

DC_INT32 pli_sGetHostByName ( DC_CHAR *name)
{
	struct hostent *hp = NULL;

	DEBUG_PRINT ( fprintf ( stderr, "\n DRMP_PORT_IMPL.C> API 30 \n" ) );

	// 2012.02.10 namsj gethostbyname() 이 실패할 경우가 있다.
	// 인터넷에 찾아 보면 대략 실제 호스트가 많을 경우 그럴 때가 있다는 것 같다.
	// gethostbyname2()를 사용하게 되면 잘 얻어 진다.
	//if ( (hp =gethostbyname(name)) == NULL)
	if ( (hp =gethostbyname2(name, AF_INET)) == NULL)
		return 0;

//	fprintf ( stderr, "DRMP_PORT_IMPL.C> API 30 % name [ %s ] add [ %s ] add [ %d ] len [ %d ]",  hp ->h_name, hp ->h_addr, ((struct in_addr*) (hp ->h_addr))->s_addr, hp ->h_length );

	return ((struct in_addr*) (hp->h_addr))->s_addr;
}

DC_INT32 pli_getTickCount(void)
{
	struct timeval t;

	gettimeofday(&t, NULL);
	return ((t.tv_sec * 1000) + (t.tv_usec / 25000) * 25);
}

#ifdef __cplusplus
}
#endif



