/*
 * mcas-reg-dummy-impl.c
 */

#include "mcas_port.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sched.h>
#include <pthread.h>
#include <stdarg.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <time.h>
#include <sys/time.h>

#include "mcas_iptv.h"
#include "utile_socket.h"
#include "scs-protocol.h"

#include <wise_skdrm.h>
#include "dmux_define.h"
#include "utile_wscs.h"
#include "jsmn.h"
#include <systemproperty.h>

#include "util_drm_logmgr.h"
#ifdef _WIN64
#include <lib/curl/include/curl/curl.h> //for 64bit build
#endif
//#include "iptvmedia/util_logmgr.h"

#define IPTV_CAS_LAST_OTA  "/btf/btvmedia/last_ota"
#define IPTV_CAS_TEMP_PATCH_REPORT_OK  "/btf/btvmedia/tmp/_upm_patch_report_ok"
#define IPTV_CAS_TEMP_PATCH_OK	"/btf/btvmedia/tmp/patch_ok"


/* SAK - 25Feb2009 - For testing , get the SCSHOST address from the /mnt/config/svcagent.conf file*/
int Parse_mnt_config(char *filename, char *ScsHostAddr, char *ScsVer);

#define PORT 30000

//#define _USE_TEST_STBID_MAC // for testing
extern int pli_getDeviceInfo ( char *type, char *value );
extern int wpi_get_interface_info( const char *eth, int infotype, char *rtnval, int fmttype );

// cas portting add by ksyoon 20180725
static int jsonequal(const char *json, jsmntok_t *tok, const char *s){
    if(tok->type == JSMN_STRING && (int) strlen(s) == tok->end - tok->start &&
       strncmp(json + tok->start, s, tok->end - tok->start) == 0 ){
        return 0;
    }
    return -1;
}

#define SERVER_LIST_PATH	"/btf/config/server-list.conf"

int get_wscs_server_address_from_xml( char *ip, int *port, char *apiKey )
{
    struct stat st;
    FILE *fp = NULL;
    char *tbuf = NULL;
    char *tPointerString = NULL;
    char *tPointerStart = NULL;
    char *tPointerEnd = NULL;
    int nStringCount;
    char szBuf[1024];
    int ret = 0;

	char dataPath[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
	strcat(dataPath, SERVER_LIST_PATH);

    fp = fopen( dataPath, "r" );

    if( fp == NULL )
    {
        DPRINTSRC( "fail to file open.." );
        ret = -1;
        goto FINISH;
    }

    stat( dataPath, &st );

    tbuf = malloc( st.st_size );
    fread( tbuf, st.st_size, 1, fp );
    fclose( fp );

    tPointerString = strstr(tbuf, "<server id=\"wscs\"");
    if(tPointerString == NULL)
    {
        DPRINTSRC( "fail to find scs server address info.." );
        ret = -1;
        goto FINISH;
    }

    tPointerStart = strstr(tPointerString, "port=\"");
    if(tPointerStart == NULL)
    {
        DPRINTSRC( "fail to find scs server port info.." );
        ret = -1;
        goto FINISH;
    }

    tPointerStart += strlen("port=\"");
    tPointerEnd = strstr(tPointerStart, "\"");

    if(tPointerEnd == NULL)
    {
        DPRINTSRC( "fail to find scs server port info end data .." );
        ret = -1;
        goto FINISH;
    }

    nStringCount = tPointerEnd - tPointerStart;

    if (nStringCount > 10)
    {
        DPRINTSRC( "fail to find scs server port info correct data .." );
        ret = -1;
        goto FINISH;
    }

    memcpy(szBuf, tPointerStart, nStringCount);
    szBuf[nStringCount] = 0;

    if( port != NULL )
    {
        *port = atoi(szBuf);
        DPRINTSRC( "port info [%d]", *port);
    }

    tPointerStart = strstr(tPointerString, "address=\"");
    if(tPointerStart == NULL)
    {
        DPRINTSRC( "fail to find scs server address info.." );
        ret = -1;
        goto FINISH;
    }

    tPointerStart += strlen("address=\"");
    tPointerEnd = strstr(tPointerStart, "\"");

    nStringCount = tPointerEnd - tPointerStart;

    if (nStringCount > 1023)
    {
        DPRINTSRC( "fail to find scs server address info correct data .." );
        ret = -1;
        goto FINISH;
    }

    memcpy(szBuf, tPointerStart, nStringCount);
    szBuf[nStringCount] = 0;

    DPRINTSRC( "address info [%s]", szBuf);

    if( ip != NULL )
    {
        strcpy( ip, szBuf);
    }

    // apk key..
    for(int i = 0 ; i < 3 && tPointerString < (tbuf + st.st_size) ;i++) {
        tPointerString = strstr(tPointerString, "<server id=\"dest\"");

        if(tPointerString == NULL) {
            DPRINTSRC( "fail to find apk key tag.." );
            ret = -1;
            goto FINISH;
        }

        //마지막 라인체크
        char *pLineEnd = strchr(tPointerString, '\n');

        //주석이 있는지 체크
        char *pTemp = strstr(tPointerString, "-->");
        if(pTemp != NULL) {
            if(pLineEnd == NULL || pLineEnd >= pTemp) { //마지막 라인에 주석이 포함되어 잇는지 체크
                //주석이다.
                if(pLineEnd != NULL) tPointerString = pLineEnd;
                else tPointerString++;
                continue;
            }
        }

        tPointerStart = strstr(tPointerString, "address=\"");
        if(tPointerStart == NULL) {
            DPRINTSRC( "fail to find apk key value.." );
            ret = -1;
            goto FINISH;
        }

        tPointerStart += strlen("address=\"");
        tPointerEnd = strstr(tPointerStart, "\"");

        nStringCount = tPointerEnd - tPointerStart;
        memcpy(szBuf, tPointerStart, nStringCount);
        szBuf[nStringCount] = 0;

        //DPRINTSRC( "apk key [%s]", szBuf);

        if( apiKey != NULL ) {
            strcpy( apiKey, szBuf);
        }
        break;
    }

    FINISH:
    if(tbuf) free(tbuf);

    return ret;

}

// WSCS SKCASDRM JIMIN ADD
int16 process_reg_confirm(void)
{
    ResponseData responseData;
    JSONBuf jsonBuf;
//	bson_t *bson; cas portting delete by ksyoon 20180725

    char wscs[20];
    char url[1024];  // by ksyoon modify UI5.0 SCS 규격수정
    int port;
    int ret = WSCS_OK;
    char  buf[1024] = { 0, };  // by ksyoon add UI5.0 SCS 규격수정

    char apiKey[256] = { 0 };

    int m1, m2, m3, m4, m5, m6;
    char mac_addr[32];

    char *pos1 = NULL;
    byte cmd1, cmd2;
    UINT16 skblen = 256;
    BYTE skbmsg[256] = {0,};
    BYTE real_skbmsg[256] = {0,};
    UINT16 certlen = 8192;
    BYTE certmsg[8192] = {0,};
    uint16 cmd_len;

    responseData.buffer = NULL;
    responseData.data = NULL;
    jsonBuf.buf = NULL;

    char tempBuffer[2048];

    get_wscs_server_address_from_xml(wscs, &port, apiKey);

    sprintf(url, WSCS_STB_REG_CONFIRM_URL, wscs, port);

    DPRINTSRC("[%s] [1] wscs stb reg confirm server url = %s, port : %d", __func__, wscs, port);

    if(get_systemproperty("STBID", jsonBuf.stb_id, 39) != 0 || strlen(jsonBuf.stb_id) == 0) {
        DPRINTSRC( "!!!! ERROR : STBID IS NULL !!!!!!");
        return WSCS_ERROR;
    }

    if(get_systemproperty(PROPERTY__ETHERNET_MAC, jsonBuf.mac_addr, 18) != 0 || strlen(jsonBuf.mac_addr) == 0) {
        DPRINTSRC( "!!!! ERROR : MAC IS NULL !!!!!!");
        return WSCS_ERROR;
    }
    getTimeString(jsonBuf.time_str);



    sscanf(jsonBuf.mac_addr, "%02X:%02X:%02X:%02X:%02X:%02X", &m1, &m2, &m3, &m4, &m5, &m6);
    sprintf(mac_addr, "%x:%x:%x:%x:%x:%x", (unsigned char) m1, (unsigned char) m2, (unsigned char) m3, (unsigned char) m4, (unsigned char) m5, (unsigned char) m6);

    DPRINTSRC("[%s] [2] STBID = %s", __func__, jsonBuf.stb_id);
    DPRINTSRC("[%s] [3] MAC = %s", __func__, jsonBuf.mac_addr);

    // MCAS_IPTV__Programming_Guide_V0.71_PG-20111209_D_KR_.pdf document required reading.
    ret = (int) SPA_InteractCmd(jsonBuf.stb_id, SPA_CMD_CERT_ISSUE, MSG_REQUEST, &cmd1, &cmd2, SPA_REQ_REASON_NO_ISSUE, 0x00,
                                &skblen, skbmsg, &certlen, certmsg);

    if(ret != NO_ERROR){
        DPRINTSRC("[%s] SPA_InteractCmd error = %d", __func__, ret);
        return ret;
    }

    DPRINTSRC("[%s] [4] SPA_InteractCmd skbmsg = %s", __func__, skbmsg);

    int prefixSize = (int) strlen("^SKBDATA|");
    if(strnlen(skbmsg, 255) > 0 && strnlen(skbmsg, 255) > prefixSize ) {
        pos1 = strstr(skbmsg, "^SKBDATA|");
        pos1 += strlen("^SKBDATA|");
        strcpy(real_skbmsg, pos1);
    } else
        DPRINTSRC("[%s] SKBDATA is empty, %s", __func__, skbmsg);

    sprintf(buf, WSCS_SKCASDRM_PARAM, "IF-SCS-GWSVC-UI5-004", "1.0", curl_escape(jsonBuf.stb_id, 0), mac_addr,
            cmd1, cmd2, real_skbmsg, "R01", jsonBuf.time_str, curl_escape(certmsg, 0), "get");

    sprintf(url, "%s?%s", url, buf);

    DPRINTSRC("[%s] [5] GET GW URL = %s", __func__, url);

    // cas portting 재시도 없음 by ksyoon
    // by ksyoon modify UI5.0 SCS 규격수정
    ret = http_get_gw(url, &responseData, NO_RETRY_COUNT, apiKey);

    DPRINTSRC("[%s] [6] ret = %d, %s", __func__, ret, (ret != WSCS_ERROR) ? "SUCCESS":"ERROR");
    DPRINTSRC("[%s] [7] responseData.response_code = %d", __func__, responseData.response_code);
	if(responseData.buffer == NULL) DPRINTSRC("[%s] [8] responseData.buffer is null", __func__);
    else DPRINTSRC("[%s] [8] responseData.buffer.size = %d", __func__, strlen(responseData.buffer));

    if (responseData.buffer != NULL && strlen(responseData.buffer) != 0 && ret != WSCS_ERROR && responseData.response_code < 400) {
        jsmn_parser pr;
        int res_parse;
        jsmntok_t  json_res[128];
        int i = 0;

        jsmn_init(&pr);

        res_parse = jsmn_parse(&pr, responseData.buffer, responseData.buffer_size, json_res, sizeof(json_res)/sizeof(json_res[0]));

        if(res_parse < 0){
            DPRINTSRC("[%s] IF-SCS-013 responseData.buffer parse error", __func__);
            ret = WSCS_ERROR;
            goto finish;
        }

        if(res_parse < 1 || json_res[0].type != JSMN_OBJECT){
            DPRINTSRC("[%s] IF-SCS-013 responseData.buffer not json response error", __func__);
            ret = WSCS_ERROR;
            goto finish;
        }

        memset(responseData.result, 0, sizeof(char)*128);

        for(i=1; i < res_parse; i++){
            if(jsonequal(responseData.buffer, &json_res[i], "result") == 0){
                strncpy(responseData.result, responseData.buffer+json_res[i+1].start,
                        (size_t)(json_res[i+1].end-json_res[i+1].start));

                DPRINTSRC("[%s] [9] parse result : %s ", __func__, responseData.result);
            }
            else if(jsonequal(responseData.buffer, &json_res[i], "command_type1") == 0){
                memset(responseData.cmd1, 0, sizeof(responseData.cmd1));
                strncpy(responseData.cmd1, responseData.buffer+json_res[i+1].start,
                        (size_t)(json_res[i+1].end-json_res[i+1].start));

                DPRINTSRC("[%s] [10] parse cmd1 : %s ", __func__, responseData.cmd1);
            }
            else if(jsonequal(responseData.buffer, &json_res[i], "command_type2") == 0){
                memset(responseData.cmd2, 0, sizeof(responseData.cmd2));
                strncpy(responseData.cmd2, responseData.buffer+json_res[i+1].start,
                        (size_t)(json_res[i+1].end-json_res[i+1].start));

                DPRINTSRC("[%s] [11] parse cmd2 : %s ", __func__, responseData.cmd2);
            }
            else if(jsonequal(responseData.buffer, &json_res[i], "data") == 0){
                responseData.data = calloc( (json_res[i+1].end-json_res[i+1].start)+1, sizeof(char));
                strncpy(responseData.data, responseData.buffer+json_res[i+1].start,
                        (size_t)(json_res[i+1].end-json_res[i+1].start));
                responseData.data_length = (unsigned short) (json_res[i + 1].end - json_res[i + 1].start);

                DPRINTSRC("[%s] [12] parse data : %s ", __func__, responseData.data);
            }
            else if(jsonequal(responseData.buffer, &json_res[i], "cert_data") == 0){
                responseData.cert_data = calloc( (json_res[i+1].end-json_res[i+1].start)+1, sizeof(char));
                strncpy(responseData.cert_data, responseData.buffer+json_res[i+1].start,
                        (size_t)(json_res[i+1].end-json_res[i+1].start));
                responseData.cert_data_length = (unsigned short) (json_res[i + 1].end - json_res[i + 1].start);

                DPRINTSRC("[%s] [13] parse cert_data size : %d ", __func__, responseData.cert_data_length);
                //log..
                /*strncpy(tempBuffer, responseData.buffer+json_res[i+1].start, responseData.cert_data_length);
                tempBuffer[responseData.cert_data_length] = 0;
                DPRINTSRC("[%s] [13] parse cert_data : %s ", __func__, tempBuffer);*/
            }
        }

        if (strlen(responseData.result) == 0 || strcmp(responseData.result, "0000") != 0)  // by ksyoon add UI5.0 SCS 규격수정
        {
            DPRINTSRC("[%s] IF-SCS-013 responseData.result error", __func__);
            ret = WSCS_ERROR;
            goto finish;
        }
    }
    else {
        DPRINTSRC("[%s] IF-SCS-013 responseData.buffer error", __func__);
        ret = WSCS_ERROR;
        goto finish;
    }

    cmd1 = (unsigned char) strtol(responseData.cmd1, NULL, 16);
    cmd2 = (unsigned char) strtol(responseData.cmd2, NULL, 16);

    DPRINTSRC("[%s] [14] cmd 1 : %x, cmd2 : %x", __func__, cmd1, cmd2);

    DPRINTSRC("[%s] [15] SPA_InteractCmd(.., SPA_CMD_CERT_ISSUE, MSG_RESPONSE, %d, %d, SPA_REQ_REASON_NO_ISSUE, 0x00,...)", __func__, cmd1, cmd2);
    // MCAS_IPTV__Programming_Guide_V0.71_PG-20111209_D_KR_.pdf document required reading.
    ret = (int) SPA_InteractCmd((unsigned char *) jsonBuf.stb_id, SPA_CMD_CERT_ISSUE, MSG_RESPONSE, &cmd1, &cmd2, SPA_REQ_REASON_NO_ISSUE, 0x00,
                                &responseData.data_length, responseData.data, &responseData.cert_data_length, responseData.cert_data);

    if (ret == WSCS_OK) {
        DPRINTSRC("[%s] [16] SPA_InteractCmd SUCCESS [result = %d]", __func__, ret);
    } else {
        DPRINTSRC("[%s] IF-SCS-013 MCAS_GetCmd FAIL [result = %d]", __func__, ret);
    }

    finish:

    if (responseData.buffer) {
        DPRINTSRC("[%s] free(responseData.buffer)", __func__);
        free(responseData.buffer);
    }

    if (responseData.data) {
        DPRINTSRC("[%s] free(responseData.data)", __func__);
        DPRINTSRC("[%s] SPA_InteractCmd SUCCESS [result = %d]", __func__, ret);
        free(responseData.data);
    }

    if (jsonBuf.buf != NULL) {
        DPRINTSRC("[%s] free(jsonBuf.buf)", __func__);
        //free(jsonBuf.buf);
    }

    return ret;
}

// TRUE : Auto Reset or patch 후 booting
// FALSE : Not
unsigned int isCheckTheCasVerification()
{
    char strAutoResetBoot[9] = "\0";
    int ret = 0;
    struct stat file_info;

    //cache/recovery 에 접근이 가능할
    memset(&file_info, 0x00, sizeof(struct stat));
    if(!stat("/cache/recovery/last_ota", &file_info)) {
        // 파일을 삭제한다.
        remove("/cache/recovery/last_ota");
        DPRINTSRC("check /cache/recovery/last_ota file => ok!!!");
        return TRUE;
    }

    //cache/recovery 에 접근안될때 CPHOME이 옮겨주는 위치
    memset(&file_info, 0x00, sizeof(struct stat));
	char iptv_cas_last_ota[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, iptv_cas_last_ota, 128);
	strcat(iptv_cas_last_ota, IPTV_CAS_LAST_OTA);	
    //if(!stat("/data/user/0/com.skb.framework.myapplication/btf/btvmedia/last_ota", &file_info)) {
    if(!stat(iptv_cas_last_ota, &file_info)) {
        // 파일을 삭제한다.
        //remove("/data/user/0/com.skb.framework.myapplication/btf/btvmedia/last_ota");
		remove(iptv_cas_last_ota);
        DPRINTSRC("check /data/user/0/packageName/btf/btvmedia/last_ota file => ok!!!");
        return TRUE;
    }

    // check the auto reset
    ret = get_systemproperty("AUTORESETBOOT", strAutoResetBoot, (9-1));
    if (ret == 0)
    {
        // CAS Verify is based policy
        return TRUE;
    }
    DPRINTSRC("autoresetboot = %s", strAutoResetBoot);
    if (strcmp(strAutoResetBoot, "1") == 0)
    {
        // 초기화 시켜 놓는다.
        DPRINTSRC("[AUTORESETBOOT = 1] ");
        set_systemproperty("AUTORESETBOOT", "0", 1);
        return TRUE;
    }

    // Check the patch
    // Patch success path : /btv_home/tmp/_upm_patch_report_ok
    memset(&file_info, 0x00, sizeof(struct stat));
	char iptv_cas_temp_patch_report_ok[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, iptv_cas_temp_patch_report_ok, 128);
	strcat(iptv_cas_temp_patch_report_ok, IPTV_CAS_TEMP_PATCH_REPORT_OK);	
    //if(!stat("/data/user/0/com.skb.framework.myapplication/btf/btvmedia/tmp/_upm_patch_report_ok", &file_info))
	if(!stat(iptv_cas_temp_patch_report_ok, &file_info))
    {
        // UpdateManager에서 삭제한다.
        DPRINTSRC("check _upm_patch_report_ok file => ok!!!");
        return TRUE;
    }

    // Check the patch2
    // Patch success path : /btv_home/tmp/patch_ok
    memset(&file_info, 0x00, sizeof(struct stat));
	char iptv_cas_temp_patch_ok[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, iptv_cas_temp_patch_ok, 128);
	strcat(iptv_cas_temp_patch_ok, IPTV_CAS_TEMP_PATCH_OK);	
    //if(!stat("/data/user/0/com.skb.framework.myapplication/btf/btvmedia/tmp/patch_ok", &file_info))
	if(!stat(iptv_cas_temp_patch_ok, &file_info))
    {
        // mediamanger가 직접 삭제한다.
        DPRINTSRC("check /data/user/0/packageName/btf/btvmedia/tmp/patch_ok file => ok!!!");
        //remove("/data/user/0/com.skb.framework.myapplication/btf/btvmedia/tmp/patch_ok");
		remove(iptv_cas_temp_patch_ok);
        return TRUE;
    }

    // 2017.08.04 파셜 패치 적용
    // Check the patch3
    // Patch success path : /DATA/SKAF/partial_patch_ok
    memset(&file_info, 0x00, sizeof(struct stat));
    if(!stat("/DATA/SKAF/partial_patch_ok", &file_info))  // kgh check
    {
        // mediamanger가 직접 삭제한다.
        DPRINTSRC("check /DATA/SKAF/partial_patch_ok file => ok!!!");
        remove("/DATA/SKAF/partial_patch_ok");
        return TRUE;
    }

    return FALSE;
}

// 20201029 : jung2604 : 11개월 이상 지났는지 체크하는 함수
int16 checkAuthOldFileIdentification(void) {
    struct stat     st = {0,};
    // check th file
    DPRINTSRC("checkAuthOldFileIdentification() ");
	char dataPath[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
	strcat(dataPath, WSCS_INFO_SAVE_FILE_PATH);	
    if (stat(dataPath, &st) != 0) {
        DPRINTSRC("[%s] no search file %s", __func__, dataPath);
        return 0;
    }

    struct 	timeval curTime;
    struct 	tm *ptm = NULL;
    gettimeofday( &curTime, NULL );
    ptm = (struct tm *)localtime( &curTime.tv_sec );

    LOGSRC("[MCASAUTH_CONF] [%s] 현재시간 [%d년, %02d/%02d]", __func__, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday);

    struct tm *fileTime = localtime(&st.st_mtime);
    DPRINTSRC("[MCASAUTH_CONF] [%s] %s file 수정된 시간 : %d년 %d월 %d일", __func__, WSCS_INFO_SAVE_FILE_PATH, fileTime->tm_year + 1900, fileTime->tm_mon + 1, fileTime->tm_mday);

    fileTime = localtime(&st.st_mtime);
    struct tm yearLaterDate= { 0 };
    yearLaterDate.tm_year = fileTime->tm_year + 1;
    yearLaterDate.tm_mon = fileTime->tm_mon;
    yearLaterDate.tm_mday = fileTime->tm_mday;

    DPRINTSRC("[MCASAUTH_CONF] [%s] %s file 1년 되는 시간 : %d년 %d월 %d일", __func__, dataPath, yearLaterDate.tm_year + 1900, yearLaterDate.tm_mon + 1, yearLaterDate.tm_mday);
    time_t yearLaterTime = mktime(&yearLaterDate);
    int remainTime = (int)(difftime(yearLaterTime, curTime.tv_sec) / (60 * 60 * 24));

    LOGSRC("[MCASAUTH_CONF] [%s] 인증 파일 생성이후 약 11개월 되기까지 : %d일 남음",__func__,  remainTime - 30);

    if(remainTime <= 30 || remainTime > 366) {
        LOGSRC("[MCASAUTH_CONF] [%s] 인증 만료 파일 삭제!! ", __func__);
        remove(dataPath);
        return -1;
    }

    return 0;
}

// FALSE : have to do auth cerification
// TRUE : There is no auth cerification
int16 isCheckMcasAuth(void)
{
    byte stb_cmd = 0xD1;
    byte cmd1, cmd2;

    int readed=0;
    char stb_id[40]={0,};
    int ret = WSCS_OK;
    struct stat     st = {0,};
    char *readData=NULL;
    int pos=0;
    int buffer_size;
    uint16 data_length;
    char szCmd1[128];
    char szCmd2[128];
    char *data=NULL;
    int totalSize=0;
    FILE *fp;

    // 20201029 : jung2604 : 1년이 지났는지 체크
    checkAuthOldFileIdentification();
	char dataPath[128];
	get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
	strcat(dataPath, WSCS_INFO_SAVE_FILE_PATH);

    // check th file
    if (stat(dataPath, &st) != 0)
    {
        DPRINTSRC("isCheckMcasAuth() no search file %s", dataPath);
        return 0;
    }

    // read the file WSCS_INFO_SAVE_FILE_PATH(/DATA/SKAF/skaf_home/conf/mcasAuth.conf
    if (fp = fopen(dataPath, "r"))
    {
        int nTmp=sizeof(int);
        char *tmpBuf=NULL;

        tmpBuf = (char*)malloc(nTmp+1);
        if (!tmpBuf)
        {
            // 메모리 부족이니 그냥 넘어간다. 어차피 상단에서도 메모리 부족일테니.
            return 1;
        }
        memset(tmpBuf, 0x00, nTmp+1);
        fread(tmpBuf, 1, nTmp, fp);
        memcpy((void*)&totalSize, tmpBuf, nTmp);
        if (totalSize < 8)
        {
            if (tmpBuf)
            {
                free(tmpBuf);
                tmpBuf = NULL;
            }
            fclose(fp);
            // 메모리 부족이니 그냥 넘어간다. 어차피 상단에서도 메모리 부족일테니.
            return 1;
        }

        if (tmpBuf)
        {
            free(tmpBuf);
            tmpBuf = NULL;
        }

        fseek(fp, 0, SEEK_SET);
        DPRINTSRC("totalsize=%d", totalSize);
        readData = (char*)malloc(totalSize+1);
        if (!readData)
        {
            fclose(fp);
            // 메모리 부족이니 그냥 넘어간다. 어차피 상단에서도 메모리 부족일테니.
            return 1;
        }
        memset(readData, 0x00, totalSize+1);
        readed = fread(readData, 1, totalSize, fp);
        fclose(fp);
    }
    else
    {
        // open 실패시는 해당 파일을 지우고 다시 인증 처리를 한다.
        char szCmd[128]={0,};
        sprintf(szCmd, "rm %s", dataPath);
        system(szCmd);

        return 0;
    }

    if (readed != totalSize)
    {
        if (readData)
        {
            free(readData);
            readData = NULL;
        }
        return 0;
    }

    // totalSize(int) | responseData.buffer_size(int) | responseData.buffer(char*buffer_size) |
    // responseData.responseCode(long) | responseData.cmd1(char*128) | responseData.cmd2(char*128) |
    // responseData.data_length(int) | responseData.data(char*data_length) | responseData.result(char*128)
    // I need to cmd1, cmd2, data
    // buffer_size
    pos = sizeof(int);
    memcpy((void*)&buffer_size, &readData[pos], sizeof(int));
    DPRINTSRC("buffer_size : %d", buffer_size);
    // cmd1
    pos = sizeof(int) + sizeof(int) + (sizeof(char)*buffer_size) + sizeof(long);
    memset(szCmd1, 0x00, sizeof(char)*128);
    memcpy(szCmd1, &readData[pos], sizeof(char)*128);
    DPRINTSRC("szCmd1 : %s", szCmd1);
    // cmd2
    pos += sizeof(char)*128;
    memset(szCmd2, 0x00, sizeof(char)*128);
    memcpy(szCmd2, &readData[pos], sizeof(char)*128);
    DPRINTSRC("szCmd2 : %s", szCmd2);
    // data_length
    pos += sizeof(char)*128;
    memcpy((void*)&data_length, &readData[pos], sizeof(uint16));
    DPRINTSRC("data_length : %d", data_length);
    // data
    data = (char*)malloc(data_length+1);
    if (!data)
    {
        if (readData)
        {
            free(readData);
            readData = NULL;
        }
        free(data);
        data = NULL;

        // 메모리 부족이니 그냥 넘어 간다.
        return 1;
    }
    memset(data, 0x00, data_length+1);
    pos += sizeof(uint16);
    memcpy(data, &readData[pos], data_length);

    stb_cmd = data[5];

    cmd1 = strtol(szCmd1, NULL, 16);
    cmd2 = strtol(szCmd2, NULL, 16);

    if (readData)
    {
        free(readData);
        readData = NULL;
    }

    if(get_systemproperty("STBID", stb_id, 40) != 0 || strlen(stb_id) == 0) {
        DPRINTSRC( "!!!! ERROR : STBID IS NULL !!!!!!");
        return 0;
    }

    // MCAS_IPTV__Programming_Guide_V0.71_PG-20111209_D_KR_.pdf document required reading.
    //ret = MCAS_GetCmd(stb_id, CMD_STB_AUTH, MSG_RESPONSE, &stb_cmd, &cmd1, &cmd2, &data_length, data);

    ret = (int) SPA_InteractCmd(stb_id, SPA_CMD_STB_AUTH, MSG_RESPONSE, &cmd1, &cmd2, SPA_REQ_REASON_STB_AUTH, 0x00,
                                &data_length, data, null, null);

    if(data) free(data);

    if (ret == WSCS_OK)
    {
        DPRINTSRC("[%s] MCAS_GetCmd SUCCESS [result = %d]", __func__, ret);
        return 1;
    }
    else
    {
        DPRINTSRC("[%s] MCAS_GetCmd FAIL [result = %d]", __func__, ret);
        return 0;
    }

    return 0;
}

// WSCS SKCASDRM JIMIN ADD
int16 process_auth_confirm(void)
{
    ResponseData responseData;
    JSONBuf jsonBuf;
//	bson_t *bson;  cas portting by ksyoon 20180725

    char wscs[20];
    char url[128];
    int port;
    int ret = WSCS_OK;

    char apiKey[256] = { 0 };

    int m1, m2, m3, m4, m5, m6;
    char mac_addr[32];

    char *pos1 = NULL;
    BYTE real_skbmsg[256] = {0,};
    byte cmd1, cmd2;
    UINT16 skblen = 256;
    BYTE skbmsg[256] = {0,};
    UINT16 certlen = 8192;
    BYTE certmsg[8192] = {0,};

    responseData.buffer = NULL;
    responseData.data = NULL;
    jsonBuf.buf = NULL;

    char tempBuffer[2048];
	
	DPRINTSRC("process_auth_confirm...");

    if (isCheckTheCasVerification())
    {
        DPRINTSRC("mcas auth check entering...");
        if (isCheckMcasAuth())
        {
            DPRINTSRC("mcas auth check ok!!!");
            return WSCS_OK;
        }
    }

    get_wscs_server_address_from_xml(wscs, &port, apiKey);

    sprintf(url, WSCS_STB_AUTH_URL, wscs, port);

    DPRINTSRC("[%s] [0] url : %s", __func__, url);
    DPRINTSRC("[%s] [1] wscs stb auth server url = %s, port : %d", __func__, wscs, port);

    if(strstr(wscs, "agw-stg.sk") != NULL) {
        if (isCheckMcasAuth()) {
            DPRINTSRC("[%s] [!] got the mcas auth information from file mcasAuth.conf ", __func__);
        }

        DPRINTSRC("[%s] [!!]  %s is stg server!!!! return ok!!", __func__, url);
        return WSCS_OK;
    }

    if(get_systemproperty("STBID", jsonBuf.stb_id, 39) != 0 || strlen(jsonBuf.stb_id) == 0) {
        DPRINTSRC( "!!!! ERROR : STBID IS NULL !!!!!!");
        return WSCS_ERROR;
    }

    if(get_systemproperty(PROPERTY__ETHERNET_MAC, jsonBuf.mac_addr, 18) != 0 || strlen(jsonBuf.mac_addr) == 0) {
        DPRINTSRC( "!!!! ERROR : MAC IS NULL !!!!!!");
        return WSCS_ERROR;
    }

    getTimeString(jsonBuf.time_str);

    sscanf(jsonBuf.mac_addr, "%02X:%02X:%02X:%02X:%02X:%02X", &m1, &m2, &m3, &m4, &m5, &m6);
    sprintf(mac_addr, "%x:%x:%x:%x:%x:%x", (unsigned char) m1, (unsigned char) m2, (unsigned char) m3, (unsigned char) m4, (unsigned char) m5, (unsigned char) m6);

    DPRINTSRC("[%s] [2] STBID = %s", __func__, jsonBuf.stb_id);
    DPRINTSRC("[%s] [3] MAC = %s", __func__, jsonBuf.mac_addr);

    // MCAS_IPTV__Programming_Guide_V0.71_PG-20111209_D_KR_.pdf document required reading.
    ret = (int) SPA_InteractCmd(jsonBuf.stb_id, SPA_CMD_STB_AUTH, MSG_REQUEST, &cmd1, &cmd2, SPA_REQ_REASON_STB_AUTH , 0x00, &skblen,
                                skbmsg, &certlen, certmsg);

    DPRINTSRC("[%s] SPA_InteractCmd ret = %d cmd1 = %d, cmd2 = %d", __func__, ret, cmd1, cmd2);

    if(ret != NO_ERROR && ret != ERR_NOT_NEED_SERVER){  // kgh cas check
        DPRINTSRC("[%s] SPA_InteractCmd error = %d", __func__, ret);
        return ret;
    }

    if(ret == NO_ERROR) {  // kgh cas check
        DPRINTSRC("[%s] [4] SPA_InteractCmd skbmsg = %s", __func__, skbmsg);

        int prefixSize = (int) strlen("^SKBDATA|");
        if(strnlen(skbmsg, 255) > 0 && strnlen(skbmsg, 255) > prefixSize ) {
            pos1 = strstr(skbmsg, "^SKBDATA|");
            pos1 += strlen("^SKBDATA|");
            strcpy(real_skbmsg, pos1);
        } else
            DPRINTSRC("[%s] SKBDATA is empty, %s", __func__, skbmsg);

        // by ksyoon modify UI5.0 SCS 규격수정
        asprintf(&jsonBuf.buf, WSCS_SKCASDRM_JSON, "IF-SCS-GWSVC-UI5-005", "1.0", jsonBuf.stb_id, mac_addr, cmd1, cmd2, real_skbmsg, "R01", jsonBuf.time_str, certmsg, "post");

        DPRINTSRC("[%s] [5] POST body = %s", __func__, jsonBuf.buf);

        ret = http_post(url, jsonBuf.buf, &responseData, NORMAL_HTTP_RETRY_COUNT, apiKey);

        DPRINTSRC("[%s] [6] ret = %d, %s", __func__, ret, (ret != WSCS_ERROR) ? "SUCCESS":"ERROR");
        DPRINTSRC("[%s] [7] responseData.response_code = %d", __func__, responseData.response_code);
        if(responseData.buffer == NULL) DPRINTSRC("[%s] [8] responseData.buffer is null", __func__);
        else DPRINTSRC("[%s] [8] responseData.buffer.size = %d", __func__, strlen(responseData.buffer));

        if (responseData.buffer != NULL && strlen(responseData.buffer) != 0 && ret != WSCS_ERROR && responseData.response_code < 400) {
            jsmn_parser pr;
            int res_parse;
            jsmntok_t  json_res[128];
            int i = 0;

            jsmn_init(&pr);

            res_parse = jsmn_parse(&pr, responseData.buffer, responseData.buffer_size, json_res, sizeof(json_res)/sizeof(json_res[0]));

            if(res_parse < 0){
                DPRINTSRC("[%s] IF-SCS-014 responseData.buffer parse error", __func__);
                ret = WSCS_ERROR;
                goto finish;
            }

            if(res_parse < 1 || json_res[0].type != JSMN_OBJECT){
                DPRINTSRC("[%s] IF-SCS-014 responseData.buffer not json response error", __func__);
                ret = WSCS_ERROR;
                goto finish;
            }

            memset(responseData.result, 0, sizeof(char)*128);

            for(i=1; i < res_parse; i++){
                if(jsonequal(responseData.buffer, &json_res[i], "result") == 0){
                    strncpy(responseData.result, responseData.buffer+json_res[i+1].start,
                            (size_t)(json_res[i+1].end-json_res[i+1].start));

                    DPRINTSRC("[%s] [9] parse result : %s ", __func__, responseData.result);
                }
                else if(jsonequal(responseData.buffer, &json_res[i], "command_type1") == 0){
                    memset(responseData.cmd1, 0, sizeof(responseData.cmd1));
                    strncpy(responseData.cmd1, responseData.buffer+json_res[i+1].start,
                            (size_t)(json_res[i+1].end-json_res[i+1].start));

                    DPRINTSRC("[%s] [10] parse cmd1 : %s ", __func__, responseData.cmd1);
                }
                else if(jsonequal(responseData.buffer, &json_res[i], "command_type2") == 0){
                    memset(responseData.cmd2, 0, sizeof(responseData.cmd2));
                    strncpy(responseData.cmd2, responseData.buffer+json_res[i+1].start,
                            (size_t)(json_res[i+1].end-json_res[i+1].start));

                    DPRINTSRC("[%s] [11] parse cmd2 : %s ", __func__, responseData.cmd2);
                }
                else if(jsonequal(responseData.buffer, &json_res[i], "data") == 0){
                    responseData.data = calloc( (json_res[i+1].end-json_res[i+1].start)+1, sizeof(char));
                    strncpy(responseData.data, responseData.buffer+json_res[i+1].start,
                            (size_t)(json_res[i+1].end-json_res[i+1].start));
                    responseData.data_length = json_res[i+1].end-json_res[i+1].start;

                    DPRINTSRC("[%s] [12] parse data : %s ", __func__, responseData.data);
                }
            }

            if (strlen(responseData.result) == 0 || strcmp(responseData.result, "0000") != 0)  // by ksyoon modify UI5.0 SCS 규격수정
            {
                DPRINTSRC("[%s] IF-SCS-014 responseData.result error", __func__);
                ret = WSCS_ERROR;
                goto finish;
            }
        }
        else
        {
            DPRINTSRC("[%s] IF-SCS-014 responseData.buffer error", __func__);
            ret = WSCS_CONN_ERROR;
            goto finish;
        }

        cmd1 = (unsigned char) strtol(responseData.cmd1, NULL, 16);
        cmd2 = (unsigned char) strtol(responseData.cmd2, NULL, 16);

        DPRINTSRC("[%s] [13] cmd 1 : %x, cmd2 : %x", __func__, cmd1, cmd2);

        DPRINTSRC("[%s] [14] SPA_InteractCmd(.., SPA_CMD_STB_AUTH, MSG_RESPONSE, %d, %d, SPA_REQ_REASON_STB_AUTH, 0x00,...)", __func__, cmd1, cmd2);
        // MCAS_IPTV__Programming_Guide_V0.71_PG-20111209_D_KR_.pdf document required reading.
        ret = (int) SPA_InteractCmd((unsigned char *) jsonBuf.stb_id, SPA_CMD_STB_AUTH, MSG_RESPONSE, &cmd1, &cmd2, SPA_REQ_REASON_STB_AUTH, 0x00,
            &responseData.data_length, responseData.data, null, null);

        if (ret == WSCS_OK) {
            DPRINTSRC("[%s] [15] SPA_InteractCmd SUCCESS [result = %d]", __func__, ret);
            // if is is succeed, save file
            /* typedef struct ResponseData {
                char *buffer;
                size_t buffer_size;
                long response_code;
                char cmd1[128];
                char cmd2[128];
                char *data;
                uint16 data_length;
                char result[128];
            } ResponseData;
            */
            // if file exist, first delete the file.
            struct stat     st = {0,};
            char *saveData=NULL;
            int increaseLength=0;
		    char dataPath[128];
		    get_systemproperty(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, dataPath, 128);
		    strcat(dataPath, WSCS_INFO_SAVE_FILE_PATH);
		    DPRINTSRC("path=%s", dataPath);
            FILE *fp;
            if (stat(dataPath, &st) == 0)
            {
                char szCmd[128];
                memset(szCmd, 0x00, 128);
                unlink(dataPath);
            }

            // Last 16bit is CRC-16
            int totalSize = sizeof(int) + sizeof(int) + sizeof(char)*responseData.buffer_size +  sizeof(long) +
                        sizeof(char)*128*2 + sizeof(uint16) + sizeof(char)*responseData.data_length + sizeof(char)*128;

            DPRINTSRC("totalSize=%d", totalSize);
            saveData = (char*)malloc(totalSize+1);
            if (!saveData) {
                goto finish;
            }
            DPRINTSRC("goto 1");
            memset(saveData, 0x00, totalSize+1);
            // total size
            memcpy(saveData, &totalSize, sizeof(int));
            increaseLength = sizeof(int);
            DPRINTSRC("goto 2");
            // buffer_size
            memcpy(&saveData[increaseLength], &responseData.buffer_size, sizeof(int));
            increaseLength += sizeof(int);
            DPRINTSRC("goto 3 buffer size = %d", responseData.buffer_size);
            // buffer
            memcpy(&saveData[increaseLength], responseData.buffer, responseData.buffer_size);
            increaseLength += responseData.buffer_size;
            DPRINTSRC("goto 4");
            // response_code
            memcpy(&saveData[increaseLength], &responseData.response_code, sizeof(long));
            increaseLength += sizeof(long);
            DPRINTSRC("goto 5");
            // cmd1
            memcpy(&saveData[increaseLength], responseData.cmd1, sizeof(char)*128);
            increaseLength += sizeof(char)*128;
            DPRINTSRC("goto 6");
            // cmd2
            memcpy(&saveData[increaseLength], responseData.cmd2, sizeof(char)*128);
            increaseLength += sizeof(char)*128;
            DPRINTSRC("goto 7");
            // data_length
            memcpy(&saveData[increaseLength], &responseData.data_length, sizeof(uint16));
            increaseLength += sizeof(uint16);
            // data
            DPRINTSRC("goto 8");
            memcpy(&saveData[increaseLength], responseData.data, responseData.data_length);
            increaseLength += responseData.data_length;
            DPRINTSRC("goto 9");
            // result
            memcpy(&saveData[increaseLength], responseData.result, sizeof(char)*128);

            DPRINTSRC("MCAS Auth Data : totalSize(%d), buffer_size(%d), response_code(%d)",
                  totalSize, responseData.buffer_size, responseData.response_code);
            DPRINTSRC("MCAS Auth Data : cmd1(%s), cmd2(%s), data_length(%d), result(%s)",
                  responseData.cmd1, responseData.cmd2, responseData.data_length,
                  responseData.result);
            // save the data
            if (fp = fopen(dataPath, "w"))
            {
                fwrite(saveData, 1, totalSize, fp);
                fsync((int) fp);
                fclose(fp);
            }
            if (saveData)
            {
                free(saveData);
                saveData = NULL;
            }
        }
        else {
            DPRINTSRC("[%s] IF-SCS-014 MCAS_GetCmd FAIL [result = %d]", __func__, ret);
        }

        finish:

        if (responseData.buffer) {
            free(responseData.buffer);
        }

        if (responseData.data) {
            free(responseData.data);
        }

        free(jsonBuf.buf);
    } else {
        DPRINTSRC("[%s] [15] cmd 1 : %x, cmd2 : %x", __func__, cmd1, cmd2);

        DPRINTSRC("[%s] [16] SPA_InteractCmd(.., SPA_CMD_STB_AUTH, MSG_RESPONSE, %d, %d, SPA_REQ_REASON_STB_AUTH, 0x00,...)", __func__, cmd1, cmd2);
        // MCAS_IPTV__Programming_Guide_V0.71_PG-20111209_D_KR_.pdf document required reading.
        ret = (int) SPA_InteractCmd((unsigned char *) jsonBuf.stb_id, SPA_CMD_STB_AUTH, MSG_RESPONSE, &cmd1, &cmd2, SPA_REQ_REASON_STB_AUTH, 0x00,
                &responseData.data_length, responseData.data, null, null);
    }

    return ret;
}

/* SAK - 25Feb2009 - Obtain the SCSHOST address from /mnt/config/svcagent.conf file */
int Parse_mnt_config(char *filename, char *ScsHostAddr, char *ScsVer)
{
    FILE *fp = NULL;
    char xbuf[256];
    char str1[50], str2[50];

    fp = fopen(filename, "r");
    if (!fp)
    {

    }
    while (fgets(xbuf, sizeof(xbuf), fp))
    {
        if (xbuf[0] == '#' || xbuf[0] == ' ' || xbuf[0] == '\n' || xbuf[0] == '\r' || strlen(xbuf) < 6)
        {
            memset(xbuf, 0, sizeof(xbuf));
            continue;
        }

        if (xbuf[strlen(xbuf) - 1] == '\n' || xbuf[strlen(xbuf) - 1] == '\r')
        {
            xbuf[strlen(xbuf) - 1] = '\0';
        }

        if (strstr(xbuf, "ScsHost"))
        {
            sscanf(xbuf, "ScsHost%49s", str1);
            sscanf(str1, "%127s", ScsHostAddr);
        }
        else if (strstr(xbuf, "ScsVer"))
        {
            sscanf(xbuf, "ScsVer%49s", str2);
            sscanf(str2, "%127s", ScsVer);
        }
        else
        {
            continue;
        }
    }
    fclose(fp);

    return 0;
}

#if 0
// SAK - 16Mar2009 - Added
int16 process_cert_update(void)
{
    byte skt_data[2048] =
            { 0, };
    byte stb_cmd = 0xD1;
    byte *skt_ret_data;
    byte cmd1;
    byte cmd2;
    uint16 cmd_len, cmd_len_ntoh, skt_pdu_size = 0;
    int16 ret = 0, offset = 0;
    ;
    byte send_data[256] =
            { 0, };

    byte stb_id[64] =
            { 0, };

    struct hostent *hp;
    time_t current;
    int n_time;
    char ScsHostAddr[256];  // SAK - 25Feb2009 -Added

    int scs_version;

    int sockfd;
    int len;
    struct sockaddr_in address;
    int result;
    long total;
    // JIMIN
    int ndx = 0;

    get_scs_server_address_from_xml(ScsHostAddr, NULL);

    //if ((hp = gethostbyname( ScsHostAddr )) == NULL)
    hp = gethostbyname2(ScsHostAddr, AF_INET);

    // 2회 반복 2014.12.07
    // JIMIN
    for (ndx = 0; ndx < 2; ndx++)
    {

        time(&current);
        memcpy(&n_time, &current, 4);

        DPRINTSRC("[%s] INFO : ScsHostAddr=%s", __func__, ScsHostAddr);

        scs_version = 15;

        send_data[0] = 0xff; //start byte
        send_data[1] = scs_version; // Version
        send_data[2] = 1; // Svc Code
        send_data[3] = 0x01;
        send_data[4] = 0x01;
        send_data[5] = 0xD1;
        send_data[6] = 0x00;

        //#ifdef _USE_TEST_STBID_MAC
#if 0
        {
			byte mac[] =
			{	0x01, 0x02, 0x03, 0x04, 0x05, 0x06};
			strcpy( stb_id, "{CFC57113-DC50-44BC-A738-0409180B8529}");
			memcpy(send_data+7, mac, 6);
		}
#else
        pli_getDeviceInfo("STBID", stb_id); //javaos
        wpi_get_interface_info("eth0", SIOCGIFHWADDR, send_data + 7, 0);
#endif /* _USE_TEST_STBID_MAC */

        memcpy(send_data + 13, (byte*) &n_time, 4);
        offset = 20; // position of command type 1

        ret = MCAS_GetCmd(stb_id, CMD_STB_CERT_UPDATE, MSG_REQUEST, &stb_cmd, &cmd1, &cmd2, &cmd_len, skt_data);
        stb_cmd = 0xD1;
        DBGMSG(MCAS_DBG_ALL, "REG_CONFIRM ret=%d cmd=%x, cmd1=%x, cmd2=%x, cmd_len=%d\n", ret, stb_cmd, cmd1, cmd2, cmd_len);

        // scs uses little endian, don't change byte order
        cmd_len_ntoh = cmd_len + 2; //including type 1  type 2  position
        memcpy(send_data + 17, &cmd_len_ntoh, 2);
        send_data[offset] = cmd1;
        send_data[offset + 1] = cmd2;
        memcpy(send_data + offset + 1 + 1, skt_data, cmd_len);

        total = offset + 2 + cmd_len;

        // JIMIN
        if (hp == NULL)
        {
            address.sin_addr.s_addr = inet_addr(ScsHostAddr);
            if (address.sin_addr.s_addr == -1)
            {
                return -1;
            }
        }
        else
        {
            if (hp->h_addr_list[ndx] == NULL)
            {
                address.sin_addr.s_addr = inet_addr(inet_ntoa(*((struct in_addr *) hp->h_addr_list[ndx])));
            }
            else
            {
                return -1;
            }
        }

        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd == -1)
        {
            return -1;
        }

        address.sin_family = AF_INET;
        address.sin_port = htons(PORT);
        len = sizeof(address);

        LOG_TIME_STARTV("connectEx sockfd(%d) ScsHostAddr(%s) PORT(%d)", sockfd, ScsHostAddr, PORT);
            result = connectEx(sockfd, (struct sockaddr *) &address, len, CAS_TIME_OUT);
        LOG_TIME_END("connectEx sockfd(%d) result(%d)", sockfd, result);
        if (0 > result)
        {
            shutdownEx(sockfd);
            DPRINTSRC("ERROR:process_cert_update connectEx");
            // JIMIN
            continue;
        }

        LOG_TIME_STARTV("scs_send sockfd(%d) ScsHostAddr(%s) PORT(%d)", sockfd, ScsHostAddr, PORT);
            result = scs_sendEx(sockfd, send_data, total); // Encode
        LOG_TIME_END("scs_send sockfd(%d) result(%d)", sockfd, result);
        if (0 > result)
        {
            shutdownEx(sockfd);
            DPRINTSRC("ERROR:process_cert_update scs_send");
            // JIMIN
            continue;
        }

        memset(send_data, 0, 256);

        LOG_TIME_STARTV("recvEx sockfd(%d) ScsHostAddr(%s) PORT(%d)", sockfd, ScsHostAddr, PORT);
            result = scs_recvEx(sockfd, skt_data, 2048); // Decode
        LOG_TIME_END("recvEx sockfd(%d) result(%d)", sockfd, result);

        shutdownEx(sockfd);

        if (0 >= result)
        {
            DPRINTSRC("ERROR:process_cert_update recvEx");
            // JIMIN
            continue;
        }

        ///   Response !!!!!

        stb_cmd = skt_data[5];
        memcpy(&cmd_len, skt_data + 17, 2);
        // scs uses little endian, don't change byte order
        cmd_len_ntoh = (cmd_len);
        offset = 17;

        skt_pdu_size = cmd_len;
        cmd1 = *(skt_data + offset + 3);
        cmd2 = *(skt_data + offset + 4);
        skt_ret_data = skt_data + offset + 5;
        ret = MCAS_GetCmd(stb_id, CMD_STB_CERT_UPDATE, MSG_RESPONSE, &stb_cmd, &cmd1, &cmd2, &skt_pdu_size, skt_ret_data);
        DPRINTSRC("[%s] MCAS_GetCmd:CMD_STB_CERT_UPDATE=>ret=%d", __func__, ret);
        return ret;

    }

    // JIMIN
    return -1;
}
#endif
