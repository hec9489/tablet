/*
 * utile_wscs.h
 *
 *  Created on: 2015. 05. 07.
 *      Author: JIMIN
 */

#include "mcas_types.h"

#ifndef UTILE_WSCS_H_
#define UTILE_WSCS_H_

#define WSCS_CONN_ERROR -2
#define WSCS_ERROR -1
#define WSCS_OK 0

#define MAX_PROTO_LENGTH    32
#define MAX_URL_LENGTH      1024
#define MAX_IP_LENGTH       1024

#define NORMAL_HTTP_RETRY_COUNT 3
// cas portting ��õ� ���� ���� �߰� by ksyoon
#define NO_RETRY_COUNT 1

//#define WSCS_SKCASDRM_JSON    "{\"response_format\":\"%s\",\"IF\":\"%s\",\"ver\":\"%s\",\"stb_id\":\"%s\",\"mac_address\":\"%s\",\"command_type1\":\"0x%02x\",\"command_type2\":\"0x%02x\",\"data\":\"%s\",\"reason\":\"%s\",\"reg_date\":\"%s\",\"cert_data\":\"%s\"}"
#define WSCS_SKCASDRM_JSON    "{\"if\":\"%s\",\"ver\":\"%s\",\"stb_id\":\"%s\",\"mac_address\":\"%s\",\"command_type1\":\"0x%02x\",\"command_type2\":\"0x%02x\",\"data\":\"%s\",\"reason\":\"%s\",\"req_date\":\"%s\",\"cert_data\":\"%s\",\"method\":\"%s\"}"
#define WSCS_SKCASDRM_TRIGGER_JSON    "{\"if\":\"%s\",\"ver\":\"%s\",\"stb_id\":\"%s\",\"mac_address\":\"%s\",\"command_type1\":\"0x%02x\",\"command_type2\":\"0x%02x\",\"data\":\"%s\",\"req_date\":\"%s\",\"method\":\"%s\"}"
#define WSCS_SKCASDRM_PARAM    "if=%s&ver=%s&stb_id=%s&mac_address=%s&command_type1=0x%02x&command_type2=0x%02x&data=%s&reason=%s&req_date=%s&cert_data=%s&method=%s"

#define WSCS_STB_REG_CONFIRM_URL "%s:%d/scs/v5/pks/issue"//"https://%s:%d/scs/v5/pks/issue"
#define WSCS_STB_AUTH_URL "%s:%d/scs/v5/pks/cert"
#define WSCS_DRM_TRIGGER_URL "%s:%d/scs/v5/drmtrigger"
#define WSCS_INFO_SAVE_FILE_PATH "/btf/btvmedia/cas/mcasAuth.conf" //"/btv_home/IPTV/conf/mcasAuth.conf"

typedef struct ResponseData {
	char *buffer;
	size_t buffer_size;
	long response_code;
	char cmd1[16];
	char cmd2[16];
	char *data;
	uint16 data_length;
        char *cert_data;
        uint16 cert_data_length;
	char result[128];
} ResponseData;

typedef struct JSONBuf {
	char* buf;
	char time_str[20];
	char mac_addr[32];
	char stb_id[40];
} JSONBuf;


void fileOpen();

void fileClose();

void FILELOG(const char *fmt, ...);

void getTimeString(char* time);

int JSONParse(char* json, char* name, char* value);

int JSONParseAlloc(char* json, char* name, char** value, uint16* length);

int get_data_from_url(char *url, char *server, char *proto, char *path);

int get_ip_from_url(char* url, char* resolving_url);

size_t WriteDataCallback(void *ptr, size_t size, size_t nmemb, void *data);

int http_post(char *cpURL, char *requestData, ResponseData *responseData, int tries, char *apiKey);

int http_get_gw(char *cpURL, ResponseData *responseData, int tries, char *apiKey);

int test(void);

static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp);
#endif
