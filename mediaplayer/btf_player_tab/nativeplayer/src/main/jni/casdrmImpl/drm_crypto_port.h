#ifndef DRMCRYPTOPORT_H_
#define DRMCRYPTOPORT_H_

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "Cust_Feature.h"

#ifdef __cplusplus
extern "C" {
#endif



#define INT8			char
#define UINT8		unsigned char
#define INT16		short
#define UINT16		unsigned short
#define UINT32		unsigned int
#define INT32		int
#define LONG		long
#define ULONG		unsigned long
#define INT64		long long
#define UINT64		unsigned long long
#define HFILE			INT32
#define CHAR			char
#define BYTE			unsigned char
#define BOOL		unsigned char

#define MCU_E_OK			0
#define MCU_E_ERROR 		-1
#define MCU_E_BAD_FD		-2
#define MCU_E_EXIST		-5
#define MCU_E_INVALID_PARAMETER 	 -9
#define MCU_E_NO_SUB		((MCU_E)-16)	
#define MCU_E_BUFFER_TOO_SMALL 	-17
#define MCU_E_EOF			-23
#define MCU_E_NOT_EXIST	-28

#define CRYPTO_STORAGE_PATH	"/btf/btvmedia/cas"

#define 	FILE_IS_DIR		0x01
typedef struct _MCU_FILE_INFO {
	INT32		attrib;
	UINT32		creationTime;
	UINT32		size;
} MCU_FILE_INFO;
/**
 * memory-related functions
 */
void *mcu_Malloc( INT32 size);
void mcu_Free( void *ptr);
void* mcu_Realloc( void *p, INT32 size);

/**
 * File I/O related functions
 */
HFILE	mcu_FileOpen( CHAR *filename, CHAR*mode);
INT32 	mcu_FileClose( HFILE hFile);
INT32 	mcu_FileRead( HFILE hFile, BYTE *buff, INT32 size);
INT32 	mcu_FileWrite( HFILE hFile, BYTE *buff, INT32 size);
INT32 	mcu_FileSeek( HFILE hFIle, INT32 offset, INT32 opt);
INT32 	mcu_FileTell( HFILE hFile);
INT32 	mcu_FileFlush( HFILE hFile);
INT32 	mcu_FileRemove( CHAR *fileName);
INT32 	mcu_FileRename( CHAR *oldName, CHAR *newName);
INT32	mcu_FileIsExist( CHAR *fileName);
INT32 	mcu_FileList( CHAR *folderName, CHAR *buff, INT32 size);
INT32 	mcu_FileInfo( CHAR *fileName, MCU_FILE_INFO *fileInfo);
INT32 	mcu_MkDir( CHAR *dirName);
INT32 	mcu_RmDir( CHAR *dirName);

INT32 	mcu_GetDeviceID( BYTE *buff, INT32 size);
INT32	mcu_RandomNumber( CHAR *buff, INT32 size);
int64_t	mcu_GetCurrentTime(void);
INT32	mcu_GetStoragePath(CHAR *buff, INT32 size);
INT32	mcu_GetStoragePath2(CHAR *buff, INT32 size);
void 	mcu_DebugPrint( CHAR *format, ...);

#ifdef __cplusplus
}
#endif

#endif /*DRMCRYPTOPORT_H_*/
