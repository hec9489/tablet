/*
-----------------------------------------------------------------------------
******************************* C HEADER FILE *******************************
-----------------------------------------------------------------------------
**                                                                         **
** PROJECT		: CAS Project for Device                                   **
** FILENAME		: mcas_iptv                                                **
** VERSION		: 1.1.0                                                    **
** DATE			: 2006-04-12                                               **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2005, DigiCAPS Inc.    All rights reserved.               **
**                                                                         **
*****************************************************************************/

#ifndef _MCASIPTV_H_
#define _MCASIPTV_H_

#ifdef __cplusplus
extern "C" {
#endif 

/***************************************************************************/
/**                                                                		  **/
/** MODULES USED                                                          **/
/**                                                                       **/
/***************************************************************************/
#include "mcas_types.h"

/***************************************************************************/
/**                                                                       **/
/** DEFINITIONS AND MACROS                                                **/
/**                                                                       **/
/***************************************************************************/
#define MAX_AV_PID						(3)		// Service 정보의 AV PID 리스트 최대 개수 
#define MAX_TS_LEN						(188)	// Stream TS Packet 길이

/***********************************************************************
* Smart Card 관련 Define
***********************************************************************/
#define SMARTCARD_IN					(0)
#define SMARTCARD_OUT					(1)
#define SMARTCARD_ERROR					(2)

/***********************************************************************
* Control Word의 Type (PLI 가이드의 set_descrambler_cw 참조)
***********************************************************************/
#define EVEN							(0)		// EVEN Control Word
#define ODD								(1)		// ODD Contrl Word
	
/***********************************************************************
* STB Registration Procedure (API 가이드의 MCAS_GetCmd 참조)
***********************************************************************/
#define CMD_STB_REG_CONFIRM    			(0)
#define CMD_STB_AUTH					(1)
#define CMD_STB_CERT_UPDATE				(2)

/***********************************************************************
* STB Registration Procedure (API 가이드의 SPA_InteractCmd 참조)
***********************************************************************/
#define SPA_CMD_CERT_ISSUE    			(4)
#define SPA_CMD_STB_AUTH				(5)

#define	SPA_REQ_REASON_STB_AUTH			(0)
#define	SPA_REQ_REASON_NO_ISSUE			(1)
#define SPA_REQ_REASON_ISSUE_EXPIRED	(2)
#define SPA_REQ_REASON_ISSUE_BREAK		(3)
#define SPA_REQ_REASON_UNKNOWN			(99)


/***********************************************************************
* On Screen MD 
* API 가이드의 MCAS_OnScreen 사용 및 참조
***********************************************************************/
#define CMD_BUY_PRODUCT					(10)

/***********************************************************************
* EPG <-> MCAS Message Request/Response opcode  
* API의 MCAS_GetStatus, MCAS_OnScreen에서 사용
***********************************************************************/
#define MSG_REQUEST						(1)   //데이터 요청. 응답을 필요로 함
#define MSG_RESPONSE					(0)   //데이터 전달. 응답 필요 없음 

/***********************************************************************
* EMM Message의 수신 경로
* PLI의 filter_set_pid에서 사용, Status Screen에서 사용(EMM/ECM Monitor)
***********************************************************************/
#define IB_ECM							(1)		// Broadcast ECM
#define	OOB_EMM							(3)		// OTA EMM
#define	IB_EMM							(7)		// Broadcast EMM
#define SECTION_EMM						(8)		// Section EMM
#define SECTION_ECM						(9)		// Section ECM

/***********************************************************************
* Pay TV 구매 요청 또는 취소 구분 
***********************************************************************/
#define	PAYTV_BUY						(0)   // 선불권 구매
#define PAYTV_CANCEL					(1)   // 선불권 취소(Not Used)
#define IPPV_BUY						(2)   // IPPV 구매
#define IPPV_CANCEL						(3)   // IPPV 취소

/***************************************************************************/
/**                                                                       **/
/** ERROR CODE DEFINITIONS                                                **/
/**                                                                       **/
/**	에러 코드를 네 가지로 구분											  **/
/**	1. Preview 등 예외 상황에 의한 시청 가능							  **/
/**	2. 정상 가입/프로그램 구매 등 권한 획득에 의한 시청 가능 (251~300)	  **/
/**	3. 미가입에 의한 시청 불가 (301-350)								  **/
/**	4. Block-Out, Spot Beam 등 예외 상황에 의한 시청 불가 (351-400)		  **/
/***************************************************************************/
#ifndef NO_ERROR
#define NO_ERROR								(0)
#endif
#define ERR_CLEAN_SERVICE						(0)

/***********************************************************************
* STB Registration ERROR (API 가이드의 MCAS_GetCmd, SPA_InteractCmd 참조)
***********************************************************************/
#define ERR_UNKNOWN_CMD							(1)
#define ERR_NOT_EXIST_CERT						(0)
#define ERR_EXIST_CERT							(1)
#define ERR_STB_REG_CMD							(10)
#define ERR_FILE_ERROR							(11)
#define ERR_CERT_ERROR							(12)
#define ERR_REG_CERT_ERROR						(13)
#define ERR_INVALID_PARAMETER					(14)

#define ERR_NOT_NEED_SERVER						(21)


/***********************************************************************
* CAS System Environment Error
***********************************************************************/
#define G_ERROR_SMARTCARD_OUT					(100)	// Smart Card가 없음
#define ERR_OUT_OF_MEMORY						(101)	// 동적메모리 부족
#define ERR_MCAS_NOT_INITED						(102)	// MCAS 내부 프로세스 초기화 실패
#define	ERR_REBOOT_NEEDED						(103)	// 재부팅 필요(Firmware Upgrade)
#define	G_ERR_NOT_MCAS_APPLET					(104)	// Smart Card 연결 실패
#define ERR_CALLCENTER_NEEDED					(105)	// 고객센터 방문 필요(Firmware Upgrade)
#define	ERR_NOT_ECM_MESSAGE						(106)	// ECM을 받지 못함.
#define ERR_ECM_MESSAGE_NOT_CHANGE				(107) 
#define ERR_SC_UPGRADE_PROGRESS					(108)	// S/C Software Upgrade Progressing
#define ERR_NO_CAT_PACKET						(201)
#define	ERR_NOT_USED_SERVICE					(204)
#define	ERR_EMM_MESSAGE_NOT_CHANGE				(205)
#define	ERR_EMM_INVALID_TABLE_ID				(206)

/***********************************************************************
*  정상, 미리보기 등에 의한 시청 가능 (251 ~ 300)
***********************************************************/
#define ERR_CLEAR_CHANNEL						(251) // PMT에 ECM PID 없음(Clear 채널)
#define	ERR_VIEW_BY_DURATION					(252) // 기간 선불권에 의한 Basic 채널 시청 가능
#define	ERR_VIEW_BY_TIME						(253) // 시간 선불권에 의한 basic 시청 가능
#define	ERR_VIEW_BY_ENTL						(254) // 권한에 의한 basic 시청 가능
#define ERR_VIEW_BY_DURATION_PREMIUM			(256) // 기간 선불권에 의한 Premium 채널 시청 가능
#define ERR_VIEW_BY_DURATION_ETC				(257) // 기간 선불권에 의한 기타 채널 시청 가능
#define ERR_VIEW_BY_ENTL_PREMIUM				(258) // 권한에 의한 premium 시청 가능
#define ERR_VIEW_BY_ENTL_ETC					(259) // 권한에 의한 기타 채널 시청 가능
#define ERR_VIEW_BY_ENTL_PPV					(260) // 권한에 의한 PPV 채널 시청 가능
#define ERR_VIEW_BY_TIME_PREMIUM				(262) // 시간 선불권에 의한 premium 시청 가능
#define ERR_VIEW_BY_TIME_ETC					(263) // 시간 선불권에 의한 etc 시청 가능
#define ERR_VIEW_BY_PRE_ENABLEMENT				(264) // Pre-Enablement에 의한 시청 가능
#define ERR_VIEW_BY_ENTITLEMENT2				(265) // 권한에 의한 시청 가능 2

#define ERR_EXPIRED_TIME_TOKEN_LIFETIME			(266) // 특정 선불권의 유효기간 만료. 그러나 타 선불권으로 추가 시청 가능
#define	ERR_EXIST_TIME_TOKEN_RIGHT				(267) // Time Token이 있으므로 구매 확인(PIN)후 시청 가능
#define	ERR_PAYTV_PREVIEW						(255) // 미리보기에 의한 시청 가능
#define ERR_VIEW_BY_PREVIEW_PPV					(261) // PPV 채널 미리보기에 의한 시청 가능
#define ERR_VIEW_BY_DEFAULT						(268)
#define ERR_VIEW_BY_PREVIEW_POLICY				(269) // Preview Policy에 의해 시청 가능

#define	ERR_EMM_PID_NOT_EXIST					(270) // CAT에 IA나 MCAS의 EMM PID가 존재하지 않음(Clean Service)
#define	ERR_ECM_PID_NOT_EXIST					(271) // PMT에 IA나 MCAS의 ECM PID가 존재하지 않음(Clean Service)


/*********************************************************
* 권한 없음에 의한 시청 불가 (301 ~ 350)
*********************************************************/
#define ERR_NO_RIGHTS 							(301) // 미가입자 시청 권한 없음
#define ERR_NO_RIGHTS_PPV_CHANNEL 				(302) // 시청 가능한 ENTL 없음
#define	ERR_NO_RIGHTS_PREMIUM_CHANNEL			(303) // Premium 채널에 대한 ENTL 없음
#define ERR_EXPIRED_TIME_PREVIEW				(304) // 미리보기 시간 만료
#define	ERR_TIME_TOKEN_EXPIRED_USED_TIME		(305) // 선불권의 사용기간을 모두 소모함
#define ERR_NO_RIGHTS_BASIC_CHANNEL				(306) // Basic에 대한 ENTL 없음
#define	ERR_TIME_TOKEN_REVOCATION				(307) // 선불권의 가입 해지 --> 선불권이 유효기간(기간)이나 amount 모두 사용
#define ERR_NO_MORE_BUY_PPV						(308) // Debit limit 제한에 의해 더이상 구매 불가  
#define ERR_PE_TIME_EXPIRED						(309) // Pre-Enablement 기간 만료
#define ERR_NO_CM_RIGHT							(310) // CM Rights가 시청 불가능 상태(ECM의 CM 유효기간이 현재시간보다 이전임)
#define ERR_NO_RIGHTS_ETC						(311) // 기타 Entl. 에 대한 시청 권한 없음 -->  20060601추가

/*********************************************************
* Block-Out 등 예외 상황에 의한 시청 불가 (351 ~ 400)
*********************************************************/
#define ERR_NOT_ALLOWED_AGE						(351) // 시청불가 연령
#define ERR_NOT_ALLOWED_COUNTRY					(352) // 시청불가 국가코드
#define ERR_ENTL_BLOCKOUT						(353) // ENTL에 의해 Block-Out 됨
#define ERR_USERCLASS_BLOCKOUT					(354) // UserClass에 의해 Block-Out됨
#define ERR_BAD_CLIENT_VERSION_INFO				(355) // Client 버전 불일치에 의한 시청 불가
#define ERR_BAD_SCARD_VERSION_INFO				(356) // SmartCard 버전 불일치에 의한 시청 불가
#define	ERR_PREVIEW_DISABLED					(357) // 서비스 정책에 미리보기 불가
#define ERR_PPV_CALLBACK_LIMIT					(358) // ppv 구매 회수 제한에 걸려 더이상 구매할 수 없음(callback 필요)
#define ERR_PPV_NOT_ALLOWED						(359) // 기타 Entl. 에 대한 시청 권한 없음
												 
/**************************************************************
* EPG 메시지 통신 중 발생할 수 있는 상태 코드 401~450
******************************************************************/
#define ERR_SC_PIN_LOCK							(401) // PIN잠김
#define	ERR_PIN_VERIFICATION_FAILED				(402) // PIN 인증 실패(잘못된 PIN 입력)
#define ERR_SC_PIN_VERIFY_SUCCESS				(403) // PIN 인증 성공
#define ERR_EXPIRED_BUY_REFUND_TIME				(404) // 구매 또는 구매 취소 가능시간 만료
#define ERR_PAYTV_BUY_SUCCESS					(405) // 구매 성공
#define ERR_CANNOT_TIME_TOKEN_REFUND			(406) // 시간선불권의 구매취소 불가(1회 이상 선불권 사용한 경우)
#define ERR_NO_VIEW_SERVICE						(407) // 요청한 채널 정보가 없음(현재 시청 중인 채널이 아님)
#define ERR_RECORDABLE_EVENT					(408) // UserClass에 의한 녹화 가능 채널
#define ERR_CALLBACK_DATA_EXIST					(409) // Callback할 데이터가 있음

/*****************************************************************
* SI Table에서 MCAS용 CA Descriptor 또는 CA_PID를 찾지 못함 (501~600)
******************************************************************/
#define ERR_PMT_IREDETO_ECM_PRIVATE_NOT_EXIST	(501) // PMT에 IA의 CA Descriptor만 존재하며, Private Data 없음	      	
#define ERR_PMT_ECM_PID_NOT_EXIST				(502) // PMT에 IA나 MCAS의 CA_PID 존재하지 않음
#define ERR_PMT_CA_DESC_NOT_EXIST				(503) // PMT에 CA Descriptor가 없음
#define ERR_CAT_IREDETO_EMM_PRIVATE_NOT_EXIST	(504) // CAT에 IA의 CA Descriptor만 존재하며, Private Data 없음 	
#define ERR_CAT_EMM_PID_NOT_EXIST				(505)
#define ERR_CAT_CA_DESC_NOT_EXIST				(506)  // CA Descriptor에 MCAS 및 이데토 CA_SYSTEM_ID 없음
#define ERR_EMM_CRC								(507)
#define ERR_ECM_CRC								(508)
#define ERR_DEV_BIND_FAIL						(510)

/*************************************************************
*           Internal Error Message
*************************************************************/
#define	ERR_SC_UNKNOWN_ERROR					(520)  // S/C 내부의 알수 없는 오류		   
#define ERR_VERIFY_MAC_FAILED					(521)  // Encryption Error 1
#define ERR_INVALID_TABLE_ID					(522)  // 유효하지 않은 Table ID
#define ERR_INVALID_PINCODE_TYPE				(523)  // 지원하지 않는 PIN type
#define ERR_INVALID_EMM_TABLE_ID				(524)  // 유효하지 않은 EMM Table ID
#define ERR_MCAS_COMMON_FAIL					(525)  // MCAS S/C Unknown Error
#define ERR_INVALIED_CURRENT_TIME				(526)  // MCAS Time Invalid(ECM Required)
#define ERR_NO_EXIST_EMM						(527)  // EMM 포맷 에러
#define ERR_BADFORM_EMM_MESSAGE					(528)  // EMM 포맷 에러
#define ERR_NO_CARD_INIT						(529)  // SmartCard가 Init되지 않음
#define ERR_READ_CSID_KEY_FAILED				(530)  // Smart Card Key1 오류
#define ERR_READ_SUBSCRIBER_KEY_FAILED			(531)  // Smart Card Key2 오류
#define ERR_GROUP_KEY_FAILED					(532)  // Smart Card Key3 오류
#define ERR_NOT_EXIST_KEY_BOX_ID				(533)  // Smart Card Key4 오류
#define ERR_BADFORM_KEY_CHG_MESSAGE				(534)  // Smart Card Key5 오류													   
#define ERR_WRONG_CS_ID							(535)  // 올바르지 않은 UserID1
#define ERR_WRONG_GROUP_ID						(536)  // 올바르지 않은 UserID2
#define ERR_WRONG_USER_CLASS_ID					(537)  // 올바르지 않은 UserID3
#define ERR_ALREADY_EXIST_USERCLASS				(538)  // 권한 관리 warnging 1
#define ERR_NO_EXIST_ENTL_ID_DEL				(539)  // 권한 관리 오류 1
#define ERR_NO_EXIST_ENTL_ID_CHG				(540)  // 권한 관리 오류 2
#define ERR_NO_EXIST_USERCLASS					(541)  // 권한 관리 오류 3
#define ERR_USERCLASS_CHANGE_FAILED				(542)  // 권한 관리 오류 4
#define ERR_NO_EXIST_USER_CLASS_CHG				(543)  // 권한 관리 오류 5
#define ERR_NATIONALITY_CHANGE_FAILED			(544)  // 권한 관리 오류 6													   
#define ERR_PINCODE_RESET_FAILED				(545)  // S/C File Access 오류 1
#define ERR_PINCODE_ASSIGN_FAILED				(546)  // S/C File Access 오류 2
#define ERR_DEBIT_LIMIT_UPDATE_FAILED			(547)  // S/C File Access 오류 3
#define ERR_PURCHASE_LIMIT_UPDATE_FAILED		(548)  // S/C File Access 오류 4
#define ERR_READ_CSID_FAILED					(549)  // S/C File Access 오류 5
#define ERR_UPDATE_PRE_ENABLE_FAILED			(550)  // S/C File Access 오류 6
#define ERR_APPEND_PRE_ENABLE_FAILED			(551)  // S/C File Access 오류 7
#define ERR_RESET_DURATION_FAILED				(552)  // S/C File Access 오류 8
#define ERR_UPDATE_RESET_TIME_FAILED			(553)  // S/C File Access 오류 9
#define ERR_RESET_CYCLE_FAILED					(554)  // S/C File Access 오류 10
#define ERR_NO_EXIST_FUNCTION_BOX				(555)  // S/C File Access 오류 11
#define ERR_MCAS_TIME_FAILED					(556)  // S/C File Access 오류 12
#define ERR_READ_PEGSK_SEED_FAILED				(557)  // S/C File Access 오류 13
#define ERR_GKSK_SEED_FAILED					(558)  // S/C File Access 오류 14
#define ERR_CMSK_SEEDKEY_FAILED					(559)  // S/C File Access 오류 15
#define ERR_FILE_NOT_FOUND						(560)  // S/C File Access 오류 16
#define ERR_FILE_UPDATE_FAILED					(561)  // S/C File Access 오류 17
#define ERR_FILE_APPEND_FAILED					(562)  // S/C File Access 오류 18
#define ERR_FILE_READ_FAILED					(563)  // S/C File Access 오류 19
#define ERR_FILE_NO_DATA						(564)  // S/C File Access 오류 20
#define ERR_RECORD_APPEND_FAILED				(565)  // S/C File Access 오류 21
#define ERR_RECORD_READ_FAILED					(566)  // S/C File Access 오류 22
#define ERR_OVERFLOW_FILE_NUM					(567)  // S/C File Access 오류 23
#define ERR_FILE_DELETE_FAILED					(568)  // S/C File Access 오류 24
#define ERR_PIN_WRITE_FAILED					(569)  // S/C File Access 오류 25
#define ERR_PCI_FILE_UPDATE_FAILED				(570)  // S/C File Access 오류 26
#define ERR_PIN_FILE_READ_FAILED				(571)  // S/C File Access 오류 27
#define ERR_NOT_SUPPORTED_COMMAND				(572)  // S/C File Access 오류 28
#define ERR_NOT_SUPPORTED_DATA_FILE				(573)  // S/C File Access 오류 29
#define ERR_NO_EXIST_VALID_DATA					(574)  // S/C File Access 오류 30
#define ERR_RECORD_UPDATE_FAILED				(575)  // S/C File Access 오류 31
#define ERR_NO_EXIST_CM_GROUP_INFO				(576)  // S/C CAS Info Fail 1
#define ERR_INVAILD_GROUP_SEED					(577)  // S/C CAS Info Fail 2
#define ERR_INVALID_CP_INFO						(578)  // S/C CAS Info Fail 3
#define ERR_BAD_ECM_MESSAGE						(579)  // S/C CAS Info Fail 4
#define ERR_RESET_TIME_FAILED					(580)  // S/C CAS Info Fail 5
#define ERR_READ_CM_RESET_TIME					(581)  // S/C CAS Info Fail 6
#define ERR_BAD_SERVICE_GRANT					(582)  // MSG CAS Info Fail 1
#define ERR_NO_EXIST_INFO_BLOCK					(583)  // MSG CAS Info Fail 2
#define ERR_BAD_KDI_INFO						(584)  // MSG CAS Info Fail 3
#define ERR_INVALID_BIRTH_INFO					(585)  // MSG CAS Info Fail 4
#define ERR_INVALID_VERSION_INFO				(586)  // MSG CAS Info Fail 5
#define ERR_CLIENT_VERSION_FAILED				(587)  // MSG CAS Info Fail 6
#define ERR_SCARD_VERSION_FAILED				(588)  // MSG CAS Info Fail 7
#define ERR_MAC_INFO							(589)  // MSG CAS Info Fail 8
#define ERR_MSG_DEC_READ_KEY_FAILED				(590)  // MSG CAS Info Fail 9
#define ERR_MSG_DEC_WRONG_KEY_ID				(591)  // MSG CAS Info Fail 10
#define ERR_WRONG_ENC_MODE						(592)  // MSG CAS Info Fail 11
#define ERR_MSG_DECRYPTION_FAILED				(593)  // MSG CAS Info Fail 12
#define ERR_ECM_READ_KEY_FAILED					(594)  // MSG CAS Info Fail 12
#define ERR_ECM_WRONG_ENC_MODE					(595)  // MSG CAS Info Fail 13
#define ERR_ECM_WRONG_KEY_ID					(596)  // MSG CAS Info Fail 14
#define ERR_ECM_DECRYPTION_FAILED				(597)  // MSG CAS Info Fail 15
#define ERR_NOT_EXIST_TOKEN_SN					(598)  // MSG CAS Info Fail 16
#define ERR_MORE_EMM_MSG_NEED					(599)  // MSG CAS Info Fail 17
#define ERR_NOT_CONTINUE_EMM					(600)  // MSG CAS Info Fail 18
#define ERR_INVALID_EMM_LENGTH					(601)  // MSG CAS Info Fail 19
#define	ERR_NOT_EXIST_ID_SEED_ID				(602)  // MSG CAS Info Fail 20
#define	ERR_NOT_EXIST_FUNC_BOX_ID				(603)  // MSG CAS Info Fail 21
#define ERR_EMM_READ_KEY_FAILED					(604)  // MSG USER Info Fail 1
#define ERR_EMM_WRONG_ENC_MODE					(605)  // MSG USER Info Fail 2
#define ERR_EMM_KEY_ID							(606)  // MSG USER Info Fail 3
#define ERR_EMM_DECRYPTION_FAILED				(607)  // MSG USER Info Fail 4
#define ERR_KDI_READ_KEY_FAILED					(608)  // MSG USER Info Fail 5
#define ERR_KDI_WRONG_ENC_MODE					(609)  // MSG USER Info Fail 6
#define ERR_KDI_WRONG_KEY_ID					(610)  // MSG USER Info Fail 7
#define ERR_KDI_DECRYPTION_FAILED				(611)  // MSG USER Info Fail 8													  
#define ERR_PE_KEY_FAILED						(612)  // PREV USER Info Fail 1
#define ERR_PE_WRONG_ENC_MODE					(613)  // PREV USER Info Fail 2
#define ERR_PE_WRONG_KEY_ID						(614)  // PREV USER Info Fail 3
#define ERR_PE_DECRYPTION_FAILED				(615)  // PREV USER Info Fail 4
#define ERR_SW_MAKE_MSG_FAILED					(616)  // PREV USER Info Fail 5
#define ERR_WRONG_MAC_KEY_ID					(617)  // PREV USER Info Fail 6
#define	ERR_TRANSACTION_FAILED					(618)  // S/C Process Error 1
#define	ERR_ABORT_TRANSACTION_FAILED			(619)  // S/C Process Error 2
#define ERR_NO_EXIST_PROPER_SERVICE_ID			(620)  // S/C Process Error 3
#define	ERR_COMMIT_TRANSACTION_FAILED			(621)  // S/C Process Error 4
#define ERR_BADFORM_DECRYPTED_MSG				(622)
#define ERR_NO_RIGHT_EXIST_TO_BUY				(623)
#define ERR_ALREADY_EXIST_IPPV					(624)
#define ERR_NEED_MORE_CM_MSG					(625)

/*************************************************************
 Smart Card Patch 에 의해 추가된 코드 
**********************************************************/
#define ERR_INVALID_APDU_COMMAND			 	(626)   // 유효하지 못한 APDU Command임.

/*************************************************************
*            ISO-7816 규격 에러
*************************************************************/
#define ERR_7816_CLA_NOT_SUPPORTED				(700)  
#define ERR_7816_INS_NOT_SUPPORTED				(701)
#define ERR_7816_RECORD_NOT_FOUND				(702)
#define ERR_7816_WRONG_LENGTH					(703)
#define ERR_7816_WRONG_DATA						(704)
#define ERR_7816_FILE_NOT_FOUND					(705)
#define ERR_7816_FILE_FULL						(706)	
#define ERR_7816_P1_NOT_SUPPORTED 				(707) //P1, P2 NOT SUPPORTED
#define ERR_7816_UNKNOWN						(708)
#define ERR_7816_COMMAND_NOT_ALLOWED			(709)

/*************************************************************
*            Interface (751 ~ 799)
*************************************************************/
#define ERR_GET_STORAGE_PATH					(781)
#define ERR_NOT_INPUT_PATH						(782)
#define ERR_INVALID_PATH_LENGTH					(783)
#define ERR_INVALID_PATH_RULE					(784)
#define ERR_NOT_EXIST_PATH						(785)
#define ERR_NOT_CREATE_SESS_PATH				(786)
#define ERR_INVALID_CURRENT_UNIX_TIME			(787)

/*************************************************************
*            Device Binding (800 ~ 819)
*************************************************************/
#define ERR_DEV_BIND_DATA_DELETE				(801)
#define ERR_DEV_BIND_DATA_DISABLE				(802)
#define ERR_DEV_ESN_REQUIRED					(803)
#define ERR_DEV_GET_CARD_INFO					(810)
#define ERR_DEV_BIND_NOT_INIT					(811)
#define ERR_DEV_BIND_SC_FAIL					(813) // S/C에 바인딩 정보 없음


/***************************************************************************/
/**                                                                       **/
/** TYPEDEFS AND STRUCTURES                                               **/
/**                                                                       **/
/***************************************************************************/

/*************************************************************
* 디버그 메시지 출력 레벨 정의
*************************************************************/
typedef enum
{
	MCAS_NONE,        /* lowest */
	MCAS_CRITICAL,
	MCAS_WARN,
	MCAS_DBG_ALL      /* highest */
}MCAS_DBG_LEVEL;

/*************************************************************
* TASK 관리를 위한 정의
*************************************************************/
typedef	UINT32	OS_DRV_TASK_ID;
typedef UINT32  OS_DRV_SEMAPHORE;

/*************************************************************
* Notification 함수의 전달 인자 정의 (Message 종류의 Type)
*************************************************************/
#ifndef MCAS_MSG_EN
#define MCAS_MSG_EN UINT16
#endif

/******************************************************
* MCAS <--> EPG Messages lists
*******************************************************/
#define EPG_MSG_PSI_SERVICE_START				(100)
#define EPG_MSG_PSI_PMT_UPDATE					(101)
#define EPG_MSG_PSI_SERVICE_STOP				(102)
#define EPG_MSG_PSI_NIT_UPDATE					(103)
#define EPG_MSG_PSI_CAT_INVALIDATE				(104)
#define EPG_MSG_PSI_CAT_UPDATE					(105)
#define EPG_MSG_SCARD_INFO						(106)
#define EPG_MSG_SERVICE_INFO					(107)

#define EPG_MSG_PIN_CODE_CHANGE					(109)
#define EPG_MSG_PIN_CODE_CHECK					(110)
#define EPG_MSG_MONITOR_ECM						(111)
#define EPG_MSG_MONITOR_EMM						(112)
#define EPG_MSG_ANOUNCE							(113)
#define MCAS_NOTIFY_MCAS_ERROR					(115)
#define EPG_MSG_DISPLAY_MESSAGE					(116)
#define EPG_MSG_CA_ID							(117)
#define EPG_MSG_TOKEN_BUY						(119)
#define EPG_MSG_TOKEN_CANCEL					(120)
#define EPG_MSG_CALLBACK						(121)
#define EPG_MSG_USER_INFO						(122)
#define EPG_MSG_SS_TOKEN						(123)
#define EPG_MSG_SS_ENTL							(124)
#define EPG_MSG_SS_PURCHASE						(125)	
#define EPG_MSG_CODEDOWN						(126)
#define EPG_MSG_SC_FPRINT						(127)
#define EPG_MSG_PE_ENTL_INFO					(128)
#define EPG_MSG_IPPV_PREVIEW					(129)
#define EPG_MSG_IPPV_PURCHASE					(130)
#define EPG_MSG_IPPV_PURCHASE_REPLY				(131)
#define EPG_MSG_IPPV_REFUND						(132)
#define EPG_MSG_IPPV_REFUND_REPLY				(133)
#define EPG_MSG_IPPV_DEBIT						(134)
#define EPG_MSG_OOB_EMM							(135)
#define EPG_MSG_SS_OOB_RESULT  					(136)
#define EPG_MSG_SS_PPV							(138)  // PPV 구매 이력

#ifdef	BYTE_PACK	//_	Byte Alignment
#define	BYTE_ALIGN		__packed
#else
#define	BYTE_ALIGN
#endif


/*************************************************************
* CAS 권한의 시간 정보 표시를 위한 Struct
*************************************************************/
typedef BYTE_ALIGN struct {
	UINT16	nYear;				// 년 표시. 숫자로 2006이면 2006년을 의미
	UINT16	nMonth;				// 월 표시. 숫자로 4이면 4월을 의미
	BYTE	nDay;				// 일 표시. 숫자로 16이면 16일을 의미
	BYTE	nHour;				// 시 표시. 숫자로 14면 오후 2시를 의미
	BYTE	nMin;				// 분 표시. 숫자로 50이면 50분 의미
	BYTE	nSec;				// 초 표시. 숫자로 13이면 13초 의미
} MCAS_GenericTime;

/*************************************************************
* 가입자 정보 표시를 위한 Struct
*************************************************************/
typedef BYTE_ALIGN struct {
	INT16	result;				// 구체체 값의 유효성
	BYTE	bScSerNo[12];		// 
	BYTE	ScType;
	INT16	ScVersion;
	INT16	AppletVersion;
	INT16	AppVersion;
	BYTE	abScNationality[3];
	BYTE	subcribe_model; // 20060605
} scard_info_st;

/*************************************************************
* 서비스 정보 표시를 위한 Struct
*************************************************************/
typedef BYTE_ALIGN struct {
	UINT16 es_pid;
	UINT16 es_ecm_pid;
} es_pid_st;

typedef BYTE_ALIGN struct {
	INT16 result;
	UINT16 view_id;
	UINT16 service_id;
	UINT16 service_status;
	UINT16 ecm_pid;
	es_pid_st es_pids[MAX_AV_PID];
}service_info_st;

/*************************************************************
* PIN 코드 정보를 위한 Struct
*************************************************************/
typedef BYTE_ALIGN struct {
	INT16 result;
	UINT16 service_id;
	BYTE pin_type;
	BYTE old_pin[8];
	BYTE new_pin[8];
}pin_st;

/*************************************************************
* Token 정보를 위한 Struct
*************************************************************/
typedef BYTE_ALIGN struct {
	INT16 result;
	UINT16 service_id;
	UINT16 event_id;
	BYTE action;	//구매, 구매 취소
	BYTE token_type;
	UINT16 amount;
	BYTE pin[8];
} token_trans_st;

/*************************************************************
* ECM/EMM Monitoring 정보를 위한 Struct
*************************************************************/
typedef BYTE_ALIGN struct {
	INT16 result;
	UINT16 pid;
	BYTE type;
	BYTE data[MAX_TS_LEN];
} mcas_monitor_st;

/*************************************************************
* CAS Status 정보를 위한 Struct
*************************************************************/
typedef BYTE_ALIGN struct {
	INT16 result;
	UINT16 view_id;
	UINT16 service_id;
	UINT16 service_flag;
} service_status_st;

/*************************************************************
* IPPV 구매 정보를 위한 Struct
*************************************************************/
typedef BYTE_ALIGN struct {
	INT16 result;
	BYTE type;			//구매, 구매 취소
	BYTE token_type;	// 위치 수정
	UINT16 service_id;
	UINT16 event_id;
	UINT16 token_amount;
	BYTE pin[8];
	UINT32 ctime;
}ppv_purchase_st;

/*************************************************************
* Text Message 를 위한 Struct
*************************************************************/
typedef BYTE_ALIGN struct {
	INT16 result;
	BYTE forced_flag;
	BYTE alarm_flag;
	BYTE text_length;
	BYTE *text;			//동적 메모리 할당 포인터. 사용후 별도로 해지해야 함
}mail_st;

/*************************************************************
* 가입자 정보를 위한 Struct
*************************************************************/
typedef BYTE_ALIGN struct {
	INT16 result;
	BYTE subscribe_flag;
	BYTE nation[3];
	MCAS_GenericTime birth_info;
}user_info_st;

/*************************************************************
* Callback 정보를 위한 Struct
*************************************************************/
typedef BYTE_ALIGN struct {
	UINT16 service_id;
    UINT16 event_id;
    UINT32 buy_time; 
    UINT16 total_limit;
    UINT16 customer_purse;
}callback_st;

/*************************************************************
* Entitlement 정보를 위한 Struct
*************************************************************/
typedef BYTE_ALIGN struct {
	UINT16 entl_id;					// 상품 ID
	MCAS_GenericTime start_time;	// 상품 시작시간
	MCAS_GenericTime end_time;		// 상품 종료시간
}entl_info_st;

/*************************************************************
* 구매 로그 정보를 위한 Struct
*************************************************************/
typedef BYTE_ALIGN struct {
	BYTE type;
	UINT16 service_id;
	UINT16 event_id;
	MCAS_GenericTime buy_time;
	UINT16 amount;
}purchase_log_st;

/*************************************************************
* Pre-Enablement 정보를 위한 Struct
*************************************************************/
typedef BYTE_ALIGN struct {
	INT16 result;
	INT32 pe_expire_time;
	INT32 pe_used_time;
}pe_info_st;

/*************************************************************
* 선불권 정보를 위한 Struct
*************************************************************/
typedef BYTE_ALIGN struct {
	UINT16 amount;
	BYTE reserved[6];
}amount_st;

typedef struct {
	UINT16 token_type;
	UINT16 entl_id;
	BYTE token_sn[6];
	BYTE reserved[2];
	union {
		MCAS_GenericTime start_time;
		amount_st amount;
	} start_or_amount;
	MCAS_GenericTime end_or_lifetime;
	UINT32 used_time;
}token_info_st;

/*************************************************************
* IPPV 상품 정보를 위한 Struct
*************************************************************/
typedef BYTE_ALIGN struct {
	UINT16 status;
	UINT16 service_id;
	UINT16 event_id;
	UINT32 time;
	UINT16 total_limit;
	UINT16 customer_purse;
}ppv_info_st;

/*************************************************************
* SXC 버전 정보를 위한 Struct
*************************************************************/
typedef struct _SXC_VERSION_T {
	UINT16 target_id;
	DCBYTE version_no[16];
	DCBYTE is_sw_desc;
	DCBYTE svc_type; 
	DCBYTE ca_mode;
	DCBYTE trans_mode;
} SXC_VERSION_T;

/***************************************************************************/
/**                                                                       **/
/**  EXPORT FUNCTIONS                                                     **/
/**                                                                       **/
/***************************************************************************/

/**
*	@brief		CAS로 부터 변경된 상태 정보를 수신하기 위한 Notification 함수 Prototype
*/
typedef VOID(* MCAS_NOTIFICATION) (BYTE opcode, MCAS_MSG_EN mcas_msg, BYTE *msg, UINT32 msg_length);

/**
*	@fn 		MCAS_NotificationRegister
*	@brief		CAS에 Notification 콜백함수 등록
*	@param[in]	fnCallback		Notification 함수 포인터
*/
VOID   MCAS_NotificationRegister (MCAS_NOTIFICATION fnCallback);

/**
*	@fn 		MCAS_Init
*	@brief		CAS 초기화 함수
*	@param[in]	DbgLevel			출력될 Debug Msg 레벨
*/
UINT16   MCAS_Init( MCAS_DBG_LEVEL DbgLevel );

/**
*	@fn 		MCAS_Destroy
*	@brief		CAS 종료 함수
*/
VOID   MCAS_Destroy(VOID);

/**
*	@fn 		MCAS_GetStatus
*	@brief		CAS에 메세지 전달을 위한 함수
*	@param[in]	opcode				요청 & 전달
*	@param[in]	msg					메시지 종류
*	@param[in]	pvParams			메시지 데이터
*	@param[in]	ulParamLength		메시지 데이터 길이
*/
BOOL   MCAS_GetStatus( BYTE opcode, MCAS_MSG_EN msg, BYTE *pvParams, UINT32 ulParamLength );

/**
*	@fn 			MCAS_GetCmd
*	@brief			CAS 인증시 메시지 전달을 위한 함수
*	@param[in]		stb_id			단말의 STB ID
*	@param[in]		cmdtype			메시지 종류
*	@param[in]		opcode			요청 & 전달
*	@param[in, out]	stbcmd			SKB 인증 프로토콜의 6번째 바이트
*	@param[in, out]	cmd1			SKB 인증 프로토콜의 Command Type1
*	@param[in, out]	cmd2			SKB 인증 프로토콜의 Command Type2
*	@param[in, out]	len				skbmsg의 길이
*	@param[in, out]	skbmsg			메시지 데이터
*/
INT16  MCAS_GetCmd( BYTE *stb_id, UINT16 cmdtype, BYTE opcode, BYTE *stbcmd, BYTE *cmd1, BYTE *cmd2, UINT16 *len, BYTE *skbmsg );

/**
*	@fn 			SPA_InteractCmd
*	@brief			CAS 인증시 메시지 전달을 위한 신규 함수
*					(MCAS_GetCmd를 대체하여 신규 인증에서 사용)
*	@param[in]		stbid			단말의 STB ID
*	@param[in]		cmdtype			메시지 종류
*	@param[in]		opcode			요청 & 전달
*	@param[in, out]	cmd1			SKB 인증 프로토콜의 Command Type1
*	@param[in, out]	cmd2			SKB 인증 프로토콜의 Command Type2
*	@param[in]		reason			요청 사유
*	@param[in]		result			SCS의 처리 결과
*	@param[in, out]	skblen			skbmsg의 길이
*	@param[in, out]	skbmsg			메시지 데이터
*	@param[in, out]	certlen			certmsg의 길이
*	@param[in, out]	certmsg			메시지 데이터
*/
INT32 SPA_InteractCmd( BYTE *stbid, UINT16 cmdtype, BYTE opcode, BYTE *cmd1, BYTE *cmd2, BYTE reason, BYTE result, UINT16 *skblen, BYTE *skbmsg, UINT16 *certlen, BYTE *certmsg );

/**
*	@fn 			MCAS_OnScreen
*	@brief			CAS를 통해 On-Screen 구매를 위한 함수
*	@param[in]		stb_id			단말의 STB ID
*	@param[in]		cmdtype			메시지 종류	(CMD_BUY_PRODUCT)
*	@param[in]		opcode			요청 & 전달
*	@param[in]		req_cmd			구매하려는 채널의 상품 개수와 상품명 및 유효기간
*	@param[in, out]	cmd1			SKB 인증 프로토콜의 Command Type1
*	@param[in, out]	cmd2			SKB 인증 프로토콜의 Command Type2
*	@param[in, out]	len				skbmsg의 길이
*	@param[in, out]	skbmsg			메시지 데이터
*/
INT16  MCAS_OnScreen(byte *stb_id, uint16 cmdtype, byte opcode, byte *req_cmd, byte *cmd1, byte *cmd2, uint16 *len, byte *skbmsg );


/**
*	@fn 		MCAS_CasTs
*	@brief		필터링된 ECM/EMM을 CAS 모듈로 전달하기 위한 함수
*	@param[in]	emm_ecm				ECM / EMM 데이터
*	@param[in]	view_id 			ECM 인 경우 해당 화면의 View ID (Service Start 시의 View ID)
*/
INT32 MCAS_CasTs(BYTE *emm_ecm, UINT16 view_id);

/**
*	@fn 		MCAS_DescrambleTS
*	@brief		S/W Descrambler
*	@param[in]	scrambled			암호화된 A/V
*	@param[out]	descrambled			복호화된 A/V
*	@param[in]	tslen				Scrambled로 전달되는 TS 길이
*	@param[in]	view_id 			해당 화면의 View ID (Service Start 시의 View ID)
*/
INT32 MCAS_DescrambleTS( BYTE *scrambled, BYTE *descrambled, INT32 tslen, UINT16 view_id );

/**
*	@fn 		MCAS_DescrambleTS_CH
*	@brief		S/W Descrambler
*	@param[in]	scrambled			암호화된 A/V
*	@param[out]	descrambled			복호화된 A/V
*	@param[in]	tslen				Scrambled로 전달되는 TS 길이
*	@param[in]	service_id			해당 채널의 Service ID (Service Start 시의 Service ID)
*	@param[in]	view_id 			해당 화면의 View ID (Service Start 시의 View ID)
*/
INT32 MCAS_DescrambleTS_CH( BYTE *scrambled, BYTE *descrambled, INT32 tslen, UINT16 service_id, UINT16 view_id );

/**
 *	@fn 		SXCF_GetVersion
 *	@brief		SXC 라이브러리 버전 정보를 전달하는 함수
 *	@param[in]	pVersion	pointer of struct SXC_VERSION_T
 */
INT32 SXCF_GetVersion( SXC_VERSION_T * pVersion );

#ifdef __cplusplus
}
#endif 

#endif

