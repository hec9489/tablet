/*
-----------------------------------------------------------------------------
******************************* C HEADER FILE *******************************
-----------------------------------------------------------------------------
**                                                                         **
** PROJECT      : CAS Project for Device                                   **
** FILENAME     : dcsxc_wrapper.h                                          **
** DATE         : 2020-07-20                                               **
**                                                                         **
*****************************************************************************
**                                                                         **
** Copyright (c) 2020, DigiCAPS Inc.    All rights reserved.               **
**                                                                         **
*****************************************************************************/

#ifndef _dcsxc_wrapper_h
#define _dcsxc_wrapper_h

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "mcas_types.h"

#if !defined(__gmtime64_t_DEFINED)
typedef int64_t gmtime64_t;            // unix time (UTC)
#define __gmtime64_t_DEFINED
#endif


/*************************************************************
* CA Client Error Code (wrapper)
*************************************************************/
#define SXC_CA_CIR_OK                              (0)

#define SXC_CA_CIR_ERROR                           (-1)
#define SXC_CA_CIR_INVALID_ALGO                    (-2)
#define SXC_CA_CIR_INVALID_CB_TYPE                 (-3)
#define SXC_CA_CIR_ARGUMENTS_BAD                   (-4)
#define SXC_CA_CIR_CB                              (-5)
#define SXC_CA_CIR_INVALID_INFO                    (-6)
#define SXC_CA_CIR_RO_INFO                         (-7)
#define SXC_CA_CIR_INVALID_ENC                     (-8)
#define SXC_CA_CIR_BUFFER_TOO_SMALL                (-9)
#define SXC_CA_CIR_INVALID_KEY                     (-10)
#define SXC_CA_CIR_INVALID_OPTION                  (-11)
#define SXC_CA_CIR_INVALID_PAD                     (-12)
#define SXC_CA_CIR_INVALID_CERTYPE                 (-13)
#define SXC_CA_CIR_NOT_FOUND                       (-14)
#define SXC_CA_CIR_INVALID_EXT                     (-15)
#define SXC_CA_CIR_NOT_EXIST                       (-16)
#define SXC_CA_CIR_NOT_SUCCESSFUL_ORES             (-17)
#define SXC_CA_CIR_INVALID_INPUT_LEN               (-18)
#define SXC_CA_CIR_NOT_SUPPORTED                   (-19)

#define SXC_CA_CIR_LIB_NOT_INITIALIZED             (-101)
#define SXC_CA_CIR_INVALID_HANDLE                  (-102)
#define SXC_CA_CIR_INVALID_DATA                    (-103)
#define SXC_CA_CIR_INVALID_CERT                    (-104)
#define SXC_CA_CIR_INVALID_KEY_PAIR                (-105)
#define SXC_CA_CIR_VERIFY_FAIL                     (-106)
#define SXC_CA_CIR_CERT_EXT_KEYUSAGE               (-107)
#define SXC_CA_CIR_CERT_REVOKED                    (-108)
#define SXC_CA_CIR_CERT_EXPIRED                    (-109)

#define SXC_CA_CIR_CONNECT_FAIL                    (-121)
#define SXC_CA_CIR_CERT_ISSUE_FAIL                 (-122)

#define SXC_CA_CIR_FILE_OPEN_FAIL                  (-131)
#define SXC_CA_CIR_FILE_READ_FAIL                  (-132)
#define SXC_CA_CIR_FILE_WRITE_FAIL                 (-133)
#define SXC_CA_CIR_FILE_ENC_FAIL                   (-134)

#define SXC_CA_CIR_INSTALLCERT_INVALID_DATA        (-300)
#define SXC_CA_CIR_INSTALLCERT_INVALID_NONCE       (-301)
#define SXC_CA_CIR_INSTALLCERT_INVALID_VERSION     (-302)
#define SXC_CA_CIR_INSTALLCERT_INVALID_CERT        (-303)

#define SXC_CA_CIR_INSTALLCERT_EMPTY_NONCE         (-311)
#define SXC_CA_CIR_INSTALLCERT_EMPTY_VERSION       (-312)
#define SXC_CA_CIR_INSTALLCERT_EMPTY_CERT          (-313)

#define SXC_CA_CIR_INSTALLCERT_STROAGE_ERROR       (-321)


/*************************************************************
* CA Client Define (wrapper)
* SXCW_dci_CertVerify() 함수의 flags 파라미터에 사용
*************************************************************/
#define SXC_CA_CIC_CERT_VERIFY_SIGN                (1)


/**
*    @fn                SXCW_dci_CertVerify
*    @brief            인증서 유효성 검사를 위한 함수 (wrapper function of dci_CertVerify())
*    @param[in]        now             현재 시간(millisecond)
*    @param[in]        flags            SXC_CA_CIC_CERT_VERIFY_SIGN(1) 사용
*    @return                                 CA Error Code (ref. SXC_CA_CIR_*)
*/
INT32 SXCW_dci_CertVerify(gmtime64_t now, INT32 flags);

/**
*    @fn                SXCW_dci_DirectGetErrorMessage
*    @brief            에러 상세 메시지를 출력하기 위한 함수 (wrapper function of dci_DirectGetErrorMessage())
*                          SXCW_dci_CertVerify() 함수의 Error Code 값이 SXC_CA_CIR_OK가 아닌 경우에 호출
*    @return                                 Error Message
*/
CHAR* SXCW_dci_DirectGetErrorMessage(VOID);

/**
*    @fn                SXCW_dci_DirectClearErrorMessage
*    @brief            에러 상세 메시지의 버퍼를 Clear 하기 위한 함수 (wrapper function of dci_DirectClearErrorMessage())
*                          SXCW_dci_CertVerify() 함수의 Error Code 값이 SXC_CA_CIR_OK가 아닌 경우에 호출
*                          SXCW_dci_DirectGetErrorMessage() 함수 호출한 이후에 호출
*/
VOID SXCW_dci_DirectClearErrorMessage(VOID);

#ifdef __cplusplus
}
#endif

#endif /* _dcsxc_wrapper_h */
