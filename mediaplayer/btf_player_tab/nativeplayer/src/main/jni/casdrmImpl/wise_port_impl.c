#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <systemproperty.h>
#include <ctype.h>

#include "drmp_port.h"
#include "wise_port.h"
#include "drmp_Command.h"

#include "util_drm_logmgr.h"

//extern int errno;
/**
 * internal function declartion/definition
 */
static int 		check_file_open_mode	( char *mode, char v);
static void 	remove_folder_recursive	( char *dir, int bdelete_root);

#define WPI_E_NOT_EXIST 	-28

#ifndef USE_INLINE_DEFINE

/*
 *======================================== String Handling Function ======================================
 */
WPI_CHAR	*wpi_strtok ( WPI_CHAR *str, const WPI_CHAR *delimit)
{
	return  strtok ( str, delimit );
}

WPI_INT32 wpi_atoi ( const WPI_CHAR *str)
{
	if( str)
		return atoi( str);

	return 0;
}

WPI_DOUBLE wpi_atof ( const WPI_CHAR *str)
{
	if( str)
		return atof(str);

	return 0;
}

WPI_INT32 wpi_sprintf ( WPI_CHAR *buffer, const WPI_CHAR *format, ...)
{
	int 			n=0;
	va_list  		ap;

	va_start ( ap, format);
	n = vsprintf ( buffer, format, ap);
	va_end (ap);

	return n;
}

/*
 *======================================== Memory Handling Functions ======================================
 */

WPI_VOID* wpi_malloc ( WPI_INT32 size)
{
	if( size > 0)
		return malloc( size);

	return 0;
}

WPI_VOID wpi_free ( WPI_VOID *ptr)
{
	if( ptr)
	{
		free(ptr);
		ptr = NULL;
	}
}

WPI_VOID* wpi_realloc ( WPI_VOID *block, WPI_INT32 size)
{
	if( block && size > 0)
		return realloc( block,size);
	else if( !block && size > 0)
		return malloc( size);
	else
	{
		free( block);
		return 0;
	}
}

/*
 *========================================File I/O Handling Functions ======================================
 */

WPI_INT32 wpi_mkdir ( WPI_CHAR *name)
{
	if( name )
		return mkdir ( name, 0755);

	return -1;
}

WPI_INT32 wpi_rmdir ( WPI_CHAR *name)
{
	if(name)
		return rmdir( name);

	return -1;
}

WPI_VOID wpi_close(WPI_INT32 handle)
{
	close(handle);
}

WPI_INT32 wpi_remove ( WPI_CHAR *name)
{
	return remove(name);
}

WPI_INT32 wpi_read ( WPI_INT32 handle, WPI_VOID * pBuf, WPI_INT32 size)
{
	return read( handle, pBuf, size);
}

WPI_INT32 wpi_write ( WPI_INT32 handle, WPI_VOID *pBuf, WPI_INT32 size)
{
	return write( handle, pBuf, size);
}

WPI_INT32 wpi_flush ( WPI_INT32 handle)
{
	return fsync(handle);	//ensure
}

WPI_INT32 wpi_rename ( WPI_CHAR *oldName, WPI_CHAR *newName)
{
	if( oldName && newName)
		return rename( oldName, newName);

	return -1;
}

WPI_INT32 wpi_stat ( WPI_CHAR *filename, struct stat *fileinfo)
{
	if( filename && fileinfo)
		return stat( filename, fileinfo);

	return -1;
}
#endif // USE_INLINE_DEFINE

WPI_INT32 wpi_mkdir_p ( WPI_CHAR *name)
{
	int 			status = 0;
	char 		*p,
				*q,
				*path 	= strdup(name);
	struct stat 	st = {0,};

	q = p = 0;
	do {
		q = p;

		if( (status=mkdir(path, 0755)) < 0) 	//ensure
		{
			if( errno == ENOENT)
			{
				p = strrchr( path, '/');
				if(p)
					*p = '\0';
				if(q)
					*q = '/';
			}
			else if( errno == EEXIST)
			{
				if( q && *q !='/')
					*q = '/';
				else
					break;
			}
		}
		else
		{
			if(q)
				*q = '/';
			else
				break;
		}
	} while(1);

	free(path);

	if( stat( name, &st) == 0)
	{
		return 0;
	}
	else
	{
		return -1;
	}
}

WPI_INT32 wpi_open ( WPI_CHAR *name, WPI_INT32 mode)
{
	int ret;

	if (mode & O_CREAT)
	{
		ret = open(name, mode, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
	}
	else
	{
		ret = open(name, mode);
	}
	return ret;
}

WPI_INT32 wpi_open2( WPI_CHAR *name, WPI_CHAR *mode)
{
	int 		_mode = 0, _fd = 0;

	if( !name)
		return -1;

	if(!mode)
		return wpi_open( name, 0); // O_RDONLY

	if( check_file_open_mode( mode, 'r') && check_file_open_mode( mode, 'w'))
	{
		_mode = O_RDWR;
		DEBUG_PRINT ( fprintf ( stderr, "WISE_PORT_IMPL.C> wpi_open2 read write \n") );
	}
	else if( check_file_open_mode( mode, 'r'))
	{
		_mode = O_RDONLY;
		DEBUG_PRINT ( fprintf ( stderr, "WISE_PORT_IMPL.C> wpi_open2 read only \n") );
	}
	else if( check_file_open_mode( mode, 'w'))
	{
		_mode = O_WRONLY;
		DEBUG_PRINT ( fprintf ( stderr, "WISE_PORT_IMPL.C> wpi_open2 write only \n") );
	}
	else if( check_file_open_mode( mode, 'a'))
	{
		_mode = (O_APPEND | O_WRONLY);
		DEBUG_PRINT ( fprintf ( stderr, "WISE_PORT_IMPL.C> wpi_open2 append \n") );
	}

	if( check_file_open_mode( mode, '+'))
	{
		_mode |= O_TRUNC;
		DEBUG_PRINT ( fprintf ( stderr, "WISE_PORT_IMPL.C> wpi_open2 trunc \n") );
	}

	if ( 0 == wpi_exist ( name ) )
	{
		if ( O_RDONLY == _mode )
		{
			DEBUG_PRINT ( fprintf ( stderr, "WISE_PORT_IMPL.C> wpi_open2 File Not Exist \n") );
			return WPI_E_NOT_EXIST;
		}
		else
		{
			_mode |=  O_CREAT;
		}
	}
	else
	{
		if ( O_WRONLY == _mode )
		{
			DEBUG_PRINT ( fprintf ( stderr, "WISE_PORT_IMPL.C> wpi_open2 Remove and create in write mode \n") );
			wpi_remove ( name );
			_mode |=  O_CREAT;
		}
	}
	_fd = wpi_open( name, _mode);

	DEBUG_PRINT ( fprintf ( stderr, "WISE_PORT_IMPL.C> wpi_open2 [ %d ] \n", _fd ) );

	return _fd;
}

WPI_INT32 wpi_close2 (WPI_INT32 handle)
{
	if( handle > 0)
	{
		wpi_close(handle);
		return 0;
	}

	return -1;
}

WPI_UINT32 wpi_size (WPI_CHAR *name)
{
	struct stat  st = {0,};

	if( stat( name, &st) == 0)
		return st.st_size;

	return -1;
}

WPI_INT32 wpi_seek ( WPI_INT32 handle, WPI_INT32 offset, WPI_INT32 origin)
{
	return lseek( handle, offset, origin);
}

WPI_INT32 wpi_tell ( WPI_INT32 handle)
{
	return lseek( handle, 0, SEEK_CUR);
}

WPI_BOOL wpi_exist ( WPI_CHAR *fileName)
{
	struct stat  st = {0,};

	if( fileName && (stat( fileName, &st) == 0))
		return 1;

	return 0;
}

WPI_INT32 wpi_remove_rf ( WPI_CHAR *path)
{
	struct stat 	st = {0,};

	if( path && (stat(path, &st) == 0))
	{
		remove_folder_recursive( strdup(path), 0);
		bzero( &st, sizeof(st));
	}
	return 0;
}

WPI_INT32 wpi_list (WPI_CHAR *dir, WPI_CHAR *buffer, WPI_INT32 size)
{
	struct dirent		*dentry = NULL;
	DIR 				*pdir = NULL;
	struct stat		st = {0,};
	char				fname[256]={0,};
	int 				pos = 0,
					count =0;

	if( !dir || !buffer || size <= 0)
	{
		return -1;
	}

	if( !(pdir = opendir(dir)))
		return -1;

	dentry = readdir( pdir);

	while( dentry  && pos < (size -1))
	{
		if(  strcmp( dentry->d_name, ".") && strcmp(dentry->d_name, "..") )
		{
			bzero( &st, sizeof(st));
			bzero( fname, sizeof(fname));
#ifdef WPI_LIST_FULL_PATH
			sprintf( fname, "%s/%s", dir, dentry->d_name);
#else
			sprintf( fname, "%s", dentry->d_name);
#endif
			memcpy( buffer + pos, fname, strlen(fname) +1);
			pos += (strlen(fname)+1);
			count++;
		}
		dentry = readdir(pdir);
	}
	if( count > 0)
	{
		buffer[pos] = '\0';
		return count;
	}
	else
	{
		return -1;
	}
}


static void remove_folder_recursive ( char *dir, int bdelete_root)
{
	struct dirent		*dentry = NULL;
	DIR 				*pdir = NULL;
	struct stat		st = {0,};
	char 			fname[256]={0,};

	//printf("called .find_update_content_version\n");
	if( dir == NULL || !strlen(dir))
		return;

	if( !(pdir = opendir( dir)))
	{
		remove( dir);

		if( bdelete_root)
			free(dir);

		return ;
	}
	dentry = readdir(pdir);
	do {
		if( !dentry )
			break;

		if(  strcmp( dentry->d_name, ".") && strcmp(dentry->d_name, "..") )
		{
			memset( &st, 0, sizeof(st));
			memset( fname, 0, sizeof(fname));
			sprintf( fname, "%s/%s", dir, dentry->d_name);
			//printf("target grid name (%s)\n", fname);
			if( !stat( fname, &st))
			{
				if( remove( fname)  < 0)
					remove_folder_recursive( strdup(fname), 1);
#if 0
				if( S_ISDIR(st.st_mode) && !S_ISLNK(st.st_mode))
				{
					remove_folder_recursive( strdup(fname), TRUE);
				}
				else if( S_ISREG(st.st_mode) || S_ISLNK(st.st_mode) )
				{
					unlink( fname);
				}
#endif
			}
		}
		dentry = readdir(pdir);
	}while( dentry);

	closedir(pdir);

	if( bdelete_root)
	{
		remove( dir);
		free( dir);
	}
}

static int 	check_file_open_mode ( char *mode, char v)
{
	char	*p = mode;
	int i32C = 0;

	while( '\0' != *p )
	{
		if( *p == v)
		{
			DEBUG_PRINT ( fprintf ( stderr, "WISE_PORT_IMPL.C> wpi_open2 mode Sel [ %c ] [ %c ] [%d ]\n", *p, v, i32C ) );
			return 1;
		}
		p++;
		i32C ++;
	}
	return 0;
}

WPI_INT32 wpi_GetContentWithFormat (
												WPI_CHAR *fmt_data,
												WPI_CHAR *DesPtr,
												WPI_CHAR *FormatSearch,
												WPI_CHAR delimt )
{
	WPI_CHAR 	*cStrPtr	= NULL;
	WPI_INT32	i32len	= 0;

	cStrPtr = strstr( fmt_data,FormatSearch);
	if ( cStrPtr != NULL )
	{
		WPI_CHAR	*cDelim_found_ptr	= NULL;
		WPI_INT32	i32nme_len			= 0;

		i32nme_len = strlen(FormatSearch);
		cStrPtr += i32nme_len;
		cDelim_found_ptr = strchr( cStrPtr, delimt );

		if (cDelim_found_ptr != NULL )
		{
			i32len = cDelim_found_ptr - cStrPtr;
			if ( i32len > 0 )
			{
				strncpy (DesPtr, cStrPtr, i32len);
				DesPtr[i32len] = '\0';
				return 1;
			}
		}
		else
		{
			i32len = strchr( cStrPtr, '\0' ) - cStrPtr;
			if ( i32len > 0 )
			{
				strncpy (DesPtr, cStrPtr, i32len);
				DesPtr[i32len] = '\0';
				return 1;
			}
		}
	}

	return 0;
}

WPI_INT32 wpi_GetFileOpenMode ( DC_FHANDLE_ACCESS_MODE mode )
{
	switch ( mode )
	{
		case DC_FILE_OPEN_WRTRUNC:
		{
			return O_TRUNC | O_CREAT |O_RDWR;//WRONLY;
		}
		break;

		case DC_FILE_OPEN_RDWR:
		{
			return O_RDWR;
		}
		break;

		case DC_FILE_OPEN_RDONLY:
		{
			return O_RDONLY;
		}
		break;

		case DC_FILE_OPEN_WRONLY:
		{
			return O_WRONLY | O_CREAT;
		}
		break;

		case DC_FILE_OPEN_APPEND:
		{
			return O_APPEND|O_RDWR;
		}
		break;

		default:
			return -1;
	}
	return -1;
}

#define MAC_CHECK_FILE "/mnt/config/_test_mac"
//get STB-MAC-ADDRESS
int wpi_get_interface_info( const char *eth, int infotype, char *rtnval, int fmttype ) {
	int i, fd, len =0;
	struct ifreq ifrq;
	struct sockaddr_in *sin;
	struct stat st = {0,};
	long long convertMacValues = 0;

	if(infotype == SIOCGIFHWADDR) {
		// TODO : jung2604 : WAN의 MAC을 제공하기 위해서 기존 방식에서 cas에서 읽어오는 방식과 동일하게 사용한다.
		char readtBuff[40];

		memset(readtBuff, 0, 40);

		get_systemproperty(PROPERTY__STB_ID, readtBuff, 39);

		if(6 == sscanf(readtBuff, "%x:%x:%x:%x:%x:%x",
					   &rtnval[0], &rtnval[1], &rtnval[2],
					   &rtnval[3], &rtnval[4], &rtnval[5])) {
			return 0;
		}
		return -1;
	}

	return get_interface_info(eth, infotype, rtnval);
}
#define CASDRM_TEST_MODE 	"/mnt/config/_skcasdrm_test"
int wpi_is_test_mode(void)
{
	/*struct stat st = {0,};
	
	if( stat(CASDRM_TEST_MODE, &st) == 0) // test mode
		return 1;
	else
		return 0;
		*/

	return 1;
}
int wpi_GetScsHostDetailes (char *filename, char *ScsHostAddr, unsigned short	 *pi32scsPort, signed short *pi8ScsVertion )
{
	FILE *fp = NULL;
	char xbuf[256];  
	int i32LocalFlag = 0;

	fp = fopen( filename, "r");
	if (!fp) 
	{
		fprintf(stderr, "WISE_PORT_IMPL.C %d> OOPS:	Can't open the file[%s]!!!!!!!!!!!!\n", __LINE__, filename );
	}
	while (fgets(xbuf, sizeof(xbuf), fp)) 
	{
		if (xbuf[0] == '#' || xbuf[0] == ' ' ||xbuf[0] == '\n' || xbuf[0] == '\r' ||strlen(xbuf) < 6) 
		{
			memset(xbuf, 0, sizeof(xbuf));
			continue;
		}
		
		if (xbuf[strlen(xbuf)-1] == '\n' || xbuf[strlen(xbuf)-1] == '\r')
		{
			xbuf[strlen(xbuf)-1] = '\0';
		}

		if(strstr(xbuf, "ScsHost"))
		{
			sscanf ( xbuf, "ScsHost %s", ScsHostAddr );
			i32LocalFlag |=(1<<0);			
		}
		else if (strstr(xbuf, "ScsPort"))
		{
			sscanf ( xbuf, "ScsPort %d", pi32scsPort );		
			i32LocalFlag |=(1<<1);			
		}
		else if (strstr(xbuf, "ScsVer"))
		{
			i32LocalFlag |=(1<<2);					
			sscanf ( xbuf, "ScsVer %d", pi8ScsVertion );		
		}
		if ( i32LocalFlag == 7 )
		{
			break;
		}
	}
	fclose(fp); 			

	fprintf(stderr, "WISE_PORT_IMPL.C %d> The SCSHOST is:[%s] port No: [%d] Version [%d] \n", __LINE__, ScsHostAddr, *pi32scsPort, (int) *pi8ScsVertion   );	


	return 0;
}
