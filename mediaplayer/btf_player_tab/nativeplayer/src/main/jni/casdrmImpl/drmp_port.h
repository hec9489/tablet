#ifndef DRMP_PORT_H_
#define DRMP_PORT_H_

#include "drmp_basictype.h"
#include "wise_port.h"

#ifdef __cplusplus
extern "C" {
#endif

#define READ_BUF_SIZE 	80
#define DEVICE_INFO 		"STBID="

#if 0
typedef enum  tagDC
{
	SKDRM_SEEK_SET,
	SKDRM_SEEK_CUR,
	SKDRM_SEEK_END
} DC_FHANDLE_SET_POSITION ;
#else
typedef enum {
    DC_SEEK_SET                = 0x0000,
    DC_SEEK_CUR                = 0x0001,
    DC_SEEK_END                = 0x0002
} DC_FHANDLE_SET_POSITION;
#endif

#if 0
typedef enum tagDC_FAM
{
	SKDRM_O_RDONLY,
	SKDRM_O_WRONLY,
	SKDRM_O_RDWR,
	SKDRM_O_APPEND,
	SKDRM_O_ASYNC,
	SKDRM_O_CREAT,
	SKDRM_O_DIRECT,
	SKDRM_O_DIRECTORY,
	SKDRM_O_EXCL,
	SKDRM_O_LARGEFILE,
	SKDRM_O_NOATIME,
	SKDRM_O_NOCTTY,
	SKDRM_O_NOFOLLOW,
	SKDRM_O_NONBLOCK,
	SKDRM_O_NDELAY,
	SKDRM_O_SYNC,
	SKDRM_O_TRUNC,
} DC_FHANDLE_ACCESS_MODE;
#else
typedef enum tagDC_FAM
{
	DC_FILE_OPEN_WRTRUNC       = 0x0001,
	DC_FILE_OPEN_RDWR          	= 0x0002,
	DC_FILE_OPEN_RDONLY       	= 0x0003,
	DC_FILE_OPEN_WRONLY        	= 0x0004,
	DC_FILE_OPEN_APPEND     	= 0x0005
} DC_FHANDLE_ACCESS_MODE; 
#endif /* BOGUS */

/*
 * String Handling Function 
 */
DC_CHAR	*pli_strtok( DC_CHAR *str, const DC_CHAR *delimit);
DC_INT32 pli_atoi( const DC_CHAR *str);
DC_INT32 pli_atof( const DC_CHAR *str);
DC_INT32 pli_sprintf( DC_CHAR *buffer, const DC_CHAR *format, ...);

/**
 * File I/O Handling Functions 
 */
DC_INT32 pli_fsCreateDir( DC_CHAR *name);
DC_INT32 pli_fsOpen( DC_CHAR *name, DC_FHANDLE_ACCESS_MODE mode);
DC_VOID pli_fsClose(DC_INT32 fhandle);
DC_INT32 pli_fsRemove( DC_CHAR *name);
DC_INT32 pli_fsRead( DC_INT32 handle, DC_VOID * pBuf, DC_INT32 size);
DC_INT32 pli_fsWrite( DC_INT32 handle, DC_VOID *pBuf, DC_INT32 size);
DC_INT32 pli_fsSize(DC_CHAR *name);
DC_INT32 pli_fsSeek( DC_INT32 handle, DC_INT32 offset, DC_FHANDLE_SET_POSITION origin);
DC_INT32 pli_fsTell( DC_INT32 handle);
DC_INT32 pli_fsFlush( DC_INT32 handle);
DC_BOOL pli_fsIsExist( DC_PCHAR fileName);
DC_INT32 pli_fsRename( DC_CHAR *oldName, DC_CHAR *newName);
DC_INT32 pli_fsRemoveAll( DC_CHAR *path);

/**
 * Memory Handling Functions
 */
DC_VOID* pli_malloc( DC_INT32 size);
DC_VOID pli_free( DC_VOID *ptr);
DC_VOID* pli_realloc( DC_VOID *block, DC_INT32 size);

/**
 * Time Handling Function
 */
DC_INT64 pli_time( DC_VOID);
/**
 * Random Number Function
 */
DC_INT32 pli_randBytes( DC_BYTE *buf, DC_INT32 num);
/**
 * Device Info Funtions
 */
DC_INT32 pli_getDeviceInfo( DC_BYTE *type, DC_BYTE *value);
DC_INT32 pli_getDrmSecureDirPath( DC_CHAR *path);
DC_INT32 pli_getDrmSecureDirPath2( DC_CHAR *path);

/**
 * Socket Function 
 */
DC_INT32 pli_sOpen( DC_INT32 type);
DC_INT32 pli_sClose( DC_INT32 s);

DC_INT32 pli_sConnect( DC_INT32 s, DC_UINT16 port, DC_UINT32 addr);
DC_INT32 pli_sRecv( DC_INT32 s, DC_BYTE *buf, DC_INT32 len);
DC_INT32 pli_sSend( DC_INT32 s, DC_BYTE *buf, DC_INT32 len);
DC_INT32 pli_sGetHostByName( DC_CHAR *name);

/**
 * Time �Լ�
 */
DC_INT32 pli_getTickCount(void);

#ifdef __cplusplus
}
#endif

#endif /*DRMP_PORT_H_*/

