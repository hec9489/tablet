/*
 * scs-trigger-handler.c
 *
 *  Created on: 2008. 10. 20
 *      Author: javaos
 */


#include "scs-protocol.h"
#include "encBlowFish.h"


#include "mcas_port.h"
#include "utile_socket.h"

// TODO : jung2604 : 추가
#include "dmux_define.h"

#include "util_drm_logmgr.h"

//extern int errno;

static tpacket	_send;// = { 0, };
static tpacket	_recv;// = { 0, };

static struct sockaddr_in sin = {0,};
static struct sockaddr_in pin = {0,};
static struct hostent *hp;




/* Command Code */

#define CMD_STB_REG                         0x01
#define CMD_ADULT_PASS                      0x05
#define CMD_ADULT_PASS2                     0xB5
#define CMD_EPG_CHECK                       0x07
#define CMD_STB_REG_CONFIRM                 0x0A
#define CMD_XHG_ADULTPASS                   0x31
#define CMD_XHG_ADULTPASS2                  0xB6
#define CMD_QUERY2                          0x34
#define CMD_EPGSVR_INFO                     0x3A
#define CMD_CTS_INFO2                       0x40
#define CMD_CTS_INFO4                       0x60
#define CMD_CDR_LOG2                        0x41
#define CMD_DIS_CONNECT                     0xEE
#define CMD_KID_PASS                        0xB1
#define CMD_XHG_KIDPASS                     0xB2
#define CMD_CASH_INFO                       0xB3
#define CMD_CONFIRM_CASH                    0xB4
#define CMD_PURCHASE_PASS					0xB7
#define CMD_XGH_PURCHASEPASS				0xB8
#define CMD_OTP								0xB9
#define CMD_LICENSE_INFO					0x90	//RDRM
/* Command Code */

/**
 * @param hostname scs host name
 * @param port scs port number
 * @return scs socket descriptor
 */
int scs_init(char *hostname, int port)
{
	int 		sd;
	hp = NULL;

	// 2012.02.10 namsj gethostbyname() 이 실패할 경우가 있다.
	// 인터넷에 찾아 보면 대략 실제 호스트가 많을 경우 그럴 때가 있다는 것 같다.
	// gethostbyname2()를 사용하게 되면 잘 얻어 진다.
	//if( (hp=gethostbyname(hostname)) == NULL)
	if( (hp=gethostbyname2(hostname, AF_INET)) == NULL)
	{
		return -1;
	}
	memset( &pin, 0, sizeof(pin));
	pin.sin_family = AF_INET;
	pin.sin_addr.s_addr = ((struct in_addr*)(hp->h_addr))->s_addr;
	pin.sin_port = htons(port);

	if( (sd=socket(AF_INET, SOCK_STREAM, 0)) == -1){
		return -3;
	}

	if( connect_nonb( sd, (struct sockaddr *) &pin, sizeof(pin),2) < 0)
		return -4;
	return sd;
}

int scs_sendEx(int fd, void *buffer, int nlen)
{
	int 	wlen;
	clear_packet( &_send);
	memcpy(&_send, buffer, nlen);

	_send.header.nEncType = ENC_BF;
	if( _send.header.nEncType == ENC_BF){

		ex_encode_blowfish( &_send);
	}

	wlen = sendEx( fd, (char*)&_send,  get_packet_data_length(&_send), 0, CAS_TIME_OUT);
	return wlen;
}

int scs_recvEx( int fd, void *buffer, int nlen)
{
	int 	rlen = 0;

	struct _Header  header;

	rlen = recvEx( fd, (char*)&_recv, nlen + sizeof(_recv.header), 0, CAS_TIME_OUT);

/*
	DPRINTSRC("_recv.header.nStartCode(%d)", _recv.header.nStartCode);
	DPRINTSRC("_recv.header.nVersion(%d)", _recv.header.nVersion);
	DPRINTSRC("_recv.header.nSvcCode(%d)", _recv.header.nSvcCode);
	DPRINTSRC("_recv.header.nMsgType(%d)", _recv.header.nMsgType);
	DPRINTSRC("_recv.header.nEncType(%d)", _recv.header.nEncType);
	DPRINTSRC("_recv.header.nCmdCode(%d)", _recv.header.nCmdCode);
	DPRINTSRC("_recv.header.nErrCode(%d)", _recv.header.nErrCode);
	DPRINTSRC("_recv.header.nPDUSize(%d)", _recv.header.nPDUSize);
*/


	if( rlen > 0){
		if( _recv.header.nEncType == ENC_BF){
			ex_decode_blowfish( &_recv );
		}

		memcpy( buffer, &_recv, get_packet_data_length(&_recv));
		return get_packet_data_length(&_recv);
	}

	return rlen;
}

int scs_recv( int fd, void *buffer, int nlen)
{
	int 	rlen = 0;
	clear_packet( &_recv);

	rlen = recvEx( fd, (char*)&_recv, nlen + sizeof(_recv.header), 0, CAS_TIME_OUT);
	if( rlen > 0){
		if( _recv.header.nEncType == ENC_BF){
			ex_decode_blowfish( &_recv );

		}

		memcpy( buffer, _recv.pdu, _recv.header.nPDUSize);
		return _recv.header.nPDUSize;
	}
	return rlen;
}



int scs_send(int fd, void *buffer, int nlen)
{
	int 	wlen;
	clear_packet( &_send);

	set_default_header( &_send, 0x0d, ENC_BF);
	set_packet_cmd( &_send, 0x11);  //trigger command
	set_packet_mac( &_send);
	set_packet_time( &_send);
	copy_packet_data( &_send, buffer, nlen);


	if( _send.header.nEncType == ENC_BF){
		ex_encode_blowfish( &_send);
	}

	wlen = sendEx( fd, (char*)&_send,  get_packet_data_length(&_send), 0, CAS_TIME_OUT);
	return wlen;
}
void scs_colose( fd)
{
	close(fd);
}
int     connect_nonb( int fd, struct sockaddr *sa, socklen_t salen, int nsec)
{
    int         			flags, n , error;
    socklen_t   		len;
    fd_set      		rset, wset;
    struct timeval      tval={0,};

    flags = fcntl( fd, F_GETFL, 0);
    fcntl( fd, F_SETFL, flags | O_NONBLOCK);
    error = 0;
    if( ( n=connect( fd, sa, salen)) < 0)
    {
        if( errno != EINPROGRESS )
            return -1;
    }

    if( n == 0 && errno != EINTR )
        goto done;

    FD_ZERO( &rset);
    FD_SET( fd, &rset);
    wset = rset;

    tval.tv_sec = nsec;
    tval.tv_usec = 900000;

    if( (n=select(fd+1, &rset, &wset, NULL, nsec ? &tval : NULL)) == 0)
    {
        close(fd);
        errno = ETIMEDOUT;
        return -5;
    }

    if( FD_ISSET( fd, &rset) || FD_ISSET( fd, &wset))
    {
        len = sizeof(error);
        if( (getsockopt( fd, SOL_SOCKET, SO_ERROR, &error, &len) < 0))
        {
        	close(fd);
            return -5;
        }
    }

done:
    fcntl( fd, F_SETFL, flags);
    if( error)
    {
        close(fd);
        errno = error;
        return -5;
    }
    return 0;
}

/**
 *  @brief   scs로 보낼 packet의 header를 기본값으로 세팅해줌.
 *  @return  현재는 무조건 true
 */
int set_default_header( tpacket   *packet, int ver, int enc )
{
    packet->header.nStartCode   = HEADER_DEFAULT_STARTCODE;
    packet->header.nVersion     =  ver;
    packet->header.nSvcCode     = HEADER_DEFAULT_SVCCODE;
    packet->header.nMsgType     = HEADER_DEFAULT_MSGTYPE;
    packet->header.nEncType     = enc;
    packet->header.nCrcCheck    = HEADER_DEFAULT_CRCCHECK;
    packet->header.nPDUSize     = 0;
    packet->pdu[0] = '\0';

    return 0;
}

int set_packet_cmd( tpacket *packet, unsigned char cmd)
{
    return packet->header.nCmdCode = cmd;
}

int set_packet_err( tpacket *packet, unsigned char err)
{
    return packet->header.nErrCode = err;
}

int set_packet_mac( tpacket *packet )
{
	return get_interface_info( "eth0", SIOCGIFHWADDR, (unsigned char *)(packet->header.byStbMsc) );
}

int set_packet_time( tpacket *packet )
{
    time( (time_t*)&packet->header.dTimeStamp );
    return 0;
}

/**
 * �꾩옱 packet���ㅼ뼱 �덈뒗  pdu���대떦�섎뒗 遺�텇���ш린留�0�쇰줈 �ㅼ젙�섎떎.
 * �ㅼ젣 pdu��硫붾え由щ� clear�섏����딅뒗��
 * @param packet  tpacket 援ъ“泥대줈  SCS�쒕쾭���듭떊�섎뒗���ъ슜�섎뒗 援ъ“泥� * @return �깃났�대㈃ SA_OK, �ㅽ뙣�섎㈃ �ㅻⅨ 媛� */
int   clear_packet( tpacket *packet)
{
    packet->header.nPDUSize = 0;
    bzero( &packet->header, sizeof(packet->header));
    return 0;
}

int  copy_packet_data( tpacket *packet, void* data, int len)
{
	memcpy( packet->pdu, data, len);
	packet->header.nPDUSize = len;
    return 0;
}
int   set_packet_data( tpacket *packet, char* data)
{
    strcpy( packet->pdu, data );
    packet->header.nPDUSize = strlen(data);
    return 0;
}

/**
 * �꾩옱 packet���곗씠�곕� 梨꾩썙�ｋ뒗 �⑥닔
 * �곗씠�곕뒗 tpacket�� pdu��湲곕줉�쒕떎.
 * @param packet tpacket 援ъ“泥대줈  SCS�쒕쾭���듭떊�섎뒗���ъ슜�섎뒗 援ъ“泥� * @param data SCS handler�먯꽌 湲곕줉�섍퀬���섎뒗 �곗씠��(format��SCS�곕룞 protocol
 ��紐낆떆��format ��
 * @return �깃났�대㈃ SA_OK �ㅽ뙣�대㈃ �ㅻⅨ 媛� */
int   append_packet_data( tpacket *packet, char* data)
{
    strcpy( (char*)(packet->pdu + packet->header.nPDUSize), data);
    packet->header.nPDUSize += strlen(data);
    return 0;
}

/**
 * �꾩옱 packet���ㅼ뼱 �덈뒗 �곗씠�곗쓽 �ш린 (header �ш린��pdu �ш린瑜��섎���
 * @param packet tpacket 援ъ“泥대줈  SCS�쒕쾭���듭떊�섎뒗���ъ슜�섎뒗 援ъ“泥� * @param �뺤닔�뺤쓽 �ш린(byte�⑥쐞)
 *
 * header 援ъ“泥대뒗 byte align�쇰줈 �곗냽�쇰줈 �좊떦�쒕떎. header���ш린��sizeof 濡�蹂대궦��
 */
int     get_packet_data_length( tpacket *packet)
{
    return sizeof(packet->header) + packet->header.nPDUSize;
}

int     get_packet_free_length( tpacket *packet)
{
    return sizeof(packet->header) + MAX_BUFFER_SIZE;
}

int  get_interface_info( const char *eth, int infotype, char *rtnval)
{
	int i, fd, len =0;
	struct ifreq ifrq;
	struct sockaddr_in *sin;

	//bzero( rtnval, sizeof(rtnvalue));
	fd = socket( AF_INET, SOCK_DGRAM, 0 );
	strcpy( ifrq.ifr_name, eth );
	if( ioctl(fd, infotype, &ifrq) < 0 )
	{
		close( fd );
		if(infotype == SIOCGIFADDR && strcmp(eth, "eth0") == 0) {
			return get_interface_info("wlan0", infotype, rtnval);
		} else return -1;
	}

	switch( infotype )
	{
	case SIOCGIFHWADDR: /* return mac address */
		memcpy( rtnval, ifrq.ifr_addr.sa_data, 6);
		break;
	case SIOCGIFADDR:   /* return ip address */
		sin = (struct sockaddr_in *)&ifrq.ifr_addr;
		if(sizeof(sin->sin_addr) >= 4) {
			unsigned char *pAddr = (unsigned char *) &sin->sin_addr;
			sprintf(rtnval, "%d.%d.%d.%d", pAddr[0], pAddr[1], pAddr[2], pAddr[3]);
		}
		break;
	default:
		/* wrong method, return NULL */
		close( fd );
		return -1;
	}

	close( fd );
	return 0;
}

#ifdef TEST_DRIVER
int main(int argc, char *argv)
{
	int wlen = 0;
	char	  buf[1024] = {0,};
	int fd = scs_init( "172.16.130.10", 30000); //scs-simulator
	set_default_header( &_send, 0x0c, 1); // plain
	set_packet_cmd( &_send, 0x41); // cmd-0x41
	copy_packet_data( &_send, "dummy-text-data", strlen("dummy-tex-data"));
	wlen = scs_send( fd, &_send, get_packet_data_length(&_send));

	wlen = scs_recv( fd, &_recv, 1024);
	printf("cmd(%d) errno(%d) pdu-len(%d)\n", _recv.header.nCmdCode, _recv.header.nErrCode, _recv.header.nPDUSize);
	printf("pdu-value(%s)\n", _recv.header.pdu);
	scs_close(fd);
}
#endif

