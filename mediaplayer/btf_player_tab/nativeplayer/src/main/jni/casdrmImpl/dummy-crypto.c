/*
 * dummy-crypto.c
 *
 *  Created on: 2008. 10. 30
 *      Author: javaos
 */
#include "CAAgent.h"
#include "DRMCrypto.h"
#include "mcas_iptv.h"

int dca_RequestUserCert(unsigned int ServiceID, CIS_USER_INFO* pUserInfo,
                         char* szCAUrl, dca_RESULTCB cb, void* param)
{
 return 0;
}

DCI_API CIR dci_InitializeLib(DCI_IN const void *args)
{
 return 0;
}
CIR dci_ReleaseLib(DCI_IN const void *args)
{
 return 0;
}
CIR dci_RSADecrypt(CIH_PRIKEY hPrikey, CIC_RSA_PAD padding, DCI_IN const uint8_t *in, int inLength, DCI_OUT uint8_t *out, DCI_INOUT int *p_outLength)
{
        memcpy(in, out, inLength);
        *p_outLength = inLength;
 return 0;
}
CIR dci_GetUserCertNKey(uint32_t serviceID, DCI_IN const char *userID, DCI_OUT CIH_CERT *p_hCert, DCI_OUT CIH_PRIKEY *p_hPrikey)
{
 return 0;
}
CIR dci_SaveOtherCert(DCI_IN const char *label, CIH_CERT hCert)
{
 return 0;
}
CIR dci_CertFree(CIH_CERT hCert)
{
        return 0;
}
CIR dci_CertDecode(DCI_IN const uint8_t *in, int inLength, DCI_OUT CIH_CERT *p_hCert)
{
        return 0;
}
