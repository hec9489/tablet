#ifndef _MCAS_REG_DUMMY_IMPL_H_______________
#define _MCAS_REG_DUMMY_IMPL_H_______________

#ifdef __cplusplus
extern "C" {
#endif


/* javaos for test registeration */
int16  process_reg_confirm(void);
int16  process_auth_confirm(void);
int16  process_cert_update(void); // SAK - 16Mar2009 - Added

// 20201029 : jung2604 : 11개월 이상 지났는지 체크하는 함수
int16 checkAuthOldFileIdentification(void);

#ifdef __cplusplus
}
#endif
#endif /*_MCAS_REG_DUMMY_IMPL_H_______________ */

