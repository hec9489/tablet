/**
 * simple MPEG2 TS parser 
 * used only for CAS 
 * process only given pid for ECM,AUDIO,VIDEO
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TS_PACKET_LEN (188)              /* TS RDSIZE is fixed !! */
#define TS_SYNC_BYTE  (0x47)             /* SyncByte fuer TS  ISO 138181-1 */

typedef  struct _TSBuffer {
	unsigned char	buffer[TS_PACKET_LEN*8];
	int 		sp;  // buffer start pos;
	int 		ep; // buffer end pos
	int 		cp; // buffer current pos;
	unsigned int 		len; // buffer len;
	struct _TSBuffer *next;
} TSBufferType;


typedef enum {
	Unknown,
	AudioPID,
	VideoPID,
	ECMPID,
	PATPID,
	PMTPID,
} PidType;

typedef struct _pidTab {
	unsigned int 	ecm;
	unsigned int 	audio;
	unsigned int 	video;
} pidTableType ;


typedef int (*sectionFilter)( PidType type,unsigned int pid, unsigned char *buf, int size, int tshdr);

typedef struct _demux {
	pidTableType		pidTab;
	sectionFilter		ECMFilter;
	sectionFilter 		AVFilter;
	TSBufferType		inBuffer;
	TSBufferType		outBuffer;
	int 					TSFound;
}SWDemux;


/**
 * find ts packet & validate ts packet in givien input buffer
 */ 
int		wpi_find_sync( unsigned char *buf, int size)
{
	int 	offset =-1, pos=0;
	int		found = 0;
	
	do {
		if( buf[pos] == TS_SYNC_BYTE )
		{
			if(  (pos+TS_PACKET_LEN < size && buf[pos+TS_PACKET_LEN] == TS_SYNC_BYTE) &&
				(pos+2*TS_PACKET_LEN < size && buf[pos+2*TS_PACKET_LEN] == TS_SYNC_BYTE))
				found = 1;
				offset=pos;
				break;
		}
	} while( ++pos < size && !found);
	return offset;
}

int SectionFilterImpl ( PidType type, unsigned int pid, unsigned char *buf, int size, int tshdr)
{
	FILE 		*fp = NULL;
	char 		fname[20] = {0,};
	if( type == ECMPID || type == AudioPID || type == VideoPID)
	{
		sprintf( fname, "%d.out", pid);
		fp = fopen( fname, "ab");
		fwrite( buf + tshdr, 1, size, fp);
		fclose( fp);
	}
	return 0;
}

int DemuxCheckTS( SWDemux *demux, int pos)
{
	unsigned char	*buf= 0;
	unsigned char	*_tmp =0;
	
	buf = demux->inBuffer.buffer;
	do {
		if( buf[pos] == TS_SYNC_BYTE )
		{
			if(  (pos+TS_PACKET_LEN < demux->inBuffer.ep && buf[pos+TS_PACKET_LEN] == TS_SYNC_BYTE) &&
				(pos+2*TS_PACKET_LEN < demux->inBuffer.ep && buf[pos+2*TS_PACKET_LEN] == TS_SYNC_BYTE))
				demux->TSFound = 1;
				_tmp = (unsigned char*) malloc( sizeof(char) * (demux->inBuffer.ep - pos));
				memcpy( _tmp, demux->inBuffer.buffer+pos, demux->inBuffer.len - pos);
				memcpy( demux->inBuffer.buffer, _tmp, demux->inBuffer.len - pos);
				free( _tmp);
				demux->inBuffer.ep -= pos;
				demux->inBuffer.cp = 0;
				demux->inBuffer.len = demux->inBuffer.ep+1;
				break;
		}
	} while( ++pos < demux->inBuffer.ep && !demux->TSFound);		
	return demux->inBuffer.cp;
}
int DemuxFeedBuffer( SWDemux *demux, void *buffer, int size)
{
	int 		tspos = -1;
	if( !demux->TSFound) // initial 
	{
		memcpy( demux->inBuffer.buffer, buffer, size);
		demux->inBuffer.cp = demux->inBuffer.sp = 0;
		demux->inBuffer.len = size;
		demux->inBuffer.ep = demux->inBuffer.len-1; 
		demux->inBuffer.cp = DemuxCheckTS( demux, demux->inBuffer.cp);
	}
	else
	{
		if( demux->inBuffer.cp < demux->inBuffer.ep) //some byte remains,
		{
			memcpy( demux->inBuffer.buffer, demux->inBuffer.buffer+demux->inBuffer.cp, demux->inBuffer.ep-demux->inBuffer.cp);
			demux->inBuffer.ep -= demux->inBuffer.cp;
			demux->inBuffer.cp = 0;
		}
		else
			demux->inBuffer.cp = demux->inBuffer.ep = 0; // no byte remains;
		memcpy( demux->inBuffer.buffer + demux->inBuffer.ep+1, buffer, size);
		demux->inBuffer.ep += size;
		demux->inBuffer.len = demux->inBuffer.ep+1;
	}

	return tspos;
}

int DemuxParse( SWDemux *demux, void *buffer, int size)
{
	int 				cp = 0;
	unsigned int 	_pid = 0, _scramble = 0;
	unsigned char	*buf = 0;
	
	DemuxFeedBuffer( demux, buffer, size);
	
	buf = demux->inBuffer.buffer;
	do {
		cp = demux->inBuffer.cp;
		if( buf[cp] != TS_SYNC_BYTE )
		{
			demux->TSFound = 0;
			demux->inBuffer.cp = -1;
			cp = DemuxCheckTS( demux, cp);
			if( cp < 0)
				break;
		}
		if( buf[cp+1] & 0x80) // error  just skip
		{
			demux->inBuffer.cp += TS_PACKET_LEN;
			continue;
		}
		_pid = (buf[cp+1] & 0x1F) << 8;
		_pid |= (buf[cp+2]);
		_scramble = (buf[cp+3] & 0xC0)  ? 1 : 0;
		if(  _scramble && demux->AVFilter && (_pid == demux->pidTab.audio || _pid == demux->pidTab.video) )
			demux->AVFilter( _pid == demux->pidTab.audio ? AudioPID : VideoPID, _pid, &buf[cp], TS_PACKET_LEN, 0);
		else if( demux->ECMFilter && _pid == demux->pidTab.ecm)
			demux->ECMFilter( ECMPID,_pid, &buf[cp], TS_PACKET_LEN, 0);
		demux->inBuffer.cp += TS_PACKET_LEN;
	} while( demux->inBuffer.cp +TS_PACKET_LEN  < demux->inBuffer.ep);

	return 0;
}


#define BUF_SIZE 	188*7
SWDemux		demux;
int main(int argc, char * argv[])
{
	unsigned char 	buf[BUF_SIZE] = {0,};
	int 					tshdr = -1, bpos = 0, ts_found= 0, rsize = -1;
	FILE					*fp;
	if( argc == 2 && argv[1] )
		fp = fopen( argv[1], "rb");
	
	demux.pidTab.audio = 0x1022;
	demux.pidTab.video = 0x1023;
	demux.pidTab.ecm = -1;
	demux.AVFilter = SectionFilterImpl;
	demux.ECMFilter = 0; //SectionFilterImpl;
	demux.TSFound = 0;
	do {
		rsize = fread(buf, 1,BUF_SIZE, fp);
		if( rsize > 0)
			DemuxParse( &demux, buf, rsize);
	}while ( rsize);
	fclose(fp);
}
