// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#define LOG_TAG "BtvMediaPlayer"

#include <log/log.h>
#include <binder/Parcel.h>

#ifdef FEATURE_SOC_AMLOGIC
//#include <gui/bufferqueue/1.0/H2BGraphicBufferProducer.h>
#endif

#include "BtvMediaPlayer.h"

#include <android/log.h>
#define ALogD( msg... )     __android_log_print( ANDROID_LOG_DEBUG,      LOG_TAG, msg )
#define ALogI( msg... )     __android_log_print( ANDROID_LOG_INFO,      LOG_TAG, msg )
#define LOG_INFO	ALogI		//dummyDebug

namespace com {
namespace skb {
namespace btvservice {
namespace V1_0 {
namespace implementation {

using ::com::skb::btvservice::V1_0::DsmccSignalEvent;
using ::android::ApplicationInfo;
using ::android::Vector;
using ::android::Parcel;

#ifdef FEATURE_SOC_AMLOGIC
//using ::android::hardware::graphics::bufferqueue::V1_0::utils::H2BGraphicBufferProducer;
#endif

enum ValueType {
	TYPE_INT    = 0,
	TYPE_LONG   = 1,		
	TYPE_STRING = 2
};

class BtvMediaPlayerCallback: public BtvMediaPlayerObserver {
	public:
		BtvMediaPlayerCallback(BtvMediaPlayer* player) : player_(player) {}
		~BtvMediaPlayerCallback() {}
		virtual void notifyCallback(int32_t msg, int32_t ext1, int32_t ext2, const int8_t* obj)
		{
			LOG_INFO("[%s][%s] called", __FILE__, __FUNCTION__);
			player_->notifyCallback(msg, ext1, ext2, obj);
		}

		virtual void dataCallback(int32_t msg, int32_t ext1, int32_t ext2, const int8_t* dataPtr)
		{
			LOG_INFO("[%s][%s] called", __FILE__, __FUNCTION__);
			player_->dataCallback(msg, ext1, ext2, dataPtr);			
		}	

	private:
		BtvMediaPlayer* player_;
};

class BtvDsmccCallback: public BtvDsmccObserver {
	public:
		BtvDsmccCallback(BtvMediaPlayer* player) : player_(player) {}
		~BtvDsmccCallback() {}
		virtual void dsmccSignalEvent(int32_t dsmccEventType, char* dsmccRootPath, SignalEvent* event)
		{
			LOG_INFO("[%s][%s] called", __FILE__, __FUNCTION__);
			player_->dsmccSignalEvent(dsmccEventType, dsmccRootPath, event);
		}

	private:
		BtvMediaPlayer* player_;
};

class BtvMediaCommunicationCallback: public BtvMediaCommunicationObserver {
	public:
		BtvMediaCommunicationCallback(BtvMediaPlayer* player) : player_(player) {}
		~BtvMediaCommunicationCallback() {}
		virtual void familyStateEvent(int32_t state)
		{
			LOG_INFO("[%s][%s] called", __FILE__, __FUNCTION__);
			player_->familyStateEvent(state);
		}

		virtual void	onCasInfoEvent(int32_t type, std::string casInfo)
		{
			LOG_INFO("[%s][%s] called", __FILE__, __FUNCTION__);
			player_->onCasInfoEvent(type, casInfo);
		}

	private:
		BtvMediaPlayer* player_;
};

class BtvAudioProgramUpdateCallback: public BtvAudioProgramUpdateObserver {
	public:
		BtvAudioProgramUpdateCallback(BtvMediaPlayer* player) : player_(player) {}
		~BtvAudioProgramUpdateCallback() {}
		virtual void audioProgramUpdate(char* filepath, int32_t updateType, int32_t audioPid)
		{
			LOG_INFO("[%s][%s] called", __FILE__, __FUNCTION__);
			player_->audioProgramUpdateEvent(filepath, updateType, audioPid);
		}

	private:
		BtvMediaPlayer* player_;
};


BtvMediaPlayer::BtvMediaPlayer(int device) {
	LOG_INFO("[%s][%s][%d] called", __FILE__, __FUNCTION__, device);
	mDeviceID = device;
	mBtvPlayerService = ::android::BtvMediaPlayerService::getInstance();
	mObserver = new BtvMediaPlayerCallback(this);
	mDsmccObserver = new BtvDsmccCallback(this);
	mMediaCommunicationObserver = new BtvMediaCommunicationCallback(this);
	mAudioProgramUpdateObserver = new BtvAudioProgramUpdateCallback(this);
	mListener = NULL;
	mDsmccListener = NULL;
	mMediaCommunicationListener = NULL;
	mAudioProgramUpdateListener = NULL;		
}

BtvMediaPlayer::~BtvMediaPlayer() {

}

Return<Status> BtvMediaPlayer::connect(int32_t device)
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, device);
	Mutex::Autolock _l(mLock);
	
	mDeviceID = device;
	mObserver->setDeviceID(device);
	
    if (::android::BtvMediaPlayerService::getInstance()->connect(mObserver, mDeviceID) != ::android::OK) {
        return Status::INTERNAL_ERROR;
    }
	
	return Status::OK;
}

Return<Status> BtvMediaPlayer::disconnect()
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	Mutex::Autolock _l(mLock);
	
	::android::BtvMediaPlayerService::getInstance()->disconnect(mDeviceID);
	return Status::OK;
}


Return<Status> BtvMediaPlayer::open(const hidl_string &dataXML)
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	LOG_INFO("[%s][%s] dataXML[%s]", __FILE__, __FUNCTION__, dataXML.c_str());		
	Mutex::Autolock _l(mLock);
	
	if (::android::BtvMediaPlayerService::getInstance()->open(mDeviceID, dataXML.c_str()) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}
	
	return Status::OK;
}

Return<Status> BtvMediaPlayer::tuneTV(const hidl_string &dataXML)
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	LOG_INFO("[%s][%s] dataXML[%s]", __FILE__, __FUNCTION__, dataXML.c_str());		
	Mutex::Autolock _l(mLock);
	
	if (::android::BtvMediaPlayerService::getInstance()->tuneTV(mDeviceID, dataXML.c_str()) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}

	return Status::OK;
}

Return<Status> BtvMediaPlayer::closeTV()
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	Mutex::Autolock _l(mLock);
	
	if (::android::BtvMediaPlayerService::getInstance()->closeTV(mDeviceID) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}
	
	return Status::OK;
}

Return<Status> BtvMediaPlayer::bindingFilter(const hidl_string &dataXML)
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	LOG_INFO("[%s][%s] dataXML[%s]", __FILE__, __FUNCTION__, dataXML.c_str());		

	Mutex::Autolock _l(mLock);
	
	if (::android::BtvMediaPlayerService::getInstance()->bindingFilter(mDeviceID, dataXML.c_str()) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}
	
	return Status::OK;
}

Return<Status> BtvMediaPlayer::releaseFilter(const hidl_string &dataXML)
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	LOG_INFO("[%s][%s] dataXML[%s] called", __FILE__, __FUNCTION__, dataXML.c_str());		

	Mutex::Autolock _l(mLock);
	
	if (::android::BtvMediaPlayerService::getInstance()->releaseFilter(mDeviceID, dataXML.c_str()) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}
	
	return Status::OK;
}

Return<Status> BtvMediaPlayer::changeAudioChannel(const hidl_string &dataXML)
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	LOG_INFO("[%s][%s] dataXML[%s]", __FILE__, __FUNCTION__, dataXML.c_str());		

	Mutex::Autolock _l(mLock);
	
	if (::android::BtvMediaPlayerService::getInstance()->changeAudioChannel(mDeviceID, dataXML.c_str()) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}
	
	return Status::OK;
}

Return<Status> BtvMediaPlayer::changeAudioOnOff(const hidl_string &dataXML)
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	LOG_INFO("[%s][%s] dataXML[%s]", __FILE__, __FUNCTION__, dataXML.c_str());		

	Mutex::Autolock _l(mLock);
	
	if (::android::BtvMediaPlayerService::getInstance()->changeAudioOnOff(mDeviceID, dataXML.c_str()) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}
	
	return Status::OK;
}


Return<void> BtvMediaPlayer::getPlayerMode(getPlayerMode_cb _hidl_cb)
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);

	Mutex::Autolock _l(mLock);

	Status retval = Status::OK;
	uint32_t mode = ::android::BtvMediaPlayerService::getInstance()->getPlayerMode(mDeviceID);
	
	_hidl_cb(retval, mode);
	return Void();
	
}


Return<void> BtvMediaPlayer::getPlayerStatus(getPlayerStatus_cb _hidl_cb)
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);

	Mutex::Autolock _l(mLock);
	
	Status retval = Status::OK;
	uint32_t player_status = ::android::BtvMediaPlayerService::getInstance()->getPlayerStatus(mDeviceID);
	
	_hidl_cb(retval, player_status);
	return Void();
}

Return<void> BtvMediaPlayer::getCurrentPosition(getCurrentPosition_cb _hidl_cb)
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);

	Mutex::Autolock _l(mLock);
	
	Status retval = Status::OK;
	uint32_t position = ::android::BtvMediaPlayerService::getInstance()->getCurrentPosition(mDeviceID);
	
	_hidl_cb(retval, position);
	return Void();
}

Return<Status> BtvMediaPlayer::setWindowSize(const hidl_string &dataXML)
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	LOG_INFO("[%s][%s] dataXML[%s]", __FILE__, __FUNCTION__, dataXML.c_str());		

	Mutex::Autolock _l(mLock);
	
	if (::android::BtvMediaPlayerService::getInstance()->setWindowSize(mDeviceID, dataXML.c_str()) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}
	
	return Status::OK;
}

Return<Status> BtvMediaPlayer::play()
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);

	Mutex::Autolock _l(mLock);
	
	if (::android::BtvMediaPlayerService::getInstance()->play(mDeviceID) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}
	
	return Status::OK;
}

Return<Status> BtvMediaPlayer::pause()
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);

	Mutex::Autolock _l(mLock);
	
	if (::android::BtvMediaPlayerService::getInstance()->pause(mDeviceID) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}
	
	return Status::OK;
}

Return<Status> BtvMediaPlayer::resume()
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);

	Mutex::Autolock _l(mLock);
	
	if (::android::BtvMediaPlayerService::getInstance()->resume(mDeviceID) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}
	
	return Status::OK;
}

Return<Status> BtvMediaPlayer::seek(const hidl_string &dataXML, bool pause)
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	LOG_INFO("[%s][%s] dataXML[%s], pause:[%s]", __FILE__, __FUNCTION__, dataXML.c_str(), pause ? "true":"false");		

	Mutex::Autolock _l(mLock);
	
	if (::android::BtvMediaPlayerService::getInstance()->seek(mDeviceID, dataXML.c_str(), pause) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}
	
	return Status::OK;
}

Return<Status> BtvMediaPlayer::pauseAt(const hidl_string &dataXML)
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	LOG_INFO("[%s][%s] dataXML[%s]", __FILE__, __FUNCTION__, dataXML.c_str());

	Mutex::Autolock _l(mLock);

	if (::android::BtvMediaPlayerService::getInstance()->pauseAt(mDeviceID, dataXML.c_str()) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}

	return Status::OK;
}

Return<Status> BtvMediaPlayer::trick(const hidl_string &dataXML)
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	LOG_INFO("[%s][%s] dataXML[%s]", __FILE__, __FUNCTION__, dataXML.c_str());		

	Mutex::Autolock _l(mLock);
	
	if (::android::BtvMediaPlayerService::getInstance()->trick(mDeviceID, dataXML.c_str()) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}
	
	return Status::OK;
}

Return<Status> BtvMediaPlayer::keepLastFrame(bool flag)
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	LOG_INFO("[%s][%s] flag[%s]", __FILE__, __FUNCTION__, flag ? "true":"false");		

	Mutex::Autolock _l(mLock);
	
	if (::android::BtvMediaPlayerService::getInstance()->keepLastFrame(mDeviceID, flag) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}
	
	return Status::OK;
}

Return<Status> BtvMediaPlayer::stop()
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);

	Mutex::Autolock _l(mLock);
	
	if (::android::BtvMediaPlayerService::getInstance()->stop(mDeviceID) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}
	
	return Status::OK;
}

Return<Status> BtvMediaPlayer::close()
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);

	Mutex::Autolock _l(mLock);
	
	if (::android::BtvMediaPlayerService::getInstance()->close(mDeviceID) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}
	
	return Status::OK;
}

Return<void> BtvMediaPlayer::invoke(const hidl_vec<ParcelData>& request, invoke_cb _hidl_cb)
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);

	//Mutex::Autolock _l(mLock);
	
	Status retval = Status::OK;
	Parcel data, reply;
	size_t countEntries = request.size();	
	
	data.writeUint32(countEntries);	// size
	for (size_t i = 0; i < countEntries; i++) {
		int32_t type = request[i].parcelType; // type
		data.writeInt32(type);
		switch (type) {		
			case TYPE_INT : {			
				int32_t val; 
				val = request[i].parcelInt; 
				data.writeInt32(val);			
				break;		
			}		
			case TYPE_LONG : {			
				int64_t val; 
				val = request[i].parcelLong; 
				data.writeInt64(val);			
				break;		
			}		
			case TYPE_STRING : {			
				hidl_string val; 
				val = request[i].parcelString; 
				data.writeUtf8AsUtf16(val.c_str());			
				break;		
			}
		}
	}

	std::vector<ParcelData> Datas;
	
	if (::android::BtvMediaPlayerService::getInstance()->invoke(mDeviceID, data, &reply) != ::android::OK) {
		retval = Status::INTERNAL_ERROR;
		_hidl_cb(retval, Datas);
		return Void();		
	}

	reply.setDataPosition(0);
	countEntries = reply.readUint32();
	//LOG_INFO("[%s][%s] parcel response size [%d]", __FILE__, __FUNCTION__, countEntries);
	for (size_t i = 0; i < countEntries; i++) {
		ParcelData parcel;
		int32_t type = reply.readInt32();
		//LOG_INFO("[%s][%s] parcel response type [%d]", __FILE__, __FUNCTION__, type);
		switch (type) {		
			case TYPE_INT : {
				parcel.parcelType = TYPE_INT;
				parcel.parcelInt = reply.readInt32();
				//LOG_INFO("[%s][%s] parcel response Int value [%d]", __FILE__, __FUNCTION__, parcel.parcelInt);
 				break;
			}		
			case TYPE_LONG : {			
				parcel.parcelType = TYPE_LONG;
				parcel.parcelLong = reply.readInt64();
				//LOG_INFO("[%s][%s] parcel response Long value [%lld]", __FILE__, __FUNCTION__, parcel.parcelLong);
				break;		
			}		
			case TYPE_STRING : {
				std::string val; 
				reply.readUtf8FromUtf16(&val);
				parcel.parcelType = TYPE_STRING;
				parcel.parcelString = val;
				break;		
			}
		}
		Datas.push_back(parcel);
	}
	
	_hidl_cb(retval, Datas);
	return Void();
}

Return<Status> BtvMediaPlayer::setPlayerSize(int32_t nLeft, int32_t nTop, int32_t nWidth, int32_t nHeight)
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	LOG_INFO("[%s][%s] left:%d, top:%d, width:%d, height:%d", __FILE__, __FUNCTION__, nLeft, nTop, nWidth, nHeight);		
	Mutex::Autolock _l(mLock);


	if (::android::BtvMediaPlayerService::getInstance()->setPlayerSize(mDeviceID, nLeft, nTop, nWidth, nHeight) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}
	
	return Status::OK;
}
 
Return<Status> BtvMediaPlayer::setPlayerSurfaceView(int32_t deviceID, uint32_t nativWindows) {
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);		
	Mutex::Autolock _l(mLock);
	::android::BtvMediaPlayerService::getInstance()->setPlayerSurfaceView(deviceID, nativWindows);
	return Status::OK;
}

#ifdef FEATURE_SOC_AMLOGIC
#if 0
Return<Status> BtvMediaPlayer::setPlayerSurface(const sp<IGraphicBufferProducer>& producer) {
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);		
	Mutex::Autolock _l(mLock);
	sp<android::IGraphicBufferProducer> bufferProducer =
	new H2BGraphicBufferProducer(producer);
	
	sp<android::Surface> surface = new ::android::Surface(bufferProducer);
	
	::android::BtvMediaPlayerService::getInstance()->setPlayerSurface(mDeviceID, surface);	

	return Status::OK;
}
#endif
#endif

Return<Status> BtvMediaPlayer::startDmxFilter(int32_t pid, int32_t tid) {
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);		
	LOG_INFO("[%s][%s] pid:%d, tid:%d", __FILE__, __FUNCTION__, pid, tid);		

	Mutex::Autolock _l(mLock);
	::android::BtvMediaPlayerService::getInstance()->startDmxFilter(mDeviceID, pid, tid);
	return Status::OK;
}

Return<Status> BtvMediaPlayer::stopDmxFilter(int32_t pid, int32_t tid) {
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);		
	LOG_INFO("[%s][%s] pid:%d, tid:%d", __FILE__, __FUNCTION__, pid, tid);		

	Mutex::Autolock _l(mLock);
	::android::BtvMediaPlayerService::getInstance()->stopDmxFilter(mDeviceID, pid, tid);
	return Status::OK;
}


Return<Status> BtvMediaPlayer::setDummys(const hidl_string &dummys)
{
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	LOG_INFO("[%s][%s] dummys[%s]", __FILE__, __FUNCTION__, dummys.c_str());		

	Mutex::Autolock _l(mLock);
	
	if (::android::BtvMediaPlayerService::getInstance()->setDummys(mDeviceID, dummys.c_str()) != ::android::OK) {
		return Status::INTERNAL_ERROR;
	}
	
	return Status::OK;
}

void BtvMediaPlayer::serviceDied(uint64_t /* cookie */, const wp<IBase>& /* who */) {
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
}


Return<void> BtvMediaPlayer::setListener(const sp<IBtvMediaPlayerListener>& listener) {
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	android::Mutex::Autolock l(&mNotifyLock);	
	if (mListener != nullptr) {
		LOG_INFO("[%s][%s] unlinkToDeath", __FILE__, __FUNCTION__);
		mListener->unlinkToDeath(this).isOk();
		mListener = nullptr;
	}
	if (listener != nullptr) {
		LOG_INFO("[%s][%s] linkToDeath", __FILE__, __FUNCTION__);
		mListener = listener;
		mListener->linkToDeath(this, 0 /*cookie*/).isOk();
	}
	return Void();
}

void BtvMediaPlayer::notifyCallback(int32_t msg, int32_t ext1, int32_t ext2, const int8_t* obj)
{
	LOG_INFO("[%s][%s][%d] device [%d] message received msg=%d, ext1=%d, ext2=%d", __FILE__, __FUNCTION__, __LINE__, mDeviceID, msg, ext1, ext2);

	Mutex::Autolock _l(mNotifyLock);

	sp<IBtvMediaPlayerListener> listener = mListener;

	// this prevents re-entrant calls into client code
	if (listener != nullptr)
	{
		LOG_INFO("[%s][%s][%d] callback application", __FILE__, __FUNCTION__, __LINE__);
		android::hardware::hidl_vec<int8_t> data;
		if(obj != NULL)	data = std::vector<int8_t>(obj, obj + strlen((const char*)obj));		
		auto ret = listener->notifyCallback(msg, ext1, ext2, data);
		if (!ret.isOk() && ret.isDeadObject()) {
			mListener = nullptr;
		} 
		LOG_INFO("[%s][%s][%d] back from callback", __FILE__, __FUNCTION__, __LINE__);
	}
}

void BtvMediaPlayer::dataCallback(int32_t msg, int32_t ext1, int32_t ext2, const int8_t* dataPtr)
{
	LOG_INFO("[%s][%s][%d] device [%d] message received msg=%d, ext1=%d, ext2=%d", __FILE__, __FUNCTION__, __LINE__, mDeviceID, msg, ext1, ext2);

	Mutex::Autolock _l(mNotifyLock);

	sp<IBtvMediaPlayerListener> listener = mListener;

	if (listener != nullptr)
	{
		LOG_INFO("[%s][%s][%d] callback application", __FILE__, __FUNCTION__, __LINE__);
		android::hardware::hidl_vec<int8_t> data;
		if(dataPtr != NULL)	data = std::vector<int8_t>(dataPtr, dataPtr + ext2);		
		auto ret = listener->dataCallback(msg, ext1, ext2, data);
		if (!ret.isOk() && ret.isDeadObject()) {
			mListener = nullptr;
		} 
		LOG_INFO("[%s][%s][%d] back from callback", __FILE__, __FUNCTION__, __LINE__);
	}
}

Return<void> BtvMediaPlayer::setDsmccListener(const sp<IBtvDsmccListener>& dsmcclistener) {
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	android::Mutex::Autolock l(&mNotifyLock);	
	if (mDsmccListener != nullptr) {
		LOG_INFO("[%s][%s] unlinkToDeath", __FILE__, __FUNCTION__);
		mDsmccListener->unlinkToDeath(this).isOk();
		mDsmccListener = nullptr;
	}
	if (dsmcclistener != nullptr) {
		LOG_INFO("[%s][%s] linkToDeath", __FILE__, __FUNCTION__);
		mDsmccListener = dsmcclistener;
		mDsmccListener->linkToDeath(this, 0 /*cookie*/).isOk();
	}
	::android::BtvMediaPlayerService::getInstance()->setDsmccClient(mDsmccObserver);
	return Void();
}

void BtvMediaPlayer::dsmccSignalEvent(int32_t eventType, char* rootPath, SignalEvent* signalEvent)
{
	LOG_INFO("[%s][%s][%d] dsmccSignalEvent received event=%d, rootpath=%s", __FILE__, __FUNCTION__, __LINE__, eventType, rootPath);

	Mutex::Autolock _l(mNotifyLock);

	sp<IBtvDsmccListener> listener = mDsmccListener;

	if (listener != nullptr)
	{
		LOG_INFO("[%s][%s][%d] callback application", __FILE__, __FUNCTION__, __LINE__);
		
		hidl_string dsmccRootPath;
		dsmccRootPath.setToExternal(rootPath, strlen(rootPath));
		
		DsmccSignalEvent dsmccEvent;
		if(signalEvent != NULL){
			dsmccEvent.eventType = signalEvent->eventType;
			dsmccEvent.serviceId = signalEvent->serviceId;
			dsmccEvent.demux = signalEvent->demux;
			dsmccEvent.uri.setToExternal(signalEvent->uri, strlen(signalEvent->uri));
			
			Vector<ApplicationInfo> appLists = signalEvent->appLists;
			size_t numApps = appLists.size();
			std::vector<AppInfo> Apps;
			for (size_t i = 0; i < numApps; i++) {
				AppInfo app;
				app.appType = appLists.itemAt(i).appType;
				app.organizationId = appLists.itemAt(i).organizationId;
				app.applicationId = appLists.itemAt(i).applicationId;
				app.dsmccPid = appLists.itemAt(i).dsmccPid;
				app.isServiceBound = appLists.itemAt(i).isServiceBound;
				app.controlCode = appLists.itemAt(i).controlCode;
				app.name.setToExternal(appLists.itemAt(i).name, strlen(appLists.itemAt(i).name));
				app.priority = appLists.itemAt(i).priority;
				app.initialPath.setToExternal(appLists.itemAt(i).initialPath, strlen(appLists.itemAt(i).initialPath));
				Apps.push_back(app);
			}
			dsmccEvent.appLists = Apps;
		}
		auto ret = listener->dsmccSignalEvent(eventType, dsmccRootPath, dsmccEvent);
		if (!ret.isOk() && ret.isDeadObject()) {
			mDsmccListener = nullptr;
		} 
		LOG_INFO("[%s][%s][%d] back from callback", __FILE__, __FUNCTION__, __LINE__);
	}
}

Return<void> BtvMediaPlayer::setMediaCommunicationListener(const sp<IBtvMediaCommunicationListener>& mediaCommunicationListener) {
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	android::Mutex::Autolock l(&mNotifyLock);	
	if (mMediaCommunicationListener != nullptr) {
		LOG_INFO("[%s][%s] unlinkToDeath", __FILE__, __FUNCTION__);
		mMediaCommunicationListener->unlinkToDeath(this).isOk();
		mMediaCommunicationListener = nullptr;
	}
	if (mediaCommunicationListener != nullptr) {
		LOG_INFO("[%s][%s] linkToDeath", __FILE__, __FUNCTION__);
		mMediaCommunicationListener = mediaCommunicationListener;
		mMediaCommunicationListener->linkToDeath(this, 0 /*cookie*/).isOk();
	}
	::android::BtvMediaPlayerService::getInstance()->setMediaCommunicationListenerClient(mMediaCommunicationObserver);
	return Void();
}

void BtvMediaPlayer::familyStateEvent(int32_t state)
{
	LOG_INFO("[%s][%s][%d] familyStateEvent received state=%d", __FILE__, __FUNCTION__, __LINE__, state);

	Mutex::Autolock _l(mNotifyLock);

	sp<IBtvMediaCommunicationListener> listener = mMediaCommunicationListener;

	if (listener != nullptr)
	{
		LOG_INFO("[%s][%s][%d] callback application", __FILE__, __FUNCTION__, __LINE__);
		auto ret = listener->familyStateEvent(state);
		if (!ret.isOk() && ret.isDeadObject()) {
			mMediaCommunicationListener = nullptr;
		} 
		LOG_INFO("[%s][%s][%d] back from callback", __FILE__, __FUNCTION__, __LINE__);
	}
}

void BtvMediaPlayer::onCasInfoEvent(int32_t type, std::string casInfo)
{
	LOG_INFO("[%s][%s][%d] cas info received type=%d", __FILE__, __FUNCTION__, __LINE__, type);

	Mutex::Autolock _l(mNotifyLock);

	sp<IBtvMediaCommunicationListener> listener = mMediaCommunicationListener;

	if (listener != nullptr)
	{
		LOG_INFO("[%s][%s][%d] callback application", __FILE__, __FUNCTION__, __LINE__);
		auto ret = listener->onCasInfoEvent(type, casInfo);
		if (!ret.isOk() && ret.isDeadObject()) {
			mMediaCommunicationListener = nullptr;
		}
		LOG_INFO("[%s][%s][%d] back from callback", __FILE__, __FUNCTION__, __LINE__);
	}
}

Return<Status> BtvMediaPlayer::enableCasInfo(bool enable) {
	android::Mutex::Autolock l(&mNotifyLock);

	::android::BtvMediaPlayerService::getInstance()->enableCasInfo(enable);
	return Status::OK;
}

Return<void> BtvMediaPlayer::setAudioProgramUpdateListener(const sp<IBtvAudioProgramUpdateListener>& audiolistener)  {
	LOG_INFO("[%s][%s] called device [%d]", __FILE__, __FUNCTION__, mDeviceID);
	android::Mutex::Autolock l(&mNotifyLock);	
	if (mAudioProgramUpdateListener != nullptr) {
		LOG_INFO("[%s][%s] unlinkToDeath", __FILE__, __FUNCTION__);
		mAudioProgramUpdateListener->unlinkToDeath(this).isOk();
		mAudioProgramUpdateListener = nullptr;
	}
	if (audiolistener != nullptr) {
		LOG_INFO("[%s][%s] linkToDeath", __FILE__, __FUNCTION__);
		mAudioProgramUpdateListener = audiolistener;
		mAudioProgramUpdateListener->linkToDeath(this, 0 /*cookie*/).isOk();
	}
	::android::BtvMediaPlayerService::getInstance()->setAudioProgramUpdateClient(mAudioProgramUpdateObserver);
	return Void();
}

void BtvMediaPlayer::audioProgramUpdateEvent(char* filepath, int32_t updateType, int32_t audioPid)
{
	LOG_INFO("[%s][%s][%d] audioProgramUpdateEvent received filepath=%s, updateType=%d, audioPid=%d", __FILE__, __FUNCTION__, __LINE__, filepath, updateType, audioPid);

	Mutex::Autolock _l(mNotifyLock);

	sp<IBtvAudioProgramUpdateListener> listener = mAudioProgramUpdateListener;

	if (listener != nullptr)
	{
		LOG_INFO("[%s][%s][%d] callback application", __FILE__, __FUNCTION__, __LINE__);
		auto ret = listener->audioProgramUpdate(filepath, updateType, audioPid);
		if (!ret.isOk() && ret.isDeadObject()) {
			mAudioProgramUpdateListener = nullptr;
		} 
		LOG_INFO("[%s][%s][%d] back from callback", __FILE__, __FUNCTION__, __LINE__);
	}
}

}  // namespace implementation
}  // namespace V1_0
}  // namespace btvservice
}  // namespace skm
}  // namespace com

