#ifndef ___ALOG____H_____
#define ___ALOG____H_____

#include <android/log.h>

#define ALOGV( msg... )     __android_log_print( ANDROID_LOG_VERBOSE,      LOG_TAG, msg )
#define ALOGD( msg... )     __android_log_print( ANDROID_LOG_DEBUG,      LOG_TAG, msg )
#define ALOGI( msg... )     __android_log_print( ANDROID_LOG_INFO,      LOG_TAG, msg )
#define ALOGW( msg... )     __android_log_print( ANDROID_LOG_WARN,      LOG_TAG, msg )
#define ALOGE( msg... )     __android_log_print( ANDROID_LOG_ERROR,      LOG_TAG, msg )

#endif
