#ifndef _BTV_OEM_HAL__________H
#define _BTV_OEM_HAL__________H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

/**
**@brief DEVICE  handle
**  
*/
typedef void * DEVICE_HANDLE;

/**
 * @brief device led id.
 */
enum
{
	DEVICE_LED_POWER,
	DEVICE_LED_NETWORK,
	DEVICE_LED_AILED,
	DEVICE_LED_STATE,	//Definition for rcu keypressed
	DEVICE_LED_SOUND_EFFECT,
	DEVICE_LED_MAX,
};

/**
 * @brief device led state.
 */
enum
{
	DEVICE_LED_STATE_OFF = 0,
	DEVICE_LED_STATE_ON,
	DEVICE_LED_STATE_BLINK,
	DEVICE_LED_STATE_BOOT_UP,
	DEVICE_LED_STATE_BOOT_COMPLETE,
	DEVICE_LED_STATE_IDLE,
	
	DEVICE_LED_STATE_LISTEN_WAKEUP,				// 3-1 Wake-up
	DEVICE_LED_STATE_LISTEN_ACTIVE,				// 3-2 Listening Active
	DEVICE_LED_STATE_LISTEN_END,					// 3-3 Listening End
	DEVICE_LED_STATE_LISTEN_PROCESSING,			// 3-4 Processing
	DEVICE_LED_STATE_SPEAKING_SPEAKING,		// 4-1 Speaking
	DEVICE_LED_STATE_ERR_SPEAKING_SPEAKING,	// 4-2 Error Speaking
	DEVICE_LED_STATE_MIC_OFF,					// 5-1 Mic-Off
	DEVICE_LED_STATE_SPEAKER_MUTE,			// 6-1 Speaker Mute
	DEVICE_LED_STATE_SPEAKER_UNMUTE,		// 6-3 Volume up / unmute
	DEVICE_LED_STATE_SPEAKER_VOL_ZERO,		// 6-2 Volume Zero
	DEVICE_LED_STATE_SPEAKER_VOL_UP,		// 6-3 Volume up / unmute
	DEVICE_LED_STATE_SPEAKER_VOL_DOWN,		// 6-4 Volume down
	DEVICE_LED_STATE_SPEAKER_VOL_CONTINUOUS_CONTROL,		// 6-5 Volume continuous control
	DEVICE_LED_STATE_SYSTEM_ERR,					// 7 System Error
	DEVICE_LED_STATE_MIC_ON,					// 5-2 Mic-On
	
	DEVICE_LED_STATE_NUGU_CALL_OUTGOING = 100, 			// [NUGU CALL] 발신 시작
	DEVICE_LED_STATE_NUGU_CALL_INCOMING, 					// [NUGU CALL] 수신 시작
	DEVICE_LED_STATE_NUGU_CALL_ESTABLISHED, 				// [NUGU CALL] 전화 연결 됨 (수발신 공통)
	DEVICE_LED_STATE_NUGU_CALL_CLOSED, 					// [NUGU CALL] 통화가 정상적으로 종료됨
	DEVICE_LED_STATE_NUGU_CALL_CANCEL_BY_MT_BUSY, 			// [NUGU CALL] 상대방이 통화를 거부함
	DEVICE_LED_STATE_NUGU_CALL_CANCEL_BY_MO_BUSY, 			// [NUGU CALL] 상대방이 통화 중
	DEVICE_LED_STATE_NUGU_CALL_CANCEL_BY_MISSED_CALL, 		// [NUGU CALL] 상대방이 전화를 받지 않음
	DEVICE_LED_STATE_NUGU_CALL_CANCEL_BY_NETWORK_DROP, 	// [NUGU CALL] 네트웍이 불안정함
	DEVICE_LED_STATE_NUGU_CALL_CANCEL_BY_OEM_CALL, 		// [NUGU CALL] 상대방이 모바일일때 상대방의 전화로 OEM 전화가 수신되어 NUGU CALL 이 종료된 경우
	DEVICE_LED_STATE_NUGU_CALL_CANCEL_BY_INACTIVE, 		// [NUGU CALL] 상대방의 NUGU CALL 이 active 상태가 아님
	DEVICE_LED_STATE_NUGU_CALL_LOGOUT, 					// [NUGU CALL] 상대방이 NUGU 서비스에서 Logout 됨
	DEVICE_LED_STATE_NUGU_CALL_CANCEL_BY_POWER_OFF, 		// [NUGU CALL] 상대방의 기기가 모두 꺼져 있음
	DEVICE_LED_STATE_NUGU_CALL_CANCEL_BY_OUT_OF_SERVICE, 	// [NUGU CALL] 그외 기타 에러
	
	DEVICE_LED_STATE_TIMER_ON, 	// timer on
	DEVICE_LED_STATE_TIMER_OFF, 	// timer off
	DEVICE_LED_STATE_ALARM_ON, 	// alarm on
	DEVICE_LED_STATE_ALARM_OFF, 	// alarm off
	DEVICE_LED_STATE_BT_ON, 	// bluetooth on
	DEVICE_LED_STATE_BT_OFF, 	// bluetooth off

	DEVICE_LED_STATE_NETWORK_ERROR = 200, 	// network error
	DEVICE_LED_STATE_NETWORK_OK, 			// network ok	

	DEVICE_LED_STATE_PASSIVE_STANDBY = 220, 			// power passive standby	
	
	DEVICE_LED_STATE_SOUND_EFFECT_OFF = 300, 	// SOUND_EFFECT_OFF
	DEVICE_LED_STATE_SOUND_EFFECT_STANDARD, 	// SOUND_EFFECT_STANDARD	
	DEVICE_LED_STATE_SOUND_EFFECT_MUSIC, 	// SOUND_EFFECT_MUSIC
	DEVICE_LED_STATE_SOUND_EFFECT_MOVIE, 	// SOUND_EFFECT_MOVIE
	DEVICE_LED_STATE_SOUND_EFFECT_NEWS, 	// SOUND_EFFECT_NEWS
	DEVICE_LED_STATE_SOUND_EFFECT_AUTO, 	// SOUND_EFFECT_AUTO

};

/**
 * @brief device led color.
 */
enum
{
    DEVICE_LED_COLOR_RED = 0,
    DEVICE_LED_COLOR_BLUE,
    DEVICE_LED_COLOR_GREEN,
    DEVICE_LED_COLOR_YELLOW,
    DEVICE_LED_COLOR_MARGENTA,
    DEVICE_LED_COLOR_VIOLET,
    DEVICE_LED_COLOR_EX1,
    DEVICE_LED_COLOR_EX2,
    DEVICE_LED_COLOR_EX3,
    DEVICE_LED_COLOR_EX4,
	DEVICE_LED_COLOR_WHITE,
    DEVICE_LED_COLOR_ORANGE,
};

/**
** @brief
** creates DEVICE module and does initial works for this DEVICE module
**
** @param[out]	device		 	 return the device module handle created by this function
**
** @retval		0			 DEVICE_Create has successfully processed.
** @retval		-1 		 	parameters error occurred or Create return failed.
**
*/
int  DEVICE_Create(DEVICE_HANDLE *device);

/**
** @brief
** destroys DEVICE module
**
** @param[in]	device		 	 the device module handle
**
** @retval		0			 DEVICE_Destroy has successfully processed
** @retval		-1 		 	parameters error occurred or Destroy return failed
**
*/
int  DEVICE_Destroy(DEVICE_HANDLE device);

/**
 ** @brief
 ** set LED on/off on front panel.
 **
 ** @param[in]	 device      the device module handle
 ** @param[in]   led_id      Led id or 0 base index. see DEVICE_LED_XXX constants.
 ** @param[in]   state       new state of target led. see DEVICE_LED_STATE_XXX constants.
 **
 ** @retval 	 0			 DEVICE_SetLedState has successfully processed
 ** @retval 	 -1 		 parameters error occurred or DEVICE_SetLedState return failed
 */
int DEVICE_SetLedState(DEVICE_HANDLE device, int led_id, int state);

/**
 ** @brief
 ** set LED color on front panel.
 **
 ** @param[in]  device 		 the device module handle
 ** @param[in] 	led_id       Led id or 0 base index. see LED_XXX constants.
 ** @param[in]  color        color of target led. see LED_COLOR_XXX constants.
 **
 ** @retval 	 0			 DEVICE_SetLedColor has successfully processed
 ** @retval 	 -1 		 parameters error occurred or DEVICE_SetLedColor return failed
 */
int DEVICE_SetLedColor(DEVICE_HANDLE device, int led_id, int color);


/**
 ** @brief
 ** set LED on/off on front panel.
 **
 ** @param[in]  device 		 the device module handle
 ** @param[in] 	led_id       Led id or 0 base index. see DEVICE_LED_XXX constants.
 ** @param[in]  state        new state of target led. It is only for DEVICE_LED_STATE_SPEAKER_VOL_ZERO, DEVICE_LED_STATE_SPEAKER_VOL_UP,LED_STATE_SPEAKER_VOL_DOWN,LED_STATE_SPEAKER_VOL_CONTINUOUS_CONTROL.
 ** @param[in]  volume       volume value.
 ** 
 ** @retval 	 0			 DEVICE_SetVolumeLedState has successfully processed
 ** @retval 	 -1 		 parameters error occurred or DEVICE_SetVolumeLedState return failed
 */
int DEVICE_SetVolumeLedState(DEVICE_HANDLE device, int led_id, int state, int volume);



/**
 ** @brief
 ** set Degree of Beamforming.
 **
 ** @param[in]  device 		 the device module handle
 ** @param[in] 	led_id       Led id or 0 base index. see DEVICE_LED_XXX constants.
 ** @param[in]  state        new state of target led. see DEVICE_LED_STATE_XXX constants.
 ** @param[in]  degree       degree of beamforming. -60 ~ 60.
 **
 ** @retval 	 0			 DEVICE_SetBeamformingLedState has successfully processed
 ** @retval 	 -1 		 parameters error occurred or DEVICE_SetBeamformingLedState return failed
 */
int DEVICE_SetBeamformingLedState(DEVICE_HANDLE device, int led_id, int state, int degree);

/**
 ** @brief
 ** setting mic on/off .
 **
 ** @param[in]  device 		 the device module handle
 ** @param[in]	enable       defines set Mic Enable or not, AVP_FALSE 
 **                          means not , AVP_TRUE means set enable
 **
 ** @retval 	 0			 DEVICE_SetMicOnOff has successfully processed
 ** @retval 	 -1 		 parameters error occurred or DEVICE_SetMicOnOff return failed
 */
int DEVICE_SetMicOnOff(DEVICE_HANDLE device, bool enable);

/**
 ** @brief
 ** Getting current level of volume
 **
 ** @param[in]  device 		 the device module handle
 ** @param[out] volume_level level of Volume
 **
 ** @retval 	 0			 DEVICE_GetMicVolume has successfully processed
 ** @retval 	 -1 		 parameters error occurred or DEVICE_GetMicVolume return failed
 */
int DEVICE_GetMicVolume(DEVICE_HANDLE device, int *volume_level);


/**
 ** @brief
 ** Setting level of mic volume
 **
 ** @param[in]  device 		 the device module handle
 ** @param[in]  volume_level level of Volume
 **
 ** @retval 	 0			 DEVICE_SetMicVolume has successfully processed
 ** @retval 	 -1 		 parameters error occurred or DEVICE_SetMicVolume return failed
 */
int DEVICE_SetMicVolume(DEVICE_HANDLE device, int volume_level);


/**
 ** @brief
 ** Getting current level of echo
 **
 ** @param[in]  device 		 the device module handle
 ** @param[out] echo_level   level of echo
 **
 ** @retval 	 0			 DEVICE_GetMicEchoLevel has successfully processed
 ** @retval 	 -1 		 parameters error occurred or DEVICE_GetMicEchoLevel return failed
 */
int DEVICE_GetMicEchoLevel(DEVICE_HANDLE device, int *echo_level);


/**
 ** @brief
 ** Setting level of echo
 **
 ** @param[in]  device 		 the device module handle
 ** @param[in]  echo_level   level of echo
 **
 ** @retval 	 0			 DEVICE_SetMicEchoLevel has successfully processed
 ** @retval 	 -1 		 parameters error occurred or DEVICE_SetMicEchoLevel return failed
 */
int DEVICE_SetMicEchoLevel(DEVICE_HANDLE device, int echo_level);

/**
 ** @brief
 ** pause mic
 **
 ** @param[in]  device 		 the device module handle
 **
 ** @retval 	 0			 DEVICE_MicPause has successfully processed
 ** @retval 	 -1 		 parameters error occurred or DEVICE_MicPause return failed
 */
int DEVICE_MicPause(DEVICE_HANDLE device);

/**
 ** @brief
 ** resume mic
 **
 ** @param[in]  device 		 the device module handle
 **
 ** @retval 	 0			 DEVICE_MicResume has successfully processed
 ** @retval 	 -1 		 parameters error occurred or DEVICE_MicResume return failed
 */
int DEVICE_MicResume(DEVICE_HANDLE device);

#ifdef __cplusplus
}
#endif

#endif //_BTV_HAL_________H
