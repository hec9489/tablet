#ifndef _BTV_TUNER_HAL__________H
#define _BTV_TUNER_HAL__________H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#define PREFIX_TUNER_TYPE_RTP "rtp://"
#define PREFIX_TUNER_TYPE_FILE "file://"

typedef void *TUNER_HANDLE;
typedef void *DEMUX_HANDLE;
typedef void *FILTER_HANDLE;

typedef int (*FUNC_POST_READ_PACKETS)	(intptr_t *param, unsigned char *itsbuf, int size);
typedef int (*FUNC_FEED_TS_PACKETS)	(intptr_t *param, unsigned char *itsbuf, int size);
typedef int (*FUNC_EOS)	(intptr_t *param);

typedef int (*FUNC_FILTER_CALLBACK) (FILTER_HANDLE filterHandle, void *ibuf, int size);

typedef enum
{
	TUNER_TYPE_IP = 0,
	TUNER_TYPE_FILE,
	TUNER_TYPE_CABLE,
} BTV_TUNER_TYPE;

typedef enum
{
	FILTER_TYPE_TS = 0,
	FILTER_TYPE_SECTION,
} BTV_FILTER_TYPE;

TUNER_HANDLE TUNER_createTuner(BTV_TUNER_TYPE tunerType);

int TUNER_tune(TUNER_HANDLE pTunerHandle, DEMUX_HANDLE pDemuxHandle,
			   char *pUri, intptr_t *param,
			   FUNC_POST_READ_PACKETS postReadPackets,
			   FUNC_FEED_TS_PACKETS feedTSPackets,
			   FUNC_EOS endOfStream);

int TUNER_getNumTuners();

int	TUNER_getNumFilters();

FILTER_HANDLE TUNER_getFilter(BTV_FILTER_TYPE type, int idx);

//int	TUNER_getDemux

int	TUNER_getState(TUNER_HANDLE pTunerHandle);

int	TUNER_stop(TUNER_HANDLE pTunerHandle);

int	TUNER_destroy(TUNER_HANDLE pTunerHandle);

//int	TUNER_readTSPacket();

FILTER_HANDLE FILTER_create();

int FILTER_addDemux(FILTER_HANDLE pFilterHandle, DEMUX_HANDLE pDemuxHandle);

int	FILTER_setParam(FILTER_HANDLE pFilterHandle, int pid, int table_id, int posMask, int negMask);

void FILTER_setCallBack(FILTER_HANDLE pFilterHandle, FUNC_FILTER_CALLBACK filterSdtCallBack);

//int	FILTER_isMatchedSection(TUNER_HANDLE pTunerHandle, ifSection_t *pSection);

int	FILTER_destroy(FILTER_HANDLE pFilterHandle);

//int	FILTER_read();

DEMUX_HANDLE DEMUX_create();
int	DEMUX_addTuner(DEMUX_HANDLE pDemuxHandle, TUNER_HANDLE pTunerHandle);
int	DEMUX_delTuner(DEMUX_HANDLE pDemuxHandle, TUNER_HANDLE pTunerHandle);
//int	DEMUX_addFilter //filter init에 추가되어있음..
//int	DEMUX_delFilter
//int	DEMUX_setInterval

int	DEMUX_destroy(DEMUX_HANDLE pDemuxHandle);

//int	DEMUX_feedTSPackets
//int	DEMUX_feedSection
//int	DEMUX_swFilterTSPacket
//int	DEMUX_swFilterTSPackets

#ifdef __cplusplus
}
#endif

#endif //_BTV_HAL_________H
