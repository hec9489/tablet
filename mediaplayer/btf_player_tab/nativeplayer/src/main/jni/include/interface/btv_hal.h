#ifndef _BTV_HAL__________H
#define _BTV_HAL__________H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

/**
**@brief player handle
**
** When player create by AVP_Create this handle will be returned. Then, when calling the other player\n
** functions,  this handle will be regarded as a necessary input parameters.
** 
** @see AVP_Create
*/
typedef void * AVP_HANDLE;

/**
**@brief Audio Mixer  handle
**  
*/
typedef void * AMIXER_HANDLE;

/**
**@brief Video Compositor  handle
**  
*/
typedef void * VCOMPO_HANDLE;

/**
**@brief DISPLAY  handle
**  
*/
typedef void * DISPLAY_HANDLE;

/**
 * @brief Player input type
 *
 * AVP_INPUT_URL:
 * 			URL is given.
 *
 * AVP_INPUT_MEMORY:
 * 			MPEG TS will be fed using memory input.
 * 			Used for file playing or IPTV playing.
 */
typedef enum
{
	AVP_INPUT_URL, /*!< data input from url*/
	AVP_INPUT_MEMORY, /*!< data input from memory.*/
}
AVP_InputType;

/**
 * @brief Play Media type
 *
 */
typedef enum
{
	AVP_IPTV, /*!< IPTV */
	AVP_VOD,  /*!< VOD */
	AVP_FILE, /*!< FILE */
}
AVP_MediaType;

/**
 * @brief URL address size.
 */
#define AVP_MAX_URL_ADDRESS_LENGTH	 256


/**
 *@brief Codec type.
 * 
 */
typedef enum
{
	AVP_CODEC_NONE,

	//	audio.
	AVP_CODEC_AUDIO_MPEG1,
	AVP_CODEC_AUDIO_MPEG2,

	AVP_CODEC_AUDIO_AAC,
	AVP_CODEC_AUDIO_AAC_PLUS,

	AVP_CODEC_AUDIO_AC3,
	AVP_CODEC_AUDIO_AC3_TRUE_HD,
	AVP_CODEC_AUDIO_AC3_PLUS,
	AVP_CODEC_AUDIO_EAC3,

	AVP_CODEC_AUDIO_DTS,
	AVP_CODEC_AUDIO_DTS_HD,
	AVP_CODEC_AUDIO_DTS_HD_MA,

	AVP_CODEC_AUDIO_LPCM,
	AVP_CODEC_AUDIO_MP3,

	//video.
	AVP_CODEC_VIDEO_MPEG1,
	AVP_CODEC_VIDEO_MPEG2,
	AVP_CODEC_VIDEO_MPEG4_PART2,

	AVP_CODEC_VIDEO_H264,
	AVP_CODEC_VIDEO_H265,
}
AVP_Codec;

/**
 *@brief Audio codec info structure.
 *
 */
typedef struct
{
	uint16_t	pid;					/*!< pid of audio stream. can be invalid (0).*/
	AVP_Codec 	codec;					/*!< Audio coded type, can be invalid(AVP_CODEC_NONE) at first.*/
}
AVP_AudioConfig;


/**
*@brief Function pointer for section callback
*
* @param       section       the buffer pointer of the section data.
* @param       length        section data length.
* @param       user_param    user parameter.
*
* @retval        void 
*/
typedef void (*AVP_SectionFilterCallback)(const unsigned char *section, const unsigned int length, void *section_user_param);

/**
* @brief Section filter info sturcture.
*/
typedef struct
{
	uint16_t pid; 							/*!< Filter PID*/
	uint8_t  tid;							/*!< Table ID*/
	AVP_SectionFilterCallback callback;     /*!< callback function pointer, @see AVP_SectionFilterCallback*/
	void * section_user_param;	    		/*!< user section callback parameter (should be delivered via user_param when callback)*/
}
AVP_SectionFilterConfig;


/**
 * @brief player callback event.
 * 
 */
typedef enum
{
	//	player events.
	AVP_FIRST_IFRAME_DISPLAYED,		/*!< when first i-frame displayed*/
	AVP_FIRST_AUDIO_FRAME_DECODED, 	/*!< when first audio-frame decoded by audeo codec*/
	AVP_EOS,						/*!< end of stream*/
	AVP_PICTURE_USER_DATA,     		/*!< data->closecaptiop*/
	AVP_PLAYER_TIME_TICK,
	AVP_PLAYER_INIT 				= 0x10,
	AVP_PLAYER_BUFFERING,
	AVP_PLAYER_BUFFERING_END,
	AVP_PLAYER_START,
	AVP_PLAYER_PAUSE,
	AVP_PLAYER_STOP,
	//	...TBD.
} AVP_MediaCallbackEvent;

/**
 * @brief  event callback function.
 * 
 * @param  event            event type.
 * @param data              event data buffer pointer.
 * @param data_size		    event data size.
 * @param user_param        user  parameters.
 *
 */
typedef void (*AVP_MediaCallback)(AVP_MediaCallbackEvent event, void * data, uint32_t data_size, void * callback_user_param);

/**
 * @brief  decoder error callback function.
 * 
 * @param data              error data buffer pointer.
 * @param data_size		    error data size.
 * @param user_param        user  parameters.
 *
 */
typedef void (*AVP_DecoderErrorCallback)(void * data, uint32_t data_size, void * callback_user_param);

/**
 * @brief Window plane type
 *
 * Video window type, only kinds of video output types,
 * MAIN or PIP on the window to be managed.
 */
typedef enum
{
    AVP_VIDEO_MAIN, 	/*!< main window plane*/
    AVP_VIDEO_PIP,    /*!< pip window plane*/
    AVP_VIDEO_MAX  	/*!< window plane max number.*/
} AVP_VIDEO_Plane;


/**
 * @brief Zorder plane type
 *
 */
typedef enum
{
    VID_MAIN, 	/*!< main video plane*/
    VID_PIP,    /*!< pip video plane*/
    GFX_MAIN,  	/*!< main gfx planer.*/
    GFX_PIP,  	/*!< pip gfx plane.*/    
} VIDEO_Zorder_Plane;

/**
 * @brief Video ScaleRatio Mode
 *
 */
typedef enum
{
    AVP_ASPECT_RATIO_ORIGINAL_SCREEN, 	/*!< original screen*/
    AVP_ASPECT_RATIO_FULL_SCREEN,    	/*!< full screen*/
} AVP_AspectRatioMode;

/**
 * @brief Audio Output Mode type
 *
 */
typedef enum
{
    AVP_AUDIO_PASSTHROUGH, 	/*!< dolby*/
    AVP_AUDIO_PCM,    		/*!< normal */
} AVP_AudioOutputMode;


/**
 * @brief Sound Effect type
 *
 */
typedef enum
{
    AVP_SOUND_EFFECT_OFF, 		/*!< off*/
    AVP_SOUND_EFFECT_STANDARD,   /*!< standard */
    AVP_SOUND_EFFECT_MUSIC, 		/*!< music*/
    AVP_SOUND_EFFECT_MOVIE, 		/*!< movie*/
    AVP_SOUND_EFFECT_NEWS, 		/*!< news*/
} AVP_SoundEffect;


/**
 *@brief Create Config
 * 
 */
typedef struct
{
	AVP_MediaCallback function;    /*!< callback function pointer*/  
	void * callback_user_param;				/*!< callback user_param.*/
	AVP_VIDEO_Plane	output;				/*!< video output plane for Main or PIP*/
}
AVP_CreateConfig;

/**
 *@brief Player Config
 * 
 */

typedef struct
{
	AVP_InputType 	type;    									/*!< specify url or memory input.*/
	uint8_t 		url_address[AVP_MAX_URL_ADDRESS_LENGTH];	/*!< url addresss */
	uint16_t		video_codec;   
	uint16_t		audio_codec;   
	uint16_t		video_pid;	   
	uint16_t		audio_pid;	   
	uint16_t		pcr_pid;	   
	bool			video_enable;  
	bool			audio_enable;  

	bool			clock_recovery_enable;
	uint16_t		playback_latency; 					/*!< milli seconds */
	bool			display_first_frame_early;
	bool			keep_last_frame;	
}
AVP_PlayerConfig;

/**
 * @brief AV(Audio/Video)  type
 *
 * AV type
 */
typedef enum
{
    AVP_PES_VIDEO,    /*!< video */
    AVP_PES_AUDIO,    /*!< audio */
    AVP_PES_PCR,      /*!< pcr */
} AVP_PES_type;

/**
 * @brief injectCommand
 *
 */
typedef enum
{
	AVP_INJECT_EOS, /*!< EOS */
	//	...TBD.		
}
AVP_InjectCmdType;

/**
 * @brief AVP_AVoffset
 *
 */
typedef struct
{
	AVP_PES_type type;   /* !<Audio/Video type  */
	int delay_time_ms; /* !<time to delay(millisecond) for AVtype */
} AVP_AVoffset;

/**
 *@brief display resolution.
 * 
 */
typedef enum
{
	BTF_DISPLAY_MODE_AUTO = 0,
	BTF_DISPLAY_MODE_720P50HZ = 1,
	BTF_DISPLAY_MODE_720P5994HZ = 2,
	BTF_DISPLAY_MODE_720P60HZ = 3,
	BTF_DISPLAY_MODE_1080I50HZ = 4,
	BTF_DISPLAY_MODE_1080I5994HZ = 5,
	BTF_DISPLAY_MODE_1080I60HZ = 6,
	BTF_DISPLAY_MODE_1080P50HZ = 7,
	BTF_DISPLAY_MODE_1080P5994HZ = 8,
	BTF_DISPLAY_MODE_1080P60HZ = 9,
	BTF_DISPLAY_MODE_720P_3DOU_AS = 10,
	BTF_DISPLAY_MODE_1080P24HZ = 11,
	BTF_DISPLAY_MODE_1080P24HZ_3DOU_AS = 12,
	BTF_DISPLAY_MODE_3840x2160P2997HZ = 13,
	BTF_DISPLAY_MODE_3840x2160P30HZ = 14,
	BTF_DISPLAY_MODE_3840x2160P5994HZ = 15,
	BTF_DISPLAY_MODE_3840x2160P60HZ = 16,

}
BTF_DISPLAY_Resolution;

typedef enum {
    COMMAND_CHANGE_CONFIG_LASTFRAME = 1,
    COMMAND_CHANGE_NO_AVSYNC_MODE = 2,
} AVP_REQUEST_COMMAND;

/**
*@brief Function pointer for HDMI Hot Plugged event callback
*
* @param       event         1: Plugged, 0: not plugged 
*
* @retval        void 
*/
typedef void (*DISPLAY_HDMIHotPluggedCallback)(int event);

/**
*@brief Function pointer for CEC Power Stae event callback
*
* @param       event         1: Power On, 0: Power Off
*
* @retval        void 
*/
typedef void (*DISPLAY_CECPowerStateCallback)(int event);

/**
*@brief Function pointer for HDMI event callback
* add BTF_LEVEL_2
*
* @param       event         1: Plugged, 0: not plugged 
* @param       data_param       data
*
* @retval        void 
*/
typedef void (*DISPLAY_HDMIEventCallback)(int event, void* data_param);

/**
*@brief Function pointer for CEC event callback
* add BTF_LEVEL_2
*
* @param       event         1: TV power off, 0: TV power on
* @param       data_param       data
*
* @retval        void 
*/
typedef void (*DISPLAY_CECEventCallback)(int event, void* data_param);

/**
*@brief Function pointer for HDMI|CEC message callback
* add BTF_LEVEL_2
*
* @param       event         1: HDMI, 0: CEC
* @param       message       send/recv message
*
* @retval        void 
*/
typedef void (*DISPLAY_HDMI_CECMessageCallback)(int type, void* message);



/**
** @brief
** creates player and does initial works for this player
**
** @param[out]	player		 	return the Player handle created by this function
** @param[in]	config			the configs used when creating the player
**
** @retval		0			 AVP_Create has successfully processed.
** @retval		-1 		 	 parameters error occurred or Create return failed.
**
*/
int  AVP_Create(AVP_HANDLE *player, AVP_CreateConfig *config);

/**
** @brief
** destroys the player
**
** @param[in]	player		 	 the player handle
**
** @retval		0			 AVP_Destroy has successfully processed
** @retval		-1 		 parameters error occurred or Destroy return failed
**
*/
int  AVP_Destroy(AVP_HANDLE player);

/**
** @brief
** starts player
**
** @param[in]	player		 	the player handle
** @param[in]	config			the configs used when starting the player
**
** @retval		0			the player starts playing successfully.
** @retval		-1 		parameters error occurred or the player doesn't start to play.
**
*/
int  AVP_Start(AVP_HANDLE player, AVP_PlayerConfig *config);

/**
** @brief
** stops player and keep the last frame
**
** @param[in]	player		 	 the Player handle 
**
** @retval		0			 the player is stopped playing successfully
** @retval		-1 		 error occurred or the player doesn't stop to play
**
*/
int  AVP_Stop(AVP_HANDLE player);

/**
** @brief
** pauses the player
**
** @param[in]	player		 	 the Player handle
**
** @retval		0			 AVP_Pause has successfully processed
** @retval		-1 		 error occurred or the player doesn't pause
**
*/
int  AVP_Pause(AVP_HANDLE player);

/**
** @brief
**resumes the Player
**
** @param[in]	player		 	 the Player handle
**
** @retval		0			 the player replays again successfully
** @retval		-1 		 error occurred or the player doesn't replay.
**
*/
int  AVP_Resume(AVP_HANDLE player);

/**
** @brief
**feeds some TS data used for playing in input_buffer
**
** @param[in]		player		handle of player to reserve play data and information
** @param[in]		buffer 		define input buffer addr
** @param[in]		size 		define input size
**
** @retval		0			AVP_InjectTS has successfully processed.
** @retval		-1 		parameters error occurred or file size not packet align
**
*/
int  AVP_InjectTS(AVP_HANDLE player, char *buffer, int size);

/**
** @brief
** performs the corresponding operation according to the command given. feeds some TS data used for playing in input_buffer
**
** @param[in]		player		handle of player to reserve play data and information
** @param[in]		command 	command of the inject to be performed
**
** @retval		0			AVP_InjectCommand has successfully processed.
** @retval		-1 			error occurred
**
*/
int  AVP_InjectCommand(AVP_HANDLE player, AVP_InjectCmdType command);

/**
** @brief
** gets the current displaying frame's pts
**
** @param[in]		player		handle of player to deal with	
** @param[out]		*pts 	 	get the current displaying frame's pts
**
** @retval		0			the current displaying PTS has got successfully
** @retval		-1 		parameters error occurred or PTS get failed
**
*/
int  AVP_GetVideoPTS(AVP_HANDLE player, long long *pts);

/**
** @brief
**Drops all previously feed TS data from internal buffer which is not yet processed
**
** @param[in]	player		 	 the Player handle
**
** @retval		0			 AVP_FlushTS has successfully processed
** @retval		-1 		 error occurred
**
*/
int  AVP_FlushTS(AVP_HANDLE player);

/**
** @brief
** sets the speed of play
**
** @param[in]	player		 	handle of player to deal with	
** @param[in]	speed 	 		multiplier factor of change
**
** @retval		0			the play speed has changed successfully.
** @retval		-1 		parameters error occurred or the play speed change failed.
**
*/
int  AVP_SetPlaySpeed(AVP_HANDLE player, float speed);

/**
** @brief
** sets the window area by specific size
**
** @param[in]	player		 	handle of player to deal with	
** @param[in]	x 	 			the starting point of x coordinates
** @param[in]	y 	 			the starting point of y coordinates
** @param[in]	width 	 		window width
** @param[in]	height 	 		window height
**
** @retval		0			AVP_SetVideoPlaneSize has successfully processed
** @retval		-1 		error occurred or window size set failed.
**
*/
int  AVP_SetVideoPlaneSize(AVP_HANDLE player, int x, int y, int width, int height);

/**
** @brief
** sets android native window 
**
** @param[in]	player		 	handle of player to deal with	
** @param[in]	nativeWidow		Pointer to the native window of the surface
**
** @retval		0			AVP_SetVideoPlaneSize has successfully processed
** @retval		-1 		error occurred or window size set failed.
**
*/
int  AVP_SetSurface(AVP_HANDLE player, void *nativeWidow);


/**
** @brief
** sets the audio/video delay by specific millisecond
**
** @param[in]	player		 	handle of player to deal with	
** @param[in]	offset      	time to delay(millisecond) for AVtype
**
** @retval		0			AVP_SetAVoffset has successfully processed
** @retval		-1 		error occurred or video delay set failed.
**
*/
int  AVP_SetAVoffset(AVP_HANDLE player, AVP_AVoffset offset);

/**
** @brief
** sets the player with specific handle video mute.
**
**
** @param[in]		player		handle of player to deal with
** @param[in]		mute 		defines set vido mute or not, FALSE 
**								means not , TRUE means set mute
**
** @retval		0			set the specific player mute successfully.
** @retval		-1 			error occurred or set mute failed.
**
*/
int  AVP_SetVideoMute(AVP_HANDLE player, bool mute);


/**
** @brief
** sets the player with specific handle audio mute.
**
** NOT system wide mute, just for player. 
**
** @param[in]		player		handle of player to deal with
** @param[in]		mute 		defines set audio mute or not, AVP_FALSE 
**								means not , AVP_TRUE means set mute
**
** @retval		0			set the specific player mute successfully.
** @retval		-1 		error occurred or set mute failed.
**
*/
int  AVP_SetAudioMute(AVP_HANDLE player, bool mute);

/**
** @brief
** gets a specific player audio mute status
**
** @param[in]		player		 	handle of player to deal with
** @param[out]		mute_status 	get the value of audio mute status currently,
**									AVP_FALSE means not mute, AVP_TRUE means mute 
**
** @retval		0				get the specific player mute status successfully.
** @retval		-1 			error occurred or got mute status failed. 
**
*/
int  AVP_GetAudioMute(AVP_HANDLE player, bool *mute_status);

/**
** @brief
** gets btv media audio volume
**
**
** @param[in]   amixer          the audio mixer handle
** @param[out]  volume          volume value to be get, [min..max][0..32]
**
** @retval              0                       AVP_GetAudioVolume successfully.
** @retval              -1                      error occurred or AVP_GetAudioVolume failed.
**
*/
int  AVP_GetMediaAudioVolume(AVP_HANDLE player, int *volume);

/**
** @brief
** sets btv media audio volume
**
** @param[in]   amixer          the audio mixer handle
** @param[in]   volume          volume value to be set, [min..max][0..32]
**
** @retval              0                       AVP_SetAudioVolume successfully.
** @retval              -1                      error occurred or AVP_SetAudioVolume failed.
**
*/
int  AVP_SetMediaAudioVolume(AVP_HANDLE player, int volume, float gain);

/**
** @brief
** changes audio pid, audio codec type, then play back the audio
**
** @param[in]		player		 	handle of player to change audio configuration
** @param[in]		audio		 	pointer to a prepared AVP_AudioConfig object
**
** @retval		0				change audio and play successfully.
** @retval		-1 			parameter input error such as player or audio equals NULL.
**
*/
int  AVP_SetAudioStream(AVP_HANDLE player, AVP_AudioConfig *audio);
/**
** @brief
** opens and sets sectionFilter by specific pid and tid with the player handle
**
** @param[in]		player		 	handle of player to add section filter
** @param[in]		config			defines config pid, tid
**								    user param and a function pointer to section filter callback
**
** @retval		0			    setSectionFilter has successfully processed
** @retval		-1 			parameters error occurred or filter open failed.
**
*/
int  AVP_SetSectionFilter(AVP_HANDLE player, AVP_SectionFilterConfig *config);

/**
** @brief
** stops sectionFilter, then sectionFilter will not work 
**
** @param[in]	player		 	handle of player 
**
** @retval		0			section filter have been stopped successfully.
** @retval		-1 		error occurred or PID filter remove failed.
**
*/
int  AVP_ClearSectionFilter(AVP_HANDLE player);

/**
** @brief
** performs the corresponding operation according to the command given.
**
** @param[in]	player		 	handle of player 
** @param[in]	request		 	command of the operation to be performed
** @param[in]	in_param		parameter to be passed to the corresponding command
** @param[out]	out_param		buffer pointer to store the result of the command
**
** @retval		0			AVP_SetPlayerControl has successfully processed
** @retval		AVP_INVALID		Invalid command.
** @retval		-1 		error occurred or command perform failed.
**
*/
int  AVP_SetCommand(AVP_HANDLE player, int request, void* in_param, void* out_param);


/**
** @brief
** Error Recovery and Concealment.
**
** @param[in]	player		 	handle of player 
** @param[in]	threshold		set error recovery and Concealment values [0-255]
**                              0 : disabled. Present all video frames regardless of its completion.
**                                  Don't conceal any video frame.
**                              127 : enabled. Present video frames which are more than 50% of completion.
**                                  Conceal video frames which are less than 50% of completion.
**                              255 : enabled. Present video frames which are complete only.
**                                  Conceal all video frames which are not complete.
** @param[out]	out_param		buffer pointer to store the result of the command
**
** @retval		0			successfully set value
** @retval		-1			failed to set value
**
*/
int  AVP_SetErrorConcealment(AVP_HANDLE player, int threshold);

/**
** @brief
** reports video codec errors as callbacks..
**
** @param[in]	player		 	handle of player 
** @param[in]	errorCallback		 	Function pointer for error callback
** @param[in] user_param    user parameter.
**
** @retval		0			AVP_VIDEO_SetErrorConcealment has successfully processed
** @retval		AVP_INVALID		Invalid command.
** @retval		-1 		error occurred or command perform failed.
**
*/
int AVP_VIDEO_SetErrorConcealment(AVP_HANDLE player, AVP_DecoderErrorCallback errorCallback, void *user_param);


int  AVP_ScreenCapture(AVP_HANDLE player);


/**
** @brief
** creates Audio Mixer  and does initial works for this Audio Mixer
**
** @param[out]	amixer		return the audio mixer handle created by this function
**
** @retval		0			AMIXER_Create has successfully processed.
** @retval		-1 		 	parameters error occurred or Create return failed.
**
*/
int  AMIXER_Create(AMIXER_HANDLE *amixer);

/**
** @brief
** destroys Audio Mixer
**
** @param[in]	amixer		the audio mixer handle
**
** @retval		0			AMIXER_Destroy has successfully processed
** @retval		-1 		 	parameters error occurred or Destroy return failed
**
*/
int  AMIXER_Destroy(AMIXER_HANDLE amixer);


/**
** @brief
** sets btv audio volume
**
** @param[in]	amixer		the audio mixer handle
** @param[in]	volume		volume value to be set, [min..max][0..32]
**
** @retval		0			AVP_SetAudioVolume successfully.
** @retval		-1 			error occurred or AVP_SetAudioVolume failed.
**
*/
int  AMIXER_SetAudioVolume(AMIXER_HANDLE amixer, int volume, float gain);


/**
** @brief
** gets btv  audio volume
**
**
** @param[in]	amixer		the audio mixer handle
** @param[out]	volume		volume value to be get, [min..max][0..32]
**
** @retval		0			AVP_GetAudioVolume successfully.
** @retval		-1 			error occurred or AVP_GetAudioVolume failed.
**
*/
int  AMIXER_GetAudioVolume(AMIXER_HANDLE amixer, int *volume);

/**
** @brief
** sets system wide audio mute.
**
**
** @param[in]	amixer		the audio mixer handle
** @param[in]	mute 		defines set audio mute or not, AVP_FALSE 
**							means not , AVP_TRUE means set mute
**
** @retval		0			set the specific master audio mute successfully.
** @retval		-1 			error occurred or set mute failed.
**
*/
int  AMIXER_SetMasterAudioMute(AMIXER_HANDLE amixer, bool mute);

/**
** @brief
** sets system wide audio volume
**
** @param[in]	amixer		the audio mixer handle
** @param[in]	volume		volume value to be set, [min..max][0..32]
**
** @retval		0			AMIXER_SetMasterAudioVolume successfully.
** @retval		-1 			error occurred or AMIXER_SetMasterAudioVolume failed.
**
*/
int  AMIXER_SetMasterAudioVolume(AMIXER_HANDLE amixer, int volume);


/**
** @brief
** gets system wide audio volume
**
**
** @param[in]	amixer		the audio mixer handle
** @param[out]	volume		volume value to be get, [min..max][0..32]
**
** @retval		0			AMIXER_GetMasterAudioVolume successfully.
** @retval		-1 			error occurred or AMIXER_GetMasterAudioVolume failed.
**
*/
int  AMIXER_GetMasterAudioVolume(AMIXER_HANDLE amixer, int *volume);


/**
** @brief
** sets the player with specific handle audio output mode.
**
**
** @param[in]	amixer		the audio mixer handle
** @param[in]	mode 		audio output mode
**
** @retval		0			AMIXER_SetAudioOutputMode successfully.
** @retval		-1 			error occurred or AMIXER_SetAudioOutputMode failed.
**
*/
int  AMIXER_SetAudioOutputMode(AMIXER_HANDLE amixer, AVP_AudioOutputMode mode);

/**
** @brief
** gets audio output mode.
**
**
** @param[in]	amixer		the audio mixer handle
** @param[out]	mode 		audio output mode
**
** @retval		0			AMIXER_GetAudioOutputMode successfully.
** @retval		-1 			error occurred or AMIXER_GetAudioOutputMode failed.
**
*/
int  AMIXER_GetAudioOutputMode(AMIXER_HANDLE amixer, AVP_AudioOutputMode *mode);

/**
** @brief
** sets the player with specific handle audio AURO-3D mode.
**
**
** @param[in]	amixer		the audio mixer handle
** @param[in]	mode 		sound effect mode
**
** @retval		0			AMIXER_SetSoundEffect successfully.
** @retval		-1 			error occurred or AMIXER_SetSoundEffect failed.
**
*/
int  AMIXER_SetSoundEffect(AMIXER_HANDLE amixer, AVP_SoundEffect mode);

/**
** @brief
** gets audio AURO-3D mode.
**
**
** @param[in]	amixer		the audio mixer handle
** @param[out]	mode 		sound effect mode
**
** @retval		0			AMIXER_GetSoundEffect successfully.
** @retval		-1 			error occurred or AMIXER_GetSoundEffect failed.
**
*/
int  AMIXER_GetSoundEffect(AMIXER_HANDLE amixer, AVP_SoundEffect *mode);


/**
** @brief
** sets audio snoop mode.
**
** @retval		0			AMIXER_SetAudioSnoop successfully.
** @retval		-1 			error occurred or AMIXER_SetAudioSnoop failed.
**
*/
int AMIXER_SetAudioSnoop(bool enable);

/**
** @brief
** creates Video Compositor  and does initial works for this Video Compositor
**
** @param[out]	vcompo		 	 return the video compositor handle created by this function
**
** @retval		0			 VCOMPO_Create has successfully processed.
** @retval		-1 		 parameters error occurred or Create return failed.
**
*/
int  VCOMPO_Create(VCOMPO_HANDLE *vcompo);

/**
** @brief
** destroys Video Compositor 
**
** @param[in]	vcompo		 	 the video compositor handle
**
** @retval		0			 VCOMPO_Destroy has successfully processed
** @retval		-1 		 parameters error occurred or Destroy return failed
**
*/
int  VCOMPO_Destroy(VCOMPO_HANDLE vcompo);

/**
** @brief
** sets the video zorder 
**
** @param[in]	vcompo		 	the video compositor handle
** @param[in]	zorder1	 		VIDEO_Zorder_Plane zorder1 
** @param[in]	zorder2	 		VIDEO_Zorder_Plane zorder2 
** @param[in]	zorder3	 		VIDEO_Zorder_Plane zorder3 
** @param[in]	zorder4	 		VIDEO_Zorder_Plane zorder4 
**
** @retval		0				VCOMPO_SetZorder has successfully processed
** @retval		-1 				error occurred or Zorder set failed.
**
*/
int  VCOMPO_SetZorder(VCOMPO_HANDLE vcompo, VIDEO_Zorder_Plane zorder1, VIDEO_Zorder_Plane zorder2, 
											VIDEO_Zorder_Plane zorder3, VIDEO_Zorder_Plane zorder4);


/**
** @brief
** sets the video scale ratio by specific mode 
**
** @param[in]	vcompo		 	the video compositor handle
** @param[in]	mode 	 		Video ScaleRatio Mode  
**
** @retval		0			VCOMPO_SetScaleRatioMode has successfully processed
** @retval		-1 		error occurred or ScaleRatio set failed.
**
*/
int  VCOMPO_SetScaleRatioMode(VCOMPO_HANDLE vcompo, AVP_AspectRatioMode mode);

/**
** @brief
** sets brightness
**
** @param[in]	vcompo		 	the video compositor handle
** @param[in]	brightness		brightness value
**
** @retval		0			VCOMPO_SetVideoBrightness successfully.
** @retval		-1 		error occurred or VCOMPO_SetVideoBrightness failed.
**
*/
int  VCOMPO_SetVideoBrightness(VCOMPO_HANDLE vcompo, int brightness);

/**
** @brief
** gets brightness value
**
** @param[in]	vcompo		 	the video compositor handle
** @param[out]	brightness		brightness value
**
** @retval		0			VCOMPO_GetVideoBrightness successfully.
** @retval		-1 		error occurred or VCOMPO_GetVideoBrightness failed.
**
*/
int  VCOMPO_GetVideoBrightness(VCOMPO_HANDLE vcompo, int *brightness);

/**
** @brief
** sets contrast
**
** @param[in]	vcompo		 	the video compositor handle
** @param[in]	contrast		contrast value
**
** @retval		0			VCOMPO_SetVideoContrast successfully.
** @retval		-1 		error occurred or VCOMPO_SetVideoContrast failed.
**
*/
int  VCOMPO_SetVideoContrast(VCOMPO_HANDLE vcompo, int contrast);

/**
** @brief
** gets contrast value
**
** @param[in]	vcompo		 	the video compositor handle
** @param[out]	contrast		contrast value
**
** @retval		0			VCOMPO_GetVideoContrast successfully.
** @retval		-1 		error occurred or VCOMPO_GetVideoContrast failed.
**
*/
int  VCOMPO_GetVideoContrast(VCOMPO_HANDLE vcompo, int *contrast);

/**
** @brief
** sets hue
**
** @param[in]	vcompo		 	the video compositor handle
** @param[in]	hue				hue value
**
** @retval		0			VCOMPO_SetVideoHue successfully.
** @retval		-1 		error occurred or VCOMPO_SetVideoHue failed.
**
*/
int  VCOMPO_SetVideoHue(VCOMPO_HANDLE vcompo, int hue);

/**
** @brief
** gets hue value
**
** @param[in]	vcompo		 	the video compositor handle
** @param[out]	hue				hue value
**
** @retval		0			VCOMPO_GetVideoHue successfully.
** @retval		-1 		error occurred or VCOMPO_GetVideoHue failed.
**
*/
int  VCOMPO_GetVideoHue(VCOMPO_HANDLE vcompo, int *hue);

/**
** @brief
** sets Saturation
**
** @param[in]	vcompo		 	the video compositor handle
** @param[in]	saturation		saturation value
**
** @retval		0			VCOMPO_SetVideoSaturation successfully.
** @retval		-1 		error occurred or VCOMPO_SetVideoSaturation failed.
**
*/
int  VCOMPO_SetVideoSaturation(VCOMPO_HANDLE vcompo, int saturation);

/**
** @brief
** gets Saturation value
**
** @param[in]	vcompo		 	the video compositor handle
** @param[out]	saturation		saturation value
**
** @retval		0			VCOMPO_GetVideoSaturation successfully.
** @retval		-1 		error occurred or VCOMPO_GetVideoSaturation failed.
**
*/
int  VCOMPO_GetVideoSaturation(VCOMPO_HANDLE vcompo, int *saturation);

/**
** @brief
** creates DISPLAY module and does initial works for this DISPLAY module
**
** @param[out]	display		 	 return the display module handle created by this function
**
** @retval		0			 DISPLAY_Create has successfully processed.
** @retval		-1 		 parameters error occurred or Create return failed.
**
*/
int  DISPLAY_Create(DISPLAY_HANDLE *display);

/**
** @brief
** destroys DISPLAY module
**
** @param[in]	display		 	 the display module handle
**
** @retval		0			 DISPLAY_Destroy has successfully processed
** @retval		-1 		 parameters error occurred or Destroy return failed
**
*/
int  DISPLAY_Destroy(DISPLAY_HANDLE display);

/**
** @brief
** gets DISPLAY_GetSupportedList
**
** @param[in]	display		 	the display module handle
** @param[out]	resolution_list	supported_resolution list value (value example => "1|3|5|9|10")
**
** @retval		0		DISPLAY_GetSupportedResolutionList successfully.
** @retval		-1 		error occurred or DISPLAY_GetSupportedResolutionList failed.
**
*/
int  DISPLAY_GetSupportedResolutionList(DISPLAY_HANDLE display, char* resolution_list);

/**
** @brief
** sets BTF_DISPLAY_Resolution
**
** @param[in]	display		 	the display module handle
** @param[in]	resolution		BTF_DISPLAY_Resolution value
**
** @retval		0			DISPLAY_SetResolution successfully.
** @retval		-1 		error occurred or DISPLAY_SetResolution failed.
**
*/
int  DISPLAY_SetResolution(DISPLAY_HANDLE display, BTF_DISPLAY_Resolution resolution);

/**
** @brief
** gets BTF_DISPLAY_Resolution value
**
** @param[in]	display		 	the display module handle
** @param[out]	resolution		BTF_DISPLAY_Resolution value
**
** @retval		0			DISPLAY_GetResolution successfully.
** @retval		-1 		error occurred or DISPLAY_GetResolution failed.
**
*/
int  DISPLAY_GetResolution(DISPLAY_HANDLE display, BTF_DISPLAY_Resolution *resolution);

/**
** @brief
** sets HDMI CEC on/off
**
** @param[in]	display		 	the display module handle
** @param[in]	enable 			defines set CEC Enable or not, AVP_FALSE 
**								means not , AVP_TRUE means set enable
**
** @retval		0			DISPLAY_CEC_SetEnable successfully.
** @retval		-1 		error occurred or DISPLAY_CEC_SetEnable failed.
**
*/
int	DISPLAY_CEC_SetEnable(DISPLAY_HANDLE display, bool enable);


/**
** @brief
** gets HDMI CEC status
**
** @param[in]	display		 	the display module handle
** @param[in]	state			defines CEC Enable state or not, AVP_FALSE 
**								means not , AVP_TRUE means enable value
**
** @retval		0			DISPLAY_CEC_GetState successfully.
** @retval		-1 		error occurred or DISPLAY_CEC_GetState failed.
**
*/
int	DISPLAY_CEC_GetState(DISPLAY_HANDLE display, bool *state);

/**
** @brief
** Command CEC Power On/Off

**
** @param[in]	display		 	the display module handle
** @param[in]	onoff 			defines set CEC Power On or not, AVP_FALSE 
**								means off , AVP_TRUE means set On
**
** @retval		0			DISPLAY_CEC_SetPowerOnOff successfully.
** @retval		-1 		error occurred or DISPLAY_CEC_SetPowerOnOff failed.
**
*/
int	DISPLAY_CEC_SetPowerOnOff(DISPLAY_HANDLE display, bool onoff);

/**
** @brief
** sets HDMI on/off
**
** @param[in]	display		 	the display module handle
** @param[in]	enable 			defines set HDMI Enable or not, AVP_FALSE 
**								means not , AVP_TRUE means set enable
**
** @retval		0			DISPLAY_HDMI_SetEnable successfully.
** @retval		-1 		    error occurred or DISPLAY_CEC_SetEnable failed.
**
*/
int	DISPLAY_HDMI_SetEnable(DISPLAY_HANDLE display, bool enable);

/**
** @brief
** sets HDMI activeSource on/off
**
** @param[in]	display		 	the display module handle
** @param[in]	activeSource 	ture : exucute activeSource, false : don't active source 
**
** @retval		0			DISPLAY_HDMI_CEC_ActiveSource successfully.
** @retval		-1 		    error occurred or DISPLAY_HDMI_CEC_ActiveSource failed.
**
*/
int	DISPLAY_HDMI_CEC_ActiveSource(DISPLAY_HANDLE display, bool activeSource);

/**
** @brief
** sets HDMI HDR on/off
**
** @param[in]	display		 	the display module handle
** @param[in]	enable 			defines set HDR Enable or not, AVP_FALSE 
**								means not , AVP_TRUE means set enable
**
** @retval		0			DISPLAY_HDR_SetEnable successfully.
** @retval		-1 		error occurred or DISPLAY_HDR_SetEnable failed.
**
*/
int	DISPLAY_HDR_SetEnable(DISPLAY_HANDLE display, bool enable);


/**
** @brief
** gets HDMI HDR status
**
** @param[in]	display		 	the display module handle
** @param[in]	state			defines HDR Enable state or not, AVP_FALSE 
**								means not , AVP_TRUE means enable value
**
** @retval		0			DISPLAY_HDR_GetState successfully.
** @retval		-1 		error occurred or DISPLAY_HDR_GetState failed.
**
*/
int	DISPLAY_HDR_GetState(DISPLAY_HANDLE display, bool *state);

/**
** @brief
** get HDMI HDR Supported

**
** @param[in]	display		 	the display module handle
** @param[in]	support 		defines HDR Supported or not, AVP_FALSE 
**								means not supported , AVP_TRUE means supported
**
** @retval		0			DISPLAY_HDR_GetSupported successfully.
** @retval		-1 		error occurred or DISPLAY_HDR_GetSupported failed.
**
*/
int	DISPLAY_HDR_GetSupported(DISPLAY_HANDLE display, bool *support);

/**
** @brief
** gets DISPLAY_GetHdmiEdidInfo
**
** @param[in]	display		 	the display module handle
** @param[out]	hdmi_edid_info	DISPLAY_GetHdmiEdidInfo value (value example => "EdidInfo")
**
** @retval		0		DISPLAY_GetHdmiEdidInfo successfully.
** @retval		-1 		error occurred or DISPLAY_GetHdmiEdidInfo failed.
**
*/
int  DISPLAY_GetHdmiEdidInfo(DISPLAY_HANDLE display, char* hdmi_edid_info);

/**
** @brief
** sets HdmiHotplugged Data Listener
**
** @param[in]	display		 	the display module handle
** @param[in]	callback		function pointer to HdmiHotplugged callback
**
** @retval		0			DISPLAY_SetHdmiHotpluggedListener has successfully processed
** @retval		-1 		Parameters error occurred or filter open failed.
**
*/
int  DISPLAY_SetHdmiHotpluggedListener(DISPLAY_HANDLE display, DISPLAY_HDMIHotPluggedCallback callback);

/**
** @brief
** sets DISPLAY_CECPowerState  Data Listener
**
** @param[in]	display		 	the display module handle
** @param[in]	callback		function pointer to CECPowerState callback
**
** @retval		0			DISPLAY_SetCECPowerStateListener has successfully processed
** @retval		-1 		Parameters error occurred or filter open failed.
**
*/
int  DISPLAY_SetCECPowerStateListener(DISPLAY_HANDLE display, DISPLAY_CECPowerStateCallback callback);

/**
** @brief
** sets DISPLAY_HDMI  event Listener
** add BTF_LEVEL_2
**
** @param[in]	display		 	the display module handle
** @param[in]	callback		function pointer to HDMI Event callback
**
** @retval		0			DISPLAY_HDMI_SetEventListener has successfully processed
** @retval		-1 		Parameters error occurred or filter open failed.
**
*/
int  DISPLAY_HDMI_SetEventListener(DISPLAY_HANDLE display, DISPLAY_HDMIEventCallback callback);

/**
** @brief
** sets DISPLAY_CEC  event Listener
** add BTF_LEVEL_2
**
** @param[in]	display		 	the display module handle
** @param[in]	callback		function pointer to CEC Event callback
**
** @retval		0			DISPLAY_CEC_SetEventListener has successfully processed
** @retval		-1 		Parameters error occurred or filter open failed.
**
*/
int  DISPLAY_CEC_SetEventListener(DISPLAY_HANDLE display, DISPLAY_CECEventCallback callback);

/**
** @brief
** sets DISPLAY_HDMI_CEC  message Listener
** add BTF_LEVEL_2
**
** @param[in]	display		 	the display module handle
** @param[in]	callback		function pointer to HDMI|CEC Message callback
**
** @retval		0			DISPLAY_HDMI_CEC_SetMessageListener has successfully processed
** @retval		-1 		Parameters error occurred or filter open failed.
**
*/
int  DISPLAY_HDMI_CEC_SetMessageListener(DISPLAY_HANDLE display, DISPLAY_HDMI_CECMessageCallback callback);

/**
** @brief
** gets DISPLAY_HDMI_CEC_SendMessage
** add BTF_LEVEL_2
**
** @param[in]	display		 	the display module handle
** @param[in]	type			1 : HDMI, 0 : CEC
** @param[in]	message			send message
**
** @retval		0		DISPLAY_HDMI_CEC_SendMessage successfully.
** @retval		-1 		error occurred or DISPLAY_HDMI_CEC_SendMessage failed.
**
*/
int  DISPLAY_HDMI_CEC_SendMessage(DISPLAY_HANDLE display, int type, void* message);

#ifdef __cplusplus
}
#endif

#endif //_BTV_HAL_________H
