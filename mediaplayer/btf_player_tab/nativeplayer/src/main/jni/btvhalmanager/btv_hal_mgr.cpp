// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

//#define LONG_NDEBUG 0
#define LOG_TAG "BOXKEY-BTV_HAL_MGR.001"

#include "btv_hal_mgr.h"

#include <memory>
#include <mutex>
#include <ALog.h>

#define AVP_DEVICE_MAIN    0
#define AVP_DEVICE_PIP     1

//namespace android
//{

class HalBtvMgr /*: public RefBase*/ {

	public:
		static HalBtvMgr* getInstance();

		AVP_HANDLE selectAVPPlayer(int deviceID);

		// Player 
		int  BtvAVP_Create(int device, AVP_CreateConfig *config);
		int  BtvAVP_Destroy(int device);
		int  BtvAVP_Start(int device, AVP_PlayerConfig *config);
		int  BtvAVP_Stop(int device);
		int  BtvAVP_Pause(int device);
		int  BtvAVP_Resume(int device);
		int  BtvAVP_InjectTS(int device, char *buffer, int size);
		int  BtvAVP_InjectCommand(int device, AVP_InjectCmdType command);
		int  BtvAVP_GetVideoPTS(int device, long long *pts);
		int  BtvAVP_FlushTS(int device);
		int  BtvAVP_SetPlaySpeed(int device, float speed);
		int  BtvAVP_SetVideoPlaneSize(int device, int x, int y, int width, int height);
		int  BtvAVP_SetSurface(int device, void *nativeWidow);
		int  BtvAVP_SetAVoffset(int device, AVP_AVoffset offset);
		int  BtvAVP_SetVideoMute(int device, bool mute);
		int  BtvAVP_SetAudioMute(int device, bool mute);
		int  BtvAVP_GetAudioMute(int device, bool *mute_status);
        int  BtvAVP_SetMediaAudioVolume(int device, int volume, float gain);
        int  BtvAVP_GetMediaAudioVolume(int device, int *volume);
		int  BtvAVP_SetAudioStream(int device, AVP_AudioConfig *audio);
		int  BtvAVP_SetSectionFilter(int device, AVP_SectionFilterConfig *config);
		int  BtvAVP_ClearSectionFilter(int device);
		int  BtvAVP_SetCommand(int device, int request, void* in_param, void* out_param);

		int  BtvAMIXER_SetMediaAudioVolume(int volume, float gain);
		int  BtvAMIXER_GetMediaAudioVolume(int *volume);
		int  BtvAMIXER_SetMasterAudioMute(bool mute);
		int  BtvAMIXER_SetMasterAudioVolume(int volume);
		int  BtvAMIXER_GetMasterAudioVolume(int *volume);
		int  BtvAMIXER_SetAudioOutputMode(AVP_AudioOutputMode mode);
		int  BtvAMIXER_GetAudioOutputMode(AVP_AudioOutputMode *mode);
		int  BtvAMIXER_SetSoundEffect(AVP_SoundEffect mode);
		int  BtvAMIXER_GetSoundEffect(AVP_SoundEffect *mode);
		int  BtvAMIXER_SetAudioSnoop(bool enable);

		int  BtvVCOMPO_SetZorder(VIDEO_Zorder_Plane zorder1, VIDEO_Zorder_Plane zorder2, 
							VIDEO_Zorder_Plane zorder3, VIDEO_Zorder_Plane zorder4);
		int  BtvVCOMPO_SetScaleRatioMode(AVP_AspectRatioMode mode);
		int  BtvVCOMPO_SetVideoBrightness(int brightness);
		int  BtvVCOMPO_GetVideoBrightness(int *brightness);
		int  BtvVCOMPO_SetVideoContrast(int contrast);
		int  BtvVCOMPO_GetVideoContrast(int *contrast);
		int  BtvVCOMPO_SetVideoHue(int hue);
		int  BtvVCOMPO_GetVideoHue(int *hue);
		int  BtvVCOMPO_SetVideoSaturation(int saturation);
		int  BtvVCOMPO_GetVideoSaturation(int *saturation);

		int  BtvDISPLAY_GetSupportedResolutionList(char* resolution_list);
		int  BtvDISPLAY_SetResolution(BTF_DISPLAY_Resolution resolution);
		int  BtvDISPLAY_GetResolution(BTF_DISPLAY_Resolution *resolution);
		int  BtvDISPLAY_CEC_SetEnable(bool enable);
		int  BtvDISPLAY_CEC_GetState(bool *state);
		int  BtvDISPLAY_CEC_SetPowerOnOff(bool onoff);
		int  BtvDISPLAY_HDR_SetEnable(bool enable);
		int  BtvDISPLAY_HDR_GetState(bool *state);
		int  BtvDISPLAY_HDR_GetSupported(bool *support);
		int  BtvDISPLAY_HDMI_SetEnable(bool enable);
		int  BtvDISPLAY_HDMI_ActiveSource(bool activeSource);
		int  BtvDISPLAY_GetHdmiEdidInfo(char* hdmi_edid_info);
		int  BtvDISPLAY_SetHdmiHotpluggedListener(DISPLAY_HDMIHotPluggedCallback callback);
		int  BtvDISPLAY_SetCECPowerStateListener(DISPLAY_CECPowerStateCallback callback);
		int  BtvDEVICE_SetLedState(int led_id, int state);
		int  BtvDEVICE_SetLedColor(int led_id, int color);
		int  BtvDEVICE_SetVolumeLedState(int led_id, int state, int volume);
		int  BtvDEVICE_SetBeamformingLedState(int led_id, int state, int degree);
		int  BtvDEVICE_SetMicOnOff(bool enable);
		int  BtvDEVICE_GetMicVolume(int *volume_level);
		int  BtvDEVICE_SetMicVolume(int volume_level);
		int  BtvDEVICE_GetMicEchoLevel(int *echo_level);
		int  BtvDEVICE_SetMicEchoLevel(int echo_level);
		int  BtvDEVICE_MicPause();
		int  BtvDEVICE_MicResume();

	private:
		HalBtvMgr();	
		~HalBtvMgr();
		
		static		HalBtvMgr *instance_;

		AVP_HANDLE			mAvpPlayerMain;
		AVP_HANDLE			mAvpPlayerPip;
		AMIXER_HANDLE		mAmixer;
		VCOMPO_HANDLE		mVideoCompositor;
		DISPLAY_HANDLE		mDisplay;
		DEVICE_HANDLE		mDevice;

		AVP_CreateConfig 	mMainCreateConfig;
		AVP_CreateConfig 	mPipCreateConfig;

    std::mutex               mAmixerLock;
    std::mutex               mVideoCompoLock;
    std::mutex               mDisplayLock;
    std::mutex               mDeviceLock;
};

HalBtvMgr* HalBtvMgr::instance_ = nullptr;

HalBtvMgr* HalBtvMgr::getInstance() {
    if (instance_ != NULL) {
        return instance_;
    }
	 ALOGI("[%s] called", __FUNCTION__);
     instance_ = new HalBtvMgr();
     return instance_;
}

HalBtvMgr::HalBtvMgr()
{
	ALOGI("[%s] called", __FUNCTION__);
	mAvpPlayerMain = NULL;
	mAvpPlayerPip = NULL;
	mAmixer = NULL;
	mVideoCompositor = NULL;
	mDisplay = NULL;
	mDevice = NULL;
	
}

HalBtvMgr::~HalBtvMgr()
{
	ALOGI("[%s] called", __FUNCTION__);
	if(mAmixer != NULL){
		AMIXER_Destroy(mAmixer);
	}

	if(mVideoCompositor != NULL){
		VCOMPO_Destroy(mVideoCompositor);
	}

	if(mDisplay != NULL){
		DISPLAY_Destroy(mDisplay);
	}

	if(mDevice != NULL){
		DEVICE_Destroy(mDevice);
	}

	if(mAvpPlayerMain != NULL){
		AVP_Destroy(mAvpPlayerMain);
	}

	if(mAvpPlayerPip != NULL){
		AVP_Destroy(mAvpPlayerPip);
	}
}

AVP_HANDLE HalBtvMgr::selectAVPPlayer(int deviceID)
{
	switch (deviceID)
	{
	case AVP_DEVICE_MAIN:
	{
		return mAvpPlayerMain;
	}	
	break;
	case AVP_DEVICE_PIP:
	{
		return mAvpPlayerPip;
	}	
	break;
	default:
	{
		return mAvpPlayerMain;
	}	
	break;
	}
}


int  HalBtvMgr::BtvAVP_Create(int device, AVP_CreateConfig *config)
{
	int ret = 0;
	if(device == AVP_DEVICE_MAIN){
		if(mAvpPlayerMain == NULL){
			mMainCreateConfig = *config;
			ret = AVP_Create(&mAvpPlayerMain, config);		
		}
	}else{
		if(mAvpPlayerPip == NULL){
			mPipCreateConfig = *config;
			ret = AVP_Create(&mAvpPlayerPip, config);		
		}
	}
	return ret;
}

int  HalBtvMgr::BtvAVP_Destroy(int device)
{
	int ret = 0;
	if(device == AVP_DEVICE_MAIN){
		if(mAvpPlayerMain){
			ret = AVP_Destroy(mAvpPlayerMain);
			mAvpPlayerMain = NULL;
		}
	}else{
		if(mAvpPlayerPip){
			ret = AVP_Destroy(mAvpPlayerPip);
			mAvpPlayerPip = NULL;
		}
	}

	return ret;
}

int  HalBtvMgr::BtvAVP_Start(int device, AVP_PlayerConfig *config)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_Start(player, config);
}

int  HalBtvMgr::BtvAVP_Stop(int device)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_Stop(player);
}

int  HalBtvMgr::BtvAVP_Pause(int device)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_Pause(player);
}

int  HalBtvMgr::BtvAVP_Resume(int device)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_Resume(player);
}

int  HalBtvMgr::BtvAVP_InjectTS(int device, char *buffer, int size)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_InjectTS(player, buffer, size);
}

int  HalBtvMgr::BtvAVP_InjectCommand(int device, AVP_InjectCmdType command)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_InjectCommand(player, command);
}

int  HalBtvMgr::BtvAVP_GetVideoPTS(int device, long long *pts)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_GetVideoPTS(player, pts);
}

int  HalBtvMgr::BtvAVP_FlushTS(int device)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_FlushTS(player);
}

int  HalBtvMgr::BtvAVP_SetPlaySpeed(int device, float speed)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_SetPlaySpeed(player, speed);
}

int  HalBtvMgr::BtvAVP_SetVideoPlaneSize(int device, int x, int y, int width, int height)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_SetVideoPlaneSize(player, x, y, width, height);
}

int  HalBtvMgr::BtvAVP_SetSurface(int device, void *nativeWidow)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_SetSurface(player, nativeWidow);
}

int  HalBtvMgr::BtvAVP_SetAVoffset(int device, AVP_AVoffset offset)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_SetAVoffset(player, offset);
}

int  HalBtvMgr::BtvAVP_SetVideoMute(int device, bool mute)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_SetVideoMute(player, mute);
}

int  HalBtvMgr::BtvAVP_SetAudioMute(int device, bool mute)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_SetAudioMute(player, mute);
}

int  HalBtvMgr::BtvAVP_GetAudioMute(int device, bool *mute_status)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_GetAudioMute(player, mute_status);
}

int  HalBtvMgr::BtvAVP_SetMediaAudioVolume(int device, int volume, float gain)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_SetMediaAudioVolume(player, volume, gain);
}

int  HalBtvMgr::BtvAVP_GetMediaAudioVolume(int device, int *volume)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_GetMediaAudioVolume(player, volume);
}

int  HalBtvMgr::BtvAVP_SetAudioStream(int device, AVP_AudioConfig *audio)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_SetAudioStream(player, audio);
}

int  HalBtvMgr::BtvAVP_SetSectionFilter(int device, AVP_SectionFilterConfig *config)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_SetSectionFilter(player, config);
}

int  HalBtvMgr::BtvAVP_ClearSectionFilter(int device)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_ClearSectionFilter(player);
}

int  HalBtvMgr::BtvAVP_SetCommand(int device, int request, void* in_param, void* out_param)
{
	AVP_HANDLE player = selectAVPPlayer(device);
	return AVP_SetCommand(player, request, in_param, out_param);
}

int  HalBtvMgr::BtvAMIXER_SetMediaAudioVolume(int volume, float gain)
{
	std::lock_guard<std::mutex> lock(mAmixerLock);

	if(mAmixer == NULL){
		AMIXER_Create(&mAmixer);
	}

	return AMIXER_SetAudioVolume(mAmixer, volume, gain);
}

int  HalBtvMgr::BtvAMIXER_GetMediaAudioVolume(int *volume)
{
	std::lock_guard<std::mutex> lock(mAmixerLock);

	if(mAmixer == NULL){
		AMIXER_Create(&mAmixer);
	}

	return AMIXER_GetAudioVolume(mAmixer, volume);
}

int  HalBtvMgr::BtvAMIXER_SetMasterAudioMute(bool mute)
{
	std::lock_guard<std::mutex> lock(mAmixerLock);

	if(mAmixer == NULL){
		AMIXER_Create(&mAmixer);
	}

	return AMIXER_SetMasterAudioMute(mAmixer, mute);
}

int  HalBtvMgr::BtvAMIXER_SetMasterAudioVolume(int volume)
{
	std::lock_guard<std::mutex> lock(mAmixerLock);

	if(mAmixer == NULL){
		AMIXER_Create(&mAmixer);
	}

	return AMIXER_SetMasterAudioVolume(mAmixer, volume);
}

int  HalBtvMgr::BtvAMIXER_GetMasterAudioVolume(int *volume)
{
	std::lock_guard<std::mutex> lock(mAmixerLock);

	if(mAmixer == NULL){
		AMIXER_Create(&mAmixer);
	}

	return AMIXER_GetMasterAudioVolume(mAmixer, volume);
}

int  HalBtvMgr::BtvAMIXER_SetAudioOutputMode(AVP_AudioOutputMode mode)
{
	std::lock_guard<std::mutex> lock(mAmixerLock);

	if(mAmixer == NULL){
		AMIXER_Create(&mAmixer);
	}

	return AMIXER_SetAudioOutputMode(mAmixer, mode);
}

int  HalBtvMgr::BtvAMIXER_GetAudioOutputMode(AVP_AudioOutputMode *mode)
{
	std::lock_guard<std::mutex> lock(mAmixerLock);

	if(mAmixer == NULL){
		AMIXER_Create(&mAmixer);
	}

	return AMIXER_GetAudioOutputMode(mAmixer, mode);
}

int  HalBtvMgr::BtvAMIXER_SetSoundEffect(AVP_SoundEffect mode)
{
	std::lock_guard<std::mutex> lock(mAmixerLock);

	if(mAmixer == NULL){
		AMIXER_Create(&mAmixer);
	}

	return AMIXER_SetSoundEffect(mAmixer, mode);
}

int  HalBtvMgr::BtvAMIXER_GetSoundEffect(AVP_SoundEffect *mode)
{
	std::lock_guard<std::mutex> lock(mAmixerLock);

	if(mAmixer == NULL){
		AMIXER_Create(&mAmixer);
	}

	return AMIXER_GetSoundEffect(mAmixer, mode);
}

int  HalBtvMgr::BtvAMIXER_SetAudioSnoop(bool enable)
{
	std::lock_guard<std::mutex> lock(mAmixerLock);

	if(mAmixer == NULL){
		AMIXER_Create(&mAmixer);
	}

	return AMIXER_SetAudioSnoop(enable);
}

int  HalBtvMgr::BtvVCOMPO_SetZorder(VIDEO_Zorder_Plane zorder1, VIDEO_Zorder_Plane zorder2, 
					VIDEO_Zorder_Plane zorder3, VIDEO_Zorder_Plane zorder4)
{
	std::lock_guard<std::mutex> lock(mVideoCompoLock);

	if(mVideoCompositor == NULL){
		VCOMPO_Create(&mVideoCompositor);
	}

	return VCOMPO_SetZorder(mVideoCompositor, zorder1, zorder2, zorder3, zorder4);
}
					
int  HalBtvMgr::BtvVCOMPO_SetScaleRatioMode(AVP_AspectRatioMode mode)
{
	std::lock_guard<std::mutex> lock(mVideoCompoLock);

	if(mVideoCompositor == NULL){
		VCOMPO_Create(&mVideoCompositor);
	}

	return VCOMPO_SetScaleRatioMode(mVideoCompositor, mode);
}

int  HalBtvMgr::BtvVCOMPO_SetVideoBrightness(int brightness)
{
	std::lock_guard<std::mutex> lock(mVideoCompoLock);

	if(mVideoCompositor == NULL){
		VCOMPO_Create(&mVideoCompositor);
	}

	return VCOMPO_SetVideoBrightness(mVideoCompositor, brightness);
}

int  HalBtvMgr::BtvVCOMPO_GetVideoBrightness(int *brightness)
{
	std::lock_guard<std::mutex> lock(mVideoCompoLock);

	if(mVideoCompositor == NULL){
		VCOMPO_Create(&mVideoCompositor);
	}

	return VCOMPO_GetVideoBrightness(mVideoCompositor, brightness);
}

int  HalBtvMgr::BtvVCOMPO_SetVideoContrast(int contrast)
{
	std::lock_guard<std::mutex> lock(mVideoCompoLock);

	if(mVideoCompositor == NULL){
		VCOMPO_Create(&mVideoCompositor);
	}

	return VCOMPO_SetVideoContrast(mVideoCompositor, contrast);
}

int  HalBtvMgr::BtvVCOMPO_GetVideoContrast(int *contrast)
{
	std::lock_guard<std::mutex> lock(mVideoCompoLock);

	if(mVideoCompositor == NULL){
		VCOMPO_Create(&mVideoCompositor);
	}

	return VCOMPO_GetVideoContrast(mVideoCompositor, contrast);
}

int  HalBtvMgr::BtvVCOMPO_SetVideoHue(int hue)
{
	std::lock_guard<std::mutex> lock(mVideoCompoLock);

	if(mVideoCompositor == NULL){
		VCOMPO_Create(&mVideoCompositor);
	}

	return VCOMPO_SetVideoHue(mVideoCompositor, hue);
}

int  HalBtvMgr::BtvVCOMPO_GetVideoHue(int *hue)
{
	if(mVideoCompositor == NULL){
		VCOMPO_Create(&mVideoCompositor);
	}

	return VCOMPO_GetVideoHue(mVideoCompositor, hue);
}

int  HalBtvMgr::BtvVCOMPO_SetVideoSaturation(int saturation)
{
	std::lock_guard<std::mutex> lock(mVideoCompoLock);

	if(mVideoCompositor == NULL){
		VCOMPO_Create(&mVideoCompositor);
	}

	return VCOMPO_SetVideoSaturation(mVideoCompositor, saturation);
}

int  HalBtvMgr::BtvVCOMPO_GetVideoSaturation(int *saturation)
{
	std::lock_guard<std::mutex> lock(mVideoCompoLock);

	if(mVideoCompositor == NULL){
		VCOMPO_Create(&mVideoCompositor);
	}

	return VCOMPO_GetVideoSaturation(mVideoCompositor, saturation);
}

int  HalBtvMgr::BtvDISPLAY_GetSupportedResolutionList(char* resolution_list)
{
	std::lock_guard<std::mutex> lock(mVideoCompoLock);

	if(mDisplay == NULL){
		DISPLAY_Create(&mDisplay);
	}

	return DISPLAY_GetSupportedResolutionList(mDisplay, resolution_list);
}

int  HalBtvMgr::BtvDISPLAY_SetResolution(BTF_DISPLAY_Resolution resolution)
{
	std::lock_guard<std::mutex> lock(mDisplayLock);

	if(mDisplay == NULL){
		DISPLAY_Create(&mDisplay);
	}

	return DISPLAY_SetResolution(mDisplay, resolution);
}

int  HalBtvMgr::BtvDISPLAY_GetResolution(BTF_DISPLAY_Resolution *resolution)
{
	std::lock_guard<std::mutex> lock(mDisplayLock);

	if(mDisplay == NULL){
		DISPLAY_Create(&mDisplay);
	}

	return DISPLAY_GetResolution(mDisplay, resolution);
}

int  HalBtvMgr::BtvDISPLAY_CEC_SetEnable(bool enable)
{
	std::lock_guard<std::mutex> lock(mDisplayLock);

	if(mDisplay == NULL){
		DISPLAY_Create(&mDisplay);
	}

	return DISPLAY_CEC_SetEnable(mDisplay, enable);
}

int  HalBtvMgr::BtvDISPLAY_CEC_GetState(bool *state)
{
	std::lock_guard<std::mutex> lock(mDisplayLock);

	if(mDisplay == NULL){
		DISPLAY_Create(&mDisplay);
	}

	return DISPLAY_CEC_GetState(mDisplay, state);
}

int  HalBtvMgr::BtvDISPLAY_CEC_SetPowerOnOff(bool onoff)
{
	std::lock_guard<std::mutex> lock(mDisplayLock);

	if(mDisplay == NULL){
		DISPLAY_Create(&mDisplay);
	}

	return DISPLAY_CEC_SetPowerOnOff(mDisplay, onoff);
}

int  HalBtvMgr::BtvDISPLAY_HDR_SetEnable(bool enable)
{
	std::lock_guard<std::mutex> lock(mDisplayLock);

	if(mDisplay == NULL){
		DISPLAY_Create(&mDisplay);
	}

	return DISPLAY_HDR_SetEnable(mDisplay, enable);
}

int  HalBtvMgr::BtvDISPLAY_HDR_GetState(bool *state)
{
	std::lock_guard<std::mutex> lock(mDisplayLock);

	if(mDisplay == NULL){
		DISPLAY_Create(&mDisplay);
	}

	return DISPLAY_HDR_GetState(mDisplay, state);
}

int  HalBtvMgr::BtvDISPLAY_HDR_GetSupported(bool *support)
{
	std::lock_guard<std::mutex> lock(mDisplayLock);

	if(mDisplay == NULL){
		DISPLAY_Create(&mDisplay);
	}

	return DISPLAY_HDR_GetSupported(mDisplay, support);
}

int  HalBtvMgr::BtvDISPLAY_HDMI_SetEnable(bool enable)
{
	std::lock_guard<std::mutex> lock(mDisplayLock);

	if(mDisplay == NULL){
		DISPLAY_Create(&mDisplay);
	}

	return DISPLAY_HDMI_SetEnable(mDisplay, enable);
}

int  HalBtvMgr::BtvDISPLAY_HDMI_ActiveSource(bool activeSource)
{
	std::lock_guard<std::mutex> lock(mDisplayLock);

	if(mDisplay == NULL){
		DISPLAY_Create(&mDisplay);
	}

	return DISPLAY_HDMI_CEC_ActiveSource(mDisplay, activeSource);
}


int  HalBtvMgr::BtvDISPLAY_GetHdmiEdidInfo(char* hdmi_edid_info)
{
	std::lock_guard<std::mutex> lock(mDisplayLock);

	if(mDisplay == NULL){
		DISPLAY_Create(&mDisplay);
	}

	return DISPLAY_GetHdmiEdidInfo(mDisplay, hdmi_edid_info);
}

int  HalBtvMgr::BtvDISPLAY_SetHdmiHotpluggedListener(DISPLAY_HDMIHotPluggedCallback callback)
{
	std::lock_guard<std::mutex> lock(mDisplayLock);

	if(mDisplay == NULL){
		DISPLAY_Create(&mDisplay);
	}

	return DISPLAY_SetHdmiHotpluggedListener(mDisplay, callback);
}

int  HalBtvMgr::BtvDISPLAY_SetCECPowerStateListener(DISPLAY_CECPowerStateCallback callback)
{
	std::lock_guard<std::mutex> lock(mDisplayLock);

	if(mDisplay == NULL){
		DISPLAY_Create(&mDisplay);
	}

	return DISPLAY_SetCECPowerStateListener(mDisplay, callback);
}

int  HalBtvMgr::BtvDEVICE_SetLedState(int led_id, int state)
{
	std::lock_guard<std::mutex> lock(mDeviceLock);
	
	if(mDevice == NULL){
		DEVICE_Create(&mDevice);
	}
	
	return DEVICE_SetLedState(mDevice, led_id, state);
}
	
int  HalBtvMgr::BtvDEVICE_SetLedColor(int led_id, int color)
{
	std::lock_guard<std::mutex> lock(mDeviceLock);
	
	if(mDevice == NULL){
		DEVICE_Create(&mDevice);
	}
	
	return DEVICE_SetLedColor(mDevice, led_id, color);
}

int  HalBtvMgr::BtvDEVICE_SetVolumeLedState(int led_id, int state, int volume)
{
	std::lock_guard<std::mutex> lock(mDeviceLock);
	
	if(mDevice == NULL){
		DEVICE_Create(&mDevice);
	}
	
	return DEVICE_SetVolumeLedState(mDevice, led_id, state, volume);
}

int  HalBtvMgr::BtvDEVICE_SetBeamformingLedState(int led_id, int state, int degree)
{
	std::lock_guard<std::mutex> lock(mDeviceLock);
	
	if(mDevice == NULL){
		DEVICE_Create(&mDevice);
	}
	
	return DEVICE_SetBeamformingLedState(mDevice, led_id, state, degree);
}

int  HalBtvMgr::BtvDEVICE_SetMicOnOff(bool enable)
{
	std::lock_guard<std::mutex> lock(mDeviceLock);
	
	if(mDevice == NULL){
		DEVICE_Create(&mDevice);
	}
	
	return DEVICE_SetMicOnOff(mDevice, enable);
}

int  HalBtvMgr::BtvDEVICE_GetMicVolume(int *volume_level)
{
	std::lock_guard<std::mutex> lock(mDeviceLock);
	
	if(mDevice == NULL){
		DEVICE_Create(&mDevice);
	}
	
	return DEVICE_GetMicVolume(mDevice, volume_level);
}

int  HalBtvMgr::BtvDEVICE_SetMicVolume(int volume_level)
{
	std::lock_guard<std::mutex> lock(mDeviceLock);
	
	if(mDevice == NULL){
		DEVICE_Create(&mDevice);
	}
	
	return DEVICE_SetMicVolume(mDevice, volume_level);
}

int  HalBtvMgr::BtvDEVICE_GetMicEchoLevel(int *echo_level)
{
	std::lock_guard<std::mutex> lock(mDeviceLock);
	
	if(mDevice == NULL){
		DEVICE_Create(&mDevice);
	}
	
	return DEVICE_GetMicEchoLevel(mDevice, echo_level);
}

int  HalBtvMgr::BtvDEVICE_SetMicEchoLevel(int echo_level)
{
	std::lock_guard<std::mutex> lock(mDeviceLock);
	
	if(mDevice == NULL){
		DEVICE_Create(&mDevice);
	}
	
	return DEVICE_SetMicEchoLevel(mDevice, echo_level);
}

int  HalBtvMgr::BtvDEVICE_MicPause()
{
	std::lock_guard<std::mutex> lock(mDeviceLock);
	
	if(mDevice == NULL){
		DEVICE_Create(&mDevice);
	}
	
	return DEVICE_MicPause(mDevice);
}

int  HalBtvMgr::BtvDEVICE_MicResume()
{
	std::lock_guard<std::mutex> lock(mDeviceLock);
	
	if(mDevice == NULL){
		DEVICE_Create(&mDevice);
	}
	
	return DEVICE_MicResume(mDevice);
}

// ---------------------------------------------------------------------------------------------------

//};	// namespace android

extern "C" {

int  HalBtv_Create(int device, AVP_CreateConfig *config)
{
	return HalBtvMgr::getInstance()->BtvAVP_Create(device, config);
}

int  HalBtv_Destroy(int device)
{
	return HalBtvMgr::getInstance()->BtvAVP_Destroy(device);
}

int  HalBtv_Start(int device, AVP_PlayerConfig *config)
{
    ALOGI("BTV PLAYER Version [%s]",BTV_PLAYER_VERSION);
	return HalBtvMgr::getInstance()->BtvAVP_Start(device, config);
}

int  HalBtv_Stop(int device)
{
	return HalBtvMgr::getInstance()->BtvAVP_Stop(device);
}

int  HalBtv_Pause(int device)
{
	return HalBtvMgr::getInstance()->BtvAVP_Pause(device);
}

int  HalBtv_Resume(int device)
{
	return HalBtvMgr::getInstance()->BtvAVP_Resume(device);
}

int  HalBtv_InjectTS(int device, char *buffer, int size)
{
	return HalBtvMgr::getInstance()->BtvAVP_InjectTS(device, buffer, size);
}

int  HalBtv_InjectCommand(int device, AVP_InjectCmdType command)
{
	return HalBtvMgr::getInstance()->BtvAVP_InjectCommand(device, command);
}

int  HalBtv_GetVideoPTS(int device, long long *pts)
{
	return HalBtvMgr::getInstance()->BtvAVP_GetVideoPTS(device, pts);
}

int  HalBtv_FlushTS(int device)
{
	return HalBtvMgr::getInstance()->BtvAVP_FlushTS(device);
}

int  HalBtv_SetPlaySpeed(int device, float speed)
{
	return HalBtvMgr::getInstance()->BtvAVP_SetPlaySpeed(device, speed);
}

int  HalBtv_SetVideoPlaneSize(int device, int x, int y, int width, int height)
{
	return HalBtvMgr::getInstance()->BtvAVP_SetVideoPlaneSize(device, x, y, width, height);
}

int  HalBtv_SetSurface(int device, void *nativeWidow)
{
	return HalBtvMgr::getInstance()->BtvAVP_SetSurface(device, nativeWidow);
}

int  HalBtv_SetAVoffset(int device, AVP_AVoffset offset)
{
	return HalBtvMgr::getInstance()->BtvAVP_SetAVoffset(device, offset);
}

int  HalBtv_SetVideoMute(int device, bool mute)
{
	return HalBtvMgr::getInstance()->BtvAVP_SetVideoMute(device, mute);
}

int  HalBtv_SetAudioMute(int device, bool mute)
{
	return HalBtvMgr::getInstance()->BtvAVP_SetAudioMute(device, mute);
}

int  HalBtv_GetAudioMute(int device, bool *mute_status)
{
	return HalBtvMgr::getInstance()->BtvAVP_GetAudioMute(device, mute_status);
}

int  HalBtv_SetAudioStream(int device, AVP_AudioConfig *audio)
{
	return HalBtvMgr::getInstance()->BtvAVP_SetAudioStream(device, audio);
}

int  HalBtv_SetSectionFilter(int device, AVP_SectionFilterConfig *config)
{
	return HalBtvMgr::getInstance()->BtvAVP_SetSectionFilter(device, config);
}

int  HalBtv_ClearSectionFilter(int device)
{
	return HalBtvMgr::getInstance()->BtvAVP_ClearSectionFilter(device);
}

int  HalBtv_SetCommand(int device, int request, void* in_param, void* out_param)
{
	return HalBtvMgr::getInstance()->BtvAVP_SetCommand(device, request, in_param, out_param);
}

int  HalBtv_SetMediaAudioVolume(int Volume, float gain) {
#ifdef USE_AVP_FOR_VOLUME_PATH
	HalBtvMgr::getInstance()->BtvAVP_SetMediaAudioVolume(0, Volume, gain);
	HalBtvMgr::getInstance()->BtvAVP_SetMediaAudioVolume(1, Volume, gain);
	return 0;
#else
	return HalBtvMgr::getInstance()->BtvAMIXER_SetMediaAudioVolume(Volume, gain);
#endif
}

int  HalBtv_GetMediaAudioVolume(int *Volume) {
#ifdef ENABLE_AVP_MEDIA_VOLUME
	return HalBtvMgr::getInstance()->BtvAVP_GetMediaAudioVolume(0, Volume);
#else
	return HalBtvMgr::getInstance()->BtvAMIXER_GetMediaAudioVolume(Volume);
#endif
}

int  HalBtv_SetMasterAudioMute(bool mute) {
	return HalBtvMgr::getInstance()->BtvAMIXER_SetMasterAudioMute(mute);	
}

int  HalBtv_SetMasterAudioVolume(int Volume) {
	return HalBtvMgr::getInstance()->BtvAMIXER_SetMasterAudioVolume(Volume);	
}

int  HalBtv_GetMasterAudioVolume(int *Volume) {
	return HalBtvMgr::getInstance()->BtvAMIXER_GetMasterAudioVolume(Volume);	
}

int  HalBtv_SetAudioOutputMode(AVP_AudioOutputMode mode) {
	return HalBtvMgr::getInstance()->BtvAMIXER_SetAudioOutputMode(mode);	
}

int  HalBtv_GetAudioOutputMode(AVP_AudioOutputMode *mode) {
	return HalBtvMgr::getInstance()->BtvAMIXER_GetAudioOutputMode(mode);	
}

int  HalBtv_SetSoundEffect(AVP_SoundEffect mode) {
	return HalBtvMgr::getInstance()->BtvAMIXER_SetSoundEffect(mode);	
}

int  HalBtv_GetSoundEffect(AVP_SoundEffect *mode) {
	return HalBtvMgr::getInstance()->BtvAMIXER_GetSoundEffect(mode);	
}

int  HalBtv_SetAudioSnoop(bool enable) {
	return HalBtvMgr::getInstance()->BtvAMIXER_SetAudioSnoop(enable);
}

int  HalBtv_SetZorder(VIDEO_Zorder_Plane zorder1, VIDEO_Zorder_Plane zorder2, 
					VIDEO_Zorder_Plane zorder3, VIDEO_Zorder_Plane zorder4)
{
	return HalBtvMgr::getInstance()->BtvVCOMPO_SetZorder(zorder1, zorder2, zorder3, zorder4);
}
					
int  HalBtv_SetScaleRatioMode(AVP_AspectRatioMode mode)
{
	return HalBtvMgr::getInstance()->BtvVCOMPO_SetScaleRatioMode(mode);
}

int  HalBtv_SetVideoBrightness(int brightness)
{
	return HalBtvMgr::getInstance()->BtvVCOMPO_SetVideoBrightness(brightness);
}

int  HalBtv_GetVideoBrightness(int *brightness)
{
	return HalBtvMgr::getInstance()->BtvVCOMPO_GetVideoBrightness(brightness);
}

int  HalBtv_SetVideoContrast(int contrast)
{
	return HalBtvMgr::getInstance()->BtvVCOMPO_SetVideoContrast(contrast);
}

int  HalBtv_GetVideoContrast(int *contrast)
{
	return HalBtvMgr::getInstance()->BtvVCOMPO_GetVideoContrast(contrast);
}

int  HalBtv_SetVideoHue(int hue)
{
	return HalBtvMgr::getInstance()->BtvVCOMPO_SetVideoHue(hue);
}

int  HalBtv_GetVideoHue(int *hue)
{
	return HalBtvMgr::getInstance()->BtvVCOMPO_GetVideoHue(hue);
}

int  HalBtv_SetVideoSaturation(int saturation)
{
	return HalBtvMgr::getInstance()->BtvVCOMPO_SetVideoSaturation(saturation);
}

int  HalBtv_GetVideoSaturation(int *saturation)
{
	return HalBtvMgr::getInstance()->BtvVCOMPO_GetVideoSaturation(saturation);
}

int  HalBtv_GetSupportedResolutionList(char* resolution_list)
{
	return HalBtvMgr::getInstance()->BtvDISPLAY_GetSupportedResolutionList(resolution_list);
}

int  HalBtv_SetResolution(BTF_DISPLAY_Resolution resolution)
{
	return HalBtvMgr::getInstance()->BtvDISPLAY_SetResolution(resolution);
}

int  HalBtv_GetResolution(BTF_DISPLAY_Resolution *resolution)
{
	return HalBtvMgr::getInstance()->BtvDISPLAY_GetResolution(resolution);
}

int  HalBtv_CEC_SetEnable(bool enable)
{
	return HalBtvMgr::getInstance()->BtvDISPLAY_CEC_SetEnable(enable);
}

int  HalBtv_CEC_GetState(bool *state)
{
	return HalBtvMgr::getInstance()->BtvDISPLAY_CEC_GetState(state);
}

int  HalBtv_CEC_SetPowerOnOff(bool onoff)
{
	return HalBtvMgr::getInstance()->BtvDISPLAY_CEC_SetPowerOnOff(onoff);
}

int  HalBtv_HDR_SetEnable(bool enable)
{
	return HalBtvMgr::getInstance()->BtvDISPLAY_HDR_SetEnable(enable);
}

int  HalBtv_HDR_GetState(bool *state)
{
	return HalBtvMgr::getInstance()->BtvDISPLAY_HDR_GetState(state);
}

int  HalBtv_HDR_GetSupported(bool *support)
{
	return HalBtvMgr::getInstance()->BtvDISPLAY_HDR_GetSupported(support);
}

int  HalBtv_HDMI_SetEnable(bool enable)
{
	return HalBtvMgr::getInstance()->BtvDISPLAY_HDMI_SetEnable(enable);
}

int  HalBtv_HDMI_ActiveSource(bool activeSource)
{
	return HalBtvMgr::getInstance()->BtvDISPLAY_HDMI_ActiveSource(activeSource);
}


int  HalBtv_GetHdmiEdidInfo(char* hdmi_edid_info)
{
	return HalBtvMgr::getInstance()->BtvDISPLAY_GetHdmiEdidInfo(hdmi_edid_info);
}

int  HalBtv_SetHdmiHotpluggedListener(DISPLAY_HDMIHotPluggedCallback callback)
{
	return HalBtvMgr::getInstance()->BtvDISPLAY_SetHdmiHotpluggedListener(callback);
}

int  HalBtv_SetCECPowerStateListener(DISPLAY_CECPowerStateCallback callback)
{
	return HalBtvMgr::getInstance()->BtvDISPLAY_SetCECPowerStateListener(callback);
}


int  HalBtv_SetLedState(int led_id, int state)
{
	return HalBtvMgr::getInstance()->BtvDEVICE_SetLedState(led_id, state);
}
	
int  HalBtv_SetLedColor(int led_id, int color)
{
	return HalBtvMgr::getInstance()->BtvDEVICE_SetLedColor(led_id, color);
}

int  HalBtv_SetVolumeLedState(int led_id, int state, int volume)
{
	return HalBtvMgr::getInstance()->BtvDEVICE_SetVolumeLedState(led_id, state, volume);
}

int  HalBtv_SetBeamformingLedState(int led_id, int state, int degree)
{
	return HalBtvMgr::getInstance()->BtvDEVICE_SetBeamformingLedState(led_id, state, degree);
}

int HalBtv_SetMicOnOff(bool enable)
{
	return HalBtvMgr::getInstance()->BtvDEVICE_SetMicOnOff(enable);
}

int  HalBtv_GetMicVolume(int *volume_level)
{
	return HalBtvMgr::getInstance()->BtvDEVICE_GetMicVolume(volume_level);
}

int  HalBtv_SetMicVolume(int volume_level)
{
	return HalBtvMgr::getInstance()->BtvDEVICE_SetMicVolume(volume_level);
}

int  HalBtv_GetMicEchoLevel(int *echo_level)
{
	return HalBtvMgr::getInstance()->BtvDEVICE_GetMicEchoLevel(echo_level);
}

int  HalBtv_SetMicEchoLevel(int echo_level)
{
	return HalBtvMgr::getInstance()->BtvDEVICE_SetMicEchoLevel(echo_level);
}

int  HalBtv_MicPause()
{
	return HalBtvMgr::getInstance()->BtvDEVICE_MicPause();
}

int  HalBtv_MicResume()
{
	return HalBtvMgr::getInstance()->BtvDEVICE_MicResume();
}

}

