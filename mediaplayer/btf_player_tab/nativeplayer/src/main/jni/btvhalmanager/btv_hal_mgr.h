// Copyright 2019 SK Broadband Co., LTD.
// Author: Kwon Soon Chan (sysc0507@sk.com)

#ifndef SOC_MANAGER_H
#define SOC_MANAGER_H

#include <btv_hal.h>
#include <btv_oem_hal.h>

#ifdef __cplusplus
extern "C" {
#endif
/**
 *@brief display resolution.
 * 
 */
//#define BTV_PLAYER_VERSION "0.9.1" //20210416
//#define BTV_PLAYER_VERSION "0.9.2" //20210430
//#define BTV_PLAYER_VERSION "0.9.3" //20210514
//#define BTV_PLAYER_VERSION "0.9.4" //20210517
//#define BTV_PLAYER_VERSION "0.9.5" //20210531
//#define BTV_PLAYER_VERSION "0.9.6" //20210601 SetAVOffset
//#define BTV_PLAYER_VERSION "0.9.7" //20210602 "vendor.sptek.pkt.log
//#define BTV_PLAYER_VERSION "0.9.8" //20210611 MLR5.27.1, save videoDelayTimeMs
//#define BTV_PLAYER_VERSION "0.9.9" //20210624 MLR FD close
#define BTV_PLAYER_VERSION "0.9.10" //20210625 MLR path

typedef enum
{
    DISPLAY_MODE_720P50HZ = 0,
    DISPLAY_MODE_720P = 1,
    DISPLAY_MODE_1080I50HZ = 2,
    DISPLAY_MODE_1080I = 3,
    DISPLAY_MODE_1080P50HZ = 4,
    DISPLAY_MODE_1080P = 5,
    DISPLAY_MODE_720P_3DOU_AS = 6,
    DISPLAY_MODE_1080P24HZ = 7,
    DISPLAY_MODE_1080P24HZ_3DOU_AS = 8,
    DISPLAY_MODE_3840x2160P30HZ = 9,
    DISPLAY_MODE_3840x2160P60HZ = 10,
    DISPLAY_MODE_AUTO = 11,
}
DISPLAY_Resolution;

int  HalBtv_Create(int device, AVP_CreateConfig *config);
int  HalBtv_Destroy(int device);
int  HalBtv_Start(int device, AVP_PlayerConfig *config);
int  HalBtv_Stop(int device);
int  HalBtv_Pause(int device);
int  HalBtv_Resume(int device);
int  HalBtv_InjectTS(int device, char *buffer, int size);
int  HalBtv_InjectCommand(int device, AVP_InjectCmdType command);
int  HalBtv_GetVideoPTS(int device, long long *pts);
int  HalBtv_FlushTS(int device);
int  HalBtv_SetPlaySpeed(int device, float speed);
int  HalBtv_SetVideoPlaneSize(int device, int x, int y, int width, int height);
int  HalBtv_SetSurface(int device, void *nativeWidow);
int  HalBtv_SetAVoffset(int device, AVP_AVoffset offset);
int  HalBtv_SetVideoMute(int device, bool mute);
int  HalBtv_SetAudioMute(int device, bool mute);
int  HalBtv_GetAudioMute(int device, bool *mute_status);
int  HalBtv_SetAudioStream(int device, AVP_AudioConfig *audio);
int  HalBtv_SetSectionFilter(int device, AVP_SectionFilterConfig *config);
int  HalBtv_ClearSectionFilter(int device);
int  HalBtv_SetCommand(int device, int request, void* in_param, void* out_param);
int  HalBtv_SetMediaAudioVolume(int Volume, float gain);
int  HalBtv_GetMediaAudioVolume(int *Volume);
int  HalBtv_SetMasterAudioMute(bool mute);
int  HalBtv_SetMasterAudioVolume(int volume);
int  HalBtv_GetMasterAudioVolume(int *volume);
int  HalBtv_SetAudioOutputMode(AVP_AudioOutputMode mode);
int  HalBtv_GetAudioOutputMode(AVP_AudioOutputMode *mode);
int  HalBtv_SetSoundEffect(AVP_SoundEffect mode);
int  HalBtv_GetSoundEffect(AVP_SoundEffect *mode);
int  HalBtv_SetAudioSnoop(bool enable);
int  HalBtv_SetZorder(VIDEO_Zorder_Plane zorder1, VIDEO_Zorder_Plane zorder2, 
					VIDEO_Zorder_Plane zorder3, VIDEO_Zorder_Plane zorder4);
int  HalBtv_SetScaleRatioMode(AVP_AspectRatioMode mode);
int  HalBtv_SetVideoBrightness(int brightness);
int  HalBtv_GetVideoBrightness(int *brightness);
int  HalBtv_SetVideoContrast(int contrast);
int  HalBtv_GetVideoContrast(int *contrast);
int  HalBtv_SetVideoHue(int hue);
int  HalBtv_GetVideoHue(int *hue);
int  HalBtv_SetVideoSaturation(int saturation);
int  HalBtv_GetVideoSaturation(int *saturation);

int  HalBtv_GetSupportedResolutionList(char* resolution_list);
int  HalBtv_SetResolution(BTF_DISPLAY_Resolution resolution);
int  HalBtv_GetResolution(BTF_DISPLAY_Resolution *resolution);
int  HalBtv_CEC_SetEnable(bool enable);
int  HalBtv_CEC_GetState(bool *state);
int  HalBtv_CEC_SetPowerOnOff(bool onoff);
int  HalBtv_HDR_SetEnable(bool enable);
int  HalBtv_HDR_GetState(bool *state);
int  HalBtv_HDR_GetSupported(bool *support);
int  HalBtv_HDMI_SetEnable(bool enable);
int  HalBtv_HDMI_ActiveSource(bool activeSource);
int  HalBtv_GetHdmiEdidInfo(char* hdmi_edid_info);
int  HalBtv_SetHdmiHotpluggedListener(DISPLAY_HDMIHotPluggedCallback callback);
int  HalBtv_SetCECPowerStateListener(DISPLAY_CECPowerStateCallback callback);

int  HalBtv_SetLedState(int led_id, int state);
int  HalBtv_SetLedColor(int led_id, int color);
int  HalBtv_SetVolumeLedState(int led_id, int state, int volume);
int  HalBtv_SetBeamformingLedState(int led_id, int state, int degree);
int  HalBtv_SetMicOnOff(bool enable);
int  HalBtv_GetMicVolume(int *volume_level);
int  HalBtv_SetMicVolume(int volume_level);
int  HalBtv_GetMicEchoLevel(int *echo_level);
int  HalBtv_SetMicEchoLevel(int echo_level);
int  HalBtv_MicPause();
int  HalBtv_MicResume();

#ifdef __cplusplus
}
#endif // __cplusplus

#endif  // SOC_MANAGER_H
