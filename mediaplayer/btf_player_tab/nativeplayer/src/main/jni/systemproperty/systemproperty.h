#ifndef _SYSTEMPROPERTY_H_
#define _SYSTEMPROPERTY_H_

#ifdef __cplusplus
extern "C" {
#endif

#define PROPERTY__STB_ID						"STB_ID" // G/S, Secure
#define PROPERTY__STB_ID_RENAME						"STBID" // G/S, Secure
#define PROPERTY__USER_ID						"USER_ID" // G/S, Secure
#define PROPERTY__USER_NAME					"USER_NAME" // G/S, Secure
#define PROPERTY__USER_ADDR					"USER_ADDR" // G/S, Secure
#define PROPERTY__STB_PASSWORD					"STB_PASSWORD" // G/S, Secure

#define PROPERTY__ETHERNET_MAC					"ETHERNET_MAC" // G
#define PROPERTY__GW_TOKEN                   "GW_TOKEN"
#define PROPERTY__AUTO_RESETBOOT                   "AUTORESETBOOT"

#define PROPERTY__SERIAL_NUMBER				"SERIAL_NUMBER" // G
#define PROPERTY__MODEL_NAME					"MODEL_NAME" // G
#define PROPERTY__FIRMWARE_VERSION				"FIRMWARE_VERSION" // G
#define PROPERTY__FIRMWARE_BUILD_DATE			"FIRMWARE_BUILD_DATE" // G
#define PROPERTY__FIRMWARE_UPDATE_DATE			"FIRMWARE_UPDATE_DATE" // G
#define PROPERTY__PURCHASE_CERTIFICATION		"PURCHASE_CERTIFICATION" // G/S
#define PROPERTY__COMMON_ADULT_CLASS			"COMMON_ADULT_CLASS" // G/S
#define PROPERTY__PICTURE_RATIO				"PICTURE_RATIO" // G/S
#define PROPERTY__TV_TYPE						"TV_TYPE" // G/S
#define PROPERTY__BRIGHTNESS					"BRIGHTNESS" // G/S
#define PROPERTY__CONTRAST						"CONTRAST" // G/S
#define PROPERTY__SATURATION					"SATURATION" // G/S
#define PROPERTY__HUE							"HUE" // G/S
#define PROPERTY__SOUND						"SOUND" // G/S
//#define PROPERTY__MEDIA_VOLUME					"MEDIA_VOLUME" // G/S
//#define PROPERTY__BELL_VOLUME					"BELL_VOLUME" // G/S
//#define PROPERTY__CALL_VOLUME					"CALL_VOLUME" // G/S
#define PROPERTY__CONSECUTIVE_PLAY				"CONSECUTIVE_PLAY" // G/S
#define PROPERTY__AUTO_HOME					"AUTO_HOME" // G/S
#define PROPERTY__RESERVATION_TIME				"RESERVATION_TIME" // G/S
#define PROPERTY__CHILDREN_SEE_LIMIT			"CHILDREN_SEE_LIMIT" // G/S
#define PROPERTY__CHILDREN_SEE_LIMIT_TIME		"CHILDREN_SEE_LIMIT_TIME" // G/S
#define PROPERTY__ADULT_MENU					"ADULT_MENU" // G/S
#define PROPERTY__BTV_ADULT_CLASS				"BTV_ADULT_CLASS" // G/S
#define PROPERTY__CHILDREN_SEE_REMAIN_TIME		"CHILDREN_SEE_REMAIN_TIME" // G/S
#define PROPERTY__SLEEP_MODE					"SLEEP_MODE" // G/S
#define PROPERTY__ACTUAL_CUSTOMER				"ACTUAL_CUSTOMER" // G/S
#define PROPERTY__MINI_TV						"MINI_TV" // G/S

#define PROPERTY__STATE_RCU_SKT				"STATE_RCU_SKT" // G

#define PROPERTY__LASTFRAME			"LASTFRAME" // 1 / 0
#define PROPERTY__HEARING_IMPAIRED				"HEARING_IMPAIRED" // G/S
#define PROPERTY__VISION_IMPAIRED				"VISIONIMPAIRED" // G/S
#define PROPERTY__SCALING_MODE				"SCALING_MODE"

#define PROPERTY__FW_INFO "fw_info"
#define PROPERTY__FW_VER "fw_ver"
#define PROPERTY__STB_MODEL "STB_MODEL"

#define PROPERTY__PACKAGE_INTERNAL_DATA_PATH				"INTERNAL_DATA_PATH" // G/S
#define PROPERTY__MLR_ACTIVATION_ENABLE         "MLR_ACTIVATION_ENABLE"

#define PROPERTY_NAME_HEAR		"vendor.sptek.hear" 	// Caption On/Off
#define PROPERTY_NAME_CAPTION	"vendor.sptek.caption" 	// Caption Mode
#define PROPERTY_NAME_EYE		"vendor.sptek.eye" 		// VI On/Off
#define PROPERTY_NAME_LANGUAGE	"vendor.sptek.language" // language

#define KEC_ENABLE_REFERENCE_KEY	"vendor.skb.btv.manufacturer.kec"

#define PROPERTY_NAME_PKT_LOG   "vendor.sptek.pkt.log" //Received packet log

int get_pkt_logging_enable(void);
int set_systemproperty(const char* property, const char* value, int length);
int get_systemproperty(const char* property, char* value, int length);

int set_settings_property(const char* property, const char* value, int length);
int get_settings_property(const char* property, char* value, int length);

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* _SYSTEMPROPERTY_H_ */
