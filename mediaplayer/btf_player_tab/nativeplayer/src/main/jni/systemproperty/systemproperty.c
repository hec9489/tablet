#ifdef __cplusplus
extern "C" {
#endif

#define LOG_TAG "BOXKEY-SYSTEM_PROPERTY"

#include <stdio.h>
#include <string.h>

#include <stdlib.h>
#include <stdbool.h>

#include <ALog.h>

#include "systemproperty.h"

#include "definePropertyInfo.h"

static const char* mac_address;
static const char* stb_id;
static const char* gw_token;
static char lastFrame[20] = "0";
static char* hearing_Impaired = "false";
static char* vision_impaird = "false";
static char* mlr_enabled  = "true";
static char* kec_enable_key = "-1";

static const char* internalDataPath;

static char* sptek_hear = "-1";
static char* sptek_caption = "-1";
static char* sptek_eye = "-1";
static char* sptek_language = "-1";

static char* sptek_pkt_log = "0";

int get_pkt_logging_enable(void)
{
    int logging_enable = 0;
    char pkt_log[128] ={0};

    if(sptek_pkt_log != NULL) 
    {
      sprintf(pkt_log, "%s", sptek_pkt_log);
      if(strcmp(sptek_pkt_log, "1") == 0) 
      {
        logging_enable = 1;
      }
    }
    return logging_enable;
}

int get_system_mlractivateionEnable(char* value, int length){
    if(mlr_enabled != NULL) {
        sprintf(value, "%s", mlr_enabled);
    } else {
        return 2;
    }
    return 0;
}

int set_system_mlractivateionEnable(char* value, int length){
    mlr_enabled = (char*)malloc(20);
    sprintf(mlr_enabled, "%s", value);
}

int setProperty(const char* property, const char* value, int length, int propertyType) {
	ALOGI("++setProperty %s, %s", property, value);
    if(strcmp(PROPERTY__STB_ID, property) == 0){
    	stb_id = (char*)malloc(64);
        sprintf(stb_id, "%s", value);
    } else if(strcmp(PROPERTY__ETHERNET_MAC, property) == 0){
		mac_address = (char*)malloc(18);
        sprintf(mac_address, "%s", value);
    }else if(strcmp(PROPERTY__GW_TOKEN, property) == 0) {
        gw_token = (char *) malloc(40);
        sprintf(gw_token, "%s", value);
    } else if(strcmp(PROPERTY__LASTFRAME, property) == 0){
        sprintf(lastFrame, "%s", value);
    } else if(strcmp(PROPERTY__HEARING_IMPAIRED, property) == 0){
        hearing_Impaired = (char*)malloc(20);
        sprintf(hearing_Impaired, "%s", value);
    } else if(strcmp(PROPERTY__VISION_IMPAIRED, property) == 0){
        vision_impaird = (char*)malloc(20);
        sprintf(vision_impaird, "%s", value);
    } else if(strcmp(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, property) == 0){
        internalDataPath = (char*)malloc(64);
        sprintf(internalDataPath, "%s", value);
    } else if(strcmp(PROPERTY__MLR_ACTIVATION_ENABLE, property) == 0){
       set_system_mlractivateionEnable(value, length);
    } else if(strcmp(PROPERTY_NAME_HEAR, property) == 0){
        sptek_hear = (char*)malloc(128);
        sprintf(sptek_hear, "%s", value);
    } else if(strcmp(PROPERTY_NAME_CAPTION, property) == 0){
        sptek_caption = (char*)malloc(128);
        sprintf(sptek_caption, "%s", value);
    } else if(strcmp(PROPERTY_NAME_EYE, property) == 0){
        sptek_eye = (char*)malloc(128);
        sprintf(sptek_eye, "%s", value);
    } else if(strcmp(PROPERTY_NAME_LANGUAGE, property) == 0){
        sptek_language = (char*)malloc(128);
        sprintf(sptek_language, "%s", value);
    } else if(strcmp(KEC_ENABLE_REFERENCE_KEY, property) == 0){
        kec_enable_key = (char*)malloc(128);
        sprintf(kec_enable_key, "%s", value);
    } else if(strcmp(PROPERTY_NAME_PKT_LOG, property) == 0){
        sptek_pkt_log = (char*)malloc(128);
        sprintf(sptek_pkt_log, "%s", value);
    }
	return 1;
}

int get_system_mac(char* macAddr, int length)
{
    if(mac_address != NULL && strlen(mac_address) > 10) {
        sprintf(macAddr, "%s", mac_address);
    } else {
        return 2;
    }
    //sprintf(macAddr, "30:eb:25:1d:56:24");
    return 0;
}


int getProperty(const char* property, char* value, int length, int propertyType) {
	if(value != NULL) memset(value, 0, length);
	ALOGI("++getProperty %s, %s", property, value);
    memset(value, 0, length);
	if (property != NULL && strlen(property) > 0 && value != NULL && length > 0 /*&& length < MAX_PROPERTY_VALUE_LENGTH*/) {
		if (strcmp(PROPERTY__ETHERNET_MAC, property) == 0) {
			if (get_system_mac(value, length) != 0) {
                ALOGI("++property_get fail, %s", property);
				return 2;
			}
		}/*else if(strcmp(PROPERTY__STB_MODEL, property) == 0) {
            sprintf(value, "BIP-AI100");
		} else if (strcmp(PROPERTY__FW_INFO, property) == 0 ||
				   strcmp(PROPERTY__FW_VER, property) == 0) {
			sprintf(value, "SW-");
		}*/ else if(strcmp(PROPERTY__STB_ID, property) == 0 || strcmp("STBID", property) == 0) {
            if(stb_id != NULL && strlen(stb_id) > 10){
                sprintf(value, "%s", stb_id);
            } else {
               ALOGI("++property_get fail, %s", property);
               return 2;
            }
            //sprintf(value, "{69AF56BB-BA9A-11EA-AB15-C57A43B26EA7}");
        } else if(strcmp(PROPERTY__GW_TOKEN, property) == 0) {
            if (gw_token != NULL && strlen(gw_token) > 10) {
                sprintf(value, "%s", gw_token);
            } else {
                ALOGI("++property_get fail, %s", property);
                return 2;
            }
            //sprintf(value, "691b21cd-3fe2-4ff2-88ab-d74a677ea411");
        } else if(strcmp(PROPERTY__LASTFRAME, property) == 0) {
            if (lastFrame != NULL && strlen(lastFrame) > 0) {
                sprintf(value, "%s", lastFrame);
            } else {
                ALOGI("++property_get fail, %s", property);
                return 2;
            }
        } else if(strcmp(PROPERTY__HEARING_IMPAIRED, property) == 0){
            if (hearing_Impaired != NULL) {
                sprintf(value, "%s", hearing_Impaired);
            } else {
                ALOGI("++property_get fail, %s", property);
                return 2;
            }
        } else if(strcmp(PROPERTY__VISION_IMPAIRED, property) == 0){
            if (vision_impaird != NULL) {
                sprintf(value, "%s", vision_impaird);
            } else {
                ALOGI("++property_get fail, %s", property);
                return 2;
            }
        } else if(strcmp(PROPERTY__PACKAGE_INTERNAL_DATA_PATH, property) == 0){
            if (internalDataPath != NULL) {
                sprintf(value, "%s", internalDataPath);
            } else {
                ALOGI("++property_get fail, %s", property);
                return 2;
            }
        } else if(strcmp(PROPERTY__MLR_ACTIVATION_ENABLE, property) == 0){
               get_system_mlractivateionEnable(value, 20);
		} else if(strcmp(PROPERTY_NAME_HEAR, property) == 0) {
            if (sptek_hear != NULL) {
                sprintf(value, "%s", sptek_hear);
            } else {
                ALOGI("++property_get fail, %s", property);
                return 2;
            }
        } else if(strcmp(PROPERTY_NAME_CAPTION, property) == 0) {
            if (sptek_caption != NULL) {
                sprintf(value, "%s", sptek_caption);
            } else {
                ALOGI("++property_get fail, %s", property);
                return 2;
            }
        } else if(strcmp(PROPERTY_NAME_EYE, property) == 0) {
            if (sptek_eye != NULL) {
                sprintf(value, "%s", sptek_eye);
            } else {
                ALOGI("++property_get fail, %s", property);
                return 2;
            }
        } else if(strcmp(PROPERTY_NAME_LANGUAGE, property) == 0) {
            if (sptek_language != NULL) {
                sprintf(value, "%s", sptek_language);
            } else {
                ALOGI("++property_get fail, %s", property);
                return 2;
            }
        } else if(strcmp(KEC_ENABLE_REFERENCE_KEY, property) == 0) {
            if (kec_enable_key != NULL) {
                sprintf(value, "%s", kec_enable_key);
            } else {
                ALOGI("++property_get fail, %s", property);
                return 2;
            }
        } else if(strcmp(PROPERTY_NAME_PKT_LOG, property) == 0) {
            if (sptek_pkt_log != NULL) {
                sprintf(value, "%s", sptek_pkt_log);
            } else {
                ALOGI("++property_get fail, %s", property);
                return 2;
            }
        } else {
            ALOGI("++property_get fail, %s", property);
            return 2;
		}
		//ALOGI("++property_get success, %s, %s", property, value);
		return 0;
	}

    ALOGI("++property_get fail, %s, %s", property, value);

	return 1;
}

int set_systemproperty(const char* property, const char* value, int length) {
	return setProperty(property, value, length, TYPE_PROPERTY_NORMAL);
}

int get_systemproperty(const char* property, char* value, int length) {
	return getProperty(property, value, length, TYPE_PROPERTY_NORMAL);
}

int set_settings_property(const char* property, const char* value, int length) {
	return setProperty(property, value, length, TYPE_PROPERTY_SETTINGS);
}

int get_settings_property(const char* property, char* value, int length) {
	return getProperty(property, value, length, TYPE_PROPERTY_SETTINGS);
}

#ifdef __cplusplus
} // extern "C"
#endif
