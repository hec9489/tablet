#ifndef ______________DEFINE_PROPERTY_INFO__________________H_
#define ______________DEFINE_PROPERTY_INFO__________________H_

#ifdef __cplusplus
extern "C" {
#endif

#define IO_SIZE 64
#define STR_SIZE 192

#define MAX_PROPERTY_VALUE_LENGTH 1024
#define MAX_COUNT 2048
#define PROPERTY_FAIL 0
#define PROPERTY_SUCCESS 1

typedef struct TAG_PROPERTY_INFO {
    char name[STR_SIZE];
    char value[MAX_PROPERTY_VALUE_LENGTH];
} t_property_info;

#define SAVE_PATH "/btv_home/run/system_property.pro"
#define SAVE_NORMAL_XML_PATH "/btv_home/config/tvservice-system-properties.xml"
#define SAVE_SETTINGS_XML_PATH "/btv_home/config/tvservice-setting-properties.xml"

#define PROPERTY_VERSION_PREFIX "SPTEK_FILE_VERSION"
#define PROPERTY_VERSION_CODE "1.0.2"

#define TYPE_PROPERTY_NORMAL 0
#define TYPE_PROPERTY_SETTINGS 1

typedef struct TAG_SPTEK_PROPERTY_VERSION {
    char versionPrefix[18];
    char versionCode[5];
} t_sptek_property_version;


#ifdef __cplusplus
} // extern "C"
#endif

#endif /* ______________DEFINE_PROPERTY_INFO__________________H_ */
