package com.skb.framework.playerui.ttachannel;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;

import androidx.appcompat.app.AppCompatDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.skb.framework.playerui.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by parkjeongho on 2021-05-11 오후 3:57
 */
public class TTAChannelDialog extends AppCompatDialog {
    private final String TTA_CHANNEL_CONFIG_PATH = "/btf/btvmedia/tta_channel/";
    private final String TTA_CHANNEL_FILENAME = "TTAchannel.json";

    private Context mContext;

    private RecyclerView mRvChannel;
    private LinearLayoutManager mLayoutManager;
    private ChannelAdapter mChannelAdapter;

    private ChannelChoiceListener mChannelChoiceListener;

    public TTAChannelDialog(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        setContentView(R.layout.dialog_tta_channel);

        findViewById(R.id.dialog_close).setOnClickListener(v -> dismiss());

        initRvChannel();
        showChannel();
    }

    public void setListener(ChannelChoiceListener listener) {
        mChannelChoiceListener = listener;
    }

    private void initRvChannel() {
        mRvChannel = findViewById(R.id.rv_channel);
        mLayoutManager = new LinearLayoutManager(mContext);

        if(mChannelAdapter != null) {
            mChannelAdapter.removeAll();
        }

        mRvChannel.setHasFixedSize(true);
        mRvChannel.setLayoutManager(mLayoutManager);
        mChannelAdapter = new ChannelAdapter() {
            @Override
            public void onCardClick(ChannelInfo item) {
                mChannelChoiceListener.onChoice(item.channelUrl);
                dismiss();
            }
        };
        mRvChannel.setAdapter(mChannelAdapter);
    }

    private void showChannel() {
        ArrayList<ChannelInfo> channels = new ArrayList<>();

        String packageDataFolder = mContext.getDataDir().getPath();
        String txt = readTextFile(packageDataFolder+TTA_CHANNEL_CONFIG_PATH+TTA_CHANNEL_FILENAME);

        Gson gson = new Gson();
        ChannelInfoList list = gson.fromJson(txt, ChannelInfoList.class);

        int size = list.channelList.size();
        for(int i=0; i<size; i++) {
            channels.add(list.channelList.get(i));
        }

        mChannelAdapter.removeAll();
        mChannelAdapter.add(channels);
    }

    public String readTextFile(String path) {
        StringBuffer strBuffer = new StringBuffer();
        try{
            InputStream is = new FileInputStream(path);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line="";
            while((line=reader.readLine())!=null){
                strBuffer.append(line+"\n");
            }

            reader.close();
            is.close();
        }catch (IOException e){
            e.printStackTrace();
            return "";
        }
        return strBuffer.toString();
    }
}
