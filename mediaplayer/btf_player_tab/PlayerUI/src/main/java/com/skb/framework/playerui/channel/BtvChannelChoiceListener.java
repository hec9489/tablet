package com.skb.framework.playerui.channel;

import com.skb.btv.framework.navigator.dvbservice.AVDvbService;

/**
 * Created by parkjeongho on 2021-05-20 오후 9:13
 */
public interface BtvChannelChoiceListener {
    void onChannelChoice(AVDvbService channel);
}
