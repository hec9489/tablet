package com.skb.framework.playerui.channel;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;

import androidx.appcompat.app.AppCompatDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.skb.btv.framework.navigator.dvbservice.AVDvbService;
import com.skb.framework.playerui.R;
import com.skb.framework.playerui.ttachannel.ChannelAdapter;
import com.skb.framework.playerui.ttachannel.ChannelChoiceListener;
import com.skb.framework.playerui.ttachannel.ChannelInfo;
import com.skb.framework.playerui.ttachannel.ChannelInfoList;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by parkjeongho on 2021-05-20 오후 9:14
 */
public class BtvChannelDialog extends AppCompatDialog {
    private Context mContext;

    private RecyclerView mRvChannel;
    private LinearLayoutManager mLayoutManager;
    private BtvChannelAdapter mChannelAdapter;

    private BtvChannelChoiceListener mChannelChoiceListener;
    private ArrayList<AVDvbService> mChannelData;

    public BtvChannelDialog(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        setContentView(R.layout.dialog_tta_channel);

        findViewById(R.id.dialog_close).setOnClickListener(v -> dismiss());

        initRvChannel();
        showChannel();
    }

    public void setListener(BtvChannelChoiceListener listener) {
        mChannelChoiceListener = listener;
    }

    public void setChannelData(ArrayList<AVDvbService> data) {
        mChannelData = data;
    }

    private void initRvChannel() {
        mRvChannel = findViewById(R.id.rv_channel);
        mLayoutManager = new LinearLayoutManager(mContext);

        if(mChannelAdapter != null) {
            mChannelAdapter.removeAll();
        }

        mRvChannel.setHasFixedSize(true);
        mRvChannel.setLayoutManager(mLayoutManager);
        mChannelAdapter = new BtvChannelAdapter() {
            @Override
            public void onCardClick(AVDvbService item) {
                mChannelChoiceListener.onChannelChoice(item);
                dismiss();
            }
        };
        mRvChannel.setAdapter(mChannelAdapter);
    }

    private void showChannel() {
        mChannelAdapter.removeAll();
        mChannelAdapter.add(mChannelData);
    }
}
