package com.skb.framework.playerui.channel;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.skb.btv.framework.navigator.dvbservice.AVDvbService;
import com.skb.framework.playerui.R;
import com.skb.framework.playerui.ttachannel.ChannelInfo;

import java.util.ArrayList;

/**
 * Created by parkjeongho on 2021-05-20 오후 9:04
 */
public abstract class BtvChannelAdapter extends RecyclerView.Adapter<BtvChannelAdapter.ViewHolder> {
    private ArrayList<AVDvbService> mItems;
    private int mItemCount;

    public BtvChannelAdapter() {
        mItems = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_btv_channel, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final AVDvbService item = mItems.get(position);

        holder.mTvChannelTitle.setText(item.getName());
        holder.mTvChannelNumber.setText(Integer.toString(item.getCh()));

        holder.mCvBase.setOnClickListener(view -> onCardClick(item));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public int add(ArrayList<AVDvbService> arrayList) {
        int size = arrayList.size();
        for(int i=0; i<size; i++) {
            mItems.add(arrayList.get(i));
        }

        mItemCount = size;
        notifyItemInserted(mItems.size());

        return size;
    }

    public void remove(int pos) {
        mItems.remove(pos);
        mItemCount = mItems.size();
        notifyDataSetChanged();
    }

    public void remove(ChannelInfo item) {
        mItems.remove(item);
        mItemCount = mItems.size();
        notifyDataSetChanged();
    }

    public void removeAll(){
        mItems.clear();
    }

    public abstract void onCardClick(AVDvbService item);

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTvChannelTitle;
        public TextView mTvChannelNumber;
        public CardView mCvBase;

        public ViewHolder(View itemView) {
            super(itemView);

            mTvChannelTitle = itemView.findViewById(R.id.tv_channel_title);
            mTvChannelNumber = itemView.findViewById(R.id.tv_channel_number);
            mCvBase = itemView.findViewById(R.id.cv_base);
        }
    }
}