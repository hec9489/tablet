package com.skb.framework.playerui.ttachannel;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.skb.framework.playerui.R;

import java.util.ArrayList;

/**
 * Created by parkjeongho on 2021-05-11 오후 4:36
 */
public abstract class ChannelAdapter extends RecyclerView.Adapter<ChannelAdapter.ViewHolder> {
    private ArrayList<ChannelInfo> mItems;
    private int mItemCount;

    public ChannelAdapter() {
        mItems = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_channel, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ChannelInfo item = mItems.get(position);

        holder.mTvChannelType.setText(item.testType);
        if(item.testType.contains("영상복호화")) {
            holder.mTvChannelType.setTextColor(Color.parseColor("#231F20"));
        } else if(item.testType.contains("음성복호화")) {
            holder.mTvChannelType.setTextColor(Color.parseColor("#3C5A99"));
        } else if(item.testType.contains("역다중화")) {
            holder.mTvChannelType.setTextColor(Color.parseColor("#231F20"));
        } else {
            holder.mTvChannelType.setTextColor(Color.parseColor("#3C5A99"));
        }

        holder.mCvBase.setOnClickListener(view -> onCardClick(item));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public int add(ArrayList<ChannelInfo> arrayList) {
        int size = arrayList.size();
        for(int i=0; i<size; i++) {
            mItems.add(arrayList.get(i));
        }

        mItemCount = size;
        notifyItemInserted(mItems.size());

        return size;
    }

    public void remove(int pos) {
        mItems.remove(pos);
        mItemCount = mItems.size();
        notifyDataSetChanged();
    }

    public void remove(ChannelInfo item) {
        mItems.remove(item);
        mItemCount = mItems.size();
        notifyDataSetChanged();
    }

    public void removeAll(){
        mItems.clear();
    }

    public abstract void onCardClick(ChannelInfo item);

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTvChannelType;
        public CardView mCvBase;

        public ViewHolder(View itemView) {
            super(itemView);

            mTvChannelType = itemView.findViewById(R.id.tv_channel_type);
            mCvBase = itemView.findViewById(R.id.cv_base);
        }
    }
}