package com.skb.framework.playerui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.skb.btv.framework.media.DvbServicePlayer;
import com.skb.btv.framework.navigator.BtvNavigator;
import com.skb.btv.framework.navigator.DvbSIService;
import com.skb.btv.framework.navigator.IBtvNavigator;
import com.skb.btv.framework.navigator.OnDsmccListener;
import com.skb.btv.framework.navigator.OnDvbSIListener;
import com.skb.btv.framework.navigator.SignalEvent;
import com.skb.btv.framework.navigator.dvbservice.AVDvbService;
import com.skb.btv.framework.navigator.dvbservice.AVDvbServiceList;
import com.skb.btv.framework.navigator.program.AVProgram;
import com.skb.btv.framework.navigator.program.AVProgramList;
import com.skb.framework.playerui.channel.BtvChannelChoiceListener;
import com.skb.framework.playerui.channel.BtvChannelDialog;
import com.skb.framework.playerui.program.ProgramAdapter;
import com.skb.framework.playerui.ttachannel.ChannelChoiceListener;
import com.skb.framework.playerui.ttachannel.TTAChannelDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.skb.btv.framework.media.core.MediaPlayer.MEDIA_INFO_EXTRA_PLAY_BAD_RECEIVING;
import static com.skb.btv.framework.media.core.MediaPlayer.MEDIA_INFO_EXTRA_PLAY_GOOD;
import static com.skb.btv.framework.media.core.MediaPlayer.MEDIA_INFO_EXTRA_SHOW_OF_FIRST_I_FRAME;
import static com.skb.btv.framework.media.core.MediaPlayer.MEDIA_INFO_WHAT_BTV_PLAYER;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        ChannelChoiceListener, BtvChannelChoiceListener {

    private final String TAG = "MainActivity";

    private final String PROPERTY_NAME_HEAR     = "vendor.sptek.hear";      // Caption On/Off
    private final String PROPERTY_NAME_CAPTION  = "vendor.sptek.caption";   // Caption Mode
    private final String PROPERTY_NAME_EYE      = "vendor.sptek.eye";       // VI On/Off
    private final String PROPERTY_NAME_LANGUAGE = "vendor.sptek.language";  // language
    private final String PROPERTY_NAME_PKT_LOG  = "vendor.sptek.pkt.log";  //Received packet log

    private SurfaceView mIPTVSurfaceView;
    private SurfaceHolder mIPTVSurfaceHolder;
    private TextureView mCCTextureView;
    private LinearLayout mLayoutTtaOptionBoard;
    private LinearLayout mLayoutAVOffsetBoard;

    private boolean mIsSufaceViewDestroyed = false;
    private boolean mIsNetworkingGood = true;

    private Handler mHandler;
    private int current_index = 0;

    private TextView mTextView;
    private TextView mChannelTitle;

    private SharedPreferences sharedPreferences;

    private DvbServicePlayer mMainTVMediaPlayer;

    private ToggleButton mlrToogleBtn;
    private ProgressBar mProgressBar;

    private String mTtaTestCurrentChannelUrl = "";

    private DvbSIService mDvbSIService;
    private ArrayList<AVDvbService> mChannelData;
    private Context mContext;
    private boolean mIsUseLocalInstance = false;

    /**
     * program
     */
    private int currentChannelSid;
    private Spinner mChannelProgramInfo;
    private List<AVProgram> mProgramList = new ArrayList<AVProgram>();
    private ProgramAdapter programAdapter;
    private int currentRunningProgramIndex = 0;
    private TextView empty_program;

    SurfaceHolder.Callback onMainSurfaceCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            Log.d(TAG, "onMainSurfaceCallback - surfaceCreated");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "new DvbServicePlayer()");
                    mMainTVMediaPlayer = new DvbServicePlayer(MainActivity.this);
                    //mMainTVMediaPlayer = new DvbServicePlayer(MainActivity.this, "{69AF56BB-BA9A-11EA-AB15-C57A43B26EA7}", "30:EB:25:1D:56:24","691b21cd-3fe2-4ff2-88ab-d74a677ea411");
                    Log.d(TAG, "setDisplay(mIPTVSurfaceHolder)");
                    mMainTVMediaPlayer.setDisplay(mIPTVSurfaceHolder);
                    mMainTVMediaPlayer.setOnPreparedListener(new DvbServicePlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(DvbServicePlayer mediaPlayer) {
                            //String chInfo = "CH No. " + mChannelData.get(mCurrentPosition).getCh() + "   -  " + mChannelData.get(mCurrentPosition).getName();
                            Log.d(TAG, "playerCallback - onPrepared,  mp : " + mediaPlayer);

                            mMainTVMediaPlayer.start();
                        }
                    });

                    mMainTVMediaPlayer.setOnInfoListener(new DvbServicePlayer.OnInfoListener() {
                        @Override
                        public boolean onInfo(DvbServicePlayer mp, int what, int extra) {
                              Log.d(TAG, "playerCallback - onInfo, what:" + what + ", mp : " + mp);
                            if (what == 7001) {
                                // Video Pid changed
                                // extra => 변경된 Pid 값
                                Log.d(TAG, "7001 pid => " + extra);
                                String url = changeTTAVedioPid(mTtaTestCurrentChannelUrl, extra);
                                setTVforTTA(url);
                            } else if (what == 7002) {
                                // audio Pid changed
                                // extra => 변경된 Pid 값
                                Log.d(TAG, "7002 pid => " + extra);
                                String url = changeTTAAudioPid(mTtaTestCurrentChannelUrl, extra);
                                setTVforTTA(url);
                            } else if (what == MEDIA_INFO_WHAT_BTV_PLAYER) {
                                if(extra == MEDIA_INFO_EXTRA_PLAY_BAD_RECEIVING) {
                                    if(mIsNetworkingGood) {
                                        mIsNetworkingGood = false;
                                        mProgressBar.setVisibility(View.VISIBLE);
                                    }
                                } else if(extra == MEDIA_INFO_EXTRA_SHOW_OF_FIRST_I_FRAME || extra == MEDIA_INFO_EXTRA_PLAY_GOOD) {
                                    if(!mIsNetworkingGood) {
                                        mIsNetworkingGood = true;
                                        mProgressBar.setVisibility(View.GONE);
                                    }
                                }
                            }
                            return false;
                        }
                    });

                    mMainTVMediaPlayer.setOnCompletionListener(new DvbServicePlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(DvbServicePlayer mp) {
                            Log.d(TAG, "playerCallback - onCompletion, mp:" + mp);
                        }
                    });

                    mMainTVMediaPlayer.setOnErrorListener(new DvbServicePlayer.OnErrorListener() {
                        @Override
                        public boolean onError(DvbServicePlayer mp, int what, int extra) {
                            Log.e(TAG, "playerCallback - onError, what:" + what + ", mp : " + mp);
                            return false;
                        }
                    });

                    mCCTextureView.setKeepScreenOn(true);
                    mCCTextureView.setSurfaceTextureListener(new CanvasListener());
                    // mChannelSpinner.setSelection(mCurrentPosition);
                    mMainTVMediaPlayer.setStbId("{69AF56BB-BA9A-11EA-AB15-C57A43B26EA7}");
                    mMainTVMediaPlayer.setMacAddress("30:EB:25:1D:56:24");
                    mMainTVMediaPlayer.setGwToken("691b21cd-3fe2-4ff2-88ab-d74a677ea411");
                    boolean mlrActivation = mMainTVMediaPlayer.getMlrActivationEnable();
                    Log.d(TAG, "call getMlrActivationEnable check : "+mlrActivation);
                    mlrToogleBtn.setEnabled(true);
                    mlrToogleBtn.setChecked(mlrActivation);
                    mlrToogleBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            Log.d(TAG, "call setMlrActivationEnable check : "+isChecked);
                            mMainTVMediaPlayer.setMlrActivationEnable(isChecked);
                        }
                    });
                }
            });
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Log.d(TAG, "onMainSurfaceCallback - surfaceChanged, channel count : "
                     + ((mChannelData != null) ? mChannelData.size() : -1) + ", mMainTVMediaPlayer : " + mMainTVMediaPlayer);

            // if (mChannelData == null)
            //    return;

            //for (int i = 0; i < mChannelData.size(); i++) {
            //     AVDvbService channel = mChannelData.get(i);
            //     if (channel.getName().equals("JTBC GOLF")) {
            //          mCurrentPosition = i;
            //         break;
            //     }
            // }
            //Log.d(TAG, "onMainSurfaceCallback - surfaceChanged, mCurrentPosition : " + mCurrentPosition);
            //if (mMainTVMediaPlayer != null) {
            // Start Add dvbsi scenario - KGH
            // setTV(mCurrentPosition);
            //    if (mChannelData.get(mCurrentPosition).getChannelType() != IAVDvbService.AVDVBSERVICE_CHANNEL_TYPE_MUSIC_CHANNEL) {
            //         setTV(mCurrentPosition);
            //     }
            //}
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            Log.d(TAG, "onMainSurfaceCallback - surfaceDestroyed");
            mIsSufaceViewDestroyed = true;
        }
    };

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick" + v.getId());
        int id = v.getId();
        switch (id) {
            case R.id.iv_up:
                moveChannel(true, false);
                break;

            case R.id.iv_down:
                moveChannel(false, false);
                break;

            case R.id.ll_channel_info:
                BtvChannelDialog builder = new BtvChannelDialog(MainActivity.this);
                builder.setCancelable(false);
                builder.setChannelData(mChannelData);
                builder.setListener(MainActivity.this);
                builder.show();
                break;

/*
            case R.id.stbid:
                if(mMainTVMediaPlayer != null) {
                    mMainTVMediaPlayer.setStbId("{69AF56BB-BA9A-11EA-AB15-C57A43B26EA7}");
                }
                break;

            case R.id.macaddress:
                if(mMainTVMediaPlayer != null) {
                    mMainTVMediaPlayer.setMacAddress("30:EB:25:1D:56:24");
                }
                break;

            case R.id.token:
                if(mMainTVMediaPlayer != null) {
                    mMainTVMediaPlayer.setGwToken("691b21cd-3fe2-4ff2-88ab-d74a677ea411");
                }
                break;
*/

            case R.id.scaling:
                if(mMainTVMediaPlayer != null) {
                    mMainTVMediaPlayer.setVideoScalingMode(1);
                }
                break;
        }

    }

    private void moveChannel(boolean isNext, boolean isPip) {
        int idx, count;
        idx = current_index; //mChannelSpinner.getSelectedItemPosition();
        count = mChannelData.size(); //mChannelSpinner.getCount();
        if (isNext) {
            if (++idx >= count) {
                idx = 0;
            }
        } else {
            if (--idx < 0) {
                idx = count - 1;
            }
        }
        //Log.d(TAG, "moveChannel - call setTV or setPIP. isPIPShowing : " + isPIPShowing);
        setTV(idx);

    }

    private void setTV(int index) {
        //Log.d(TAG, "setTV - index : " + index + ", mChannelData : " + (mChannelData != null));
        //if (mChannelData == null
        //        || (mChannelData.get(index).getChannelType() == IAVDvbService.AVDVBSERVICE_CHANNEL_TYPE_MUSIC_CHANNEL && mMusicChannelData == null)) {
        //   return;
        //}
        //Log.d(TAG, "setTV - ChannelType: " + mChannelData.get(index).getChannelType());
        current_index = index;
        Log.d(TAG, "setTV index : "+index);
        if(mMainTVMediaPlayer != null && mChannelData != null && mChannelData.size() > 0) {
            Log.d(TAG, "call tune");
            String uri = mChannelData.get(index).getChannelUri();
            String chNo = getChannelNumber(uri);
            Log.d(TAG, "channel number : "+chNo);
            uri = convert(uri);
            Log.d(TAG, "channel uri : "+uri);


            mTextView.setText(chNo);
            mChannelTitle.setText(mChannelData.get(index).getName());
            //mTextView.setTextColor(0x00ff00);
            //String uri = "239.192.49.7;49220;1623;2623;1623;27;129;5303;19158;1;1;1;1;";

            //if (mChannelData.get(index).getChannelType() == IAVDvbService.AVDVBSERVICE_CHANNEL_TYPE_MUSIC_CHANNEL) {
            //    uri = mMusicChannelData.get(0).getChannelUri();
            // } else {
                uri = mChannelData.get(index).getChannelUri() + "&lf=1";
            //}
            mMainTVMediaPlayer.setDisplay(mIPTVSurfaceHolder);
            mMainTVMediaPlayer.tune(uri);
            try {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mMainTVMediaPlayer.prepareAsync();
                    }
                }).start();
            } catch(Exception e) {
                e.printStackTrace();
            }
            //if (mDvbSIService != null) {
//                mDvbSIService.setCurrentChannelUri(uri);
            //}
        }
        onSaveIndex(current_index);
        //mCurrentPosition = index;
        //mChannelSpinner.setSelection(index);
        //mCurrentChannelSID = mChannelData.get(index).getSid();
        moveProgram(current_index);
    }

    private void moveProgram(int index){
        currentChannelSid = mChannelData.get(index).getSid();
        Log.d(TAG, "moveProgram channel : "+currentChannelSid);
        AVProgramList program_result = null;
        if (mIsUseLocalInstance) {
            if (mDvbSIService != null) {
                Log.d(TAG, "moveProgram1 channel : "+currentChannelSid);
                program_result = mDvbSIService.getAVProgramList(currentChannelSid);
            }
        } else {
            Log.d(TAG, "moveProgram2 channel : "+currentChannelSid);
            program_result = BtvNavigator.getDvbSIService(mContext).getAVProgramList(currentChannelSid);
        }
        Log.d(TAG, "moveProgram3 program_result : "+program_result);
        Log.d(TAG, "moveProgram3 program_result getAVProgramsList : "+program_result.getAVProgramsList());
        if(program_result != null && program_result.getAVProgramsList() != null && program_result.getAVProgramsList().size() > 0){
            if(mProgramList != null) mProgramList.clear();
            if(program_result.getAVProgramsList().get(0) != null && program_result.getAVProgramsList().get(0).getAVProgramList() != null) {
                for (AVProgram tempValue : program_result.getAVProgramsList().get(0).getAVProgramList()) {
                    mProgramList.add(tempValue);
                }
                Log.d(TAG, "moveProgram1 size : "+mProgramList.size());
                programAdapter.setList(mProgramList);
                programAdapter.notifyDataSetChanged();
                currentRunningProgramIndex = getCurrentRunningProgram();
                Log.d(TAG, "moveProgram1 currentRunningProgramIndex : "+currentRunningProgramIndex);
                mChannelProgramInfo.setSelection(currentRunningProgramIndex);
                mChannelProgramInfo.setVisibility(View.VISIBLE);
                empty_program.setVisibility(View.GONE);
            } else {
                empty_program.setVisibility(View.VISIBLE);
                mChannelProgramInfo.setVisibility(View.GONE);
            }
        }  else {
            empty_program.setVisibility(View.VISIBLE);
            mChannelProgramInfo.setVisibility(View.GONE);
        }

    }

    private int getCurrentRunningProgram(){
        int retIndex = -1;
        Calendar calendar = Calendar.getInstance();
        long reqST = calendar.getTimeInMillis();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        long reqET = calendar.getTimeInMillis();
        if(mProgramList != null && mProgramList.size() > 0){
            for(AVProgram entity : mProgramList) {
                retIndex++;
                long proST = entity.getStartDate().getTime();
                long proET = entity.getEndDate().getTime();
                if ((reqST <= proST && reqET >= proET)
                        || (reqST > proST && reqET >= proET && reqST < proET)
                        || (reqST <= proST && reqET < proET && reqET > proST)
                        || (reqST > proST && reqET < proET))
                {
                    break;
                }
            }
        }
        Log.d(TAG, "getCurrentRunningProgram() index : "+retIndex);
        return retIndex;
    }

    private String getChannelNumber(String url){
        int start = url.indexOf("ch=");
        int end = url.indexOf("&ci");
        Log.d(TAG, " getchannel start : "+start+" end : "+end);
        return url.substring(start+3, end);
    }

    private class CanvasListener implements TextureView.SurfaceTextureListener {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface,
                                              int width, int height) {
            Log.d(TAG, "CanvasListener");
            mMainTVMediaPlayer.setCCDisplay(mCCTextureView);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return true;
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface,
                                                int width, int height) {
            Log.d(TAG, "onSurfaceTextureSizeChanged");
				mMainTVMediaPlayer.setCCDisplay(mCCTextureView);
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
            //Log.d(TAG, "onSurfaceTextureUpdated " +mIsSufaceViewDestroyed);
            if(mIsSufaceViewDestroyed) {
                mIsSufaceViewDestroyed = false;
                mMainTVMediaPlayer.setCCDisplay(mCCTextureView);
            }
        }
    }

    private OnDvbSIListener onDvbSIListener = new OnDvbSIListener() {
        @Override
        public void onDvbSIUpdated(int dvbsiType, String extraData) {
            if (dvbsiType == IBtvNavigator.DVBSI_TYPE_UPDATE_CHANNEL) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String channelListVersion = "";
                        String extraString = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            extraString = json.getString(IBtvNavigator.DVBSI_JSONOBJECT_INFO);
                            channelListVersion = json.getString(IBtvNavigator.DVBSI_JSONOBJECT_DVBSERVICEVERSION);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d(TAG, "version : " + channelListVersion);
                        AVDvbServiceList channelList = null;
                        if (mIsUseLocalInstance) {
                            if (mDvbSIService != null) {
                                channelList = mDvbSIService.getAVDvbServiceList();
                            }
                        } else {
                            channelList = BtvNavigator.getDvbSIService(mContext).getAVDvbServiceList();
                        }

                        if (channelList != null && channelList.isReady()) {
                            mChannelData = Utils.channelParser(MainActivity.this, channelList);
                            for (int i = 0; i < mChannelData.size(); i++) {
                                Log.i(TAG, "onDvbSIUpdated() ch - " + mChannelData.get(i).getCh());
                            }
                        }
                    }
                });
            }


            if (mChannelData == null) {
                AVDvbServiceList channelList = null;
                if (mIsUseLocalInstance) {
                    if (mDvbSIService != null) {
                        channelList = mDvbSIService.getAVDvbServiceList();
                    }
                } else {
                    channelList = BtvNavigator.getDvbSIService(mContext).getAVDvbServiceList();
                }

                if (channelList != null && channelList.isReady()) {
                    mChannelData = Utils.channelParser(MainActivity.this, channelList);
                    Log.d(TAG, "onDvbSIUpdated() mChannelData size: " + mChannelData.size());
                    for(AVDvbService service : mChannelData) {
                        Log.i(TAG, "onDvbSIUpdated() - mChannelData - " + service.getCh() + "[" + service.getSid() + "] : " + service.getName() + " " + service.getChannelUri());
                    }
                }
            }
        }
    };

    private OnDsmccListener onDsmccListener = new OnDsmccListener() {
        @Override
        public void onDsmccEvent(String path, SignalEvent sigevent) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mHandler = new Handler();

        sharedPreferences = getSharedPreferences("btf_test_file",MODE_PRIVATE);

        mContext = this;
        mDvbSIService = BtvNavigator.getDvbSIService(mContext);
        mDvbSIService.setOnDvbSIListener(onDvbSIListener);
        mDvbSIService.setOnDsmccListener(onDsmccListener);

        mIPTVSurfaceView = findViewById(R.id.sv_player);
        mIPTVSurfaceView.setKeepScreenOn(true);
        mIPTVSurfaceHolder = mIPTVSurfaceView.getHolder();
        mIPTVSurfaceHolder.addCallback(onMainSurfaceCallback);
        mIPTVSurfaceHolder.setFormat(PixelFormat.TRANSPARENT);

        mTextView = findViewById(R.id.main);
        mChannelTitle = findViewById(R.id.channel_title);
        mCCTextureView = findViewById(R.id.trv_cc);
        mLayoutTtaOptionBoard = findViewById(R.id.ll_tta_option_board);

        mProgressBar = findViewById(R.id.progress_circular);
        mProgressBar.setIndeterminate(true);
        mProgressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#FFE200"), PorterDuff.Mode.MULTIPLY);
		mLayoutAVOffsetBoard = findViewById(R.id.av_offset_board);

        mChannelProgramInfo = (Spinner) findViewById(R.id.s_programInfo);
        programAdapter = new ProgramAdapter(MainActivity.this);
        mChannelProgramInfo.setAdapter(programAdapter);
        empty_program = (TextView)findViewById(R.id.empty_program);

        findViewById(R.id.ll_channel_info).setOnClickListener(this);
        findViewById(R.id.iv_up).setOnClickListener(this);
        findViewById(R.id.iv_down).setOnClickListener(this);
        ToggleButton vision = findViewById(R.id.vision);
        vision.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                if(mMainTVMediaPlayer != null) {
                    mMainTVMediaPlayer.setVisionImpaired(true);
                }
            }else {
                mMainTVMediaPlayer.setVisionImpaired(false);
            }
        });
        ToggleButton hearing = findViewById(R.id.hearing);
        hearing.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                if(mMainTVMediaPlayer != null) {
                    mMainTVMediaPlayer.setHearingImpaired(true);
                }
            }else {
                mMainTVMediaPlayer.setHearingImpaired(false);
            }
        });
        findViewById(R.id.scaling).setOnClickListener(this);

        ToggleButton multilang = findViewById(R.id.multilang);
        multilang.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                Log.d(TAG, "call setauditotrack(1)");
                if(mMainTVMediaPlayer != null) {
                    mMainTVMediaPlayer.setAudioTrack(1);
                }
            }else {
                Log.d(TAG, "call setauditotrack(0)");
                mMainTVMediaPlayer.setAudioTrack(0);
            }
        });

        mlrToogleBtn = findViewById(R.id.mlrenable);
        mlrToogleBtn.setEnabled(false);

        ToggleButton ttaTest = findViewById(R.id.tta_test);
        ttaTest.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                mLayoutTtaOptionBoard.setVisibility(View.VISIBLE);
            }else {
                mLayoutTtaOptionBoard.setVisibility(View.GONE);
            }
        });

        ToggleButton hearingOption = findViewById(R.id.tta_hearing_on_off);
        hearingOption.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked) {
                if(mMainTVMediaPlayer != null) {
                    mMainTVMediaPlayer.setTtaTestOption(PROPERTY_NAME_HEAR, "1");
                }
            } else {
                mMainTVMediaPlayer.setTtaTestOption(PROPERTY_NAME_HEAR, "0");
            }
        });

        ToggleButton visionOnOff = findViewById(R.id.tta_vision_on_off);
        visionOnOff.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked) {
                if(mMainTVMediaPlayer != null) {
                    mMainTVMediaPlayer.setTtaTestOption(PROPERTY_NAME_EYE, "1");
                    if(!setTVforTTA(mTtaTestCurrentChannelUrl)) {
                        current_index = sharedPreferences.getInt("index", 0);
                        setTV(current_index);
                    }
                }
            } else {
                mMainTVMediaPlayer.setTtaTestOption(PROPERTY_NAME_EYE, "0");
                if(!setTVforTTA(mTtaTestCurrentChannelUrl)) {
                    current_index = sharedPreferences.getInt("index", 0);
                    setTV(current_index);
                }
            }
        });

        RadioGroup rgHearing = findViewById(R.id.rg_hearing);
        rgHearing.setOnCheckedChangeListener((group, checkedId) -> {
            switch(checkedId) {
                case R.id.rb_caption1:
                    if(mMainTVMediaPlayer != null) {
                        mMainTVMediaPlayer.setTtaTestOption(PROPERTY_NAME_CAPTION, "1");
                    }
                    break;
                case R.id.rb_caption2:
                    if(mMainTVMediaPlayer != null) {
                        mMainTVMediaPlayer.setTtaTestOption(PROPERTY_NAME_CAPTION, "2");
                    }
                    break;
                case R.id.rb_caption3:
                    if(mMainTVMediaPlayer != null) {
                        mMainTVMediaPlayer.setTtaTestOption(PROPERTY_NAME_CAPTION, "3");
                    }
                    break;
                case R.id.rb_caption4:
                    if(mMainTVMediaPlayer != null) {
                        mMainTVMediaPlayer.setTtaTestOption(PROPERTY_NAME_CAPTION, "4");
                    }
                    break;
                case R.id.rb_caption5:
                    if(mMainTVMediaPlayer != null) {
                        mMainTVMediaPlayer.setTtaTestOption(PROPERTY_NAME_CAPTION, "5");
                    }
                    break;
                case R.id.rb_caption6:
                    if(mMainTVMediaPlayer != null) {
                        mMainTVMediaPlayer.setTtaTestOption(PROPERTY_NAME_CAPTION, "6");
                    }
                    break;
            }
        });

        RadioGroup rgVision = findViewById(R.id.rg_vision);
        rgVision.setOnCheckedChangeListener((group, checkedId) -> {
            switch(checkedId) {
                case R.id.rb_korean:
                    if(mMainTVMediaPlayer != null) {
                        mMainTVMediaPlayer.setTtaTestOption(PROPERTY_NAME_LANGUAGE, "0");
                        if(!setTVforTTA(mTtaTestCurrentChannelUrl)) {
                            current_index = sharedPreferences.getInt("index", 0);
                            setTV(current_index);
                        }
                    }
                    break;
                case R.id.rb_english:
                    if(mMainTVMediaPlayer != null) {
                        mMainTVMediaPlayer.setTtaTestOption(PROPERTY_NAME_LANGUAGE, "1");
                        if(!setTVforTTA(mTtaTestCurrentChannelUrl)) {
                            current_index = sharedPreferences.getInt("index", 0);
                            setTV(current_index);
                        }
                    }
                    break;
            }
        });

        findViewById(R.id.tta_channel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TTAChannelDialog builder = new TTAChannelDialog(MainActivity.this);
                builder.setCancelable(false);
                builder.setListener(MainActivity.this);
                builder.show();
            }
        });

        ToggleButton AVOffset = findViewById(R.id.AVOffset);
        AVOffset.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                mLayoutAVOffsetBoard.setVisibility(View.VISIBLE);
            }else {
                mLayoutAVOffsetBoard.setVisibility(View.GONE);
            }
        });

        findViewById(R.id.AVOffset1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mMainTVMediaPlayer != null) {
                    mMainTVMediaPlayer.setAVOffset(1, 5); //1:AVP_PES_AUDIO 5ms
                }
            }
        });

        findViewById(R.id.AVOffset2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mMainTVMediaPlayer != null) {
                    mMainTVMediaPlayer.setAVOffset(1, -5); //1:AVP_PES_AUDIO -5ms
                }
            }
        });

        ToggleButton logging = findViewById(R.id.log_enable);
        logging.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                if(mMainTVMediaPlayer != null) {
                    mMainTVMediaPlayer.setTtaTestOption(PROPERTY_NAME_PKT_LOG, "1");
                }
            }else {
                if(mMainTVMediaPlayer != null) {
	                mMainTVMediaPlayer.setTtaTestOption(PROPERTY_NAME_PKT_LOG, "0");
                }
            }
        });
    }


    @Override
    protected void onResume(){
        super.onResume();
        Log.d(TAG, "onResume()");
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                current_index = sharedPreferences.getInt("index", 0);
                setTV(current_index);

                ToggleButton visionOnOff = findViewById(R.id.tta_vision_on_off);
                visionOnOff.setChecked(false);

                RadioButton rbKorean = findViewById(R.id.rb_korean);
                rbKorean.setChecked(true);
            }
        }, 1000);
    }

    @Override
    protected void onStop(){
        super.onStop();
        Log.d(TAG, "onStop()");
        if (mMainTVMediaPlayer != null) {
            mMainTVMediaPlayer.release();
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() mMainTVMediaPlayer : "+mMainTVMediaPlayer);
        //if (mMainTVMediaPlayer != null) {
        //    mMainTVMediaPlayer.release();
        //}
    }

    @Override
    public void onConfigurationChanged(Configuration configuration){
        super.onConfigurationChanged(configuration);
        Log.d(TAG, "onConfigurationChanged()");
    }

    protected void onSaveIndex(int indeex) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("index", indeex);
        editor.commit();
    }

    public String convert(String value) {
        return value
                .replaceAll("&lt;", "<")
                .replaceAll("&gt;", ">")
                .replaceAll("&amp;", "&")
                .replaceAll("&quot;", "\"")
                .replaceAll("&apos;", "\'")
                .replaceAll("&nbsp;", " ");
    }

    @Override
    public void onChoice(String channelUrl) {
        setTVforTTA(channelUrl);
    }

    @Override
    public void onChannelChoice(AVDvbService channel) {
        int size = mChannelData.size();

        for(int i=0; i<size; i++) {
            if(mChannelData.get(i).getCh() == channel.getCh()) {
                setTV(i);
                break;
            }
        }
    }


    private boolean setTVforTTA(String url) {
        if(url == null || url.length() == 0) return false;

        if(mMainTVMediaPlayer != null) {
            Log.d(TAG, "setTVforTTA => " + url);
            String uri = url;
            mTtaTestCurrentChannelUrl = uri;

            int start = uri.indexOf("ch=");
            int end = uri.indexOf("&");
            String chNo = uri.substring(start+3, end);

            Log.d(TAG, "channel number : "+chNo);

            mTextView.setText(chNo);
            mChannelTitle.setText("");
            mMainTVMediaPlayer.tune(uri);
            try {
                new Thread(() -> mMainTVMediaPlayer.prepareAsync()).start();
            } catch(Exception e) {
                e.printStackTrace();
                return false;
            }

            return true;
        } else {
            return false;
        }
    }

    private String changeTTAVedioPid(String url, int vpid) {
        String uri = url;

        String vp = Integer.toString(vpid);

        int start = uri.indexOf("vc=");
        int end = uri.indexOf("&ap");
        String vc = uri.substring(start+3, end);

        start = uri.indexOf("ap=");
        end = uri.indexOf("&ac");
        String ap = uri.substring(start+3, end);

        start = uri.indexOf("ac=");
        end = uri.indexOf("&lf");
        String ac = uri.substring(start+3, end);

        String temp = "skbiptv://239.1.1.1:8208?ch=1000&ci=0&cp=0&pp=201&vp="+vp+"&vc="+vc+"&ap="+ap+"&ac="+ac+"&lf=1";

        return temp;
    }

    private String changeTTAAudioPid(String url, int apid) {
        String uri = url;

        String ap = Integer.toString(apid);

        int start = uri.indexOf("vc=");
        int end = uri.indexOf("&ap");
        String vc = uri.substring(start+3, end);

        start = uri.indexOf("vp=");
        end = uri.indexOf("&vc");
        String vp = uri.substring(start+3, end);

        start = uri.indexOf("ac=");
        end = uri.indexOf("&lf");
        String ac = uri.substring(start+3, end);

        String temp = "skbiptv://239.1.1.1:8208?ch=1000&ci=0&cp=0&pp=201&vp="+vp+"&vc="+vc+"&ap="+ap+"&ac="+ac+"&lf=1";

        return temp;
    }
}