package com.skb.framework.playerui.ttachannel;

/**
 * Created by parkjeongho on 2021-05-11 오후 5:29
 */
public interface ChannelChoiceListener {
    void onChoice(String channelUrl);
}
