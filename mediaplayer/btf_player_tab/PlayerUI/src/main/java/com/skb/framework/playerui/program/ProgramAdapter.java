package com.skb.framework.playerui.program;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.skb.btv.framework.navigator.program.AVProgram;
import com.skb.framework.playerui.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

//import android.sptek.stb.servicecore.si.program.ProgramInfo;

public class ProgramAdapter extends BaseAdapter {

    private static final String TAG =  "ProgramAdapter";

    private Context mContext = null;
    private List<AVProgram> mListData = null;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.KOREAN);


    public ProgramAdapter(Context mContext) {
        super();
        Log.d(TAG, "ProgramAdapter");
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        Log.d(TAG, "getCount");
        return mListData == null ? 0 : mListData.size();
    }

    @Override
    public Object getItem(int position) {
        Log.d(TAG, "getItem");
        return mListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        Log.d(TAG, "getItemId");
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d(TAG, "getView");

        SubViewHolder holder;

        if (convertView == null) {
            holder = new SubViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listitem_program, null);

            holder.mText = (TextView) convertView.findViewById(R.id.tv_title);
            holder.mImage = (ImageView) convertView.findViewById(R.id.iv_folder);

            convertView.setLayoutParams(new ListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 75));
            convertView.setTag(holder);

        } else {
            holder = (SubViewHolder) convertView.getTag();
        }

        Date starttime = mListData.get(position).getStartDate();
        Date endtime = mListData.get(position).getEndDate();
        String title = mListData.get(position).getName();
        String startdate = simpleDateFormat.format(starttime);
        String enddate = simpleDateFormat.format(endtime);
        String progInfo = title+"("+startdate+"~"+enddate + ")";// + mListData.get(position).getProgramName();

        // specific process
        holder.mText.setText(progInfo);

        return convertView;
    }

    public void remove(int position) {
        Log.d(TAG, "remove");
        mListData.remove(position);
    }

    public void setList(List<AVProgram> list) {
        this.mListData = list;
    }

    public List<AVProgram> getList() {
        return mListData;
    }

    public class SubViewHolder
    {
        public TextView mText;
        public ImageView mImage;
    }

}
