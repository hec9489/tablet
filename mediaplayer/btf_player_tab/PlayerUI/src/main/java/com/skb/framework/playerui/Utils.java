package com.skb.framework.playerui;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.util.TypedValue;

import com.skb.btv.framework.navigator.dvbservice.AVDvbService;
import com.skb.btv.framework.navigator.dvbservice.AVDvbServiceList;
import com.skb.btv.framework.navigator.dvbservice.MultiViewDvbService;
import com.skb.btv.framework.navigator.dvbservice.MultiViewDvbServiceList;
import com.skb.btv.framework.navigator.dvbservice.MusicDvbService;
import com.skb.btv.framework.navigator.dvbservice.MusicDvbServiceList;

import java.util.ArrayList;

/**
 * Created by parkjeongho on 2021-05-20 오후 8:18
 */
public class Utils {
    private static final String TAG =  "NavigatorUI-Utils";

    public static int getDpToPixel(Context context, int DP) {
        Log.d(TAG, "getDpToPixel");
        float px = 0;
        try {
            px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DP,
                    context.getResources().getDisplayMetrics());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (int) px;
    }

    public static String getAppVersionName(Context context){
        Log.d(TAG, "getAppVersionName");
        String version = "";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }


    public static ArrayList<AVDvbService> channelParser(Context context, AVDvbServiceList result) {
        Log.d(TAG, "channelParser");
        return new ArrayList<>(result.getList());
    }

    /**
     * Start Add dvbsi scenario - KGH
     */

    public static ArrayList<MusicDvbService> musicChannelParser(Context context, MusicDvbServiceList data) {
        Log.d(TAG, "musicChannelParser");
        return new ArrayList<>(data.getList());
    }

    public static ArrayList<MultiViewDvbService> multiViewChannelParser(Context context, MultiViewDvbServiceList data){
        Log.d(TAG, "multiViewChannelParser");
        return new ArrayList<>(data.getList());
    }
}
