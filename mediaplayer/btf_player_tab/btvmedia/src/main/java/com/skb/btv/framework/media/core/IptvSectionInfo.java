
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.media.core;

/**
 * IptvSectionInfo class can be used to IptvSection information
 *
 * @author Kwon Soon Chan (sysc0507@sk.com)
 */

class IptvSectionInfo
{
 // IptvSectionInfo
    public final static int     IPTV_SECTION_EVENT_DATA_IN_ORDER	= 0;
    public final static int     IPTV_SECTION_EVENT_CRC_FAIL			= 1;
    public final static int     IPTV_SECTION_EVENT_DESCRAMBLE_FAIL	= 2;
    public final static int     IPTV_SECTION_EVENT_TIMEOUT			= 3;
    public final static int     IPTV_SECTION_EVENT_UNKNOWN_FAIL		= 4;
    
    public int   mIndex = 0;
    public int	 mPID = 0;
    public int	 mTableID = 0;
    public int   mTimeOut = 0;

    public void SetIndex(int nIndex)
    {
    	mIndex = nIndex;
    }

    public int GetIndex()
    {
        return mIndex;
    }

	public void SetPID(int nPID)
    {
		mPID = nPID;
    }

    public int GetPID()
    {
        return mPID;
    }

	public void SetTableID(int nTableID)
    {
		mTableID = nTableID;
    }

    public int GetTableID()
    {
        return mTableID;
    }

    public void SetTimeOut(int nTimeOut)
    {
    	mTimeOut = nTimeOut;
    }

    public int GetTimeOut()
    {
        return mTimeOut;
    }
}
    
