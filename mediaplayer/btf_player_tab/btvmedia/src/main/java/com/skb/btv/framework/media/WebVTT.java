
// Copyright 2019 SK Broadband Co., LTD.


package com.skb.btv.framework.media;

/**
 * WebVTT class can be used to display WebVTT caption data
 *
 * @author Kwon Soon Chan (sysc0507@sk.com)
 */
public class WebVTT {
    public long startTime;
    public long endTime;
    public CharSequence text;

    /**
     * WebVTT Default constructor.
     *
     * @param startTime WebVTT start time
     * @param endTime WebVTT end time
     * @param text WebVTT text
     */
    public WebVTT(long startTime, long endTime, CharSequence text) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.text = text;
    }
    /**
     * Gets WebVTT start time.
     *
     * @return startTime
     */
    public long getStartTime() {
        return this.startTime;
    }

    /**
     * Gets WebVTT end time.
     *
     * @return endTime
     */
    public long getEndTime() {
        return this.endTime;
    }

    /**
     * Gets WebVTT data text.
     *
     * @return WebVTT text
     */
    public CharSequence getText() {
        return this.text;
    }
}
