
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.media.core;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.RemoteException;
import android.text.TextUtils;

import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.TextureView;

import com.skb.btv.framework.media.DmxFilter;
import com.skb.btv.framework.media.WebVTT;


import sptek.sptek_media_player.HLSServiceInterface;
import sptek.sptek_media_player.IOnCompletionCallback;
import sptek.sptek_media_player.IOnErrorCallback;
import sptek.sptek_media_player.IOnInfoCallback;
import sptek.sptek_media_player.IOnRenderedFirstFrameCallback;
import sptek.sptek_media_player.IOnSeekProcessedCallback;
import sptek.sptek_media_player.IWebVTTCallback;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * MediaPlayer class can be used to control playback For Btv Media Service
 *
 * @author Kwon Soon Chan (sysc0507@sk.com)
 */

public class MediaPlayer {

    public class BUILD {
        /**
         * MediaPlayer API LEVEL
         */
        public static final int API_LEVEL         = 2;
        /**
         * MediaPlayer VERSION MAJOR
         */
        public static final int VERSION_MAJOR     = 2;
        /**
         * MediaPlayer VERSION MINOR
         */
        public static final int VERSION_MINOR     = 0;
        /**
         * MediaPlayer VERSION BUILD
         */
        public static final int VERSION_BUILD     = 1;
        /**
         * MediaPlayer CODE_NAME
         */
        public static final String CODE_NAME      = VERSION_MAJOR + "." + VERSION_MINOR + "." + VERSION_BUILD;
    }

    private static final String TAG =  "BTVMEDIA_MediaPlayer_" + BUILD.CODE_NAME;
    protected static final String TAG_IPTV = "BTVMEDIA_IPTV";
    protected static final String TAG_IPTV_HLS = "BTVMEDIA_IPTV_HLS";
    protected static final String TAG_BTVMEDIA_MULTIVIEW_IPTV = "BTVMEDIA_MutiView_IPTV";
    protected static final String TAG_BTVMEDIA_IPTV_CALLBACK = "BTVMEDIA_IPTV_CALLBACK";

    private final static String STB_MEDIA_PLAYER_VER = TAG;

    private final static String STB_ON_SLEEP = "STB_ON_SLEEP";
    private final static String STB_ON_WAKE_UP = "STB_ON_WAKE_UP";
    private final static String STB_AUDIO_EVENT_CONTROL = "com.skbb.intent.action.AUDIO_EXTERNAL_EVENT_CONTROL";
    private final static String SYSPROP_SKB_AUDIO_PATH_THROUGH = "sys.stb.audio.paththrough";
    private final static String SYSPROP_EDID_AC3_PASSTHROUGH = "vendor.edid.ac3_passthrough";
    
    // this property to check whether support vendor trickmode or not
    // if set vendor support iframe  trick play without audio in tunnelmode.
    private final static String HLS_VENDOR_TRICKMODE = "ro.vendor.trickmode";

    private final static Boolean HLS_TUNNEL = true;

    private static final String BUILD_DATE =  "2021.01.07";

    /**
     * MediaPlayer Vod Trick 1X FF
     */
    public final static int     IPTV_VOD_TRICK_1XFF		= 0;
    /**
     * MediaPlayer Vod Trick 0.8X FF
     */
    public final static int     IPTV_VOD_TRICK_0_8XFF		= 1;
    /**
     * MediaPlayer Vod Trick 1.2X FF
     */
    public final static int     IPTV_VOD_TRICK_1_2XFF		= 2;
    /**
     * MediaPlayer Vod Trick 1.5X FF
     */
    public final static int     IPTV_VOD_TRICK_1_5XFF		= 3;
    /**
     * MediaPlayer Vod Trick 2X FF
     */
    public final static int     IPTV_VOD_TRICK_2XFF		= 4;
    /**
     * MediaPlayer Vod Trick 4X FF
     */
    public final static int     IPTV_VOD_TRICK_4XFF		= 5;
    /**
     * MediaPlayer Vod Trick 8X FF
     */
    public final static int     IPTV_VOD_TRICK_8XFF		= 6;
    /**
     * MediaPlayer Vod Trick 16X FF
     */
    public final static int     IPTV_VOD_TRICK_16XFF	= 7;
    /**
     * MediaPlayer Vod Trick 2X BW
     */
    public final static int     IPTV_VOD_TRICK_2XBW		= 8;
    /**
     * MediaPlayer Vod Trick 4X BW
     */
    public final static int     IPTV_VOD_TRICK_4XBW		= 9;
    /**
     * MediaPlayer Vod Trick 8X BW
     */
    public final static int     IPTV_VOD_TRICK_8XBW		= 10;
    /**
     * MediaPlayer Vod Trick 16X BW
     */
    public final static int     IPTV_VOD_TRICK_16XBW	= 11;

    /**
     * MediaPlayer IDLE Status
     */
    public final static int     IPTV_STATUS_IDLE		        = 0;
    /**
     * MediaPlayer INITIALIZED Status
     */
    public final static int     IPTV_STATUS_INITIALIZED	        = 1;
    /**
     * MediaPlayer PREPARED Status
     */
    public final static int     IPTV_STATUS_PREPARED	        = 2;
    /**
     * MediaPlayer STARTED Status
     */
    public final static int     IPTV_STATUS_STARTED		        = 3;
    /**
     * MediaPlayer PAUSEED Status
     */
    public final static int     IPTV_STATUS_PAUSEED		        = 4;
    /**
     * MediaPlayer STOPED Status
     */
    public final static int     IPTV_STATUS_STOPED		        = 5;
    /**
     * MediaPlayer PLAYBACKCOMPLETED Status
     */
    public final static int     IPTV_STATUS_PLAYBACKCOMPLETED	= 6;

    /**
     * Interface definition for a callback to be invoked when the media
     * source is ready for playback.
     */
    public interface OnPreparedListener {
        /**
         * Called when the media file is ready for playback.
         *
         * @param mp the MediaPlayer that is ready for playback
         */
        void onPrepared(MediaPlayer mp);
    }

    /** Informs btv player related.
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnInfoListener
     */
    public static final int MEDIA_INFO_WHAT_BTV_PLAYER = 10000;

    /** Informs SERVER IP When playing VOD.
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnInfoListener
     */
    public static final int MEDIA_INFO_WHAT_BTV_VOD_SERVER_IP = 10001;

    /** Informs SERVER PORT When playing VOD.
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnInfoListener
     */
    public static final int MEDIA_INFO_WHAT_BTV_VOD_SERVER_PORT = 10002;

    /** Informs what Decoding error occurs when playing BTV player.
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnInfoListener
     */
    public static final int MEDIA_INFO_WHAT_PLAY_DECODE_ERROR = 1004;

    /** informs that the live-stream reception status is good.
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnInfoListener
     */
    public static final int MEDIA_INFO_EXTRA_PLAY_GOOD = 1000;

    /** informs that the live-stream reception status is bad.
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnInfoListener
     */
    public static final int MEDIA_INFO_EXTRA_PLAY_BAD_RECEIVING = 1001;

    /** informs the begin of stream when current position is zero during the rewind operation.
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnInfoListener
     */
    public static final int MEDIA_INFO_EXTRA_BEGIND_OF_STREAM = 20001;

    /** informs the end of stream.
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnInfoListener
     */
    public static final int MEDIA_INFO_EXTRA_END_OF_STREAM = 20006;

    /** nofify when first I frame is showing.
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnInfoListener
     */
    public static final int MEDIA_INFO_EXTRA_SHOW_OF_FIRST_I_FRAME = 20008;

    /**
     * Interface definition of a callback to be invoked to communicate some
     * info and/or warning about the media or its playback.
     */
    public interface OnInfoListener {
        /**
         * Called to indicate an info or a warning.
         *
         * @param mp the MediaPlayer the info pertains to.
         * @param what the type of info or warning.
         * <ul>
         * <li>{@link #MEDIA_INFO_WHAT_BTV_PLAYER}
         * <li>{@link #MEDIA_INFO_WHAT_BTV_VOD_SERVER_IP}
         * <li>{@link #MEDIA_INFO_WHAT_BTV_VOD_SERVER_PORT}
         * <li>{@link #MEDIA_INFO_WHAT_PLAY_DECODE_ERROR}
         * </ul>
         * @param extra an extra code, specific to the info.
         */
        boolean onInfo(MediaPlayer mp, int what, int extra);
    }


    /** btv player related errors.
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnErrorListener
     */
    public static final int MEDIA_ERROR_WHAT_BTV_PLAYER = 10000;

    /** Media server died. In this case, the application must release the
     * MediaPlayer object and instantiate a new one.
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnErrorListener
     */
    public static final int MEDIA_ERROR_WHAT_SERVER_DIED = 100;

    /** unsupported media data source.
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnErrorListener
     */
    public static final int MEDIA_ERROR_WHAT_UNSUPPORTED = 0;

    /** Extractor internal error.
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnErrorListener
     */
    public static final int MEDIA_ERROR_EXTRA_EXTRACTOR_INTERNAL_ERROR = 4001;

    /** Extractor IO error or network error
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnErrorListener
     */
    public static final int MEDIA_ERROR_EXTRA_EXTRACTOR_IO_ERROR = 4002;

    /** contents end of stream.
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnErrorListener
     */
    public static final int MEDIA_ERROR_EXTRA_CONTENT_EOS = 4003;

    /** contents url error
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnErrorListener
     */
    public static final int MEDIA_ERROR_EXTRA_CONTENT_URL_ERROR = 4004;

    /** http server error
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnErrorListener
     */
    public static final int MEDIA_ERROR_EXTRA_HTTP_SERVER_ERROR = 4005;

    /** Internal Error of MP4 Contents
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnErrorListener
     */
    public static final int MEDIA_ERROR_EXTRA_MP4_CONTENTS_ERROR = 4006;

    /** Internal Error of TS Contents
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnErrorListener
     */
    public static final int MEDIA_ERROR_EXTRA_TS_CONTENTS_ERROR = 4007;

    /** Internal Error of WebVTT Contents
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnErrorListener
     */
    public static final int MEDIA_ERROR_EXTRA_WEBVTT_CONTENTS_ERROR = 4008;

    /** HLS/Dash error
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnErrorListener
     */
    public static final int MEDIA_ERROR_EXTRA_HLS_DASH_ERROR = 4009;

    /** DRM client error
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnErrorListener
     */
    public static final int MEDIA_ERROR_EXTRA_DRM_CLIENT_ERROR = 4010;

    /** Playback URL expiration error
     * @see com.skb.btv.framework.media.core.MediaPlayer.OnErrorListener
     */
    public static final int MEDIA_INFO_EXTRA_PLAYBACK_URL_EXPIRATION_ERROR = 4011;

    /** AV offset control
    * @type AV_OFFSET_TYPE
     */
    public final static int     IPTV_AV_OFFSET_TYPE_VIDEO	= 0;

    /** AV offset control
     * @type AV_OFFSET_TYPE
     */
    public final static int     IPTV_AV_OFFSET_TYPE_AUDIO	= 1;

    /** AV offset control
     * @type AV_OFFSET_TYPE
     */
    public final static int     IPTV_AV_OFFSET_TYPE_PCR	    = 2;



    /**
     * FIXME : Temporary code for pass through setup on HLS player
     * add SPTEK : 2020.12.23 : S ++
     */
    public static final int ENCODED_SURROUND_OUTPUT_AUTO                = 0;
    public static final String ENCODED_SURROUND_OUTPUT                  = "encoded_surround_output";
    private static final String INTENT_HLS_PLAY_STATE_START                  = "com.skb.btv.intent.action.hls_start";
    private static final String INTENT_HLS_PLAY_STATE_STOP                  = "com.skb.btv.intent.action.hls_stop";
    public static boolean mIsMainHLS                                    = false;

    /**
     * Interface definition of a callback to be invoked when there
     * has been an error during an asynchronous operation (other errors
     * will throw exceptions at method call time).
     */
    public interface OnErrorListener {
        /**
         * Called to indicate an error.
         *
         * @param mp the MediaPlayer the error pertains to
         * @param what the type of error that has occurred
         * <ul>
         * <li>{@link #MEDIA_ERROR_WHAT_BTV_PLAYER}
         * <li>{@link #MEDIA_ERROR_WHAT_SERVER_DIED}
         * <li>{@link #MEDIA_ERROR_WHAT_UNSUPPORTED}
         * </ul>
         * @param extra an extra code, specific to the error.
         *
         */
        boolean onError(MediaPlayer mp, int what, int extra);
    }

    /**
     * Interface definition for a callback to be invoked when playback of
     * a media source has completed.
     */
    public interface OnCompletionListener {
        /**
         * Called when the end of a media source is reached during playback.
         *
         * @param mp the MediaPlayer that reached the end of the file
         */
        void onCompletion(MediaPlayer mp);
    }

    /**
     * Interface definition of a callback to be invoked indicating
     * the completion of a seek operation.
     */
    public interface OnSeekCompletionListener {
        /**
         * Called to indicate the completion of a seek operation.
         *
         * @param mp the MediaPlayer that issued the seek operation
         */
        void onSeekCompletion(MediaPlayer mp);
    }

    /**
     * Interface definition of a callback to be invoked to received
     * WebVTT data updated event.
     */
    public interface OnWebVTTUpdateListener {
        /**
         * Called to indicate the receiving of a WebVTT updated event.
         *
         * @param mp the MediaPlayer the WebVTT updated event pertains to
         * @param webVTTList  {@link WebVTT} data list
         *
         * @see com.skb.btv.framework.media.WebVTT
         */
        void onWebVTTUpdated(MediaPlayer mp, List<WebVTT> webVTTList);
    }

    /**
     * Interface definition of a callback to be invoked when receiving of
     * a demux filter data given tid, pid
     */
    public interface OnDmxFilterListener  {
        /**
         * Called to indicate the receiving of a filter operation.
         *
         * @param mp the MediaPlayer that issued the filter operation
         * @param sectiondata a filter data
         */
        void onDmxFilter(MediaPlayer mp,  byte[] sectiondata);
    }

    /**
     * Class for MediaPlayer to return each audio/video/subtitle track's metadata.
     *
     * @see com.skb.btv.framework.media.core.MediaPlayer#getTrackInfo
     */
    public class TrackInfo {
        public static final int MEDIA_TRACK_TYPE_UNKNOWN = 0;
        public static final int MEDIA_TRACK_TYPE_VIDEO = 1;
        public static final int MEDIA_TRACK_TYPE_AUDIO = 2;
        public static final int MEDIA_TRACK_TYPE_TIMEDTEXT = 3;
        public static final int MEDIA_TRACK_TYPE_SUBTITLE = 4;
        public static final int MEDIA_TRACK_TYPE_METADATA = 5;

        int trackType;

        public int getTrackType() {
            return trackType;
        }

        TrackInfo() {
            trackType = MEDIA_TRACK_TYPE_AUDIO;
        }
    }

    /**
     * Class for MediaPlayer to return each audio track's metadata.
     *
     * @see com.skb.btv.framework.media.core.MediaPlayer#getAudioTrack
     */
    public class AudioTrack {
        int pid;
        int codec;

        public int getAudioPid() {
            return pid;
        }

        public int getAudioCodec() {
            return codec;
        }

        AudioTrack(int pid, int codec) {
            this.pid = pid;
            this.codec = codec;
        }
    }

    /**
     *  Trick Speed that can be used
     *  <p>
     *  <li>{@link #BX160}</li>
     *  <li>{@link #BX080}</li>
     *  <li>{@link #BX040}</li>
     *  <li>{@link #BX020}</li>
     *  <li>{@link #X010}</li>
     *  <li>{@link #X008}</li>
     *  <li>{@link #X012}</li>
     *  <li>{@link #X015}</li>
     *  <li>{@link #X020}</li>
     *  <li>{@link #X040}</li>
     *  <li>{@link #X080}</li>
     *  <li>{@link #X160}</li>
     */
    public enum TRICK_SPEED {
        BX160(IPTV_VOD_TRICK_16XBW, -16.0),
        BX080(IPTV_VOD_TRICK_8XBW, -8.0),
        BX040(IPTV_VOD_TRICK_4XBW, -4.0),
        BX020(IPTV_VOD_TRICK_2XBW, -2.0),
        X010(IPTV_VOD_TRICK_1XFF, 1.0),
        X008(IPTV_VOD_TRICK_0_8XFF, 0.8),
        X012(IPTV_VOD_TRICK_1_2XFF, 1.2),
        X015(IPTV_VOD_TRICK_1_5XFF, 1.5),
        X020(IPTV_VOD_TRICK_2XFF, 2.0),
        X040(IPTV_VOD_TRICK_4XFF, 4.0),
        X080(IPTV_VOD_TRICK_8XFF, 8.0),
        X160(IPTV_VOD_TRICK_16XFF, 16.0),;

        int speedId;
        double speedRatio;

        TRICK_SPEED(int i, double ratio) {
            speedId = i;
            speedRatio = ratio;
        }

        public int getSpeedId() {
            return speedId;
        }

        public TRICK_SPEED next() {
            return values()[Math.min(this.ordinal() + 1, values().length - 1)];
        }

        public TRICK_SPEED prev() {
            return values()[Math.max(this.ordinal() - 1, 0)];
        }

        public double getSpeedRatio() {
            return speedRatio;
        }
    }

    private static HLSServiceInterface mHLSPlayer = null;
    private static boolean mHLSPlayerPassthrough = false;
    private static boolean hlsPlayer_init = false;
    private static boolean mForceQuit = false;
    private IptvMediaPlayer legacyPlayer;

    private MediaPlayer.OnPreparedListener onPreparedListener;
    private MediaPlayer.OnInfoListener onInfoListener;
    private MediaPlayer.OnErrorListener onErrorListener;
    private MediaPlayer.OnCompletionListener onCompletionListener;
    private MediaPlayer.OnSeekCompletionListener onSeekCompletionListener;
    private MediaPlayer.OnWebVTTUpdateListener onWebVTTUpdateListener;
    private MediaPlayer.OnDmxFilterListener  onDmxFilterListener;

    private IptvMediaPlayer.OnDisconnectedListener onIptvDisconnectedListener;
    private IptvMediaPlayer.OnPlayStatusListener onIptvPlayStatusListener;
    private IptvMediaPlayer.OnSectionFilterListener onIptvSectionFilterListener;
    private IptvMediaPlayer.OnPidChangedListener onIptvPidChangedListener;
    private IptvMediaPlayer.OnSeekCompletionListener onIptvSeekCompletionListener;
    private IptvMediaPlayer.OnTuneEventListener onIptvTuneEventListener;

    private SurfaceHolder surfaceHolder;
//    private SurfaceHolder ccSurfaceHolder;
    private TextureView ccTextureView;

    private IptvPlayInfo playInfo;
    private IptvTuneInfo tuneInfo;
    private ArrayList<IptvInfo> mvTuneInfo;
    private int mvMode = 0;
    private int playerId;

    private List<WebVTT> mWebVTTInfoList = new ArrayList();
    private boolean mLastMVAudioOnOff= false;
    private IptvSectionInfo sectionInfo;
    private boolean isPlaying = false, isPaused = false;
    private Handler mHandler;
    private long duration;
    private long currntTime;
    private boolean bShowFirstFrame;
    private long retryTimeOffset;
    private Context mContext;
    private BroadcastReceiver mControlBroadcastReceiver;
    private int mPlayerStatus;

    /**
     * MediaPlayer Default constructor.
     * <p>
     * Same as {@link #MediaPlayer(Context, int)} with {@code pipId = 0}.
     *
     * @param context the Context to use
     *
     * <p>When done with the MediaPlayer, you should call  {@link #release()},
     * to free the resources. If not released, too many MediaPlayer instances may
     * result in an exception.</p>
     */
    public MediaPlayer(Context context) {
        mContext = context;
        legacyPlayer = IptvMediaPlayer.connect(IptvMediaPlayer.IPTV_DEVICE_MAIN, context);
        mForceQuit = false;
        bindHLSPlayerConnection();
        playerId = IptvMediaPlayer.IPTV_DEVICE_MAIN;
        initialize();
    }

    /**
     * MediaPlayer constructor for a given pip id player.
     *
     * @param context the Context to use
     * @param pipId indicate main(0) player or pip(1) player
     *
     * <p>When done with the MediaPlayer, you should call  {@link #release()},
     * to free the resources. If not released, too many MediaPlayer instances may
     * result in an exception.</p>
     */
    public MediaPlayer(Context context, int pipId) {
        mContext = context;
        legacyPlayer = IptvMediaPlayer.connect(pipId, context);
        mForceQuit = false;
        bindHLSPlayerConnection();
        playerId = pipId;
        initialize();
    }

    /**
     * Register a callback to be invoked when the media source is ready
     * for playback.
     *
     * @param listener the callback that will be run
     */
    public void setOnPreparedListener(MediaPlayer.OnPreparedListener listener) {
        onPreparedListener = listener;
        SLog.d(TAG_IPTV,  "IN|listener = " + listener + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
    }


    /**
     * Register a callback to be invoked when an info/warning is available.
     *
     * @param listener the callback that will be run
     */
    public void setOnInfoListener(MediaPlayer.OnInfoListener listener) {
        onInfoListener = listener;
        SLog.d(TAG_IPTV,  "IN|listener = " + listener + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        if(listener != null) {
            legacyPlayer.setOnTuneEventListener(onIptvTuneEventListener);
            legacyPlayer.setOnDisconnectedListener(onIptvDisconnectedListener);
            legacyPlayer.setOnPlayStatusListener(onIptvPlayStatusListener);
            legacyPlayer.setOnPidChangedListener(onIptvPidChangedListener);
            legacyPlayer.setOnSectionFilterListener(onIptvSectionFilterListener);
        }else {
            legacyPlayer.setOnTuneEventListener(null);
            legacyPlayer.setOnDisconnectedListener(null);
            legacyPlayer.setOnPlayStatusListener(null);
            legacyPlayer.setOnPidChangedListener(null);
            legacyPlayer.setOnSectionFilterListener(null);
        }
    }

    /**
     * Register a callback to be invoked when an error has happened
     * during an asynchronous operation.
     *
     * @param listener the callback that will be run
     */
    public void setOnErrorListener(MediaPlayer.OnErrorListener listener) {
        onErrorListener = listener;
        SLog.d(TAG_IPTV,  "IN|listener = " + listener + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
    }

    /**
     * Register a callback to be invoked when the end of a media source
     * has been reached during playback.
     *
     * @param listener the callback that will be run
     */
    public void setOnCompletionListener(MediaPlayer.OnCompletionListener listener) {
        onCompletionListener = listener;
        SLog.d(TAG_IPTV,  "IN|listener = " + listener + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
    }

    /**
     * Register a callback to be invoked when a seek operation has been
     * completed.
     *
     * @param listener the callback that will be run
     */
    protected void setOnSeekCompletionListener(MediaPlayer.OnSeekCompletionListener listener) {
        onSeekCompletionListener = listener;
        SLog.d(TAG_IPTV,  "IN|listener = " + listener + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        if(listener != null) {
            legacyPlayer.setOnSeekCompletionListener(onIptvSeekCompletionListener);
        }else {
            legacyPlayer.setOnSeekCompletionListener(null);
        }
    }

    /**
     * Register a callback to be invoked when WebVTT Updated.
     *
     * @param listener the callback that will be run
     */
    protected void setOnWebVTTUpdateListener(MediaPlayer.OnWebVTTUpdateListener listener) {
        onWebVTTUpdateListener = listener;
        SLog.d(TAG_IPTV,  "IN|listener = " + listener + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
    }

    /**
     * Register a callback to be invoked when demux filter data received
     *
     * @param listener the callback that will be run
     */
    protected void setDmxFilterListener(MediaPlayer.OnDmxFilterListener  listener) {
        onDmxFilterListener = listener;
        SLog.d(TAG_IPTV,  "IN|listener = " + listener + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
    }


    /**
     * Sets the {@link SurfaceHolder} to use for displaying the video
     * portion of the media.
     *
     * @param sh the SurfaceHolder to use for video display
     */
    public void setDisplay(SurfaceHolder sh) {
        surfaceHolder = sh;
        if(sh == null) {
            SLog.d(TAG_IPTV,  "IN|SurfaceHolder = null" + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        }else {
            SLog.d(TAG_IPTV,  "IN|SurfaceHolder = " + sh + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
            if(playerId != IptvMediaPlayer.IPTV_DEVICE_MAIN){
                try {
                    Thread.sleep(5);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
            SLog.d(TAG_IPTV,  "setIptvDisplay = " + surfaceHolder + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
            legacyPlayer.setIptvDisplay(surfaceHolder);
        }
    }

    /**
     * Sets the {@link TextureView} to use for displaying closed caption.
     *
     * @param textureView the TextureView to use for closed caption
     */
    public void setCCDisplay(TextureView textureView) {
        SLog.d(TAG_IPTV,  "IN|textureView = " + textureView + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        ccTextureView = textureView;
        legacyPlayer.setCCDisplay(ccTextureView);
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
    }

    /**
     * Sets the data source as a content Uri.
     *
     * <p>iptv play uri started with skbiptv://..
     * <p>rtsp vod play uri started with skbvod://..
     * <p>file play uri started with skbfile://..
     * <p>multi-view play uri started with skbsmv://..
     * <p>hls play uri started with skbhttps://..
     *
     * @param strUri the Content URI of the data you want to play
     */
    public void setDataSource(String strUri) {
        SLog.d(TAG_IPTV,  "IN|strUri = " + strUri + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            Uri uri = Uri.parse(strUri);
            String scheme = uri.getScheme();

            if (scheme.equalsIgnoreCase("skbiptv")) {         //IPTV
                if (tuneInfo != null) {
                    tuneInfo = null;
                }
                reset();
                tuneInfo = getTuneInfo(uri);
            } else if (scheme.equalsIgnoreCase("skbvod")) {   //VOD
                reset();
                playInfo = getVodPlayInfo(uri);
            } else if (scheme.equalsIgnoreCase("skbfile")) {   //FILE
                reset();
                playInfo = getFilePlayInfo(uri);
            } else if (scheme.equalsIgnoreCase("skbsmv")) {   //멀티 뷰
                if (mvTuneInfo != null) {
                    mvTuneInfo = null;
                }
                reset();
                mvTuneInfo = getMVTuneInfo(uri);
            } else if ((scheme.equalsIgnoreCase("skbhttp") || scheme.equalsIgnoreCase("skbhttps"))) {
                SLog.d(TAG_IPTV_HLS,"setDataSource HLS ");
                reset();
                playInfo = getHLSPlayInfo(uri);
                if(strUri.contains("playtype=main")){
                    mIsMainHLS = true;
                } else if (strUri.contains("playtype=promotion") || strUri.contains("playtype=preview_in_synopsis") || strUri.contains("playtype=preview_vod")){
                    mIsMainHLS = false;
                } else {
                    SLog.w(TAG_IPTV_HLS,"setDataSource not found playtype ");
                    mIsMainHLS = true;
                }

                SLog.d(TAG_IPTV_HLS,"GetContentsURL: " + playInfo.GetContentsURL() + " mIsMainHLS : " + mIsMainHLS);

                // 2020.12.30 : + S
                /* TODO YIWOO Check
                if(mIsMainHLS == true ) {
                    String path_through = SystemProperties.get(SYSPROP_SKB_AUDIO_PATH_THROUGH); // 0 : External, 1: B tv
                    if(!path_through.equals("0")) {
                        SLog.d(TAG_IPTV_HLS, "Set force auto mode");
                        Settings.Global.putInt(mContext.getContentResolver(), ENCODED_SURROUND_OUTPUT, ENCODED_SURROUND_OUTPUT_AUTO);
                    }

                    mContext.sendBroadcast(new Intent(INTENT_HLS_PLAY_STATE_START));
                    // <<--- FIXME : Temporary code for pass through setup on HLS player

                    if(!path_through.equals("0")){

                        int sleepTime = 1000;

                        SLog.d(TAG_IPTV_HLS,"HLS path_through : " + path_through + " sleepTime : " + sleepTime);
                        try {
                            Thread.sleep(sleepTime);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                 */
                // 2020.12.30 : + E

                hls_player_initialize();
                if(!TextUtils.isEmpty(playInfo.GetLicenseURL())){
                    SLog.d(TAG_IPTV_HLS,"setDrmLicenseUrl: " + playInfo.GetLicenseURL());
                    mHLSPlayer.setDataSource(playInfo.GetContentsURL(), playInfo.GetLicenseURL());
                }
            } else {
                if (onErrorListener != null)
                    onErrorListener.onError(this, MEDIA_ERROR_WHAT_UNSUPPORTED, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (onErrorListener != null)
                onErrorListener.onError(this, MEDIA_ERROR_WHAT_UNSUPPORTED, 0);
        }
        mPlayerStatus = IPTV_STATUS_INITIALIZED;
        bShowFirstFrame = false;
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
    }

    /**
     * Prepares the player for playback, synchronously.
     * <p>
     * After setting the datasource and the display surface, you need to either
     * call prepare() or prepareAsync(). For files, it is OK to call prepare(),
     * which blocks until MediaPlayer is ready for playback.
     *
     */
    public void prepare() {
        SLog.d(TAG_IPTV,  "IN|" + "nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            if (playInfo != null) {
                //SLog.d(TAG_IPTV, "prepare " + playInfo.getUri());
                if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                    try {
                        mHLSPlayer.prepare();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    legacyPlayer.open(playInfo);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        mPlayerStatus = IPTV_STATUS_PREPARED;
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
    }

    /**
     * Prepares the player for playback, asynchronously.
     * <p>
     * After setting the datasource and the display surface, you need to either
     * call prepare() or prepareAsync(). For streams, you should call prepareAsync(),
     * which returns immediately, rather than blocking until enough data has been
     * buffered.
     *
     */
    public void prepareAsync() {
        SLog.d(TAG_IPTV,  "IN|player = " + MediaPlayer.this);
        Thread prepareThread = new Thread(new Runnable(){
            @Override
            public void run() {
                if (onPreparedListener != null) {
                    try {
                        if (playInfo != null) {
                            SLog.d(TAG_IPTV, "IN|" + "prepareAsync " + playInfo.getUri() + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
                            if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS) {
                                try {
                                    mHLSPlayer.prepare();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                legacyPlayer.open(playInfo);
                            }
                        }

                        mPlayerStatus = IPTV_STATUS_PREPARED;
                        SLog.d(TAG_IPTV,  "onPrepared " + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
                        onPreparedListener.onPrepared(MediaPlayer.this);
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        prepareThread.setPriority(Thread.MAX_PRIORITY);
        prepareThread.start();
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
    }

    /**
     * Starts or resumes playback.
     * <p>
     * If playback had previously been paused,
     * playback will continue from where it was paused. If playback had
     * been stopped, or never started before, playback will start at the
     * beginning.
     *
     */
    public void start() {
        SLog.d(TAG_IPTV,  "IN|start" + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);

        duration = 0;
        currntTime = 0;
        retryTimeOffset = System.currentTimeMillis();

        try {
            if (tuneInfo != null) {
                SLog.d(TAG_IPTV, "Request tuneTV");
                legacyPlayer.tuneTV(tuneInfo, playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? IptvMediaPlayer.IPTV_AUDIO_ON : IptvMediaPlayer.IPTV_AUDIO_OFF);
            } else if (mvTuneInfo != null) {
                SLog.d(TAG_IPTV, "Request MultiView tuneTV");
                legacyPlayer.tuneMultiViewTV(mvTuneInfo, mLastMVAudioOnOff ? IptvMediaPlayer.IPTV_AUDIO_ON : IptvMediaPlayer.IPTV_AUDIO_OFF, mvMode);
            } else if (playInfo != null) {
                if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                    if (!isPaused){
                        SLog.d(TAG_IPTV, "Request HLS Play");
                        try {
                            mHLSPlayer.start(playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN? true:false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        SLog.d(TAG_IPTV, "Request HLS resume");
                        try {
                            mHLSPlayer.resume();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }else{
                    if (!isPaused) {
                        SLog.d(TAG_IPTV, "Request play");
                        if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_VOD) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    InetAddress address = null;
                                    try {
                                        if (playInfo != null) {
                                            address = InetAddress.getByName(new URL("http://" + playInfo.getUri().getHost()).getHost());
                                            final byte[] ipAddress = address.getAddress();
                                            mHandler.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    int result = 0;
                                                    for (byte b : ipAddress) {
                                                        result = result << 8 | (b & 0xFF);
                                                    }
                                                    if (onInfoListener != null && playInfo != null) {
                                                        try {
                                                            onInfoListener.onInfo(MediaPlayer.this, MEDIA_INFO_WHAT_BTV_VOD_SERVER_IP, result);
                                                            onInfoListener.onInfo(MediaPlayer.this, MEDIA_INFO_WHAT_BTV_VOD_SERVER_PORT, playInfo.getUri().getPort());
                                                        } catch (Exception e){
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                    } catch (UnknownHostException e) {
                                        e.printStackTrace();
                                    } catch (MalformedURLException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();
                        }
                        SLog.d(TAG_IPTV, "play before");
                        legacyPlayer.play();
                        SLog.d(TAG_IPTV, "play done");
                    } else {
                        SLog.d(TAG_IPTV, "Request resume");
                        legacyPlayer.resume();
                    }
                }
            }else{
                SLog.d(TAG_IPTV, "playinfo is empty");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        SLog.d(TAG_IPTV,  "OUT|start done player = " + MediaPlayer.this);
        isPlaying = true;
        isPaused = false;

        mPlayerStatus = IPTV_STATUS_STARTED;
    }

    /**
     * Pauses playback.
     * <p>
     * Call start() to resume.
     *
     */
    public void pause() {
        try {
            SLog.d(TAG_IPTV,  "IN|" + "nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
            if (playInfo != null && playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                try {
                    mHLSPlayer.pause();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                legacyPlayer.pause();
            }
            isPaused = true;
            mPlayerStatus = IPTV_STATUS_PAUSEED;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Resets the MediaPlayer to its uninitialized state.
     * <p>
     * After calling this method, you will have to initialize it again by setting the
     * data source and calling prepare().
     */
    public void reset() {
        SLog.d(TAG_IPTV,  "IN|" + "nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            if (tuneInfo != null) {
                SLog.d(TAG_IPTV, "closeTV");
                if(playerId != IptvMediaPlayer.IPTV_DEVICE_MAIN){
                    try {
                        Thread.sleep(5);
                    }catch (InterruptedException e){
                        e.printStackTrace();
                    }
                }
                legacyPlayer.closeTV();
                tuneInfo = null;
            }

            if (playInfo != null) {
                if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                    SLog.d(TAG_IPTV_HLS, "close");
                    hls_player_release();
                }else{
                    SLog.d(TAG_IPTV, "close");
                    if(playerId != IptvMediaPlayer.IPTV_DEVICE_MAIN){
                        try {
                            Thread.sleep(5);
                        }catch (InterruptedException e){
                            e.printStackTrace();
                        }
                    }
                    legacyPlayer.close();
                }
                playInfo = null;
            }

            if (mvTuneInfo != null) {
                SLog.d(TAG_BTVMEDIA_MULTIVIEW_IPTV, "close");
                legacyPlayer.closeTV();
                mvTuneInfo = null;
            }

            isPlaying = false;
            isPaused = false;
            mPlayerStatus = IPTV_STATUS_STOPED;
        } catch (IOException e) {
            e.printStackTrace();
        }

        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
    }

    /**
     * Releases resources associated with this MediaPlayer object.
     *
     */
    public void release() {
        SLog.d(TAG_IPTV,  "IN|" + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            if (tuneInfo != null) {
                SLog.d(TAG_IPTV, "closeTV");
                legacyPlayer.closeTV();
                tuneInfo = null;
            }

            if (playInfo != null) {
                if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                    SLog.d(TAG_IPTV_HLS, "close");
                    hls_player_release();
                }else{
                    SLog.d(TAG_IPTV, "close");
                    legacyPlayer.close();
                }
                playInfo = null;
            }

            if (mvTuneInfo != null) {
                SLog.d(TAG_BTVMEDIA_MULTIVIEW_IPTV, "close");
                legacyPlayer.closeTV();
                mvTuneInfo = null;
            }

            isPlaying = false;
            isPaused = false;
            mPlayerStatus = IPTV_STATUS_IDLE;

            legacyPlayer.setOnTuneEventListener(null);
            legacyPlayer.setOnDisconnectedListener(null);
            legacyPlayer.setOnPlayStatusListener(null);
            legacyPlayer.setOnSectionFilterListener(null);
            legacyPlayer.setOnPidChangedListener(null);
            legacyPlayer.setOnSeekCompletionListener(null);
            legacyPlayer.disconnect();

        } catch (IOException e) {
            e.printStackTrace();
        }

//        unbindHLSPlayerConnection();

        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);

        if(mContext != null && mControlBroadcastReceiver != null) {
            mContext.unregisterReceiver(mControlBroadcastReceiver);
            mControlBroadcastReceiver = null;
        }
    }

    /**
     * Checks whether the MediaPlayer is playing.
     *
     * @return true if currently playing, false otherwise
     */
    public boolean isPlaying() {
        SLog.d(TAG_IPTV,  "IN|" + "nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
/*
        if(playInfo != null && mLooper != null) {
            if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS) {
                return hlsPlayer.isPlaying();
            }
        }

 */
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
        return isPlaying && !isPaused;
    }

    /**
     * Moves the media to specified time position by considering the given pause mode.
     * <p>
     * When seekTo is finished, the user will be notified via OnSeekComplete supplied by the user.
     *
     * @param msec the offset in milliseconds from the start to seek to.
     * @param pause the mode indicating play or pause after seek to.
     *              Use true if one wants to pause after seek to.
     *              Use false if one wants to play after seek to.
     */
    public void seekTo(long msec, boolean pause) {
        SLog.d(TAG_IPTV,  "IN|seekTo:" + msec + "pause:" + pause + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            currntTime = msec;
            if(playInfo != null && playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                try {
                    mHLSPlayer.seekTo(msec);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                legacyPlayer.seek(msec, pause);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
    }


    /**
     * Seeks to specified time position.
     * <p>
     * Same as {@link #seekTo(long, boolean)} with {@code pause = false}.
     *
     * @param msec the offset in milliseconds from the start to seek to.
     */
    public void seekTo(long msec) {
        SLog.d(TAG_IPTV,  "IN|seekTo:" + msec + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            currntTime = msec;
            if(playInfo != null && playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                try {
                    mHLSPlayer.seekTo(msec);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                legacyPlayer.seek(msec, false);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
    }


    /**
     * Pauses playback at specified time position.
     * <p>
     * When arrived to the specified time position, pauses playback
     * <p>
     * Call start() to resume.
     *
     * @param msec the offset in milliseconds from the start to want to pause.
     */
    public void pauseAt(long msec) {
        try {
            SLog.d(TAG_IPTV,  "IN|pauseAt:" + msec + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
            if (playInfo != null && playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                try {
//                    mHLSPlayer.pause();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                legacyPlayer.pauseAt(msec);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets Player Status
     *
     * @return player status
     *         <p>
     *         status are:
     *         {@link #IPTV_STATUS_IDLE},
     *         {@link #IPTV_STATUS_INITIALIZED},
     *         {@link #IPTV_STATUS_PREPARED},
     *         {@link #IPTV_STATUS_STARTED},
     *         {@link #IPTV_STATUS_PAUSEED},
     *         {@link #IPTV_STATUS_STOPED},
     *         {@link #IPTV_STATUS_PLAYBACKCOMPLETED}
     */
    public int getPlayerStatus(){
        return mPlayerStatus;
    }

    /**
     * Sets keep last frame flag.
     * <p>
     * if you set flag true, last frame of video be showing When the video ends
     *
     * @param keep  the mode indicating last frame of video be showing.
     *              Use true if one wants to show the last frame of video.
     *              Use false if one wants to show black screen.
     */
    public void setKeepLastFrame(boolean keep) {
        SLog.d(TAG_IPTV,  "IN|KeepLastFrame:" + keep + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        if (playInfo != null){
            try {
                legacyPlayer.setKeepLastFrame(keep);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
    }


    /**
     * Gets the duration of the file.
     *
     * @return the duration in milliseconds, if no duration is available
     *         (for example, if streaming live content), 0 is returned.
     */
    public int getDuration() {
        SLog.d(TAG_IPTV,  "IN|" + "nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        if (playInfo != null && playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
            try {
                SLog.d(TAG_IPTV,  "OUT|duration = " + (int) mHLSPlayer.getDuration() + ", player = " + MediaPlayer.this);
                return (int) mHLSPlayer.getDuration();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        SLog.i(TAG_IPTV, "Get Duration from " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP") + " " + duration);
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
        return (int) duration;
    }

    /**
     * Gets the current playback position.
     *
     * @return the current position in milliseconds
     */
    public int getCurrentPosition() {
        SLog.d(TAG_IPTV,  "IN|" + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        //SLog.i(TAG_IPTV, "Get Current Position from " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP") + " " + currntTime);
        //return (int) currntTime;
        if (playInfo != null && playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
            try {
                SLog.d(TAG_IPTV,  "OUT|Current Position = " + (int) mHLSPlayer.getCurrentPosition() + ", player = " + MediaPlayer.this);
                return (int) mHLSPlayer.getCurrentPosition();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Parcel request = newRequest();
        Parcel reply = Parcel.obtain();
        request.writeInt(1105);
        invoke(request, reply);
        long currentPts = reply.readLong() * 2 / 90;
        SLog.i(TAG_IPTV, "Get Current Position from " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP") + " " + currentPts);
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
        return (int) currentPts;
    }

    /**
     * Sets playback speed rate.
     *
     * @param eTrick  the ratio between desired playback speed rate.
     *         <p>
     *         Trick Speed that can be used:
     *         <em>TRICK_SPEED.BX160</em>,
     *         <em>TRICK_SPEED.BX080</em>,
     *         <em>TRICK_SPEED.BX040</em>,
     *         <em>TRICK_SPEED.BX020</em>,
     *         <em>TRICK_SPEED.X010</em>,
     *         <em>TRICK_SPEED.X008</em>,
     *         <em>TRICK_SPEED.X012</em>,
     *         <em>TRICK_SPEED.X015</em>,
     *         <em>TRICK_SPEED.X020</em>,
     *         <em>TRICK_SPEED.X040</em>,
     *         <em>TRICK_SPEED.X080</em>,
     *         <em>TRICK_SPEED.X160</em>
     *
     * @return the result for operation.
     *          if operation is success, 0 is returned.
     *          if operation is fail, -1 is returned.
     *
     * @see com.skb.btv.framework.media.core.MediaPlayer.TRICK_SPEED
     */
    public int trick(TRICK_SPEED eTrick) {
        int result = -1;
        SLog.d(TAG_IPTV,  "IN|trick = " + (float)eTrick.getSpeedRatio() + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            if (playInfo != null && playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                try {
                    mHLSPlayer.playTrick((float)eTrick.getSpeedRatio());
                    result = 0;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                return legacyPlayer.trick(eTrick.getSpeedId());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
        return result;
    }

    /**
     * Returns an array of multi-view track information.
     *
     * @return Array of {@link TrackInfo}. The total number of tracks is the array length.
     *
     * @see com.skb.btv.framework.media.core.MediaPlayer.TrackInfo
     */
    public TrackInfo[] getTrackInfo () {
        SLog.d(TAG_IPTV,  "IN|" + "nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        TrackInfo[] trackInfos = null;

        if(mvTuneInfo != null) {
            trackInfos = new TrackInfo[mvTuneInfo.size()];

            for(int i = 0; i < mvTuneInfo.size(); i++) {
                trackInfos[i] = new TrackInfo();
                trackInfos[i].trackType = TrackInfo.MEDIA_TRACK_TYPE_AUDIO;
            }
        }
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
        return trackInfos;
    }

    /**
     * Selects a track of multi-view
     * <p>
     * Set audio focus for each channel of multi-view track.
     * </p>
     *
     * @param index the index of the track to be selected. The valid range of the index
     * is 0..total number of track - 1. The total number of tracks as well as the type of
     * each individual track can be found by calling {@link #getTrackInfo()} method.
     *
     * @see com.skb.btv.framework.media.core.MediaPlayer#getTrackInfo
     */
    public void selectTrack(int index) {
        SLog.d(TAG_IPTV,  "IN|selectTrack = " + index + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        if(mvTuneInfo != null && index < mvTuneInfo.size()) {
            try {
                if(mvTuneInfo.get(index) instanceof IptvTuneInfo) {
                    IptvTuneInfo iptvTuneInfo = (IptvTuneInfo) mvTuneInfo.get(index);
                    legacyPlayer.changeAudioChannel(iptvTuneInfo.GetAudioPID(), iptvTuneInfo.GetAudioStream());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
    }

    /**
     * Sets the volume on this player.
     * <p>
     * This API is recommended for balancing the output of audio streams
     * within main player(multi-view) and pip player.
     * <p>
     * Note that the passed volume values are raw scalars in range 0.0 to 1.0.
     *
     * @param volume volume scalar
     */
    public void setVolume(float volume) {
        SLog.d(TAG_IPTV,  "IN|setVolume = " + volume + "," + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        mute(volume == 0);
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
    }

    /**
     * mute the volume on this player.
     *
     * @param isMute the mode indicating mute or unmute.
     *               Use true if one wants to mute.
     *               Use false if one wants to unmute.
     */
    public void mute(boolean isMute) {
        SLog.d(TAG_IPTV,  "IN|mute = " + isMute + "," + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            if (playInfo != null && playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                try {
                    mHLSPlayer.muteAudio(isMute);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            legacyPlayer.changeAudioOnOff(isMute ? IptvMediaPlayer.IPTV_AUDIO_OFF : IptvMediaPlayer.IPTV_AUDIO_ON);
            mLastMVAudioOnOff = isMute;

        } catch (IOException e) {
            e.printStackTrace();
        }
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
    }

    /**
     * Create a request parcel which can be routed to the native media
     * player using {@link #invoke(Parcel, Parcel)}. The Parcel
     * returned has the proper InterfaceToken set. The caller should
     * not overwrite that token, i.e it can only append data to the
     * Parcel.
     *
     * @return A parcel suitable to hold a request for the native
     * player.
     */

    public Parcel newRequest() {
        return legacyPlayer.newRequest();
    }

    /**
     * Invoke a generic method on the native player using opaque
     * parcels for the request and reply. Both payloads' format is a
     * convention between the java caller and the native player.
     * Must be called after setDataSource to make sure a native player
     * exists.
     *
     * @param request Parcel with the data for the extension. The
     * caller must use {@link #newRequest()} to get one.
     *
     * @param reply Output parcel with the data returned by the
     * native player.
     */
    public void invoke(Parcel request, Parcel reply) {
        legacyPlayer.invoke(request, reply);
    }

    /**
     * Gets version info for MediaPlayer
     */
    public String getVersion() {
        SLog.i(TAG_IPTV, "Get Version: " + STB_MEDIA_PLAYER_VER);
        return STB_MEDIA_PLAYER_VER;
    }

    protected boolean isDualAudio() {
        if (tuneInfo != null) {
            return tuneInfo.isDualChannelAudio();
        }
        return false;
    }

    protected boolean isMainAudio() {
        if (tuneInfo != null) {
            return tuneInfo.isMainAudio();
        }
        return true;
    }

    protected void switchAudio() {
        if (tuneInfo != null) {
            try {
                if (tuneInfo.isDualChannelAudio()) {
                    if (tuneInfo.isMainAudio()) {
                        legacyPlayer.changeAudioChannel(tuneInfo.GetAudioPID1(), tuneInfo.GetAudioStream1());

                    } else {
                        legacyPlayer.changeAudioChannel(tuneInfo.GetAudioPID(), tuneInfo.GetAudioStream());
                    }

                    tuneInfo.setMainAudio(!tuneInfo.isMainAudio());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Returns an array of audio track information.
     *
     * @return Array of {@link AudioTrack}. The total number of audio tracks is the array length.
     *
     * @see com.skb.btv.framework.media.core.MediaPlayer.AudioTrack
     */
    protected AudioTrack[] getAudioTrack () {
        SLog.d(TAG_IPTV,  "IN|" + "nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        AudioTrack[] tracks = null;
        int numofAudioTrack = 1;
        int index = 0;
        if (tuneInfo != null) {
            boolean dualChannelAudio = tuneInfo.isDualChannelAudio();
            if(dualChannelAudio){
                numofAudioTrack = 2;
            }
            tracks = new AudioTrack[numofAudioTrack];
            tracks[index] = new AudioTrack(tuneInfo.GetAudioPID(), tuneInfo.GetAudioStream());
            if(dualChannelAudio){
                index++;
                tracks[index] = new AudioTrack(tuneInfo.GetAudioPID1(), tuneInfo.GetAudioStream1());
            }
        }
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
        return tracks;
    }

    /**
     * Changes to the given audio track.
     * <p>
     * Set audio focus for each track of multi-audio track.
     * </p>
     *
     * @param index the index of the track to be selected. The valid range of the index
     * is 0..total number of track - 1. The total number of tracks as well as the type of
     * each individual track can be found by calling {@link #getAudioTrack()} method.
     *
     * @see com.skb.btv.framework.media.core.MediaPlayer#getAudioTrack
     */
    protected void setAudioTrack(int index) {
        SLog.d(TAG_IPTV,  "IN|index:" + index + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        if (tuneInfo != null) {
            try {
                if (tuneInfo.isDualChannelAudio()) {
                    SLog.d(TAG_IPTV,  "IN|this channel is dual audio => Handling");
                    if (index != 0) {
                        legacyPlayer.changeAudioChannel(tuneInfo.GetAudioPID1(), tuneInfo.GetAudioStream1());
                    } else {
                        legacyPlayer.changeAudioChannel(tuneInfo.GetAudioPID(), tuneInfo.GetAudioStream());
                    }

                    tuneInfo.setMainAudio(!tuneInfo.isMainAudio());
                }else{
                    SLog.d(TAG_IPTV,  "IN|this channel isn't dual audio");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
    }

    protected int setWindowSize(int nLeft, int nTop, int nWidth, int nHeight) {
        try {
            legacyPlayer.setWindowSize(nLeft, nTop, nWidth, nHeight);
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    protected void startDmxFilter(DmxFilter filter)
    {
        SLog.d(TAG_IPTV,  "IN|pid = " + filter.pid + ", tid = " + filter.tid + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        legacyPlayer.startDmxFilter(filter);
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
    }

    protected void stopDmxFilter(DmxFilter filter){
        SLog.d(TAG_IPTV,  "IN|pid = " + filter.pid + ", tid = " + filter.tid + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        legacyPlayer.stopDmxFilter(filter);
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
    }

    protected ArrayList<String> getLanguageList(int index) {
        SLog.d(TAG_IPTV,  "IN|index = " + index + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            if (mHLSPlayer !=null){
                return (ArrayList<String>)mHLSPlayer.getLanguageList(index);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
        return null;
    }

    protected void setTrackLanguage(int index, String language){
        SLog.d(TAG_IPTV,  "IN|index = " + index + ", language = " + language + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            if (mHLSPlayer !=null){
                mHLSPlayer.setTrackLanguage(index, language);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
    }

    protected void setTrackDisable(int index, boolean flag){
        SLog.d(TAG_IPTV,  "IN|index = " + index + ", flag = " + flag + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            if (mHLSPlayer !=null){
                mHLSPlayer.setTrackDisable(index, flag);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
    }

    protected void adjustSubtitleOffset(long offset){
        SLog.d(TAG_IPTV,  "IN|offset = " + offset + ", nDevice = " + (playerId == 0 ? "Main" : "PIP") + ", player = " + MediaPlayer.this);
        try {
            if (mHLSPlayer !=null){
                mHLSPlayer.adjustSubtitleOffset(offset);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        SLog.d(TAG_IPTV,  "OUT|player = " + MediaPlayer.this);
    }

    private void initialize() {

        onPreparedListener = null;
        onInfoListener = null;
        onErrorListener = null;
        onCompletionListener = null;
        onSeekCompletionListener = null;
        onWebVTTUpdateListener = null;
        onDmxFilterListener = null;
        mPlayerStatus =IPTV_STATUS_IDLE;

        onIptvDisconnectedListener = new IptvMediaPlayer.OnDisconnectedListener() {
            @Override
            public void onDisconnected(IptvMediaPlayer iptv_mp, int nDevice, int nReason) {
                SLog.d(TAG_BTVMEDIA_IPTV_CALLBACK, "onDisconnected - nDevice : " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP") + nDevice + " nReason : " + nReason);
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        SLog.d(TAG_BTVMEDIA_IPTV_CALLBACK, "onErrorListener - onError(what:100, extra:0) before" + ", this->" + MediaPlayer.this);
                        if (onErrorListener != null) {
                            SLog.d(TAG_BTVMEDIA_IPTV_CALLBACK, "onErrorListener - onError(what:100, extra:0)" + ", this->" + MediaPlayer.this);
                            onErrorListener.onError(MediaPlayer.this, MEDIA_ERROR_WHAT_SERVER_DIED , 0);
                        }
                    }
                }, 1000);
            }
        };

        onIptvPlayStatusListener = new IptvMediaPlayer.OnPlayStatusListener() {
            @Override
            public void onPlayStatus(IptvMediaPlayer iptv_mp, int nDevice, String playStatus) {
                SLog.d(TAG_BTVMEDIA_IPTV_CALLBACK, "onPlayStatus - nDevice : " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP") + nDevice + " playStatus : " + playStatus);

                Status status = Status.parse(playStatus);

                if (status instanceof Status.StatusPlay) {
                    if (((Status.StatusPlay) status).getPlaybackStatus().equals("PLAY")) {
                        if (onInfoListener != null) {
                            if (((Status.StatusPlay) status).getTime() == 0L && currntTime > 0) {
                                onInfoListener.onInfo(MediaPlayer.this, MEDIA_INFO_WHAT_BTV_PLAYER, MEDIA_INFO_EXTRA_BEGIND_OF_STREAM);
                                SLog.i(TAG_IPTV, "Send Play Start Info(20001)");
                            }
                        }
                    } else if (((Status.StatusPlay) status).getPlaybackStatus().equals("STOP_EOF")) {
                        if (onCompletionListener != null && (mPlayerStatus == IPTV_STATUS_STARTED)) {
                            onCompletionListener.onCompletion(MediaPlayer.this);
                        }
                        mPlayerStatus = IPTV_STATUS_PLAYBACKCOMPLETED;
                    } else if (((Status.StatusPlay) status).getPlaybackStatus().equals("RTSP_EOS")) {
                        if (onInfoListener != null) {
                            onInfoListener.onInfo(MediaPlayer.this, MEDIA_INFO_WHAT_BTV_PLAYER, MEDIA_INFO_EXTRA_END_OF_STREAM);
                            SLog.i(TAG_IPTV, "Send RTSP_EOS Info(20006)");
                        }
                    } else if (((Status.StatusPlay) status).getPlaybackStatus().equals("PAUSE")) {
                        isPaused = true;
                    }

                    duration = ((Status.StatusPlay) status).getDuration();
                    currntTime = ((Status.StatusPlay) status).getTime();
                    SLog.i(TAG_IPTV, "Set Current Position : " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main " : "PIP ") + currntTime);
                } else if (onErrorListener != null) {
                    if (status instanceof Status.StatusError) {
                        mPlayerStatus = IPTV_STATUS_STOPED;
                        if(((Status.StatusError) status).getErrorCode() == MEDIA_INFO_EXTRA_PLAY_BAD_RECEIVING) {
                            onInfoListener.onInfo(MediaPlayer.this, MEDIA_INFO_WHAT_BTV_PLAYER, ((Status.StatusError) status).getErrorCode());
                        } else {
                            onErrorListener.onError(MediaPlayer.this, MEDIA_ERROR_WHAT_BTV_PLAYER, ((Status.StatusError) status).getErrorCode());
                        }
                    }
                }
            }
        };

        onIptvSectionFilterListener = new IptvMediaPlayer.OnSectionFilterListener() {
            @Override
            public void onSectionFilter(IptvMediaPlayer iptv_mp, byte[] sectionData) {
                StringBuilder log = new StringBuilder();
                for (byte b : sectionData) {
                    log.append(String.format("0x%02X ", b));
                }
                SLog.i(TAG_BTVMEDIA_IPTV_CALLBACK, "onSectionFilter - " + " sectionData : " + log.toString());
                if (onDmxFilterListener != null) {
                    onDmxFilterListener.onDmxFilter(MediaPlayer.this, sectionData);
                }
            }
        };

        onIptvPidChangedListener = new IptvMediaPlayer.OnPidChangedListener() {
            @Override
            public void onPidChagned(IptvMediaPlayer iptv_mp, int event, String pid) {
                SLog.d(TAG_BTVMEDIA_IPTV_CALLBACK, "onPidChagned - event : " + event + " pid : " + pid);
                if (onInfoListener != null){
                    int what = 7001; // Video pid Changed
                    if(event == 1) what = 7002; // Audio pid changed
                    if(event == 2) what = 7003; // num of Audio pid changed
                    int ProcessId = Integer.parseInt(pid);
                    onInfoListener.onInfo(MediaPlayer.this, what, ProcessId);
                }
            }
        };

        onIptvSeekCompletionListener = new IptvMediaPlayer.OnSeekCompletionListener() {
            @Override
            public void onSeekCompletion(IptvMediaPlayer iptv_mp) {
                SLog.d(TAG_BTVMEDIA_IPTV_CALLBACK, "onSeekCompletion");
                if (onSeekCompletionListener != null){
                    onSeekCompletionListener.onSeekCompletion(MediaPlayer.this);
                }
            }
        };

        onIptvTuneEventListener = new IptvMediaPlayer.OnTuneEventListener() {
            @Override
            public void onTuneEvent(IptvMediaPlayer iptv_mp, int nDevice, int nStatus) {
                //SLog.d(TAG_BTVMEDIA_IPTV_CALLBACK, "onTuneEvent - nDevice : " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP") + nDevice + " nStatus : " + nStatus);
                if (onInfoListener != null) {
                    switch (nStatus) {
                        case 0:
                            if (!bShowFirstFrame) {
                                if(mvTuneInfo != null){
                                    // Inform firtst iframe event to hlsplayer
                                    Intent intent = new Intent("com.skplanet.hlsplayer.action.PIP_ENTER");
                                    mContext.sendBroadcast(intent);
                                }
                                SLog.d(TAG_BTVMEDIA_IPTV_CALLBACK, "before onInfo ");
                                onInfoListener.onInfo(MediaPlayer.this, MEDIA_INFO_WHAT_BTV_PLAYER, MEDIA_INFO_EXTRA_SHOW_OF_FIRST_I_FRAME);
                                SLog.d(TAG_BTVMEDIA_IPTV_CALLBACK, "after onInfo ");
                                bShowFirstFrame = true;
                            }else{
                                onInfoListener.onInfo(MediaPlayer.this, MEDIA_INFO_WHAT_BTV_PLAYER, MEDIA_INFO_EXTRA_PLAY_GOOD);
                                SLog.d(TAG_BTVMEDIA_IPTV_CALLBACK, "bShowFirstFrame == true");
                            }
                            break;
                        case 1:
                            if (System.currentTimeMillis() - retryTimeOffset > 2000) {
                                onInfoListener.onInfo(MediaPlayer.this, MEDIA_INFO_WHAT_BTV_PLAYER, MEDIA_INFO_EXTRA_PLAY_BAD_RECEIVING);
                            }
                            break;
                    }
                }else{
                    SLog.d(TAG_BTVMEDIA_IPTV_CALLBACK, "onInfoListener is null ");
                }
            }
        };

        if(mContext != null) {
            mControlBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if(intent.getAction().equalsIgnoreCase(STB_ON_SLEEP)) {
                        SLog.d(TAG_BTVMEDIA_IPTV_CALLBACK, "sptek:STB_ON_SLEEP receive " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP"));
                        reset();
                    } else if(intent.getAction().equalsIgnoreCase(STB_ON_WAKE_UP)) {
                        //TODO do nothing
                        SLog.d(TAG_BTVMEDIA_IPTV_CALLBACK, "sptek:STB_ON_WAKE_UP receive " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP"));
                    } else if(intent.getAction().equalsIgnoreCase(STB_AUDIO_EVENT_CONTROL)) {
                        String event = intent.getStringExtra("event");
                        SLog.d(TAG_BTVMEDIA_IPTV_CALLBACK, "sptek:STB_AUDIO_EVENT_CONTROL receive " + (playerId == IptvMediaPlayer.IPTV_DEVICE_MAIN ? "Main" : "PIP") + ", event:" + event);
                        if(event.equalsIgnoreCase("path_through")){
                            int passthrough = intent.getIntExtra("enable", 0);
                            SLog.d(TAG_BTVMEDIA_IPTV_CALLBACK, "passthrough  enable:" + passthrough);
                            if(is_support_edid_ac3_passthrough()) {
                                settingHLSAudioPassthrough(passthrough);
                            }
                        }
                    }
                }
            };

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(STB_ON_SLEEP);
            intentFilter.addAction(STB_ON_WAKE_UP);
            intentFilter.addAction(STB_AUDIO_EVENT_CONTROL);

            mContext.registerReceiver(mControlBroadcastReceiver, intentFilter);
        }

        mHandler = new Handler(Looper.getMainLooper());
    }

    private void settingHLSAudioPassthrough(int passthrough){
        SLog.d(TAG_IPTV_HLS, "settingHLSAudioPassthrough passthrough  enable:" + passthrough
                + ", mHLSPlayerPassthrough: " +mHLSPlayerPassthrough + ", hlsPlayer_init: " + hlsPlayer_init);
        if(mHLSPlayerPassthrough){
            if(passthrough == 0){
                mHLSPlayerPassthrough = false;
                if(mHLSPlayer != null)
                {
                    try {
                        SLog.i(TAG_IPTV_HLS, "allowPassthrough false");
                        mHLSPlayer.allowPassthrough(mHLSPlayerPassthrough);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }else{
            if(passthrough == 1){
                mHLSPlayerPassthrough = true;
                if(mHLSPlayer != null)
                {
                    try {
                        SLog.i(TAG_IPTV_HLS, "allowPassthrough true");
                        mHLSPlayer.allowPassthrough(mHLSPlayerPassthrough);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private IptvTuneInfo getTuneInfo(Uri uri) throws Exception {
        IptvTuneInfo info = new IptvTuneInfo();

        info.SetChNum(uri.getQueryParameter("ch") == null ? 0 : Integer.decode(uri.getQueryParameter("ch")));
        info.SetURL(uri.getHost());
        info.SetPort(uri.getPort());
        info.SetVideoPID(uri.getQueryParameter("vp") == null ? 0 : Integer.decode(uri.getQueryParameter("vp")));
        info.SetAudioPID(uri.getQueryParameter("ap") == null ? 0 : Integer.decode(uri.getQueryParameter("ap")));
        int audioPid1 = 0;
        if(uri.getQueryParameter("ap1") != null)  audioPid1 = Integer.decode(uri.getQueryParameter("ap1"));
        if(uri.getQueryParameter("ap2") != null)  audioPid1 = Integer.decode(uri.getQueryParameter("ap2"));
        info.SetAudioPID1(audioPid1);
        info.SetPCRPID(uri.getQueryParameter("pp") == null ? 0 : Integer.decode(uri.getQueryParameter("pp")));
        info.SetCAPID(uri.getQueryParameter("cp") == null ? 0 : Integer.decode(uri.getQueryParameter("cp")));
        info.SetCASystemID(uri.getQueryParameter("ci") == null ? 0 : Integer.decode(uri.getQueryParameter("ci")));
        info.SetVideoStream(uri.getQueryParameter("vc") == null ? 0 : Integer.decode(uri.getQueryParameter("vc")));
        info.SetAudioStream(uri.getQueryParameter("ac") == null ? 0 : Integer.decode(uri.getQueryParameter("ac")));
        int audioStream1 = 0;
        if(uri.getQueryParameter("ac1") != null)  audioStream1 = Integer.decode(uri.getQueryParameter("ac1"));
        if(uri.getQueryParameter("ac2") != null)  audioStream1 = Integer.decode(uri.getQueryParameter("ac2"));
        info.SetAudioStream1(audioStream1);
        info.setLastFrame(uri.getQueryParameter("lf") == null ? 0 : Integer.decode(uri.getQueryParameter("lf")));
        info.setAuioEnable(uri.getQueryParameter("ae") == null ? 0 : Integer.decode(uri.getQueryParameter("ae")));
        info.setPip(uri.getQueryParameter("p"));
        info.SetResolution(IptvTuneInfo.IPTV_TUNEINFO_RES_HD);

        SLog.d(TAG_IPTV, "getTuneInfo " + info.GetURL());

        return info;
    }

    private IptvPlayInfo getVodPlayInfo(Uri uri) throws Exception {
        IptvPlayInfo info = new IptvPlayInfo();
        info.setUri(uri);

        info.SetContentID(uri.getQueryParameter("ci") == null ? "" : uri.getQueryParameter("ci"));
        info.SetOTPID(uri.getQueryParameter("oi") == null ? "" : uri.getQueryParameter("oi")); // one time password
        info.SetOTPPasswd(uri.getQueryParameter("op") == null ? "" : uri.getQueryParameter("op")); // one time password
        info.SetPath("rtsp://" + uri.getHost() + (uri.getPort() == 0 ? "" : ":" + uri.getPort()) + uri.getPath());
        info.SetResumeSec(uri.getQueryParameter("rp") == null ? 0 : Integer.decode(uri.getQueryParameter("rp")));
        info.setLastFrame(uri.getQueryParameter("lf") == null ? 0 : Integer.decode(uri.getQueryParameter("lf")));
        info.SetPlayMode(IptvPlayInfo.IPTV_MODE_VOD);
        info.SetPlayType(IptvPlayInfo.IPTV_PLAY_TYPE_NORMAL);

        SLog.d(TAG_IPTV, "getVodPlayInfo " + info.getUri());

        return info;
    }

    private IptvPlayInfo getFilePlayInfo(Uri uri) throws Exception {
        IptvPlayInfo info = new IptvPlayInfo();
        info.setUri(uri);

        info.SetContentID(uri.getQueryParameter("ci") == null ? "" : uri.getQueryParameter("ci"));
        info.SetOTPID(uri.getQueryParameter("oi") == null ? "" : uri.getQueryParameter("oi")); // one time password
        info.SetOTPPasswd(uri.getQueryParameter("op") == null ? "" : uri.getQueryParameter("op")); // one time password
        info.SetPath(uri.getPath());
        info.SetResumeSec(uri.getQueryParameter("rp") == null ? 0 : Integer.decode(uri.getQueryParameter("rp")));
        info.setLastFrame(uri.getQueryParameter("lf") == null ? 0 : Integer.decode(uri.getQueryParameter("lf")));
        info.SetPlayMode(IptvPlayInfo.IPTV_MODE_FILE);
        info.SetPlayType(IptvPlayInfo.IPTV_PLAY_TYPE_ADV);

        SLog.d(TAG_IPTV, "getFilePlayInfo " + info.getUri());

        return info;
    }

    private IptvPlayInfo getHLSPlayInfo(Uri uri) throws Exception {
        IptvPlayInfo info = new IptvPlayInfo();
        info.setUri(uri);

        String strUri = uri.toString();
        if (strUri.contains("contents=")) {
            int endPos = strUri.length();
            if (strUri.contains("&license=")) {
                endPos = strUri.indexOf("&license=");
                info.SetLicenseURL(strUri.substring(strUri.indexOf("&license=") + 9, strUri.length()));
            }
            String contentsUri = strUri.substring(strUri.indexOf("contents=") + 9, endPos);
            info.SetContentsURL(contentsUri);
            //URI contextUri = new URI(strUri.substring(strUri.indexOf("contents=") + 9, endPos));
            //info.SetContentsURL(contextUri.getScheme()+"://" + contextUri.getAuthority() + contextUri.getRawPath());
        }else{
            int endPos = strUri.length();
            URI contextUri = new URI(strUri.substring(strUri.indexOf("skb") + 3, endPos));
            info.SetContentsURL(contextUri.getScheme()+"://" + contextUri.getAuthority() + contextUri.getRawPath());
        }

        info.SetContentID(uri.getQueryParameter("ci") == null ? "" : uri.getQueryParameter("ci"));
        info.SetOTPID(uri.getQueryParameter("oi") == null ? "" : uri.getQueryParameter("oi")); // one time password
        info.SetOTPPasswd(uri.getQueryParameter("op") == null ? "" : uri.getQueryParameter("op")); // one time password
        info.SetPath(uri.getPath());
        info.SetResumeSec(uri.getQueryParameter("rp") == null ? 0 : Integer.decode(uri.getQueryParameter("rp")));
        info.setLastFrame(uri.getQueryParameter("lf") == null ? 0 : Integer.decode(uri.getQueryParameter("lf")));

        info.SetPlayMode(IptvPlayInfo.IPTV_MODE_HLS);
        info.SetPlayType(IptvPlayInfo.IPTV_PLAY_TYPE_NORMAL);
        //SLog.d(TAG_IPTV, "getHLSPlayInfo " + info.getUri());

        return info;
    }

    private ArrayList<IptvInfo> getMVTuneInfo(Uri uris) throws Exception {
        if (uris == null) {
            return null;
        }

        ArrayList<IptvInfo> tuneInfos = new ArrayList<>();
        ArrayList<Uri> uriList = new ArrayList<>();
        //FIXME Parse List
        //
        String strUri = uris.toString();
        if (strUri.contains("view1=")) {
            int endPos = strUri.length();
            if (strUri.contains("&view2=")) {
                endPos = strUri.indexOf("&view2=");
            }
            uriList.add(Uri.parse(strUri.substring(strUri.indexOf("view1=") + 6, endPos)));
        }
        if (strUri.contains("view2=")) {
            int endPos = strUri.length();
            if (strUri.contains("&view3=")) {
                endPos = strUri.indexOf("&view3=");
            }
            uriList.add(Uri.parse(strUri.substring(strUri.indexOf("view2=") + 6, endPos)));
        }
        if (strUri.contains("view3=")) {
            int endPos = strUri.length();
            if (strUri.contains("&viewmode=")) {
                endPos = strUri.indexOf("&viewmode=");
            }
            uriList.add(Uri.parse(strUri.substring(strUri.indexOf("view3=") + 6, endPos)));
        }
        for (Uri uri : uriList) {
            IptvInfo iptvInfo = null;
            if(uri.getScheme().contains("skbiptv")) {
                iptvInfo = getTuneInfo(uri);
            } else if(uri.getScheme().contains("skbfile")) {
                iptvInfo = getFilePlayInfo(uri);
            } else  if(uri.getScheme().contains("skbvod")) {
                iptvInfo = getVodPlayInfo(uri);
            }
            if (iptvInfo != null) {
                tuneInfos.add(iptvInfo);
            }
        }
        if(strUri.contains("viewmode=")) {
            mvMode = Integer.parseInt(strUri.substring(strUri.indexOf("viewmode=") + 9, strUri.length()));
        }

        return tuneInfos;
    }


///////////////////////////////////////////////////////////////////////////////////////
//  HLS Player
    private void bindHLSPlayerConnection() {
        SLog.d(TAG_IPTV_HLS, "IN|" + " bindHLSPlayerConnection");
        Intent i = new Intent();
        i.setComponent(new ComponentName("com.skplanet.hlsplayer", "com.skplanet.hlsplayer.HLSPlayerService"));
        boolean bindResult = mContext.bindService(i, mHLSconnection, Context.BIND_AUTO_CREATE);
        SLog.d(TAG_IPTV_HLS, "OUT|" + " bindService : " + bindResult);
    }

    private void retryHLSPlayerConnection() {
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                if(mHLSPlayer == null) {
                    bindHLSPlayerConnection();
                }
            }
        }, 1000);
    }

    private void unbindHLSPlayerConnection() {
        SLog.d(TAG_IPTV_HLS, "IN|" + " unbindHLSPlayerConnection");
        mForceQuit = true;
        if (mHLSPlayer != null) {
            mContext.unbindService(mHLSconnection);
        }
        SLog.d(TAG_IPTV_HLS, "OUT|" + " unbindHLSPlayerConnection");
    }

    IOnInfoCallback mOnInfoCallback = new IOnInfoCallback.Stub() {
        @Override
        public void onInfo(Message what) throws RemoteException {
            Bundle bundle = what.getData();
            if (bundle.containsKey("PLAYER_STATE")) {
                SLog.i(TAG_BTVMEDIA_IPTV_CALLBACK, "onInfo : " + " PLAYER_STATE : " + bundle.getInt("PLAYER_STATE"));
            }
            if (bundle.containsKey("RENDER_FIRSTFRAME")) {
                SLog.i(TAG_BTVMEDIA_IPTV_CALLBACK, "onInfo : " + " RENDER_FIRSTFRAME : " + bundle.getInt("RENDER_FIRSTFRAME"));
            }
        }
    };

    IOnErrorCallback mOnErrorCallback = new IOnErrorCallback.Stub() {
        @Override
        public void onError(int errorCode, long detailErrorCode, String msg) throws RemoteException {
            SLog.i(TAG_BTVMEDIA_IPTV_CALLBACK, "HLS onError : " + "error : " + errorCode + ", detail error : " + detailErrorCode + ", msg : " + msg);
            mPlayerStatus = IPTV_STATUS_STOPED;
            if(errorCode == MEDIA_INFO_EXTRA_PLAY_BAD_RECEIVING){
                if (onInfoListener != null) {
                    onInfoListener.onInfo(MediaPlayer.this, MEDIA_INFO_WHAT_BTV_PLAYER, errorCode);
                }
            }else{
                if (onErrorListener != null) {
                    onErrorListener.onError(MediaPlayer.this, MEDIA_ERROR_WHAT_BTV_PLAYER, errorCode);
                }
            }
        }
    };

    IOnCompletionCallback mOnCompletionCallback = new IOnCompletionCallback.Stub() {
        @Override
        public void onCompletion() throws RemoteException {
            SLog.i(TAG_BTVMEDIA_IPTV_CALLBACK, "HLS onCompletion");
            if (onCompletionListener != null && (mPlayerStatus == IPTV_STATUS_STARTED)) {
                onCompletionListener.onCompletion(MediaPlayer.this);
            }
            mPlayerStatus = IPTV_STATUS_PLAYBACKCOMPLETED;
        }
    };

    IOnSeekProcessedCallback mOnSeekCompletionCallback = new IOnSeekProcessedCallback.Stub() {
        @Override
        public void onSeekProcessed() throws RemoteException {
            SLog.i(TAG_BTVMEDIA_IPTV_CALLBACK, "HLS onSeekProcessed");
            if (onSeekCompletionListener != null){
                SLog.i(TAG_BTVMEDIA_IPTV_CALLBACK, "Send onSeekCompletion");
                onSeekCompletionListener.onSeekCompletion(MediaPlayer.this);
            }
        }
    };

    IOnRenderedFirstFrameCallback mOnRenderedFirstFrameCallback = new IOnRenderedFirstFrameCallback.Stub() {
        @Override
        public void onRenderedFirstFrame() throws RemoteException {
            SLog.i(TAG_BTVMEDIA_IPTV_CALLBACK, "HLS onRenderedFirstFrame");
            if (onInfoListener != null) {
                if (mHLSPlayer.getCurrentPosition() == 0L) {
                    TRICK_SPEED trickSpeed = TRICK_SPEED.X010;
                    mHLSPlayer.playTrick((float)trickSpeed.getSpeedRatio());
                    onInfoListener.onInfo(MediaPlayer.this, MEDIA_INFO_WHAT_BTV_PLAYER, MEDIA_INFO_EXTRA_BEGIND_OF_STREAM);
                    SLog.i(TAG_BTVMEDIA_IPTV_CALLBACK, "Send Play Start Info(20001)");
                }
                if (!bShowFirstFrame) {
                    onInfoListener.onInfo(MediaPlayer.this, MEDIA_INFO_WHAT_BTV_PLAYER, MEDIA_INFO_EXTRA_SHOW_OF_FIRST_I_FRAME);
                    SLog.i(TAG_BTVMEDIA_IPTV_CALLBACK, "Send Play Start Info(20008)");
                    bShowFirstFrame = true;
                }
            }
        }
    };

    IWebVTTCallback mFirstWebVTTCallback = new IWebVTTCallback.Stub() {
        @Override
        public void setWebVTTInfoList(Bundle bundle) throws RemoteException {
            SLog.i(TAG_BTVMEDIA_IPTV_CALLBACK, "HLS setWebVTTInfoList");
            if (onWebVTTUpdateListener != null) {
                mWebVTTInfoList.clear();

                long[] startTimeList = bundle.getLongArray("START_TIME");
                long[] endTimeList = bundle.getLongArray("END_TIME");

                ArrayList<CharSequence>
                        textList= bundle.getCharSequenceArrayList("TEXT");

                if(textList != null) {
                    long startTime = startTimeList[0];
                    long endTime = endTimeList[0];
                    CharSequence text = textList.get(0);
                    SLog.i(TAG_BTVMEDIA_IPTV_CALLBACK, "start time:" + startTime + ", end time:" + endTime + ", text:" + text);
                    mWebVTTInfoList.add(new WebVTT(startTime, endTime, text));
                    onWebVTTUpdateListener.onWebVTTUpdated(MediaPlayer.this, mWebVTTInfoList);
                }
            }
        }
    };

    private ServiceConnection mHLSconnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            SLog.d(TAG_IPTV_HLS, "IN|" + "onServiceConnected : Class = " + name.getClassName() + ", Package = " + name.getPackageName());
            if (mHLSPlayer == null) {
                try {
                    mHLSPlayer = HLSServiceInterface.Stub.asInterface(service);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            SLog.d(TAG_IPTV_HLS, "OUT|" + "onServiceConnected ");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            SLog.d(TAG_IPTV_HLS, "IN|" + "connection mHLSPlayer " + "onServiceDisConnected : " + name.getClassName());
            mHLSPlayer = null;
            mSetPassthrough = false;
            SLog.d(TAG_BTVMEDIA_IPTV_CALLBACK, "onErrorListener - onError(what:100, extra:0) before" + ", this->" + MediaPlayer.this);
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (onErrorListener != null) {
                        SLog.d(TAG_BTVMEDIA_IPTV_CALLBACK, "onErrorListener - onError(what:100, extra:0)" + ", this->" + MediaPlayer.this);
                        onErrorListener.onError(MediaPlayer.this, MEDIA_ERROR_WHAT_SERVER_DIED , 0);
                    }
                }
            }, 1000);
/*
            if(mForceQuit == false){
                retryHLSPlayerConnection();
            }
 */
            SLog.d(TAG_IPTV_HLS, "OUT|" + "connection mHLSPlayer " + "onServiceDisConnected : ");
        }
    };

    private boolean mSetPassthrough = false;

    private boolean isAmlogic() {
        //return SystemProperties.get("ro.hardware").equals("amlogic");
        return false;  // TODO YIWOO Check
    }

 // TODO YIWOO Check
 private boolean CHECK_TEST_PACKAGE = true; 
 
    private boolean is_support_edid_ac3_passthrough() {
        boolean isAc3Support = false;
        if(!isAmlogic()) {
            SLog.d(TAG_IPTV_HLS, "It's not amlogic soc");
            return true;
        } else {
            SLog.d(TAG_IPTV_HLS, "Detect amlogic soc");
            try {
                //파일 객체 생성
                File file;
                if(CHECK_TEST_PACKAGE)
                    file = new File("/data/user/0/com.skb.framework.myapplication/btf/btvmedia/tmp/hdmi-edid.xml");
				else
					file = new File("/data/user/0/packagename/btf/btvmedia/tmp/hdmi-edid.xml");
                //입력 스트림 생성
                FileReader filereader = new FileReader(file);
                //입력 버퍼 생성
                BufferedReader bufReader = new BufferedReader(filereader);

                String line = "";
                while((line = bufReader.readLine()) != null){
                    //SLog.w(TAG_IPTV_HLS, "file line " + line);
                    if(line.contains("AC3")){
                        isAc3Support = true;
                        break;
                    }
                }
                filereader.close();
            } catch (FileNotFoundException e) {
                SLog.e(TAG_IPTV_HLS, "file not found /data/user/0/packageName/btf/btvmedia/tmp/hdmi-edid.xml ");
            } catch (IOException e) {
                SLog.e(TAG_IPTV_HLS, "file error /data/user/0/packageName/btf/btvmedia/tmp/hdmi-edid.xml ");
            }
        }

        SLog.d(TAG_IPTV_HLS, "deivce support ac3 isAc3Support : " + isAc3Support);
        return isAc3Support;
    }

    private void hls_player_initialize(){
        if(mHLSPlayer != null) {
            try {
                SLog.i(TAG_IPTV_HLS, "before hlsPlayer init");
                if(hlsPlayer_init == false){
                    if(HLS_TUNNEL) {
                        SLog.i(TAG_IPTV_HLS, "hlsPlayer init tunnel mode");
                        mHLSPlayer.init(true);
                    }else{
                        SLog.i(TAG_IPTV_HLS, "hlsPlayer init non-tunnel mode");
                        mHLSPlayer.init(false);
                    }

                    hlsPlayer_init = true;
                }

                Surface surface;
                if (surfaceHolder != null)
                {
                    surface = surfaceHolder.getSurface();
                }
                else
                {
                    surface = null;
                }
                if(surface != null) {
                    mHLSPlayer.setSurface(surface);
                }

                mHLSPlayer.registerOnInfoCallback(mOnInfoCallback);
                mHLSPlayer.registerOnErrorCallback(mOnErrorCallback);
                mHLSPlayer.registerOnCompletionCallback(mOnCompletionCallback);
                mHLSPlayer.registerFirstWebVTTCallback(mFirstWebVTTCallback);
                mHLSPlayer.registerOnSeekProcessedCallback(mOnSeekCompletionCallback);
                mHLSPlayer.registerOnRenderedFirstFrameCallback(mOnRenderedFirstFrameCallback);
                //mHLSPlayer.registerSecondWebVTTCallback(mSecondWebVTTCallback);
                //mHLSPlayer.registerPlayerDebugCallback(mPlayerDebugCallback);

                mHLSPlayerPassthrough = false;
                /* TODO YIWOO Check
                String path_through = SystemProperties.get(SYSPROP_SKB_AUDIO_PATH_THROUGH);
                if(path_through.equals("0") && is_support_edid_ac3_passthrough()){
                    mHLSPlayerPassthrough = true;
                } else {
                    mHLSPlayerPassthrough = false;
                }
                //if(mSetPassthrough == false) {
                //    mSetPassthrough = true;
                    mHLSPlayer.allowPassthrough(mHLSPlayerPassthrough);
                //}

                // video trickmode
                String vendor_trickmode = SystemProperties.get(HLS_VENDOR_TRICKMODE);
                SLog.i(TAG_IPTV_HLS, "set vendor trickmode is : " + vendor_trickmode);
                if("true".equals(vendor_trickmode)) {
                    mHLSPlayer.setCommand("SET_VENDOR_TRICKMODE");
                }
                */
                SLog.i(TAG_IPTV_HLS, "after hlsPlayer init");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void hls_player_release(){
        if(hlsPlayer_init) {
            try {
                SLog.d(TAG_IPTV_HLS, "before hls_player release");
                mWebVTTInfoList.clear();
                SLog.d(TAG_IPTV_HLS, "before hls_player stop");
                mHLSPlayer.stop(true);
                SLog.d(TAG_IPTV_HLS, "after hls_player stop");

                if(mIsMainHLS) {
                    // FIXME : Temporary code for pass through setup on HLS player
                    mContext.sendBroadcast(new Intent(INTENT_HLS_PLAY_STATE_STOP));
                }
                mHLSPlayer.unregisterOnInfoCallback(mOnInfoCallback);
                mHLSPlayer.unregisterOnErrorCallback(mOnErrorCallback);
                mHLSPlayer.unregisterOnCompletionCallback(mOnCompletionCallback);
                mHLSPlayer.unregisterFirstWebVTTCallback(mFirstWebVTTCallback);
                mHLSPlayer.unregisterOnSeekProcessedCallback(mOnSeekCompletionCallback);
                mHLSPlayer.unregisterOnRenderedFirstFrameCallback(mOnRenderedFirstFrameCallback);
                mHLSPlayer.release();
                hlsPlayer_init = false;
                SLog.d(TAG_IPTV_HLS, "after hls_player release");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * STBID 설정 API
     * @param stbId STBID
     * @return
     */
    public int setStbId(String stbId){
        SLog.d(TAG_IPTV,  "IN|STBID Setting Value " + stbId);
        try {
            if (playInfo != null) {
                if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                    try {
                        //mHLSPlayer.setStbId();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    return legacyPlayer.setStbId(stbId);
                }
            }
            // 임시로 API 체크
            else {
                return  legacyPlayer.setStbId(stbId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     *  MAC_ADDR 설정 API
     * @param macAddr
     * @return
     */
    public int setMacAddress(String macAddr){
        SLog.d(TAG_IPTV,  "IN|setMacAddress  : " + macAddr);
        try {
            if (playInfo != null) {
                if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                    try {
                        //mHLSPlayer.setMacAddress();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    return  legacyPlayer.setMacAddress(macAddr);
                }
            }
            // 임시로 API 체크
            else {
                return  legacyPlayer.setMacAddress(macAddr);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * GW_Token 설정 API
     * @param gwToken
     * @return
     */
    public int setGwToken(String gwToken){
        SLog.d(TAG_IPTV,  "IN|setGwToken  : " + gwToken);
        try {
            if (playInfo != null) {
                if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                    try {
                        //mHLSPlayer.setGwToken();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    return legacyPlayer.setGwToken(gwToken);
                }
            }
            // 임시로 API 체크
            else {
                return  legacyPlayer.setGwToken(gwToken);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 화면해설 설정 API
     * @param visionImpaired
     * @return
     */
    public int setVisionImpaired(boolean visionImpaired){
        SLog.d(TAG_IPTV,  "IN|setVisionImpaired  : " + visionImpaired);
        try {
            if (playInfo != null) {
                if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                    try {
                        //mHLSPlayer.setVisionImpaired();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    return legacyPlayer.setVisionImpaired(visionImpaired);
                }
            }
            // 임시로 API 체크
            else {
                return  legacyPlayer.setVisionImpaired(visionImpaired);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 자막방송 설정 API
     * @return
     */
    public int setHearingImpaired(boolean hearingImpaired){
        SLog.d(TAG_IPTV,  "IN|setHearingImpaired  : " + hearingImpaired);
        try {
            if (playInfo != null) {
                if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                    try {
                        //mHLSPlayer.setHearingImpaired();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    return legacyPlayer.setHearingImpaired(hearingImpaired);
                }
            }
            // 임시로 API 체크
            else {
                return  legacyPlayer.setHearingImpaired(hearingImpaired);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Scaling_mode 설정 API
     * @param scalingMode
     * @return
     */
    public int setVideoScalingMode(int scalingMode){
        SLog.d(TAG_IPTV,  "IN|setVideoScalingMode  : " + scalingMode);
        try {
            if (playInfo != null) {
                if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                    try {
                        //mHLSPlayer.setVideoScalingMode();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    return legacyPlayer.setVideoScalingMode(scalingMode);
                }
            }
            // 임시로 API 체크
            else {
                return  legacyPlayer.setVideoScalingMode(scalingMode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * MLR Activation Enable API
     */

    public boolean getMlrActivationEnable(){
        SLog.d(TAG_IPTV,  "IN|getMlrActivationEnable");
        try {
            if (playInfo != null) {
                if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                    try {
                        //mHLSPlayer.getMlrActivationEnable();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    return legacyPlayer.getMlrActivationEnable();
                }
            }
            // 임시로 API 체크
            else {
                return legacyPlayer.getMlrActivationEnable();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public int setMlrActivationEnable(boolean enable){
        SLog.d(TAG_IPTV,  "IN|setMlrActivationEnable  : " + enable);
        try {
            if (playInfo != null) {
                if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                    try {
                        //mHLSPlayer.setMlrActivationEnable(enable);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    return legacyPlayer.setMlrActivationEnable(enable);
                }
            }
            // 임시로 API 체크
            else {
                return  legacyPlayer.setMlrActivationEnable(enable);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }


    /**
     * TTA 테스트 옵션 설정 API
     * @param key
     * @param value
     * @return
     */
    public int setTtaTestOption(String key, String value) {
        SLog.d(TAG_IPTV,  "IN|setTtaTestOption  key => " + key + ", value => " + value);
        try {
            if (playInfo != null) {
                if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                    try {
                        //mHLSPlayer.prepare();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    return legacyPlayer.setTtaTestOption(key, value);
                }
            }
            // 임시로 API 체크
            else {
                return  legacyPlayer.setTtaTestOption(key, value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * setAVOffset 설정 API
     * @param type
	 * @param nTimeMs     
     * @return
     */
    public int setAVOffset(int type, int nTimeMs){
        SLog.d(TAG_IPTV,  "IN|setAVOffset type : "+type+", nTimeMs : "+nTimeMs );
        try {
            if (playInfo != null) {
                if (playInfo.GetPlayMode() == IptvPlayInfo.IPTV_MODE_HLS){
                    try {
                        //mHLSPlayer.setVideoScalingMode();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    return legacyPlayer.setAVOffset(type, nTimeMs);
                }
            }
            // 임시로 API 체크
            else {
                return  legacyPlayer.setAVOffset(type, nTimeMs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

}

