package com.skb.btv.framework.media;

import android.os.Parcel;
import android.os.Parcelable;

public class AppInfo implements Parcelable {
    public static final int AUTOSTART = 1;
    public static final int PRESENT = 2;
    public static final int DESTROY = 3;
    public static final int KILL = 4;
    public static final int PREFETCH = 5;
    public static final int REMOTE = 6;
    public static final int UNKNOWN_APP_TYPE = 0;
    public static final int HTML_APP_TYPE = 28678;
    public static final int ANDROID_APP_TYPE = 28677;

    public int getmAppType() {
        return mAppType;
    }

    public void setmAppType(int mAppType) {
        this.mAppType = mAppType;
    }

    public long getmOrganizationId() {
        return mOrganizationId;
    }

    public void setmOrganizationId(long mOrganizationId) {
        this.mOrganizationId = mOrganizationId;
    }

    public long getmApplicationId() {
        return mApplicationId;
    }

    public void setmApplicationId(long mApplicationId) {
        this.mApplicationId = mApplicationId;
    }

    public int getmDsmccPid() {
        return mDsmccPid;
    }

    public void setmDsmccPid(int mDsmccPid) {
        this.mDsmccPid = mDsmccPid;
    }

    public boolean ismIsServiceBound() {
        return mIsServiceBound;
    }

    public void setmIsServiceBound(boolean mIsServiceBound) {
        this.mIsServiceBound = mIsServiceBound;
    }

    public int getmControlCode() {
        return mControlCode;
    }

    public void setmControlCode(int mControlCode) {
        this.mControlCode = mControlCode;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public int getmPriority() {
        return mPriority;
    }

    public void setmPriority(int mPriority) {
        this.mPriority = mPriority;
    }

    public String getmInitialPath() {
        return mInitialPath;
    }

    public void setmInitialPath(String mInitialPath) {
        this.mInitialPath = mInitialPath;
    }

    int mAppType;
    long mOrganizationId;
    long mApplicationId;
    int mDsmccPid;
    boolean mIsServiceBound;
    int mControlCode;
    String mName;
    int mPriority;
    String mInitialPath;

    protected AppInfo(Parcel in) {
        mAppType = in.readInt();
        mOrganizationId = in.readLong();
        mApplicationId = in.readLong();
        mDsmccPid = in.readInt();
        mIsServiceBound = in.readByte() != 0;
        mControlCode = in.readInt();
        mName = in.readString();
        mPriority = in.readInt();
        mInitialPath = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mAppType);
        dest.writeLong(mOrganizationId);
        dest.writeLong(mApplicationId);
        dest.writeInt(mDsmccPid);
        dest.writeByte((byte) (mIsServiceBound ? 1 : 0));
        dest.writeInt(mControlCode);
        dest.writeString(mName);
        dest.writeInt(mPriority);
        dest.writeString(mInitialPath);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AppInfo> CREATOR = new Creator<AppInfo>() {
        @Override
        public AppInfo createFromParcel(Parcel in) {
            return new AppInfo(in);
        }

        @Override
        public AppInfo[] newArray(int size) {
            return new AppInfo[size];
        }
    };
}
