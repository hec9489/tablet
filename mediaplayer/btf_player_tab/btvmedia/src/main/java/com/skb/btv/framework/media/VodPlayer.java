// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.media;

import android.content.Context;
import android.os.Looper;
import android.util.Log;

import com.skb.btv.framework.media.core.MediaPlayer;

import java.util.ArrayList;
import java.util.List;

/**
 * VodPlayer class can be used to control playback For Vod (rtsp, hls content)
 *
 * @author Kwon Soon Chan (sysc0507@sk.com)
 */

public class VodPlayer extends MediaPlayer {

    /**
     * Interface definition for a callback to be invoked when the media
     * source is ready for playback.
     */
    public interface OnPreparedListener {
        /**
         * Called when the media file is ready for playback.
         *
         * @param mp the VodPlayer that is ready for playback
         */
        void onPrepared(VodPlayer mp);
    }

    /**
     * Interface definition of a callback to be invoked to communicate some
     * info and/or warning about the media or its playback.
     */
    public interface OnInfoListener {
        /**
         * Called to indicate an info or a warning.
         *
         * @param mp the VodPlayer the info pertains to.
         * @param what the type of info or warning.
         * @param extra an extra code, specific to the info.
         */
        boolean onInfo(VodPlayer mp, int what, int extra);
    }

    /**
     * Interface definition of a callback to be invoked when there
     * has been an error during an asynchronous operation (other errors
     * will throw exceptions at method call time).
     */
    public interface OnErrorListener {
        /**
         * Called to indicate an error.
         *
         * @param mp the VodPlayer the error pertains to
         * @param what the type of error that has occurred
         * @param extra an extra code, specific to the error.
         */
        boolean onError(VodPlayer mp, int what, int extra);
    }

    /**
     * Interface definition for a callback to be invoked when playback of
     * a media source has completed.
     */
    public interface OnCompletionListener {
        /**
         * Called when the end of a media source is reached during playback.
         *
         * @param mp the VodPlayer that reached the end of the file
         */
        void onCompletion(VodPlayer mp);
    }

    /**
     * Interface definition of a callback to be invoked indicating
     * the completion of a seek operation.
     */
    public interface OnSeekCompletionListener {
        /**
         * Called to indicate the completion of a seek operation.
         *
         * @param mp the VodPlayer that issued the seek operation
         */
        void onSeekCompletion(VodPlayer mp);
    }

    /**
     * Interface definition of a callback to be invoked to received
     * WebVTT data updated event.
     */
    public interface OnWebVTTUpdateListener {
        /**
         * Called to indicate the receiving of a WebVTT updated event.
         *
         * @param mp the VodPlayer the WebVTT updated event pertains to
         * @param webVTTList  {@link WebVTT} data list
         *
         * @see com.skb.btv.framework.media.WebVTT
         */
        void onWebVTTUpdated(VodPlayer mp, List<WebVTT> webVTTList);
    }

    private OnPreparedListener onPreparedListener;
    private OnInfoListener onInfoListener;
    private OnErrorListener onErrorListener;
    private OnCompletionListener onCompletionListener;
    private OnSeekCompletionListener onSeekCompletionListener;
    private OnWebVTTUpdateListener  onWebVTTUpdateListener;

    private MediaPlayer.OnPreparedListener onMediaPlayerPreparedListener;
    private MediaPlayer.OnInfoListener onMediaPlayerInfoListener;
    private MediaPlayer.OnErrorListener onMediaPlayerErrorListener;
    private MediaPlayer.OnCompletionListener onMediaPlayerCompletionListener;
    private MediaPlayer.OnSeekCompletionListener onMediaPlayerSeekCompletionListener;
    private MediaPlayer.OnWebVTTUpdateListener  onMediaPlayerWebVTTUpdateListener;

    /**
     * VodPlayer Default constructor.
     * <p>
     * Same as {@link #VodPlayer(Context, int)} with {@code pipId = 0}.
     *
     * @param context the Context to use
     *
     * <p>When done with the VodPlayer, you should call  {@link #release()},
     * to free the resources. If not released, too many VodPlayer instances may
     * result in an exception.</p>
     */
    public VodPlayer(Context context) {
        super(context);
        initialize();
    }

    /**
     * VodPlayer constructor for a given pip id player.
     *
     * @param context the Context to use
     * @param pipId indicate main(0) player or pip(1) player
     *
     * <p>When done with the VodPlayer, you should call  {@link #release()},
     * to free the resources. If not released, too many VodPlayer instances may
     * result in an exception.</p>
     */
    public VodPlayer(Context context, int pipId) {
        super(context, pipId);
        initialize();
    }


    /**
     * Register a callback to be invoked when the media source is ready
     * for playback.
     *
     * @param listener the callback that will be run
     */
    public void setOnPreparedListener(OnPreparedListener listener) {
        onPreparedListener = listener;
        if(listener != null)
            super.setOnPreparedListener(onMediaPlayerPreparedListener);
        else
            super.setOnPreparedListener(null);
    }

    /**
     * Register a callback to be invoked when an info/warning is available.
     *
     * @param listener the callback that will be run
     */
    public void setOnInfoListener(OnInfoListener listener) {
        onInfoListener = listener;
        if(listener != null)
            super.setOnInfoListener(onMediaPlayerInfoListener);
        else
            super.setOnInfoListener(null);
    }

    /**
     * Register a callback to be invoked when an error has happened
     * during an asynchronous operation.
     *
     * @param listener the callback that will be run
     */
    public void setOnErrorListener(OnErrorListener listener) {
        onErrorListener = listener;
        if(listener != null)
            super.setOnErrorListener(onMediaPlayerErrorListener);
        else
            super.setOnErrorListener(null);
    }

    /**
     * Register a callback to be invoked when the end of a media source
     * has been reached during playback.
     *
     * @param listener the callback that will be run
     */
    public void setOnCompletionListener(OnCompletionListener listener) {
        onCompletionListener = listener;
        if(listener != null)
            super.setOnCompletionListener(onMediaPlayerCompletionListener);
        else
            super.setOnCompletionListener(null);
    }

    /**
     * Register a callback to be invoked when a seek operation has been
     * completed.
     *
     * @param listener the callback that will be run
     */
    public void setOnSeekCompletionListener(OnSeekCompletionListener listener) {
        onSeekCompletionListener = listener;
        if(listener != null)
            super.setOnSeekCompletionListener(onMediaPlayerSeekCompletionListener);
        else
            super.setOnSeekCompletionListener(null);
    }

    /**
     * Register a callback to be invoked when WebVTT Updated.
     *
     * @param listener the callback that will be run
     */
    public void setOnWebVTTUpdateListener(OnWebVTTUpdateListener listener){
        Log.i(TAG_IPTV, "setOnWebVTTUpdateListener");
        onWebVTTUpdateListener = listener;
        if(listener != null)
            super.setOnWebVTTUpdateListener(onMediaPlayerWebVTTUpdateListener);
        else
            super.setOnWebVTTUpdateListener(null);
    }

    /**
     * Sets the data source as a content Uri.
     *
     * <p>rtsp vod play uri started with skbvod://..
     * <p>multi-view play uri started with skbsmv://..
     * <p>hls play uri started with skbhttps://..
     *
     * @param strUri the Content URI of the data you want to play
     */
    public void setDataSource(String strUri){
        super.setDataSource(strUri);
    }

    /**
     * Gets Language list for current track
     *
     * @param index the index of the request for Audio(1) or Subtitle(2).
     *              Use 1 if one wants to get Audio language list .
     *              Use 2 if one wants to get Subtitle language list .
     *
     * @return ArrayList of Lanauage.
     *
     */
    public ArrayList<String> getLanguageList(int index) {
        return super.getLanguageList(index);
    }

    /**
     * Sets track language for selected renderer
     *
     * @param index the index of the request for Audio(1) or Subtitle(2).
     *              Use 1 if one wants to set Audio language.
     *              Use 2 if one wants to set Subtitle language.
     * @param language Language to be set.
     *
     */
    public void setTrackLanguage(int index, String language){
        super.setTrackLanguage(index, language);
    }

    /**
     * Sets audio, subtitle track disable or enable
     *
     * @param index the index of the request for Audio(1) or Subtitle(2).
     *              Use 1 if one wants to set Audio diable or enable.
     *              Use 2 if one wants to set Subtitle diable or enable.
     * @param flag  disable flag.
     *              Use true if one wants to set diable.
     *              Use false if one wants to set enable.
     *
     */
    public void setTrackDisable(int index, boolean flag){
        super.setTrackDisable(index, flag);
    }

    /**
     * Adjusts subtitle offset in milliseconds
     *
     * @param offset the milliseconds.
     *
     */
    public void adjustSubtitleOffset(long offset){
        super.adjustSubtitleOffset(offset);
    }

    /**
     * Releases resources associated with this VodPlayer object.
     *
     */
    public void release() {
        super.setOnSeekCompletionListener(null);
        super.setOnCompletionListener(null);
        super.setOnErrorListener(null);
        super.setOnInfoListener(null);
        super.setOnPreparedListener(null);
        super.setOnWebVTTUpdateListener(null);
        super.release();

        onMediaPlayerPreparedListener = null;
        onMediaPlayerInfoListener = null;
        onMediaPlayerErrorListener = null;
        onMediaPlayerCompletionListener = null;
        onMediaPlayerSeekCompletionListener = null;
        onMediaPlayerWebVTTUpdateListener = null;
    }

    private void initialize(){
        onMediaPlayerPreparedListener = new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                Log.i(TAG_IPTV, "VodPlayer onPrepared");
                if(onPreparedListener != null)  onPreparedListener.onPrepared(VodPlayer.this);
            }
        };

        onMediaPlayerInfoListener = new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                Log.i(TAG_IPTV, "VodPlayer onInfo");
                if(onInfoListener != null) onInfoListener.onInfo(VodPlayer.this, what, extra);
                return false;
            }
        };

        onMediaPlayerErrorListener = new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.i(TAG_IPTV, "VodPlayer onError");
                if(onErrorListener != null) onErrorListener.onError(VodPlayer.this, what, extra);
                return false;
            }
        };

        onMediaPlayerCompletionListener = new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Log.i(TAG_IPTV, "VodPlayer onCompletion");
                if(onCompletionListener != null) onCompletionListener.onCompletion(VodPlayer.this);
            }
        };

        onMediaPlayerSeekCompletionListener = new MediaPlayer.OnSeekCompletionListener() {
            @Override
            public void onSeekCompletion(MediaPlayer mp) {
                Log.i(TAG_IPTV, "VodPlayer onSeekCompletion");
                if(onSeekCompletionListener != null) onSeekCompletionListener.onSeekCompletion(VodPlayer.this);
            }
        };

        onMediaPlayerWebVTTUpdateListener = new MediaPlayer.OnWebVTTUpdateListener() {
            @Override
            public void onWebVTTUpdated(MediaPlayer mp, List<WebVTT> webVTTList) {
                if(onWebVTTUpdateListener != null) onWebVTTUpdateListener.onWebVTTUpdated(VodPlayer.this, webVTTList);
            }
        };
    }
}
