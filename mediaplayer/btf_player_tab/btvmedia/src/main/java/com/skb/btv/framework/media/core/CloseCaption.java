
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.media.core;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;

import android.util.Log;
import android.view.TextureView;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * CloseCaption class can be used to closed caption display
 *
 * @author Kwon Soon Chan (sysc0507@sk.com)
 */

public class CloseCaption {
    private static final String TAG = "BTVMEDIA_CloseCaption";

    private static final int CLOSE_CAPTION_EVENT    = 0x08;

    private static final int EVENT_CAPTION_DATA     = 0x01;
    private static final int EVENT_CLEAR_WINDOW     = 0x02;
    private static final int EVENT_WINDOW_COLOR     = 0x03;
    private static final int EVENT_WINDOW_ROW_COL   = 0x04;
    private static final int EVENT_FONT_SIZE        = 0x05;
    private static final int EVENT_WINDOW_POINT     = 0x06;
    private static final int EVENT_WINDOW_ANCHOR    = 0x07;
    private static final int EVENT_WINDOW_POSITON   = 0x08;
    private static final int EVENT_WINDOW_JUSTIFY   = 0x09;
    private static final int EVENT_DELETE_WINDOW    = 0x0A;
    private static final int EVENT_TTA_CERT_MODE    = 0x0F;

    private static final int ANCHOR_POINT_TOP_LEFT      = 0;
    private static final int ANCHOR_POINT_TOP_CENTER 	= 1;
    private static final int ANCHOR_POINT_TOP_RIGHT 	= 2;
    private static final int ANCHOR_POINT_MIDDLE_LEFT 	= 3;
    private static final int ANCHOR_POINT_MIDDLE_CENTER = 4;
    private static final int ANCHOR_POINT_MIDDLE_RIGHT 	= 5;
    private static final int ANCHOR_POINT_BOTTOM_LEFT 	= 6;
    private static final int ANCHOR_POINT_BOTTOM_CENTER = 7;
    private static final int ANCHOR_POINT_BOTTOM_RIGHT 	= 8;

    private static final int SMALL_FONT_SIZE        = 36;
    private static final int STANDARD_FONT_SIZE     = 40;
    private static final int LARGE_FONT_SIZE        = 44;

    private static final float CRITERION_SURFACE_WIDTH = 1920.0f;

    private static final int WINDOW_JUSTIFY_LEFT	= 0;
    private static final int WINDOW_JUSTIFY_RIGHT	= 1;
    private static final int WINDOW_JUSTIFY_CENTER	= 2;
    private static final int WINDOW_JUSTIFY_FULL	= 3;

    private static final int DTVCC_SCREENGRID_ROWS    = 75;
    private static final int DTVCC_SCREENGRID_COLUMNS = 210;

    private static final int DTVCC_WINDOW_INIT = 0x0F;

    private static final String ITALIC_FIRST_PATTERN = "<i>";
    private static final String ITALIC_LAST_PATTERN = "</i>";
    private static final String UNDERLINE_FIRST_PATTERN = "<u>";
    private static final String UNDERLINE_LAST_PATTERN = "</u>";
    private static final String COLOR_FIRST_PATTERN = "<font color=";
    private static final String COLOR_LAST_PATTERN = "</font>";
    private static final String BG_COLOR_FIRST_PATTERN = "<bg color=";
    private static final String BG_COLOR_LAST_PATTERN = "</bg>";
    private static final String BR_PATTERN = "<br>";
    private static final String QUOTATION_MARK = "\"";
    private static final String DELIMETER = ">";
    private static final String TEST_STRING = "한";

    private static Typeface mDefaultTypeFace;

    private TextureView  mCCSurfaceTextureView;
    private Canvas mCanvas;
    private Object mLock = new Object();
    private CCDrawHandler mHandler;
    private boolean active = true;
    private boolean initialized;

    private int marginLeft;
    private int marginTop;
    private int backGroundColor;
    private int rowCount;
    private int colCount;
    private int fontSize;
    private int justification;
    private int anchorId;
    private int anchorTop;
    private int anchorLeft;
    private int rowPosition;
    private int colPosition;
    private int leftMargin;
    private int surface_width;
    private int surface_height;
    private int FontWidth;
    private int FontHeight;
    private int SafeWidth;
    private int SafeHeight;
    private int activeWindowId;
    ArrayList<CCWindow> Windows = new ArrayList<>();

    public CloseCaption()
    {
        Log.i(TAG, "CloseCaption() Constructor");
        active = true;
        mHandler = new CCDrawHandler();
        fontSize = STANDARD_FONT_SIZE;
        rowCount = 3;
        colCount = 40;
        anchorId = 7;
        rowPosition = 0;
        colPosition = 0;
        leftMargin = 3;
        justification = WINDOW_JUSTIFY_LEFT;
        backGroundColor = Color.BLACK;
        getDefaultTypeFace();
    }

    public void setCCDisplay(TextureView textureView) {
        Log.i(TAG, "CloseCaption() setCCDisplay");
        if(textureView != null) {
            mCCSurfaceTextureView = textureView;
            mCCSurfaceTextureView.setOpaque(false);
            marginLeft = mCCSurfaceTextureView.getWidth() / 5;
            marginTop = mCCSurfaceTextureView.getHeight() / 10;
            surface_width = mCCSurfaceTextureView.getWidth();
            surface_height = mCCSurfaceTextureView.getHeight();

            fontSize = STANDARD_FONT_SIZE;

            float ratio = (surface_width / CRITERION_SURFACE_WIDTH);
            fontSize = (int)(ratio*fontSize);

            FontWidth = drawLayoutWidth(TEST_STRING, TEST_STRING.length(), fontSize);
            FontHeight = drawLayoutHeight(TEST_STRING, TEST_STRING.length(), fontSize);
            initialized = false;
            activeWindowId = DTVCC_WINDOW_INIT;
            clearCloseCaption(activeWindowId);
        }else{
            Log.i(TAG, "CloseCaption() setCCDisplay set null");
        }
        Log.i(TAG, "CloseCaption() setCCDisplay end");
    }

    public void updateScreen(){
        CCWindow window = null;
        for (int i = 0; i < Windows.size(); i++) {
            window = Windows.get(i);

            if(window.text == null) continue;

            marginLeft = window.marginLeft;
            marginTop = window.marginTop;
            fontSize = window.fontSize;
            justification = window.justification;
            colCount = window.colCount;
            drawWindow(window);
            drawCCText(window);
        }
    }

    public void drawCloseCaption(int windowId, String cc_data){
        try {
            Log.i(TAG, "cc_data:" + cc_data);
            mCanvas = mCCSurfaceTextureView.lockCanvas(null);
            synchronized (mLock) {
                if (mCanvas != null) {
                    mCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                    CCWindow window = getWindow(windowId);
                    if(window != null) {
                        window.text = cc_data;
                    }
                    updateScreen();
                }
                mLock.notify();
            }
        } catch (Exception e) {
            Log.e(TAG, "drawCloseCaption()" + e.getMessage());
        } finally {
            if (mCanvas != null) {
                mCCSurfaceTextureView.unlockCanvasAndPost(mCanvas);
            }
        }
    }

    public void refreshScreen(){
        try {
            mCanvas = mCCSurfaceTextureView.lockCanvas(null);
            synchronized (mLock) {
                if (mCanvas != null) {
                    mCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                    updateScreen();
                }
                mLock.notify();
            }
        } catch (Exception e) {
            Log.e(TAG, "RefreshScreen()" + e.getMessage());
        } finally {
            if (mCanvas != null) {
                mCCSurfaceTextureView.unlockCanvasAndPost(mCanvas);
            }
        }
    }

    public void clearCloseCaption(int windowId){
        try {
            Log.i(TAG, "clearCloseCaption() windowId:" + windowId);
            CCWindow window = null;
            mCanvas = mCCSurfaceTextureView.lockCanvas(null);
            synchronized (mLock) {
                if (mCanvas != null) {
                    mCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                    if(windowId == DTVCC_WINDOW_INIT){
                        activeWindowId = DTVCC_WINDOW_INIT;
                        clearWindows();
                    }else{
                        window = getWindow(windowId);
                        if(window != null) {
                            window.text = null;
                        }
                        updateScreen();
                    }
                    mLock.notify();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "clearCloseCaption()" + e.getMessage());
        } finally {
            if (mCanvas != null) {
                mCCSurfaceTextureView.unlockCanvasAndPost(mCanvas);
            }
        }
    }

    public boolean containsItalic(String line){
        boolean italic_found = false;
        int found = -1;
        found = line.indexOf(ITALIC_FIRST_PATTERN, 0);
        if (found == -1){
            italic_found = false;
        }else{
            italic_found = true;
        }
        return italic_found;
    }

    public boolean containsUnderline(String line){
        boolean underline_found = false;
        int found = -1;
        found = line.indexOf(UNDERLINE_FIRST_PATTERN, 0);
        if (found == -1){
            underline_found = false;
        }else{
            underline_found = true;
        }
        return underline_found;
    }


    public int printCCMessage(String ccString, int row_length) {

        Location location;

        int found;
        int bg_start = 0;
        int bg_end = 0;
        int font_start = 0;
        int font_end = 0;
        int length = 0;

        int col_length = 0;

        int italic = 0;
        int underline = 0;

        int bgColor = Color.BLACK;
        int fontColor = Color.WHITE;

        bg_start = ccString.indexOf(BG_COLOR_FIRST_PATTERN, 0);
        if (bg_start != -1){
            found = ccString.indexOf(BG_COLOR_LAST_PATTERN, bg_start);
            bg_end = found + BG_COLOR_LAST_PATTERN.length();

            String data = ccString.substring(font_start, bg_end);
            String hexColor = getBGColorValue(data);
            bgColor = Color.parseColor("#" + hexColor);
            ccString = getCCMessageBGColor(data);
        }

        font_start = ccString.indexOf(COLOR_FIRST_PATTERN, 0);
        if (font_start != -1){
            found = ccString.indexOf(COLOR_LAST_PATTERN, font_start);
            font_end = found + COLOR_LAST_PATTERN.length();

            String data = ccString.substring(font_start, font_end);
            String hexColor = getColorValue(data);
            fontColor = Color.parseColor("#" + hexColor);
            ccString = getCCMessage(data);
        }

        String ccData = ccString;

        italic = 0;
        if(containsItalic(ccData)){
            ccData = ccData.replaceAll(ITALIC_FIRST_PATTERN, "");
            ccData = ccData.replaceAll(ITALIC_LAST_PATTERN, "");
            italic = 1;
        }

        underline = 0;
        if(containsUnderline(ccData)){
            ccData = ccData.replaceAll(UNDERLINE_FIRST_PATTERN, "");
            ccData = ccData.replaceAll(UNDERLINE_LAST_PATTERN, "");
            underline = 1;
        }

        if(ccData.trim().length() == 0){
            location = drawRowText(ccData, 0, fontSize, bgColor, fontColor, 0, 0, row_length, col_length);
            length = location.height;
            return length;
        }

        location = drawRowText(ccData, ccData.length(), fontSize, bgColor, fontColor, italic, underline, row_length, col_length);
        length = location.height;

        return length;
    }

    public Location drawRowText(String text, int textWidth, int textSize, int backColor,
                                int textColor, int italic, int underline, int row_length, int col_length) {

        Location location = new Location(0, 0);
        if (textWidth == 0) {
            location.height = FontHeight;
            return location;
        }

        // Get text dimensions
        TextPaint textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG
                | Paint.LINEAR_TEXT_FLAG);
        textPaint.setTypeface(getDefaultTypeFace());
        textPaint.setStyle(Paint.Style.FILL);

        if (underline == 1) {
            textPaint.setUnderlineText(true);
        }
        if (italic == 1) {
            textPaint.setTextSkewX(-0.25f);
        }

        textPaint.setColor(textColor);
        textPaint.setTextSize(textSize);

        int end = textWidth;
        int start = 0;
        while (end > start) {
            if (Character.isSpaceChar(text.charAt(start))) {
                start++;
            } else {
                break;
            }
        }

        text = text.trim();
        textWidth = text.length();

        Paint.Align align = Paint.Align.LEFT;
        int textXCoordinate = FontWidth * start;
        if (justification == WINDOW_JUSTIFY_CENTER) {
            align = Paint.Align.CENTER;
            textXCoordinate += ((FontWidth * colCount) - 10) / 2 ;
        } else if (justification == WINDOW_JUSTIFY_RIGHT) {
            align = Paint.Align.RIGHT;
            textXCoordinate += (FontWidth * colCount) - 20;
        }
        textPaint.setTextAlign(align);

        Rect rect = new Rect();
        textPaint.getTextBounds(text, 0, textWidth, rect);
        int width = rect.width() + 20;

        StaticLayout mTextLayout = new StaticLayout(text, textPaint,
                width, Layout.Alignment.ALIGN_CENTER, 1.0f, 1.0f, true);

        mCanvas.save();
        mCanvas.translate(marginLeft + col_length + textXCoordinate, marginTop + row_length);

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG
                | Paint.LINEAR_TEXT_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(backColor);

        int numberOfTextLines = mTextLayout.getLineCount();
        for (int i = 0; i < numberOfTextLines; i++) {
            mTextLayout.getLineBounds(i, rect);
            int rect_width = 0;
            if (justification == WINDOW_JUSTIFY_CENTER) {
                rect_width = (rect.width() - 10) / 2;
            } else if (justification == WINDOW_JUSTIFY_RIGHT) {
                rect_width = rect.width() - 20;
            }
            rect.set(rect.left - rect_width, rect.top, rect.right - rect_width, rect.bottom);
            mCanvas.drawRect(rect, paint);
        }

        // Draw text
        mTextLayout.draw(mCanvas);
        mCanvas.restore();

        location.width = mTextLayout.getWidth();
        location.height = mTextLayout.getHeight();

        return location;
    }

    public void drawWindow(CCWindow window) {

        if(window.text == null) return;

        mCanvas.save();

        Rect rect = new Rect();

        // Draw background
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG
                | Paint.LINEAR_TEXT_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(window.backGroundColor);

        int left = window.marginLeft;
        int top = window.marginTop;
        int right = window.marginLeft + FontWidth*window.colCount;
        if(right > mCCSurfaceTextureView.getWidth() - SafeWidth)
            right = mCCSurfaceTextureView.getWidth() - SafeWidth;
        int bottom = window.marginTop + FontHeight*window.rowCount;
        if(bottom > mCCSurfaceTextureView.getHeight() - SafeHeight)
            bottom = mCCSurfaceTextureView.getHeight() - SafeHeight;

        rect.set(left, top, right, bottom);
        mCanvas.drawRect(rect, paint);

        mCanvas.restore();
    }

    public void drawCCText(CCWindow window) {

        if(window.text == null) return;

        Pattern p = Pattern.compile(BR_PATTERN);
        String[] attr = p.split(window.text);
        int length = 0;
        for(int i= 0; i<attr.length; i++){
            //Log.i(TAG, "string[" + i +"]:" +attr[i]);
            String cc = attr[i];

            length += printCCMessage(cc, length);

        }
    }

    private String getBGColorValue(String msg){

        int index = msg.lastIndexOf(BG_COLOR_FIRST_PATTERN);
        index += BG_COLOR_FIRST_PATTERN.length();

        String subString = msg.substring(index);
        index = subString.indexOf(DELIMETER);

        String val = subString.substring(0, index);
        val = val.substring(val.indexOf(QUOTATION_MARK)+1, val.lastIndexOf(QUOTATION_MARK));

        return val;
    }

    private String getColorValue(String msg){

        int index = msg.lastIndexOf(COLOR_FIRST_PATTERN);
        index += COLOR_FIRST_PATTERN.length();

        String subString = msg.substring(index);
        index = subString.indexOf(DELIMETER);

        String val = subString.substring(0, index);
        val = val.substring(val.indexOf(QUOTATION_MARK)+1, val.lastIndexOf(QUOTATION_MARK));

        return val;
    }

    private String getCCMessageBGColor(String msg){

        int index = msg.lastIndexOf(BG_COLOR_FIRST_PATTERN);
        index += BG_COLOR_FIRST_PATTERN.length();

        String subString = msg.substring(index);
        index = subString.indexOf(DELIMETER);

        String val = subString.substring(index);
        index = val.lastIndexOf(BG_COLOR_LAST_PATTERN);
        if(index == -1){
            val = val.substring(val.indexOf(DELIMETER)+1);
        }else{
            val = val.substring(val.indexOf(DELIMETER)+1, val.lastIndexOf(BG_COLOR_LAST_PATTERN));
        }


        return val;
    }

    private String getCCMessage(String msg){

        int index = msg.lastIndexOf(COLOR_FIRST_PATTERN);
        index += COLOR_FIRST_PATTERN.length();

        String subString = msg.substring(index);
        index = subString.indexOf(DELIMETER);

        String val = subString.substring(index);
        index = val.lastIndexOf(COLOR_LAST_PATTERN);
        if(index == -1){
            val = val.substring(val.indexOf(DELIMETER)+1);
        }else{
            val = val.substring(val.indexOf(DELIMETER)+1, val.lastIndexOf(COLOR_LAST_PATTERN));
        }


        return val;
    }

    public int drawLayoutHeight(String text, int textWidth, int textSize) {

        // Get text dimensions
        TextPaint textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG
                | Paint.LINEAR_TEXT_FLAG);
        textPaint.setTypeface(getDefaultTypeFace());
        textPaint.setStyle(Paint.Style.FILL);

        textPaint.setTextSize(textSize);

        Rect rect = new Rect();
        textPaint.getTextBounds(text, 0, textWidth, rect);

        int width = rect.width() + 20;

        StaticLayout mTextLayout = new StaticLayout(text, textPaint,
                width, Layout.Alignment.ALIGN_NORMAL, 1.0f, 1.0f, true);

        return mTextLayout.getHeight();
    }

    public int drawLayoutWidth(String text, int textWidth, int textSize) {

        // Get text dimensions
        TextPaint textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG
                | Paint.LINEAR_TEXT_FLAG);
        textPaint.setTypeface(getDefaultTypeFace());
        textPaint.setStyle(Paint.Style.FILL);

        textPaint.setTextSize(textSize);

        Rect rect = new Rect();
        textPaint.getTextBounds(text, 0, textWidth, rect);
        int width = rect.width();

        StaticLayout mTextLayout = new StaticLayout(text, textPaint,
                width, Layout.Alignment.ALIGN_NORMAL, 1.0f, 1.0f, true);

        return mTextLayout.getWidth();
    }

    private void clearWindows(){
        CCWindow window = null;
        for (int i = 0; i < Windows.size(); i++) {
            window = Windows.get(i);
            window.text = null;
        }
    }

    private CCWindow getWindow(int windowId){
        CCWindow window = null;

        for (int i = 0; i < Windows.size(); i++) {
            window = Windows.get(i);
            window.fontSize = fontSize;
            if(window.windowId == windowId){
                return window;
            }
        }

        return null;
    }

    public void release() {
        active = false;
        if(Windows.size() > 0) Windows.clear();
    }

    //TODO work here~
    public void onUpdateCC(Message msg) {
        try {
            if(!active) return;
            mHandler.sendMessage(Message.obtain(msg));
        } catch (Exception e) {
            Log.e(TAG, "onUpdateCC()" + e.getMessage());
        }
    }


    private class CCDrawHandler extends Handler {

        public void handleMessage(Message msg) {

            if(null == msg || !active) return;

            switch (msg.what) {
                case CLOSE_CAPTION_EVENT: {
                    int windowId = (msg.arg2 & 0xFF00) >> 8;
                    int event = (msg.arg2 & 0xFF);
                    switch (event) {
                        case EVENT_CAPTION_DATA: {
                            Log.i(TAG, "EVENT_CAPTION_DATA" + ", Windows Id:" + windowId);
                            if(!initialized) break;
                            String cc_data = new String((byte[]) msg.obj);
                            drawCloseCaption(windowId, cc_data);
                        }
                        break;
                        case EVENT_DELETE_WINDOW: {
                            Log.i(TAG, "EVENT_DELETE_WINDOW:" + ", Windows Id:" + windowId);
                            CCWindow window = getWindow(windowId);
                            if(window != null){
                                Windows.remove(window);
                                refreshScreen();
                            }
                        }
                        break;
                        case EVENT_CLEAR_WINDOW: {
                            Log.i(TAG, "EVENT_CLEAR_WINDOW:" + ", Windows Id:" + windowId);
                            clearCloseCaption(windowId);
                        }
                        break;
                        case EVENT_WINDOW_COLOR: {
                            try {
                                String hexColor = new String((byte[]) msg.obj);
                                Log.i(TAG, "EVENT_WINDOW_COLOR:" + hexColor + ", Windows Id:" + windowId);
                                backGroundColor = Color.parseColor(hexColor);
                                CCWindow window = getWindow(windowId);
                                if(window != null){
                                    window.backGroundColor = backGroundColor;
                                }

                            } catch (Exception e) {
                                Log.e(TAG, "handleMessage()" + e.getMessage());
                            }
                        }
                        break;
                        case EVENT_WINDOW_ANCHOR: {
                            try {
                                String anchor = new String((byte[]) msg.obj);
                                Log.i(TAG, "EVENT_WINDOW_ANCHOR:" + anchor + ", Windows Id:" + windowId);
                                activeWindowId = DTVCC_WINDOW_INIT;
                                anchorId = Integer.parseInt(anchor);
                                rowPosition = 0;
                                colPosition = 0;
                                CCWindow window = getWindow(windowId);
                                if(window != null){
                                    window.anchorId = anchorId;
                                    window.rowPosition = rowPosition;
                                    window.colPosition = colPosition;
                                    window.text = null;
                                }else{
                                    window = new CCWindow(windowId, anchorId, rowPosition, colPosition);
                                    Windows.add(window);
                                }

                            } catch (Exception e) {
                                Log.e(TAG, "handleMessage()" + e.getMessage());
                            }
                        }
                        break;
                        case EVENT_WINDOW_ROW_COL: {
                            try {
                                String strRowColcount = new String((byte[]) msg.obj);
                                Log.i(TAG, "EVENT_WINDOW_ROW_COL: " + strRowColcount + ", Windows Id:" + windowId);
                                if (strRowColcount.contains(":")) {
                                    int pos = strRowColcount.indexOf(":");
                                    rowCount = Integer.parseInt(strRowColcount.substring(0, pos));
                                    colCount = Integer.parseInt(strRowColcount.substring(pos + 1));
                                    Log.i(TAG, "row: " + rowCount + " ,column:" + colCount);
                                    CCWindow window = getWindow(windowId);
                                    if(window != null){
                                        window.rowCount = rowCount;
                                        window.colCount = colCount;
                                        calculatePosition(windowId);
                                    }
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "handleMessage()" + e.getMessage());
                            }
                        }
                        break;
                        case EVENT_FONT_SIZE: {
                            try {
                                String strFontSize = new String((byte[]) msg.obj);
                                Log.i(TAG, "EVENT_FONT_SIZE: " + strFontSize + ", Windows Id:" + windowId);
                                int font = Integer.parseInt(strFontSize);

                                int font_size = STANDARD_FONT_SIZE;
                                if(font == 0)
                                    font_size = SMALL_FONT_SIZE;
                                else if(font == 1)
                                    font_size = STANDARD_FONT_SIZE;
                                else if(font == 2)
                                    font_size = LARGE_FONT_SIZE;

                                float ratio = (surface_width / CRITERION_SURFACE_WIDTH);
                                font_size = (int)(ratio*font_size);

                                if(fontSize == font_size) break;

                                fontSize = font_size;
                                FontWidth = drawLayoutWidth(TEST_STRING, TEST_STRING.length(), fontSize);
                                FontHeight = drawLayoutHeight(TEST_STRING, TEST_STRING.length(), fontSize);

                                CCWindow window = getWindow(windowId);
                                if(window != null){
                                    window.fontSize = fontSize;
                                    calculatePosition(windowId);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "handleMessage()" + e.getMessage());
                            }
                        }
                        break;
                        case EVENT_WINDOW_POINT: {
                            try {
                                String position = new String((byte[]) msg.obj);
                                Log.i(TAG, "EVENT_WINDOW_POINT: " + position + ", Windows Id:" + windowId);
                                if (position.contains(":")) {
                                    int pos = position.indexOf(":");
                                    anchorTop = Integer.parseInt(position.substring(0, pos));
                                    anchorLeft = Integer.parseInt(position.substring(pos + 1));
                                    Log.i(TAG, "top: " + anchorTop + " ,left:" + anchorLeft);
                                    CCWindow window = getWindow(windowId);
                                    if(window != null){
                                        window.anchorTop = anchorTop;
                                        window.anchorLeft = anchorLeft;
                                    }
                                    initialized = true;
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "handleMessage()" + e.getMessage());
                            }
                        }
                        break;
                        case EVENT_WINDOW_POSITON: {
                            try {
                                String position = new String((byte[]) msg.obj);
                                Log.i(TAG, "EVENT_WINDOW_POSITON: " + position + ", Windows Id:" + windowId);
                                if (position.contains(":")) {
                                    int pos = position.indexOf(":");
                                    rowPosition = Integer.parseInt(position.substring(0, pos));
                                    colPosition = Integer.parseInt(position.substring(pos + 1));
                                    Log.i(TAG, "rowPosition: " + rowPosition + " ,colPosition:" + colPosition);
                                    CCWindow window = getWindow(windowId);
                                    if(window != null){
                                        window.rowPosition = rowPosition;
                                        window.colPosition = colPosition;
                                    }
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "handleMessage()" + e.getMessage());
                            }
                        }
                        break;
                        case EVENT_WINDOW_JUSTIFY: {
                            try {
                                String strJustify = new String((byte[]) msg.obj);
                                Log.i(TAG, "EVENT_WINDOW_JUSTIFY: " + strJustify + ", Windows Id:" + windowId);
                                justification = Integer.parseInt(strJustify);

                                CCWindow window = getWindow(windowId);
                                if(window != null){
                                    window.justification = justification;
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "handleMessage()" + e.getMessage());
                            }
                        }
                        break;
                        case EVENT_TTA_CERT_MODE: {
                            Log.i(TAG, "EVENT_TTA_CERT_MODE");
                        }
                        break;
                    }
                }
                break;
            }

        }
    }

    public void calculatePosition(int windowId) {

        Log.i(TAG, "calculatePosition start active window:" + activeWindowId + " ,windowId:" + windowId);

        if(activeWindowId == windowId) return;

        CCWindow window = getWindow(windowId);
        if(window == null) {
            Log.e(TAG, "CCWindow is null");
            return;
        }

        Log.i(TAG, "calculatePosition start");

        activeWindowId = windowId;

        anchorId = window.anchorId;
        anchorLeft = window.anchorLeft;
        anchorTop = window.anchorTop;
        colCount = window.colCount;
        rowCount = window.rowCount;

        SafeWidth =  (int)(mCCSurfaceTextureView.getWidth() / (double)20); // Safe 영역
        SafeHeight = (int)(mCCSurfaceTextureView.getHeight() / (double)20); // Safe 영역

        surface_width = mCCSurfaceTextureView.getWidth() - SafeWidth*2;
        surface_height = mCCSurfaceTextureView.getHeight() - SafeHeight*2;

        Log.i(TAG, "surface_width: " + surface_width + " ,surface_height:" + surface_height);
        Log.i(TAG, "FontWidth: " + FontWidth + " ,FontHeight:" + FontHeight);

        switch (anchorId)
        {
            case ANCHOR_POINT_TOP_LEFT:
                marginLeft = SafeWidth + (int)((surface_width / (double)DTVCC_SCREENGRID_COLUMNS) * anchorLeft);
                marginTop = SafeHeight + (int)((surface_height / (double)DTVCC_SCREENGRID_ROWS) * anchorTop);
                break;
            case ANCHOR_POINT_TOP_CENTER:
                marginLeft = SafeWidth + (int)((surface_width / (double)DTVCC_SCREENGRID_COLUMNS) * anchorLeft) - (FontWidth*colCount)/2;
                marginTop = SafeHeight + (int)((surface_height / (double)DTVCC_SCREENGRID_ROWS) * anchorTop);
                break;
            case ANCHOR_POINT_TOP_RIGHT:
                marginLeft = SafeWidth + (int)((surface_width / (double)DTVCC_SCREENGRID_COLUMNS) * anchorLeft) - FontWidth*colCount;
                marginTop = SafeHeight + (int)((surface_height / (double)DTVCC_SCREENGRID_ROWS) * anchorTop);
                break;
            case ANCHOR_POINT_MIDDLE_LEFT:
                marginLeft = SafeWidth + (int)((surface_width / (double)DTVCC_SCREENGRID_COLUMNS) * anchorLeft);
                marginTop = SafeHeight + (int)((surface_height / (double)DTVCC_SCREENGRID_ROWS) * anchorTop) - (FontHeight*rowCount)/2;
                break;
            case ANCHOR_POINT_MIDDLE_CENTER:
                marginLeft = SafeWidth + (int)((surface_width / (double)DTVCC_SCREENGRID_COLUMNS) * anchorLeft) - (FontWidth*colCount)/2;
                marginTop = SafeHeight + (int)((surface_height / (double)DTVCC_SCREENGRID_ROWS) * anchorTop) - (FontHeight*rowCount)/2;
                break;
            case ANCHOR_POINT_MIDDLE_RIGHT:
                marginLeft = SafeWidth + (int)((surface_width / (double)DTVCC_SCREENGRID_COLUMNS) * anchorLeft) - FontWidth*colCount;
                marginTop = SafeHeight + (int)((surface_height / (double)DTVCC_SCREENGRID_ROWS) * anchorTop) - (FontHeight*rowCount)/2;
                break;
            case ANCHOR_POINT_BOTTOM_LEFT:
                marginLeft = SafeWidth + (int)(((surface_width / (double)DTVCC_SCREENGRID_COLUMNS) * anchorLeft));
                marginTop = SafeHeight + (int)((surface_height / (double)DTVCC_SCREENGRID_ROWS) * anchorTop) - FontHeight*rowCount;
                break;
            case ANCHOR_POINT_BOTTOM_CENTER:
                marginLeft = SafeWidth + (int)((surface_width / (double)DTVCC_SCREENGRID_COLUMNS) * anchorLeft) - (FontWidth*colCount)/2;
                marginTop = SafeHeight + (int)((surface_height / (double)DTVCC_SCREENGRID_ROWS) * anchorTop) - FontHeight*rowCount;
                break;
            case ANCHOR_POINT_BOTTOM_RIGHT:
                marginLeft = SafeWidth + (int)((surface_width / (double)DTVCC_SCREENGRID_COLUMNS) * anchorLeft) - FontWidth*colCount;
                marginTop = SafeHeight + (int)((surface_height / (double)DTVCC_SCREENGRID_ROWS) * anchorTop) - FontHeight*rowCount;
                break;
        }

        window.marginLeft = marginLeft;
        window.marginTop = marginTop;
        Log.e(TAG, "marginLeft: " + marginLeft + " ,marginTop:" + marginTop);
    }

    public static Typeface getDefaultTypeFace() {
        if (mDefaultTypeFace == null) {
            try {
                mDefaultTypeFace = Typeface.createFromFile("/system/fonts/YDYGO33.ttf");
            } catch (Throwable e) {
                    // PASS
            }
            if (mDefaultTypeFace == null) {
                mDefaultTypeFace = Typeface.DEFAULT;
            }
        }
        return mDefaultTypeFace;
    }


    public class Location {
        public int width;
        public int height;

        public Location (int width, int height){
            this.width = width;
            this.height = height;
        }
    }

    public class CCWindow  {
        public int windowId = 0;
        public int fontSize = STANDARD_FONT_SIZE;
        public int justification = WINDOW_JUSTIFY_LEFT;
        public int rowCount = 3;
        public int colCount = 40;
        public int anchorId = 7;
        public int anchorTop;
        public int anchorLeft;
        public int rowPosition = 0;
        public int colPosition = 0;
        public int marginLeft = 0;
        public int marginTop = 0;
        public int backGroundColor = Color.BLACK;

        public String text = null;

        public CCWindow (int windowId, int anchorId, int rowPosition, int colPosition){
            this.windowId = windowId;
            this.anchorId = anchorId;
            this.rowPosition = rowPosition;
            this.colPosition = colPosition;
        }
    }
}
