
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.media;

/**
 * DmxFilter class can be used to request filter section data
 *
 * @author Kwon Soon Chan (sysc0507@sk.com)
 */
public class DmxFilter {
    public int pid;
    public int tid;

    /**
     * DmxFilter Default constructor.
     *
     * @param pid pid
     * @param tid tid
     */
    public DmxFilter(int pid, int tid) {
        this.pid = pid;
        this.tid = tid;
    }
}
