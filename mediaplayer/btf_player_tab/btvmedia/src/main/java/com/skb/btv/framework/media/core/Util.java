
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.media.core;

/**
 * Util class can be used to handle for etc utils
 *
 * @author Kwon Soon Chan (sysc0507@sk.com)
 */
public class Util {
    public static String printStackTrace() {
        StringBuffer sb = new StringBuffer();
        StackTraceElement[] stackTraceElement = new Exception().getStackTrace();

        for( StackTraceElement element : stackTraceElement ){
            sb.append( element.toString() + "\n" );
        }

        return sb.toString();
    }
}
