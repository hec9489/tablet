
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.media.core;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;

import static org.xmlpull.v1.XmlPullParser.END_TAG;
import static org.xmlpull.v1.XmlPullParser.START_TAG;
import static org.xmlpull.v1.XmlPullParser.TEXT;

/**
 * Status class can be used to handle for player Status
 *
 * @author Kwon Soon Chan (sysc0507@sk.com)
 */

class Status {
    private Status() {}

    private String eventName;

    public static Status parse(String xml) {
        Status status = null;
        XmlPullParserFactory factory = null;
        try {
            factory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(new StringReader(xml));
            int eventType = parser.getEventType();
            String curTag = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case START_TAG: {
                        curTag = parser.getName();
                        if (parser.getName().equals("event")) {
                            String eventName = parser.getAttributeValue(null, "name");
                            if (eventName != null) {
                                if (eventName.equals("RTSPStatus")) {
                                    status = new StatusError();
                                } else if (eventName.equals("PlayerStatusChanged")) {
                                    status = new StatusPlay();
                                } else if (eventName.equals("SizeChanged")) {
                                    status = new StatusSize();
                                } else {
                                    status = new Status();
                                }
                            } else {
                                status = new Status();
                            }
                            status.eventName = eventName;
                        }
                    }
                    break;
                    case END_TAG: {
                        curTag = null;
                    }
                    break;

                    case TEXT: {
                        if(curTag == null) {
                            eventType = parser.next();
                            continue;
                        }

                        if (curTag.equals("ErrCode")) {
                            ((StatusError) status).errorCode = parser.getText().trim() == null || parser.getText().trim().length() == 0 ? 0 : Integer.parseInt(parser.getText().trim());
                        } else if (curTag.equals("IPAddress")) {
                            ((StatusError) status).ip = parser.getText().trim();
                        } else if (curTag.equals("Port")) {
                            ((StatusError) status).port = parser.getText().trim() == null || parser.getText().trim().length() == 0 ? 0 : Integer.parseInt(parser.getText());
                        } else if (curTag.equals("sWindow")) {
                            ((StatusPlay) status).window = parser.getText().trim();
                        } else if (curTag.equals("sPlayerMode")) {
                            ((StatusPlay) status).mode = parser.getText().trim();
                        } else if (curTag.equals("sPlaybackStatus")) {
                            ((StatusPlay) status).playbackStatus = parser.getText().trim();
                        } else if (curTag.equals("dContentSize")) {
                            ((StatusPlay) status).duration = parser.getText().trim() == null || parser.getText().trim().length() == 0 ? 0 : Long.parseLong(parser.getText().trim());
                        } else if (curTag.equals("dCurrentPositionInMiliSec")) {
                            ((StatusPlay) status).time = parser.getText().trim() == null || parser.getText().trim().length() == 0 ? 0 : Long.parseLong(parser.getText().trim());
                        } else if (curTag.equals("dCurrentPositionKiloByte")) {
                            ((StatusPlay) status).position = parser.getText().trim() == null || parser.getText().trim().length() == 0 ? 0 : Long.parseLong(parser.getText().trim());
                        } else if (curTag.equals("sLastURL")) {
                            ((StatusPlay) status).lastURL = parser.getText().trim();
                        }
                    }
                    break;
                }

                eventType = parser.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return status;
    }

    public String getEventName() {
        return eventName;
    }

    static class StatusError extends Status {
        private int errorCode;
        private String ip;
        private int port;

        public int getErrorCode() {
            return errorCode;
        }

        public String getIp() {
            return ip;
        }

        public int getPort() {
            return port;
        }
    }

    static class StatusPlay extends Status {
        private String window;
        private String mode;
        private String playbackStatus;
        private long duration;
        private long position;
        private long time;
        private String lastURL;

        public String getWindow() {
            return window;
        }

        public String getPlaybackStatus() {
            return playbackStatus;
        }

        public long getDuration() {
            return duration;
        }

        public long getPosition() {
            return position;
        }

        public long getTime() {
            return time;
        }

        public String getLastURL() {
            return lastURL;
        }

        public String getMode() {
            return mode;
        }
    }

    static class StatusSize extends Status {
    }
}
