
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.media;

import android.content.Context;
import android.media.AudioTrack;
import android.util.Log;

import com.skb.btv.framework.media.core.MediaPlayer;

import java.util.ArrayList;


/**
 * DvbServicePlayer class can be used to control playback For Iptv (btv live stream, file content)
 *
 * @author Kwon Soon Chan (sysc0507@sk.com)
 */

public class DvbServicePlayer extends MediaPlayer {

    /**
     * Interface definition for a callback to be invoked when the media
     * source is ready for playback.
     */
    public interface OnPreparedListener {
        /**
         * Called when the media file is ready for playback.
         *
         * @param mp the DvbServicePlayer that is ready for playback
         */
        void onPrepared(DvbServicePlayer mp);
    }

    /**
     * Interface definition of a callback to be invoked to communicate some
     * info and/or warning about the media or its playback.
     */
    public interface OnInfoListener {
        /**
         * Called to indicate an info or a warning.
         *
         * @param mp the DvbServicePlayer the info pertains to.
         * @param what the type of info or warning.
         * @param extra an extra code, specific to the info.
         */
        boolean onInfo(DvbServicePlayer mp, int what, int extra);
    }

    /**
     * Interface definition of a callback to be invoked when there
     * has been an error during an asynchronous operation (other errors
     * will throw exceptions at method call time).
     */
    public interface OnErrorListener {
        /**
         * Called to indicate an error.
         *
         * @param mp the DvbServicePlayer the error pertains to
         * @param what the type of error that has occurred
         * @param extra an extra code, specific to the error.
         */
        boolean onError(DvbServicePlayer mp, int what, int extra);
    }

    /**
     * Interface definition for a callback to be invoked when playback of
     * a media source has completed.
     */
    public interface OnCompletionListener {
        /**
         * Called when the end of a media source is reached during playback.
         *
         * @param mp the DvbServicePlayer that reached the end of the file
         */
        void onCompletion(DvbServicePlayer mp);
    }

    /**
     * Interface definition of a callback to be invoked when receiving of
     * a demux filter data given tid, pid
     */
    public interface OnDmxFilterListener  {
        /**
         * Called to indicate the receiving of a filter operation.
         *
         * @param mp the DvbServicePlayer that issued the filter operation
         * @param sectiondata a filter data
         */
        void onDmxFilter(DvbServicePlayer mp,  byte[] sectiondata);
    }

    private OnPreparedListener onPreparedListener;
    private OnInfoListener onInfoListener;
    private OnErrorListener onErrorListener;
    private OnCompletionListener onCompletionListener;
    private OnDmxFilterListener  onDmxFilterListener;

    private MediaPlayer.OnPreparedListener onMediaPlayerPreparedListener;
    private MediaPlayer.OnInfoListener onMediaPlayerInfoListener;
    private MediaPlayer.OnErrorListener onMediaPlayerErrorListener;
    private MediaPlayer.OnCompletionListener onMediaPlayerCompletionListener;
    private MediaPlayer.OnDmxFilterListener  onMediaPlayerDmxFilterListener;

    /**
     * DvbServicePlayer Default constructor.
     * <p>
     * Same as {@link #DvbServicePlayer(Context, int)} with {@code pipId = 0}.
     *
     * @param context the Context to use
     *
     * <p>When done with the DvbServicePlayer, you should call  {@link #release()},
     * to free the resources. If not released, too many DvbServicePlayer instances may
     * result in an exception.</p>
     */
    public DvbServicePlayer(Context context) {
        super(context);
        initialize();
    }

    /**
     * DvbServicePlayer constructor for a given pip id player.
     *
     * @param context the Context to use
     * @param pipId indicate main(0) player or pip(1) player
     *
     * <p>When done with the DvbServicePlayer, you should call  {@link #release()},
     * to free the resources. If not released, too many DvbServicePlayer instances may
     * result in an exception.</p>
     */
    public DvbServicePlayer(Context context, int pipId) {
        super(context, pipId);
        initialize();
    }

    /**
     * Register a callback to be invoked when the media source is ready
     * for playback.
     *
     * @param listener the callback that will be run
     */
    public void setOnPreparedListener(OnPreparedListener listener) {
        onPreparedListener = listener;
        if(listener != null)
            super.setOnPreparedListener(onMediaPlayerPreparedListener);
        else
            super.setOnPreparedListener(null);
    }

    /**
     * Register a callback to be invoked when an info/warning is available.
     *
     * @param listener the callback that will be run
     */
    public void setOnInfoListener(OnInfoListener listener) {
        onInfoListener = listener;
        if(listener != null)
            super.setOnInfoListener(onMediaPlayerInfoListener);
        else
            super.setOnInfoListener(null);
    }

    /**
     * Register a callback to be invoked when an error has happened
     * during an asynchronous operation.
     *
     * @param listener the callback that will be run
     */
    public void setOnErrorListener(OnErrorListener listener) {
        onErrorListener = listener;
        if(listener != null)
            super.setOnErrorListener(onMediaPlayerErrorListener);
        else
            super.setOnErrorListener(null);
    }

    /**
     * Register a callback to be invoked when the end of a media source
     * has been reached during playback.
     *
     * @param listener the callback that will be run
     */
    public void setOnCompletionListener(OnCompletionListener listener) {
        onCompletionListener = listener;
        if(listener != null)
            super.setOnCompletionListener(onMediaPlayerCompletionListener);
        else
            super.setOnCompletionListener(null);
    }

    /**
     * Register a callback to be invoked when demux filter data received
     *
     * @param listener the callback that will be run
     */
    public void setDmxFilterListener(OnDmxFilterListener listener){
        onDmxFilterListener = listener;
        if(listener != null)
            super.setDmxFilterListener(onMediaPlayerDmxFilterListener);
        else
            super.setDmxFilterListener(null);
    }

    /**
     * Sets the data source as a content Uri.
     *
     * <p>iptv play uri started with skbiptv://..
     * <p>file play uri started with skbfile://..
     * <p>multi-view play uri started with skbsmv://..
     *
     * @param strUri the Content URI of the data you want to play
     */
    public void tune(String strUri){
        super.setDataSource(strUri);
    }

    /**
     * Returns an array of audio track information.
     *
     * @return Array of {@link AudioTrack}. The total number of audio tracks is the array length.
     *
     * @see com.skb.btv.framework.media.core.MediaPlayer.AudioTrack
     */
    public AudioTrack[] getAudioTrack () {
        return super.getAudioTrack();
    }

    /**
     * Changes to the given audio track.
     * <p>
     * Set audio focus for each track of multi-audio track.
     * </p>
     *
     * @param index the index of the track to be selected. The valid range of the index
     * is 0..total number of track - 1. The total number of tracks as well as the type of
     * each individual track can be found by calling {@link #getAudioTrack()} method.
     *
     * @see com.skb.btv.framework.media.DvbServicePlayer#getAudioTrack
     */
    public void setAudioTrack(int index) {
        super.setAudioTrack(index);
    }

    /**
     * Starts demux filter operation for given tid, pid
     * @param filter {@link DmxFilter} request filter data for tid, pid
     *
     * @see com.skb.btv.framework.media.DmxFilter
     */
    public void startDmxFilter(DmxFilter filter){
        super.startDmxFilter(filter);
    }

    /**
     * Stops demux filter operation for given tid, pid
     * @param filter {@link DmxFilter} request filter data for tid, pid
     *
     * @see com.skb.btv.framework.media.DmxFilter
     */
    public void stopDmxFilter(DmxFilter filter){
        super.stopDmxFilter(filter);
    }

    /**
     * Releases resources associated with this DvbServicePlayer object.
     *
     */
    public void release() {
        super.setOnCompletionListener(null);
        super.setOnErrorListener(null);
        super.setOnInfoListener(null);
        super.setOnPreparedListener(null);
        super.setDmxFilterListener(null);
        super.release();

        onMediaPlayerPreparedListener = null;
        onMediaPlayerInfoListener = null;
        onMediaPlayerErrorListener = null;
        onMediaPlayerCompletionListener = null;
        onMediaPlayerDmxFilterListener = null;
    }

    /**
     * STBID 설정 API
     * @param stbId STBID
     * @return
     */
    public int setStbId(String stbId){
        return super.setStbId(stbId);
    }

    /**
     *  MAC_ADDR 설정 API
     * @param macAddr
     * @return
     */
    public int setMacAddress(String macAddr){
        return super.setMacAddress(macAddr);
    }

    /**
     * GW_Token 설정 API
     * @param gwToken
     * @return
     */
    public int setGwToken(String gwToken){
        return super.setGwToken(gwToken);
    }

    /**
     * LastFrame 설정 API
     * @param keep  the mode indicating last frame of video be showing.
     *              Use true if one wants to show the last frame of video.
     */
    public void setKeepLastFrame(boolean keep) {
        super.setKeepLastFrame(keep);
    }

    /**
     * 화면해설 설정 API
     * @param visionImpaired
     * @return
     */
    public int setVisionImpaired(boolean visionImpaired){
        return super.setVisionImpaired(visionImpaired);
    }

    /**
     * 자막방송 설정 API
     * @return
     */
    public int setHearingImpaired(boolean hearingImpaired){
        return super.setHearingImpaired(hearingImpaired);
    }

    /**
     * Scaling_mode 설정 API
     * @param scalingMode
     * @return
     */

    public int setVideoScalingMode(int scalingMode){
        return super.setVideoScalingMode(scalingMode);
    }

    /**
     * MLR Activation Enable API
     */

    public boolean getMlrActivationEnable(){
        return super.getMlrActivationEnable();
    }

    public int setMlrActivationEnable(boolean enable){
        return super.setMlrActivationEnable(enable);
    }

    /**
     * TTA 테스트 옵션 설정 API
     * @param key
     * @param value
     * @return
     */
    public int setTtaTestOption(String key, String value) {
        return super.setTtaTestOption(key, value);
    }

    public int setAVOffset(int type, int nTimeMs) {
        return super.setAVOffset(type, nTimeMs);
    }

    private void initialize(){
        onMediaPlayerPreparedListener = new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                Log.i(TAG_IPTV, "DvbServicePlayer onPrepared");
                if(onPreparedListener != null)  onPreparedListener.onPrepared(DvbServicePlayer.this);
            }
        };

        onMediaPlayerInfoListener = new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                //Log.i(TAG_IPTV, "DvbServicePlayer onInfo");
                if(onInfoListener != null) onInfoListener.onInfo(DvbServicePlayer.this, what, extra);
                return false;
            }
        };

        onMediaPlayerErrorListener = new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.i(TAG_IPTV, "DvbServicePlayer onError");
                if(onErrorListener != null) onErrorListener.onError(DvbServicePlayer.this, what, extra);
                return false;
            }
        };

        onMediaPlayerCompletionListener = new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Log.i(TAG_IPTV, "DvbServicePlayer onCompletion");
                if(onCompletionListener != null) onCompletionListener.onCompletion(DvbServicePlayer.this);
            }
        };

        onMediaPlayerDmxFilterListener = new MediaPlayer.OnDmxFilterListener() {
            @Override
            public void onDmxFilter(MediaPlayer mp, byte[] sectiondata) {
                Log.i(TAG_IPTV, "DvbServicePlayer onDmxFilter");
                if(onDmxFilterListener != null) onDmxFilterListener.onDmxFilter(DvbServicePlayer.this, sectiondata);
            }
        };
    }
}
