
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.media.core;


import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.TextureView;
import android.view.View;

import com.skb.btv.framework.media.DmxFilter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * IptvMediaPlayer class can be used to Iptv play
 *
 * @author Kwon Soon Chan (sysc0507@sk.com)
 */

class IptvMediaPlayer {
    static
    {
        System.loadLibrary("btf_media_jni");
        native_init();
    }

    private final static String TAG = "BTVMEDIA_IptvMediaPlayer";

    // IptvMediaPlayer Error
    public final static int     IPTV_ERR_SUCCESS		= 0;
    public final static int     IPTV_ERR_ERROR			= -1;
    public final static int     IPTV_ERR_BADFILENAME	= -2;
    public final static int     IPTV_ERR_INPROGRESS		= -3;
    public final static int     IPTV_ERR_INUSE			= -4;
    public final static int     IPTV_ERR_INVALID		= -5;
    public final static int     IPTV_ERR_NOTCONN		= -6;
    public final static int     IPTV_ERR_NOTSUP			= -7;
    public final static int     IPTV_ERR_TIMEOUT		= -8;
    public final static int     IPTV_ERR_NOTEXIST		= -9;
    public final static int     IPTV_ERR_INVALIDSTATUS	= -10;
    public final static int     IPTV_ERR_INVALIDFORMAT	= -11;
    public final static int     IPTV_ERR_UNKNOWN		= -12;

    // IptvMediaPlayer ID
    public final static int     IPTV_DEVICE_MAIN            = 0;
    public final static int     IPTV_DEVICE_PIP             = 1;

    // IptvMediaPlayer Mode
    public final static int     IPTV_MODE_NONE		= 0;
    public final static int     IPTV_MODE_FILE		= 1;
    public final static int     IPTV_MODE_VOD		= 2;
    public final static int     IPTV_MODE_TUNE		= 3;


    // IptvMediaPlayer Status
    public final static int     IPTV_STATUS_NONE		= 0;
    public final static int     IPTV_STATUS_TUNE		= 1;
    public final static int     IPTV_STATUS_PLAY		= 1;
    public final static int     IPTV_STATUS_STOP		= 2;
    public final static int     IPTV_STATUS_PAUSE		= 3;
    public final static int     IPTV_STATUS_2XFF		= 4;
    public final static int     IPTV_STATUS_4XFF		= 5;
    public final static int     IPTV_STATUS_8XFF		= 6;
    public final static int     IPTV_STATUS_16XFF		= 7;
    public final static int     IPTV_STATUS_2XBW		= 8;
    public final static int     IPTV_STATUS_4XBW		= 9;
    public final static int     IPTV_STATUS_8XBW		= 10;
    public final static int     IPTV_STATUS_16XBW		= 11;

    // IptvMediaPlayer Status
    public final static int     IPTV_VOD_TRICK_1XFF		= 0;
    public final static int     IPTV_VOD_TRICK_0_8XFF		= 1;
    public final static int     IPTV_VOD_TRICK_1_2XFF		= 2;
    public final static int     IPTV_VOD_TRICK_1_5XFF		= 3;
    public final static int     IPTV_VOD_TRICK_2XFF		= 4;
    public final static int     IPTV_VOD_TRICK_4XFF		= 5;
    public final static int     IPTV_VOD_TRICK_8XFF		= 6;
    public final static int     IPTV_VOD_TRICK_16XFF	= 7;
    public final static int     IPTV_VOD_TRICK_2XBW		= 8;
    public final static int     IPTV_VOD_TRICK_4XBW		= 9;
    public final static int     IPTV_VOD_TRICK_8XBW		= 10;
    public final static int     IPTV_VOD_TRICK_16XBW	= 11;

    // IptvMediaPlayer audio
    public final static int     IPTV_AUDIO_OFF			= 0;
    public final static int     IPTV_AUDIO_ON			= 1;

    // IptvMediaPlayer Disconnect
    public final static int     IPTV_DISCONNECT_BY_SERVICE_CRASHED	= 0;
    public final static int     IPTV_DISCONNECT_BY_OTHER_OPEN		= 1;

    private int mNativeContext;
    private int mListener;
    private int mDeviceId;
    private int mNativeSurfaceTexture;
    private int mNativeHandle;
    private int mDsmccListener;
    private SurfaceHolder mSurfaceHolder = null;

    private int mReqeustTrick;
    private int mCurrentTrick;
    private boolean isTrickOnExecute = false;

    private long mReqeustSeek;
    private long mCurrentSeek;
    private boolean mReqeustPause;
    private boolean mCurrentPause;
    private boolean isSeekOnExecute = false;

    private boolean mTryBindConnection = false;

    private boolean regOnDisconnectCallback = false;
    private boolean regOnPlayStatusCallback = false;
    private boolean regOnSectionFilterCallback = false;
    private boolean regOnPidChangedCallback = false;
    private boolean regOnSeekCompletionCallback = false;
    private boolean regOnTuneEventCallback = false;
    private boolean regOnUpdateCCCallback = false;

    private EventHandler mEventHandler;
    private CloseCaption closeCaption;

    private OnSectionFilterListener mOnSectionFilterListener;
    private OnPlayStatusListener mOnPlayStatusListener;
    private OnTuneEventListener mOnTuneEventListener;
    private OnDisconnectedListener mOnDisconnectedListener;
    private OnPidChangedListener mOnPidChangedListener;
    private OnSeekCompletionListener mOnSeekCompletionListener;

    private int mX = 0, mY = 0, mWidth = 0, mHeight = 0;

    private String CAS_FOLDER_PATH =  "/btf/btvmedia/cas/";
    private String CONFIG_FOLDER_PATH = "/btf/config/";
    private String ASSERT_CONFIG_FOLDER_PATH = "btf/config/";
    private String SERVER_LIST_CONFIG_FILENAME = "server-list.conf";
    private String MLR_CLIENT_CONFIG_FOLDER_PATH = "btf/btvmedia/mlr_client/";
    private String MLR_CLIENT_FOLDER_PATH =  "/btf/btvmedia/mlr_client/";
    private String MLR_CONFIG_FILENAME = "mlrc.conf";
    private String TTA_CHANNEL_CONFIG_FOLDER_PATH = "btf/btvmedia/tta_channel/";
    private String TTA_CHANNEL_CONFIG_PATH = "/btf/btvmedia/tta_channel/";
    private String TTA_CHANNEL_FILENAME = "TTAchannel.json";

    private static native final void native_init();
    private native final void native_setup(Object iptv_mediaplayer_this, int deviceID, String internalPath);
    private native final void native_release();
    private native final int native_invoke(Parcel request, Parcel reply);
    public native int _filePlay(Surface surface, String path, int vpid, int vcodec, int apid, int acodec) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native int _open(String dataXML) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native int _tuneTV(String dataXML) throws Exception;
    private native int _closeTV() throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native int _bindingFilter(String dataXML) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native int _releaseFilter(String dataXML) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native int _changeAudioChannel(String dataXML) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native int _changeAudioOnOff(String dataXML) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native int _getPlayerMode();
    private native int _getPlayerStatus();
    private native int _getCurrentPositionInSec();
    private native int _setWindowSize(String dataXML) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native int _play() throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native int _pause() throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native int _resume() throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native int _seek(String dataXML, boolean pause) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native int _trick(String dataXML) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native int _pauseAt(String dataXML) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native int _keepLastFrame(boolean flag) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native int _stop() throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native int _close() throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native void _setIptvVideoSurface(Surface surface);
    private native int _startDmxFilter(int pid, int tid);
    private native int _stopDmxFilter(int pid, int tid);
    private native int _setPlayerSize(int nLeft, int nTop, int nWidth, int nHeight) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native int _setDummys(String dummys) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException;
    private native int _setStbId(String stbId);
    private native int _setMacAddress(String macAddr);
    private native int _setGwToken(String gwToken);
    private native int _setVisionImpaired(boolean visionImpaired);
    private native int _setHearingImpaired(boolean hearingImpaired);
    private native int _setVideoScalingMode(int scalingMode);
    private native boolean _getMlrActivationEnable();
    private native int _setMlrActivationEnable(boolean enable);
    private native int _setTtaTestOption(String key, String value);
    private native int _setAVOffset(int type, int nTimeMs);

    IptvMediaPlayer(int playerId, Context context) {
        Looper looper;
        if ((looper = Looper.myLooper()) != null) {
            mEventHandler = new EventHandler(this, looper);
        } else if ((looper = Looper.getMainLooper()) != null) {
            mEventHandler = new EventHandler(this, looper);
        } else {
            mEventHandler = null;
        }

        mDeviceId = playerId;
        isTrickOnExecute = false;
        isSeekOnExecute = false;

        /*
         * Native setup requires a weak reference to our object.
         * It's easier to create it here than in C++.
         */

        initMakeFolderAndFile(context);

        native_setup(new WeakReference<IptvMediaPlayer>(this), playerId, context.getDataDir().getPath());

		//TTA kecmode on/off depend on TTAchannel.json exist in /storage/emulated/0/TTAchannel.json
        File tta_channel_file_external = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + TTA_CHANNEL_FILENAME);

        if(tta_channel_file_external.exists()) {
            Log.d(TAG, "tta_channel_file_external is existes => " + tta_channel_file_external.getPath());
            _setTtaTestOption("vendor.skb.btv.manufacturer.kec", "1");
        } else {
            Log.d(TAG, "tta_channel_file_external is not existes => " + tta_channel_file_external.getPath());
            _setTtaTestOption("vendor.skb.btv.manufacturer.kec", "-1");
        }
    }

    public static IptvMediaPlayer connect(int deviceID, Context context) {
        return new IptvMediaPlayer(deviceID, context);
    }

    public static IptvMediaPlayer connect(Context context) {
        return new IptvMediaPlayer(IPTV_DEVICE_MAIN, context);
    }

    public void disconnect() {
        Log.v(TAG, "disconnect");
        if(closeCaption != null) {
            closeCaption.release();
            closeCaption = null;
        }
        native_release();
    }

    protected void finalize() {
        Log.v(TAG, "finalize");
        //native_release();
    }

    public Parcel newRequest() {
        Parcel parcel = Parcel.obtain();
        //parcel.writeInterfaceToken(IIPTV_MEDIA_PLAYER);
        return parcel;
    }

    public void invoke(Parcel request, Parcel reply) {
        int retcode = native_invoke(request, reply);
        if(isSeekOnExecute){
            reply.setDataPosition(0);
            reply.writeLong(((mReqeustSeek) * 90) / 2);
            Log.v(TAG, "invoke replace pts=" + ((mReqeustSeek) * 90) / 2);
        }
        reply.setDataPosition(0);
        if (retcode != 0) {
            throw new RuntimeException("failure code: " + retcode);
        }
    }


    public int open(IptvPlayInfo playInfo) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        String dataXML;
        dataXML = String.format("%d;%d;%d;%d;%d;%s;%s;%s;%s;%s;%d;",
                playInfo.GetPlayMode(),
                playInfo.GetPlayType(),
                playInfo.getDRMType(),
                playInfo.GetDurationSec(),
                playInfo.GetResumeSec(),
                playInfo.GetPath(),
                playInfo.GetMetaInfo(),
                playInfo.GetContentID(),
                playInfo.GetOTPID(),
                playInfo.GetOTPPasswd(),
                playInfo.getLastFrame()
        );

		/*if (mGLIPTVSurfaceView != null) {
			mGLIPTVSurfaceView.setVisibility(View.VISIBLE);
		}*/

        return _open(dataXML);
    }

    public int play() throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        return _play();
    }

    public int stop() throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        return _stop();
    }

    public int close() throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
		/*if (mGLIPTVSurfaceView != null)
		{
			mGLIPTVSurfaceView.setVisibility(View.GONE);
		}*/

        return _close();
    }

    public int pause() throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        return _pause();
    }

    public int resume() throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        return _resume();
    }

    public int seek(long mSec, boolean pause) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        Log.e(TAG, "seek start :" + mSec);
        if(isSeekOnExecute){
            mReqeustSeek = mSec;
            mReqeustPause = pause;
            Log.e(TAG, "OnSeekOnExecute seek :" + mReqeustSeek + "pause :" + mReqeustPause);
            return 0;
        }
        mReqeustSeek = mSec;
        mReqeustPause = pause;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    isSeekOnExecute = true;
                    do {
                        mCurrentSeek = mReqeustSeek;
                        mCurrentPause = mReqeustPause;
                        Log.e(TAG, "Request seek :" + mCurrentSeek  + "pause :" + mCurrentPause);
                        String dataXML;
                        dataXML = String.format("%d;", mCurrentSeek);
                        _seek(dataXML, mCurrentPause);
                        Log.e(TAG, "Request seek end" );
                    } while(mCurrentSeek != mReqeustSeek);
                    isSeekOnExecute = false;
                    Log.e(TAG, "Request seek execute end" );
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

        return 0;
    }

    public int trick(int nTrick) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        if (nTrick < IPTV_VOD_TRICK_1XFF || nTrick > IPTV_VOD_TRICK_16XBW)
            return IPTV_ERR_INVALID;

        Log.e(TAG, "trick start :" + nTrick);
        if(isTrickOnExecute){
            mReqeustTrick = nTrick;
            Log.e(TAG, "OnTrickOnExecute trick :" + mReqeustTrick);
            return 0;
        }
        mReqeustTrick = nTrick;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    isTrickOnExecute = true;
                    do {
                        mCurrentTrick = mReqeustTrick;
                        Log.e(TAG, "Request trick :" + mCurrentTrick);
                        String dataXML;
                        dataXML = String.format("%d;", mCurrentTrick);
                        _trick(dataXML);
                        Log.e(TAG, "Request trick end" );
                    } while(mCurrentTrick != mReqeustTrick);
                    isTrickOnExecute = false;
                    Log.e(TAG, "Request trick execute end" );
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        return 0;
    }

    public int pauseAt(long mSec) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        String dataXML;
        dataXML = String.format("%d;", mSec);
        Log.e(TAG, "pauseAt start :" + dataXML);
        return _pauseAt(dataXML);
    }

    public int setKeepLastFrame(boolean flag) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        return _keepLastFrame(flag);
    }

    public int getPlayerMode() {
        return _getPlayerMode();
    }

    public void setCCDisplay(TextureView textureView) {
        if(closeCaption == null) {
            closeCaption = new CloseCaption();
        }
        closeCaption.setCCDisplay(textureView);
    }

    public int setStbId(String stbId) {
        Log.d(TAG, "setStbId : "+stbId );
        return _setStbId(stbId);
    }

    public int setMacAddress(String macAddr) {
        Log.d(TAG, "setMacAddress : "+macAddr );
        return _setMacAddress(macAddr);
    }

    public int setGwToken(String gwToken) {
        Log.d(TAG, "setGwToken : "+gwToken );
        return _setGwToken(gwToken);
    }

    public int setVisionImpaired(boolean visionImpaired) {
        Log.d(TAG, "setVisionImpaired : "+visionImpaired );
        return _setVisionImpaired(visionImpaired);
    }

    public int setHearingImpaired(boolean hearingImpaired) {
        Log.d(TAG, "setHearingImpaired : "+hearingImpaired );
        return _setHearingImpaired(hearingImpaired);
    }

    public int setVideoScalingMode(int scalingMode) {
        Log.d(TAG, "setVideoScalingMode : "+scalingMode );
        return _setVideoScalingMode(scalingMode);
    }

    public boolean getMlrActivationEnable() {
        Log.d(TAG, "getMlrActivationEnable" );
        return _getMlrActivationEnable();
    }

    public int setMlrActivationEnable(boolean enable) {
        Log.d(TAG, "setMlrActivationEnable : "+enable );
        return _setMlrActivationEnable(enable);
    }

    public int setTtaTestOption(String key, String value) {
        Log.d(TAG, "setTtaTestOption key=> "+key + ", value=>" + value );
        return _setTtaTestOption(key, value);
    }

    public int setAVOffset(int type, int nTimeMs) {
        Log.d(TAG, "setAVOffset type : "+type+", nTimeMs : "+nTimeMs );
        return _setAVOffset(type, nTimeMs);
    }

    public int tuneTV(IptvTuneInfo tuneInfo, int nAudioOnOff) throws Exception {
        String dataXML;

        if (nAudioOnOff < IPTV_AUDIO_OFF || nAudioOnOff > IPTV_AUDIO_ON)
            return IPTV_ERR_INVALID;

        int defaultAudioOnOff = nAudioOnOff;

        if(tuneInfo.getIsPip()) defaultAudioOnOff = tuneInfo.getAuioEnable();

        dataXML = String.format("%s;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;",
                tuneInfo.GetURL(),
                tuneInfo.GetPort(),
                tuneInfo.GetVideoPID(),
                tuneInfo.GetAudioPID(),
                tuneInfo.GetPCRPID(),
                tuneInfo.GetVideoStream(),
                tuneInfo.GetAudioStream(),
                tuneInfo.GetCAPID(),
                tuneInfo.GetCASystemID(),
                tuneInfo.GetChNum(),
                tuneInfo.GetResolution(),
                defaultAudioOnOff,
                tuneInfo.getLastFrame()
        );


		/*if (mGLIPTVSurfaceView != null) {
			mGLIPTVSurfaceView.setVisibility(View.VISIBLE);
		}*/

        return _tuneTV(dataXML);
    }

    public int tuneMultiViewTV(ArrayList<IptvInfo> tuneInfos, int nAudioOnOff, int viewMode) throws Exception {
        if(tuneInfos == null || tuneInfos.size() == 0) {
            return -1;
        }

        String dataXML = "";

        if (nAudioOnOff < IPTV_AUDIO_OFF || nAudioOnOff > IPTV_AUDIO_ON)
            return IPTV_ERR_INVALID;

        for (IptvInfo info : tuneInfos) {
            if(info instanceof IptvTuneInfo) {
                IptvTuneInfo iptvTuneInfo = (IptvTuneInfo)info;
                dataXML += String.format("%s;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;\n",
                        iptvTuneInfo.GetURL(),
                        iptvTuneInfo.GetPort(),
                        iptvTuneInfo.GetVideoPID(),
                        iptvTuneInfo.GetAudioPID(),
                        iptvTuneInfo.GetPCRPID(),
                        iptvTuneInfo.GetVideoStream(),
                        iptvTuneInfo.GetAudioStream(),
                        iptvTuneInfo.GetCAPID(),
                        iptvTuneInfo.GetCASystemID(),
                        iptvTuneInfo.GetChNum(),
                        iptvTuneInfo.GetResolution(),
                        nAudioOnOff,
                        iptvTuneInfo.getLastFrame(),
                        viewMode
                );
            } else if(info instanceof IptvPlayInfo) {
                IptvPlayInfo playInfo = (IptvPlayInfo)info;
                dataXML = String.format("%d;%d;%d;%d;%d;%s;%s;%s;%s;%s;%d;",
                        playInfo.GetPlayMode(),
                        playInfo.GetPlayType(),
                        playInfo.getDRMType(),
                        playInfo.GetDurationSec(),
                        playInfo.GetResumeSec(),
                        playInfo.GetPath(),
                        playInfo.GetMetaInfo(),
                        playInfo.GetContentID(),
                        playInfo.GetOTPID(),
                        playInfo.GetOTPPasswd(),
                        playInfo.getLastFrame()
                );
            }
        }

		/*if (mGLIPTVSurfaceView != null) {
			mGLIPTVSurfaceView.setVisibility(View.VISIBLE);
		}*/

        return _tuneTV(dataXML);
    }

    public int closeTV() throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        //	mGLIPTVSurfaceView.setVisibility(View.GONE);
        return _closeTV();
    }

    public int changeAudioChannel(int nAudioPid, int nAudioDecoder) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        String dataXML;
        dataXML = String.format("%d;%d;", nAudioPid, nAudioDecoder);

        return _changeAudioChannel(dataXML);
    }

    public int changeAudioOnOff(int nAudioOnOff) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        if (nAudioOnOff < IPTV_AUDIO_OFF || nAudioOnOff > IPTV_AUDIO_ON)
            return IPTV_ERR_INVALID;

        String dataXML;
        dataXML = String.format("%d;", nAudioOnOff);

        return _changeAudioOnOff(dataXML);
    }

    public int bindingFilter(IptvSectionInfo sectionInfo) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        String dataXML;
        dataXML = String.format("%d;%d;%d;%d;", sectionInfo.mIndex, sectionInfo.mPID, sectionInfo.mTableID, sectionInfo.mTimeOut);

        return _bindingFilter(dataXML);
    }

    public int releaseFilter(int nIndex) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        String dataXML;
        dataXML = String.format("%d;", nIndex);

        return _releaseFilter(dataXML);
    }

    public int filePlay(SurfaceHolder sh,  String path, int vpid, int vcodec, int apid, int acodec) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        mSurfaceHolder = sh;
        Surface surface;
        if (sh != null) {
            surface = sh.getSurface();
        } else {
            surface = null;
        }

		/*if (mGLIPTVSurfaceView != null) {
			mGLIPTVSurfaceView.setVisibility(View.VISIBLE);
		}*/

        return _filePlay(surface, path, vpid, vcodec, apid, acodec);
    }

    public void setIptvDisplay(SurfaceHolder sh) {
        mSurfaceHolder = sh;
        Surface surface;
        if (sh != null) {
            surface = sh.getSurface();
        } else {
            surface = null;
        }
        if(surface != null) {
            _setIptvVideoSurface(surface);
        }
    }

    public void startDmxFilter(DmxFilter filter) {
        int pid = filter.pid;
        int tid = filter.tid;
        Log.e(TAG, "startDmxFilter pid:" + pid + ",tid:" + tid );
        _startDmxFilter(pid, tid);
    }

    public void stopDmxFilter(DmxFilter filter) {
        int pid = filter.pid;
        int tid = filter.tid;
        Log.e(TAG, "stopDmxFilter pid:" + pid + ",tid:" + tid );
        _stopDmxFilter(pid, tid);
    }

    public int getPlayerStatus() {
        return _getPlayerStatus();
    }

    public int getCurrentPositionInSec() {
        return _getCurrentPositionInSec();
    }

    public int setWindowSize(int nLeft, int nTop, int nWidth, int nHeight) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        String dataXML;
		/*LayoutParams params;

		if (mGLIPTVSurfaceView == null)
		{
			return -1;
		}
		params = mGLIPTVSurfaceView.getLayoutParams();

		mGLIPTVSurfaceView.setX(nLeft);
		mGLIPTVSurfaceView.setY(nTop);
		params.height = nHeight;
		params.width = nWidth;

		mGLIPTVSurfaceView.setLayoutParams(params);*/
        dataXML = String.format("%d;%d;%d;%d;", nLeft, nTop, nWidth, nHeight);

		/*mX = nLeft;
		mY = nTop;
		mWidth = nWidth;
		mHeight = nHeight;

		mGLIPTVSurfaceView.setVisibility(View.GONE);*/

        return _setWindowSize(dataXML);
    }

    public int setPlayerSize(View playerView) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        int left=0, top=0, width=0, height=0;
        int [] location = new int[2];
        playerView.getLocationOnScreen(location);
        left = location[0];
        top = location[1];
        width = playerView.getWidth();
        height = playerView.getHeight();

        //custom Log
        Log.v(TAG, "left=" + left + " top=" + top + " width=" + width + " height=" + height);

        return _setPlayerSize(left, top, width, height);
    }

    public int setDummys(String dummys) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        return _setDummys(dummys);
    }


    private static void postEventFromNative(Object iptvmediaplayer_ref, int what, int arg1, int arg2, Object obj) {
        IptvMediaPlayer iptv_mp = (IptvMediaPlayer)((WeakReference)iptvmediaplayer_ref).get();
        if (iptv_mp == null) {
            return;
        }

        if (iptv_mp.mEventHandler != null) {
            Message m = iptv_mp.mEventHandler.obtainMessage(what, arg1, arg2, obj);
            iptv_mp.mEventHandler.sendMessage(m);
        }
    }


    public interface OnSectionFilterListener {
        void onSectionFilter(IptvMediaPlayer iptv_mp, byte[] sectionData);
    }
    public void setOnSectionFilterListener(OnSectionFilterListener listener) {
        mOnSectionFilterListener = listener;
    }

    public interface OnPlayStatusListener {
        void onPlayStatus(IptvMediaPlayer iptv_mp, int nDevice, String playStatus);
    }
    public void setOnPlayStatusListener(OnPlayStatusListener listener) {
        mOnPlayStatusListener = listener;
    }

    public interface OnTuneEventListener {
        void onTuneEvent(IptvMediaPlayer iptv_mp, int nDevice, int nStatus);
    }
    public void setOnTuneEventListener(OnTuneEventListener listener) {
        mOnTuneEventListener = listener;
    }

    public interface OnDisconnectedListener {
        void onDisconnected(IptvMediaPlayer iptv_mp, int nDevice, int nReason);
    }
    public void setOnDisconnectedListener(OnDisconnectedListener listener) {
        mOnDisconnectedListener = listener;
    }

    public interface OnPidChangedListener {
        void onPidChagned(IptvMediaPlayer iptv_mp, int event, String pid);
    }
    public void setOnPidChangedListener(OnPidChangedListener listener) {
        mOnPidChangedListener = listener;
    }

    public interface OnSeekCompletionListener {
        void onSeekCompletion(IptvMediaPlayer iptv_mp);
    }
    public void setOnSeekCompletionListener(OnSeekCompletionListener listener) {
        mOnSeekCompletionListener = listener;
    }

    public void initMakeFolderAndFile(Context context){

        String packageDataFolder = context.getDataDir().getPath();
        File config_folder = new File(packageDataFolder+CONFIG_FOLDER_PATH);
        File server_list_file = new File(packageDataFolder+CONFIG_FOLDER_PATH+SERVER_LIST_CONFIG_FILENAME);
        File cas_folder = new File(packageDataFolder+CAS_FOLDER_PATH);
		File mlr_folder = new File(packageDataFolder+MLR_CLIENT_FOLDER_PATH);
        File mlr_client_file = new File(packageDataFolder+MLR_CLIENT_FOLDER_PATH+MLR_CONFIG_FILENAME);
        File tta_channel_folder = new File(packageDataFolder+TTA_CHANNEL_CONFIG_PATH);
        File tta_channel_file = new File(packageDataFolder+TTA_CHANNEL_CONFIG_PATH+TTA_CHANNEL_FILENAME);


        if(!config_folder.exists()){
            config_folder.mkdirs();
        }

        if(!cas_folder.exists()){
            cas_folder.mkdirs();
        }

        if(!mlr_folder.exists()){
            mlr_folder.mkdirs();
        }
        if(!tta_channel_folder.exists()){
            tta_channel_folder.mkdirs();
        }

        if(!server_list_file.exists()){
            try {
                String assetFilePath = ASSERT_CONFIG_FOLDER_PATH+SERVER_LIST_CONFIG_FILENAME;
                Log.d(TAG, "assetFilePath : "+assetFilePath);
                InputStream is = context.getResources().getAssets().open(assetFilePath);
                convertStreamToString(is, server_list_file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        if(!mlr_client_file.exists()){
            try {
                String MLRclientFilePath = MLR_CLIENT_CONFIG_FOLDER_PATH+MLR_CONFIG_FILENAME;
                Log.d(TAG, "MLRclientFilePath : "+MLRclientFilePath);
                InputStream is = context.getResources().getAssets().open(MLRclientFilePath);
                convertStreamToString(is, mlr_client_file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(!tta_channel_file.exists()){
            try {
                String TTAChannelFilePath = TTA_CHANNEL_CONFIG_FOLDER_PATH+TTA_CHANNEL_FILENAME;
                Log.d(TAG, "TTAChannelFilePath : "+TTAChannelFilePath);
                InputStream is = context.getResources().getAssets().open(TTAChannelFilePath);
                convertStreamToString(is, tta_channel_file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(server_list_file.exists()){
            Log.d(TAG, " server list file exist");
        } else {
            Log.d(TAG, "server list file file not exist");
        }

        if(mlr_client_file.exists()){
            Log.d(TAG, "mlr client file exist");
        } else {
            Log.d(TAG, "mlr client file file not exist");
        }

        if(cas_folder.exists()){
            Log.d(TAG, "cas folder exist");
        } else {
            Log.d(TAG, "case folder not exist");
        }

        if(tta_channel_file.exists()){
            Log.d(TAG, "tta channel file exist");
        } else {
            Log.d(TAG, "tta channel file file not exist");
        }

    }

    public static void convertStreamToString(InputStream is, File out)
            throws IOException {

        try {
            try (OutputStream output = new FileOutputStream(out)) {
                byte[] buffer = new byte[4 * 1024]; // or other buffer size
                int read;

                while ((read = is.read(buffer)) != -1) {
                    output.write(buffer, 0, read);
                }

                output.flush();
            }
        } finally {
            is.close();
        }
    }


    private class EventHandler extends Handler {
        private IptvMediaPlayer mIptvMediaPlayer;

        public EventHandler(IptvMediaPlayer iptv_mp, Looper looper) {
            super(looper);
            mIptvMediaPlayer = iptv_mp;
        }

        @Override
        public void handleMessage(Message msg) {
            // 1 ~ 3 		: not used
            // 4			: section filter
            // 5		    : playstatus
            // 6			: tuneEvent
            // 7			: disconnected
            // 8			: close caption data event
            // 9			: pid changed event
            // 10			: seek completion event

            //Log.d(TAG, "what=" + msg.what + "arg1=" + msg.arg1 + "arg2=" + msg.arg2);
            switch (msg.what)
            {
                case 1:
                    if (msg.arg1 == 0 && msg.arg2 == 0)
                    {
					/*if (mGLIPTVSurfaceView == null)
					{
						break;
					}*/

                        //	mGLIPTVSurfaceView.setVisibility(View.VISIBLE);

                    }
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    Log.d(TAG, "section what=" + msg.what + " arg1=" + msg.arg1 + " arg2=" + msg.arg2);
                    if (mOnSectionFilterListener != null)
                    {
                        mOnSectionFilterListener.onSectionFilter(mIptvMediaPlayer, (byte[])msg.obj);
                    }
                    break;
                case 5:
                    Log.d(TAG, "playstatus what=" + msg.what + " arg1=" + msg.arg1 + " arg2=" + msg.arg2);
                    if (mOnPlayStatusListener != null)
                        mOnPlayStatusListener.onPlayStatus(mIptvMediaPlayer, msg.arg1, new String ((byte[])msg.obj));
                    break;
                case 6:
                    Log.d(TAG, "TuneEvent what=" + msg.what + " arg1=" + msg.arg1 + " arg2=" + msg.arg2);
                    if (mOnTuneEventListener != null)
                        mOnTuneEventListener.onTuneEvent(mIptvMediaPlayer, msg.arg1, msg.arg2);
                    break;
                case 7:
                    if (mOnDisconnectedListener != null)
                        mOnDisconnectedListener.onDisconnected(mIptvMediaPlayer, msg.arg1, msg.arg2);
                    break;
                case 8:
                    if(closeCaption != null) {
                        closeCaption.onUpdateCC(msg);
                    }
                    break;
                case 9:
                    Log.d(TAG, "PidChagned what=" + msg.what + " arg1=" + msg.arg1 + " arg2=" + msg.arg2);
                    if (mOnPidChangedListener != null)
                        mOnPidChangedListener.onPidChagned(mIptvMediaPlayer, msg.arg2, new String ((byte[])msg.obj));
                    break;
                case 10:
                    Log.d(TAG, "SeekCompletion what=" + msg.what + " arg1=" + msg.arg1 + " arg2=" + msg.arg2);
                    if (mOnSeekCompletionListener != null)
                        mOnSeekCompletionListener.onSeekCompletion(mIptvMediaPlayer);
                    break;

                default:
                    Log.e(TAG, "Unknown message type " + msg.what);
                    return;
            }
        }
    }
}
