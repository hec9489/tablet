
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.media.core;

import android.util.Log;

/**
 * SLog class can be used to logging
 *
 * @author Kwon Soon Chan (sysc0507@sk.com)
 */

public class SLog {

    private static boolean mLogEnable = true;
    private static boolean mClassName = false;   //클래스 이름
    private static boolean mMethodName = true;   //클래스 이름
    private static boolean mLineNumber = true;  //로그 라인

    public SLog()
    {

    }

    public static void d(String tag, String message)
    {
        if(mLogEnable)
        {
            Log.d(tag, getLine() + message);
        }
    }

    public static void e(String tag, String message)
    {
        if(mLogEnable)
        {
            Log.e(tag, getLine() + message);
        }
    }

    public static void i(String tag, String message)
    {
        if(mLogEnable)
        {
            Log.i(tag, getLine() + message);
        }
    }

    public static void v(String tag, String message)
    {
        if(mLogEnable)
        {
            Log.v(tag, getLine() + message);
        }
    }

    public static void w(String tag, String message)
    {
        if(mLogEnable)
        {
            Log.d(tag, getLine() + message);
        }
    }

    private static String getLine() {

        StackTraceElement el = new Throwable().fillInStackTrace().getStackTrace()[2];
        String className = el.getClassName();
        String methodName = el.getMethodName();
        int lineNumber = el.getLineNumber();

        String line="BTF";

        if(mClassName){
            line = line+"|" +className +"|";
        }

        if(mMethodName){
            line = line+"|" +methodName +"|";
        }

        if(mLineNumber){
            line = line+"|" +lineNumber +"|";
        }

        return line;
    }
}
