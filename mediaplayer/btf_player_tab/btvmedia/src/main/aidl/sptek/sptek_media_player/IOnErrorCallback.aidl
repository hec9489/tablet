// IOnErrorCallback.aidl
package sptek.sptek_media_player;

// Declare any non-default types here with import statements

interface IOnErrorCallback {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void onError(int errorCode, long detailErrorCode, String msg);
}