// IOnInfoCallback.aidl
package sptek.sptek_media_player;

// Declare any non-default types here with import statements

interface IOnInfoCallback {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void onInfo(in Message msg);
}