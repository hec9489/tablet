// IWebVTTCallback.aidl
package sptek.sptek_media_player;

// Declare any non-default types here with import statements

interface IWebVTTCallback {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void setWebVTTInfoList(in Bundle bundle);
}