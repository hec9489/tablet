// IPlayerDebugCallback.aidl
package sptek.sptek_media_player;

// Declare any non-default types here with import statements

interface IPlayerDebugCallback {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void onUpdatePlayerDebug(in Bundle bundle);
}