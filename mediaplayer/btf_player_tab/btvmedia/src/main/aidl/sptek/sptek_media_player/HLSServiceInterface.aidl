// HLSServiceInterface.aidl
package sptek.sptek_media_player;

import android.view.Surface;
import sptek.sptek_media_player.IOnInfoCallback;
import sptek.sptek_media_player.IOnErrorCallback;
import sptek.sptek_media_player.IOnCompletionCallback;
import sptek.sptek_media_player.IOnSeekProcessedCallback;
import sptek.sptek_media_player.IOnRenderedFirstFrameCallback;
import sptek.sptek_media_player.IWebVTTCallback;
import sptek.sptek_media_player.IPlayerDebugCallback;

// Declare any non-default types here with import statements

interface HLSServiceInterface {
    /**
         * Demonstrates some basic types that you can use as parameters
         * and return values in AIDL.
         */
        boolean registerOnInfoCallback(in IOnInfoCallback callback);
        boolean unregisterOnInfoCallback(in IOnInfoCallback callback);
        boolean registerOnErrorCallback(in IOnErrorCallback callback);
        boolean unregisterOnErrorCallback(in IOnErrorCallback callback);
        boolean registerOnCompletionCallback(in IOnCompletionCallback callback);
        boolean unregisterOnCompletionCallback(in IOnCompletionCallback callback);
        boolean registerOnSeekProcessedCallback(in IOnSeekProcessedCallback callback);
        boolean unregisterOnSeekProcessedCallback(in IOnSeekProcessedCallback callback);
        boolean registerOnRenderedFirstFrameCallback(in IOnRenderedFirstFrameCallback callback);
        boolean unregisterOnRenderedFirstFrameCallback(in IOnRenderedFirstFrameCallback callback);
        boolean registerFirstWebVTTCallback(in IWebVTTCallback callback);
        boolean unregisterFirstWebVTTCallback(in IWebVTTCallback callback);
        boolean registerSecondWebVTTCallback(in IWebVTTCallback callback);
        boolean unregisterSecondWebVTTCallback(in IWebVTTCallback callback);
        boolean registerPlayerDebugCallback(in IPlayerDebugCallback callback);
        boolean unregisterPlayerDebugCallback(in IPlayerDebugCallback callback);
        void setDataSource(String URLScheme, String drmLicenseUrl);
        void setTunneledMode(boolean flag);
        void init(boolean tunneling);
        void setSurface(in Surface surface);
        void prepare();
        void start(boolean ready);
        void resume();
        void pause();
        void stop(boolean reset);
        void release();
        void playTrick(float speed);
        long getDuration();
        long getCurrentPosition();
        void seekTo(long position);
        boolean isPlaying();
        void adjustSubtitleOffset(long offset);
        List<String> getLanguageList(int index);
        void setTrackLanguage(int index, String language);
        void allowPassthrough(boolean flag);
        void muteAudio(boolean mute);
        int getCurrentPlayerState();
        void setTrackDisable(int index, boolean flag);
        void startPlayerDebug();
        void stopPlayerDebug();
        String getVersion();
}