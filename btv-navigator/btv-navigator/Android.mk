
# Copyright 2019 SK Broadband Co., LTD.

LOCAL_PATH:= $(call my-dir)

# navigator.jar library
# ============================================================
include $(CLEAR_VARS)

LOCAL_MODULE := navigator

LOCAL_MODULE_TAGS := optional

LOCAL_STATIC_JAVA_LIBRARIES := android-support-annotations
LOCAL_STATIC_JAVA_LIBRARIES += libtikxml-annotation libtikxml libokio libtikxml-processor-common libkotlin-stdlib libkotlin-stdlib-common libkotlin-reflect libjavapoet

LOCAL_SRC_FILES := $(call all-java-files-under, src/main/java)

# Libraries needed by the compiler (JACK) to generate code.
PROCESSOR_LIBRARIES_TARGET := \
	libtikxml \
	libtikxml-annotation \
	libtikxml-processor-common \
	libtikxml-processor \
	libkotlin-stdlib \
	libkotlin-stdlib-common \
	libkotlin-reflect \
	libjavapoet

# Libraries needed by the compiler (JACK) to generate code.
# Resolve the jar paths.
PROCESSOR_JARS := $(call java-lib-deps, $(PROCESSOR_LIBRARIES_TARGET))

# Necessary for annotation processors to work correctly.
LOCAL_ADDITIONAL_DEPENDENCIES += $(PROCESSOR_JARS)

LOCAL_JACK_FLAGS += --processorpath $(call normalize-path-list,$(PROCESSOR_JARS))
LOCAL_JAVACFLAGS += -processorpath $(call normalize-path-list,$(PROCESSOR_JARS))

LOCAL_ANNOTATION_PROCESSOR_CLASSES := \
com.tickaroo.tikxml.processor.XmlProcessor

LOCAL_JAVACFLAGS += $(foreach class,$(LOCAL_ANNOTATION_PROCESSOR_CLASSES),-processor $(class))

LOCAL_USE_AAPT2 := true

LOCAL_JAVA_VERSION := 1.8

LOCAL_REQUIRED_MODULES := navigator.xml

# This will install the file in /system/framework
LOCAL_MODULE_PATH := $(TARGET_OUT_JAVA_LIBRARIES)

LOCAL_DEX_PREOPT := false

include $(BUILD_JAVA_LIBRARY)

# ====  library ========================

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := \
	libtikxml-annotation:libs/annotation-0.8.13.jar \
	libtikxml:libs/core-0.8.13.jar \
	libtikxml-processor:libs/processor-0.8.13.jar \
	libtikxml-processor-common:libs/processor-common-0.8.13.jar \
	libokio:libs/okio-1.15.0.jar \
	libkotlin-stdlib:libs/kotlin-stdlib-1.3.61.jar \
	libkotlin-stdlib-common:libs/kotlin-stdlib-common-1.3.61.jar \
	libkotlin-reflect:libs/kotlin-reflect-1.3.61.jar \
	libjavapoet:libs/javapoet-1.9.0.jar

include $(BUILD_MULTI_PREBUILT)

# ====  permissions ========================
include $(CLEAR_VARS)

LOCAL_MODULE := navigator.xml

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE_CLASS := ETC

# This will install the file in /system/etc/permissions
LOCAL_MODULE_PATH := $(TARGET_OUT_ETC)/permissions

LOCAL_SRC_FILES := $(LOCAL_MODULE)

include $(BUILD_PREBUILT)

