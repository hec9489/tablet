
// Copyright 2019 SK Broadband Co., LTD.
 


/*
 * NavigatorHandler.h
 *
 */

#ifndef NavigatorHandler_H_
#define NavigatorHandler_H_

#include <stdio.h>
#include <stdlib.h>

#include <jni.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>         // O_WRONLY
#include <unistd.h>        // write(), close()
#include <string.h>
#include <mutex>
#include <android/log.h>

#ifndef NELEM
# define NELEM(x) ((int) (sizeof(x) / sizeof((x)[0])))
#endif

#define LOG_TAG "NAVIGATOR-JNI"

#define LOG_DEBUG( ... )     __android_log_print( ANDROID_LOG_DEBUG,      LOG_TAG, __VA_ARGS__ );
#define LOG_INFO( ... )     __android_log_print( ANDROID_LOG_INFO,       LOG_TAG, __VA_ARGS__ );
#define LOG_ERROR( ... )     __android_log_print( ANDROID_LOG_ERROR,      LOG_TAG, __VA_ARGS__ );

#include "DvbsiDataHandler.h"

/*
 **************************************** 
 * Definitions
 **************************************** 
 */


/*
 **************************************** 
 * Structure Definitions
 **************************************** 
 */

class NavigatorHandler {
public:
	NavigatorHandler();
	virtual ~NavigatorHandler();
	static std::mutex mLock;

	static NavigatorHandler*	m_pInstance;		// NavigatorHandler instance
	static JavaVM* m_pJavaVM;						// java VM
	static JNIEnv*	m_pJNIEnv;						// JNI

	static JNIEnv* getJNIEnv();
	static JavaVM* getJavaVM();

	static jfieldID  navigator_context;

	static int JNI_OnLoad(JavaVM* vm, JNIEnv* env);
	static jmethodID getObjectMethod(JNIEnv* env, jclass clazz, const char* name, const char* sig);
	static jclass getNavigatorHandlerInterfaceClazz(JNIEnv* env);
	static jobject getNavigatorHandlerInterfaceObject(JNIEnv* env, jclass clazz);
	static int registerNativeMethods(JNIEnv* env, const char* className, JNINativeMethod* gMethods, int numMethods);
	static NavigatorHandler* getInstance();

	static DvbsiDataHandler* mDvbsiDataHandler;

	static void navigator_init(JNIEnv *env, jobject thiz);
	static void navigator_dvbsi_start(JNIEnv *env, jobject thiz);
	static void navigator_dvbsi_release(JNIEnv *env, jobject thiz);	

	// dsmcc Listener
	static void navigator_setDsmccEventListener(JNIEnv *env, jobject thiz, jobject weak_this) ;

	// audio program update
	static void navigator_setAudioProgramUpdateListener(JNIEnv *env, jobject thiz, jobject weak_this) ;

	// DVBSI Related
	static void navigator_setDvbsiUpdateListener(JNIEnv *env, jobject thiz, jobject weak_this) ;

	static void navigator_setRegionCode(JNIEnv *env, jobject thiz, jstring region_code) ;	
	static void navigator_setSegId(JNIEnv *env, jobject thiz, jint segid) ;	
	static void navigator_setCurrentChannel(JNIEnv *env, jobject thiz, jstring channel) ;	

	static jobject navigator_getAllAVQueryData(JNIEnv *env, jobject thiz) ;
	static jobject navigator_getAVQueryDataForChannel(JNIEnv *env, jobject thiz, jint ch) ;	
	static jobject navigator_getAVQueryDataForSid(JNIEnv *env, jobject thiz, jint sid) ;		
	static jobject navigator_getMultiAVQueryData(JNIEnv *env, jobject thiz, jstring chlist) ;	

	static jobject navigator_getAllMusicQueryData(JNIEnv *env, jobject thiz) ;
	static jobject navigator_getMusicQueryDataForChannel(JNIEnv *env, jobject thiz, jint ch) ; 
	static jobject navigator_getMusicQueryDataForSid(JNIEnv *env, jobject thiz, jint sid) ;		
	static jobject navigator_getMultiMusicQueryData(JNIEnv *env, jobject thiz, jstring chlist);

	static void navigator_slogi(JNIEnv *env, jobject thiz, jstring log);
	static void navigator_slogd(JNIEnv *env, jobject thiz, jstring log);
	static void navigator_sloge(JNIEnv *env, jobject thiz, jstring log);
private:


};

#endif /* NavigatorHandler_H_ */
