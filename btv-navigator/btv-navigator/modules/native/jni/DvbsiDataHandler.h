
// Copyright 2019 SK Broadband Co., LTD.
 

 
#ifndef DVBSI_DATA_HANDLER_H
#define DVBSI_DATA_HANDLER_H

#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include <unistd.h>
#include <jni.h>

#include "Navigator.h"
#include "dsmcc.h"
// ----------------------------------------------------------------------------
class DvbsiDataHandler
{
    public:
        DvbsiDataHandler(JNIEnv* env, jobject thiz);
        ~DvbsiDataHandler();

        void dsmccSignalEvent(char* dsmccRootPath, dsmcc_sigevent* event);
        void audioProgramUpdate(char* filepath, int32_t updateType, int32_t audioPid);
        void dvbsiUpdate(int32_t updateType);

        void setDsmccEventListener(JNIEnv *env, jobject thiz, int flag);
        void setAudioProgramUpdateListener(JNIEnv *env, jobject thiz, int flag);
        void setDvbsiUpdateListener(JNIEnv *env, jobject thiz, int flag);
        void setRegionCode(JNIEnv *env, jobject thiz, jstring regionCode);
        void setSegId(JNIEnv *env, jobject thiz, jint segid);
        void setCurrentChannel(JNIEnv *env, jobject thiz, jstring url);

        jobject getAllAVQueryData(JNIEnv *env, jobject thiz);
        jobject getAVQueryDataForChannel(JNIEnv* env, jobject thiz, jint ch);
        jobject getAVQueryDataForSid(JNIEnv* env, jobject thiz, jint sid);
        jobject getMultiAVQueryData(JNIEnv* env, jobject thiz, jstring chlist);
        jobject getAllMusicQueryData(JNIEnv* env, jobject thiz);
        jobject getMusicQueryDataForChannel(JNIEnv* env, jobject thiz, jint ch);
        jobject getMusicQueryDataForSid(JNIEnv* env, jobject thiz, jint sid);
        jobject getMultiMusicQueryData(JNIEnv* env, jobject thiz, jstring chlist);
	
    private:
        jobject programEntity(JNIEnv* env, AVProgram_t* program);
        jobject programLinkEntity(JNIEnv* env, AVProgramLinkInfo_t* linkinfo);
        jobject programsListEntity(JNIEnv* env, char* data);

        jclass      mClass;     

        jmethodID   mPostEventFromDsmcc; // postEventFromDsmcc method ID.		
        jmethodID   mPostEventFromDvbsiUpdate; // postEventFromDvbsiUpdate method ID.
        jmethodID   mPostEventFromAudioProgramUpdate; // postEventFromAudioProgramUpdate method ID.

        int         mDsmccEventListener;
        int         mDvbsiUpdateListener;
        int         mAudioProgramUpdateListener;

        jclass      mEventClass;  // strong reference to DSMCCSignalEvent class
        jclass      mAppInfoClass; // strong reference to AppInfo class

        jclass      mAVProgramListClass;
        jclass      mAVProgramsClass;
        jclass      mAVProgramClass;
        jclass      mAVProgramLinkInfoClass;
};

#endif // DVBSI_DATA_HANDLER_H

