
// Copyright 2019 SK Broadband Co., LTD.
 


//#define LOG_NDEBUG 0

#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include <unistd.h>
#include <string.h>
#include <jni.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include <sys/types.h> 

#include "Navigator.h"
#include "NavigatorHandler.h"

//-------------------------------------------------------------------------------------------------
std::mutex NavigatorHandler::mLock;

NavigatorHandler *NavigatorHandler::m_pInstance = NULL;
JavaVM* NavigatorHandler::m_pJavaVM = NULL;								// java VM
JNIEnv*	NavigatorHandler::m_pJNIEnv = NULL;

jfieldID  NavigatorHandler::navigator_context;

DvbsiDataHandler* NavigatorHandler::mDvbsiDataHandler = NULL;

static const char *classJNINavigatorInterface = "com/skb/btv/framework/navigator/BtvJniInterface";

static JNINativeMethod sNavigatorHandlerMethods[] = {
    /* name,                signature,                  funcPtr */
    {"navigator_init", "()V", (void*)NavigatorHandler::navigator_init},

    {"navigator_dvbsi_start", "()V", (void*)NavigatorHandler::navigator_dvbsi_start},
    {"navigator_dvbsi_release", "()V", (void*)NavigatorHandler::navigator_dvbsi_release},

    //dsmcc event callback
    {"navigator_setDsmccEventListener",	"(Ljava/lang/Object;)V", (void*)NavigatorHandler::navigator_setDsmccEventListener},
	
    //audio program update
    {"navigator_setAudioProgramUpdateListener",	"(Ljava/lang/Object;)V", (void*)NavigatorHandler::navigator_setAudioProgramUpdateListener},
		
    //DVBSI Related
    {"navigator_setDvbsiUpdateListener", "(Ljava/lang/Object;)V", (void*)NavigatorHandler::navigator_setDvbsiUpdateListener},
	
    {"navigator_setRegionCode", "(Ljava/lang/String;)V", (void*)NavigatorHandler::navigator_setRegionCode},
    {"navigator_setSegId", "(I)V", (void*)NavigatorHandler::navigator_setSegId},
    {"navigator_setCurrentChannel", "(Ljava/lang/String;)V", (void*)NavigatorHandler::navigator_setCurrentChannel},
	
    {"navigator_getAllAVQueryData",	"()Lcom/skb/btv/framework/navigator/program/AVProgramList;", (void*)NavigatorHandler::navigator_getAllAVQueryData}, 
    {"navigator_getAVQueryDataForChannel",	"(I)Lcom/skb/btv/framework/navigator/program/AVProgramList;", (void*)NavigatorHandler::navigator_getAVQueryDataForChannel},	
    {"navigator_getAVQueryDataForSid", "(I)Lcom/skb/btv/framework/navigator/program/AVProgramList;",	(void*)NavigatorHandler::navigator_getAVQueryDataForSid},	
    {"navigator_getMultiAVQueryData", "(Ljava/lang/String;)Lcom/skb/btv/framework/navigator/program/AVProgramList;",	(void*)NavigatorHandler::navigator_getMultiAVQueryData},		
		
    {"navigator_getAllMusicQueryData", "()Lcom/skb/btv/framework/navigator/program/AVProgramList;", (void*)NavigatorHandler::navigator_getAllMusicQueryData}, 
    {"navigator_getMusicQueryDataForChannel", "(I)Lcom/skb/btv/framework/navigator/program/AVProgramList;", (void*)NavigatorHandler::navigator_getMusicQueryDataForChannel},	
    {"navigator_getMusicQueryDataForSid", "(I)Lcom/skb/btv/framework/navigator/program/AVProgramList;", (void*)NavigatorHandler::navigator_getMusicQueryDataForSid},	
    {"navigator_getMultiMusicQueryData", "(Ljava/lang/String;)Lcom/skb/btv/framework/navigator/program/AVProgramList;", (void*)NavigatorHandler::navigator_getMultiMusicQueryData},

	{"navigator_slogi", "(Ljava/lang/String;)V", (void*)NavigatorHandler::navigator_slogi},
	{"navigator_slogd", "(Ljava/lang/String;)V", (void*)NavigatorHandler::navigator_slogd},
	{"navigator_sloge", "(Ljava/lang/String;)V", (void*)NavigatorHandler::navigator_sloge},


};



NavigatorHandler::NavigatorHandler() {

}

NavigatorHandler::~NavigatorHandler() {

}

int NavigatorHandler::JNI_OnLoad(JavaVM* vm, JNIEnv* env)
{
	m_pInstance->m_pJavaVM = vm;
	m_pInstance->m_pJNIEnv = env;

	jclass clazz = env->FindClass(classJNINavigatorInterface);
	if (clazz == NULL) {
		LOG_ERROR("[%s][%d] Failed to find %s", __func__, __LINE__, classJNINavigatorInterface);
		return JNI_FALSE;
	}

	if (!NavigatorHandler::registerNativeMethods(env, classJNINavigatorInterface, sNavigatorHandlerMethods, NELEM(sNavigatorHandlerMethods))) {
		return JNI_FALSE;
	}
	return JNI_TRUE;
}


JNIEnv* NavigatorHandler::getJNIEnv()
{
    JNIEnv* env;
    assert(m_pJavaVM != NULL);

    if (m_pJavaVM->GetEnv((void**) &env, JNI_VERSION_1_6) != JNI_OK)
        return NULL;
    return env;
}

JavaVM* NavigatorHandler::getJavaVM()
{
    assert(m_pJavaVM != NULL);
    return m_pJavaVM;
}

jmethodID NavigatorHandler::getObjectMethod(JNIEnv* env, jclass clazz, const char* name, const char* sig){
	if(clazz == NULL) return NULL;

    jmethodID getResourceMethod = env->GetMethodID(clazz, name, sig);
	if(getResourceMethod == NULL) return NULL;

	return getResourceMethod;
}

jclass NavigatorHandler::getNavigatorHandlerInterfaceClazz(JNIEnv* env)
{
	return env->FindClass(classJNINavigatorInterface);
}

jobject NavigatorHandler::getNavigatorHandlerInterfaceObject(JNIEnv* env, jclass clazz)
{
	if(clazz == NULL) return NULL;

    jmethodID constructMethod = env->GetMethodID(clazz, "<init>", "()V");
	if(constructMethod == NULL) return NULL;

	jobject NavigatorHandlerInterface = env->NewObject(clazz, constructMethod);
	if(NavigatorHandlerInterface == NULL) return NULL;

	return NavigatorHandlerInterface;
}

int NavigatorHandler::registerNativeMethods(JNIEnv* env, const char* className, JNINativeMethod* gMethods, int numMethods)
{
	jclass cls;
	cls = env->FindClass(className);
	if(cls == NULL)
	{
		LOG_ERROR("Native registration unable to find clss '%s'", className);
		return JNI_FALSE;
	}

	LOG_INFO("RegisterNatives start for '%s', numMethods(%d)", className, numMethods);
	int ret = env->RegisterNatives(cls, gMethods, numMethods);
	if(ret < 0)
	{
		LOG_ERROR("RegisterNatives failed for '%s', ret(%d)", className, ret);
		return JNI_FALSE;
	}

	return JNI_TRUE;
}

NavigatorHandler* NavigatorHandler::getInstance()
{
	if(m_pInstance != NULL)	{ return m_pInstance; }
	m_pInstance = new NavigatorHandler();
	return m_pInstance;
}

void NavigatorHandler::navigator_init(JNIEnv *env, jobject thiz)
{
	LOG_INFO("%s start", __FUNCTION__);
    jclass clazz;
    clazz = env->FindClass(classJNINavigatorInterface);
    if (clazz == NULL)
    {
        LOG_INFO("[%s][%d] clazz == NULL ", __FUNCTION__, __LINE__);
        return;
    }

    navigator_context = env->GetFieldID(clazz, "mNavigatorContext", "I");
    if (navigator_context == NULL)
    {
        LOG_INFO("[%s][%d] navigator_context == NULL", __FUNCTION__, __LINE__);
        return;
    }
}

void NavigatorHandler::navigator_dvbsi_start(JNIEnv *env, jobject thiz)
{
	LOG_INFO("%s start", __FUNCTION__);
	std::lock_guard<std::mutex> lock(mLock);

	if(mDvbsiDataHandler == NULL){
		mDvbsiDataHandler = new DvbsiDataHandler(env, thiz);
	}
}


void NavigatorHandler::navigator_dvbsi_release(JNIEnv *env, jobject thiz)
{
	LOG_INFO("%s start", __FUNCTION__);
	std::lock_guard<std::mutex> lock(mLock);
}

// dsmcc event callback
void NavigatorHandler::navigator_setDsmccEventListener(JNIEnv *env, jobject thiz, jobject weak_this)
{
	LOG_INFO("%s start", __FUNCTION__);
	std::lock_guard<std::mutex> lock(mLock);

	mDvbsiDataHandler->setDsmccEventListener(env, thiz, 1);
}

// audio program update
void NavigatorHandler::navigator_setAudioProgramUpdateListener(JNIEnv *env, jobject thiz, jobject weak_this)
{
	LOG_INFO("%s start", __FUNCTION__);
	std::lock_guard<std::mutex> lock(mLock);

	mDvbsiDataHandler->setAudioProgramUpdateListener(env, thiz, 1);
}

void NavigatorHandler::navigator_setDvbsiUpdateListener(JNIEnv *env, jobject thiz, jobject weak_this)
{
	LOG_INFO("%s start", __FUNCTION__);
	std::lock_guard<std::mutex> lock(mLock);

	mDvbsiDataHandler->setDvbsiUpdateListener(env, thiz, 1);
}

void NavigatorHandler::navigator_setRegionCode(JNIEnv *env, jobject thiz, jstring region_code)
{
	LOG_INFO("%s start", __FUNCTION__);
	mDvbsiDataHandler->setRegionCode(env, thiz, region_code);
}
void NavigatorHandler::navigator_setSegId(JNIEnv *env, jobject thiz, jint segid) 
{
	LOG_INFO("%s start", __FUNCTION__);
	mDvbsiDataHandler->setSegId(env, thiz, segid);
}



void NavigatorHandler::navigator_setCurrentChannel(JNIEnv *env, jobject thiz, jstring channel)
{
	LOG_INFO("%s start", __FUNCTION__);
	mDvbsiDataHandler->setCurrentChannel(env, thiz, channel);
}


jobject NavigatorHandler::navigator_getAllAVQueryData(JNIEnv *env, jobject thiz)
{
	LOG_INFO("%s start", __FUNCTION__);
	std::lock_guard<std::mutex> lock(mLock);

	return mDvbsiDataHandler->getAllAVQueryData(env, thiz);
}

jobject NavigatorHandler::navigator_getAVQueryDataForChannel(JNIEnv *env, jobject thiz, jint ch)
{
	LOG_INFO("%s start", __FUNCTION__);
	std::lock_guard<std::mutex> lock(mLock);

	return mDvbsiDataHandler->getAVQueryDataForChannel(env, thiz, ch);
}

jobject NavigatorHandler::navigator_getAVQueryDataForSid(JNIEnv *env, jobject thiz, jint sid)
{
	LOG_INFO("%s start", __FUNCTION__);
	std::lock_guard<std::mutex> lock(mLock);

	return mDvbsiDataHandler->getAVQueryDataForSid(env, thiz, sid);
}

jobject NavigatorHandler::navigator_getMultiAVQueryData(JNIEnv *env, jobject thiz, jstring chlist)
{
	LOG_INFO("%s start", __FUNCTION__);
	std::lock_guard<std::mutex> lock(mLock);

	return mDvbsiDataHandler->getMultiAVQueryData(env, thiz, chlist);
}

jobject NavigatorHandler::navigator_getAllMusicQueryData(JNIEnv *env, jobject thiz)
{
	LOG_INFO("%s start", __FUNCTION__);
	std::lock_guard<std::mutex> lock(mLock);

	return mDvbsiDataHandler->getAllMusicQueryData(env, thiz);
}

jobject NavigatorHandler::navigator_getMusicQueryDataForChannel(JNIEnv *env, jobject thiz, jint ch)
{
	LOG_INFO("%s start", __FUNCTION__);
	std::lock_guard<std::mutex> lock(mLock);

	return mDvbsiDataHandler->getMusicQueryDataForChannel(env, thiz, ch);
}

jobject NavigatorHandler::navigator_getMusicQueryDataForSid(JNIEnv *env, jobject thiz, jint sid)
{
	LOG_INFO("%s start", __FUNCTION__);
	std::lock_guard<std::mutex> lock(mLock);

	return mDvbsiDataHandler->getMusicQueryDataForSid(env, thiz, sid);
}

jobject NavigatorHandler::navigator_getMultiMusicQueryData(JNIEnv *env, jobject thiz, jstring chlist)
{
	LOG_INFO("%s start", __FUNCTION__);
	std::lock_guard<std::mutex> lock(mLock);

	return mDvbsiDataHandler->getMultiMusicQueryData(env, thiz, chlist);
}

void NavigatorHandler::navigator_slogi(JNIEnv *env, jobject thiz, jstring log)
{
	const char *tmp = (env)->GetStringUTFChars(log, NULL);
	//LOG_INFO(tmp);
	(env)->ReleaseStringUTFChars(log, tmp);
}

void NavigatorHandler::navigator_slogd(JNIEnv *env, jobject thiz, jstring log)
{
	const char *tmp = (env)->GetStringUTFChars(log, NULL);
	//LOG_DEBUG(tmp);
	(env)->ReleaseStringUTFChars(log, tmp);
}

void NavigatorHandler::navigator_sloge(JNIEnv *env, jobject thiz, jstring log)
{
	const char *tmp = (env)->GetStringUTFChars(log, NULL);
	//LOG_ERROR(tmp);
	(env)->ReleaseStringUTFChars(log, tmp);
}

