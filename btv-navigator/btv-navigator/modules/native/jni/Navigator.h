
// Copyright 2019 SK Broadband Co., LTD.
 

/**
 * @file Navigator.h
 *
 * @brief Header for Navigator Protocol
 *
 */


#ifndef _NAVIGATOR_H_
#define _NAVIGATOR_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <pthread.h>
#include <stdbool.h>
#include <unistd.h>

#include "programs.h"


/*
 **************************************** 
 * Definitions
 **************************************** 
 */


#define AVPROGRAM_LINK_UNIT_SIZE            (sizeof(AVProgramLinkInfo_t) - sizeof(AVProgramLinkInfo_t*))
#define AVPROGRAM_UNIT_SIZE                 (sizeof(AVProgram_t) - sizeof(AVProgram_t*) - AVPROGRAM_LINK_UNIT_SIZE)

/*
 **************************************** 
 * Structure Definitions
 **************************************** 
 */
typedef struct {
    uint32_t    message_id;
    uint32_t    message_length;
    char        value_data[1];
} MessageHeaderType;

typedef struct {
    uint16_t count;
    char     value[1];
} DvbsiDataType;

#endif /* _NAVIGATOR_H_ */
