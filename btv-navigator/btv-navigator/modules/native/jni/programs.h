//
// Created by Administrator on 2020-05-06.
//

#ifndef NAVIGATOR_PROGRAMS_H
#define NAVIGATOR_PROGRAMS_H

#define D_MAX_EVENT_NAME_LENS 384 + 1
#define D_MAX_EVENT_DESC_LENS 384 + 1
#define D_MAX_IMAGE_URL_LENS  160
#define D_MAX_ITEM_PRICE_LENS      10
#define D_MAX_ITEM_DIRECTOR_LENS 80
#define D_MAX_ITEM_ACTOR_LEMS    160
#define D_MAX_ITEM_TEXT_LEMS     10
#define D_MAX_LINK_CID_LENS      160
#define D_MAX_LINK_VOID_ID_LENS      160

#define D_MAX_LINK_BUTTON_IMAGE_FILENAME_LENS 160
#define D_MAX_LINK_VAS_ITEM_ID_LENS           256
#define D_MAX_LINK_VAS_PATH_LENS              256
#define D_MAX_LINK_VAS_SERVICE_ID_LENS        256

/*************************************************************************
    for Database
*************************************************************************/
typedef struct AVProgramLinkInfo_t AVProgramLinkInfo_t;
typedef struct AVProgram_t AVProgram_t;

typedef struct AVProgramLinkInfo_t{
    char                    buttonImagePath[D_MAX_LINK_BUTTON_IMAGE_FILENAME_LENS];
    uint8_t                 buttonType;
    uint32_t                cMenuValue;
    char                    displayStartTime[32];
    char                    displayEndTime[32];
    char                    linkedServiceText[D_MAX_LINK_CID_LENS];
    uint32_t                linkedServiceType;
    char                    vasItemId[D_MAX_LINK_VAS_ITEM_ID_LENS];
    char                    vasPath[D_MAX_LINK_VAS_PATH_LENS];
    char                    vasServiceId[D_MAX_LINK_VAS_SERVICE_ID_LENS];
    AVProgramLinkInfo_t*    next;
} AVProgramLinkInfo_t;
/*
typedef struct  AVProgram_t{
    char                    eventName[D_MAX_EVENT_NAME_LENS];
    char                    eventDesc[D_MAX_EVENT_DESC_LENS];
    char                    item_price[D_MAX_ITEM_PRICE_LENS];
    char                    resolution[5];
    char                    audio[10];
    int32_t                 eventid;
    int32_t                 rating;
    int32_t                 duration;
    char                    startTime[20];
    char                    endTime[20];
    int32_t                 startYear;
    int32_t                 startMonth;
    int32_t                 startDay;
    int32_t                 endYear;
    int32_t                 endMonth;
    int32_t                 endDay;
    char                    lang[4];
    char                    item_actor[D_MAX_ITEM_ACTOR_LEMS];
    char                    isCaption;
    int32_t                 contentNibble1;
    int32_t                 contentNibble2;
    int32_t                 contentUserNibble1;
    int32_t                 contentUserNibble2;
    char                    item_director[D_MAX_ITEM_DIRECTOR_LENS];
    char                    isDolbyAudio;
    char                    isDvs;
    char                    imagePath[D_MAX_IMAGE_URL_LENS];
    int32_t                 runningStatus;
    char                    vodid[D_MAX_LINK_VOID_ID_LENS];
    int32_t                 channelId;
    int32_t                 audioType;
    int32_t                 programId;
    int32_t                 signLanguage;

    AVProgramLinkInfo_t*  avProgramLinkInfo;
    AVProgramAudioType_t*   avProgramaudioTypeList;
    AVProgram_t*            next;
};
*/
typedef struct AVProgram_t{
    int32_t                 serviceid;
    char                    eventName[D_MAX_EVENT_NAME_LENS];
    char                    eventDesc[D_MAX_EVENT_DESC_LENS];
    char                    item_price[D_MAX_ITEM_PRICE_LENS];
    char                    resolution[5];
    char                    audio[10];
    int32_t                 eventid;
    int32_t                 rating;
    int32_t                 duration;
    char                    startTime[20];
    char                    endTime[20];
    int32_t                 startYear;
    int32_t                 startMonth;
    int32_t                 startDay;
    int32_t                 endYear;
    int32_t                 endMonth;
    int32_t                 endDay;
    char                    lang[4];
    char                    item_actor[D_MAX_ITEM_ACTOR_LEMS];
    char                    isCaption[4];
    int32_t                 contentNibble1;
    int32_t                 contentNibble2;
    int32_t                 contentUserNibble1;
    int32_t                 contentUserNibble2;
    char                    item_director[D_MAX_ITEM_DIRECTOR_LENS];
    char                    isDolbyAudio[4];
    char                    isDvs[4];
    char                    imagePath[D_MAX_IMAGE_URL_LENS];
    int32_t                 runningStatus;
    char                    vodid[D_MAX_LINK_VOID_ID_LENS];
    int32_t                 signLanguage;
    int32_t                 chnum;
    char                    startftime[32];
    char                    endftime[32];
    char                    updatetime[32];

    AVProgramLinkInfo_t    avProgramLinkInfo;

    AVProgram_t*            next;
};

#endif //NAVIGATOR_PROGRAMS_H
