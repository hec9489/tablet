
// Copyright 2019 SK Broadband Co., LTD.
 

#include "NavigatorHandler.h"
#include "DvbsiDataHandler.h"

// ----------------------------------------------------------------------------

static const char *classAVProgramList = "com/skb/btv/framework/navigator/program/AVProgramList";
static const char *classAVPrograms = "com/skb/btv/framework/navigator/program/AVPrograms";
static const char *classAVProgram = "com/skb/btv/framework/navigator/program/AVProgram";
static const char *classAVProgramLinkInfo = "com/skb/btv/framework/navigator/program/AVProgramLinkInfo";

static const char *classDsmccSignalEvent = "com/skb/btv/framework/navigator/SignalEvent";
static const char *classAppInfo = "com/skb/btv/framework/navigator/AppInfo";

struct avprogramlist_fields_t {
	jmethodID	AVProgramListConstructor;
	jmethodID	addAVProgramsMethod;

	jmethodID	AVProgramsConstructor;		
	jmethodID	addAVProgramMethod;

	jmethodID	AVProgramConstructor;		
	jmethodID	addAVProgramLinkInfoMethod;

	jmethodID	setChannelId;		
	jmethodID	setNameStr;
	jmethodID	setDescriptionStr;	
	jmethodID	setPrice;
	jmethodID	setResolutionStr;	
	jmethodID	setAudioStr;	
	jmethodID	setEventid;	
	jmethodID	setRating;	
	jmethodID	setDuration;	
	jmethodID	setStartTime;	
	jmethodID	setEndTime;	
	jmethodID	setStartYear;	
	jmethodID	setStartMonth;	
	jmethodID	setStartDay;	
	jmethodID	setEndYear;	
	jmethodID	setEndMonth;	
	jmethodID	setEndDay;	
	jmethodID	setLang;	
	jmethodID	setActors;	
	jmethodID	setIsCaptionStr;	
	jmethodID	setContentNibble1;	
	jmethodID	setContentNibble2;	
	jmethodID	setContentUserNibble1;	
	jmethodID	setContentUserNibble2;	
	jmethodID	setDirector;	
	jmethodID	setIsDolbyAudioStr;	
	jmethodID	setisDvsStr;	
	jmethodID	setImagePath;		
	jmethodID	setRunningStatus;		
	jmethodID	setVodId;		
	jmethodID	setIsSignLangOn;

	jmethodID	AVProgramLinkInfoConstructor;		

	jmethodID	setButtonImagePath;
	jmethodID	setButtonType;
	jmethodID	setCMenuValue;
	jmethodID	setDisplayStartDateStr;
	jmethodID	setDisplayEndDateStr;
	jmethodID	setLinkedServiceText;
	jmethodID	setLinkedServiceType;
	jmethodID	setVasItemId;
	jmethodID	setVasPath;
	jmethodID	setVasServiceId;
};

static avprogramlist_fields_t avprogram_fields;


struct dsmcc_fields_t {
	jmethodID	mEventConstructor;
	jfieldID	mEventType;
	jfieldID	mChannelNum;
	jfieldID	mDemux;
	jfieldID	mUri;
	jfieldID	mAppInfoList;		

	jmethodID	mAppInfoConstructor;		
	jfieldID	mAppType;
	jfieldID	mOrganizationId;
	jfieldID	mApplicationId;
	jfieldID	mDsmccPid;
	jfieldID	mIsServiceBound;
	jfieldID	mControlCode;
	jfieldID	mName;
	jfieldID	mPriority;
	jfieldID	mInitialPath;		
	
};

static dsmcc_fields_t Dsmccfields;

struct field {
    const char *class_name;
    const char *field_name;
    const char *field_type;
    jfieldID   *jfield;
};

static void find_fields(JNIEnv *env, field *fields, int count)
{
    for (int i = 0; i < count; i++) {
        field *f = &fields[i];
        jclass clazz = env->FindClass(f->class_name);
        jfieldID field = env->GetFieldID(clazz, f->field_name, f->field_type);
        *(f->jfield) = field;
    }
}

static JNIEnv* getJniEnv() {
    JNIEnv* env = NavigatorHandler::getInstance()->getJNIEnv();
    return env;
}

static void checkAndClearExceptionFromCallback(JNIEnv* env, const char* methodName) {
    if (env->ExceptionCheck()) {
        LOG_INFO("An exception was thrown by callback '%s'.", methodName);
        env->ExceptionClear();
    }
}

DvbsiDataHandler::DvbsiDataHandler(JNIEnv* env, jobject thiz)
{
    // Hold onto the NativeTvSystem class for use in calling the static method
    // that posts events to the application thread.
    jclass clazz = env->GetObjectClass(thiz);
    if (clazz == NULL) {
        return;
    }
    mClass = (jclass)env->NewGlobalRef(clazz);

	mDsmccEventListener = 0;
	mDvbsiUpdateListener = 0;
	mAudioProgramUpdateListener = 0;

    mPostEventFromDsmcc = env->GetStaticMethodID(clazz, "postEventFromDsmcc", "(Ljava/lang/String;Lcom/skb/btv/framework/navigator/SignalEvent;)V");
    mPostEventFromDvbsiUpdate = env->GetStaticMethodID(clazz, "postEventFromDvbsiUpdate", "(I)V");
    mPostEventFromAudioProgramUpdate = env->GetStaticMethodID(clazz, "postEventFromAudioProgramUpdate", "(Ljava/lang/String;II)V");

    jclass avprogramlistClazz = env->FindClass(classAVProgramList);
    mAVProgramListClass = (jclass) env->NewGlobalRef(avprogramlistClazz);
    avprogram_fields.AVProgramListConstructor = env->GetMethodID(avprogramlistClazz, "<init>", "()V");
    avprogram_fields.addAVProgramsMethod = env->GetMethodID(avprogramlistClazz, "addAVPrograms", "(Lcom/skb/btv/framework/navigator/program/AVPrograms;)V");

    jclass avprogramsClazz = env->FindClass(classAVPrograms);
    mAVProgramsClass = (jclass) env->NewGlobalRef(avprogramsClazz);
    avprogram_fields.AVProgramsConstructor = env->GetMethodID(avprogramsClazz, "<init>", "(I)V");
    avprogram_fields.addAVProgramMethod = env->GetMethodID(avprogramsClazz, "addAVProgram", "(Lcom/skb/btv/framework/navigator/program/AVProgram;)V");

    jclass avprogramClazz = env->FindClass(classAVProgram);
    mAVProgramClass = (jclass) env->NewGlobalRef(avprogramClazz);
    avprogram_fields.AVProgramConstructor = env->GetMethodID(avprogramClazz, "<init>", "()V");
    avprogram_fields.addAVProgramLinkInfoMethod = env->GetMethodID(avprogramClazz, "addAVProgramLinkInfo", "(Lcom/skb/btv/framework/navigator/program/AVProgramLinkInfo;)V");

    avprogram_fields.setChannelId = env->GetMethodID(avprogramClazz, "setChannelId", "(I)V");	
    avprogram_fields.setNameStr = env->GetMethodID(avprogramClazz, "setNameStr", "(Ljava/lang/String;)V");	
    avprogram_fields.setDescriptionStr = env->GetMethodID(avprogramClazz, "setDescriptionStr", "(Ljava/lang/String;)V");	
    avprogram_fields.setPrice = env->GetMethodID(avprogramClazz, "setPrice", "(Ljava/lang/String;)V");	
    avprogram_fields.setResolutionStr = env->GetMethodID(avprogramClazz, "setResolutionStr", "(Ljava/lang/String;)V");	
    avprogram_fields.setAudioStr = env->GetMethodID(avprogramClazz, "setAudioStr", "(Ljava/lang/String;)V");	
    avprogram_fields.setEventid = env->GetMethodID(avprogramClazz, "setEventid", "(I)V");	
    avprogram_fields.setRating = env->GetMethodID(avprogramClazz, "setRating", "(I)V");	
    avprogram_fields.setDuration = env->GetMethodID(avprogramClazz, "setDuration", "(I)V");	
    avprogram_fields.setStartTime = env->GetMethodID(avprogramClazz, "setStartTime", "(Ljava/lang/String;)V");	
    avprogram_fields.setEndTime = env->GetMethodID(avprogramClazz, "setEndTime", "(Ljava/lang/String;)V");	
    avprogram_fields.setStartYear = env->GetMethodID(avprogramClazz, "setStartYear", "(I)V");	
    avprogram_fields.setStartMonth = env->GetMethodID(avprogramClazz, "setStartMonth", "(I)V");	
    avprogram_fields.setStartDay = env->GetMethodID(avprogramClazz, "setStartDay", "(I)V");	
    avprogram_fields.setEndYear = env->GetMethodID(avprogramClazz, "setEndYear", "(I)V");	
    avprogram_fields.setEndMonth = env->GetMethodID(avprogramClazz, "setEndMonth", "(I)V");	
    avprogram_fields.setEndDay = env->GetMethodID(avprogramClazz, "setEndDay", "(I)V");	
    avprogram_fields.setLang = env->GetMethodID(avprogramClazz, "setLang", "(Ljava/lang/String;)V");	
    avprogram_fields.setActors = env->GetMethodID(avprogramClazz, "setActors", "(Ljava/lang/String;)V");	
    avprogram_fields.setIsCaptionStr = env->GetMethodID(avprogramClazz, "setIsCaptionStr", "(Ljava/lang/String;)V");	
    avprogram_fields.setContentNibble1 = env->GetMethodID(avprogramClazz, "setContentNibble1", "(I)V");	
    avprogram_fields.setContentNibble2 = env->GetMethodID(avprogramClazz, "setContentNibble2", "(I)V");	
    avprogram_fields.setContentUserNibble1 = env->GetMethodID(avprogramClazz, "setContentUserNibble1", "(I)V");	
    avprogram_fields.setContentUserNibble2 = env->GetMethodID(avprogramClazz, "setContentUserNibble2", "(I)V");	
    avprogram_fields.setDirector = env->GetMethodID(avprogramClazz, "setDirector", "(Ljava/lang/String;)V");	
    avprogram_fields.setIsDolbyAudioStr = env->GetMethodID(avprogramClazz, "setIsDolbyAudioStr", "(Ljava/lang/String;)V");	
    avprogram_fields.setisDvsStr = env->GetMethodID(avprogramClazz, "setisDvsStr", "(Ljava/lang/String;)V");	
    avprogram_fields.setImagePath = env->GetMethodID(avprogramClazz, "setImagePath", "(Ljava/lang/String;)V");	
    avprogram_fields.setRunningStatus = env->GetMethodID(avprogramClazz, "setRunningStatus", "(I)V");	
    avprogram_fields.setVodId = env->GetMethodID(avprogramClazz, "setVodId", "(Ljava/lang/String;)V");	
    avprogram_fields.setIsSignLangOn = env->GetMethodID(avprogramClazz, "setIsSignLangOn", "(I)V");	

    jclass avprogramslinkinfoClazz = env->FindClass(classAVProgramLinkInfo);
    mAVProgramLinkInfoClass = (jclass) env->NewGlobalRef(avprogramslinkinfoClazz);
    avprogram_fields.AVProgramLinkInfoConstructor = env->GetMethodID(avprogramslinkinfoClazz, "<init>", "()V");
    avprogram_fields.setButtonImagePath = env->GetMethodID(avprogramslinkinfoClazz, "setButtonImagePath", "(Ljava/lang/String;)V");
    avprogram_fields.setButtonType = env->GetMethodID(avprogramslinkinfoClazz, "setButtonType", "(I)V");
    avprogram_fields.setCMenuValue = env->GetMethodID(avprogramslinkinfoClazz, "setCMenuValue", "(I)V");
    avprogram_fields.setDisplayStartDateStr = env->GetMethodID(avprogramslinkinfoClazz, "setDisplayStartDateStr", "(Ljava/lang/String;)V");
    avprogram_fields.setDisplayEndDateStr = env->GetMethodID(avprogramslinkinfoClazz, "setDisplayEndDateStr", "(Ljava/lang/String;)V");	
    avprogram_fields.setLinkedServiceText = env->GetMethodID(avprogramslinkinfoClazz, "setLinkedServiceText", "(Ljava/lang/String;)V");	
    avprogram_fields.setLinkedServiceType = env->GetMethodID(avprogramslinkinfoClazz, "setLinkedServiceType", "(I)V");	
    avprogram_fields.setVasItemId = env->GetMethodID(avprogramslinkinfoClazz, "setVasItemId", "(Ljava/lang/String;)V");	
    avprogram_fields.setVasPath = env->GetMethodID(avprogramslinkinfoClazz, "setVasPath", "(Ljava/lang/String;)V");	
    avprogram_fields.setVasServiceId = env->GetMethodID(avprogramslinkinfoClazz, "setVasServiceId", "(Ljava/lang/String;)V");	

    jclass eventClazz = env->FindClass(classDsmccSignalEvent);
    mEventClass = (jclass) env->NewGlobalRef(eventClazz);
    Dsmccfields.mEventConstructor = env->GetMethodID(eventClazz, "<init>", "()V");

    jclass appClazz = env->FindClass(classAppInfo);
    mAppInfoClass = (jclass) env->NewGlobalRef(appClazz);
    Dsmccfields.mAppInfoConstructor = env->GetMethodID(appClazz, "<init>", "()V");

    field fields_to_find[] = {
    	{ classDsmccSignalEvent, "mEventType", "I", &Dsmccfields.mEventType },
    	{ classDsmccSignalEvent, "mChannelNum", "I", &Dsmccfields.mChannelNum },
    	{ classDsmccSignalEvent, "mDemux", "I", &Dsmccfields.mDemux },
    	{ classDsmccSignalEvent, "mUri", "Ljava/lang/String;", &Dsmccfields.mUri },
    	{ classDsmccSignalEvent, "mAppInfoList", "[Lcom/skb/btv/framework/navigator/AppInfo;", &Dsmccfields.mAppInfoList},

    	{ classAppInfo, "mAppType",   "I", &Dsmccfields.mAppType },
    	{ classAppInfo, "mOrganizationId",	 "J", &Dsmccfields.mOrganizationId },
    	{ classAppInfo, "mApplicationId",	"J", &Dsmccfields.mApplicationId },
    	{ classAppInfo, "mDsmccPid",  "I", &Dsmccfields.mDsmccPid },
    	{ classAppInfo, "mIsServiceBound", "Z", &Dsmccfields.mIsServiceBound },
    	{ classAppInfo, "mControlCode", "I", &Dsmccfields.mControlCode},
    	{ classAppInfo, "mName", "Ljava/lang/String;", &Dsmccfields.mName},
    	{ classAppInfo, "mPriority", "I", &Dsmccfields.mPriority},
    	{ classAppInfo, "mInitialPath", "Ljava/lang/String;", &Dsmccfields.mInitialPath },
	};

    find_fields(env, fields_to_find, NELEM(fields_to_find));	

}


DvbsiDataHandler::~DvbsiDataHandler()
{
    // remove global references
    JNIEnv *env = getJniEnv();	
    if (env == NULL) {
        return;
    }
    if (mClass != NULL) {
        env->DeleteGlobalRef(mClass);
        mClass = NULL;
    }	
    if (mAVProgramListClass != NULL) {
        env->DeleteGlobalRef(mAVProgramListClass);
        mAVProgramListClass = NULL;
    }	
    if (mAVProgramsClass != NULL) {
        env->DeleteGlobalRef(mAVProgramsClass);
        mAVProgramsClass = NULL;
    }	
    if (mAVProgramClass != NULL) {
        env->DeleteGlobalRef(mAVProgramClass);
        mAVProgramClass = NULL;
    }
    if (mAVProgramLinkInfoClass != NULL) {
        env->DeleteGlobalRef(mAVProgramLinkInfoClass);
        mAVProgramLinkInfoClass = NULL;
    }	
    if (mEventClass != NULL) {
        env->DeleteGlobalRef(mEventClass);
        mEventClass = NULL;
    }	
    if (mAppInfoClass != NULL) {
        env->DeleteGlobalRef(mAppInfoClass);
        mAppInfoClass = NULL;
    }	
}

jobject DvbsiDataHandler::programEntity(JNIEnv* env, AVProgram_t* program)
{
	jobject avprogram = env->NewObject(mAVProgramClass, avprogram_fields.AVProgramConstructor);
	jstring jstrName;
	
	env->CallVoidMethod(avprogram, avprogram_fields.setChannelId, program->serviceid); 
	
	jstrName = env->NewStringUTF(program->eventName);
	env->CallVoidMethod(avprogram, avprogram_fields.setNameStr, jstrName); 
	env->DeleteLocalRef(jstrName);
	
	jstrName = env->NewStringUTF(program->eventDesc);
	env->CallVoidMethod(avprogram, avprogram_fields.setDescriptionStr, jstrName); 
	env->DeleteLocalRef(jstrName);
	
	jstrName = env->NewStringUTF(program->item_price);
	env->CallVoidMethod(avprogram, avprogram_fields.setPrice, jstrName); 
	env->DeleteLocalRef(jstrName);
	
	jstrName = env->NewStringUTF(program->resolution);
	env->CallVoidMethod(avprogram, avprogram_fields.setResolutionStr, jstrName);
	env->DeleteLocalRef(jstrName);
	
	jstrName = env->NewStringUTF(program->audio);
	env->CallVoidMethod(avprogram, avprogram_fields.setAudioStr, jstrName);
	env->DeleteLocalRef(jstrName);
	
	env->CallVoidMethod(avprogram, avprogram_fields.setEventid, program->eventid); 
	env->CallVoidMethod(avprogram, avprogram_fields.setRating, program->rating); 
	env->CallVoidMethod(avprogram, avprogram_fields.setDuration, program->duration); 
	
	jstrName = env->NewStringUTF(program->startTime);
	env->CallVoidMethod(avprogram, avprogram_fields.setStartTime, jstrName);
	env->DeleteLocalRef(jstrName);
	
	jstrName = env->NewStringUTF(program->endTime);
	env->CallVoidMethod(avprogram, avprogram_fields.setEndTime, jstrName);
	env->DeleteLocalRef(jstrName);
	
	env->CallVoidMethod(avprogram, avprogram_fields.setStartYear, program->startYear); 
	env->CallVoidMethod(avprogram, avprogram_fields.setStartMonth, program->startMonth); 
	env->CallVoidMethod(avprogram, avprogram_fields.setStartDay, program->startDay); 
	env->CallVoidMethod(avprogram, avprogram_fields.setEndYear, program->endYear); 
	env->CallVoidMethod(avprogram, avprogram_fields.setEndMonth, program->endMonth); 
	env->CallVoidMethod(avprogram, avprogram_fields.setEndDay, program->endDay); 
	
	jstrName = env->NewStringUTF(program->lang);
	env->CallVoidMethod(avprogram, avprogram_fields.setLang, jstrName);
	env->DeleteLocalRef(jstrName);
	
	jstrName = env->NewStringUTF(program->item_actor);
	env->CallVoidMethod(avprogram, avprogram_fields.setActors, jstrName);
	env->DeleteLocalRef(jstrName);
	
	jstrName = env->NewStringUTF(program->isCaption);
	env->CallVoidMethod(avprogram, avprogram_fields.setIsCaptionStr, jstrName);
	env->DeleteLocalRef(jstrName);
	
	env->CallVoidMethod(avprogram, avprogram_fields.setContentNibble1, program->contentNibble1); 
	env->CallVoidMethod(avprogram, avprogram_fields.setContentNibble2, program->contentNibble2); 
	env->CallVoidMethod(avprogram, avprogram_fields.setContentUserNibble1, program->contentUserNibble1); 
	env->CallVoidMethod(avprogram, avprogram_fields.setContentUserNibble2, program->contentUserNibble2); 
	
	jstrName = env->NewStringUTF(program->item_director);
	env->CallVoidMethod(avprogram, avprogram_fields.setDirector, jstrName);
	env->DeleteLocalRef(jstrName);
	
	jstrName = env->NewStringUTF(program->isDolbyAudio);
	env->CallVoidMethod(avprogram, avprogram_fields.setIsDolbyAudioStr, jstrName);
	env->DeleteLocalRef(jstrName);
	
	jstrName = env->NewStringUTF(program->isDvs);
	env->CallVoidMethod(avprogram, avprogram_fields.setisDvsStr, jstrName);
	env->DeleteLocalRef(jstrName);
	
	jstrName = env->NewStringUTF(program->imagePath);
	env->CallVoidMethod(avprogram, avprogram_fields.setImagePath, jstrName);
	env->DeleteLocalRef(jstrName);
	
	env->CallVoidMethod(avprogram, avprogram_fields.setRunningStatus, program->runningStatus); 
	
	jstrName = env->NewStringUTF(program->vodid);
	env->CallVoidMethod(avprogram, avprogram_fields.setVodId, jstrName);
	env->DeleteLocalRef(jstrName);
	
	env->CallVoidMethod(avprogram, avprogram_fields.setIsSignLangOn, program->signLanguage); 

	return avprogram;
}

jobject DvbsiDataHandler::programLinkEntity(JNIEnv* env, AVProgramLinkInfo_t* linkinfo)
{
	jobject programlinkinfo = env->NewObject(mAVProgramLinkInfoClass, avprogram_fields.AVProgramLinkInfoConstructor);
	jstring jstrName;			
	
	jstrName = env->NewStringUTF(linkinfo->buttonImagePath);
	env->CallVoidMethod(programlinkinfo, avprogram_fields.setButtonImagePath, jstrName);
	env->DeleteLocalRef(jstrName);

	env->CallVoidMethod(programlinkinfo, avprogram_fields.setButtonType, linkinfo->buttonType); 
	env->CallVoidMethod(programlinkinfo, avprogram_fields.setCMenuValue, linkinfo->cMenuValue); 

	jstrName = env->NewStringUTF(linkinfo->displayStartTime);
	env->CallVoidMethod(programlinkinfo, avprogram_fields.setDisplayStartDateStr, jstrName);
	env->DeleteLocalRef(jstrName);
	
	jstrName = env->NewStringUTF(linkinfo->displayEndTime);
	env->CallVoidMethod(programlinkinfo, avprogram_fields.setDisplayEndDateStr, jstrName);
	env->DeleteLocalRef(jstrName);

	jstrName = env->NewStringUTF(linkinfo->linkedServiceText);
	env->CallVoidMethod(programlinkinfo, avprogram_fields.setLinkedServiceText, jstrName);
	env->DeleteLocalRef(jstrName);

	env->CallVoidMethod(programlinkinfo, avprogram_fields.setLinkedServiceType, linkinfo->linkedServiceType); 

	jstrName = env->NewStringUTF(linkinfo->vasItemId);
	env->CallVoidMethod(programlinkinfo, avprogram_fields.setVasItemId, jstrName);
	env->DeleteLocalRef(jstrName);

	jstrName = env->NewStringUTF(linkinfo->vasPath);
	env->CallVoidMethod(programlinkinfo, avprogram_fields.setVasPath, jstrName);
	env->DeleteLocalRef(jstrName);

	jstrName = env->NewStringUTF(linkinfo->vasServiceId);
	env->CallVoidMethod(programlinkinfo, avprogram_fields.setVasServiceId, jstrName);
	env->DeleteLocalRef(jstrName);

	return programlinkinfo;
}

jobject DvbsiDataHandler::programsListEntity(JNIEnv* env, char* data)
{
    DvbsiDataType *dvbsi_ptr;

    char    *buffer_ptr;
	char    *linkcount_ptr;

    uint8_t  link_count = 0;	

    AVProgram_t program;
	AVProgramLinkInfo_t linkinfo;

    dvbsi_ptr = (DvbsiDataType *)data;
    buffer_ptr = &(dvbsi_ptr->value[0]);
	int backnumber = -1;

	jobject avprogramlist = env->NewObject(mAVProgramListClass, avprogram_fields.AVProgramListConstructor);
	jobject avprograms = NULL;
	
	int programs_size = dvbsi_ptr->count;
	for (int i = 0; i < programs_size; i++) {
		memcpy((void*)&program, (void*)buffer_ptr, AVPROGRAM_UNIT_SIZE);
		buffer_ptr += AVPROGRAM_UNIT_SIZE;
		int serviceid = program.serviceid;
		if(backnumber != serviceid){
			if(avprograms != NULL){
				env->CallVoidMethod(avprogramlist, avprogram_fields.addAVProgramsMethod, avprograms);
				env->DeleteLocalRef(avprograms);	
			}
			avprograms = env->NewObject(mAVProgramsClass, avprogram_fields.AVProgramsConstructor, program.serviceid);
	
			jobject avprogram = programEntity(env, &program);
			
			linkcount_ptr = buffer_ptr;
			buffer_ptr += sizeof(uint8_t);
		
			memcpy((void*)&link_count, (void *)linkcount_ptr, sizeof(uint8_t));

			int linkinfo_size = link_count;
			for (int k = 0; k < linkinfo_size; k++) {
				memcpy((void *)&linkinfo, (void*)buffer_ptr, AVPROGRAM_LINK_UNIT_SIZE);
				buffer_ptr += AVPROGRAM_LINK_UNIT_SIZE;

				jobject programlinkinfo = programLinkEntity(env, &linkinfo);

				env->CallVoidMethod(avprogram, avprogram_fields.addAVProgramLinkInfoMethod, programlinkinfo);
				env->DeleteLocalRef(programlinkinfo);
			}
				
			backnumber = serviceid;
	
			env->CallVoidMethod(avprograms, avprogram_fields.addAVProgramMethod, avprogram);
			env->DeleteLocalRef(avprogram);
				
		} else {
	
			jobject avprogram = programEntity(env, &program);
			
			linkcount_ptr = buffer_ptr;
			buffer_ptr += sizeof(uint8_t);
	
			memcpy((void*)&link_count, (void *)linkcount_ptr, sizeof(uint8_t));

			int linkinfo_size = link_count;
			for (int k = 0; k < linkinfo_size; k++) {
	
				memcpy((void *)&linkinfo, (void*)buffer_ptr, AVPROGRAM_LINK_UNIT_SIZE);
				buffer_ptr += AVPROGRAM_LINK_UNIT_SIZE;
	
				jobject programlinkinfo = programLinkEntity(env, &linkinfo);
				
				env->CallVoidMethod(avprogram, avprogram_fields.addAVProgramLinkInfoMethod, programlinkinfo);
				env->DeleteLocalRef(programlinkinfo);
			}
	
			env->CallVoidMethod(avprograms, avprogram_fields.addAVProgramMethod, avprogram);
			env->DeleteLocalRef(avprogram);
		}
	}
	
	if(avprograms != NULL){
		env->CallVoidMethod(avprogramlist, avprogram_fields.addAVProgramsMethod, avprograms);
		env->DeleteLocalRef(avprograms);	
	}
	
	return avprogramlist;
}

void DvbsiDataHandler::dsmccSignalEvent(char* dsmccRootPath, dsmcc_sigevent* event)
{
	int attached = 0;
	dsmcc_application* application;

    LOG_INFO("[%s] dsmccSignalEvent received dsmccRootPath=%s", __FUNCTION__, dsmccRootPath);

	if(mDsmccEventListener == 0) return;
	
    JNIEnv *env = getJniEnv();		
    if(env == NULL)
    {
        jint attachResult = NavigatorHandler::getInstance()->getJavaVM()->AttachCurrentThread(&env, nullptr);
        if (attachResult != JNI_OK) {
			LOG_INFO("[%s][%d] Unable to attach thread. Error %d", __FUNCTION__, __LINE__, attachResult);
			return;
        }
		attached = 1;
    }

    jstring jstrPath = env->NewStringUTF(dsmccRootPath);

    jobject sigEvent = env->NewObject(mEventClass, Dsmccfields.mEventConstructor);

    env->SetIntField(sigEvent, Dsmccfields.mEventType, event->eventType);
    env->SetIntField(sigEvent, Dsmccfields.mChannelNum, event->serviceId);
    env->SetIntField(sigEvent, Dsmccfields.mDemux, event->demux);
	
    jstring jstrUri = env->NewStringUTF(event->uri);
    env->SetObjectField(sigEvent, Dsmccfields.mUri, jstrUri);	
    env->DeleteLocalRef(jstrUri);	

	application = &(event->application);

    int applist_size = 1;
    if (applist_size > 0)
    {
        jobjectArray appList = NULL;
        appList = (jobjectArray) env->NewObjectArray(applist_size, mAppInfoClass, NULL);
        for (int i = 0; i < applist_size; i++) {		
            jobject appInfo = env->NewObject(mAppInfoClass, Dsmccfields.mAppInfoConstructor);

            env->SetIntField(appInfo, Dsmccfields.mAppType, application->appType);
            env->SetLongField(appInfo, Dsmccfields.mOrganizationId, application->organizationId);
            env->SetLongField(appInfo, Dsmccfields.mApplicationId, application->applicationId);
            env->SetIntField(appInfo, Dsmccfields.mDsmccPid, application->dsmccPid);
            env->SetBooleanField(appInfo, Dsmccfields.mIsServiceBound, (application->isServiceBound > 0 ? true : false));
            env->SetIntField(appInfo, Dsmccfields.mControlCode, application->controlCode);
			
            jstring jstrName = env->NewStringUTF(application->name);
            env->SetObjectField(appInfo, Dsmccfields.mName, jstrName);
            env->DeleteLocalRef(jstrName);
			
            env->SetIntField(appInfo, Dsmccfields.mPriority, application->priority);

            jstring jstrInitPath = env->NewStringUTF(application->initialPath);			
            env->SetObjectField(appInfo, Dsmccfields.mInitialPath, jstrInitPath);
            env->DeleteLocalRef(jstrInitPath);

            env->SetObjectArrayElement(appList, i, appInfo);
            env->DeleteLocalRef(appInfo);
    	}
        env->SetObjectField(sigEvent, Dsmccfields.mAppInfoList, appList);
        env->DeleteLocalRef(appList);
    }

    env->CallStaticVoidMethod(mClass,
                              mPostEventFromDsmcc,
                              jstrPath, sigEvent);

    env->DeleteLocalRef(jstrPath);
    env->DeleteLocalRef(sigEvent);

    checkAndClearExceptionFromCallback(env, __FUNCTION__);	
	if(attached == 1){
		NavigatorHandler::getInstance()->getJavaVM()->DetachCurrentThread();
	}
}



void DvbsiDataHandler::audioProgramUpdate(char* filepath, int32_t updateType, int32_t audioPid)
{
	int attached = 0;
    LOG_INFO("[%s] audioProgramUpdate received filepath=%s, updateType=%d, audioPid=%d", __FUNCTION__, filepath, updateType, audioPid);

	if(mAudioProgramUpdateListener == 0) return;
	
    JNIEnv *env = getJniEnv();		
    if(env == NULL)
    {
        jint attachResult = NavigatorHandler::getInstance()->getJavaVM()->AttachCurrentThread(&env, nullptr);
        if (attachResult != JNI_OK) {
			LOG_INFO("[%s][%d] Unable to attach thread. Error %d", __FUNCTION__, __LINE__, attachResult);
			return;
        }
		attached = 1;
    }

    jstring jstrPath = env->NewStringUTF(filepath);

    env->CallStaticVoidMethod(mClass,
                              mPostEventFromAudioProgramUpdate,
                              jstrPath, updateType, audioPid);

    env->DeleteLocalRef(jstrPath);

    checkAndClearExceptionFromCallback(env, __FUNCTION__);	

	if(attached == 1){
		NavigatorHandler::getInstance()->getJavaVM()->DetachCurrentThread();
	}
}

void DvbsiDataHandler::dvbsiUpdate(int32_t updateType)
{
	int attached = 0;
    LOG_INFO("[%s] dvbsiUpdate received updateType=%d", __FUNCTION__, updateType);

	if(mDvbsiUpdateListener == 0) return;

    JNIEnv *env = getJniEnv();
    if(env == NULL)
    {
        jint attachResult = NavigatorHandler::getInstance()->getJavaVM()->AttachCurrentThread(&env, nullptr);
        if (attachResult != JNI_OK) {
			LOG_INFO("[%s][%d] Unable to attach thread. Error %d", __FUNCTION__, __LINE__, attachResult);
			return;
        }
		attached = 1;
    }

    env->CallStaticVoidMethod(mClass,
                              mPostEventFromDvbsiUpdate,
                              updateType);

    checkAndClearExceptionFromCallback(env, __FUNCTION__);	

	if(attached == 1){
		NavigatorHandler::getInstance()->getJavaVM()->DetachCurrentThread();
	}
}

void DvbsiDataHandler::setDsmccEventListener(JNIEnv *env, jobject thiz, int flag)
{
	LOG_INFO("%s start flag:%d", __FUNCTION__, flag);
	mDsmccEventListener = flag;
	LOG_INFO("%s end", __FUNCTION__);	
}

void DvbsiDataHandler::setAudioProgramUpdateListener(JNIEnv *env, jobject thiz, int flag)
{
	LOG_INFO("%s start flag:%d", __FUNCTION__, flag);
	mAudioProgramUpdateListener = flag;
	LOG_INFO("%s end", __FUNCTION__);	
}

void DvbsiDataHandler::setDvbsiUpdateListener(JNIEnv *env, jobject thiz, int flag)
{
	LOG_INFO("%s start flag:%d", __FUNCTION__, flag);
	mDvbsiUpdateListener = flag;
	LOG_INFO("%s end", __FUNCTION__);	
}

void DvbsiDataHandler::setRegionCode(JNIEnv *env, jobject thiz, jstring regionCode)
{
	LOG_INFO("%s start", __FUNCTION__);


	LOG_INFO("%s end", __FUNCTION__);		
}

void DvbsiDataHandler::setSegId(JNIEnv *env, jobject thiz, jint segid)
{
	LOG_INFO("%s start segid:%d", __FUNCTION__, segid);


	LOG_INFO("%s end", __FUNCTION__);		
}

void DvbsiDataHandler::setCurrentChannel(JNIEnv *env, jobject thiz, jstring url)
{
	LOG_INFO("%s start", __FUNCTION__);


	LOG_INFO("%s end", __FUNCTION__);		
}


jobject DvbsiDataHandler::getAllAVQueryData(JNIEnv *env, jobject thiz)
{
	LOG_INFO("%s start", __FUNCTION__);
	jobject result = NULL;

	//ToDo : AVPrograms를 programsListEntity로 변환 필요
	//result = programsListEntity(env, buffer_ptr);

	LOG_INFO("%s end", __FUNCTION__);
    return result;
	
}

jobject DvbsiDataHandler::getAVQueryDataForChannel(JNIEnv* env, jobject thiz, jint ch)
{
	LOG_INFO("%s start ch:%d", __FUNCTION__, ch);
	jobject result = NULL;

	//ToDo : AVPrograms를 programsListEntity로 변환 필요
	//result = programsListEntity(env, buffer_ptr);

	LOG_INFO("%s end", __FUNCTION__);
    return result;
}

jobject DvbsiDataHandler::getAVQueryDataForSid(JNIEnv* env, jobject thiz, jint sid)
{
	LOG_INFO("%s start sid:%d", __FUNCTION__, sid);
	jobject result = NULL;

	//ToDo : AVPrograms를 programsListEntity로 변환 필요
	//result = programsListEntity(env, buffer_ptr);

	LOG_INFO("%s end", __FUNCTION__);
    return result;
}

//This function is not used in this project
jobject DvbsiDataHandler::getMultiAVQueryData(JNIEnv* env, jobject thiz, jstring chlist)
{
	LOG_INFO("%s start", __FUNCTION__);
	jobject result = NULL;

	LOG_INFO("%s end", __FUNCTION__);		
    return result;
}

//This function is not used in this project
jobject DvbsiDataHandler::getAllMusicQueryData(JNIEnv* env, jobject thiz)
{
	LOG_INFO("%s start", __FUNCTION__);
	jobject result = NULL;


	LOG_INFO("%s end", __FUNCTION__);		
    return result;
}

//This function is not used in this project
jobject DvbsiDataHandler::getMusicQueryDataForChannel(JNIEnv* env, jobject thiz, jint ch)
{
	LOG_INFO("%s start ch:%d", __FUNCTION__, ch);
	jobject result = NULL;


	LOG_INFO("%s end", __FUNCTION__);		
    return result;
}

//This function is not used in this project
jobject DvbsiDataHandler::getMusicQueryDataForSid(JNIEnv* env, jobject thiz, jint sid)
{
	LOG_INFO("%s start sid:%d", __FUNCTION__, sid);
	jobject result = NULL;


	LOG_INFO("%s end", __FUNCTION__);		
    return result;

}

//This function is not used in this project
jobject DvbsiDataHandler::getMultiMusicQueryData(JNIEnv* env, jobject thiz, jstring chlist)
{
	LOG_INFO("%s start", __FUNCTION__);
	jobject result = NULL;


	LOG_INFO("%s end", __FUNCTION__);		
    return result;
}