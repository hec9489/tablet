
// Copyright 2019 SK Broadband Co., LTD.
 


#include <jni.h>
#include <stdio.h>
#include "NavigatorHandler.h"

jint JNI_OnLoad(JavaVM* vm, void* /* reserved */)
{
    JNIEnv* env = NULL;
    jint result = -1;
    LOG_INFO("### JNI_OnLoad");

    if(vm->GetEnv((void**)&env, JNI_VERSION_1_6) != JNI_OK)
    {
        LOG_ERROR("ERROR: GetEnv failed");
        goto fail;
    }

    if(NavigatorHandler::getInstance()->JNI_OnLoad(vm, env) != JNI_TRUE){
    	LOG_ERROR("ERROR: NavigatorHandler registerNatives failed");
        goto fail;
    }

    result = JNI_VERSION_1_6;

fail:
    return result;
}

void JNI_OnUnload(JavaVM *vm, void* /* reserved */)
{
	LOG_INFO("### JNI_OnUnload");
}


