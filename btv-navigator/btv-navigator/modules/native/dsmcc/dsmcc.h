
// Copyright 2019 SK Broadband Co., LTD.
 


#ifndef __dsmcc_h__
#define __dsmcc_h__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <signal.h>
#include <fcntl.h>
#include <stdarg.h>
#include <errno.h>
#include <pthread.h>
#include <assert.h>
#define __STDC_FORMAT_MACROS
#include <unistd.h>
#define __STDC_LIMIT_MACROS
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "dsmcc-cache.h"

#define DSMCC_DEBUG_LOG_ENABLE 
//#define DSMCC_PRINT_DEBUG 
//#define DSMCC_MALLOC_PRINT_DEBUG 

#define DSMCC_DATA_BUFFER_CAPACITY	128

#define DSMCC_ROOTPATH "/data/btv_home/tmp/navi_dsmcc"

#define DSMCC_MUSIC_APP_TYPE 0x5789

#define DSMCC_TIME_OUT_SEC 60	// Timeout 1 minute

#define DSMCC_TIME_OUT_5_SEC 5	// Timeout 5 sec

// event types
enum DSMCC_EVENTS
{
    DSMCC_EVENT_SECTION_FILTER_DATA = 0x8001,
    DSMCC_EVENT_DSMCC_SECTION_FILTER_DATA,
    DSMCC_EVENT_APPLICATION_DATA	
};

enum DsmccEvent
{
    OC_DOWNLOAD_COMPLETE = 0,
    DC_DOWNLOAD_COMPLETE = 1,
    OC_OBJECT_UPDATED = 2,
    OME_CHANGE = 3,    
    DOWNLOAD_FAIL = 4,
    INFO_MUSIC_DOWNLOAD = 0x90,
    INFO_MUSIC_CACHE = 0x91	
};

enum DsmccSignalEvent
{
    APP_INFO_SIGNAL = 0,
    APP_INFO_CHANGED = 1
};

enum DsmccUpdate
{
    APP_NEW_DOWN = 0,
    APP_UPDATE_DOWN = 1
};

// expired_action
enum DSMCC_EXPIRED_ACTIONS
{
	DSMCC_EXPIRED_ACTION_NONE = 0x0001,
	DSMCC_EXPIRED_ACTION_DSMCC_REQUEST,
	DSMCC_EXPIRED_ACTION_AIT_REQUEST	
};

struct _dsmcc_event_list;
struct _dsmcc_event_head;

typedef struct _dsmcc_event_list 
{
    int event;
    int param;
    void *data;
    struct _dsmcc_event_list *next;
} dsmcc_event_list_t;

typedef struct _dsmcc_event_head 
{
    struct _dsmcc_event_list *volatile head;
    struct _dsmcc_event_list *last;
    pthread_cond_t cond;
    pthread_mutex_t lock;
} dsmcc_event_head_t;

typedef void (*dsmcc_callback)(void *handle, int event, char* path, void *data);

typedef struct _dsmcc_section_data
{
    void *section_user_param;			//	user section callback parameter
    void *section_data;					//	section data pointer. which have received section.
    unsigned int section_data_size;		//	section data size.
} dsmcc_section_data;

typedef struct _dsmcc_main_event {
    int32_t           mainEvent;
    int32_t           audioPid;
    char              path[128];
} dsmcc_main_event;
 
typedef struct _dsmcc_application{
    int32_t appType;
    int32_t organizationId;
    int32_t applicationId;
    int32_t dsmccPid;	 
    int8_t  isServiceBound;
    char    name[32];
    int32_t controlCode;
    int32_t priority;
    int32_t channelNum;	 
    char    initialPath[32];
} dsmcc_application;

typedef struct _dsmcc_sigevent{
    int32_t           eventType;
    int32_t           serviceId;
    int32_t           demux;
    char              uri[128];
    dsmcc_application application;
} dsmcc_sigevent;

typedef struct dsmcc_ac3_stream_descriptor
{
	uint8_t		stream_type;
	uint16_t	pid;
	char 		sample_rate_code;
	char 		bsid;
	char 		bit_rate_code;
	char 		surround_mode;	
	char 		bsmod;	
	char 		full_svc;	
	char 		langcod;
	char 		langcod2;	
	char 		mainid;	
	char 		priority;		
	char 		asvcflags;
	char 		text_code;		
	int  		num_channels;
	int  		langflag;
	char 		language[4];	
	char 		language2[4];		
	char 		text_desc[128];	
} dsmcc_ac3_stream_descriptor;

typedef struct dsmcc_iso_639_language_descriptor
{
	uint8_t		stream_type;
	uint16_t	pid;
	char 		audio_type;
	char 		language[4];	
} dsmcc_iso_639_language_descriptor;

typedef struct _dsmcc_audio_service_descriptor
{
	dsmcc_ac3_stream_descriptor ac3_stream_descriptor;	
	dsmcc_iso_639_language_descriptor language_descriptor;
} dsmcc_audio_service_descriptor;


typedef struct dsmcc_application_signalling_descriptor
{
	uint16_t	application_type ;
	uint8_t		ait_version;
} dsmcc_application_signalling_descriptor;

typedef struct dsmcc_application_descriptor
{
	uint16_t 	applicaiton_profile;
	uint8_t 	major;
	uint8_t 	minor;
	uint8_t 	micro;
	uint8_t 	service_bound_flag;
	uint8_t 	visibility;
	uint8_t 	application_priority;
	uint8_t 	transport_protocol_label;
} dsmcc_application_descriptor;

typedef struct dsmcc_application_name_descriptor
{
	char language[4];
	char application_name[64];
} dsmcc_application_name_descriptor;

typedef struct dsmcc_dvbj_application_location_descriptor
{
	char base_directory[64];
	char classpath_extension[64];
	char initial_class[64];
} dsmcc_dvbj_application_location_descriptor;

typedef struct dsmcc_application_storage_descriptor
{
	uint8_t 	storage_property;
	uint8_t 	not_launchable_from_broadcast;
	uint8_t 	launchable_completely_from_cache;
	uint8_t 	is_launchable_with_older_version;
	uint32_t	version;
	uint8_t		priority;	
} dsmcc_application_storage_descriptor;

typedef struct dsmcc_application_location_descriptor
{
	char	initial_path_bytes[254];	
} dsmcc_application_location_descriptor;

typedef struct dsmcc_application_tranport_descriptor
{
	uint16_t	protocol_id ;
	uint8_t		transport_protocol_label;
	uint8_t		remote_connection;	
	uint8_t		component_tag;		
} dsmcc_application_tranport_descriptor;

typedef struct
{
	uint16_t	appication_type;
	uint32_t	organisation_id;
	uint16_t	application_id;
	uint8_t		application_control_code;
	uint16_t	application_descriptor_loop_length;	
} dsmcc_appication_unit;

typedef struct dsmcc_application_info_table
{
	dsmcc_appication_unit application;
	dsmcc_application_descriptor application_descriptor;
	dsmcc_application_name_descriptor name_descriptor;
	dsmcc_dvbj_application_location_descriptor dvbj_location_descriptor;
	dsmcc_application_storage_descriptor storage_descriptor;
	dsmcc_application_location_descriptor location_descriptor;	
	dsmcc_application_tranport_descriptor transport_descriptor;
} dsmcc_application_info_table;

typedef struct dsmcc_carousel_id_descriptor
{
	uint32_t	carousel_id;
	uint8_t		format_id;	
} dsmcc_carousel_id_descriptor;

typedef struct dsmcc_association_tag_descriptor
{
	uint16_t	association_tag;
	uint16_t	use;
	uint32_t	transaction_id;
	uint32_t	timeout;
} dsmcc_association_tag_descriptor;

typedef struct dsmcc_broadcast_id_descriptor
{
	uint16_t	data_broadcast_id;
} dsmcc_broadcast_id_descriptor;

typedef struct dsmcc_stream_id_descriptor
{
	uint8_t		component_tag;
} dsmcc_stream_id_descriptor;


typedef struct dsmcc_stream_service
{
	uint8_t		stream_type;
	uint16_t	pid;
	dsmcc_carousel_id_descriptor carousel_id_descriptor;	
	dsmcc_association_tag_descriptor association_tag_descriptor;	
	dsmcc_broadcast_id_descriptor broadcast_id_descriptor;	
	dsmcc_stream_id_descriptor stream_id_descriptor;		
} dsmcc_stream_service;


struct _dsmcc_stream_type;
typedef struct _dsmcc_stream_type {
	uint16_t pid;
	uint16_t assoc_tag;
	struct _dsmcc_stream_type *next;
} dsmcc_stream_type;

typedef struct dsmcc_ait_stream_service
{
	uint8_t		stream_type;
	uint16_t	pid;
	dsmcc_application_signalling_descriptor signalling_descriptor;
} dsmcc_ait_stream_service;

struct obj_carousel {
    dsmcc_application application;
    struct cache *filecache;
    struct cache_module_data *cache; 
    struct dsmcc_dsi *gate;
    unsigned long id;
    unsigned long flag;	
    struct obj_carousel *next;
};

typedef struct dsmcc_ctx
{
    struct obj_carousel *carousels;
    struct obj_carousel *active_carousel;

    char buffer[DSMCC_DATA_BUFFER_CAPACITY];
		
    // event
    dsmcc_event_head_t dsmcc_event;

    // thread stop request flag
    int stop_requested;
    pthread_t dsmcc_thread;

    dsmcc_callback callback;

    void* user_param;

	timer_t timer_id;
	int expired_action;

	dsmcc_application_info_table application_info;

	dsmcc_stream_service dsmcc_stream;
	dsmcc_ait_stream_service ait_stream;
	dsmcc_audio_service_descriptor audio_descriptor;

	dsmcc_stream_type *dsmcc_stream_head;

	uint16_t	dsmcc_pid;
	uint16_t	pmt_pid;

	int on_request_section_filter;
	int on_dsmcc_request_section_filter;
	
    void* handle;
} dsmcc_ctx;

void dsmcc_log_dump(const char* data, int lengths);
void dsmcc_log(const char *format, ...);

void dsmcc_process_send_event_toapp(dsmcc_ctx *ctx, int eventType, int cached);

void dsmcc_post_application_data(dsmcc_ctx *ctx, dsmcc_application *data);
void dsmcc_post_section_filter_data(dsmcc_ctx *ctx, void *data, int size, void *user_param);

void dsmcc_request_pat_data(dsmcc_ctx *ctx);
void dsmcc_request_dsmcc_data(dsmcc_ctx *ctx);

void dsmcc_stop_section_filter(dsmcc_ctx *ctx);
void dsmcc_stop_dsmcc_section_filter(dsmcc_ctx *ctx);
void dsmcc_unset_timer(dsmcc_ctx *ctx);

dsmcc_ctx *dsmcc_init(void *handle, dsmcc_callback callback);
void dsmcc_destroy(dsmcc_ctx *ctx);
void dsmcc_finish(dsmcc_ctx *ctx);


#ifdef __cplusplus
}
#endif

#endif //__dsmcc_h__
