
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;

import java.util.ArrayList;


/**
 * IAVDvbService Interface
 *
 */

public interface IAVDvbService {

    public static final int AVDVBSERVICE_CHANNEL_FREE = 0;
    public static final int AVDVBSERVICE_CHANNEL_PAY = 1;

    public static final int AVDVBSERVICE_CHANNEL_TYPE_RESERVED = 0x00;
    public static final int AVDVBSERVICE_CHANNEL_TYPE_DTV = 0x01;
    public static final int AVDVBSERVICE_CHANNEL_TYPE_DIGIT_RADIO = 0x02;
    public static final int AVDVBSERVICE_CHANNEL_TYPE_MUSIC_CHANNEL = 0x80;
    public static final int AVDVBSERVICE_CHANNEL_TYPE_VIRTUAL_VOD = 0x82;

    public static final int AVDVBSERVICE_RATING_AGE_15 = 15;
    public static final int AVDVBSERVICE_RATING_AGE_18 = 18;
    public static final int AVDVBSERVICE_RATING_AGE_19 = 19;
    public static final int AVDVBSERVICE_RATING_AGE_3 = 3;
    public static final int AVDVBSERVICE_RATING_AGE_7 = 7;
    public static final int AVDVBSERVICE_RATING_ALL = 0;

    public static final int AVDVBSERVICE_RUNNING_STATUS_HIDDEN = 0;
    public static final int AVDVBSERVICE_RUNNING_STATUS_NOT_RUNNING = 1;
    public static final int AVDVBSERVICE_RUNNING_STATUS_START_A_FEW_SECONDS = 2;
    public static final int AVDVBSERVICE_RUNNING_STATUS_PAUSE = 3;
    public static final int AVDVBSERVICE_RUNNING_STATUS_RUNNING = 4;

    public static final int AVDVBSERVICE_VEDIO_RESULTION_SD = 0;
    public static final int AVDVBSERVICE_VEDIO_RESULTION_HD = 1;
    public static final int AVDVBSERVICE_VEDIO_RESULTION_UHD_4K = 2;
    public static final int AVDVBSERVICE_VEDIO_RESULTION_UHD_8K = 3;

    int getSid();
    String getName();
    int getCh();
    int getGenre();
    int getCategory();
    int getLocalAreaCode();
    int getRunningStatus();
    int getRating();
    boolean isPay();
    int getSample();
    int getRes();
    int getPpid();
    ArrayList<String> getProduct();
    String getImagePath();
    String getChannelUri();
    int getSeqId();
    int getChannelType();
    int getSubChannel();
    String getVirtualVodUri();
    ArrayList<ElementaryStream> getElementaryStreamList();
    String getChannelCallsign();
    int getOcPid();
    String getSignLangVideoUrl();
}
