package com.skb.btv.framework.navigator;


public interface OnDsmccListener {
    void onDsmccEvent(String path, SignalEvent sigevent);
}
