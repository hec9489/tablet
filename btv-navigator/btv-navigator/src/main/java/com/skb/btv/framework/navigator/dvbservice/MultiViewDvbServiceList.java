
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;

import android.os.Parcel;
import android.os.Parcelable;

import com.skb.btv.framework.navigator.IBtvNavigator;
import com.tickaroo.tikxml.annotation.Element;
import com.tickaroo.tikxml.annotation.Xml;

import java.util.ArrayList;
import java.util.List;


/**
 * MultiViewDvbServiceList Class
 *
 */

@Xml(name = "channels")
public class MultiViewDvbServiceList extends DvbServiceList implements IMultiViewDvbServiceList {

    private static final String TAG =  "MultiViewDvbServiceList";

    boolean mReady;

    @SuppressWarnings("WeakerAccess")
    @Element
    List<MultiViewDvbService> multiViewChannelList;

    public MultiViewDvbServiceList(){
    }

    public MultiViewDvbServiceList(boolean ready){
        mReady = ready;
    }

    public MultiViewDvbServiceList(int ready, List<MultiViewDvbService> list){
        mReady = ready == 1 ? true : false;
        multiViewChannelList = list;
    }

    protected MultiViewDvbServiceList(Parcel in) {
        mReady = in.readByte() != 0;
        multiViewChannelList = in.createTypedArrayList(MultiViewDvbService.CREATOR);
    }

    public static final Parcelable.Creator<MultiViewDvbServiceList> CREATOR = new Parcelable.Creator<MultiViewDvbServiceList>() {
        /**
         * create service from parcel
         * @param in
         * @return MultiViewDvbServiceList
         */
        @Override
        public MultiViewDvbServiceList createFromParcel(Parcel in) {
            return new MultiViewDvbServiceList(in);
        }

        /**
         * create service array
         * @param size
         * @return MultiViewDvbServiceList[]
         */
        @Override
        public MultiViewDvbServiceList[] newArray(int size) {
            return new MultiViewDvbServiceList[size];
        }
    };

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (mReady ? 1 : 0));
        dest.writeTypedList(multiViewChannelList);
    }

    /**
     * Get ready state
     * @return  ready state
     */
    @Override
    public boolean isReady() {
        if(multiViewChannelList != null && multiViewChannelList.size() > 0){
            mReady = true;
        } else {
            mReady = false;
        }
        return mReady;
    }

    /**
     * Get MultiViewDvbService list
     * @return  MultiViewDvbService list
     */
    @Override
    public ArrayList<MultiViewDvbService> getList() {
        return (ArrayList<MultiViewDvbService>)multiViewChannelList;
    }

    /**
     * Get MultiViewDvbService type
     * @return  MultiViewDvbService type DVBSERVICE_TYPE_MULTIVIEW
     */
    @Override
    public int getRequestType(){
        return IBtvNavigator.DVBSERVICE_TYPE_MULTIVIEW;
    }
}
