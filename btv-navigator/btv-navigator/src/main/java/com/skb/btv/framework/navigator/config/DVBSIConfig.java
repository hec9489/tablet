
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.config;

import com.tickaroo.tikxml.annotation.Element;
import com.tickaroo.tikxml.annotation.Xml;

import java.util.ArrayList;
import java.util.List;


/**
 * DVBSIConfig Class
 *
 */

@Xml(name = "properties")
public class DVBSIConfig {

    boolean mReady;

    @SuppressWarnings("WeakerAccess")
    @Element
    List<PropertiesConfig> entries;

    private String newRegionCode;
    private int segId = -1;

    DVBSIConfig(){}

    public DVBSIConfig(boolean ready) {
        mReady = ready;
    }

    /**
     * set region code
     * @param newRegionCode
     */
    public void setNewRegionCode(String newRegionCode) {
        this.newRegionCode = newRegionCode;
    }

    /**
     * set Seg. Id
     * @param segId
     */
    public void setSegId(int segId){
        this.segId = segId;
    }

    /**
     * generate config xml string
     * @param isRegionCode
     * @param isSegId
     * @return String
     */
    public String generateXML(boolean isRegionCode, boolean isSegId) {
        String xml = "";
        if(entries != null) {
            xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                    "\n<properties>";
            xml += "\n\t<comment>DVBSI-PROPERTIES</comment>";

            for(PropertiesConfig entry : entries) {
                if(entry.key.equals("codes") && newRegionCode != null && isRegionCode) {
                    xml += "\n\t<entry key=\"" + entry.key + "\">" + newRegionCode + "</entry>";
                } else if(entry.key.equals("segid") && isSegId){
                    xml += "\n\t<entry key=\"" + entry.key + "\">" + segId+ "</entry>";
                } else {
                    xml += "\n\t<entry key=\"" + entry.key + "\">" + entry.value + "</entry>";
                }
            }
            xml += "\n</properties>";
        }
        return xml;
    }

    /**
     * get config entry list
     * @return ArrayList<PropertiesConfig>
     */
    public ArrayList<PropertiesConfig> getEntriesList(){
        return (ArrayList<PropertiesConfig>)entries;
    }

}
