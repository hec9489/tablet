
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.skb.btv.framework.navigator.log.SLog;
import com.skb.btv.framework.navigator.program.AVProgramList;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;


/**
 * BtvJniInterface Class
 *
 */

// 2020.02.12 : add tvPowerOn api
public class BtvJniInterface {

    static {
        System.loadLibrary("navigator_jni.skb");
        navigator_init();
    }

    public static final String TAG = "BtvJniInterface";
    public static final String BUILD_DATE = "2020.06.18";
    public Context mContext = null;
    private int mNavigatorContext;

    private static native void navigator_init();

    private native void navigator_dvbsi_start();
    private native void navigator_dvbsi_release();

    // dsmcc event Listener
    private native void navigator_setDsmccEventListener(Object BtvJniInterface);

    // audio program update
    private native void navigator_setAudioProgramUpdateListener(Object BtvJniInterface);

    // DVBSI related
    private native void navigator_setDvbsiUpdateListener(Object BtvJniInterface);

    private native void navigator_setRegionCode(String regionCode);
    private native void navigator_setSegId(int segid);
    private native void navigator_setCurrentChannel(String url);

    private native AVProgramList navigator_getAllAVQueryData();
    private native AVProgramList navigator_getAVQueryDataForChannel(int ch);
    private native AVProgramList navigator_getAVQueryDataForSid(int sid);
    private native AVProgramList navigator_getMultiAVQueryData(String chlist);

    private native AVProgramList navigator_getAllMusicQueryData();
    private native AVProgramList navigator_getMusicQueryDataForChannel(int ch);
    private native AVProgramList navigator_getMusicQueryDataForSid(int sid);
    private native AVProgramList navigator_getMultiMusicQueryData(String chlist);

    private native void navigator_watermark_start();
    private native int navigator_watermark_release();
    private native boolean navigator_watermark_isPrepared();
    private native boolean navigator_watermark_isActivated();
    private native int navigator_watermark_setStbId(short msb, int lsb);
    private native int navigator_watermark_setStubEmbedding(int stubType);
    private native int navigator_watermark_setActive(int active);
    private native int navigator_watermark_processSurface(int[] output);

    private native void navigator_slogi(String log);
    private native void navigator_slogd(String log);
    private native void navigator_sloge(String log);

    private static BtvJniInterface _instance;

    /**
     * get BtvJniInterface instance
     * @param context
     * @return BtvJniInterface
     */
    public static BtvJniInterface getInstance(Context context)
    {
        if(_instance == null) {
            _instance = new BtvJniInterface(context);
        }
        return _instance;
    }

    public static BtvJniInterface getInstance()
    {
        if(_instance == null) {
            _instance = new BtvJniInterface();
        }
        return _instance;
    }

    private BtvJniInterface(Context context) {
        mContext = context;
    }

    private BtvJniInterface() {
    }

    public interface AudioProgramUpdateListener
    {
        void onUpdate(String filepath, int updateType, int audioPid);
    }

    /**
     * set audio program updated listener
     * @param listener AudioProgramUpdateListener
     */
    public void setAudioProgramUpdateListener(AudioProgramUpdateListener listener)
    {
        Log.d(TAG, "setAudioProgramUpdateListener");
        navigator_setAudioProgramUpdateListener(new WeakReference<BtvJniInterface>(this));
        mAudioProgramUpdateListener = listener;
    }

    static private AudioProgramUpdateListener mAudioProgramUpdateListener;

    /**
     * post audio program updated event
     * @param filepath
     * @param updateType
     * @param audioPid
     */
    private static void postEventFromAudioProgramUpdate(String filepath, int updateType, int audioPid)
    {
        if(mAudioProgramUpdateListener != null) {
            Log.d(TAG, "postEventFromAudioProgramUpdate filepath = " + filepath + ", updateType=" + updateType + ", audioPid = " + audioPid );
            mAudioProgramUpdateListener.onUpdate(filepath, updateType, audioPid);
        }
    }

    public interface OnDsmccCallbackListener
    {
        void onDsmccEventUpdated(String path, SignalEvent event);
    }

    public void setOnDsmccCallbackListener(OnDsmccCallbackListener listener)
    {
        Log.d(TAG, "setOnDsmccCallbackListener");
        navigator_setDsmccEventListener(new WeakReference<BtvJniInterface>(this));
        mOnDsmccCallbackListener = listener;
    }

    static private OnDsmccCallbackListener mOnDsmccCallbackListener;

    private static void postEventFromDsmcc(String rootPath, SignalEvent dsmccSignalEvent)
    {
        if(mOnDsmccCallbackListener != null) {
            Log.d(TAG, "postEventFromDsmcc rootPath =" + rootPath);
            mOnDsmccCallbackListener.onDsmccEventUpdated(rootPath, dsmccSignalEvent);
        }
    }

    // DVBSI related
    public interface DvbsiUpdateListener
    {
        void onUpdate(int updateType);
    }

    /**
     * set dvbsi update listener
     * @param listener DvbsiUpdateListener
     */
    public void setDvbsiUpdateListener(DvbsiUpdateListener listener)
    {
        Log.d(TAG, "setDvbUpdateListener");
        navigator_setDvbsiUpdateListener(new WeakReference<BtvJniInterface>(this));
        mDvbsiUpdateListener = listener;
    }

    static private DvbsiUpdateListener mDvbsiUpdateListener;

    /**
     * post dvbsi updated event
     * @param updateType
     */
    private static void postEventFromDvbsiUpdate(int updateType)
    {
        if(mDvbsiUpdateListener != null) {
            Log.d(TAG, "postEventFromDvbsiUpdate updateType=" + updateType);
            mDvbsiUpdateListener.onUpdate(updateType);
        }
    }

    /**
     * start native dvbsi
     */
    public void dvbsi_start(){
        SLog.d(TAG, "dvb_start");
        navigator_dvbsi_start();
    }

    /**
     * release native dvbsi
     */
    public void dvbsi_release(){
        SLog.d(TAG, "dvbsi_release");
        navigator_dvbsi_release();
    }

    /**
     * set region code
     * @param regionCode
     */
    public void setRegionCode(String regionCode){
        SLog.d(TAG, "setRegionCode regionCode:" + regionCode);
        navigator_setRegionCode(regionCode);
    }

    /**
     * set seg id
     * @param segid
     */
    public void setSegId(int segid){
        SLog.d(TAG, "setSegId segid:" + segid);
        navigator_setSegId(segid);
    }

    /**
     * set current channel
     * @param url String
     */
    public void setCurrentChannel(String url){
        SLog.d(TAG, "setCurrentChannel url:" + url);
        navigator_setCurrentChannel(url);
    }

    /**
     * get all av program list
     * @return AVProgramList
     */
    public AVProgramList getAllAVQueryData() {
        SLog.d(TAG, "getAllAVQueryData");
        return navigator_getAllAVQueryData();
    }

    /**
     * get av program list for channel
     * @param ch channel
     * @return AVProgramList
     */
    public AVProgramList getAVQueryDataForChannel(int ch){
        SLog.d(TAG, "getAVQueryDataForChannel ch:" + ch);
        return navigator_getAVQueryDataForChannel(ch);
    }

    /**
     * get av program list for sid
     * @param sid
     * @return AVProgramList
     */
    public AVProgramList getAVQueryDataForSid(int sid){
        SLog.d(TAG, "getAVQueryDataForSid sid:" + sid);
        return navigator_getAVQueryDataForSid(sid);
    }

    /**
     * get multi view av program list for channel list
     * @param chlist channel list
     * @return AVProgramList
     */
    public AVProgramList getMultiAVQueryData(String chlist){
        SLog.d(TAG, "getMultiAVQueryData chlist:" + chlist);
        return navigator_getMultiAVQueryData(chlist);
    }

    /**
     * get all music program list
     * @return AVProgramList
     */
    public AVProgramList getAllMusicQueryData(){
        SLog.d(TAG, "getAllMusicQueryData");
        return navigator_getAllMusicQueryData();
    }

    /**
     * get music program list for channel
     * @param ch channel
     * @return AVProgramList
     */
    public AVProgramList getMusicQueryDataForChannel(int ch){
        SLog.d(TAG, "getMusicQueryDataForChannel ch:" + ch);
        return navigator_getMusicQueryDataForChannel(ch);
    }

    /**
     * get music program list for sid
     * @param sid
     * @return AVProgramList
     */
    public AVProgramList getMusicQueryDataForSid(int sid){
        SLog.d(TAG, "getMusicQueryDataForSid sid:" + sid);
        return navigator_getMusicQueryDataForSid(sid);
    }

    /**
     * get music program list for channel
     * @param chlist channel list
     * @return AVProgramList
     */
    public AVProgramList getMultiMusicQueryData(String chlist){
        SLog.d(TAG, "getMultiMusicQueryData chlist:" + chlist);
        return navigator_getMultiMusicQueryData(chlist);
    }

    /**
     * start native watermark service
     */
    public void initWatermark() {
        SLog.i(TAG, "initWatermark - call navigator_watermark_start");
        navigator_watermark_start();
    }

    /**
     * release native watermark service
     */
    public int releaseWatermark() {
        SLog.i(TAG, "releaseWatermark - call navigator_watermark_release");
        return navigator_watermark_release();
    }

    /**
     * get watermark service prepared state
     * @return boolean : is prepared
     */
    public boolean isWmPrepared() {
        SLog.i(TAG, "isWmPrepared - call navigator_watermark_isPrepared");
        return navigator_watermark_isPrepared();
    }

    /**
     * get watermark service activated state
     * @return boolean : is activated
     */
    public boolean isWmActivated() {
        SLog.i(TAG, "isWmActivated - call navigator_watermark_isActivated");
        return navigator_watermark_isActivated();
    }

    /**
     * set STB device ID
     * @param msb : reserved (should be 0)
     * @param lsb : LSB portion of the StbId
     * @return int : native static watermark lib. func. return value
     */
    public int setWmStbId(short msb, int lsb) {
        SLog.d(TAG, "setWmStbId - msb : " + msb + ", lsb : " + lsb);
        return navigator_watermark_setStbId(msb, lsb);
    }

    /**
     * Set the stub pattern
     * @param stubType : 0 disables all visible marks
     * – 1 is reserved
     * – 2 is reserved
     * – 3 activates visible marks for alpha blending test pattern
     * @return int : native static watermark lib. func. return value
     */
    public int setWmStubEmbedding(int stubType) {
        SLog.d(TAG, "setWmStubEmbedding - stubType:" + stubType);
        return navigator_watermark_setStubEmbedding(stubType);
    }

    /**
     * Turns watermark embedding on/off
     * @param active : 1 on 0 off
     * @return int : native static watermark lib. func. return value
     */
    public int setWmActive(int active) {
        SLog.i(TAG, "setWmActive active:" + active);
        return navigator_watermark_setActive(active);
    }

    /**
     * Applies a watermark to an surface
     * @param buffer : output rgb buffer
     * @return int : native static watermark lib. func. return value
     */
    public int processWmSurface(int [] buffer) {
        int ret = navigator_watermark_processSurface(buffer);
        return ret;
    }

    /**
     * file log info
     */
    public void slogi(String log) {
        navigator_slogi(log);
    }

    /**
     * file log debug
     */
    public void slogd(String log) {
        navigator_slogd(log);
    }

    /**
     * file log error
     */
    public void sloge(String log) {
        navigator_sloge(log);
    }


}
