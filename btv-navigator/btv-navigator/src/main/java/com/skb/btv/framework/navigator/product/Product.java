
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.product;

import android.os.Parcel;
import android.os.Parcelable;

import com.skb.btv.framework.navigator.xmlUtils.XmlUtilDateConverter;
import com.skb.btv.framework.navigator.xmlUtils.XmlUtilStringConverter;
import com.tickaroo.tikxml.annotation.Attribute;
import com.tickaroo.tikxml.annotation.Xml;

import java.util.Date;


/**
 * Product Class
 *
 */

@Xml(name = "Product")
public class Product implements IProduct, Parcelable {

    private static final String TAG =  "Product";

    @Attribute(name = "code")
    String code;

    @Attribute(name = "name", converter = XmlUtilStringConverter.class)
    String name;

    @Attribute(name = "type")
    int type;

    @Attribute(name = "start",  converter = XmlUtilDateConverter.class)
    Date start;

    @Attribute(name = "end",  converter = XmlUtilDateConverter.class)
    Date end;

    @Attribute(name = "term")
    int term;

    @Attribute(name = "value")
    int value;

    @Attribute(name = "price")
    int price;

    @Attribute(name = "dc")
    int dC;

    @Attribute(name = "dsc", converter = XmlUtilStringConverter.class)
    String dsc;

    public Product(){

    }

    protected Product(Parcel in) {
        code = in.readString();
        name = in.readString();
        type = in.readInt();
        term = in.readInt();
        value = in.readInt();
        price = in.readInt();
        dC = in.readInt();
        dsc = in.readString();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        /**
         * create service from parcel
         * @param in
         * @return Product
         */
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        /**
         * create service array
         * @param size
         * @return Product[]
         */
        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(name);
        dest.writeInt(type);
        dest.writeInt(term);
        dest.writeInt(value);
        dest.writeInt(price);
        dest.writeInt(dC);
        dest.writeString(dsc);
    }

    /**
     * Get product id (code)
     * @return  product id (code)
     */
    @Override
    public String getCode() {
        return code;
    }

    /**
     * Get product name
     * @return  product name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Get product type
     * @return  product type
     */
    @Override
    public int getType() {
        return type;
    }

    /**
     * Get product start date
     * @return  product start date
     */
    @Override
    public Date getStartDate() {
        return start;
    }

    /**
     * Get product end date
     * @return  product end date
     */
    @Override
    public Date getEndDate() {
        return end;
    }

    /**
     * Get product fg unit
     * @return  product fg unit  : hour, day, week, month, year
     */
    @Override
    public int getTerm() {
        return term;
    }

    /**
     * Get product fg value
     * @return  product fg value
     */
    @Override
    public int getValue() {
        return value;
    }

    /**
     * Get product price
     * @return  product price
     */
    @Override
    public int getPrice() {
        return price;
    }

    /**
     * Get product dc price
     * @return  product dc price
     */
    @Override
    public int getdC() {
        return dC;
    }

    /**
     * Get product dcscription
     * @return  product dcscription
     */
    @Override
    public String getDsc() {
        return dsc;
    }
}
