
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;

import android.os.Parcel;

import com.skb.btv.framework.navigator.IBtvNavigator;
import com.tickaroo.tikxml.annotation.Element;
import com.tickaroo.tikxml.annotation.Xml;

import java.util.ArrayList;
import java.util.List;


/**
 * AVDvbServiceList Class
 *
 */

@Xml(name = "root")
public class AVDvbServiceList extends DvbServiceList implements IAVDvbServiceList {

    private static final String TAG =  "AVDvbServiceList";
    @SuppressWarnings("WeakerAccess")

    boolean mReady;

    @Element
    List<AVDvbService> avChannelList;

    public AVDvbServiceList(){
        super();
    }

    public AVDvbServiceList(boolean ready){
        mReady = ready;
    }

    public AVDvbServiceList(int ready, List<AVDvbService> list){
        mReady = ready == 1 ? true : false;
        avChannelList = list;
    }

    protected AVDvbServiceList(Parcel in) {
        super(in);
        mReady = in.readByte() != 0;
        avChannelList = in.createTypedArrayList(AVDvbService.CREATOR);
    }

    public static final Creator<AVDvbServiceList> CREATOR = new Creator<AVDvbServiceList>() {
        /**
         * create service from parcel
         * @param in
         * @return AVDvbServiceList
         */
        @Override
        public AVDvbServiceList createFromParcel(Parcel in) {
            return new AVDvbServiceList(in);
        }

        /**
         * create service array
         * @param size
         * @return AVDvbServiceList[]
         */
        @Override
        public AVDvbServiceList[] newArray(int size) {
            return new AVDvbServiceList[size];
        }
    };

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeByte((byte) (mReady ? 1 : 0));
        dest.writeTypedList(avChannelList);
    }

    /**
     * Get ready state
     * @return  ready state
     */
    @Override
    public boolean isReady() {
        if(avChannelList != null && avChannelList.size() > 0){
            mReady = true;
        } else {
            mReady = false;
        }
        return mReady;
    }

    /**
     * Get AVDvbService list
     * @return  AVDvbService list
     */
    @Override
    public ArrayList<AVDvbService> getList() {
        return (ArrayList<AVDvbService>)avChannelList;
    }

    /**
     * add AV channel info
     * @param chInfo
     */
    public void addAVChannel(AVDvbService chInfo){
        if(avChannelList == null){
            avChannelList = new ArrayList<>();
        }
        avChannelList.add(chInfo);
    }

    /**
     * Get AVDvbService type
     * @return  AVDvbService type DVBSERVICE_TYPE_AV
     */
    @Override
    public int getRequestType(){
        return IBtvNavigator.DVBSERVICE_TYPE_AV;
    }
}
