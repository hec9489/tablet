
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;


/**
 * IMusicDvbService Interface
 *
 */

public interface IMusicDvbService {
    int getChannelType();
    int getAudioChannelType();
    int getApid();
    int getAst();
    int getCh_no();
    int getCh();
    String getChannelUri();
    String getImagePath();
    int getOpid();
    int getPpid();
    String getTitle();
}
