
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.program;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * IAVProgram Interface
 *
 */

public interface IAVProgram {

    public static final int AVPROGRAM_CAPTION_NO = 0;
    public static final int AVPROGRAM_CAPTION_YES = 1;

    public static final int AVPROGRAM_DOLBY_NO = 0;
    public static final int AVPROGRAM_DOLBY_YES = 1;

    public static final int AVPROGRAM_DVS_NO = 0;
    public static final int AVPROGRAM_DVS_YES = 1;

    public static final int AVPROGRAM_AUDIO_TYPE_NONE = 0;
    public static final int AVPROGRAM_AUDIO_TYPE_MONO = 1;
    public static final int AVPROGRAM_AUDIO_TYPE_STEREO = 2;
    public static final int AVPROGRAM_AUDIO_TYPE_AC3 = 3;

    public static final int AVPROGRAM_RATING_AGE_12 = 12;
    public static final int AVPROGRAM_RATING_AGE_15 = 15;
    public static final int AVPROGRAM_RATING_AGE_18 = 18;
    public static final int AVPROGRAM_RATING_AGE_19 = 19;
    public static final int AVPROGRAM_RATING_AGE_3 = 3;
    public static final int AVPROGRAM_RATING_AGE_7 = 7;
    public static final int AVPROGRAM_RATING_ALL = 0;

    public static final int AVPROGRAM_VEDIO_RESULTION_SD = 0;
    public static final int AVPROGRAM_VEDIO_RESULTION_HD = 1;
    public static final int AVPROGRAM_VEDIO_RESULTION_UHD_4K = 2;
    public static final int AVPROGRAM_VEDIO_RESULTION_UHD_8K = 3;

    public static final int AVPROGRAM_CHANNEL_FREE = 0;
    public static final int AVPROGRAM_CHANNEL_PAY = 1;

    public static final int AVPROGRAM_CHANNEL_TYPE_RESERVED = 0x00;
    public static final int AVPROGRAM_CHANNEL_TYPE_DTV = 0x01;
    public static final int AVPROGRAM_CHANNEL_TYPE_DIGIT_RADIO = 0x02;
    public static final int AVPROGRAM_CHANNEL_TYPE_AUDIO_CHANNEL = 0x80;

    String getActors();
    int getContentNibble1();
    int getContentNibble2();
    int getContentUserNibble1();
    int getContentUserNibble2();
    String getDirector();
    int getDuration();
    Date getEndDate();
    String getPrice();
    String getDescription();
    String getImagePath();
    ArrayList<AVProgramLinkInfo> getAVProgramLinkInfo();
    String getName();
    int getEventid();
    int getRating();
    int getRunningStatus();
    Date getStartDate();
    String getVodId();
    boolean isCaption();
    boolean isDolbyAudio();
    boolean isDvs();
    int getResolution();
    int getChannelId();
    int getAVProgramAudioType();
    List<AVProgramAudioType> getAVProgramAudioTypeList();
    int getProgramId();
    boolean isSignLangOn();
}
