
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.xmlUtils;

import com.tickaroo.tikxml.TypeConverter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * XmlUtilDateConverter Class
 *
 */

public class XmlUtilDateConverter implements TypeConverter<Date> {
    /**
     * get Date from String
     * @param s String
     * @return Date
     */
    @Override
    public Date read(String s) throws Exception {
        Date date;
        try {
            date = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREAN).parse(s);
        } catch (ParseException e) {
            date = new Date(0);
        }
        return date;
    }

    /**
     * get String from Date
     * @param date
     * @return String
     */
    @Override
    public String write(Date date) throws Exception {
        return new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREAN).format(date);
    }
}
