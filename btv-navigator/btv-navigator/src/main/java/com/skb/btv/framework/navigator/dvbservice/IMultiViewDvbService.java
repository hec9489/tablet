
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;

import java.util.ArrayList;


/**
 * IMultiViewDvbService Interface
 *
 */

public interface IMultiViewDvbService {
    int getGroupID();
    String getGroupName();
    int getGroupOrder();
    ArrayList<MultiViewLogicalCell> getMultiViewLogicalCellInfoList();
    int getMultiviewEntryPoint();
    int getMultiviewId();
    int getNumberOfHorizentalElementaryCells();
    int getNumberOfVerticalElementaryCells();
    int getRating();
}
