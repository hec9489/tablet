
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.xmlUtils;

import com.tickaroo.tikxml.TypeConverter;


/**
 * XmlUtilBooleanYnConverter Class
 *
 */

public class XmlUtilBooleanYnConverter implements TypeConverter<Boolean> {
    /**
     * get Boolean value from name
     * @param s String
     * @return Boolean
     */
    @Override
    public Boolean read(String s) throws Exception {
        if (s != null) {
            if (!s.equals("N")) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    /**
     * get String from Boolean value
     * @param aBoolean
     * @return String
     */
    @Override
    public String write(Boolean aBoolean) throws Exception {
        return aBoolean ? "Y" : "N";
    }
}
