
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator;

import android.text.TextUtils;

import com.skb.btv.framework.navigator.xmlUtils.XmlUtilStringConverter;
import com.tickaroo.tikxml.TikXml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.locks.ReentrantLock;

import okio.BufferedSource;
import okio.Okio;


/**
 * DvbSIRouteParcelableData Class
 *
 */

public class DvbSIRouteParcelableData<T> {

    private static final String TAG = "DvbSIData";

    private String path;
    private Class<T> cls;
    private T defaultT;
    /**
     * Tik Xml parser
     */
    private TikXml xmlParser;

    /**
     * Object last version(xml root time)
     */
    private String lastVersion;
    private String tempVersion;

    private T cache;
    ReentrantLock lock = new ReentrantLock();

    @SuppressWarnings("unchecked")
    DvbSIRouteParcelableData(String path, T data){
        this.path = path;
        this.cls = (Class<T>) data.getClass();
        this.defaultT = data;
        this.xmlParser = new TikXml.Builder()
                .exceptionOnUnreadXml(false)
                .addTypeConverter(String.class, new XmlUtilStringConverter())
                .build();
    }

    /**
     * Read Parceable Data
     * @return : Object T
     */
    T beginDataRead(){
        lock.lock();
        return cache;
    }

    /**
     * End Read Parceable Data
     */
    void endDataRead(){
        lock.unlock();
    }

    /**
     * Update xml file
     * @return : true updated, false : not updated
     */
    boolean updateDataRead() {
        if (isDataNewer()) {
            T temp = dataLoad();
            lock.lock();
            lastVersion = tempVersion;
            cache = temp;
            lock.unlock();
            return true;
        }
        return false;
    }

    /**
     * Load xml file
     * @return : Object T
     */
    private T dataLoad() {
        try {
            //SLog.d(TAG, "load: " + path);
            BufferedSource source = Okio.buffer(Okio.source(new FileInputStream(new File(path))));
            return xmlParser.read(source, cls);
        } catch (IOException e) {
            e.printStackTrace();
            return defaultT;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return defaultT;
        } catch (NullPointerException e) {
            e.printStackTrace();
            return defaultT;
        }
    }

    /**
     * Check if new xml file
     * @return : True (New File), False(Same File)
     */
    private boolean isDataNewer() {
        FileInputStream fis = null;
        try {
            final File file = new File(path);
            fis = new FileInputStream(file);
            final int length = Math.min(fis.available(), 100);
            final byte[] buf = new byte[length];
            final int read = fis.read(buf, 0, length);

            if (read == length) {
                final String str = new String(buf, Charset.forName("UTF-8"));
                final String mark = "time=\"";
                final int p = str.indexOf(mark);
                if (p > 0) {
                    final int p0 = p + mark.length();
                    if (p0 < str.length()) {
                        final int p1 = str.indexOf("\"", p0);
                        if (p1 > 0) {
                            final String readVersion = str.substring(p0, p1);
                            final boolean newer;
                            if (tempVersion != null) {
                                newer = !TextUtils.equals(readVersion, tempVersion);
                            } else {
                                newer = true;
                            }
                            if (newer) {
                                tempVersion = readVersion;
                            }
                            return newer;
                        }
                    }
                }
                //SLog.d(TAG, "isDataNewer: true" );
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        //SLog.d(TAG, "isDataNewer: false");
        return false;
    }

    /**
     * Get Last version
     * @return : Time (String)
     */
    public String getLastVersion(){
        return lastVersion;
    }


}
