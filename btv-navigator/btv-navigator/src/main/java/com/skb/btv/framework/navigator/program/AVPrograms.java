
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.program;

import android.os.Parcel;
import android.os.Parcelable;

import com.tickaroo.tikxml.annotation.Element;
import com.tickaroo.tikxml.annotation.PropertyElement;
import com.tickaroo.tikxml.annotation.Xml;

import java.util.ArrayList;
import java.util.List;


/**
 * AVPrograms Class
 *
 */

@SuppressWarnings("WeakerAccess")
@Xml(name = "channel")
public class AVPrograms implements IAVPrograms, Parcelable {

    private static final String TAG =  "AVPrograms";

    @PropertyElement(name = "number")
    int sid;

    @Element
    List<AVProgram> avProgramList;

    public AVPrograms(){
    }

    public AVPrograms(int id) {
        sid = id;
        avProgramList = new ArrayList<>();
    }

    protected AVPrograms(Parcel in) {
        sid = in.readInt();
        avProgramList = in.createTypedArrayList(AVProgram.CREATOR);
    }

    public static final Creator<AVPrograms> CREATOR = new Creator<AVPrograms>() {
        /**
         * create service from parcel
         * @param in
         * @return AVPrograms
         */
        @Override
        public AVPrograms createFromParcel(Parcel in) {
            return new AVPrograms(in);
        }

        /**
         * create service array
         * @param size
         * @return AVPrograms[]
         */
        @Override
        public AVPrograms[] newArray(int size) {
            return new AVPrograms[size];
        }
    };

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(sid);
        dest.writeTypedList(avProgramList);
    }

    /**
     * Get channel
     * @return  channel
     */
    @Override
    public int getSid() {
        return sid;
    }

    @Override
    public void setSid(int id) {
        sid = id;
    }

    /**
     * Get AV program list
     * @return  AV program list
     */
    @Override
    public ArrayList<AVProgram> getAVProgramList() {
        return (ArrayList<AVProgram>)avProgramList;
    }

    /**
     * add AV program
     * @param  _program AV program
     */
    public void addAVProgram(AVProgram _program)
    {
        if(avProgramList == null){
            avProgramList = new ArrayList<>();
        }
        avProgramList.add(_program);
    }
}
