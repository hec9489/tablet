
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.xmlUtils;

import com.tickaroo.tikxml.TypeConverter;


/**
 * XmlUtilStringConverter Class
 *
 */

public class XmlUtilStringConverter implements TypeConverter<String> {
    /**
     * convert xml regex to string
     * @param value String
     * @return String
     */
    @Override
    public String read(String value) throws Exception {
        return value
                .replaceAll("&lt;", "<")
                .replaceAll("&gt;", ">")
                .replaceAll("&amp;", "&")
                .replaceAll("&quot;", "\"")
                .replaceAll("&apos;", "\'")
                .replaceAll("&nbsp;", " ");
    }

    /**
     * convert xml string to regex
     * @param value String
     * @return String
     */
    @Override
    public String write(String value) throws Exception {
        return value
                .replaceAll("&lt;", "<")
                .replaceAll("&gt;", ">")
                .replaceAll("&amp;", "&")
                .replaceAll("&quot;", "\"")
                .replaceAll("&apos;", "\'")
                .replaceAll("&nbsp;", " ");
    }
}
