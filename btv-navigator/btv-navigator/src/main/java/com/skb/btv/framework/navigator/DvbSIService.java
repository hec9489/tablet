
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator;

import android.content.Context;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

import com.skb.btv.framework.navigator.dvbservice.AVDvbServiceList;
import com.skb.btv.framework.navigator.dvbservice.MultiViewDvbServiceList;
import com.skb.btv.framework.navigator.dvbservice.MultiViewLogicalCell;
import com.skb.btv.framework.navigator.dvbservice.MultiViewLogicalCellList;
import com.skb.btv.framework.navigator.dvbservice.MusicDvbServiceList;
import com.skb.btv.framework.navigator.product.ProductList;
import com.skb.btv.framework.navigator.program.AVProgramList;
import com.skb.btv.framework.navigator.program.MusicProgramList;
import com.skb.btv.framework.navigator.log.SLog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * DvbSIService Class
 *
 */

public class DvbSIService  {

    private static final String TAG =  "Navigator-DvbSIService";

    private static final String TAG_SUB_INIT  = TAG+"[Init]";
    private static final String TAG_SUB_CHANNEL  = TAG+"[DvbService]";
    private static final String TAG_SUB_PRODUCT  = TAG+"[Product]";
    private static final String TAG_SUB_PROGRAM = TAG+"[Program]";
    private static final String TAG_SUB_CONFIG = TAG+"[Config]";
    private static final String TAG_SUB_UTILS = TAG+"[Utils]";

    private static Context mContext;
    private DvbSIParcelableDataRoute dvbSIDataRoute;

    private static ArrayList<OnDvbSIListener> mDvbSICallbacks = new ArrayList<>();

    private static DvbSIService _instance;

    private DvbSIService(Context context) {
        Log.d(TAG, "creator");
        mContext = context;
        if (dvbSIDataRoute == null) {
            dvbSIDataRoute = new DvbSIParcelableDataRoute(mContext);
        }
        Thread routeThread = new Thread(new Runnable(){
            @Override
            public void run() {
                if (dvbSIDataRoute != null) {
                    dvbSIDataRoute.startRouteProcess();
                }
            }
        });
        routeThread.setPriority(Thread.MAX_PRIORITY);
        routeThread.start();
    }

    /**
     * Get DvbSIService Instance
     * @param context
     * @return DvbSIService
     */
    public static DvbSIService getInstance(Context context) {
        Log.d(TAG, "getInstance - instance : " + _instance);
        if(_instance == null) {
            _instance = new DvbSIService(context);
        }
        return _instance;
    }

    public Context getContext(){
        return mContext;
    }

    /**
     * DvbSIService release
     * @return void
     */
    public void release() {
        Log.d(TAG, "IN| " + "DvbSIService release");
        if(dvbSIDataRoute != null){
            dvbSIDataRoute.stopRouteProcess();
        }
        _instance = null;
    }

    // region DvbSI Callback listener APIs
    /**
     * Register DvbSI callback listener
     * @param listener
     */
    public void setOnDvbSIListener(OnDvbSIListener listener) {
        SLog.d(TAG_SUB_UTILS, "setOnDvbSIListener() start OnDvbSIListener : "+listener);
        synchronized(this) {
            if (listener != null) {
                boolean bRet = mDvbSICallbacks.add(listener);
                if(bRet && dvbSIDataRoute != null) {
                    dvbSIDataRoute.sendTestRegisterCompleted("");
                }
            }
        }
        SLog.d(TAG_SUB_UTILS, "setOnDvbSIListener() end ");
    }

    /**
     * Unregister DvbSI callback listener
     * @param listener
     */
    public void clearOnDvbSIListener(OnDvbSIListener listener) {
        SLog.d(TAG_SUB_UTILS, "clearOnDvbSIListener() start OnDvbSIListener : "+listener);
        synchronized(this) {
            if (listener != null) {
                mDvbSICallbacks.remove(listener);
            }
        }
        SLog.d(TAG_SUB_UTILS, "clearOnDvbSIListener() end ");
    }

    // region Dsmcc Callback listener APIs
    /**
     * Register Dsmcc Event callback listener
     * @param listener
     */
    public void setOnDsmccListener(OnDsmccListener listener) {
        SLog.d(TAG_SUB_UTILS, "setOnDsmccListener() start OnDsmccListener : "+listener);
        synchronized(this) {
            if(dvbSIDataRoute != null) {
                dvbSIDataRoute.setOnDsmccListener(listener);
            }
        }
        SLog.d(TAG_SUB_UTILS, "setOnDsmccListener() end ");
    }

    /**
     * Unregister Dsmcc Event callback listener
     * @param listener
     */
    public void clearOnDsmccListener(OnDsmccListener listener) {
        SLog.d(TAG_SUB_UTILS, "clearOnDsmccListener() start OnDsmccListener : "+listener);
        synchronized(this) {
            if(dvbSIDataRoute != null) {
                dvbSIDataRoute.clearOnDsmccListener(listener);
            }
        }
        SLog.d(TAG_SUB_UTILS, "clearOnDsmccListener() end ");
    }

    static void sendOnDvbSICallback(int dvbsiType, String extraData) {
        SLog.d(TAG_SUB_CHANNEL, " start");
        synchronized(mDvbSICallbacks) {
            if (mDvbSICallbacks != null) {
                for (OnDvbSIListener listener : mDvbSICallbacks) {
                    if (listener != null) {
                        listener.onDvbSIUpdated(dvbsiType, extraData);
                    }
                }
            }
        }
        SLog.d(TAG_SUB_CHANNEL, " end");
    }
    // endregion

    // region Related Channel APIs
    /**
     * Get AV Channel List
     * @return AV Channel List(AVDvbServiceChList)
     * @throws RemoteException
     */
    public AVDvbServiceList getAVDvbServiceList() {
        SLog.d(TAG_SUB_CHANNEL, "getAVDvbServiceList() start");
        synchronized(this) {
            if (dvbSIDataRoute != null) {
                return dvbSIDataRoute.getAVDvbServiceList();
            }
            return new AVDvbServiceList(false);
        }
    }

    /**
     * Get Music Channel List
     * @return : Music Channel List(MusicDvbServiceChList)
     * @throws RemoteException
     */
    public MusicDvbServiceList getMusicDvbServiceList() {
        SLog.d(TAG_SUB_CHANNEL, "getMusicDvbServiceList() start ");
        synchronized(this) {
            if (dvbSIDataRoute != null) {
                return dvbSIDataRoute.getMusicDvbServiceList();
            }
            return new MusicDvbServiceList(false);
        }
    }

    /**
     * Get MultiView Channel List
     * @return : MultiView Channel List(MultiViewDvbServiceChList)
     * @throws RemoteException
     */
    public MultiViewDvbServiceList getMultiViewDvbServiceList() {
        SLog.d(TAG_SUB_CHANNEL, "getMultiViewDvbServiceList()");
        synchronized(this) {
            if (dvbSIDataRoute != null) {
                return dvbSIDataRoute.getMultiViewDvbServiceList();
            }
            return new MultiViewDvbServiceList(false);
        }
    }

    /**
     * Get sid of channel number
     * @param ch : channel no
     * @return : sid (integer)
     * @throws RemoteException
     */
    /*public int getDvbServiceSid(int ch) {
        SLog.d(TAG_SUB_CHANNEL, "getDvbServiceSid()");
        synchronized(this) {
            if (dvbSIDataRoute != null) {
                return dvbSIDataRoute.getDvbServiceSid(ch);
            }
            return -1;
        }
    }*/

    /**
     * Get MultiViewLogicalCell List of Group Id
     * @param groupId : group id
     * @return : MultiViewLogicalCell List(MultiViewLogicalCellList)
     * @throws RemoteException
     */
    public MultiViewLogicalCellList getMultiViewLogicalCellList(int groupId) {
        SLog.d(TAG_SUB_CHANNEL, " getLogicalCellList() groupId : "+groupId);
        synchronized(this) {
            if (dvbSIDataRoute != null) {
                return dvbSIDataRoute.getLogicalCellList(groupId);
            }
            return new MultiViewLogicalCellList();
        }
    }

    /**
     * Make Channel URI of MultiViewLogicalCell
     * @param logicalcells : selected MultiViewLogicalCell
     * @return : Uri (String)
     */
    public String makeMultiViewUri(List<MultiViewLogicalCell> logicalcells) {
        SLog.d(TAG_SUB_CHANNEL, "makeMultiViewUri() start ");
        synchronized(this) {
            if (dvbSIDataRoute != null) {
                return dvbSIDataRoute.makeMultiViewUri(logicalcells);
            }
            return "";
        }
    }
    // endregion

    // region Related Product APIs
    /**
     * Get Product List
     * @return : Product List(ProductList)
     * @throws RemoteException
     */
    public ProductList getProductList() {
        SLog.d(TAG_SUB_PRODUCT, "getProductList()");
        synchronized(this) {
            if (dvbSIDataRoute != null) {
                return dvbSIDataRoute.getProductList();
            }
            return new ProductList(false);
        }
    }
    // endregion

    // region Related Program APIs
    /**
     * Get Program information of sid number
     * @param sid : sid number
     * @return : Program List(AVProgramList)
     * @throws RemoteException
     */
    public AVProgramList getAVProgramList(int sid) {
        SLog.d(TAG_SUB_PROGRAM, "getAVProgramList()");
        synchronized(this) {
            if (dvbSIDataRoute != null) {
                return dvbSIDataRoute.getProgramListOfDvbService(sid);
            }
            return new AVProgramList(false);
        }
    }

    /**
     * Get Program Lists of given multi sids
     * @param sidList : multi sids
     * @return : Program List(AVProgramList)
     * @throws RemoteException
     */
    public AVProgramList getMultiAVProgramList(String sidList) {
        SLog.d(TAG_SUB_PROGRAM, "getMultiAVProgramList() sidList = "+sidList);
        synchronized(this) {
            if (dvbSIDataRoute != null) {
                return dvbSIDataRoute.getMultiDvbServiceProgramList(sidList);
            }
            return new AVProgramList(false);
        }
    }

    /**
     * Get Music Program List of audioPid
     * @param audioPid : Music Channel audio Pid
     * @return : Music Program List(MusicProgramList)
     * @throws RemoteException
     */
    public MusicProgramList getMusicProgramList(int audioPid) {
        SLog.d(TAG_SUB_PROGRAM, "getMusicProgramListFromPid() audioPid -> "+audioPid);
        synchronized(this) {
            if (dvbSIDataRoute != null) {
                return dvbSIDataRoute.getMusicDvbServiceProgramList(audioPid);
            }
            return new MusicProgramList(false);
        }
    }
    // endregion

    // region Related Config APIs
    /**
     * Update region code in config.xml
     * @param regionCode : code value
     * @throws RemoteException
     */
    public void setRegionCode(String regionCode) {
        SLog.d(TAG_SUB_CONFIG, "setRegionCode() start regionCode :" + regionCode);
        synchronized(this) {
            if (dvbSIDataRoute != null) {
                dvbSIDataRoute.setRegionCode(regionCode);
            }
            SLog.d(TAG_SUB_CONFIG, "setRegionCode() end ");
        }
    }

    /**
     * Update seg id in config.xml
     * @param segId : seg id
     * @throws RemoteException
     */
    public void setSegId(int segId) {
        SLog.d(TAG_SUB_CONFIG, "setSegId() start segid : "+segId);
        synchronized(this) {
            if (dvbSIDataRoute != null) {
                dvbSIDataRoute.setSegId(segId);
            }
        }
        SLog.d(TAG_SUB_CONFIG, "setSegId() end segid : "+segId);
    }
    // endregion

    /**
     * Set current showing sid list
     * @param sidList : current showing sid list
     */
    public void setCurrentMultiChannel(String sidList) {
        SLog.d(TAG_SUB_CHANNEL, "setCurrentMultiChannel() start sid(s) : "+sidList);
        synchronized(this) {
            if (dvbSIDataRoute != null) {
                dvbSIDataRoute.setCurrentMultiChannel(sidList);
            }
        }
    }

    /**
     * Set current channel info
     * @param strUri : current showing channel uri
     */
    public void setCurrentChannelUri(String strUri) {
        SLog.d(TAG_SUB_CHANNEL, " setChannelUri() strUri : "+ strUri);
        synchronized(this) {
            try {
                Uri uri = Uri.parse(strUri);
                String scheme = uri.getScheme();

                if (scheme.equalsIgnoreCase("skbiptv")) {         //IPTV
                    IptvTuneInfo tuneInfo = getTuneInfo(uri);
                    if (dvbSIDataRoute != null) {
                        dvbSIDataRoute.setCurrentChannelInfo(tuneInfo);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Get current program
     * @param sid : current showing sid
     */
    public AVProgramList getCurrentAVProgram(int sid) {
        SLog.d(TAG_SUB_PROGRAM, " sid : "+ sid);
        synchronized(this) {
            if (dvbSIDataRoute != null) {
                return dvbSIDataRoute.getCurrentAVProgram(sid);
            }
            return new AVProgramList(false);
        }
    }


    /**
     * Get program at a special time
     * @param sidList : sid list string
     * @param startTime : start time
     * @param endTime : end time
     * @return : AVProgramList
     */
    public AVProgramList getMultiAVProgramListByTime(String sidList, Date startTime, Date endTime) {
        SLog.d(TAG_SUB_PROGRAM, " sidList : "+ sidList);
        synchronized(this) {
            if (dvbSIDataRoute != null) {
                return dvbSIDataRoute.getMultiDvbServiceAVProgramListByTime(sidList, startTime, endTime);
            }
            return new AVProgramList(false);
        }
    }

    /**
     * Get AV program list by count from current time
     * @param sid : service id
     * @param requestCount : request count
     * @return : AVProgramList
     */
    public AVProgramList getPartialProgramListByCount(int sid, int requestCount){
        SLog.d(TAG_SUB_PROGRAM, " sid : "+sid
                +" requestCount : "+requestCount);
        synchronized(this) {
            if (dvbSIDataRoute != null) {
                return dvbSIDataRoute.getPartialProgramListByCount(sid, requestCount);
            }
            return new AVProgramList(false);
        }
    }

    private IptvTuneInfo getTuneInfo(Uri uri) throws Exception {
        IptvTuneInfo info = new IptvTuneInfo();

        info.SetChNum(uri.getQueryParameter("ch") == null ? 0 : Integer.decode(uri.getQueryParameter("ch")));
        info.SetURL(uri.getHost());
        info.SetPort(uri.getPort());
        info.SetVideoPID(uri.getQueryParameter("vp") == null ? 0 : Integer.decode(uri.getQueryParameter("vp")));
        info.SetAudioPID(uri.getQueryParameter("ap") == null ? 0 : Integer.decode(uri.getQueryParameter("ap")));
        info.SetAudioPID1(uri.getQueryParameter("ap1") == null ? 0 : Integer.decode(uri.getQueryParameter("ap1")));
        info.SetPCRPID(uri.getQueryParameter("pp") == null ? 0 : Integer.decode(uri.getQueryParameter("pp")));
        info.SetCAPID(uri.getQueryParameter("cp") == null ? 0 : Integer.decode(uri.getQueryParameter("cp")));
        info.SetCASystemID(uri.getQueryParameter("ci") == null ? 0 : Integer.decode(uri.getQueryParameter("ci")));
        info.SetVideoStream(uri.getQueryParameter("vc") == null ? 0 : Integer.decode(uri.getQueryParameter("vc")));
        info.SetAudioStream(uri.getQueryParameter("ac") == null ? 0 : Integer.decode(uri.getQueryParameter("ac")));
        info.SetAudioStream1(uri.getQueryParameter("ac1") == null ? 0 : Integer.decode(uri.getQueryParameter("ac1")));
        info.setLastFrame(uri.getQueryParameter("lf") == null ? 0 : Integer.decode(uri.getQueryParameter("lf")));
        info.setAuioEnable(uri.getQueryParameter("ae") == null ? 0 : Integer.decode(uri.getQueryParameter("ae")));
        info.setPip(uri.getQueryParameter("p"));
        info.SetResolution(IptvTuneInfo.IPTV_TUNEINFO_RES_HD);

        SLog.d(TAG_SUB_CHANNEL, "getTuneInfo : "+ info.GetURL());
        return info;
    }

}
