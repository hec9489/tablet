package com.skb.btv.framework.navigator;

import android.os.Parcel;
import android.os.Parcelable;

public class SignalEvent {
    public static final int APP_INFO_CHANGED = 1;
    public static final int APP_INFO_SIGNAL = 0;
    public static final int UNKNOWN_EVENT = 2;

    int mEventType;
    int mChannelNum;
    int mDemux;
    String mUri;
    AppInfo[] mAppInfoList = null;

    public int getmEventType() {
        return mEventType;
    }

    public void setmEventType(int mEventType) {
        this.mEventType = mEventType;
    }

    public int getmChannelNum() {
        return mChannelNum;
    }

    public void setmChannelNum(int mChannelNum) {
        this.mChannelNum = mChannelNum;
    }

    public int getmDemux() {
        return mDemux;
    }

    public void setmDemux(int mDemux) {
        this.mDemux = mDemux;
    }

    public String getmUri() {
        return mUri;
    }

    public void setmUri(String mUri) {
        this.mUri = mUri;
    }

    public AppInfo[] getmAppInfoList() {
        return mAppInfoList;
    }

    public void setmAppInfoList(AppInfo[] mAppInfoList) {
        this.mAppInfoList = mAppInfoList;
    }
}
