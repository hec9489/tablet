
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.program;

import java.util.Date;


/**
 * IAVProgramLinkInfo Interface
 *
 */

public interface IAVProgramLinkInfo {

    public static final int LINKINFO_BUTTON_TYPE_DEFAULT = 1;
    public static final int LINKINFO_BUTTON_TYPE_DYNAMIC = 2;

    public static final int LINKINFO_SERVICE_TYPE_VOD = 1;
    public static final int LINKINFO_SERVICE_TYPE_IPTV = 2;
    public static final int LINKINFO_SERVICE_TYPE_BROWSER_URL = 3;
    public static final int LINKINFO_SERVICE_TYPE_VAS = 4;
    public static final int LINKINFO_SERVICE_TYPE_VOD_1_DEPTH_MENU = 5;
    public static final int LINKINFO_SERVICE_TYPE_VOD_2_DEPTH_MENU = 6;

    String getButtonImagePath();
    int getButtonType();
    int getCMenuValue();
    Date getDisplayEndDate();
    Date getDisplayStartDate();
    String getLinkedServiceText();
    int getLinkedServiceType();
    String getVasItemId();
    String getVasPath();
    String getVasServiceId();
}
