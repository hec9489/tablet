
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.database;


/**
 * DatabaseColumnIndex Class
 *
 */

public class DatabaseColumnIndex {

    public static final int COL_SERVICE_INDEX = 0;
    public static final int COL_NAME_INDEX = 1;
    public static final int COL_DESCRIPTION_INDEX = 2;
    public static final int COL_PRICE_INDEX = 3;
    public static final int COL_RESOLUTION_INDEX = 4;
    public static final int COL_AUDIO_INDEX = 5;
    public static final int COL_EVENTID_INDEX = 6;
    public static final int COL_RATING_INDEX = 7;
    public static final int COL_DURATION_INDEX = 8;
    public static final int COL_STARTTIME_INDEX = 9;
    public static final int COL_ENDTIME_INDEX = 10;
    public static final int COL_STARTYEAR_INDEX = 11;
    public static final int COL_STARTMONTH_INDEX = 12;
    public static final int COL_STARTDAY_INDEX = 13;
    public static final int COL_ENDYEAR_INDEX = 14;
    public static final int COL_ENDMONTH_INDEX = 15;
    public static final int COL_ENDDAY_INDEX = 16;
    public static final int COL_LANG_INDEX = 17;
    public static final int COL_ACTORS_INDEX = 18;
    public static final int COL_CAPTION_INDEX = 19;
    public static final int COL_CONTENTNIBBLE1_INDEX = 20;
    public static final int COL_CONTENTNIBBLE2_INDEX = 21;
    public static final int COL_CONTENTUSERNIBBLE1_INDEX = 22;
    public static final int COL_CONTENTUSERNIBBLE2_INDEX = 23;
    public static final int COL_DIRECTOR_INDEX = 24;
    public static final int COL_DOLBYAUDIO_INDEX = 25;
    public static final int COL_DVS_INDEX = 26;
    public static final int COL_PROGRAMIMAGEPATH_INDEX = 27;
    public static final int COL_BUTTONIMAGEPATH_INDEX = 28;
    public static final int COL_BUTTONTYPE_INDEX = 29;
    public static final int COL_CMENUVALUE_INDEX = 30;
    public static final int COL_DISPLAYSTARTTIME_INDEX = 31;
    public static final int COL_DISPLAYENDTIME_INDEX = 32;
    public static final int COL_LINKEDSERVICETEXT_INDEX = 33;
    public static final int COL_LINKEDSERVICETYPE_INDEX = 34;
    public static final int COL_VASITEMID_INDEX = 35;
    public static final int COL_VASPATH_INDEX = 36;
    public static final int COL_VASSERVICEID_INDEX = 37;
    public static final int COL_RUNNINGSTATUS_INDEX = 38;
    public static final int COL_VODID_INDEX = 39;
    public static final int COL_SIGNLANGUAGE_INDEX = 40;
    public static final int COL_CHNUM_INDEX = 41;
    public static final int COL_STARTFTIME_INDEX = 42;
    public static final int COL_EDNFTIME_INDEX = 43;
    public static final int COL_UPDATE_TIME_INDEX = 44;
    public static final int COL_INSERT_TIME_INDEX = 45;


    public static final String COL_SERVICE_STRING = "serviceid";
    public static final String COL_NAME_STRING = "name";
    public static final String COL_DESCRIPTION_STRING = "description";
    public static final String COL_PRICE_STRING = "price";
    public static final String COL_RESOLUTION_STRING = "resolution";
    public static final String COL_AUDIO_STRING = "audio";
    public static final String COL_EVENTID_STRING = "eventid";
    public static final String COL_RATING_STRING = "rating";
    public static final String COL_DURATION_STRING = "duration";
    public static final String COL_STARTTIME_STRINGX = "starttime";
    public static final String COL_ENDTIME_STRING = "endtime";
    public static final String COL_STARTYEAR_STRING = "startyear";
    public static final String COL_STARTMONTH_STRING = "startmonth";
    public static final String COL_STARTDAY_STRINGX = "startday";
    public static final String COL_ENDYEAR_STRING = "endyear";
    public static final String COL_ENDMONTH_STRING = "endmonth";
    public static final String COL_ENDDAY_STRING = "endday";
    public static final String COL_LANG_STRING = "lang";
    public static final String COL_ACTORS_STRING = "actors";
    public static final String COL_CAPTION_STRING = "caption";
    public static final String COL_CONTENTNIBBLE1_STRING = "contentNibble1";
    public static final String COL_CONTENTNIBBLE2_STRING = "contentNibble2";
    public static final String COL_CONTENTUSERNIBBLE1_STRING = "contentUserNibble1";
    public static final String COL_CONTENTUSERNIBBLE2_STRING = "contentUserNibble2";
    public static final String COL_DIRECTOR_STRING = "director";
    public static final String COL_DOLBYAUDIO_STRING = "dolbyAudio";
    public static final String COL_DVS_STRING = "dvs";
    public static final String COL_PROGRAMIMAGEPATH_STRING = "programImagePath";
    public static final String COL_BUTTONIMAGEPATH_STRING = "ButtonImagePath";
    public static final String COL_BUTTONTYPE_STRING = "ButtonType";
    public static final String COL_CMENUVALUE_STRING = "CMenuValue";
    public static final String COL_DISPLAYSTARTTIME_STRING = "DisplayStartTime";
    public static final String COL_DISPLAYENDTIME_STRING = "DisplayEndTime";
    public static final String COL_LINKEDSERVICETEXT_STRING = "LinkedServiceText";
    public static final String COL_LINKEDSERVICETYPE_STRING = "LinkedServiceType";
    public static final String COL_VASITEMID_STRING = "VasItemId";
    public static final String COL_VASPATH_STRING = "VasPath";
    public static final String COL_VASSERVICEID_STRING = "VasServiceId";
    public static final String COL_RUNNINGSTATUS_STRING = "runningStatus";
    public static final String COL_VODID_STRING = "vodId";
    public static final String COL_SIGNLANGUAGE_STRING = "SignLanguage";
    public static final String COL_CHNUM_STRING = "chnum";
    public static final String COL_STARTFTIME_STRING = "startftime";
    public static final String COL_ENDFTIME_STRING = "endftime";
    public static final String COL_UPDATE_TIME_STRING = "updatetime";
    public static final String COL_INSERT_TIME_STRING = "insert_time";
}
