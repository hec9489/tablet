
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.product;

import java.util.ArrayList;


/**
 * IProductList Interface
 *
 */

public interface IProductList {
    boolean isReady();
    ArrayList<Product> getProductList();
}
