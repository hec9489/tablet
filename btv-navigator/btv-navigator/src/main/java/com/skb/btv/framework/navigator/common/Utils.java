
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.common;

import com.skb.btv.framework.navigator.log.SLog;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * Utils Class
 *
 */

public class Utils {

    public static final String TAG = "Utils";

    /**
     * rename file
     * @param filename
     * @param newFilename
     * @return boolean
     */
    public static boolean renameFile(String filename, String newFilename) {

        boolean ret = false;
        SLog.d(TAG, "renameFile filename = " + filename + ", newFilename = " + newFilename);
        File file = new File( filename );
        File fileNew = new File( newFilename );
        if(fileNew.exists()){
            SLog.d(TAG, "renameFile newFilename exist so delete");
            fileNew.delete();
        }
        if( file.exists() ){
            SLog.d(TAG, "renameFile rename");
            ret = file.renameTo( fileNew );
        }
        SLog.d(TAG, "renameFile rename - ret : " + ret);
        return ret;
    }

    /**
     * change date info. to Date instance
     * @param year
     * @param month
     * @param day
     * @param time
     * @return Date
     */
    public static Date toDate(int year, int month, int day, String time) {
        String transMonth = String.format(Locale.KOREAN, "%02d", month);
        String transTime = getEndTime(time);

        StringBuilder StringDate = new StringBuilder()
                .append(year)
                .append("/")
                .append(transMonth)
                .append("/")
                .append(day)
                .append(" ")
                .append(transTime);
        Date transDate;
        try {
            transDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.KOREAN).parse(StringDate.toString());
        } catch (ParseException e) {
            transDate = new Date();
        }
        return transDate;
    }

    /**
     * change date info. to Date instance
     * @param date
     * @return Date
     */
    public static Date toDate(String date){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

        Date transDate = null;

        try {
            transDate = simpleDateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return transDate;
    }

    /**
     * get end Date instance from start time & duration
     * @param startTime
     * @param duration
     * @return Date
     */
    public static Date toEndDate(Date startTime, int duration){
        long endTime = startTime.getTime() + duration * 1000;
        Date transEndTime = new Date(endTime);
        return transEndTime;
    }

    /**
     * get "HH:mm:ss" format end Date string
     * @param endTime
     * @return String
     */
    private static String getEndTime(String endTime){
        Date date;
        try {
            date = new SimpleDateFormat("HH:mm:ss", Locale.KOREAN).parse(endTime);
        } catch (ParseException e) {
            date = new Date(0);
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        SimpleDateFormat transFormat = new SimpleDateFormat("HH:mm:ss");

        String newEndTime = transFormat.format(calendar.getTime());

        return newEndTime;
    }
}
