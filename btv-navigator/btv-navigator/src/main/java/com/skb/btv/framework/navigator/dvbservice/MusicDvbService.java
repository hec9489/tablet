
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;

import android.os.Parcel;
import android.os.Parcelable;

import com.skb.btv.framework.navigator.xmlUtils.XmlUtilStringConverter;
import com.tickaroo.tikxml.annotation.Attribute;
import com.tickaroo.tikxml.annotation.Xml;


/**
 * MusicDvbService Class
 *
 */

@SuppressWarnings("WeakerAccess")
@Xml(name = "Channel")
public class MusicDvbService implements IMusicDvbService, Parcelable {

    private static final String TAG =  "MusicDvbService";

    @Attribute(name = "ch")
    int ch;

    @Attribute(name = "ip")
    String ip;

    @Attribute(name = "port")
    int port;

    @Attribute(name = "ch_no")
    int ch_no;

    @Attribute(name = "apid")
    int apid;

    @Attribute(name = "ppid")
    int ppid;

    @Attribute(name = "ast")
    int ast;

    @Attribute(name = "opid")
    int opid;

    @Attribute(name = "ctype")
    int ctype;

    @Attribute(name = "audio_channel_type")
    int audio_channel_type;

    @Attribute(name = "title")
    String title;

    @Attribute(name = "image")
    String image;

    @Attribute(name = "ca_id")
    int ca_id;

    @Attribute(name = "ca_pid")
    int ca_pid;

    @Attribute(name = "channelUri", converter = XmlUtilStringConverter.class)
    String channelUri;

    public MusicDvbService(){

    }

    protected MusicDvbService(Parcel in) {
        ch = in.readInt();
        ip = in.readString();
        port = in.readInt();
        ch_no = in.readInt();
        apid = in.readInt();
        ppid = in.readInt();
        ast = in.readInt();
        opid = in.readInt();
        ctype = in.readInt();
        audio_channel_type = in.readInt();
        title = in.readString();
        image = in.readString();
        ca_id = in.readInt();
        ca_pid = in.readInt();
        channelUri = in.readString();
    }

    public static final Creator<MusicDvbService> CREATOR = new Creator<MusicDvbService>() {
        /**
         * create service from parcel
         * @param in
         * @return MusicDvbService
         */
        @Override
        public MusicDvbService createFromParcel(Parcel in) {
            return new MusicDvbService(in);
        }

        /**
         * create service array
         * @param size
         * @return MusicDvbService[]
         */
        @Override
        public MusicDvbService[] newArray(int size) {
            return new MusicDvbService[size];
        }
    };

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ch);
        dest.writeString(ip);
        dest.writeInt(port);
        dest.writeInt(ch_no);
        dest.writeInt(apid);
        dest.writeInt(ppid);
        dest.writeInt(ast);
        dest.writeInt(opid);
        dest.writeInt(ctype);
        dest.writeInt(audio_channel_type);
        dest.writeString(title);
        dest.writeString(image);
        dest.writeInt(ca_id);
        dest.writeInt(ca_pid);
        dest.writeString(channelUri);
    }

    /**
     * Get channel
     * @return  channel
     */
    @Override
    public int getCh() {
        return ch;
    }

    /**
     * Get ip
     * @return  ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * Get port
     * @return  port
     */
    public int getPort() {
        return port;
    }

    /**
     * Get channel number
     * @return  channel number
     */
    @Override
    public int getCh_no() {
        return ch_no;
    }

    /**
     * Get apid
     * @return  apid
     */
    @Override
    public int getApid() {
        return apid;
    }

    /**
     * Get ppid
     * @return  ppid
     */
    @Override
    public int getPpid() {
        return ppid;
    }

    /**
     * Get ast
     * @return  ast
     */
    @Override
    public int getAst() {
        return ast;
    }

    /**
     * Get opid
     * @return  opid
     */
    @Override
    public int getOpid() {
        return opid;
    }

    /**
     * Get service type
     * @return  service type
     */
    @Override
    public int getChannelType() {
        return ctype;
    }

    /**
     * Get audio channel type
     * @return audio channel type
     */
    @Override
    public int getAudioChannelType() {
        return audio_channel_type;
    }

    /**
     * Get title
     * @return  title
     */
    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String changeTitle){
        title = changeTitle;
    }

    /**
     * Get image path
     * @return  image path
     */
    @Override
    public String getImagePath() {
        return image;
    }

    /**
     * Get ca_id
     * @return  ca_id
     */
    public int getCaId() {
        return ca_id;
    }

    /**
     * Get ca_pid
     * @return  ca_pid
     */
    public int getCaPid() {
        return ca_pid;
    }

    /**
     * Get channel Uri
     * @return  channel Uri
     */
    @Override
    public String getChannelUri() {
        return channelUri;
    }

}
