
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.xmlUtils;

import com.tickaroo.tikxml.TypeConverter;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * XmlUtilCommaListConverter Class
 *
 */

public class XmlUtilCommaListConverter implements TypeConverter<ArrayList<String>> {

    public static final char SEPERATE_CHAR = ',';
    public static final String SEPERATE = String.valueOf(SEPERATE_CHAR);

    /**
     * get String list from String with separate char
     * @param value String
     * @return ArrayList<String>
     */
    @Override
    public ArrayList<String> read(String value) throws Exception {
        return new ArrayList<String>(Arrays.asList(value.split(SEPERATE)));
    }

    /**
     * get String with separate char from String list
     * @param value
     * @return String
     */
    @Override
    public String write(ArrayList<String> value) throws Exception {
        boolean hasComma = false;
        final StringBuilder builder = new StringBuilder();
        for (final String item : value) {
            if (hasComma) {
                builder.append(SEPERATE_CHAR);
            } else {
                hasComma = true;
            }
            builder.append(item);
        }
        return builder.toString();
    }
}
