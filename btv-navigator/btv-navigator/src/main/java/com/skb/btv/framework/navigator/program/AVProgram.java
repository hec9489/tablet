
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.program;

import android.os.Parcel;
import android.os.Parcelable;

import com.skb.btv.framework.navigator.xmlUtils.XmlUtilResolutionConverter;
import com.skb.btv.framework.navigator.common.Utils;
import com.skb.btv.framework.navigator.xmlUtils.XmlUtilAudioTypeConverter;
import com.skb.btv.framework.navigator.xmlUtils.XmlUtilBooleanYnConverter;
import com.skb.btv.framework.navigator.xmlUtils.XmlUtilStringConverter;
import com.tickaroo.tikxml.annotation.Element;
import com.tickaroo.tikxml.annotation.PropertyElement;
import com.tickaroo.tikxml.annotation.Xml;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * AVProgram Class
 *
 */

@SuppressWarnings("WeakerAccess")
@Xml(name = "program")
public class AVProgram implements IAVProgram, Parcelable {

    private static final String TAG =  "AVProgram";
    private static Map<String, Integer> res = new HashMap<String, Integer>();
    static {
        res.put("SD", 0);
        res.put("HD", 1);
        res.put("4K", 2);
        res.put("8K", 3);
    }

    private static Map<String, Integer> audioval = new HashMap<String, Integer>();
    static {
        audioval.put("none", 0);
        audioval.put("mono", 1);
        audioval.put("stereo", 2);
        audioval.put("ac3", 3);
    }

    int channelId = -1;

    @PropertyElement(name = "name", converter = XmlUtilStringConverter.class)
    String name;

    @PropertyElement(name = "description", converter = XmlUtilStringConverter.class)
    String description;

    @PropertyElement(name = "price")
    String price;

    @PropertyElement(name = "resolution", converter = XmlUtilResolutionConverter.class)
    int resolution;

    @PropertyElement(name = "audio", converter = XmlUtilAudioTypeConverter.class)
    int audio;

    @PropertyElement(name = "rating")
    int rating;

    @PropertyElement(name = "eventid")
    int eventid;

    @PropertyElement(name = "duration")
    int duration;

    @PropertyElement(name = "starttime")
    String startTime;

    @PropertyElement(name = "endtime")
    String endTime;

    @PropertyElement(name = "startyear")
    int startYear;

    @PropertyElement(name = "startmonth")
    int startMonth;

    @PropertyElement(name = "startday")
    int startDay;

    @PropertyElement(name = "endyear")
    int endYear;

    @PropertyElement(name = "endmonth")
    int endMonth;

    @PropertyElement(name = "endday")
    int endDay;

    @PropertyElement(name = "lang")
    String lang;

    @PropertyElement(name = "actors")
    String actors;

    @PropertyElement(name = "caption", converter = XmlUtilBooleanYnConverter.class)
    boolean isCaption;

    @PropertyElement(name = "contentNibble1")
    int contentNibble1;

    @PropertyElement(name = "contentNibble2")
    int contentNibble2;

    @PropertyElement(name = "contentUserNibble1")
    int contentUserNibble1;

    @PropertyElement(name = "contentUserNibble2")
    int contentUserNibble2;

    @PropertyElement(name = "director")
    String director;

    @PropertyElement(name = "dolbyAudio", converter = XmlUtilBooleanYnConverter.class)
    boolean isDolbyAudio;

    @PropertyElement(name = "dvs", converter = XmlUtilBooleanYnConverter.class)
    boolean isDvs;

    @PropertyElement(name = "programImagePath")
    String imagePath;

    @Element
    List<AVProgramLinkInfo> avProgramLinkInfo;

    @PropertyElement(name = "runningStatus")
    int runningStatus;

    @PropertyElement(name = "vodId")
    String vodid;

    Date startDate;

    Date endDate;

    int audioType = -1;

    List<AVProgramAudioType> avProgramaudioTypeList;

    int programId = -1;

    @PropertyElement(name = "SignLanguage")
    int signLanguage;

    public AVProgram(){
    }

    protected AVProgram(Parcel in) {
        name = in.readString();
        description = in.readString();
        price = in.readString();
        resolution = in.readInt();
        audio = in.readInt();
        eventid = in.readInt();
        rating = in.readInt();
        duration = in.readInt();
        startTime = in.readString();
        endTime = in.readString();
        startYear = in.readInt();
        startMonth = in.readInt();
        startDay = in.readInt();
        endYear = in.readInt();
        endMonth = in.readInt();
        endDay = in.readInt();
        lang = in.readString();
        actors = in.readString();
        isCaption = in.readByte() != 0;
        contentNibble1 = in.readInt();
        contentNibble2 = in.readInt();
        contentUserNibble1 = in.readInt();
        contentUserNibble2 = in.readInt();
        director = in.readString();
        isDolbyAudio = in.readByte() != 0;
        isDvs = in.readByte() != 0;
        imagePath = in.readString();
        avProgramLinkInfo = in.createTypedArrayList(AVProgramLinkInfo.CREATOR);
        runningStatus = in.readInt();
        vodid = in.readString();
        channelId = in.readInt();
        audioType = in.readInt();
        avProgramaudioTypeList = in.createTypedArrayList(AVProgramAudioType.CREATOR);
        programId = in.readInt();
        signLanguage = in.readInt();
    }

    public static final Creator<AVProgram> CREATOR = new Creator<AVProgram>() {
        /**
         * create service from parcel
         * @param in
         * @return AVProgram
         */
        @Override
        public AVProgram createFromParcel(Parcel in) {
            return new AVProgram(in);
        }

        /**
         * create service array
         * @param size
         * @return AVProgram[]
         */
        @Override
        public AVProgram[] newArray(int size) {
            return new AVProgram[size];
        }
    };

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(price);
        dest.writeInt(resolution);
        dest.writeInt(audio);
        dest.writeInt(eventid);
        dest.writeInt(rating);
        dest.writeInt(duration);
        dest.writeString(startTime);
        dest.writeString(endTime);
        dest.writeInt(startYear);
        dest.writeInt(startMonth);
        dest.writeInt(startDay);
        dest.writeInt(endYear);
        dest.writeInt(endMonth);
        dest.writeInt(endDay);
        dest.writeString(lang);
        dest.writeString(actors);
        dest.writeByte((byte) (isCaption ? 1 : 0));
        dest.writeInt(contentNibble1);
        dest.writeInt(contentNibble2);
        dest.writeInt(contentUserNibble1);
        dest.writeInt(contentUserNibble2);
        dest.writeString(director);
        dest.writeByte((byte) (isDolbyAudio ? 1 : 0));
        dest.writeByte((byte) (isDvs ? 1 : 0));
        dest.writeString(imagePath);
        dest.writeTypedList(avProgramLinkInfo);
        dest.writeInt(runningStatus);
        dest.writeString(vodid);
        dest.writeInt(channelId);
        dest.writeInt(audioType);
        dest.writeTypedList(avProgramaudioTypeList);
        dest.writeInt(programId);
        dest.writeInt(signLanguage);
    }

    /**
     * Get program name
     * @return program name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * set program name
     * @param _name program name
     */
    public void setName(String _name){
        name = _name;
    }

    /**
     * convert name string to xml string
     * @param _name program name
     */
    public void setNameStr(String _name){
        name = XmlUtilStringConverter(_name);
    }

    /**
     * Get program description
     * @return program description
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * set program description
     * @param _description program description
     */
    public void setDescription(String _description){
        description = _description;
    }

    /**
     * convert description string to xml string
     * @param _description program description
     */
    public void setDescriptionStr(String _description){
        description = XmlUtilStringConverter(_description);
    }

    /**
     * Get price
     * @return price
     */
    @Override
    public String getPrice() {
        return price;
    }

    /**
     * set program price
     * @param _price program price
     */
    public void setPrice(String _price){
        price = _price;
    }

    /**
     * Get resolution
     * @return resolution
     */
    @Override
    public int getResolution() {
        return resolution;
    }

    /**
     * set program _resolution
     * @param _resolution program _resolution
     */
    public void setResolution(int _resolution){
        resolution = _resolution;
    }

    /**
     * convert resolution string to xml string
     * @param _resolution program resolution
     */
    public void setResolutionStr(String _resolution){
        resolution = XmlUtilResolutionConverter(_resolution);
    }

    /**
     * Get audio type
     * @return audio type
     */
    public int getAudio() {
        return audio;
    }

    /**
     * set program audio type
     * @param _audio program audio type
     */
    public void setAudio(int _audio){
        avProgramaudioTypeList = null;
        this.audio = _audio;
        audioType = _audio;
    }

    /**
     * convert audio string to xml string
     * @param _audio
     */
    public void setAudioStr(String _audio){
        setAudio(XmlUtilAudioTypeConverter(_audio));
    }

    /**
     * Get event Id
     * @return eventid
     */
    @Override
    public  int getEventid(){
        return eventid;
    }

    /**
     * set program event Id
     * @param _eventid program event Id
     */
    public void setEventid(int _eventid){
        eventid = _eventid;
    }

    /**
     * Get rating
     * @return rating
     */
    @Override
    public int getRating() {
        return rating;
    }

    /**
     * set program rating
     * @param _rating program rating
     */
    public void setRating(int _rating){
        rating = _rating;
    }

    /**
     * Get duration
     * @return duration
     */
    @Override
    public int getDuration() {
        return duration;
    }

    /**
     * set program _duration
     * @param _duration program _duration
     */
    public void setDuration(int _duration){
        duration = _duration;
    }

    /**
     * Get program startTime
     * @return  startTime
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * set program _startTime
     * @param _startTime program _startTime
     */
    public void setStartTime(String _startTime){
        startTime = _startTime;
    }

    /**
     * Get program endTime
     * @return  endTime
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * set program _endTime
     * @param _endTime program _endTime
     */
    public void setEndTime(String _endTime){
        endTime = _endTime;
    }

    /**
     * Get program startYear
     * @return  startYear
     */
    public int getStartYear() {
        return startYear;
    }

    /**
     * set program _startYear
     * @param _startYear program _startYear
     */
    public void setStartYear(int _startYear){
        startYear = _startYear;
    }

    /**
     * Get program startMonth
     * @return  startMonth
     */
    public int getStartMonth() {
        return startMonth;
    }

    /**
     * set program _startMonth
     * @param _startMonth program _startMonth
     */
    public void setStartMonth(int _startMonth){
        startMonth = _startMonth;
    }

    /**
     * Get program startDay
     * @return  startDay
     */
    public int getStartDay() {
        return startDay;
    }

    /**
     * set program _startDay
     * @param _startDay program _startDay
     */
    public void setStartDay(int _startDay){
        startDay = _startDay;
    }

    /**
     * Get program endYear
     * @return  endYear
     */
    public int getEndYear() {
        return endYear;
    }

    /**
     * set program _endYear
     * @param _endYear program _endYear
     */
    public void setEndYear(int _endYear){
        endYear = _endYear;
    }

    /**
     * Get program endMonth
     * @return  endMonth
     */
    public int getEndMonth() {
        return endMonth;
    }

    /**
     * set program _endMonth
     * @param _endMonth program _endMonth
     */
    public void setEndMonth(int _endMonth){
        endMonth = _endMonth;
    }

    /**
     * Get program endDay
     * @return  endDay
     */
    public int getEndDay() {
        return endDay;
    }

    /**
     * set program _endDay
     * @param _endDay program _endDay
     */
    public void setEndDay(int _endDay){
        endDay = _endDay;
    }

    /**
     * Get program lang
     * @return  lang
     */
    public String getLang() {
        avProgramaudioTypeList = null;
        return lang;
    }

    /**
     * set program _lang
     * @param _lang program _lang
     */
    public void setLang(String _lang) {
        avProgramaudioTypeList = null;
        this.lang = _lang;
    }

    /**
     * Get actors
     * @return  actors
     */
    @Override
    public String getActors() {
        return actors;
    }

    /**
     * set _actors
     * @param _actors _actors
     */
    public void setActors(String _actors){
        actors = _actors;
    }

    /**
     * Get isCaption
     * @return  isCaption
     */
    @Override
    public boolean isCaption() {
        return isCaption;
    }

    /**
     * set _isCaption
     * @param _isCaption caption status
     */
    public void setIsCaption(boolean _isCaption){
        isCaption = _isCaption;
    }

    public void setIsCaptionStr(String _isCaption){
        isCaption = XmlUtilBooleanYnConverter(_isCaption);
    }

    /**
     * Get content nibble1
     * @return  contentNibble1
     */
    @Override
    public int getContentNibble1() {
        return contentNibble1;
    }

    /**
     * set content nibble1
     * @param _contentNibble1 caption content nibble1
     */
    public void setContentNibble1(int _contentNibble1){
        contentNibble1 = _contentNibble1;
    }

    /**
     * Get content nibble2
     * @return  contentNibble2
     */
    @Override
    public int getContentNibble2() {
        return contentNibble2;
    }

    /**
     * set content nibble2
     * @param _contentNibble2 caption content nibble2
     */
    public void setContentNibble2(int _contentNibble2){
        contentNibble2 = _contentNibble2;
    }

    /**
     * Get content user nibble1
     * @return  contentUserNibble1
     */
    @Override
    public int getContentUserNibble1() {
        return contentUserNibble1;
    }

    /**
     * set content user nibble1
     * @param _contentUserNibble1 caption content user nibble1
     */
    public void setContentUserNibble1(int _contentUserNibble1){
        contentUserNibble1 = _contentUserNibble1;
    }

    /**
     * Get content user nibble2
     * @return  contentUserNibble2
     */
    @Override
    public int getContentUserNibble2() {
        return contentUserNibble2;
    }

    /**
     * set content user nibble2
     * @param _contentUserNibble2 caption content user nibble2
     */
    public void setContentUserNibble2(int _contentUserNibble2){
        contentUserNibble2 = _contentUserNibble2;
    }

    /**
     * Get director
     * @return  director
     */
    @Override
    public String getDirector() {
        return director;
    }

    /**
     * set director
     * @param _director director
     */
    public void setDirector(String _director){
        director = _director;
    }

    /**
     * Get DolbyAudio status
     * @return  isDolbyAudio
     */
    @Override
    public boolean isDolbyAudio() {
        return isDolbyAudio;
    }

    /**
     * set DolbyAudio status
     * @param _isDolbyAudio DolbyAudio status
     */
    public void setIsDolbyAudio(boolean _isDolbyAudio){
        isDolbyAudio = _isDolbyAudio;
    }

    /**
     * set dolby audio
     * @param _isDolbyAudio DolbyAudio status
     */
    public void setIsDolbyAudioStr(String _isDolbyAudio){
        isDolbyAudio = XmlUtilBooleanYnConverter(_isDolbyAudio);
    }

    /**
     * Get isDvs
     * @return  isDvs
     */
    @Override
    public boolean isDvs() {
        return isDvs;
    }

    /**
     * set Dvs status
     * @param _isDvs Dvs status
     */
    public void setisDvs(boolean _isDvs){
        isDvs = _isDvs;
    }

    /**
     * set Dvs status from string
     * @param _isDvs
     */
    public void setisDvsStr(String _isDvs){
        isDvs = XmlUtilBooleanYnConverter(_isDvs);
    }

    /**
     * Get imagePath
     * @return  imagePath
     */
    @Override
    public String getImagePath() {
        return imagePath;
    }

    /**
     * set image path
     * @param _imagePath image path
     */
    public void setImagePath(String _imagePath){
        imagePath = _imagePath;
    }

    /**
     * Get program link info
     * @return  avProgramLinkInfo list
     */
    @Override
    public ArrayList<AVProgramLinkInfo> getAVProgramLinkInfo() {
        return (ArrayList<AVProgramLinkInfo>)avProgramLinkInfo;
    }

    /**
     * set program link info
     * @param addLinkInfo avProgramLinkInfo
     */
    public void addAVProgramLinkInfo(AVProgramLinkInfo addLinkInfo){
        if(avProgramLinkInfo == null){
            avProgramLinkInfo = new ArrayList<>();
        }
        avProgramLinkInfo.add(addLinkInfo);
    }

    /**
     * Get program running status
     * @return  running status
     */
    @Override
    public int getRunningStatus() {
        return runningStatus;
    }

    /**
     * set program running status
     * @param _runningStatus program running status
     */
    public void setRunningStatus(int _runningStatus) {
        runningStatus = _runningStatus;
    }

    /**
     * Get program vod Id
     * @return  vodid
     */
    @Override
    public String getVodId() {
        return vodid;
    }

    /**
     * set program vod Id
     * @param _vodid program vod Id
     */
    public void setVodId(String _vodid) {
        vodid = _vodid;
    }

    /**
     * Get program channel id
     * @return  channel id
     */
    @Override
    public int getChannelId() {
        return channelId;
    }

    /**
     * set program channel id
     * @param id program channel id
     */
    public void setChannelId(int id){
        channelId = id;
    }

    /**
     * Get start date
     * @return  start date
     */
    @Override
    public Date getStartDate(){
        if(startDate == null) {
            startDate = Utils.toDate(startYear, startMonth, startDay, startTime);
        }
        return startDate;
    }

    /**
     * set program start date
     * @param date program start date
     */
    public void setStartDate(Date date){
        startDate = date;
    }

    /**
     * Get end date
     * @return  end date
     */
    @Override
    public Date getEndDate(){
        if(endDate == null){
            endDate= Utils.toDate(endYear, endMonth, endDay, endTime );
        }
        return endDate;
    }

    /**
     * set program end date
     * @param date program end date
     */
    public void setEndDate(Date date){
        endDate = date;
    }

    /**
     * Get program audio type
     * @return  audio type
     */
    @Override
    public int getAVProgramAudioType() {
        return audioType;
    }

    /**
     * set program audio type
     * @param type program audio type
     */
    public void setAVProgramAudioType(int type){
        audioType = type;
    }

    /**
     * Get program audio type list
     * @return  audio type list
     */
    @Override
    public List<AVProgramAudioType> getAVProgramAudioTypeList() {
        if (avProgramaudioTypeList == null) {
            avProgramaudioTypeList = new ArrayList<>();
            avProgramaudioTypeList.add(new AVProgramAudioType(audio, lang));
        }
        return avProgramaudioTypeList;
    }

    /**
     * set program audio type list
     * @param list program audio type list
     */
    public void setAVProgramAudioTypeList(List<AVProgramAudioType> list){
        avProgramaudioTypeList = list;
    }

    /**
     * add program audio type
     * @param audioTypeInfo program audio type list
     */
    public void addAVProgramAudioTypeList(AVProgramAudioType audioTypeInfo){
        avProgramaudioTypeList.add(audioTypeInfo);
    }

    /**
     * Get program Id
     * @return  program Id
     */
    @Override
    public int getProgramId() {
        return programId;
    }

    /**
     * set program program Id
     * @param id program program Id
     */
    public void setProgramId(int id){
        programId = id;
    }

    /**
     * Get Sing Language On status
     * @return  Sing Language On status  (on = true, off = false)
     */
    @Override
    public boolean isSignLangOn(){
        return signLanguage == 1 ? true : false;
    }

    /**
     * set Sing Language On status
     * @param _signLanuage Sing Language On status
     */
    public void setIsSignLangOn(int _signLanuage){
        signLanguage = _signLanuage;
    }

    /**
     * xml string converter
     * @param value
     * @return String
     */
    private  String XmlUtilStringConverter(String value) {
        return value
                .replaceAll("&lt;", "<")
                .replaceAll("&gt;", ">")
                .replaceAll("&amp;", "&")
                .replaceAll("&quot;", "\"")
                .replaceAll("&apos;", "\'")
                .replaceAll("&nbsp;", " ");
    }

    /**
     * convert resolution string to int value
     * @param s
     * @return String
     */
    private int XmlUtilResolutionConverter(String s){
        return res.get(s);
    }

    /**
     * convert audio type string to int value
     * @param s
     * @return String
     */
    private int XmlUtilAudioTypeConverter(String s){
        return audioval.get(s);
    }

    /**
     * convert "Y","N" string to boolean value
     * @param s
     * @return boolean
     */
    private boolean XmlUtilBooleanYnConverter(String s){
        if (s != null) {
            if (!s.equals("N")) {
                return true;
            }
        }
        return false;
    }

}
