
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

import com.skb.btv.framework.navigator.log.SLog;

import static android.widget.ImageView.ScaleType.FIT_XY;


/**
 * WatermarkService Class
 *
 */

public class WatermarkService {

    private static final String TAG =  "WatermarkService";

    private static final int WATERMARK_IMAGE_WIDTH = 720;
    private static final int WATERMARK_IMAGE_HEIGHT = 480;

    private static Context mContext;
    private static WatermarkService mInstance;

    private BtvJniInterface mBtvJniInterface;
    private ImageView mWatermarkView;
    private int[] mBuffer;
    private static Boolean mActive;

    private WatermarkService(Context context) {
        SLog.d(TAG, "creator");
        mContext = context;
        mBtvJniInterface = BtvJniInterface.getInstance(mContext);
        mBtvJniInterface.initWatermark();
        mActive = true;
    }

    /**
     * Get WatermarkService Instance
     * @param context
     * @return WatermarkService
     */
    public static WatermarkService getInstance(Context context) {
        if(mInstance == null) {
            mInstance = new WatermarkService(context);
        }
        SLog.d(TAG, "getInstance - instance : " + mInstance);
        return mInstance;
    }

    public Context getContext(){
        return mContext;
    }

    /**
     * release WatermarkService
     */
    public void release() {
        SLog.d(TAG, "IN| " + "WatermarkService release");
        synchronized(this) {
            if (mBtvJniInterface != null) {
                mBtvJniInterface.releaseWatermark();
            }
        }
        mInstance = null;
    }

    /**
     * set ImageView for watermark
     */
    public void setWatermarkView(ImageView view) {
        SLog.d(TAG,  "IN|WatermarkService setWatermarkView = " + view);
        if (view != null) {
            mWatermarkView = view;
        }
    }

    /**
     * get watermark service prepared state
     * @return boolean : is prepared
     */
    public boolean isPrepared() {
        boolean ret = false;
        synchronized(this) {
            if (mBtvJniInterface != null) {
                ret = mBtvJniInterface.isWmPrepared();
                SLog.d(TAG,  "IN|WatermarkService isPrepared = " + ret);
            }
        }
        return ret;
    }

    /**
     * get watermark service activated state
     * @return boolean : is activated
     */
    public boolean isActivated() {
        boolean ret = false;
        synchronized(this) {
            if (mBtvJniInterface != null) {
                ret = mBtvJniInterface.isWmActivated();
                SLog.d(TAG,  "IN|WatermarkService isActivated = " + ret);
            }
        }
        return ret;
    }

    /**
     * set STB device ID
     * @param msb : reserved (should be 0)
     * @param lsb : LSB portion of the StbId
     */
    public void setStbId(short msb, int lsb) {
        synchronized(this) {
            if (mBtvJniInterface != null) {
                SLog.d(TAG,  "IN|WatermarkService setStbId[msb = " + msb + ", lsb = " + lsb + "]");
                mBtvJniInterface.setWmStbId(msb, lsb);
            }
        }
    }

    /**
     * Set the stub pattern
     * @param stubType : 0 disables all visible marks
     * – 1 is reserved
     * – 2 is reserved
     * – 3 activates visible marks for alpha blending test pattern
     */
    public void setStubEmbedding(int stubType) {
        synchronized(this) {
            if (mBtvJniInterface != null) {
                SLog.d(TAG,  "IN|WatermarkService setStubEmbedding[stubType = " + stubType + "]");
                mBtvJniInterface.setWmStubEmbedding(stubType);
            }
        }
    }

    /**
     * Turns watermark embedding on/off
     * @param active : 1 on 0 off
     */
    public void setActive(boolean active) {
        synchronized(this) {
            if (mBtvJniInterface != null) {
                SLog.d(TAG,  "IN|WatermarkService setActive[active = " + active + "]");
                mActive = active;
                mBtvJniInterface.setWmActive(active ? 1 : 0);
                /*if (active) {
                    processSurface();
                }*/
            }
        }
    }

    /**
     * start drawing watermark
     */
    public void startWatermark() {
        SLog.d(TAG,  "IN|WatermarkService startWatermark");
        if(mActive == false)
            drawWatermark(null);
        else {
            processSurface();
        }
    }

    /**
     * stop drawing watermark
     */
    public void stopWatermark() {
        SLog.d(TAG,  "IN|WatermarkService stopWatermark");
        drawWatermark(null);
    }

    /**
     * Applies a watermark to an surface
     */
    private void processSurface() {
        SLog.d(TAG, "processSurface()");
        if (mBuffer == null) {
            mBuffer = new int[WATERMARK_IMAGE_WIDTH * WATERMARK_IMAGE_HEIGHT];
        }
        synchronized(this) {
            if (mBtvJniInterface != null) {
                mBtvJniInterface.processWmSurface(mBuffer);
                Bitmap bitmap = Bitmap.createBitmap(mBuffer , WATERMARK_IMAGE_WIDTH , WATERMARK_IMAGE_HEIGHT , Bitmap.Config.ARGB_8888);
                drawWatermark(bitmap);
            }
        }
    }

    /**
     * draw watermark bitmap image to an surface
     */
    private void drawWatermark(Bitmap bitmap) {
        if (mWatermarkView != null) {
            int width = mWatermarkView.getMeasuredWidth();
            int height = mWatermarkView.getMeasuredHeight();
            SLog.d(TAG, "drawWatermark() width:" + width + ", height:" + height);
            if (bitmap != null && width > 0 && height > 0) {
                mWatermarkView.setScaleType(FIT_XY);
                mWatermarkView.setImageBitmap(bitmap);
                SLog.d(TAG, "drawWatermark() setImageBitmap");
            } else {
                mWatermarkView.setImageResource(android.R.color.transparent);
                SLog.d(TAG, "drawWatermark() setImageResource(transparent)");
            }
        }
    }
}
