
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.config;

import com.tickaroo.tikxml.annotation.Attribute;
import com.tickaroo.tikxml.annotation.TextContent;
import com.tickaroo.tikxml.annotation.Xml;


/**
 * PropertiesConfig Class
 *
 */

@SuppressWarnings("WeakerAccess")
@Xml(name = "entry")
public class PropertiesConfig {
    @Attribute(name = "key")
    String key;

    @TextContent
    String value;

    /**
     * get property key
     * @return String
     */
    public String getKey(){
        return key;
    }

    /**
     * get property value
     * @return String
     */
    public String getValue(){
        return value;
    }
}
