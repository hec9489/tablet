
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.xmlUtils;

import com.tickaroo.tikxml.TypeConverter;

import java.util.HashMap;
import java.util.Map;


/**
 * XmlUtilAudioTypeConverter Class
 *
 */

public class XmlUtilAudioTypeConverter implements TypeConverter<Integer> {

    private static Map<String, Integer> res = new HashMap<String, Integer>();
    private static Map<Integer, String> reverseRes;

    static {
        res.put("none", 0);
        res.put("mono", 1);
        res.put("stereo", 2);
        res.put("ac3", 3);
        reverseRes = reverseMap(res);
    }

    static <K, V> Map<V, K> reverseMap(Map<K, V> map) {
        final Map<V, K> reverseMap = new HashMap<V, K>();
        for (final Map.Entry<K, V> entry : map.entrySet()) {
            reverseMap.put(entry.getValue(), entry.getKey());
        }
        return reverseMap;
    }

    /**
     * get Integer value from name
     * @param s String
     * @return Integer
     */
    @Override
    public Integer read(String s) throws Exception {
        return res.get(s);
    }

    /**
     * get String from Integer value
     * @param integer Integer
     * @return String
     */
    @Override
    public String write(Integer integer) throws Exception {
        return reverseRes.get(integer);
    }
}
