
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.program;

import java.util.ArrayList;


/**
 * IAVPrograms Interface
 *
 */

public interface IAVPrograms {
    int getSid();
    void setSid(int sid);
    ArrayList<AVProgram> getAVProgramList();
}
