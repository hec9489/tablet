
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.product;

import android.os.Parcel;
import android.os.Parcelable;

import com.tickaroo.tikxml.annotation.Element;
import com.tickaroo.tikxml.annotation.Xml;

import java.util.ArrayList;
import java.util.List;


/**
 * ProductList Class
 *
 */

@Xml(name = "root")
public class ProductList implements IProductList, Parcelable {

    private static final String TAG =  "ProductList";

    boolean mReady;
    @SuppressWarnings("WeakerAccess")
    @Element
    List<Product> productList;

    public ProductList() {
    }

    public ProductList(boolean ready){
        mReady = ready;
    }

    protected ProductList(Parcel in) {
        mReady = in.readByte() != 0;
        productList = in.createTypedArrayList(Product.CREATOR);
    }

    public static final Creator<ProductList> CREATOR = new Creator<ProductList>() {
        /**
         * create service from parcel
         * @param in
         * @return ProductList
         */
        @Override
        public ProductList createFromParcel(Parcel in) {
            return new ProductList(in);
        }

        /**
         * create service array
         * @param size
         * @return ProductList[]
         */
        @Override
        public ProductList[] newArray(int size) {
            return new ProductList[size];
        }
    };

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (mReady ? 1 : 0));
        dest.writeTypedList(productList);
    }

    /**
     * Get ready state
     * @return  ready state
     */
    @Override
    public boolean isReady() {
        if(productList != null && productList.size() > 0){
            mReady = true;
        } else {
            mReady = false;
        }
        return mReady;
    }

    /**
     * Get product list
     * @return  product list
     */
    @Override
    public ArrayList<Product> getProductList() {
        return (ArrayList<Product>)productList;
    }
}
