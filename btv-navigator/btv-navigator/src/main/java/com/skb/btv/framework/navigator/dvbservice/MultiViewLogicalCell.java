
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;

import android.os.Parcel;
import android.os.Parcelable;

import com.tickaroo.tikxml.annotation.Attribute;
import com.tickaroo.tikxml.annotation.Element;
import com.tickaroo.tikxml.annotation.Xml;

import java.util.List;


/**
 * MultiViewLogicalCell Class
 *
 */

@SuppressWarnings("WeakerAccess")
@Xml(name = "logical_cell")
public class MultiViewLogicalCell implements IMultiViewLogicalCell, Parcelable {

    private static final String TAG =  "MultiViewLogicalCell";

    @Attribute(name = "logical_cell_id")
    int logicalCellId;

    @Attribute(name = "logical_cell_presentation_info")
    int logicalCellPresentationInfo;

    @Attribute(name = "elementary_cell_ids")
    int elementaryCellIds;

    @Attribute(name = "cell_linkage_info")
    int cellLinkageInfo;

    @Attribute(name = "service_id")
    int serviceID;

    @Attribute(name = "channel_number")
    int channelNumber;

    @Attribute(name = "iso_639_language_code")
    int iso639LanguageCode;

    @Attribute(name = "cell_name")
    String cellName;

    @Attribute(name = "ip_address")
    String ipAddress;

    @Attribute(name = "port")
    int port;

    @Attribute(name = "pcr_PID")
    int pcrPid;

    @Attribute(name = "res")
    int res;

    @Attribute(name = "audio_PID")
    int audioPID;

    @Attribute(name = "position_type")
    int positionType;

    @Attribute(name = "divisor")
    int divisor;

    @Attribute(name = "cell_base_x")
    int cellBaseX;

    @Attribute(name = "cell_base_y")
    int cellBaseY;

    @Attribute(name = "cell_width")
    int cellWidth;

    @Attribute(name = "cell_height")
    int cellHeight;

    @Attribute(name = "cell_channel_name")
    String cellChannelName;

    @Attribute(name = "group_id")
    int groupID;

    @Attribute(name = "group_order")
    int groupOrder;

    @Element
    List<ElementaryStream> elementaryStream;

    public MultiViewLogicalCell() {
    }


    protected MultiViewLogicalCell(Parcel in) {
        logicalCellId = in.readInt();
        logicalCellPresentationInfo = in.readInt();
        elementaryCellIds = in.readInt();
        cellLinkageInfo = in.readInt();
        serviceID = in.readInt();
        channelNumber = in.readInt();
        iso639LanguageCode = in.readInt();
        cellName = in.readString();
        ipAddress = in.readString();
        port = in.readInt();
        pcrPid = in.readInt();
        res = in.readInt();
        audioPID = in.readInt();
        positionType = in.readInt();
        divisor = in.readInt();
        cellBaseX = in.readInt();
        cellBaseY = in.readInt();
        cellWidth = in.readInt();
        cellHeight = in.readInt();
        cellChannelName = in.readString();
        groupID = in.readInt();
        groupOrder = in.readInt();
        elementaryStream = in.createTypedArrayList(ElementaryStream.CREATOR);
    }

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(logicalCellId);
        dest.writeInt(logicalCellPresentationInfo);
        dest.writeInt(elementaryCellIds);
        dest.writeInt(cellLinkageInfo);
        dest.writeInt(serviceID);
        dest.writeInt(channelNumber);
        dest.writeInt(iso639LanguageCode);
        dest.writeString(cellName);
        dest.writeString(ipAddress);
        dest.writeInt(port);
        dest.writeInt(pcrPid);
        dest.writeInt(res);
        dest.writeInt(audioPID);
        dest.writeInt(positionType);
        dest.writeInt(divisor);
        dest.writeInt(cellBaseX);
        dest.writeInt(cellBaseY);
        dest.writeInt(cellWidth);
        dest.writeInt(cellHeight);
        dest.writeString(cellChannelName);
        dest.writeInt(groupID);
        dest.writeInt(groupOrder);
        dest.writeTypedList(elementaryStream);
    }

    public static final Creator<MultiViewLogicalCell> CREATOR = new Creator<MultiViewLogicalCell>() {
        /**
         * create service from parcel
         * @param in
         * @return MultiViewLogicalCell
         */
        @Override
        public MultiViewLogicalCell createFromParcel(Parcel in) {
            return new MultiViewLogicalCell(in);
        }

        /**
         * create service array
         * @param size
         * @return MultiViewLogicalCell[]
         */
        @Override
        public MultiViewLogicalCell[] newArray(int size) {
            return new MultiViewLogicalCell[size];
        }
    };

    /**
     * Get id
     * @return   id
     */
    @Override
    public int getLogicalCellId(){
        return logicalCellId;
    }

    /**
     * Get presentation info
     * @return  presentation info
     */
    @Override
    public int getLogicalCellPresentationInfo(){
        return logicalCellPresentationInfo;
    }

    /**
     * Get elementary cell ids
     * @return   elementary cell ids
     */
    public int getElementaryCellIds(){
        return elementaryCellIds;
    }

    /**
     * Get linkage info
     * @return  linkage info
     */
    @Override
    public int getCellLinkageInfo() {
        return cellLinkageInfo;
    }

    /**
     * Get service id
     * @return  service id
     */
    @Override
    public int getServiceID(){
        return serviceID;
    }

    /**
     * Get channel Number
     * @return  channel Number
     */
    @Override
    public int getChannelNumber(){
        return channelNumber;
    }

    /**
     * Get iso639LanguageCode
     * @return  iso639LanguageCode
     */
    @Override
    public int getIso639LanguageCode(){
        return iso639LanguageCode;
    }

    /**
     * Get cellName
     * @return  cellName
     */
    @Override
    public String getCellName(){
        return cellName;
    }

    /**
     * Get ipAddress
     * @return  ipAddress
     */
    @Override
    public String getIpAddress(){
        return ipAddress;
    }

    /**
     * Get port
     * @return  port
     */
    @Override
    public int getPort(){
        return port;
    }

    /**
     * Get pcrPid
     * @return  pcrPid
     */
    @Override
    public int getPcrPid(){
        return pcrPid;
    }

    /**
     * Get res
     * @return  res
     */
    @Override
    public int getRes(){
        return res;
    }

    /**
     * Get audioPID
     * @return  audioPID
     */
    @Override
    public int getAudioPID(){
        return audioPID;
    }

    /**
     * Get positionType
     * @return  positionType
     */
    @Override
    public int getPositionType(){
        return positionType;
    }

    /**
     * Get divisor
     * @return  divisor
     */
    @Override
    public int getDivisor(){
        return divisor;
    }

    /**
     * Get base x
     * @return  base x
     */
    @Override
    public int getCellBaseX(){
        return cellBaseX;
    }

    /**
     * Get base y
     * @return  base y
     */
    @Override
    public int getCellBaseY(){
        return cellBaseY;
    }

    /**
     * Get width
     * @return  width
     */
    @Override
    public int getCellWidth(){
        return cellWidth;
    }

    /**
     * Get height
     * @return  height
     */
    @Override
    public int getCellHeight(){
        return cellHeight;
    }

    /**
     * Get channel name
     * @return  channel name
     */
    @Override
    public String getCellChannelName(){
        return cellChannelName;
    }

    /**
     * Get groupID
     * @return  groupID
     */
    @Override
    public int getGroupID(){
        return groupID;
    }

    /**
     * Get groupOrder
     * @return  groupOrder
     */
    @Override
    public int getGroupOrder(){
        return groupOrder;
    }

    /**
     * Get elementary stream list
     * @return  elementary stream list
     */
    public List<ElementaryStream> getElementaryStreamList() {
        return elementaryStream;
    }

    /**
     * Get multiview channel uri
     * @return  multiview channel uri
     */
    @Override
    public String getMultiViewChannelUri() {
        String channelUrl
                = "skbiptv://"
                + getIpAddress()
                + ":" + getPort() + "?"
                + "ch="+getChannelNumber()
                + "&ci=0"
                + "&cp=0"
                + "&pp=" + getPcrPid()
                + "&vp=" + ((getElementaryStreamList().size() > 1) ? getElementaryStreamList().get(0).getStreamPid() : "0")
                + "&vc=" + ((getElementaryStreamList().size() > 1) ? getElementaryStreamList().get(0).getStreamType() : "0")
                + "&ap=" + ((getElementaryStreamList().size() > 1) ? getElementaryStreamList().get(1).getStreamPid() : "0")
                + "&ac=" + ((getElementaryStreamList().size() > 1) ? getElementaryStreamList().get(1).getStreamType() : "0");
        return channelUrl;
    }
}
