
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator;

import android.content.Context;
import android.util.Log;


/**
 * BtvNavigator Class
 *
 */

public class BtvNavigator {

    public class BUILD {
        public static final int API_LEVEL         = 2;

        public static final int VERSION_MAJOR     = 2;
        public static final int VERSION_MINOR     = 0;
        public static final int VERSION_BUILD     = 1;
        public static final String CODE_NAME      = "NAVIGATOR_" + VERSION_MAJOR + "." + VERSION_MINOR + "." + VERSION_BUILD;
    }

    public static final String TAG =  "BtvNavigator_" + BUILD.CODE_NAME;

    /**
     * Get DvbSIService
     * @param context
     * @return DvbSIService
     */
    public static DvbSIService getDvbSIService(Context context) {
        Log.d(TAG, "IN| " + "getDvbSIService context : " + context);
        return DvbSIService.getInstance(context);
    }

    /**
     * Get WatermarkService
     * @param context
     * @return WatermarkService
     */
    public static WatermarkService getWatermarkService(Context context) {
        Log.d(TAG, "IN| " + "getWatermarkService context : " + context);
        return WatermarkService.getInstance(context);
    }

    /**
     * Get BtvNavigator Version
     * @return String
     */
    public static String getVersion(){
        Log.d(TAG, "IN| " + "getVersion VERSION " + BUILD.VERSION_MAJOR + "." + BUILD.VERSION_MINOR);
        return BUILD.VERSION_MAJOR + "." + BUILD.VERSION_MINOR;
    }

}
