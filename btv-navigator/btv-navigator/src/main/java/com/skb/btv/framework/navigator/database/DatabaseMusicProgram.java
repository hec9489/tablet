
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.database;

import android.content.Context;


/**
 * DatabaseMusicProgram Class
 *
 */

public class DatabaseMusicProgram extends DatabaseProgramBase {

    private static final String TAG = "DatabaseMusicProgram";

    /**
     * SQLiteOpenHelper new
     * @param context : context
     */
    public DatabaseMusicProgram(Context context) {
        dbHelper = new DatabaseMusicOpenHelper(context);
    }

}
