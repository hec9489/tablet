
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.xmlUtils;

import com.tickaroo.tikxml.TypeConverter;

import java.util.HashMap;
import java.util.Map;


/**
 * XmlUtilResolutionConverter Class
 *
 */

public class XmlUtilResolutionConverter implements TypeConverter<Integer> {

    private static Map<String, Integer> res = new HashMap<String, Integer>();
    private static Map<Integer, String> reverseRes;

    static {
        res.put("SD", 0);
        res.put("HD", 1);
        res.put("4K", 2);
        res.put("8K", 3);
        reverseRes = reverseMap(res);
    }

    static <K, V> Map<V, K> reverseMap(Map<K, V> map) {
        final Map<V, K> reverseMap = new HashMap<V, K>();
        for (final Map.Entry<K, V> entry : map.entrySet()) {
            reverseMap.put(entry.getValue(), entry.getKey());
        }
        return reverseMap;
    }

    /**
     * get Integer from String
     * @param s String
     * @return Integer
     */
    @Override
    public Integer read(String s) throws Exception {
        return res.get(s);
    }

    /**
     * get String from Integer
     * @param integer
     * @return String
     */
    @Override
    public String write(Integer integer) throws Exception {
        return reverseRes.get(integer);
    }
}
