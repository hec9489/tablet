
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;

import android.os.Parcel;
import android.os.Parcelable;

import com.skb.btv.framework.navigator.IBtvNavigator;
import com.tickaroo.tikxml.annotation.Element;
import com.tickaroo.tikxml.annotation.Xml;

import java.util.ArrayList;
import java.util.List;


/**
 * MusicDvbServiceList Class
 *
 */

@Xml(name = "root")
public class MusicDvbServiceList extends DvbServiceList implements IMusicDvbServiceList {

    private static final String TAG =  "MusicDvbServiceList";

    boolean mReady;

    @SuppressWarnings("WeakerAccess")
    @Element
    List<MusicDvbService> musicChannelList;

    public MusicDvbServiceList(){
    }

    public MusicDvbServiceList(boolean ready){
        mReady = ready;
    }

    public MusicDvbServiceList(int ready, List<MusicDvbService> list){
        mReady = ready == 1 ? true : false;
        musicChannelList = list;
    }

    protected MusicDvbServiceList(Parcel in) {
        mReady = in.readByte() != 0;
        musicChannelList = in.createTypedArrayList(MusicDvbService.CREATOR);
    }

    public static final Parcelable.Creator<MusicDvbServiceList> CREATOR = new Parcelable.Creator<MusicDvbServiceList>() {
        /**
         * create service from parcel
         * @param in
         * @return MusicDvbServiceList
         */
        @Override
        public MusicDvbServiceList createFromParcel(Parcel in) {
            return new MusicDvbServiceList(in);
        }

        /**
         * create service array
         * @param size
         * @return MusicDvbServiceList[]
         */
        @Override
        public MusicDvbServiceList[] newArray(int size) {
            return new MusicDvbServiceList[size];
        }
    };

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (mReady ? 1 : 0));
        dest.writeTypedList(musicChannelList);
    }

    /**
     * Get ready state
     * @return  ready state
     */
    @Override
    public boolean isReady() {
        if(musicChannelList != null && musicChannelList.size() > 0){
            mReady = true;
        } else {
            mReady = false;
        }
        return mReady;
    }

    /**
     * Get MusicDvbService list
     * @return  MusicDvbService list
     */
    @Override
    public ArrayList<MusicDvbService> getList() {
        return (ArrayList<MusicDvbService>)musicChannelList;
    }

    /**
     * add music channel
     * @param musicChannel MusicDvbService
     */
    public void addMusicChannel(MusicDvbService musicChannel){
        if(musicChannelList == null){
            musicChannelList = new ArrayList<>();
        }
        musicChannelList.add(musicChannel);
    }

    /**
     * Get MusicDvbService type
     * @return  MusicDvbService type DVBSERVICE_TYPE_MUSIC
     */
    @Override
    public int getRequestType(){
        return IBtvNavigator.DVBSERVICE_TYPE_MUSIC;
    }
}