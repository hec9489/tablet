
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.program;


/**
 * IAVProgramAudioType Interface
 *
 */

public  interface IAVProgramAudioType {
    String getIso639_language_code();
    int getAudioType();
}
