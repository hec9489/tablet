
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;

import android.os.Parcel;
import android.os.Parcelable;

import com.tickaroo.tikxml.annotation.Attribute;
import com.tickaroo.tikxml.annotation.Xml;


/**
 * ElementaryStream Class
 *
 */

@SuppressWarnings("WeakerAccess")
@Xml(name = "elementary_stream")
public class ElementaryStream implements IElementaryStream, Parcelable {

    private static final String TAG =  "ElementaryStream";

    @Attribute(name = "stream_pid")
    int streamPid = -1;

    @Attribute(name = "stream_type")
    int streamType = -1;

    public ElementaryStream() {
    }

    public ElementaryStream(int pid, int type){
        this.streamPid = pid;
        this.streamType = type;
    }

    protected ElementaryStream(Parcel in) {
        streamPid = in.readInt();
        streamType = in.readInt();
    }

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(streamPid);
        dest.writeInt(streamType);
    }

    public static final Creator<ElementaryStream> CREATOR = new Creator<ElementaryStream>() {
        /**
         * create service from parcel
         * @param in
         * @return ElementaryStream
         */
        @Override
        public ElementaryStream createFromParcel(Parcel in) {
            return new ElementaryStream(in);
        }

        /**
         * create service array
         * @param size
         * @return ElementaryStream[]
         */
        @Override
        public ElementaryStream[] newArray(int size) {
            return new ElementaryStream[size];
        }
    };

    /**
     * Get stream type
     * @return  stream type
     */
    @Override
    public int getStreamType() {
        return streamType;
    }

    /**
     * Get stream pid
     * @return  stream pid
     */
    @Override
    public int getStreamPid() {
        return streamPid;
    }
}
