
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.program;

import android.os.Parcel;

import com.skb.btv.framework.navigator.IBtvNavigator;
import com.tickaroo.tikxml.annotation.Element;
import com.tickaroo.tikxml.annotation.Xml;

import java.util.ArrayList;
import java.util.List;


/**
 * AVProgramList Class
 *
 */

@Xml(name = "root")
public class AVProgramList extends ProgramList implements IAVProgramList {

    private static final String TAG =  "AVProgramList";

    boolean mReady;

    @SuppressWarnings("WeakerAccess")
    @Element
    List<AVPrograms> avProgramsList;

    public AVProgramList(){
    }

    public AVProgramList(boolean ready){
        mReady = ready;
    }

    protected AVProgramList(Parcel in) {
        mReady = in.readByte() != 0;
        avProgramsList = in.createTypedArrayList(AVPrograms.CREATOR);
    }

    public static final Creator<AVProgramList> CREATOR = new Creator<AVProgramList>() {
        /**
         * create service from parcel
         * @param in
         * @return AVProgramList
         */
        @Override
        public AVProgramList createFromParcel(Parcel in) {
            return new AVProgramList(in);
        }

        /**
         * create service array
         * @param size
         * @return AVProgramList[]
         */
        @Override
        public AVProgramList[] newArray(int size) {
            return new AVProgramList[size];
        }
    };

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (mReady ? 1 : 0));
        dest.writeTypedList(avProgramsList);
    }

    /**
     * Get ready state
     * @return  ready state
     */
    @Override
    public boolean isReady() {
        if(avProgramsList != null && avProgramsList.size() > 0){
            mReady = true;
        } else {
            mReady = false;
        }
        return mReady;
    }

    /**
     * Get AV programs list
     * @return  AV programs list
     */
    @Override
    public ArrayList<AVPrograms> getAVProgramsList() {
        return (ArrayList<AVPrograms>)avProgramsList;
    }

    /**
     * add AV program
     * @param addPrograms  AV programs
     */
    public void addAVPrograms(AVPrograms addPrograms){
        if(avProgramsList == null){
            avProgramsList = new ArrayList<>();
        }
        avProgramsList.add(addPrograms);
    }

    /**
     * Get request type
     * @return  AV DVBSERVICE_TYPE_AV
     */
    @Override
    public int getRequestType(){
        return IBtvNavigator.DVBSERVICE_TYPE_AV;
    }
}
