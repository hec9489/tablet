
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;

import android.os.Parcel;
import android.os.Parcelable;

import com.tickaroo.tikxml.annotation.Attribute;
import com.tickaroo.tikxml.annotation.Element;
import com.tickaroo.tikxml.annotation.Xml;

import java.util.ArrayList;
import java.util.List;


/**
 * MultiViewDvbService Class
 *
 */

@SuppressWarnings("WeakerAccess")
@Xml(name = "channel")
public class MultiViewDvbService implements IMultiViewDvbService, Parcelable{

    private static final String TAG =  "MultiViewDvbService";

    @Attribute(name = "sid")
    int sid;

    @Attribute(name = "multiview_id")
    int multiviewId;

    @Attribute(name = "multiview_entry_point")
    int multiviewEntryPoint;

    @Attribute(name = "num_horizontal_elementary_cells")
    int numberOfHorizentalElementaryCells;

    @Attribute(name = "num_vertical_elementary_cells")
    int numberOfVerticalElementaryCells;

    @Attribute(name = "rating")
    int rating;

    @Attribute(name = "group_id")
    int groupID;

    @Attribute(name = "group_name")
    String groupName;

    @Attribute(name = "group_order")
    int groupOrder;

    @Element
    List<MultiViewLogicalCell> logicalCellInfos;

    public MultiViewDvbService() {
    }


    protected MultiViewDvbService(Parcel in) {
        sid = in.readInt();
        multiviewId = in.readInt();
        multiviewEntryPoint = in.readInt();
        numberOfHorizentalElementaryCells = in.readInt();
        numberOfVerticalElementaryCells = in.readInt();
        rating = in.readInt();
        groupID = in.readInt();
        groupName = in.readString();
        groupOrder = in.readInt();
        logicalCellInfos = in.createTypedArrayList(MultiViewLogicalCell.CREATOR);
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(sid);
        dest.writeInt(multiviewId);
        dest.writeInt(multiviewEntryPoint);
        dest.writeInt(numberOfHorizentalElementaryCells);
        dest.writeInt(numberOfVerticalElementaryCells);
        dest.writeInt(rating);
        dest.writeInt(groupID);
        dest.writeString(groupName);
        dest.writeInt(groupOrder);
        dest.writeTypedList(logicalCellInfos);
    }

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MultiViewDvbService> CREATOR = new Creator<MultiViewDvbService>() {
        /**
         * create service from parcel
         * @param in
         * @return MultiViewDvbService
         */
        @Override
        public MultiViewDvbService createFromParcel(Parcel in) {
            return new MultiViewDvbService(in);
        }

        /**
         * create service array
         * @param size
         * @return MultiViewDvbService[]
         */
        @Override
        public MultiViewDvbService[] newArray(int size) {
            return new MultiViewDvbService[size];
        }
    };

    /**
     * Get sid
     * @return  sid
     */
    public int getSid() {
        return sid;
    }

    /**
     * Get multiview Id
     * @return  multiview Id
     */
    @Override
    public int getMultiviewId() {
        return multiviewId;
    }

    /**
     * Get multiview entry point
     * @return  multiview entry point
     */
    @Override
    public int getMultiviewEntryPoint() {
        return multiviewEntryPoint;
    }

    /**
     * Get number of horizental elementary cells
     * @return  numberOfHorizentalElementaryCells
     */
    @Override
    public int getNumberOfHorizentalElementaryCells() {
        return numberOfHorizentalElementaryCells;
    }

    /**
     * Get number of vertical elementary cells
     * @return  numberOfVerticalElementaryCells
     */
    @Override
    public int getNumberOfVerticalElementaryCells() {
        return numberOfVerticalElementaryCells;
    }

    /**
     * Get rating
     * @return  rating
     */
    @Override
    public int getRating() {
        return rating;
    }

    /**
     * Get groupID
     * @return  groupID
     */
    @Override
    public int getGroupID() {
        return groupID;
    }

    /**
     * Get groupName
     * @return  groupName
     */
    @Override
    public String getGroupName() {
        return groupName;
    }

    /**
     * Get groupOrder
     * @return  groupOrder
     */
    @Override
    public int getGroupOrder() {
        return groupOrder;
    }

    /**
     * Get logical cell info list
     * @return  logical cell info list
     */
    @Override
    public ArrayList<MultiViewLogicalCell> getMultiViewLogicalCellInfoList() {
        return (ArrayList<MultiViewLogicalCell> ) logicalCellInfos;
    }

    /**
     * add multi view logical cell info
     * @param locellInfo
     */
    public void addMultiViewLogicalCell(MultiViewLogicalCell locellInfo){
        logicalCellInfos.add(locellInfo);
    }
}
