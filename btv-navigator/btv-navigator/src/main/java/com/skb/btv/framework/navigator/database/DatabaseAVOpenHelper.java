
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.skb.btv.framework.navigator.log.SLog;


/**
 * DatabaseAVOpenHelper Class
 *
 */

public class DatabaseAVOpenHelper extends SQLiteOpenHelper {

    private static final String BULK_PROGRAM_DB_FILE = "/data/btv_home/dvbsi/programall.db";
    private static final String TAG = "DatabaseAVOpenHelper";

    public DatabaseAVOpenHelper(Context context) {
        super(context, BULK_PROGRAM_DB_FILE, null, 1);
    }
    /**
     * create callback
     * @param db SQLiteDatabase
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    /**
     * upgrade callback
     * @param db SQLiteDatabase
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * open callback
     * @param db SQLiteDatabase
     */
    @Override
    public void onOpen(SQLiteDatabase db){
	    SLog.d(TAG, "DatabaseAVOpenHelper() open()");
        //db.rawQuery(" PRAGMA journal_mode = DELETE", null);
        db.disableWriteAheadLogging();
        super.onOpen(db);
    }
}
