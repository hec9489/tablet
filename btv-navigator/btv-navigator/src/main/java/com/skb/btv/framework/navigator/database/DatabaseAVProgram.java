
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.database;

import android.content.Context;
import android.database.Cursor;

import com.skb.btv.framework.navigator.program.AVProgram;
import com.skb.btv.framework.navigator.program.AVProgramList;
import com.skb.btv.framework.navigator.log.SLog;
import com.skb.btv.framework.navigator.program.AVPrograms;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * DatabaseAVProgram Class
 *
 */

public class DatabaseAVProgram extends DatabaseProgramBase {

    private static final String TAG = "DatabaseAVProgram";

    /**
     * SQLiteOpenHelper new
     * @param context : context
     */
    public DatabaseAVProgram(Context context) {
        dbHelper = new DatabaseAVOpenHelper(context);
    }

    /**
     * Query one channel program information by current time
     * @param ch : channel number
     * @return : AVProgramList
     */
    public AVProgramList getDvbServiceQueryData(int ch){
        AVProgramList queryAllEntities = new AVProgramList();
        SLog.d(TAG ,"getDvbServiceQueryData() start ch : "+ch);
        AVPrograms queryDvbEntities = new AVPrograms(ch);
        String whereClause = DatabaseColumnIndex.COL_CHNUM_STRING + " = ? AND "+DatabaseColumnIndex.COL_ENDFTIME_STRING+" >= ? ";
        String[] whereArgs = new String[] { String.valueOf(ch), currentTimeFormat.format(new Date()) };
        Cursor cursor = database.query(TABLE_NAME,
                null, whereClause, whereArgs, null, null,
                DatabaseColumnIndex.COL_CHNUM_STRING+ " ASC, " +
                        DatabaseColumnIndex.COL_STARTFTIME_STRING + " ASC", null);

        if(cursor != null && cursor.getCount()>0) {
            cursor.moveToFirst();
            SLog.d(TAG, "getDvbServiceQueryData() Program Count :  "+cursor.getCount());
            while (!cursor.isAfterLast()) {
                AVProgram programEntity = cursorToComment(cursor);
                queryDvbEntities.addAVProgram(programEntity);
                cursor.moveToNext();
            }
        }
        if(cursor != null) cursor.close();

        queryAllEntities.addAVPrograms(queryDvbEntities);
        SLog.d(TAG ,"getDvbServiceQueryData() end");
        return queryAllEntities;
    }

    /**
     * Query multi channel program information by current time
     * @param chList : channel list
     * @return : List<AVPrograms>
     */
    public List<AVPrograms> getMultiDvbServiceQueryData(String chList){
        SLog.d(TAG ,"getMultiDvbServiceQueryData() start chList : "+chList);
        List<AVPrograms> queryAllEntities = new ArrayList<AVPrograms>();
        String[] chs = chList.split("\\|");
        Cursor cursor;
        if(chs.length > 0){
            for(int i=0; i<chs.length; i++){
                try {
                    int ch = Integer.parseInt(chs[i]);
                    AVPrograms queryDvbEntities = new AVPrograms(ch);
                    String whereClause = DatabaseColumnIndex.COL_CHNUM_STRING + " = ? AND "+DatabaseColumnIndex.COL_ENDFTIME_STRING+" >= ? ";
                    String[] whereArgs = new String[] { String.valueOf(ch), currentTimeFormat.format(new Date()) };
                    cursor = database.query(TABLE_NAME,
                            null, whereClause, whereArgs, null, null,
                            DatabaseColumnIndex.COL_CHNUM_STRING + " ASC, " +
                                    DatabaseColumnIndex.COL_STARTFTIME_STRING + " ASC", null);

                    if(cursor != null && cursor.getCount()>0) {
                        cursor.moveToFirst();
                        SLog.d(TAG, "getMultiDvbServiceQueryData() ch : "+ch+" Program Count :  "+cursor.getCount());
                        while (!cursor.isAfterLast()) {
                            AVProgram programEntity = cursorToComment(cursor);
                            queryDvbEntities.addAVProgram(programEntity);
                            cursor.moveToNext();
                        }
                    }
                    if(cursor != null) cursor.close();
                    queryAllEntities.add(queryDvbEntities);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        SLog.d(TAG ,"getMultiDvbServiceQueryData() end");
        return queryAllEntities;
    }

}
