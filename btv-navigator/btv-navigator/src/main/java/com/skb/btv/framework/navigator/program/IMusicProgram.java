
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.program;

import java.util.Date;


/**
 * IMusicProgram Interface
 *
 */

public interface IMusicProgram {
    String getImagePath();
    int getAudioPid();
    int getDuration();
    Date getEndDate();
    String getName();
    String getSinger();
    Date getStartDate();
}
