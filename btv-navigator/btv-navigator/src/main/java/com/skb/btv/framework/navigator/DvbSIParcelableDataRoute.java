
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.skb.btv.framework.navigator.config.PropertiesConfig;
import com.skb.btv.framework.navigator.product.ProductList;
import com.skb.btv.framework.navigator.program.AVProgram;
import com.skb.btv.framework.navigator.program.AVProgramList;
import com.skb.btv.framework.navigator.common.Utils;
import com.skb.btv.framework.navigator.config.DVBSIConfig;
import com.skb.btv.framework.navigator.database.DatabaseAVProgram;
import com.skb.btv.framework.navigator.database.DatabaseMusicProgram;
import com.skb.btv.framework.navigator.database.DatabaseProgramBase;
import com.skb.btv.framework.navigator.dvbservice.AVDvbService;
import com.skb.btv.framework.navigator.dvbservice.AVDvbServiceList;
import com.skb.btv.framework.navigator.dvbservice.MultiViewDvbService;
import com.skb.btv.framework.navigator.dvbservice.MultiViewDvbServiceList;
import com.skb.btv.framework.navigator.dvbservice.MultiViewLogicalCell;
import com.skb.btv.framework.navigator.dvbservice.MultiViewLogicalCellList;
import com.skb.btv.framework.navigator.dvbservice.MusicDvbService;
import com.skb.btv.framework.navigator.dvbservice.MusicDvbServiceList;
import com.skb.btv.framework.navigator.program.AVPrograms;
import com.skb.btv.framework.navigator.program.MusicProgram;
import com.skb.btv.framework.navigator.program.MusicProgramList;
import com.skb.btv.framework.navigator.log.SLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static com.skb.btv.framework.navigator.IBtvNavigator.DVBSI_EVENT_AUDIO_CHANNEL_UPDATED;
import static com.skb.btv.framework.navigator.IBtvNavigator.DVBSI_EVENT_AUDIO_EPG_PRESENT_FOLLOW_UPDATED;
import static com.skb.btv.framework.navigator.IBtvNavigator.DVBSI_EVENT_AUDIO_EPG_SCHEDULE_COMPLETED;
import static com.skb.btv.framework.navigator.IBtvNavigator.DVBSI_EVENT_AUDIO_EPG_SCHEDULE_UPDATED;
import static com.skb.btv.framework.navigator.IBtvNavigator.DVBSI_EVENT_AUDIO_CHANNEL_COMPLETED;
import static com.skb.btv.framework.navigator.IBtvNavigator.DVBSI_EVENT_CHANNEL_COMPLETED;
import static com.skb.btv.framework.navigator.IBtvNavigator.DVBSI_EVENT_CHANNEL_UPDATED;
import static com.skb.btv.framework.navigator.IBtvNavigator.DVBSI_EVENT_EPG_PRESENT_FOLLOW_UPDATED;
import static com.skb.btv.framework.navigator.IBtvNavigator.DVBSI_EVENT_MOSAIC_CHANNEL_COMPLETED;
import static com.skb.btv.framework.navigator.IBtvNavigator.DVBSI_EVENT_EPG_SCHEDULE_COMPLETED;
import static com.skb.btv.framework.navigator.IBtvNavigator.DVBSI_EVENT_EPG_SCHEDULE_UPDATED;
import static com.skb.btv.framework.navigator.IBtvNavigator.DVBSI_SID_MUSIC;
import static com.skb.btv.framework.navigator.IBtvNavigator.SYSPROP_STB_NAVI_DSMCC;


/**
 * DvbSIParcelableDataRoute Class
 *
 */

public class DvbSIParcelableDataRoute {

    private static final String TAG = "Navigator-DvbSiServiceProcess";

    private static final String TAG_SUB_INIT  = TAG+"[Init]";
    private static final String TAG_SUB_UPDATE  = TAG+"[Update]";
    private static final String TAG_SUB_CHANNEL  = TAG+"[DvbService]";
    private static final String TAG_SUB_PRODUCT  = TAG+"[Product]";
    private static final String TAG_SUB_PROGRAM = TAG+"[Program]";
    private static final String TAG_SUB_THREAD = TAG+"[Thread]";
    private static final String TAG_SUB_BROADCASTRECEIVER = TAG+"[BroadcastReceiver]";
    private static final String TAG_SUB_AUDIOPROGRAMLISTENER = TAG+"[AudioProgramListener]";
    private static final String TAG_SUB_DSMCCLISTENER = TAG+"[DsmccListener]";
    private static final String TAG_SUB_CONFIG = TAG+"[Config]";
    private static final String TAG_SUB_UTILS = TAG+"[Utils]";

    /****************************************************************************************
     *
     *  Member variables
     *
     **************************************************************************************/

    private static final int SID_NUMBER_ALL = -1; // program list database query condition

    // region channel&program&config file path
    private static final String CHANNEL_FILE = "/data/btv_home/dvbsi/channel.xml";
    private static final String MUSIC_CHANNEL_FILE = "/data/btv_home/dvbsi/music_channel.xml";
    private static final String MULTIVIEW_CHANNEL_FILE = "/data/btv_home/dvbsi/multiview_channel.xml";
    private static final String PRODUCT_FILE = "/data/btv_home/dvbsi/product.xml";
    private static final String BULK_PROGRAM_FILE = "/data/btv_home/dvbsi/bulkprogramall.xml";
    private static final String AUDIO_PROGRAM_FILE = "/data/btv_home/dvbsi/audioprogramall.xml";
    private static final String DVBSI_CONFIG_FILE = "/data/btv_home/config/dvbsi.conf";
    private static final String DVBSI_CONFIG_FILE_TEMP = "/data/btv_home/config/dvbsi.conf.tmp";
    // endregion

    private static final String REGION_CODE_DEFAULT = "1|41|61|231|200|261";

    // region parceable data variable
    private static DvbSIRouteParcelableData<AVDvbServiceList> channelListData;
    private DvbSIRouteParcelableData<MusicDvbServiceList> musicChannelListData;
    private DvbSIRouteParcelableData<ProductList> productListData;
    private DvbSIRouteParcelableData<AVProgramList> programListData;
    private DvbSIRouteParcelableData<AVProgramList> musicProgramListData;
    private DvbSIRouteParcelableData<MultiViewDvbServiceList> multiViewChannelListData;
    private DvbSIRouteParcelableData<DVBSIConfig> dvbsiConfigData;
    // endregion

    // region default variable definition
    private Context mContext;

    /**
     * dsmcc event callback listener list
     */
    private static ArrayList<OnDsmccListener> mDsmccCallbacks = new ArrayList<>();
    /**
     * Current showing sid number list
     */
    private ArrayList<Integer> mRunningSidNumber = new ArrayList<Integer>();

    private ArrayList<Integer> mCheckSidNumber = new ArrayList<Integer>();

    // region update check variable
    /**
     * Update Check variable
     */
    private boolean needUpdateChannel = true;
    private boolean needUpdateMusicChannel = true;
    private boolean needUpdateProduct = true;
    private boolean needUpdateProgram = true;
    private boolean needUpdateMusicProgram = true;
    private boolean needUpdateMultiViewChannel = true;
    private boolean needUpdateMusicProgramList = true;
    // endregion
    /**
     * config  region default codes value
     */
    String mRegionCode = REGION_CODE_DEFAULT;
    /**
     * config seqId
     */
    int mSegId;

    /**
     * time format for Log.
     */
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH/mm/ss");
    // endregion

    // region music program relative variable
    /**
     * music sid number
     */
    private int mMusicSidNum =  -1;
    /**
     * Music channel program list information
     */
    private MusicProgramList  musicChannelProgramList;
    private String                      musicChannelProgramFile = "/data/btv_home/tmp/navi_dsmcc/7589/playlist_2901.txt";
    private boolean                    isUpdateMusicChannelProgramList = true;
    private int                         musicChannelProgramPid = 0;
    private ArrayList<Integer>           musicChannelProgramPidList = new ArrayList<>();
    // endregion

    // region database for AVProgram
    private boolean isUseLocalDatabase = false;  // usage of database on java
    private boolean isUsedDatabaseProgram = true;
    private boolean isPartialSearching = false;
    private boolean isRealTimeCheckDB = false;
    private AVProgramList databaseAVProgramList;
    private AVProgramList databaseMusicProgramList;
    private static final String BULK_PROGRAM_DB_FILE = "/data/btv_home/dvbsi/programall.db";
    private static final String AUDIO_PROGRAM_DB_FILE = "/data/btv_home/dvbsi/audioprogramall.db";
    // endregion

    String lastChInfoStr = ""; // it is set again when get music program updated event

    private final Object lockDbAvProgramList = new Object();

    /****************************************************************************************
     *
     *  Update-related Thread(Channel&Program)
     *
     **************************************************************************************/

    // region Thread Definition
    /**
     * updateParcelableData Thread loop variable
     */
    private boolean bUpdateThreadRunning;
    /**
     * Update Pracelable Data(Channel, Product, Program) Check Thread
     */
    private Thread updateParcelableData = new Thread(new Runnable() {
        @Override
        public void run() {

            bUpdateThreadRunning = true;

            while (bUpdateThreadRunning){
                if(needUpdateChannel){
                    updateChannelList();
                }

                if(needUpdateMusicChannel){
                    updateMusicChannelList();
                }

                if(needUpdateMultiViewChannel){
                    updateMultiViewChannelList();
                }

                if(needUpdateProduct){
                    updateProductList();
                }

                if(needUpdateProgram){
                    updateProgramList();
                }

                if(needUpdateMusicProgram){
                    updateMusicProgramList();
                }

                if(needUpdateMusicProgramList){
                    updateMusicChannelProgram();
                }

//                long startTime = System.currentTimeMillis();
//                SLog.d(TAG_SUB_THREAD, "update() time : "+ simpleDateFormat.format(startTime));

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    });

    /**
     *  Change Program(AV) Check Thread
     */

    /**
     * checkProgramChange Thread loop variable
     */
    private boolean bCheckProgramChange;
    // checking duration
    private final long checkingDuration = 1000;
    private Thread checkProgramChange = new Thread(new Runnable() {
        @Override
        public void run() {
            ArrayList<Integer> sidNumList = new ArrayList<Integer>();
            bCheckProgramChange = true;
            while(bCheckProgramChange) {
                String sidList = "";
                Calendar calendar = Calendar.getInstance();
                Date now = calendar.getTime();
                long nowTime = now.getTime() / checkingDuration;
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                Date tomorrow = calendar.getTime();
                sidNumList = getRunningSidNumber();

                if(mContext != null && sidNumList != null && sidNumList.isEmpty() == false) {
                    //channelNumList = mRunningChannelNumber;
                    for(Integer sidNumber : sidNumList){
                        AVProgramList programList = getCurrentProgramCheck(sidNumber, now, tomorrow);

                        if(programList != null){
                            ArrayList<AVPrograms> programs = programList.getAVProgramsList();
                            if(programs != null && programs.isEmpty() == false){
                                if(programs.get(0).getAVProgramList() != null && programs.get(0).getAVProgramList().isEmpty() == false ) {
                                    long startTime = programs.get(0).getAVProgramList().get(0).getStartDate().getTime() / checkingDuration;
                                    long endTime = programs.get(0).getAVProgramList().get(0).getEndDate().getTime() / checkingDuration;
                                    if(startTime == nowTime || endTime == nowTime){
                                        SLog.d(TAG_SUB_THREAD, "check get program info add sid:" + sidNumber +" startTime: "
                                                + startTime  + " now :"  + nowTime + " endTime: " + endTime + " rating:" + programs.get(0).getAVProgramList().get(0).getRating());
                                        sidList += "" + sidNumber + "|";
                                    }
                                }
                            }
                        }
                    }

                    if(sidList.isEmpty() == false) {
                        SLog.i(TAG_SUB_THREAD, "checkProgramChange - program info sidList: " + sidList);
                        sendProgramChangedEvent(sidList);
                    }
                }

                try {
                    Thread.sleep(900);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    });

    //endregion

    /****************************************************************************************
     *
     *  Update-related listener
     *
     **************************************************************************************/

    private BtvJniInterface.DvbsiUpdateListener dvbsiUpdateListener = new BtvJniInterface.DvbsiUpdateListener() {
        @Override
        public void onUpdate(int updateType) {
            SLog.sd(TAG_SUB_BROADCASTRECEIVER, "dvbsiUpdateListener - onUpdate : " + updateType);
            switch (updateType) {
                case DVBSI_EVENT_CHANNEL_COMPLETED:
                case DVBSI_EVENT_CHANNEL_UPDATED:
                    needUpdateChannel = true;
                    needUpdateProduct = true;
                    needUpdateMusicChannel = true;
                    SLog.sd(TAG_SUB_BROADCASTRECEIVER, "dvbsiUpdateListener - onUpdate : DVBSI_EVENT_CHANNEL_COMPLETED");
                    break;
                case DVBSI_EVENT_EPG_SCHEDULE_COMPLETED:
                case DVBSI_EVENT_EPG_SCHEDULE_UPDATED:
                    needUpdateProgram = true;
                    SLog.sd(TAG_SUB_BROADCASTRECEIVER, "dvbsiUpdateListener - onUpdate : DVBSI_EVENT_EPG_SCHEDULE_COMPLETED");
                    break;
                case DVBSI_EVENT_AUDIO_EPG_SCHEDULE_COMPLETED:
                case DVBSI_EVENT_AUDIO_EPG_SCHEDULE_UPDATED:
                    needUpdateMusicProgram = true;
                    setLastChannelInfo();
                    SLog.sd(TAG_SUB_BROADCASTRECEIVER, "dvbsiUpdateListener - onUpdate : DVBSI_EVENT_AUDIO_EPG_SCHEDULE_COMPLETED");
                    break;
                case DVBSI_EVENT_MOSAIC_CHANNEL_COMPLETED:
                    needUpdateMultiViewChannel = true;
                    SLog.sd(TAG_SUB_BROADCASTRECEIVER, "dvbsiUpdateListener - onUpdate : DVBSI_EVENT_MOSAIC_CHANNEL_COMPLETED");
                    break;
                case DVBSI_EVENT_AUDIO_CHANNEL_COMPLETED:
                case DVBSI_EVENT_AUDIO_CHANNEL_UPDATED:
                    needUpdateMusicChannel = true;
                    SLog.sd(TAG_SUB_BROADCASTRECEIVER, "dvbsiUpdateListener - onUpdate : DVBSI_EVENT_AUDIO_CHANNEL_COMPLETED");
                    break;
                case DVBSI_EVENT_EPG_PRESENT_FOLLOW_UPDATED:
                case DVBSI_EVENT_AUDIO_EPG_PRESENT_FOLLOW_UPDATED:
                    SLog.e(TAG_SUB_BROADCASTRECEIVER, "Not used event");
                    break;
            }
        }
    };

    long musicProgramLastModTime;
    boolean checkAudioProgramFileUpdated() {
        if (TextUtils.isEmpty(musicChannelProgramFile)) {
            SLog.e(TAG_SUB_AUDIOPROGRAMLISTENER, " error - empty file name");
            return false;
        }
        File file = new File(musicChannelProgramFile);
        if (false == file.exists()) {
            SLog.e(TAG_SUB_AUDIOPROGRAMLISTENER, " error - not exist file : " + musicChannelProgramFile);
            return false;
        }
        SLog.d(TAG_SUB_AUDIOPROGRAMLISTENER, "last modified time : "
                + musicProgramLastModTime + ", new time : " + file.lastModified());
        if (musicProgramLastModTime == 0 || musicProgramLastModTime < file.lastModified()) {
            musicProgramLastModTime = file.lastModified();
            return true;
        }
        return false;
    }

    /****************************************************************************************
     *
     *  Listener API update music program
     *
     **************************************************************************************/

    // region music program callback listerner
    /**
     * Music Program update call back
     */
    private BtvJniInterface btvJniInterface;
    private BtvJniInterface.AudioProgramUpdateListener audioProgramlistener = new BtvJniInterface.AudioProgramUpdateListener(){
        @Override
        public void onUpdate(String filepath, int updateType, int audioPid) {
            SLog.d(TAG_SUB_AUDIOPROGRAMLISTENER, "onUpdate() start updateType : "+updateType+" audioPid : "+audioPid);
            musicChannelProgramFile = filepath;
            SLog.d(TAG_SUB_AUDIOPROGRAMLISTENER, "onUpdate() programFile : " + musicChannelProgramFile);
            isUpdateMusicChannelProgramList = updateType == 0x90;
            SLog.d(TAG_SUB_AUDIOPROGRAMLISTENER, "onUpdate() isUpdate : " + isUpdateMusicChannelProgramList );
            if (checkAudioProgramFileUpdated()) {
                SLog.d(TAG_SUB_AUDIOPROGRAMLISTENER, " audio program file updated - set isUpdate true");
                isUpdateMusicChannelProgramList = true;
            }
            musicChannelProgramPid = audioPid;
            needUpdateMusicProgramList = true;
            if(!isUpdateMusicChannelProgramList){
                boolean isMusicChProgramExist = false;
                for(Integer integer: musicChannelProgramPidList){
                    if(integer.intValue() == musicChannelProgramPid) {
                        isMusicChProgramExist = true;
                        break;
                    }
                }
                if(!isMusicChProgramExist) isUpdateMusicChannelProgramList = true;
            }
            SLog.d(TAG_SUB_AUDIOPROGRAMLISTENER, "onUpdate() end ");
        }
    };

    /****************************************************************************************
     *
     *  Listener API dsmcc event listener
     *
     **************************************************************************************/

    // region dsmcc event callback listerner
    /**
     * dsmcc event call back
     */
    private BtvJniInterface.OnDsmccCallbackListener dsmccEventlistener = new BtvJniInterface.OnDsmccCallbackListener(){
        @Override
        public void onDsmccEventUpdated(String path, SignalEvent event) {
            SLog.d(TAG_SUB_DSMCCLISTENER, "onDsmccEventUpdated() start path : "+path);
            synchronized(mDsmccCallbacks) {
                if (mDsmccCallbacks != null) {
                    for (OnDsmccListener listener : mDsmccCallbacks) {
                        if (listener != null) {
                            listener.onDsmccEvent(path, event);
                        }
                    }
                }
            }
            SLog.d(TAG_SUB_DSMCCLISTENER, "onDsmccEventUpdated() end ");
        }
    };
    // endregion

    // region for AndroidTest
    String previousRegionCode;
    // endregion

    private Handler mHandler;
    private Runnable mRunSetCurrCh = new Runnable() {
        @Override
        public void run() {
            SLog.i(TAG_SUB_CHANNEL, "call setLastChannelInfo on mRunSetCurrCh runnable");
            setLastChannelInfo();
            if (mHandler != null) {
                mHandler.postDelayed(mRunSetCurrCh, 5 * 60 * 1000);
            }
        }
    };

    /****************************************************************************************
     *
     *   APIs of Constructor & Init
     *
     **************************************************************************************/

    // region Constructor & init
    /**
     * Constructor
     * @param context
     */
    public DvbSIParcelableDataRoute(Context context){
        SLog.d(TAG_SUB_INIT, "DvbSIParcelableDataRoute() start");

        mContext = context;
        btvJniInterface = BtvJniInterface.getInstance(mContext);
        if(btvJniInterface != null) {
            btvJniInterface.dvbsi_start();
            btvJniInterface.setAudioProgramUpdateListener(audioProgramlistener);
            btvJniInterface.setDvbsiUpdateListener(dvbsiUpdateListener);
            btvJniInterface.setOnDsmccCallbackListener(dsmccEventlistener);
        }

        initializingRouteData();

        checkProgramChange.start();

        mHandler = new Handler(Looper.getMainLooper());
        SLog.d(TAG_SUB_INIT, "DvbSIParcelableDataRoute() end");
    }

    /**
     * Initialize btv parcelabledata(channel, program etc) object
     */
    private void initializingRouteData(){
        try {
            FileInputStream is = new FileInputStream(new File(CHANNEL_FILE));
            SLog.d(TAG_SUB_INIT, "FileInputStream() : " + is);
        } catch (FileNotFoundException e) {
            SLog.e(TAG_SUB_INIT, "FileNotFoundException() e : " + e);
        }
        SLog.d(TAG_SUB_INIT, "initializingRouteData() start ");
        channelListData = new DvbSIRouteParcelableData<>(CHANNEL_FILE
                , new AVDvbServiceList(false));
        musicChannelListData = new DvbSIRouteParcelableData<>(MUSIC_CHANNEL_FILE
                , new MusicDvbServiceList(false));
        productListData = new DvbSIRouteParcelableData<>(PRODUCT_FILE
                , new ProductList(false));
        if(!isUsedDatabaseProgram) {
            programListData = new DvbSIRouteParcelableData<>(BULK_PROGRAM_FILE
                    , new AVProgramList(false));
            musicProgramListData = new DvbSIRouteParcelableData<>(AUDIO_PROGRAM_FILE
                    , new AVProgramList(false));
        }
        multiViewChannelListData = new DvbSIRouteParcelableData<>(MULTIVIEW_CHANNEL_FILE
                , new MultiViewDvbServiceList(false));
        dvbsiConfigData = new DvbSIRouteParcelableData<>(DVBSI_CONFIG_FILE
                , new DVBSIConfig(false));
        musicChannelProgramList = new MusicProgramList();

        SLog.d(TAG_SUB_INIT, "initializingRouteData() end ");
    }

    // endregion

    /****************************************************************************************
     *
     *   APIs of Start&End  for DvbSi serivce operation
     *
     **************************************************************************************/

    // region Route Process APIs
    /**
     * Read each parcelabledata and start update check thread
     * @return boolean
     */
    public boolean startRouteProcess(){
        SLog.d(TAG_SUB_INIT, "startRouteProcess() start ");

        if(updateParcelableData != null) {
            try {
                updateParcelableData.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        channelListData.updateDataRead();
        musicChannelListData.updateDataRead();
        multiViewChannelListData.updateDataRead();

        if(isUsedDatabaseProgram) {
            AVProgramList tempList = loadDatabaseMusicProgramList();
            if (tempList != null) {
                databaseMusicProgramList = tempList;
            }
        } else {
            musicProgramListData.updateDataRead();
        }

        addMusicChannelToChannelList();
        updateMusicChannelProgramList();

        updateParcelableData.start();
        SLog.d(TAG_SUB_INIT, "startRouteProcess() end ");
        return true;
    }

    /**
     * Stop route process
     * @return boolean
     */
    public boolean stopRouteProcess(){
        SLog.d(TAG_SUB_INIT, "stopRouteProcess() start");
        bUpdateThreadRunning = false;
        if(updateParcelableData != null && updateParcelableData.isAlive()) {
            try {
                updateParcelableData.join();
                updateParcelableData.interrupt();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        bCheckProgramChange = false;
        if(checkProgramChange != null && checkProgramChange.isAlive()){
            try {
                checkProgramChange.join();
                checkProgramChange.interrupt();
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }

        if(btvJniInterface != null) {
            btvJniInterface.dvbsi_release();
        }

        if (mHandler != null) {
            mHandler.removeCallbacks(mRunSetCurrCh);
            mHandler = null;
        }

        SLog.d(TAG_SUB_INIT, "stopRouteProcess() end ");
        return true;
    }
    // endregion

    /****************************************************************************************
     *
     *   APIs of Update parcelable data  after intent was received
     *
     **************************************************************************************/

    // region update APIs
    /**
     * Update AV Channel List + Music Channel (311) sid (105)
     */
    private void updateChannelList() {
        needUpdateChannel = false;
        SLog.d(TAG_SUB_UPDATE, "updateChannelList() start");
        if(channelListData.updateDataRead()) {
            addMusicChannelToChannelList();
            JSONObject object = new JSONObject();
            try {
                object.put(IBtvNavigator.DVBSI_JSONOBJECT_INFO, IBtvNavigator.DVBSI_EXTRADATA_UPDATE_CHANNEL_INFO);
                object.put(IBtvNavigator.DVBSI_JSONOBJECT_DVBSERVICEVERSION, getChannelLastListVersion());
            }catch(JSONException e){
                e.printStackTrace();
            }
            sendRegisterCallback(IBtvNavigator.DVBSI_TYPE_UPDATE_CHANNEL, object.toString());
        }
        SLog.d(TAG_SUB_UPDATE, "updateChannelList() end ");
    }

    /**
     * Update Music Channel List
     */
    private void updateMusicChannelList(){
        needUpdateMusicChannel = false;
        SLog.d(TAG_SUB_UPDATE, "updateMusicChannelList() start");
        if(musicChannelListData.updateDataRead()){
            addMusicChannelToChannelList();
            JSONObject object = new JSONObject();
            try {
                object.put(IBtvNavigator.DVBSI_JSONOBJECT_INFO, IBtvNavigator.DVBSI_EXTRADATA_UPDATE_MUSIC_CHANNEL_INFO);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            sendRegisterCallback(IBtvNavigator.DVBSI_TYPE_UPDATE_MUSIC_CHANNEL, object.toString());
        }
        SLog.d(TAG_SUB_UPDATE, "updateMusicChannelList() end ");
    }

    /**
     * Add music channel to channel
     */
    private void addMusicChannelToChannelList(){
        // add audio channel information to channel list
        SLog.d(TAG_SUB_UTILS, "addMusicChannelToChannelList() start ");
        if(channelListData == null){
            SLog.e(TAG_SUB_UTILS, "addMusicChannelToChannelList() channelList null");
            return;
        }
        AVDvbServiceList channelList = channelListData.beginDataRead();
        if(channelList != null) {
            ArrayList<AVDvbService> chList = channelList.getList();
            if (chList != null && !chList.isEmpty()) {
                boolean existAudioChannel = false;

                MusicDvbServiceList musicChannelList = musicChannelListData.beginDataRead();
                if (musicChannelList != null) {
                    ArrayList<MusicDvbService> list = musicChannelList.getList();
                    if (list != null && !list.isEmpty()) {
                        for (AVDvbService chInfo : chList) {
                            if (list.get(0).getCh() == chInfo.getCh()) {
                                existAudioChannel = true;
                                break;
                            }
                        }

                        if (!existAudioChannel) {
                            int chTypeMusic = list.get(0).getChannelType();
                            AVDvbService musicChannel = new AVDvbService();
                            musicChannel.setName("B tv 뮤직");
                            musicChannel.setCh(list.get(0).getCh());
                            if(list.get(0).getChannelUri() != null && list.get(0).getChannelUri().length() > 0) {
                                musicChannel.setChannelUri(list.get(0).getChannelUri());
                            }
                            else {
                                musicChannel.setChannelUri("skbiptv://239.192.70.161:49220?ch=" + list.get(0).getCh() + "&ci=0&cp=0&pp=2901&ap=2901&ac=129&op=7589");
                            }
                            musicChannel.setChannelType(chTypeMusic);//);IAVDvbService.AVDVBSERVICE_CHANNEL_TYPE_MUSIC_CHANNEL);
                            musicChannel.setCategory(0);
                            musicChannel.setChannelCallsign("");
                            musicChannel.setSid(DVBSI_SID_MUSIC);
                            mMusicSidNum = musicChannel.getSid();
                            musicChannel.setImagePath("epg_105.png|pip_105_1.png|slogan_105.png");
                            musicChannel.setGenre(32);  //genre set default to 32
                            musicChannel.setLocalAreaCode(0);
                            musicChannel.setOcPid(list.get(0).getOpid());
                            musicChannel.setPay(0);
                            musicChannel.setPpid(0);
                            musicChannel.setProduct(null);
                            musicChannel.setRating(0);
                            musicChannel.setRunningStatus(0);
                            musicChannel.setSample(0);
                            musicChannel.setSeqId(0);
                            // TODO musicChannel.setVChannelBImagePath("");
                            musicChannel.setRes(0);
                            musicChannel.setSubChannel(0);
                            musicChannel.setVirtualVodUri(null);
                            channelList.addAVChannel(musicChannel);
                        }
                    }
                }
                musicChannelListData.endDataRead();
            }
        }
        channelListData.endDataRead();
        SLog.d(TAG_SUB_UTILS, "addMusicChannelToChannelList()  end");
    }

    /**
     * Update MultiView Channel List
     */
    private void updateMultiViewChannelList(){
        needUpdateMultiViewChannel = false;
        SLog.d(TAG_SUB_UPDATE, "updateMultiViewChannelList() start ");
        if(multiViewChannelListData.updateDataRead()){
            JSONObject object = new JSONObject();
            try {
                object.put(IBtvNavigator.DVBSI_JSONOBJECT_INFO, IBtvNavigator.DVBSI_EXTRADATA_UPDATE_MULTIVIEW_CHANNEL_INFO);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            sendRegisterCallback(IBtvNavigator.DVBSI_TYPE_UPDATE_MULTI_CHANNELS, object.toString());
        }
        SLog.d(TAG_SUB_UPDATE, "updateMultiViewChannelList() end ");
    }

    /**
     * Update Product List
     */
    private void updateProductList(){
        needUpdateProduct = false;
        SLog.d(TAG_SUB_UPDATE, "updateProductList() start ");
        if(productListData.updateDataRead()){
            JSONObject object = new JSONObject();
            try {
                object.put(IBtvNavigator.DVBSI_JSONOBJECT_INFO, IBtvNavigator.DVBSI_EXTRADATA_CHANGE_PROGRAMS);
                object.put(IBtvNavigator.DVBSI_JSONOBJECT_CHANNEL_NUM, "");
            }catch(JSONException e){
                e.printStackTrace();
            }
            sendRegisterCallback(IBtvNavigator.DVBSI_TYPE_CHANGE_PROGRAMS, object.toString());
        }
        SLog.d(TAG_SUB_UPDATE, "updateProductList() end ");
    }

    /**
     * Update AV Program List
     */
    private void updateProgramList(){
        //needUpdateProgram = false;
        SLog.d(TAG_SUB_UPDATE, "IN| " + " start ");
        if(isUsedDatabaseProgram) {
            SLog.d(TAG_SUB_UPDATE, " call loadDatabaseAVProgramList");
            AVProgramList tempList = loadDatabaseAVProgramList();
            if (tempList != null && tempList.isReady()) {
                needUpdateProgram = false;
                synchronized (lockDbAvProgramList) {
                    databaseAVProgramList = tempList;
                }
                SLog.sd(TAG_SUB_UPDATE, "updateProgramList : update new AVProgramList");
                JSONObject object = new JSONObject();
                try {
                    object.put(IBtvNavigator.DVBSI_JSONOBJECT_INFO, IBtvNavigator.DVBSI_EXTRADATA_UPDATE_PROGRAM_INFO);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                sendRegisterCallback(IBtvNavigator.DVBSI_TYPE_UPDATE_PROGRAMS, object.toString());

                ArrayList<Integer> currSidList = new ArrayList<>();
                synchronized (mRunningSidNumber) {
                    SLog.sd(TAG_SUB_UPDATE, "updateProgramList - check updated, mRunningSidNumber : " + mRunningSidNumber);
                    for (Integer sid : mRunningSidNumber) {
                        currSidList.add(sid);
                    }
                }
                if (currSidList.size() > 0) {
                    synchronized (lockDbAvProgramList) {
                        String strSidList = getUpdatedProgramList(databaseAVProgramList, currSidList);
                        SLog.sd(TAG_SUB_UPDATE, " strSidList : "
                                + strSidList);
                        if (strSidList != null && strSidList.length() > 0) {
                            sendProgramChangedEvent(strSidList);
                        }
                    }
                }
            } else {
                SLog.e(TAG_SUB_UPDATE, "no program list");
            }
        } else {
            needUpdateProgram = false;
            if(programListData.updateDataRead()){
                AVProgramList program = programListData.beginDataRead();
                if(program != null && program.getAVProgramsList() != null){
                    AVDvbServiceList channelList = getAVDvbServiceList();
                    for(AVPrograms gl : program.getAVProgramsList()){
                        //int chNum = gl.getSid();
                        int sid = gl.getSid();//getSid(chNum, channelList);
                        if(gl.getAVProgramList() != null) {
                            for(AVProgram pi : gl.getAVProgramList()) {
                                pi.setChannelId(sid);
                            }
                        }
                    }
                }
                programListData.endDataRead();

                JSONObject object = new JSONObject();
                try {
                    object.put(IBtvNavigator.DVBSI_JSONOBJECT_INFO, IBtvNavigator.DVBSI_EXTRADATA_UPDATE_PROGRAM_INFO);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                sendRegisterCallback(IBtvNavigator.DVBSI_TYPE_UPDATE_PROGRAMS, object.toString());
            }
        }
        SLog.d(TAG_SUB_UPDATE, "OUT| ");
    }

    /**
     * Update Music Program List
     */
    private void updateMusicProgramList(){
        //needUpdateMusicProgram = false;
        SLog.d(TAG_SUB_UPDATE, "start ");
        if(isUsedDatabaseProgram){
            databaseMusicProgramList = loadDatabaseMusicProgramList();
            if (databaseMusicProgramList != null && databaseMusicProgramList.isReady()) {
                SLog.sd(TAG_SUB_UPDATE, "updateMusicProgramList : updated new AVProgramList");
                needUpdateMusicProgram = false;
            }
        }else {
            musicProgramListData.updateDataRead();
        }
        SLog.d(TAG_SUB_UPDATE, " end");
    }

    /**
     * Update Music channel Program Information
     */
    private void updateMusicChannelProgram(){
        needUpdateMusicProgramList = false;
        SLog.d(TAG_SUB_UPDATE, "updateMusicChannelProgram() start ");
        updateMusicChannelProgramList();
        if(isUpdateMusicChannelProgramList) {
            JSONObject object = new JSONObject();
            try {
                object.put(IBtvNavigator.DVBSI_JSONOBJECT_INFO, IBtvNavigator.DVBSI_EXTRADATA_UPDATE_MUSIC_PROGRAM_INFO);
                object.put(IBtvNavigator.DVBSI_JSONOBJECT_AUDIOPID, musicChannelProgramPid);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            sendRegisterCallback(IBtvNavigator.DVBSI_TYPE_UPDATE_MUSIC_PROGRAMS, object.toString());
        }
        isUpdateMusicChannelProgramList = false;
        musicChannelProgramPid = 0;
        SLog.d(TAG_SUB_UPDATE, "updateMusicChannelProgram() end ");
    }

    /**
     * Update Music Channel ProgramList
     */
    private synchronized  void updateMusicChannelProgramList(){
        SLog.d(TAG_SUB_UPDATE, "UpdateMusicChannelProgramList() start ");

        if(isUpdateMusicChannelProgramList)
        {
            Boolean existMusicPid = false;

            for(Integer integer: musicChannelProgramPidList){
                if(integer.intValue() == musicChannelProgramPid) {
                    existMusicPid = true;
                    break;
                }
            }

            if(existMusicPid){
                MusicProgramList musicProgramList = getMusicProgramList(musicChannelProgramPid);
                musicChannelProgramList.getMusicProgramList().removeAll(musicChannelProgramList.getMusicProgramList());
                musicChannelProgramPidList.remove(Integer.valueOf(musicChannelProgramPid));
            }
        }
        else{
            SLog.d(TAG_SUB_UPDATE, "MusicChannelProgramList() not update");
            return;
        }

        if(new File(musicChannelProgramFile).exists() == false){
            SLog.d(TAG_SUB_UPDATE, "UpdateMusicChannelProgramList() no musicProgramFile " + musicChannelProgramFile );
            return;
        }
        try{
            File file = new File(musicChannelProgramFile);
            SLog.d(TAG_SUB_UPDATE, "UpdateMusicChannelProgramList() musicProgramFile = " + musicChannelProgramFile);
            FileReader filereader = new FileReader(file);
            BufferedReader bufReader = new BufferedReader(filereader);
            String line = "";
            int index = 0, lineCount = 0;
            while((line = bufReader.readLine()) != null){
                String[] valueArray = line.split("\\|");
                MusicProgram musicProgram =  new MusicProgram();
                index = 0;
                lineCount++;
                Date startTime = null;
                for (String value : valueArray ){
                    switch(index){
                        case 0: musicProgram.setAudioPid(Integer.parseInt(value));   break;
                        case 1: musicProgram.setName(value);  break;
                        case 2: {
                            startTime = Utils.toDate(value);
                            musicProgram.setStartDate(startTime);
                        } break;
                        case 3:{
                            if(value.contains(".")){
                                value = value.substring(0, value.indexOf("."));
                            }
                            musicProgram.setDuration(Integer.parseInt(value));
                            musicProgram.setEndDate(Utils.toEndDate(startTime, Integer.parseInt(value)));
                        }  break;
                        case 4:
                            musicProgram.setSinger(value == null ? "" : (value.equals("null") ? "" : value));  break;
                        case 5: musicProgram.setImagePath(value);  break;
                    }
                    index ++;
                }


                musicChannelProgramList.getMusicProgramList().add(musicProgram);
            }
            bufReader.close();

            musicChannelProgramPidList.add(Integer.valueOf(musicChannelProgramPid));
        }catch (FileNotFoundException e) {
            // TODO: handle exception
        }catch(IOException e){
            System.out.println(e);
        }
        SLog.d(TAG_SUB_UPDATE, "UpdateMusicChannelProgramList() end  ");

    }

    /**
     * Channel.xml file version
     * @return : time string
     */
    public String getChannelLastListVersion() {
        return channelListData.getLastVersion();
    }
    // endregion

    /**********************************************************************************************
     *
     * AV or Music Program Utils APIs
     *
     *********************************************************************************************/

    // region AV/Music Program Utils
    /**
     * Get AV Program List
     * @return  : AV Program list(AVProgramList)
     */
    private AVProgramList getAVProgramList(){
        SLog.d(TAG_SUB_PROGRAM, "IN| " + "getProgramList() start ");
        AVProgramList programList = new AVProgramList();
        if(isUsedDatabaseProgram){
            synchronized (lockDbAvProgramList) {
                programList = databaseAVProgramList;
            }
        } else {
            if (programListData != null) {
                programList = programListData.beginDataRead();
                programListData.endDataRead();
            }
        }
        SLog.d(TAG_SUB_PROGRAM, "OUT| " + "getProgramList() end ProgramList : "+programList);
        return programList;
    }

    /**
     * Not used
     * Get Music Program Information
     * @return : Program list (AVProgramList)
     */
    public AVProgramList getMusicProgramList(){
        SLog.d(TAG_SUB_PROGRAM, "IN| " + "getMusicProgramList() start");
        AVProgramList programList = new AVProgramList();
        if(musicProgramListData != null) {
            AVProgramList listCache = musicProgramListData.beginDataRead();
            programList.getAVProgramsList();
            musicProgramListData.endDataRead();
        }
        SLog.d(TAG_SUB_PROGRAM, "OUT| " + "getMusicProgramList() end  ProgramList : "+programList);
        return programList;
    }

    /**
     * Get Music Channel Program Information for music pid
     * @param musicPid : music pid(int)
     * @return : Music Program List()
     */
    public synchronized MusicProgramList getMusicProgramList(int musicPid) {
        SLog.d(TAG_SUB_PROGRAM, "IN| " + "getMusicProgramList(musicPid) start musicPid = " + musicPid);
        MusicProgramList tempMusicProgramList = new MusicProgramList();

        if(musicChannelProgramList == null) {
            return tempMusicProgramList;
        }

        if(musicChannelProgramList.getMusicProgramList() != null) {
            for(MusicProgram entity : musicChannelProgramList.getMusicProgramList()) {
                if(entity.getAudioPid()== musicPid) {
                    tempMusicProgramList.addMusicPrograms(entity);
                }
            }
        }

        SLog.d(TAG_SUB_PROGRAM, "OUT| " + "getMusicProgramList(musicPid) end tempMusicProgramList : " + tempMusicProgramList);
        return tempMusicProgramList;
    }


    /**
     * Get Current Time AV Program
     * @param sidNum : sid number
     * @param now : current time
     * @param tomorrow : next day time
     * @return : AV Program List(AVProgramList)
     */
    private AVProgramList getCurrentProgramCheck(int sidNum, Date now, Date tomorrow) {
        AVProgramList programList = new AVProgramList();

        programList = getMultiChannelProgramTime(Integer.toString(sidNum), now, tomorrow);
        if (null == programList || null == programList.getAVProgramsList()) {
            programList = getDefaultProgram(sidNum);
            SLog.d(TAG_SUB_PROGRAM, "getCurrentProgramCheck() default");
        }
        return programList;
    }

    /**
     * Get Current Time AV Program by Multi Sids
     * @param sidList : Multi sid list
     * @param startTime : Program start time
     * @param endTime : Program end time
     * @return : pAV Program List(AVProgramList)
     */
    private AVProgramList getMultiChannelProgramTime(String sidList, Date startTime, Date endTime) {
        AVProgramList programList = null;

        String[] sids = sidList.split("\\|");
        if(sids.length > 0) {
            programList = new AVProgramList();
            List<AVPrograms> programLists = null;
            if(isUsedDatabaseProgram){
                boolean needDBList = false;
                if(databaseAVProgramList != null) {
                    synchronized (lockDbAvProgramList) {
                        if (databaseAVProgramList.getAVProgramsList() != null) {
                            programLists = databaseAVProgramList.getAVProgramsList();
                            if (isPartialSearching) {
                                SLog.d("KGH", "isPartialSearching()");
                                if (!isExistAVPrograms(sidList)) {
                                    needDBList = true;
                                    SLog.d(TAG_SUB_PROGRAM, "getMultiChannelProgramTime() not exist av programs - get database list");
                                }
                            }
                        }
                    }
                } else {
                    if (isRealTimeCheckDB) {
                        if (isUseLocalDatabase) {
                            if (isExistDBFile(BULK_PROGRAM_DB_FILE)) {
                                needDBList = true;
                            }
                        } else {
                            needDBList = true;
                        }
                    }
                }
                if (needDBList) {
                    List<AVPrograms> tempLists = loadDatabaseAVProgramList(sidList);
                    if (tempLists != null) {
                        programLists = tempLists;
                    }
                }
            }else {
                if (programListData != null) {
                    AVProgramList listCache = programListData.beginDataRead();
                    if (listCache != null) {
                        if (listCache.getAVProgramsList() != null) {
                            programLists = listCache.getAVProgramsList();
                        }
                    }
                }
            }

            if(programLists != null){
                for(int i = 0; i< sids.length; i++) {
                    try {
                        int sid = Integer.parseInt(sids[i]);
                        AVPrograms findProd = null;
                        for (AVPrograms proList : programLists) {
                            if (sid == mMusicSidNum) {
                                AVProgramList program1 = null;
                                if (isUsedDatabaseProgram) {
                                    boolean needDBList = false;
                                    if (databaseMusicProgramList != null && databaseMusicProgramList.getAVProgramsList() != null) {
                                        program1 = databaseMusicProgramList;
                                        if (isPartialSearching) {
                                            SLog.d("KGH", "isPartialSearching()");
                                            if (program1.getAVProgramsList().size() == 0) {
                                                needDBList = true;
                                            }
                                        }
                                    } else {
                                        SLog.d(TAG_SUB_PROGRAM, "no music program list - isRealTimeCheckDB : " + isRealTimeCheckDB);
                                        if (isRealTimeCheckDB) {
                                            needDBList = true;
                                        }
                                    }
                                    if (needDBList) {
                                        AVProgramList tempList = loadDatabaseMusicProgramList();
                                        if (tempList != null) {
                                            program1 = tempList;
                                        }
                                    }
                                } else {
                                    program1 = musicProgramListData.beginDataRead();
                                }

                                if(program1 == null){
                                    //needUpdateMusicProgram = true; // do not retry to update music program list
                                } else if (program1.getAVProgramsList() != null && program1.getAVProgramsList().isEmpty() == false) {
                                    if (startTime == null && endTime == null) {
                                        findProd = program1.getAVProgramsList().get(0);
                                    } else {
                                        AVPrograms musicProgramList = program1.getAVProgramsList().get(0);
                                        AVPrograms list = new AVPrograms(musicProgramList.getSid());
                                        if (musicProgramList.getAVProgramList() != null) {
                                            Calendar sCal = Calendar.getInstance();
                                            sCal.setTime(startTime);
                                            sCal.set(Calendar.MILLISECOND, 0);
                                            Calendar eCal = Calendar.getInstance();
                                            eCal.setTime(endTime);
                                            eCal.set(Calendar.MILLISECOND, 0);
                                            long reqST = sCal.getTimeInMillis();
                                            long reqET = eCal.getTimeInMillis();
                                            for (AVProgram program : musicProgramList.getAVProgramList()) {
                                                long proST = program.getStartDate().getTime();
                                                long proET = program.getEndDate().getTime();
                                                    /*if (proST < reqET && reqST < proET) {
                                                        list.addAVProgram(program);
                                                    }*/
                                                if ((reqST <= proST && reqET >= proET)
                                                        || (reqST > proST && reqET >= proET && reqST < proET)
                                                        || (reqST <= proST && reqET < proET && reqET > proST)
                                                        || (reqST > proST && reqET < proET)
                                                ) {
                                                    list.addAVProgram(program);
                                                }
                                            }
                                        }
                                        if (list.getAVProgramList() != null) {
                                            int programSize = list.getAVProgramList().size();
                                            if (programSize > 0) {
                                                if (!isUsedDatabaseProgram) {
                                                    Collections.sort(list.getAVProgramList(), new Comparator<AVProgram>() {
                                                        public int compare(AVProgram o1, AVProgram o2) {
                                                            return (int) (o1.getStartDate().getTime() - o2.getStartDate().getTime());
                                                        }
                                                    });
                                                }
                                                findProd = list;
                                            }
                                        }
                                    }
                                }
                                if (!isUsedDatabaseProgram) {
                                    musicProgramListData.endDataRead();
                                }
                                break;
                            } else if (proList.getSid() == sid) {
                                if (startTime == null && endTime == null) {
                                    findProd = proList;
                                } else {
                                    AVPrograms list = new AVPrograms(proList.getSid());
                                    if (proList.getAVProgramList() != null) {
                                        Calendar sCal = Calendar.getInstance();
                                        sCal.setTime(startTime);
                                        sCal.set(Calendar.MILLISECOND, 0);

                                        Calendar eCal = Calendar.getInstance();
                                        eCal.setTime(endTime);
                                        eCal.set(Calendar.MILLISECOND, 0);

                                        long reqST = sCal.getTimeInMillis();
                                        long reqET = eCal.getTimeInMillis();

                                        for (AVProgram program : proList.getAVProgramList()) {
                                            long proST = program.getStartDate().getTime();
                                            long proET = program.getEndDate().getTime();
                                            if ((reqST <= proST && reqET >= proET)
                                                    || (reqST > proST && reqET >= proET && reqST < proET)
                                                    || (reqST <= proST && reqET < proET && reqET > proST)
                                                    || (reqST > proST && reqET < proET)
                                            ) {
                                                list.addAVProgram(program);
                                            }
                                        }
                                    }

                                    if (list.getAVProgramList() != null) {
                                        int programSize = list.getAVProgramList().size();
                                        if (programSize > 0) {
                                            if (!isUsedDatabaseProgram) {
                                                Collections.sort(list.getAVProgramList(), new Comparator<AVProgram>() {
                                                    public int compare(AVProgram o1, AVProgram o2) {
                                                        return (int) (o1.getStartDate().getTime() - o2.getStartDate().getTime());
                                                    }
                                                });
                                            }
                                            findProd = list;
                                        }
                                    }
                                }
                                break;
                            }

                        }

                        if (findProd != null) {
                            programList.addAVPrograms(findProd);
                        } else {
                            SLog.i(TAG_SUB_PROGRAM, " findProd null - sid : " + sid);
                            programList.addAVPrograms(new AVPrograms(sid));
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            }
            if(!isUsedDatabaseProgram) {
                programListData.endDataRead();
            }
        }else{
            programList = getDefaultProgram(-1);
        }
        return programList;
    }

    /**
     * Make default program Object
     * @param sid : sid number
     * @return : AV Program List(AVProgramList)
     */
    private AVProgramList getDefaultProgram(int sid){
        SLog.d(TAG_SUB_PROGRAM, "getDefaultProgram() start sid = " + sid);
        AVProgramList programList = new AVProgramList();
        programList.addAVPrograms(new AVPrograms(sid));
        SLog.i(TAG_SUB_PROGRAM, "getDefaultProgram() end");
        return programList;
    }

    /**
     * check exist db file
     * @param path : file path
     * @return : boolean
     */
    private boolean isExistDBFile(String path){
        File dbFile = new File(path);
        if(dbFile.exists()){
            return true;
        }else {
            return false;
        }
    }

    /**
     * Is exist ch program in global program list
     * @param sidList : sid list
     * @return : boolean
     */
    private boolean isExistAVPrograms(String sidList){
        boolean bRet = false;
        String[] sids = sidList.split("\\|");
        List<AVPrograms> checkList = null;
        checkList = databaseAVProgramList.getAVProgramsList();
        int existCnt = 0;
        for(int i = 0; i< sids.length; i++) {
            try {
                int sid = Integer.parseInt(sids[i]);
                if (checkList != null) {
                    for (AVPrograms proList : checkList) {
                        if (sid == proList.getSid()) {
                            //bRet = true;
                            existCnt++;
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (existCnt == sids.length) {
            bRet = true;
        }
        SLog.d(TAG_SUB_PROGRAM, "isExistAVPrograms() "+bRet);
        return bRet;
    }

    /**
     * Is exist ch program in local program list
     * @param sid : sid number
     * @param list : local list
     * @return : boolean
     */
    private boolean isExistAVPrograms(int sid, AVProgramList list){
        boolean bRet = false;
        if(list != null && list.getAVProgramsList() != null){
            List<AVPrograms> checkList = list.getAVProgramsList();
            for (AVPrograms proList : checkList) {
                if (sid == proList.getSid()) {
                    bRet = true;
                    break;
                }
            }
        }
        SLog.d(TAG_SUB_PROGRAM, "isExistAVPrograms() "+bRet);
        return bRet;
    }
    // endregion

    /*********************************************************************************************
     *
     *  Send event API to callback listener registered by UI Apps
     *
     *******************************************************************************************/

    // region send callback(OnDvbSICallback)
    /**
     * Send event in registered callbacks
     * @param dvbsiType : callback event type
     * @param extraData : extra data
     */
    private void sendRegisterCallback(int dvbsiType, String extraData){
        SLog.d(TAG_SUB_UTILS, "sendRegisterCallback() start dvbsiType = " + dvbsiType +" extraData : "+extraData);
        DvbSIService.sendOnDvbSICallback(dvbsiType, extraData);
        SLog.d(TAG_SUB_UTILS, "sendRegisterCallback() end");
    }

    private String getUpdatedProgramList(AVProgramList programList, ArrayList<Integer> sidNumList) {
        SLog.i(TAG_SUB_PROGRAM, " sid list : " + sidNumList.toString());
        if (null == programList || !programList.isReady() || null == sidNumList || sidNumList.size() <= 0) {
            return null;
        }
        StringBuilder strSidList = new StringBuilder();
        for (Integer sid : sidNumList) {
            for (AVPrograms programs : programList.getAVProgramsList()) {
                if (sid == programs.getSid()) {
                    strSidList.append(sid.toString()).append("|");
                    break;
                }
            }
        }
        return strSidList.toString();
    }

    private void sendProgramChangedEvent(String strSidList) {
        if (null == strSidList || strSidList.length() <= 0) {
            return;
        }
        String strSid[] = strSidList.split("\\|");
        if (strSid.length <= 0) {
            return;
        }
        SLog.d(TAG_SUB_CHANNEL, "IN| " + "sid count : " + strSid.length
                + ", list : " + strSidList);
        if(strSid.length == 1) {
            JSONObject object = new JSONObject();
            SLog.d(TAG_SUB_CHANNEL, " sid : " + strSid[0]);
            try {
                object.put(IBtvNavigator.DVBSI_JSONOBJECT_INFO, IBtvNavigator.DVBSI_EXTRADATA_CHANGE_PROGRAM);
                object.put(IBtvNavigator.DVBSI_JSONOBJECT_SID_NUM, Integer.getInteger(strSid[0]));
            }catch(JSONException e){
                e.printStackTrace();
            }
            sendRegisterCallback(IBtvNavigator.DVBSI_TYPE_CHANGE_PROGRAM, object.toString());
        }
        JSONObject object1 = new JSONObject();
        try {
            object1.put(IBtvNavigator.DVBSI_JSONOBJECT_INFO, IBtvNavigator.DVBSI_EXTRADATA_CHANGE_PROGRAMS);
            object1.put(IBtvNavigator.DVBSI_JSONOBJECT_SID_NUM, strSidList);
        }catch(JSONException e){
            e.printStackTrace();
        }
        sendRegisterCallback(IBtvNavigator.DVBSI_TYPE_CHANGE_PROGRAMS, object1.toString());
        SLog.d(TAG_SUB_CHANNEL, "OUT| " + "sid count : " + strSid.length
                + ", list : " + strSidList);
    }

    /**
     * send Register callback for AndroidTest
     */
    public void sendTestRegisterCompleted(String sidList){
        JSONObject object = new JSONObject();
        try {
            object.put(IBtvNavigator.DVBSI_JSONOBJECT_INFO, "");
            object.put(IBtvNavigator.DVBSI_JSONOBJECT_SID_NUM, sidList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendRegisterCallback(IBtvNavigator.DVBSI_TYPE_ANDROID_TEST, object.toString());
    }
    // endregion

    /**********************************************************************************************
     *
     *                               DvSIServie  Implementaion APIs
     *
     *********************************************************************************************/
    // region DvbSIServie  Implementaion APIs
    /**
     * Get AV Channel List Information
     * @return : AV Channel List(AVDvbServiceChList)
     */
    public AVDvbServiceList getAVDvbServiceList() {
        SLog.d(TAG_SUB_CHANNEL, "IN| " + "getAVDvbServiceList() start");
        AVDvbServiceList mChannelList = new AVDvbServiceList();
        if(channelListData != null){
            mChannelList = channelListData.beginDataRead();
            if(mChannelList != null && mChannelList.getList() != null){
                Collections.sort(mChannelList.getList(), new Comparator<AVDvbService>() {
                    public int compare(AVDvbService o1, AVDvbService o2) {
                        return o1.getCh() - o2.getCh();
                    }
                });
            }
            channelListData.endDataRead();
        }

        // Get Count Log
        if(mChannelList != null && mChannelList.getList() != null && mChannelList.getList().size() > 0){
            SLog.d(TAG_SUB_CHANNEL, "getAVDvbServiceList() channel size : " + mChannelList.getList().size());
        }
        SLog.d(TAG_SUB_CHANNEL, "OUT| " + "getAVDvbServiceList() end channel : "+ mChannelList);
        return mChannelList;
    }

    /**
     * Get Music Channel List Information
     * @return : Music Channel List(AVDvbServiceChList)
     */

    public MusicDvbServiceList getMusicDvbServiceList() {
        SLog.d(TAG_SUB_CHANNEL, "IN| " + "getMusicDvbServiceList() start");

        MusicDvbServiceList mMusicChannelList = new MusicDvbServiceList();
        if(musicChannelListData != null){
            mMusicChannelList = musicChannelListData.beginDataRead();
            if(mMusicChannelList != null && mMusicChannelList.getList() != null){
                for(MusicDvbService mc : mMusicChannelList.getList()){
                    if(mc.getTitle().contains("amp;")){
                        mc.setTitle(mc.getTitle().replace("amp;", ""));
                    }
                }
            }
            musicChannelListData.endDataRead();
        }
        // Get Count Log
        if(mMusicChannelList != null && mMusicChannelList.getList() != null && mMusicChannelList.getList().size() > 0){
            SLog.d(TAG_SUB_CHANNEL, "getMusicDvbServiceList() musicchannel size : " + mMusicChannelList.getList().size());
        }

        SLog.d(TAG_SUB_CHANNEL, "OUT| " + "getMusicDvbServiceList() end musicchannel : "+ mMusicChannelList);
        return mMusicChannelList;
    }

    /**
     * Get MultiView Channel List Information
     * @return : MultiView Channel List(MultiViewDvbServiceChList)
     */
    public MultiViewDvbServiceList getMultiViewDvbServiceList(){
        SLog.d(TAG_SUB_CHANNEL, "IN| " + "getMultiViewDvbServiceList() start ");

        MultiViewDvbServiceList mMultiViewChannelList = new MultiViewDvbServiceList();
        if(mMultiViewChannelList != null){
            mMultiViewChannelList = multiViewChannelListData.beginDataRead();
            if(mMultiViewChannelList != null && mMultiViewChannelList.getList() != null){
                if(!mRegionCode.equals(REGION_CODE_DEFAULT)) {
                    ArrayList<MultiViewDvbService> multiViewChannel = mMultiViewChannelList.getList();
                    if(multiViewChannel != null && multiViewChannel.isEmpty() == false){
                        for(MultiViewDvbService entity : multiViewChannel) {
                            if (entity.getGroupName().equals("지상파")) {

                                // add echwang : 2020.08.11 : S +-
                                ArrayList<MultiViewLogicalCell>  multiViewLogicalList = entity.getMultiViewLogicalCellInfoList();
                                ArrayList<MultiViewLogicalCell> removeList = new ArrayList<>();
                                SLog.d(TAG_SUB_CHANNEL, "getMultiViewDvbServiceList() 111 multiViewLogicalList().size() : " + multiViewLogicalList.size());
                                if(multiViewLogicalList != null && multiViewLogicalList.isEmpty() == false ) {
                                    for (MultiViewLogicalCell multiViewLogical : multiViewLogicalList) {
                                        SLog.d(TAG_SUB_CHANNEL, "getMultiViewDvbServiceList() multiViewLogical().getChannelNumber() : "
                                                + multiViewLogical.getChannelNumber() + ", service id : " + multiViewLogical.getServiceID());
                                        if (multiViewLogical.getChannelNumber() != getDvbServiceChNum(multiViewLogical.getServiceID())) {
                                            removeList.add(multiViewLogical);
                                            SLog.d(TAG_SUB_CHANNEL, "getMultiViewDvbServiceList() multiViewLogical.getChannelNumber() to remove == " + multiViewLogical.getChannelNumber());
                                        }
                                    }

                                    SLog.d(TAG_SUB_CHANNEL, "getMultiViewDvbServiceList() removeList().size() : " + removeList.size());

                                    for (MultiViewLogicalCell removeEntity : removeList) {
                                        SLog.d(TAG_SUB_CHANNEL, "getMultiViewDvbServiceList() removeEntity().getChannelNumber() : "
                                                + removeEntity.getChannelNumber() + ", service id : " + removeEntity.getServiceID());
                                        entity.getMultiViewLogicalCellInfoList().remove(removeEntity);
                                    }

                                    //mMultiViewChannelList.getList().remove(entity);
                                    // add echwang : 2020.08.11 : E +-

                                    SLog.d(TAG_SUB_CHANNEL, "getMultiViewDvbServiceList() getMultiViewLogicalCellInfoList().size() : " + entity.getMultiViewLogicalCellInfoList().size());
                                }

                                break;
                            }
                        }
                    }
                }
            }
            multiViewChannelListData.endDataRead();
        }

        // Get Count Log
        if(mMultiViewChannelList != null && mMultiViewChannelList.getList() != null && mMultiViewChannelList.getList().size() > 0){
            SLog.d(TAG_SUB_CHANNEL, "getMultiViewDvbServiceList() MultiViewChannelList size : " + mMultiViewChannelList.getList().size());
        }

        SLog.d(TAG_SUB_CHANNEL, "OUT| " + "getMultiViewDvbServiceList() end MultiViewChannelList : "+ mMultiViewChannelList);
        return mMultiViewChannelList;
    }

    /**
     * Get channel number for sid
     * @param sid : sid
     * @return : channel number
     */
    private int getDvbServiceChNum(int sid) {
        SLog.d(TAG_SUB_CHANNEL, "IN| " + "getDvbServiceSid() start sid : " + sid);
        AVDvbServiceList channelList = getAVDvbServiceList();
        if(channelList != null && channelList.getList() != null){
            for(AVDvbService entity : channelList.getList()){
                if(entity.getSid() == sid){
                    SLog.d(TAG_SUB_CHANNEL, "OUT| " + "getDvbServiceSid() end ch : "+entity.getCh());
                    return entity.getCh();
                }
            }
        }
        SLog.d(TAG_SUB_CHANNEL, "OUT| " + "getDvbServiceSid() end ch : -1");
        return -1;
    }

    /**
     * Get MultiViewLogicalCell List of Channel
     * @param groupId : group id
     * @return : Multiview LogicalCell List(MultiViewLogicalCellList)
     */
    public MultiViewLogicalCellList getLogicalCellList(int groupId) {
        SLog.d(TAG_SUB_CHANNEL, "IN| " + "getLogicalCellList() start groupId : "+groupId);
        MultiViewLogicalCellList logicalCelllist = new MultiViewLogicalCellList();
        if(multiViewChannelListData != null && logicalCelllist != null){
            MultiViewDvbServiceList multiViewChannelList = multiViewChannelListData.beginDataRead();
            if(multiViewChannelList != null && multiViewChannelList.getList() != null) {
                ArrayList<MultiViewLogicalCell> logicalCells = new ArrayList<>();
                for(MultiViewDvbService entity : multiViewChannelList.getList()) {
                    if(entity.getGroupID() == groupId || groupId == -1) {
                        logicalCells.addAll(entity.getMultiViewLogicalCellInfoList());
                    }
                }
                logicalCelllist.setLogicalCellList(logicalCells);
            }
            multiViewChannelListData.endDataRead();
        }

        // Get Count Log
        if(logicalCelllist != null && logicalCelllist.getMultiViewLogicalCellList() != null && logicalCelllist.getMultiViewLogicalCellList().size() > 0){
            SLog.d(TAG_SUB_CHANNEL, "getLogicalCellList() LogicalCellList size : " + logicalCelllist.getMultiViewLogicalCellList().size());
        }

        SLog.d(TAG_SUB_CHANNEL, "OUT| " + "getLogicalCellList() end LogicalCellList : "+logicalCelllist);
        return logicalCelllist;
    }

    /**
     * Make URI Selcted MultiViewLogicalCell
     * @param logicalcells : Selcted MultiViewLogicalCell
     * @return : URI (String)
     */
    public String makeMultiViewUri(List<MultiViewLogicalCell> logicalcells) {
        SLog.d(TAG_SUB_CHANNEL, "IN| " + "makeMultiViewUri() start logicalcells : "+logicalcells);
        if(logicalcells != null && logicalcells.size() > 0) {
            String entiryUri = "skbsmv://reserved/?";
            for(int i = 0; i < logicalcells.size(); i++) {
                if(i > 0) {
                    entiryUri += "&";
                }
                entiryUri += "view" + (i + 1) + "=" + logicalcells.get(i).getMultiViewChannelUri();
            }
            entiryUri += "&viewmode=0";
            SLog.d(TAG_SUB_CHANNEL, "OUT| " + "makeMultiViewUri() end entiryUri : "+entiryUri);
            return entiryUri;
        }
        SLog.d(TAG_SUB_CHANNEL, "OUT| " + "makeMultiViewUri() end entiryUri : null");
        return null;
    }

    /**
     * set current showing sid list
     * @param sidList : current showing sid list
     */
    public void setCurrentMultiChannel(String sidList) {
        SLog.d(TAG_SUB_CHANNEL, "IN| " + "setCurrentMultiChannel() start sid(s) : "+ sidList);
        String[] sids = sidList.split("\\|");
        if(sids.length == 0) {
            return;
        }

        synchronized (mRunningSidNumber) {
            if (mRunningSidNumber.isEmpty() == false) {
                mRunningSidNumber.clear();
            }

            try {
                for (String sidNum : sids) {
                    SLog.d(TAG_SUB_CHANNEL, "setCurrentMultiChannel() sid:" + sidNum);
                    mRunningSidNumber.add(Integer.parseInt(sidNum));
                }
                sendTestRegisterCompleted(sidList);  // For AndroidTest
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        if (mHandler != null) {
            SLog.i(TAG_SUB_CHANNEL, "remove setCurrentChannelInfo runnable");
            mHandler.removeCallbacks(mRunSetCurrCh);
        }
        SLog.d(TAG_SUB_CHANNEL, "OUT| " + "setCurrentMultiChannel() end ");
    }

    /**
     * set current showing channel tune info.
     * @param tuneInfo : current showing channel tuneInfo
     */
    public void setCurrentChannelInfo(IptvTuneInfo tuneInfo) {
        SLog.d(TAG_SUB_CHANNEL, "IN| " + "setCurrentChannelInfo() start channel");

        int defaultAudioOnOff = 0;
        if(tuneInfo.getIsPip()) defaultAudioOnOff = tuneInfo.getAuioEnable();

        synchronized (lastChInfoStr) {
            lastChInfoStr = String.format("%s;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;",
                    tuneInfo.GetURL(),
                    tuneInfo.GetPort(),
                    tuneInfo.GetVideoPID(),
                    tuneInfo.GetAudioPID(),
                    tuneInfo.GetPCRPID(),
                    tuneInfo.GetVideoStream(),
                    tuneInfo.GetAudioStream(),
                    tuneInfo.GetCAPID(),
                    tuneInfo.GetCASystemID(),
                    tuneInfo.GetChNum(),
                    tuneInfo.GetResolution(),
                    defaultAudioOnOff,
                    tuneInfo.getLastFrame()
            );

            if (btvJniInterface != null) {
                btvJniInterface.setCurrentChannel(lastChInfoStr);
            }
        }

        SLog.i(TAG_SUB_CHANNEL, "is music channel : "
                + (mMusicSidNum != -1 && tuneInfo.GetChNum() == getDvbServiceChNum(mMusicSidNum))
                + ", music sid : " + mMusicSidNum
                + ", ch : " + ((mMusicSidNum != -1) ? getDvbServiceChNum(mMusicSidNum) : -1));
        if (mMusicSidNum != -1 && tuneInfo.GetChNum() == getDvbServiceChNum(mMusicSidNum)) {
            if (mHandler != null) {
                mHandler.removeCallbacks(mRunSetCurrCh);
                SLog.i(TAG_SUB_CHANNEL, "postDelayed setCurrentChannelInfo runnable");
                mHandler.postDelayed(mRunSetCurrCh, 5 * 60 * 1000);
            }
        }

        SLog.d(TAG_SUB_CHANNEL, "OUT| " + "setCurrentChannelInfo() end ");
    }

    /**
     * set last channel tune info.
     */
    private void setLastChannelInfo() {
        SLog.d(TAG_SUB_CHANNEL, "IN| " + " start tuneInfo : " + lastChInfoStr);
        synchronized (lastChInfoStr) {
            if (btvJniInterface != null && false == TextUtils.isEmpty(lastChInfoStr)) {
                btvJniInterface.setCurrentChannel(lastChInfoStr);
            }
        }
        SLog.d(TAG_SUB_CHANNEL, "OUT| " + "setLastChannelInfo() end ");
    }

    /**
     * get current showing sid list
     * @return ArrayList<Integer>
    **/
    private ArrayList<Integer> getRunningSidNumber(){
        mCheckSidNumber.removeAll(mCheckSidNumber);

        synchronized (mRunningSidNumber){

            for(Integer sid : mRunningSidNumber){
                mCheckSidNumber.add(sid);
            }
        }
        return mCheckSidNumber;
    }

    /**
     * Get Product List Inforamtion
     * @return : Product List(ProductList)
     */
    public ProductList getProductList() {
        SLog.d(TAG_SUB_PRODUCT, "IN| " + "getProductList() start ");
        ProductList productList = new ProductList();
        if(productListData != null){
            productList = productListData.beginDataRead();
            productListData.endDataRead();
        }

        // Get Count Log
        if(productList != null && productList.getProductList() != null && productList.getProductList().size() > 0){
            SLog.d(TAG_SUB_PRODUCT, "getProductList() ProductList size : " + productList.getProductList().size());
        }

        SLog.d(TAG_SUB_PRODUCT, "OUT| " + "getProductList() end ProductList : "+productList);
        return productList;
    }

    SimpleDateFormat programDateFormat = new SimpleDateFormat("yyyy:MM:dd HH/mm/ss");
    /**
     * Get AV Program information of channel number
     * @param sid : sid no
     * @return : AV Program List(AVProgramList)
     */
    public AVProgramList getProgramListOfDvbService(int sid){
        SLog.i(TAG_SUB_PROGRAM, "IN| " + "getProgramListOfDvbService() start sid : "+sid);
        AVProgramList channelProgram = new AVProgramList();
        AVProgramList program = getAVProgramList();
        boolean needDBList = false;
        if(program != null && program.getAVProgramsList() != null){
            for(AVPrograms entity : program.getAVProgramsList()){
                if(entity.getSid() == sid){
                    channelProgram.addAVPrograms(entity);
                }
            }
            if(isPartialSearching){
                needDBList = channelProgram.getAVProgramsList() == null || channelProgram.getAVProgramsList().size() == 0;
                SLog.d("KGH", "isPartialSearching()");
            }
        } else {
            if(isRealTimeCheckDB && isUsedDatabaseProgram) {
                needDBList = true;
            }
            SLog.i(TAG_SUB_PROGRAM, "getProgramListOfDvbService() getAVProgramList not exist - isRealTimeCheckDB : "
                    + isRealTimeCheckDB + ", isUsedDatabaseProgram : " + isUsedDatabaseProgram);
        }
        if (needDBList) {
            AVProgramList tempLists = loadDatabaseAVProgramList(sid);
            if (tempLists != null) {
                channelProgram = tempLists;
            }
        }
        // Get Count Log
        int total = 0;
        if(channelProgram != null && channelProgram.getAVProgramsList() != null && channelProgram.getAVProgramsList().size() > 0){
            for( AVPrograms programs : channelProgram.getAVProgramsList()) {
                if(programs.getAVProgramList() != null) {
                    SLog.i(TAG_SUB_PROGRAM, "getProgramListOfDvbService() sid : " + programs.getSid() + ", program size : " + programs.getAVProgramList().size());
                    total += programs.getAVProgramList().size();
                    if (!isUsedDatabaseProgram) {
                        Collections.sort(programs.getAVProgramList(), new Comparator<AVProgram>() {
                            public int compare(AVProgram o1, AVProgram o2) {
                                return (int) (o1.getStartDate().getTime() - o2.getStartDate().getTime());
                            }
                        });
                    }
                }
            }
        }
        SLog.i(TAG_SUB_PROGRAM, "OUT| " + "getProgramListOfDvbService() end count : " + total + ", ProgramList ->"+channelProgram);
        return channelProgram;
    }

    /**
     * Get Program Lists of given channels
     * @param channelList : Multi channels
     * @return : AV Program List(AVProgramList)
     */
    public AVProgramList getMultiDvbServiceProgramList(String channelList){
        SLog.d(TAG_SUB_PROGRAM, "IN| " + "getMultiDvbServiceProgramList() start channelList : "+channelList);
        return getMultiChannelProgramTime(channelList, null, null);
    }

    /**
     * Get Music Program information of audioPid
     * @param audioPid : Music Pid
     * @return : Music Program List(MusicProgramList)
     */
    public synchronized MusicProgramList getMusicDvbServiceProgramList(int audioPid){
        SLog.d(TAG_SUB_PROGRAM, "IN| " + "getMusicDvbServiceProgramList() start pid -> "
                + audioPid + ", musicChannelProgramList : "
                + (musicChannelProgramList != null
                    ? ((musicChannelProgramList.getMusicProgramList() != null) ? "exist" : " get music list - null") : "null"));
        MusicProgramList  resultList = new MusicProgramList();
        if(musicChannelProgramList != null && musicChannelProgramList.getMusicProgramList() != null){
            SLog.d(TAG_SUB_PROGRAM, "getMusicDvbServiceProgramList() entry count : " + musicChannelProgramList.getMusicProgramList().size());
            for(MusicProgram entity : musicChannelProgramList.getMusicProgramList()) {
                if(entity.getAudioPid()== audioPid) {
                    resultList.addMusicPrograms(entity);
                }
            }
        }

        // Get Count Log
        if(resultList != null && resultList.getMusicProgramList() != null && resultList.getMusicProgramList().size() > 0){
            SLog.d(TAG_SUB_PROGRAM, "getMusicDvbServiceProgramList() music program size : " + resultList.getMusicProgramList().size());
        }

        SLog.d(TAG_SUB_PROGRAM, "OUT| " + "getMusicDvbServiceProgramList() end MusicChannelProgramList -> "+resultList);
        return resultList;
    }

    /**
     * Get current program
     * @param sid : current showing service id
     */

    public AVProgramList getCurrentAVProgram(int sid) {
        SLog.d(TAG_SUB_PROGRAM, "IN| " + " Start sid : "+ sid);
        AVProgramList currentProgram = null;// = new AVProgramList();

        boolean isDefault = true;
        if(isUsedDatabaseProgram){
            Calendar calendar = Calendar.getInstance();
            Date now = calendar.getTime();

            calendar.add(Calendar.DAY_OF_YEAR, 1);
            Date tomorrow = calendar.getTime();

            currentProgram = getMultiChannelProgramTime(Integer.toString(sid), now, tomorrow);
            isDefault = false;
        }else {
            if (programListData != null) {
                AVProgramList listCache = programListData.beginDataRead();
                if (listCache != null) {
                    if (listCache.getAVProgramsList() != null) {
                        Calendar calendar = Calendar.getInstance();
                        Date now = calendar.getTime();

                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                        Date tomorrow = calendar.getTime();

                        currentProgram = getMultiChannelProgramTime(Integer.toString(sid), now, tomorrow);
                        isDefault = false;
                    }
                }
                programListData.endDataRead();
            }
        }

        if(isDefault){
            currentProgram = getDefaultProgram(sid);
            SLog.d(TAG_SUB_PROGRAM, "getCurrentAVProgram() default - sid : " + sid);
        }

        SLog.d(TAG_SUB_PROGRAM, "OUT| " + "getCurrentAVProgram() end - size = "
                + ((currentProgram != null && currentProgram.getAVProgramsList() != null)
                    ? currentProgram.getAVProgramsList().size() : -1));
        return currentProgram;
    }

    /**
     * Get program at a special time
     * @param sidList : sid list string
     * @param startTime : start time
     * @param endTime : end time
     * @return : AVProgramList
     */
    public AVProgramList getMultiDvbServiceAVProgramListByTime(String sidList, Date startTime, Date endTime) {
        SLog.d(TAG_SUB_PROGRAM, "IN| " + " start sidList : " + sidList + ", startTime = " + startTime + " endTime = " + endTime);
        return getMultiChannelProgramTime(sidList, startTime, endTime);
    }

    /**
     * Get AV program list by count from current time
     * @param sid : service id
     * @param requestCount : request count
     * @return : AVProgramList
     */
    public AVProgramList getPartialProgramListByCount(int sid, int requestCount) {
        SLog.d(TAG_SUB_PROGRAM, "IN| " + "getPartialProgramListByCount() Start sid : "+ sid +" count : "+requestCount);
        AVProgramList partialProgram = new AVProgramList();

        if(partialProgram != null) {
            Calendar calendar = Calendar.getInstance();
            Date now = calendar.getTime();

            calendar.add(Calendar.DAY_OF_YEAR, 1);
            Date tomorrow = calendar.getTime();

            AVProgramList currentTimeProgram = getMultiChannelProgramTime(Integer.toString(sid), now, tomorrow);
            if (currentTimeProgram != null && currentTimeProgram.getAVProgramsList() != null) {
                AVPrograms currentPrograms = currentTimeProgram.getAVProgramsList().get(0);
                if (currentPrograms != null) {
                    AVPrograms list = new AVPrograms(currentPrograms.getSid());
                    if (currentPrograms.getAVProgramList() != null) {
                        ArrayList<AVProgram> programs = currentPrograms.getAVProgramList();
                        int addCount = Math.min(programs.size(), requestCount);
                        for (int i = 0; i < addCount; i++) {
                            list.addAVProgram(programs.get(i));
                        }
                        if (list.getAVProgramList() != null) {
                            int programSize = list.getAVProgramList().size();
                            if (programSize > 0) {
                                if (!isUsedDatabaseProgram) {
                                    Collections.sort(list.getAVProgramList(), new Comparator<AVProgram>() {
                                        public int compare(AVProgram o1, AVProgram o2) {
                                            return (int) (o1.getStartDate().getTime() - o2.getStartDate().getTime());
                                        }
                                    });
                                }
                                SLog.d(TAG_SUB_PROGRAM, " sid = " + sid + ", programList Size = " + programSize);
                            }
                        }
                        partialProgram.addAVPrograms(list);
                    }
                }
            } else {
                partialProgram = getDefaultProgram(sid);
            }
        }

        SLog.d(TAG_SUB_PROGRAM, "OUT| " + "getPartialProgramListByCount() end ");
        return partialProgram;
    }


    /**
     * Update code in config.xml
     * @param regionCode : codes
     */
    public void setRegionCode(String regionCode) {
        SLog.sd(TAG_SUB_CONFIG, "IN| " + "setRegionCode() start regionCode -> "+regionCode);
        if(regionCode == null || TextUtils.isEmpty(regionCode) == true){
            SLog.se(TAG_SUB_CONFIG, "OUT| " + "regionCode empty");
            return;
        }

        File confFile = new File(DVBSI_CONFIG_FILE);
        if(confFile.exists() == false){
            SLog.se(TAG_SUB_CONFIG, "OUT| " + "not found dvbsi.conf");
            return;
        }

        dvbsiConfigData.updateDataRead();
        DVBSIConfig dvbsiConfig = dvbsiConfigData.beginDataRead();

        // Start For AndroidTest
        if(regionCode.equals("Reset")){
            SLog.d(TAG_SUB_CONFIG, "Reset codes : "+previousRegionCode);
            if(previousRegionCode != null && previousRegionCode.length() > 0){
                regionCode = previousRegionCode;
            }else {
                regionCode = mRegionCode;
            }
        }else {
            ArrayList<PropertiesConfig> configEntry = dvbsiConfig.getEntriesList();
            if (configEntry != null) {
                for (PropertiesConfig a : configEntry) {
                    if (a.getKey().equals("codes")) {
                        previousRegionCode = a.getValue();
                        SLog.d(TAG_SUB_CONFIG, "codes : " + previousRegionCode);
                    }
                }
            } else {
                SLog.se(TAG_SUB_CONFIG, "dvbsi.conf has no entry");
            }
        }
        // End For AndroidTest

        if(dvbsiConfig != null) {
            dvbsiConfig.setNewRegionCode(regionCode);
            String xml = dvbsiConfig.generateXML(true, false);
            if(TextUtils.isEmpty(xml) == false){
                FileOutputStream fileOutputStream = null;

                try {
                    fileOutputStream = new FileOutputStream(DVBSI_CONFIG_FILE_TEMP);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    dvbsiConfigData.endDataRead();
                    SLog.se(TAG_SUB_CONFIG, "OUT| " + "setRegionCode() FileNotFoundException ");
                    return;
                }

                OutputStreamWriter outputStreamWriter = null;
                Writer writer = null;
                try {
                    outputStreamWriter = new OutputStreamWriter(fileOutputStream, "utf-8");
                    writer = new BufferedWriter(outputStreamWriter);
                    writer.write(xml);
                    writer.close();

                    if(new File(DVBSI_CONFIG_FILE_TEMP).length() > 0) {
                        Utils.renameFile(DVBSI_CONFIG_FILE_TEMP, DVBSI_CONFIG_FILE);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if(writer!= null) {
                        try {
                            writer.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if(outputStreamWriter!=null) {
                        try {
                            outputStreamWriter.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if(fileOutputStream!=null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }else{
                SLog.se(TAG_SUB_CONFIG, "xml file null");
            }
        }
        dvbsiConfigData.endDataRead();
        mRegionCode = regionCode;

        if (btvJniInterface != null) {
            btvJniInterface.setRegionCode(regionCode);
        }
        SLog.sd(TAG_SUB_CONFIG, "OUT| " + "setRegionCode() end ");
    }

    /**
     * Update seqid in config.xml
     * @param segId : segid value
     */
    public void setSegId(int segId){
        SLog.sd(TAG_SUB_CONFIG, "IN| " + "setSegId() start seqId -> "+segId);

        if(new File(DVBSI_CONFIG_FILE).exists() == false){
            SLog.e(TAG_SUB_CONFIG, "OUT| " + "not found dvbsi.conf");
            return;
        }

        dvbsiConfigData.updateDataRead();
        DVBSIConfig dvbsiConfig = dvbsiConfigData.beginDataRead();

        if(dvbsiConfig != null) {
            dvbsiConfig.setSegId(segId);
            String xml = dvbsiConfig.generateXML(false, true);
            if(TextUtils.isEmpty(xml) == false){
                FileOutputStream fileOutputStream = null;

                try {
                    fileOutputStream = new FileOutputStream(DVBSI_CONFIG_FILE_TEMP);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    dvbsiConfigData.endDataRead();
                    SLog.se(TAG_SUB_CONFIG, "OUT| " + "setSegId() FileNotFoundException ");
                    return;
                }
                OutputStreamWriter outputStreamWriter = null;
                Writer writer = null;
                try {
                    outputStreamWriter = new OutputStreamWriter(fileOutputStream, "utf-8");
                    writer = new BufferedWriter(outputStreamWriter);
                    writer.write(xml);
                    writer.close();

                    if(new File(DVBSI_CONFIG_FILE_TEMP).length() > 0){
                        Utils.renameFile(DVBSI_CONFIG_FILE_TEMP, DVBSI_CONFIG_FILE);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if(writer!= null) {
                        try {
                            writer.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if(outputStreamWriter!=null) {
                        try {
                            outputStreamWriter.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    if(fileOutputStream!=null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }else{
                SLog.d(TAG_SUB_CONFIG, "xml file null");
            }
        }
        dvbsiConfigData.endDataRead();
        mSegId = segId;

        if (btvJniInterface != null) {
            btvJniInterface.setSegId(segId);
        }
        SLog.sd(TAG_SUB_CONFIG, "OUT| " + "setSegId() end ");
    }
    //endregion

    /**
     * get music program list
     * @return AVProgramList
     */
    public AVProgramList loadDatabaseMusicProgramList() {
        SLog.d(TAG_SUB_PROGRAM, "IN| loadDatabaseMusicProgramList() isUseLocalDatabase : " + isUseLocalDatabase);
        AVProgramList list = null;
        int count = 0;
        if (isUseLocalDatabase) {
            boolean isExistDB = isExistDBFile(AUDIO_PROGRAM_DB_FILE);
            DatabaseProgramBase database = new DatabaseMusicProgram(mContext);
            SLog.d(TAG_SUB_PROGRAM, "loadDatabaseMusicProgramList() isExistDB : " + isExistDB);
            if (isExistDB) {
                SLog.d(TAG_SUB_PROGRAM, "loadDatabaseMusicProgramList() start programinfo");
                if (database != null) {
                    database.open();
                    try {
                        list = database.getAllQueryData();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        database.close();
                    }
                }
            }
        } else {
            if (btvJniInterface != null) {
                SLog.i(TAG_SUB_PROGRAM, "loadDatabaseMusicProgramList() - call getAllMusicQueryData");
                list = btvJniInterface.getAllMusicQueryData();
                if (list != null && list.getAVProgramsList() != null) {
                    SLog.si(TAG_SUB_PROGRAM, "loadDatabaseMusicProgramList() - received new MusicQueryData");
                    for (AVPrograms programs : list.getAVProgramsList()) {
                        programs.setSid(DVBSI_SID_MUSIC);
                        if(programs.getAVProgramList() != null) {
                            count += programs.getAVProgramList().size();
                        }
                    }
                    SLog.d(TAG_SUB_PROGRAM, "loadDatabaseMusicProgramList() total count : "
                            + ((list != null && list.getAVProgramsList() != null) ? list.getAVProgramsList().size() : "null")
                            + ", total program count : " + count);
                } else {
                    SLog.e(TAG_SUB_UPDATE, "loadDatabaseMusicProgramList() - getAllMusicQueryData return : "
                            + list);
                }
            }
        }
        SLog.d(TAG_SUB_PROGRAM, "OUT| loadDatabaseMusicProgramList() total count : "
                + ((list != null && list.getAVProgramsList() != null) ? list.getAVProgramsList().size() : "null")
                + ", total program count : " + count);
        return list;
    }

    /**
     * get av program list
     * @return AVProgramList
     */
    public AVProgramList loadDatabaseAVProgramList() {
        return loadDatabaseAVProgramList(SID_NUMBER_ALL);
    }

    /**
     * get av program list for channel
     * @param sidNum channel
     * @return AVProgramList
     */
    public AVProgramList loadDatabaseAVProgramList(int sidNum) {
        AVProgramList list = null;
        int count = 0;
        SLog.i(TAG_SUB_PROGRAM, "IN| loadDatabaseAVProgramList() - sid (-1 == all) : " + sidNum);
        if (isUseLocalDatabase) {
            boolean isExistDB = isExistDBFile(BULK_PROGRAM_DB_FILE);
            DatabaseProgramBase database = new DatabaseAVProgram(mContext);
            if (isExistDB) {
                if (database != null) {
                    database.open();
                    try {
                        if (sidNum == SID_NUMBER_ALL) {
                            list = database.getAllQueryData();
                        } else {
                            list = ((DatabaseAVProgram)database).getDvbServiceQueryData(sidNum);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        database.close();
                    }
                }
            } else {
                SLog.e(TAG_SUB_PROGRAM, "loadDatabaseAVProgramList() error - not exist db file");
            }
        } else {
            if (btvJniInterface != null) {
                SLog.i(TAG_SUB_PROGRAM, "IN| loadDatabaseAVProgramList() call  : getAllAVQueryData[" + sidNum+"]");
                if (sidNum == SID_NUMBER_ALL) {
                    list = btvJniInterface.getAllAVQueryData();
                } else {
                    //list = btvJniInterface.getAVQueryDataForChannel(chNum);
                    list = btvJniInterface.getAVQueryDataForSid(sidNum);
                }
                if (list != null && list.getAVProgramsList() != null) {
                    SLog.si(TAG_SUB_PROGRAM, "IN| loadDatabaseAVProgramList() : received new AVProgramList");
                    for (AVPrograms programs : list.getAVProgramsList()) {
                        ArrayList<AVProgram> progList = programs.getAVProgramList();
                        if (progList != null) {
                            count += programs.getAVProgramList().size();
                        }
                    }
                    SLog.sd(TAG_SUB_PROGRAM, "loadDatabaseAVProgramList() sidNum : " + sidNum
                            + ", count : " + ((list != null && list.getAVProgramsList() != null)
                            ? list.getAVProgramsList().size() : "null")
                            + ", total program count : " + count);
                } else {
                    SLog.i(TAG_SUB_UPDATE, "loadDatabaseAVProgramList() - getAVQueryData sid : "
                            + sidNum + ", no program - return : " + list);
                }
            }
        }
        SLog.d(TAG_SUB_PROGRAM, "OUT| loadDatabaseAVProgramList() sidNum : " + sidNum
                + ", count : " + ((list != null && list.getAVProgramsList() != null)
                    ? list.getAVProgramsList().size() : "null")
                + ", total program count : " + count);
        return list;
    }

    /**
     * get av program list for sid list
     * @param sidList
     * @return List<AVPrograms>
     */
    public List<AVPrograms> loadDatabaseAVProgramList(String sidList) {
        List<AVPrograms> list = null;
        SLog.i(TAG_SUB_PROGRAM, "IN| loadDatabaseAVProgramList() - sidList : " + sidList);
        if (isUseLocalDatabase) {
            DatabaseAVProgram database = new DatabaseAVProgram(mContext);
            if (isExistDBFile(BULK_PROGRAM_DB_FILE)) {
                if (database != null) {
                    database.open();
                    try {
                        list = database.getMultiDvbServiceQueryData(sidList);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        database.close();
                    }
                }
            } else {
                SLog.e(TAG_SUB_PROGRAM, "loadDatabaseAVProgramList() error - not exist db file");
            }
        } else {
            if (btvJniInterface != null) {
                String[] sids = sidList.split("\\|");
                for(int i = 0; i< sids.length; i++) {
                    try {
                        int sid = Integer.parseInt(sids[i]);
                        AVProgramList pList = btvJniInterface.getAVQueryDataForSid(sid);
                        if (pList != null) {
                            if (list == null) {
                                list = pList.getAVProgramsList();
                            } else {
                                list.addAll(pList.getAVProgramsList());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (list != null) {
                } else {
                    SLog.e(TAG_SUB_UPDATE, "loadDatabaseAVProgramList() - getAVQueryData "
                            + sidList + ",  return : " + list);
                }
                /*AVProgramList pList = btvJniInterface.getMultiAVQueryData(sidList);
                if (pList != null) {
                    list = pList.getAVProgramsList();
                    if (list != null) {
                    } else {
                        SLog.e(TAG_SUB_UPDATE, "loadDatabaseAVProgramList() - getAVQueryData "
                                + sidList + ",  return : " + list);
                    }
                }*/
            }
        }
        SLog.d(TAG_SUB_PROGRAM, "OUT| loadDatabaseAVProgramList() chList : " + sidList
                + ", total count : " + ((list != null) ? list.size() : "null"));
        return list;
    }

    /**
     * Register Dsmcc Event callback listener
     * @param listener
     */
    public void setOnDsmccListener(OnDsmccListener listener) {
        synchronized(this) {
            if (listener != null) {
                mDsmccCallbacks.add(listener);
            }
        }
    }

    /**
     * Unregister Dsmcc Event callback listener
     * @param listener
     */
    public void clearOnDsmccListener(OnDsmccListener listener) {
        synchronized(this) {
            if (listener != null) {
                mDsmccCallbacks.remove(listener);
            }
        }
    }

}