
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;

import java.util.ArrayList;


/**
 * IAVDvbServiceList Interface
 *
 */

public interface IAVDvbServiceList {
    boolean isReady();
    ArrayList<AVDvbService> getList();
}
