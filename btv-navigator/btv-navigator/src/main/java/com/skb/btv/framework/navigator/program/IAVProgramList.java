
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.program;

import java.util.ArrayList;


/**
 * IAVProgramList Interface
 *
 */

public interface IAVProgramList {
    boolean isReady();
    ArrayList<AVPrograms> getAVProgramsList();
}
