
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.program;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * ProgramList Class
 *
 */

public class ProgramList implements Parcelable {

    public int requestType = 0;

    public ProgramList(){}

    protected ProgramList(Parcel in) {
        requestType = in.readInt();
    }


    public static final Creator<ProgramList> CREATOR = new Creator<ProgramList>() {
        /**
         * create service from parcel
         * @param in
         * @return ProgramList
         */
        @Override
        public ProgramList createFromParcel(Parcel in) {
            return new ProgramList(in);
        }

        /**
         * create service array
         * @param size
         * @return ProgramList[]
         */
        @Override
        public ProgramList[] newArray(int size) {
            return new ProgramList[size];
        }
    };

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(requestType);
    }

    /**
     * Get request type
     * @return int requestType
     */
    public int getRequestType(){
        return requestType;
    }

    /**
     * Set request type
     * @param type
     */
    public void setRequestType(int type){
        requestType = type;
    }
}
