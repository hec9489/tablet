package com.skb.btv.framework.navigator;

import android.text.TextUtils;

/**
 * 
 * @author musespy
 *
 */

class IptvTuneInfo
{
    private String mStrURL = "";
    private int mPort = 0;
    private int mVPID = 0;
    private int mAPID = 0;
    private int mPCRPID = 0;
    private int mVStream = 0; /* Video Codec type */
    private int mAStream = 0; /* Audio Codec type */
    private int mCASystemID = 0;
    private int mCAPID = 0;
	private int mChNum = 0; /* Channel Number */
	private int mRes = 0;          /* SD/HD */
    private int mLastFrame = 0;

    private boolean isMainAudio = true;
    private int mAPID1 = 0;
    private int mAStream1 = 0;    /* Audio Codec type */
    private int mAe = 0;
    private String mPip = null;
	
	public final static int     IPTV_TUNEINFO_RES_SD	= 0;
	public final static int     IPTV_TUNEINFO_RES_HD	= 1;

	public boolean isDualChannelAudio() {
	    return mAPID1 != 0 && mAStream1 != 0;
    }

    public void SetURL(String strURL)
    {
        mStrURL = strURL;
    }

    public String GetURL()
    {
        return mStrURL;
    }

    public void SetPort(int nPort)
    {
        mPort = nPort;
    }

    public int GetPort()
    {
        return mPort;
    }

    public void SetVideoPID(int nVideoPID)
    {
        mVPID = nVideoPID;
    }

    public int GetVideoPID()
    {
        return mVPID;
    }

    public void SetAudioPID(int nAudioPID) {
        mAPID = nAudioPID;
    }

    public void SetAudioPID1(int nAudioPID) {
        mAPID1 = nAudioPID;
    }

    public int GetAudioPID()
    {
        return mAPID;
    }

    public int GetAudioPID1() {
        return mAPID1;
    }

    public void SetPCRPID(int nPCRPID)
    {
        mPCRPID = nPCRPID;
    }

    public int GetPCRPID()
    {
        return mPCRPID;
    }

    public void SetVideoStream(int nVstream)
    {
        mVStream = nVstream;
    }

    public int GetVideoStream()
    {
        return mVStream;
    }

    public void SetAudioStream(int nAstream)
    {
        mAStream = nAstream;
    }

    public void SetAudioStream1(int nAstream)
    {
        mAStream1 = nAstream;
    }

    public int GetAudioStream()
    {
        return mAStream;
    }

    public int GetAudioStream1()
    {
        return mAStream1;
    }


    public void SetResolution(int nRes)
    {
        mRes = nRes;
    }

    public int GetResolution()
    {
        return mRes;
    }

    public void SetCASystemID(int nCASystemID)
    {
        mCASystemID = nCASystemID;
    }

    public int GetCASystemID()
    {
        return mCASystemID;
    }

    public void SetCAPID(int nCAPID)
    {
        mCAPID = nCAPID;
    }

    public int GetCAPID()
    {
        return mCAPID;
    }
	public void SetChNum(int nChNum)
    {
        mChNum= nChNum;
    }

    public int GetChNum()
    {
        return mChNum;
    }

    public boolean isMainAudio() {
        return isMainAudio;
    }

    public void setMainAudio(boolean mainAudio) {
        isMainAudio = mainAudio;
    }

    public int getLastFrame() {
        return mLastFrame;
    }

    public void setLastFrame(int mLastFrame) {
        this.mLastFrame = mLastFrame;
    }

    int getAuioEnable() { return mAe; }
    void setAuioEnable(int ae) { mAe = ae; }

    boolean getIsPip() { return TextUtils.equals(mPip, "pip"); }

    void setPip(String pip) { mPip = pip; }
}