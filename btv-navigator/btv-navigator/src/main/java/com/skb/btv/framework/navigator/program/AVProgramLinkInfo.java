
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.program;

import android.os.Parcel;
import android.os.Parcelable;

import com.skb.btv.framework.navigator.xmlUtils.XmlUtilDateConverter;
import com.tickaroo.tikxml.annotation.PropertyElement;
import com.tickaroo.tikxml.annotation.Xml;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * AVProgramLinkInfo Class
 *
 */

@SuppressWarnings("WeakerAccess")
@Xml(name = "link")
public class AVProgramLinkInfo implements IAVProgramLinkInfo, Parcelable {

    private static final String TAG =  "AVProgramLinkInfo";

    @PropertyElement(name = "ButtonImagePath")
    String buttonImagePath;

    @PropertyElement(name = "ButtonType")
    int buttonType;

    @PropertyElement(name = "CMenuValue")
    int cMenuValue;

    @PropertyElement(name = "DisplayStartTime", converter = XmlUtilDateConverter.class)
    Date displayStartTime;

    @PropertyElement(name = "DisplayEndTime", converter = XmlUtilDateConverter.class)
    Date displayEndTime;

    @PropertyElement(name = "LinkedServiceText")
    String linkedServiceText;

    @PropertyElement(name = "LinkedServiceType")
    int linkedServiceType;

    @PropertyElement(name = "VasItemId")
    String vasItemId;

    @PropertyElement(name = "VasPath")
    String vasPath;

    @PropertyElement(name = "VasServiceId")
    String vasServiceId;

    public AVProgramLinkInfo(){

    }

    protected AVProgramLinkInfo(Parcel in) {
        buttonImagePath = in.readString();
        buttonType = in.readInt();
        cMenuValue = in.readInt();
        displayStartTime = (Date) in.readSerializable();
        displayEndTime = (Date) in.readSerializable();
        linkedServiceText = in.readString();
        linkedServiceType = in.readInt();
        vasItemId = in.readString();
        vasPath = in.readString();
        vasServiceId = in.readString();
    }

    public static final Creator<AVProgramLinkInfo> CREATOR = new Creator<AVProgramLinkInfo>() {
        /**
         * create service from parcel
         * @param in
         * @return AVProgramLinkInfo
         */
        @Override
        public AVProgramLinkInfo createFromParcel(Parcel in) {
            return new AVProgramLinkInfo(in);
        }

        /**
         * create service array
         * @param size
         * @return AVProgramLinkInfo[]
         */
        @Override
        public AVProgramLinkInfo[] newArray(int size) {
            return new AVProgramLinkInfo[size];
        }
    };

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(buttonImagePath);
        dest.writeInt(buttonType);
        dest.writeInt(cMenuValue);
        dest.writeSerializable(displayStartTime);
        dest.writeSerializable(displayEndTime);
        dest.writeString(linkedServiceText);
        dest.writeInt(linkedServiceType);
        dest.writeString(vasItemId);
        dest.writeString(vasPath);
        dest.writeString(vasServiceId);
    }

    /**
     * Get button image path
     * @return  button image path
     */
    @Override
    public String getButtonImagePath() {
        return buttonImagePath;
    }

    /**
     * Set button image path
     * @param _buttonImagePath
     */
    public void setButtonImagePath(String _buttonImagePath){
        buttonImagePath = _buttonImagePath;
    }

    /**
     * Get button type
     * @return  button type
     */
    @Override
    public int getButtonType() {
        return buttonType;
    }

    /**
     * Set button type
     * @param _buttonType
     */
    public void setButtonType(int _buttonType) {
        buttonType = _buttonType;
    }

    /**
     * Get genre code for cid
     * @return  genre code
     */
    @Override
    public int getCMenuValue() {
        return cMenuValue;
    }

    /**
     * Set genre code for cid
     * @param _cMenuValue
     */
    public void setCMenuValue(int _cMenuValue) {
        cMenuValue = _cMenuValue;
    }

    /**
     * Get display start time
     * @return  Date display start time
     */
    @Override
    public Date getDisplayStartDate() {
        return displayStartTime;
    }

    /**
     * Set display start date
     * @param _displayStartTime
     */
    public void setDisplayStartDate(Date _displayStartTime){
        displayStartTime = _displayStartTime;
    }

    /**
     * Set display start date
     * @param _displayStartTime
     */
    public void setDisplayStartDateStr(String _displayStartTime){
        Date date;
        try {
            date = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREAN).parse(_displayStartTime);
        } catch (ParseException e) {
            date = new Date(0);
        }
        displayStartTime = date;
    }

    /**
     * Get display end date
     * @return  Date display end time
     */
    @Override
    public Date getDisplayEndDate() {
        return displayEndTime;
    }

    /**
     * Set display end date
     * @param _displayEndTime
     */
    public void setDisplayEndDate(Date _displayEndTime){
        displayEndTime = _displayEndTime;
    }

    /**
     * Set display end date
     * @param _displayEndTime
     */
    public void setDisplayEndDateStr(String _displayEndTime){
        Date date;
        try {
            date = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREAN).parse(_displayEndTime);
        } catch (ParseException e) {
            date = new Date(0);
        }
        displayEndTime = date;
    }

    /**
     * Get linked service text
     * @return  service id : vod=cid
     */
    @Override
    public String getLinkedServiceText() {
        return linkedServiceText;
    }

    /**
     * Set linked service text
     * @param _linkedServiceText
     */
    public void setLinkedServiceText(String _linkedServiceText){
        linkedServiceText = _linkedServiceText;
    }

    /**
     * Get linked service type
     * @return  service type : 1=vod
     */
    @Override
    public int getLinkedServiceType() {
        return linkedServiceType;
    }

    /**
     * Set linked service type
     * @param _linkedServiceType
     */
    public void setLinkedServiceType(int _linkedServiceType) {
        linkedServiceType = _linkedServiceType;
    }

    /**
     * Get vas item id
     * @return  vas item id
     */
    @Override
    public String getVasItemId() {
        return vasItemId;
    }

    /**
     * Set vas item id
     * @param _vasItemId
     */
    public void setVasItemId(String _vasItemId){
        vasItemId = _vasItemId;
    }

    /**
     * Get vas path
     * @return  vas path
     */
    @Override
    public String getVasPath() {
        return vasPath;
    }

    /**
     * Set vas path
     * @param _vasPath
     */
    public void setVasPath(String _vasPath){
        vasPath = _vasPath;
    }

    /**
     * Get vas service id
     * @return  vas service id
     */
    @Override
    public String getVasServiceId() {
        return vasServiceId;
    }

    /**
     * Set vas service id
     * @param _vasServiceId
     */
    public void setVasServiceId(String _vasServiceId) {
        vasServiceId = _vasServiceId;
    }
}
