
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator;


/**
 * OnDvbSIListener Interface
 *
 */

public interface OnDvbSIListener {
    void onDvbSIUpdated(int dvbsiType, String extraData);
}
