
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * DvbServiceList Class
 *
 */

public class DvbServiceList implements Parcelable {
    public int requestType = 0;

    public DvbServiceList(){}

    protected DvbServiceList(Parcel in) {
        requestType = in.readInt();
    }

    public static final Creator<DvbServiceList> CREATOR = new Creator<DvbServiceList>() {
        /**
         * create service from parcel
         * @param in
         * @return DvbServiceList
         */
        @Override
        public DvbServiceList createFromParcel(Parcel in) {
            return new DvbServiceList(in);
        }

        /**
         * create service array
         * @param size
         * @return DvbServiceList[]
         */
        @Override
        public DvbServiceList[] newArray(int size) {
            return new DvbServiceList[size];
        }
    };

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(requestType);
    }

    /**
     * get request type
     * @return int
     */
    public int getRequestType(){
        return requestType;
    }

    /**
     * set request type
     * @param type
     */
    public void setRequestType(int type){
        requestType = type;
    }

}
