
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.program;

import android.os.Parcel;
import android.os.Parcelable;

import com.skb.btv.framework.navigator.xmlUtils.XmlUtilDateConverter;
import com.tickaroo.tikxml.annotation.Attribute;
import com.tickaroo.tikxml.annotation.Xml;

import java.util.Date;


/**
 * MusicProgram Class
 *
 */

@SuppressWarnings("WeakerAccess")
@Xml(name = "AudioChProgram")
public class MusicProgram implements IMusicProgram, Parcelable {

    private static final String TAG =  "MusicProgram";

    @Attribute(name = "image")
    String image;

    @Attribute(name = "apid")
    int audioPid;

    @Attribute(name = "duration")
    int duration;

    @Attribute(name = "name")
    String name;

    @Attribute(name = "singer")
    String singer;

    @Attribute(name = "start", converter = XmlUtilDateConverter.class)
    Date startTime;

    @Attribute(name = "end", converter = XmlUtilDateConverter.class)
    Date endTime;

    public MusicProgram() {
    }


    protected MusicProgram(Parcel in) {
        image = in.readString();
        audioPid = in.readInt();
        duration = in.readInt();
        name = in.readString();
        singer = in.readString();
        startTime = (Date) in.readSerializable();
        endTime = (Date) in.readSerializable();
    }

    public static final Creator<MusicProgram> CREATOR = new Creator<MusicProgram>() {
        /**
         * create service from parcel
         * @param in
         * @return AVPrograms
         */
        @Override
        public MusicProgram createFromParcel(Parcel in) {
            return new MusicProgram(in);
        }

        /**
         * create service array
         * @param size
         * @return AVPrograms[]
         */
        @Override
        public MusicProgram[] newArray(int size) {
            return new MusicProgram[size];
        }
    };

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(image);
        dest.writeInt(audioPid);
        dest.writeInt(duration);
        dest.writeString(name);
        dest.writeString(singer);
        dest.writeSerializable(startTime);
        dest.writeSerializable(endTime);
    }

    /**
     * Get image path
     * @return  image path
     */
    @Override
    public String getImagePath(){
        return image;
    }

    /**
     * set image path
     * @param _albumImgURL
     */
    public void setImagePath(String _albumImgURL){
        image = _albumImgURL;
    }

    /**
     * Get audio pid
     * @return  audio pid
     */
    @Override
    public int getAudioPid(){
        return audioPid;
    }

    /**
     * set audio pid
     * @param _audioPid
     */
    public void setAudioPid(int _audioPid){
        audioPid = _audioPid;
    }

    /**
     * Get duration
     * @return  duration
     */
    @Override
    public int getDuration(){
        return duration;
    }

    /**
     * set duration
     * @param _duration
     */
    public void setDuration(int _duration){
        duration = _duration;
    }

    /**
     * Get name
     * @return  name
     */
    @Override
    public String getName(){
        return name;
    }

    /**
     * set name
     * @param _musicName
     */
    public void setName(String _musicName){
        name = _musicName;
    }

    /**
     * Get singer
     * @return  singer
     */
    @Override
    public String getSinger(){
        return singer;
    }

    /**
     * set singer
     * @param _singer
     */
    public void setSinger(String _singer){
        singer = _singer;
    }

    /**
     * Get start date
     * @return  start date
     */
    @Override
    public Date getStartDate(){
        return startTime;
    }

    /**
     * set start date
     * @param _startTime
     */
    public void setStartDate(Date _startTime){
        startTime = _startTime;
    }

    /**
     * Get end date
     * @return  end date
     */
    @Override
    public Date getEndDate(){
        return endTime;
    }

    /**
     * set end date
     * @param _endTime
     */
    public void setEndDate(Date _endTime){
        endTime = _endTime;
    }

}
