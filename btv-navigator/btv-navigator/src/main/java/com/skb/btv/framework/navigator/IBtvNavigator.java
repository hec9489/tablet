
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator;


/**
 * IBtvNavigator Interface
 *
 */

public interface IBtvNavigator {

    // DVBSI_TYPE
    public static final int DVBSI_TYPE_ANDROID_TEST  = 0;
    public static final int DVBSI_TYPE_UPDATE_CHANNEL = 1;
    public static final int DVBSI_TYPE_UPDATE_PROGRAMS = 2;
    public static final int DVBSI_TYPE_UPDATE_MUSIC_CHANNEL = 3;
    public static final int DVBSI_TYPE_UPDATE_MUSIC_PROGRAMS = 4;
    public static final int DVBSI_TYPE_UPDATE_MULTI_CHANNELS = 5;
    public static final int DVBSI_TYPE_CHANGE_PROGRAM = 6;
    public static final int DVBSI_TYPE_CHANGE_PROGRAMS = 7;

    public static final int DVBSERVICE_TYPE_AV  = 0x01;
    public static final int DVBSERVICE_TYPE_MUSIC  = 0x02;
    public static final int DVBSERVICE_TYPE_MULTIVIEW  = 0x03;

    public static final String DVBSI_JSONOBJECT_INFO = "info";
    public static final String DVBSI_JSONOBJECT_CHANNEL_NUM = "channel_num";
    public static final String DVBSI_JSONOBJECT_SID_NUM = "sid_num";
    public static final String DVBSI_JSONOBJECT_AUDIOPID = "audioPid";
    public static final String DVBSI_JSONOBJECT_DVBSERVICEVERSION = "version";

    public static final String DVBSI_EXTRADATA_UPDATE_CHANNEL_INFO = "UPDATE_CHANNEL_INFO";
    public static final String DVBSI_EXTRADATA_UPDATE_PROGRAM_INFO = "UPDATE_PROGRAM_INFO";
    public static final String DVBSI_EXTRADATA_UPDATE_MUSIC_CHANNEL_INFO = "UPDATE_MUSIC_CHANNEL_INFO";
    public static final String DVBSI_EXTRADATA_UPDATE_MUSIC_PROGRAM_INFO = "UPDATE_MUSIC_PROGRAM_INFO";
    public static final String DVBSI_EXTRADATA_UPDATE_MULTIVIEW_CHANNEL_INFO = "UPDATE_MULTIVIEW_CHANNEL_INFO";
    public static final String DVBSI_EXTRADATA_CHANGE_PROGRAM = "CHANGE_PROGRAM";
    public static final String DVBSI_EXTRADATA_CHANGE_PROGRAMS = "CHANGE_PROGRAMS";

    public static final String ACTION_NEED_CHANGE_REGION = "com.skb.btv.dvbsi.SET_REGION_CODE";
    public static final String ACTION_NEED_CHANGE_SEGID = "com.skb.btv.dvbsi.SET_SEGID";
    public static final String I_DVBSI_CHANNELALL_COMPLETE             = "com.skb.btv.dvbsi.DVBSI_CHANNELALL_COMPLETE";
    public static final String I_DVBSI_CHANNEL_COMPLETE                = "com.skb.btv.dvbsi.DVBSI_CHANNEL_COMPLETE";
    public static final String I_DVBSI_EPG_SCHEDULE_COMPLETE          = "com.skb.btv.dvbsi.DVBSI_EPG_SCHEDULE_COMPLETE";
    public static final String I_DVBSI_EPG_SCHEDULE_UPDATE             = "com.skb.btv.dvbsi.DVBSI_EPG_SCHEDULE_UPDATE";
    public static final String I_DVBSI_EPG_AUDIO_SCHEDULE_COMPLETE          = "com.skb.btv.dvbsi.DVBSI_EPG_AUDIO_SCHEDULE_COMPLETE";
    public static final String I_DVBSI_EPG_AUDIO_SCHEDULE_UPDATE             = "com.skb.btv.dvbsi.DVBSI_EPG_AUDIO_SCHEDULE_UPDATE";
    public static final String I_DVBSI_EPG_MULTIVIEW_CHANNEL_COMPLETE    = "com.skb.btv.dvbsi.DVBSI_EPG_MULTIVIEW_CHANNEL_COMPLETE";
    public static final String I_DVBSI_AUDIO_CHANNEL_COMPLETE                = "com.skb.btv.dvbsi.DVBSI_AUDIO_CHANNEL_COMPLETE";
    public static final String I_DVBSI_AUDIO_PROGRAM_COMPLETE                = "com.skb.btv.dvbsi.DVBSI_AUDIO_PROGRAM_COMPLETE";

    public static final String SYSPROP_STB_NAVI_DSMCC = "sys.stb.navi.dsmcc";

    int DVBSI_EVENT_CHANNEL_COMPLETED = 0;
    int DVBSI_EVENT_CHANNEL_UPDATED = 1;
    int DVBSI_EVENT_EPG_PRESENT_FOLLOW_UPDATED = 2;
    int DVBSI_EVENT_EPG_SCHEDULE_COMPLETED = 3;
    int DVBSI_EVENT_EPG_SCHEDULE_UPDATED = 4;
    int DVBSI_EVENT_AUDIO_CHANNEL_COMPLETED = 5;
    int DVBSI_EVENT_AUDIO_CHANNEL_UPDATED = 6;
    int DVBSI_EVENT_AUDIO_EPG_PRESENT_FOLLOW_UPDATED = 7;
    int DVBSI_EVENT_AUDIO_EPG_SCHEDULE_COMPLETED = 8;
    int DVBSI_EVENT_AUDIO_EPG_SCHEDULE_UPDATED = 9;
    int DVBSI_EVENT_MOSAIC_CHANNEL_COMPLETED = 10;

    int DVBSI_SID_MUSIC = 105;

}
