
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;

import java.util.ArrayList;


/**
 * IMultiViewDvbServiceList Interface
 *
 */

public interface IMultiViewDvbServiceList {
    boolean isReady();
    ArrayList<MultiViewDvbService> getList();
}
