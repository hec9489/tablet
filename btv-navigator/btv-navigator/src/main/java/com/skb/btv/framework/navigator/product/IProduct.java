
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.product;

import java.util.Date;


/**
 * IProduct Interface
 *
 */

public interface IProduct {

    public static final int PRODUCT_TERM_UNIT_DAY = 10;
    public static final int PRODUCT_TERM_UNIT_WEEK = 20;
    public static final int PRODUCT_TERM_UNIT_MONTH = 30;
    public static final int PRODUCT_TERM_UNIT_YEAR = 40;
    public static final int PRODUCT_TERM_UNIT_HOUR = 50;

    public static final int PRODUCT_TYPE_PPV = 10;
    public static final int PRODUCT_TYPE_PPM = 30;
    public static final int PRODUCT_TYPE_PACKAGE = 40;

    int getTerm();
    int getValue();
    int getdC();
    String getDsc();
    Date getEndDate();
    String getCode();
    String getName();
    int getPrice();
    Date getStartDate();
    int getType();

}
