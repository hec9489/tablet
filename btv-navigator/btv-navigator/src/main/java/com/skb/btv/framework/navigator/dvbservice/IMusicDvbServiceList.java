
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;

import java.util.ArrayList;


/**
 * IMusicDvbServiceList Interface
 *
 */

public interface IMusicDvbServiceList {
    boolean isReady();
    ArrayList<MusicDvbService> getList();
}
