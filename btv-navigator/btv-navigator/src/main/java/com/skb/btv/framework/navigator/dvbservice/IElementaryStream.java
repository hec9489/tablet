
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;


/**
 * IElementaryStream Interface
 *
 */

public interface IElementaryStream {

    public static final int ELEMENTARYSTREAM_TYPE_AACAudio = 15;
    public static final int ELEMENTARYSTREAM_TYPE_AC3Audio = 129;
    public static final int ELEMENTARYSTREAM_TYPE_AnnexADSMCC = 8;
    public static final int ELEMENTARYSTREAM_TYPE_DC2Video = 128;
    public static final int ELEMENTARYSTREAM_TYPE_DSMCCTypeA = 10;
    public static final int ELEMENTARYSTREAM_TYPE_DSMCCTypeB = 11;
    public static final int ELEMENTARYSTREAM_TYPE_DSMCCTypeC = 12;
    public static final int ELEMENTARYSTREAM_TYPE_DSMCCTypeD = 13;
    public static final int ELEMENTARYSTREAM_TYPE_H222_1 = 9;
    public static final int ELEMENTARYSTREAM_TYPE_H264Video = 27;
    public static final int ELEMENTARYSTREAM_TYPE_HVECVideo = 36;
    public static final int ELEMENTARYSTREAM_TYPE_MHEG = 7;
    public static final int ELEMENTARYSTREAM_TYPE_MPEG1Audio = 3;
    public static final int ELEMENTARYSTREAM_TYPE_MPEG1Video = 1;
    public static final int ELEMENTARYSTREAM_TYPE_MPEG2Audio = 4;
    public static final int ELEMENTARYSTREAM_TYPE_MPEG2Auxiliary = 14;
    public static final int ELEMENTARYSTREAM_TYPE_MPEG2PESPrivateData = 6;
    public static final int ELEMENTARYSTREAM_TYPE_MPEG2PrivateSection = 5;
    public static final int ELEMENTARYSTREAM_TYPE_MPEG2Video = 2;
    public static final int ELEMENTARYSTREAM_TYPE_MPEG4Video = 16;

    int getStreamType();
    int getStreamPid();
}
