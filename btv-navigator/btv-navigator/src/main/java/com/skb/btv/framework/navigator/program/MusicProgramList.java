
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.program;

import android.os.Parcel;

import com.skb.btv.framework.navigator.IBtvNavigator;
import com.tickaroo.tikxml.annotation.Element;
import com.tickaroo.tikxml.annotation.Xml;

import java.util.ArrayList;
import java.util.List;


/**
 * MusicProgramList Class
 *
 */

@Xml(name = "root")
public class MusicProgramList extends ProgramList implements IMusicProgramList{

    private static final String TAG =  "MusicProgramList";

    boolean mReady;

    @SuppressWarnings("WeakerAccess")
    @Element
    List<MusicProgram> musicProgramList;

    public MusicProgramList(){
        musicProgramList = new ArrayList<MusicProgram>();
    }

    public MusicProgramList(boolean ready){
        mReady = ready;
    }

    protected MusicProgramList(Parcel in) {
        mReady = in.readByte() != 0;
        musicProgramList = in.createTypedArrayList(MusicProgram.CREATOR);
    }

    public static final Creator<MusicProgramList> CREATOR = new Creator<MusicProgramList>() {
        /**
         * create service from parcel
         * @param in
         * @return MusicProgramList
         */
        @Override
        public MusicProgramList createFromParcel(Parcel in) {
            return new MusicProgramList(in);
        }

        /**
         * create service array
         * @param size
         * @return MusicProgramList[]
         */
        @Override
        public MusicProgramList[] newArray(int size) {
            return new MusicProgramList[size];
        }
    };

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (mReady ? 1 : 0));
        dest.writeTypedList(musicProgramList);
    }

    /**
     * Get ready state
     * @return  ready state
     */
    @Override
    public boolean isReady() {
        if(musicProgramList != null && musicProgramList.size() > 0){
            mReady = true;
        } else {
            mReady = false;
        }
        return mReady;
    }

    /**
     * Get Music program list
     * @return  Music program list
     */
    @Override
    public ArrayList<MusicProgram> getMusicProgramList() {
        return (ArrayList<MusicProgram>) musicProgramList;
    }

    /**
     * add Music program
     * @param programs  AV programs
     */
    public void addMusicPrograms(MusicProgram programs)
    {
        if(musicProgramList == null){
            musicProgramList = new ArrayList<MusicProgram>();
        }
        musicProgramList.add(programs);
    }

    /**
     * Get request type
     * @return  Music DVBSERVICE_TYPE_MUSIC
     */
    @Override
    public int getRequestType(){
        return IBtvNavigator.DVBSERVICE_TYPE_MUSIC;
    }
}
