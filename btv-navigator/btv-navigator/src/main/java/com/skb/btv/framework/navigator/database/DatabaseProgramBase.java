
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.database;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.skb.btv.framework.navigator.program.AVProgram;
import com.skb.btv.framework.navigator.program.AVProgramLinkInfo;
import com.skb.btv.framework.navigator.program.AVProgramList;
import com.skb.btv.framework.navigator.program.AVPrograms;
import com.skb.btv.framework.navigator.log.SLog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


/**
 * DatabaseProgramBase Class
 *
 */

public class DatabaseProgramBase<T extends SQLiteOpenHelper> {

    private static final String TAG = "DatabaseProgramBase";

    private static Map<String, Integer> res = new HashMap<String, Integer>();
    static {
        res.put("SD", 0);
        res.put("HD", 1);
        res.put("4K", 2);
        res.put("8K", 3);
    }

    private static Map<String, Integer> audio = new HashMap<String, Integer>();
    static {
        audio.put("none", 0);
        audio.put("mono", 1);
        audio.put("stereo", 2);
        audio.put("ac3", 3);
    }

    SimpleDateFormat currentTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    protected SQLiteDatabase database;
    protected T dbHelper;

    public static final String TABLE_NAME = "programinfo";


    /**
     * construct SQLiteDatabase
     * @throws SQLException : SQLException
     */
    public void open() throws SQLException {
        database = dbHelper.getReadableDatabase();
        SLog.w(TAG, "programinfo open() : "+ this + " dbHelper:" + dbHelper);
    }

    /**
     * SQLiteOpenHelper close
     */
    public void close() {
        if (dbHelper != null) {
            dbHelper.close();
            SLog.w(TAG, "programinfo close() : "+ this + " dbHelper:" + dbHelper);
        }
    }

    /**
     * Qeury All data by current time
     * @return : AVProgramList
     */
    public AVProgramList getAllQueryData(){
        AVProgramList queryAllEntities = new AVProgramList();
        SLog.d(TAG ,"getAllQueryData() start");
        String whereClause = DatabaseColumnIndex.COL_ENDFTIME_STRING + " >= ? ";
        String[] whereArgs = new String[] { currentTimeFormat.format(new Date()) };
        Cursor cursor = database.query(TABLE_NAME,
                null, whereClause, whereArgs, null, null,
                DatabaseColumnIndex.COL_CHNUM_STRING + " ASC, " +
                        DatabaseColumnIndex.COL_STARTFTIME_STRING + " ASC", null);

        if(cursor != null && cursor.getCount()>0) {
            cursor.moveToFirst();
            SLog.d(TAG, "getAllQueryData() Program Count :  "+cursor.getCount());
            int backnumber = -1;
            boolean first = false;
            AVPrograms programsEntity = null;
            while (!cursor.isAfterLast()) {
                int chnum = cursor.getInt(DatabaseColumnIndex.COL_CHNUM_INDEX);
                if(backnumber != chnum){
                    if(programsEntity != null) {
                        queryAllEntities.addAVPrograms(programsEntity);
                    }
                    programsEntity = new AVPrograms(chnum);
                    AVProgram programEntity = cursorToComment(cursor);
                    if(programsEntity == null) {
                        SLog.e(TAG, "getAllQueryData() programsEntity null #1");
                    } else if(programEntity == null) {
                        SLog.e(TAG, "getAllQueryData() programEntity null #1");
                    } else {
                        programsEntity.addAVProgram(programEntity);
                    }
                    backnumber = chnum;
                }else {
                    AVProgram programEntity = cursorToComment(cursor);
                    if(programsEntity == null) {
                        SLog.e(TAG, "getAllQueryData() programsEntity null #2");
                    } else if(programEntity == null) {
                        SLog.e(TAG, "getAllQueryData() programEntity null #2");
                    } else {
                        programsEntity.addAVProgram(programEntity);
                    }
                }
                cursor.moveToNext();
            }
            if(programsEntity != null) {
                queryAllEntities.addAVPrograms(programsEntity);
            }

        }
        if(cursor != null) cursor.close();
        SLog.d(TAG ,"getAllQueryData() end");
        return queryAllEntities;
    }

    /**
     * Convert cursor to data class
     * @param cursor : query result
     * @return : data class
     */
    protected AVProgram cursorToComment(Cursor cursor) {
        AVProgram programEntity = new AVProgram();
        programEntity.setChannelId(cursor.getInt(DatabaseColumnIndex.COL_SERVICE_INDEX));
        programEntity.setName(XmlUtilStringConverter(cursor.getString(DatabaseColumnIndex.COL_NAME_INDEX)));
        programEntity.setDescription(XmlUtilStringConverter(cursor.getString(DatabaseColumnIndex.COL_DESCRIPTION_INDEX)));
        programEntity.setPrice(cursor.getString(DatabaseColumnIndex.COL_PRICE_INDEX));
        programEntity.setResolution(XmlUtilResolutionConverter(cursor.getString(DatabaseColumnIndex.COL_RESOLUTION_INDEX)));
        programEntity.setAudio(XmlUtilAudioTypeConverter(cursor.getString(DatabaseColumnIndex.COL_AUDIO_INDEX)));
        programEntity.setEventid(cursor.getInt(DatabaseColumnIndex.COL_EVENTID_INDEX));
        programEntity.setRating(cursor.getInt(DatabaseColumnIndex.COL_RATING_INDEX));
        programEntity.setDuration(cursor.getInt(DatabaseColumnIndex.COL_DURATION_INDEX));
        programEntity.setStartTime(cursor.getString(DatabaseColumnIndex.COL_STARTTIME_INDEX));
        programEntity.setEndTime(cursor.getString(DatabaseColumnIndex.COL_ENDTIME_INDEX));
        programEntity.setStartYear(cursor.getInt(DatabaseColumnIndex.COL_STARTYEAR_INDEX));
        programEntity.setStartMonth(cursor.getInt(DatabaseColumnIndex.COL_STARTMONTH_INDEX));
        programEntity.setStartDay(cursor.getInt(DatabaseColumnIndex.COL_STARTDAY_INDEX));
        programEntity.setEndYear(cursor.getInt(DatabaseColumnIndex.COL_ENDYEAR_INDEX));
        programEntity.setEndMonth(cursor.getInt(DatabaseColumnIndex.COL_ENDMONTH_INDEX));
        programEntity.setEndDay(cursor.getInt(DatabaseColumnIndex.COL_ENDDAY_INDEX));
        programEntity.setLang(cursor.getString(DatabaseColumnIndex.COL_LANG_INDEX));
        programEntity.setActors(cursor.getString(DatabaseColumnIndex.COL_ACTORS_INDEX));
        programEntity.setIsCaption(XmlUtilBooleanYnConverter(cursor.getString(DatabaseColumnIndex.COL_CAPTION_INDEX)));
        programEntity.setContentNibble1(cursor.getInt(DatabaseColumnIndex.COL_CONTENTNIBBLE1_INDEX));
        programEntity.setContentNibble2(cursor.getInt(DatabaseColumnIndex.COL_CONTENTNIBBLE2_INDEX));
        programEntity.setContentUserNibble1(cursor.getInt(DatabaseColumnIndex.COL_CONTENTUSERNIBBLE1_INDEX));
        programEntity.setContentUserNibble2(cursor.getInt(DatabaseColumnIndex.COL_CONTENTUSERNIBBLE2_INDEX));
        programEntity.setDirector(cursor.getString(DatabaseColumnIndex.COL_DIRECTOR_INDEX));
        programEntity.setIsDolbyAudio(XmlUtilBooleanYnConverter(cursor.getString(DatabaseColumnIndex.COL_DOLBYAUDIO_INDEX)));
        programEntity.setisDvs(XmlUtilBooleanYnConverter(cursor.getString(DatabaseColumnIndex.COL_DVS_INDEX)));
        programEntity.setImagePath(cursor.getString(DatabaseColumnIndex.COL_PROGRAMIMAGEPATH_INDEX));

        AVProgramLinkInfo linkInfo = new AVProgramLinkInfo();
        linkInfo.setButtonImagePath(cursor.getString(DatabaseColumnIndex.COL_BUTTONIMAGEPATH_INDEX));
        linkInfo.setButtonType(cursor.getInt(DatabaseColumnIndex.COL_BUTTONTYPE_INDEX));
        linkInfo.setCMenuValue(cursor.getInt(DatabaseColumnIndex.COL_CMENUVALUE_INDEX));
        linkInfo.setDisplayStartDate(XmlUtilDateConverter(cursor.getString(DatabaseColumnIndex.COL_DISPLAYSTARTTIME_INDEX)));
        linkInfo.setDisplayEndDate(XmlUtilDateConverter(cursor.getString(DatabaseColumnIndex.COL_DISPLAYENDTIME_INDEX)));
        linkInfo.setLinkedServiceText(cursor.getString(DatabaseColumnIndex.COL_LINKEDSERVICETEXT_INDEX));
        linkInfo.setLinkedServiceType(cursor.getInt(DatabaseColumnIndex.COL_LINKEDSERVICETYPE_INDEX));
        linkInfo.setVasItemId(cursor.getString(DatabaseColumnIndex.COL_VASITEMID_INDEX));
        linkInfo.setVasPath(cursor.getString(DatabaseColumnIndex.COL_VASPATH_INDEX));
        linkInfo.setVasServiceId(cursor.getString(DatabaseColumnIndex.COL_VASSERVICEID_INDEX));
        //linkInfo.setVasServiceId(0);

        programEntity.addAVProgramLinkInfo(linkInfo);
        programEntity.setRunningStatus(cursor.getInt(DatabaseColumnIndex.COL_RUNNINGSTATUS_INDEX));
        programEntity.setVodId(cursor.getString(DatabaseColumnIndex.COL_VODID_INDEX));
        programEntity.setIsSignLangOn(cursor.getInt(DatabaseColumnIndex.COL_SIGNLANGUAGE_INDEX));

        return programEntity;
    }

    /**
     * xml string converter
     * @param value
     * @return String
     */
    private String XmlUtilStringConverter(String value) {
        return value
                .replaceAll("&lt;", "<")
                .replaceAll("&gt;", ">")
                .replaceAll("&amp;", "&")
                .replaceAll("&quot;", "\"")
                .replaceAll("&apos;", "\'")
                .replaceAll("&nbsp;", " ");
    }

    /**
     * convert resolution string to int value
     * @param s
     * @return String
     */
    private int XmlUtilResolutionConverter(String s){
        return res.get(s);
    }

    /**
     * convert audio type string to int value
     * @param s
     * @return String
     */
    private int XmlUtilAudioTypeConverter(String s){
        return audio.get(s);
    }

    /**
     * convert "Y","N" string to boolean value
     * @param s
     * @return boolean
     */
    private boolean XmlUtilBooleanYnConverter(String s){
        if (s != null) {
            if (!s.equals("N")) {
                return true;
            }
        }
        return false;
    }

    /**
     * convert date string to Date instance
     * @param s
     * @return Date
     */
    private Date XmlUtilDateConverter(String s){
        Date date;
        try {
            date = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREAN).parse(s);
        } catch (ParseException e) {
            date = new Date(0);
        }
        return date;
    }

}
