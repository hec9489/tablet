
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;

import java.util.ArrayList;


/**
 * IMultiViewLogicalCellList Interface
 *
 */

public interface IMultiViewLogicalCellList {
    boolean isReady();
    ArrayList<MultiViewLogicalCell> getMultiViewLogicalCellList();
}
