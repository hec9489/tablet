
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;

import android.os.Parcel;
import android.os.Parcelable;

import com.tickaroo.tikxml.annotation.Element;
import com.tickaroo.tikxml.annotation.Xml;

import java.util.ArrayList;
import java.util.List;


/**
 * MultiViewLogicalCellList Class
 *
 */

@Xml(name = "channels")
public class MultiViewLogicalCellList implements IMultiViewLogicalCellList, Parcelable {

	private static final String TAG =  "MultiViewLogicalCellList";

    boolean mReady;

    @SuppressWarnings("WeakerAccess")
    @Element
    List<MultiViewLogicalCell> multiViewLogicalCellList;

    public MultiViewLogicalCellList() {
    }

	public MultiViewLogicalCellList(boolean ready){
        mReady = ready;
    }

    protected MultiViewLogicalCellList(Parcel in) {
		mReady = in.readByte() != 0;
        multiViewLogicalCellList = in.createTypedArrayList(MultiViewLogicalCell.CREATOR);
    }

    public static final Creator<MultiViewLogicalCellList> CREATOR = new Creator<MultiViewLogicalCellList>() {
        /**
         * create service from parcel
         * @param in
         * @return MultiViewLogicalCellList
         */
        @Override
        public MultiViewLogicalCellList createFromParcel(Parcel in) {
            return new MultiViewLogicalCellList(in);
        }

        /**
         * create service array
         * @param size
         * @return MultiViewLogicalCellList[]
         */
        @Override
        public MultiViewLogicalCellList[] newArray(int size) {
            return new MultiViewLogicalCellList[size];
        }
    };

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
	    dest.writeByte((byte) (mReady ? 1 : 0));
        dest.writeTypedList(multiViewLogicalCellList);
    }

    /**
     * Get ready state
     * @return  ready state
     */
    @Override
    public boolean isReady() {
        if(multiViewLogicalCellList != null && multiViewLogicalCellList.size() > 0){
            mReady = true;
        } else {
            mReady = false;
        }
        return mReady;
    }

    /**
     * Get MultiViewLogicalCell list
     * @return  MultiViewLogicalCell list
     */
    @Override
    public ArrayList<MultiViewLogicalCell> getMultiViewLogicalCellList(){
        return (ArrayList<MultiViewLogicalCell>)multiViewLogicalCellList;
    }

    /**
     * set logical cell info list
     * @param logicalCellInfoList ArrayList<MultiViewLogicalCell>
     */
    public void setLogicalCellList(ArrayList<MultiViewLogicalCell> logicalCellInfoList) {
        multiViewLogicalCellList = logicalCellInfoList;
    }

    /**
     * add logical cell info
     * @param lcInfo MultiViewLogicalCell
     */
    public void addMultiViewLogicalCell(MultiViewLogicalCell lcInfo){
        if(multiViewLogicalCellList == null){
            multiViewLogicalCellList = new ArrayList<MultiViewLogicalCell>();
        }
        multiViewLogicalCellList.add(lcInfo);
    }
}
