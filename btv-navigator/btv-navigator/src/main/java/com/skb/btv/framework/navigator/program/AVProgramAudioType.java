
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.program;

import android.os.Parcel;
import android.os.Parcelable;

import com.tickaroo.tikxml.annotation.Attribute;


/**
 * AVProgramAudioType Class
 *
 */

public class AVProgramAudioType implements  IAVProgramAudioType, Parcelable {

    private static final String TAG =  "AVProgramAudioType";
	
    @Attribute(name = "iso_639_language_code")
    String _Iso639_language_code;

    @Attribute(name = "audio_type")
    int audioType;

    public AVProgramAudioType() {
    }


    public AVProgramAudioType(int audioType, String _Iso639_language_code) {
        this._Iso639_language_code = _Iso639_language_code;
        this.audioType = audioType;
    }

    protected AVProgramAudioType(Parcel in) {
        _Iso639_language_code = in.readString();
        audioType = in.readInt();
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_Iso639_language_code);
        dest.writeInt(audioType);
    }

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AVProgramAudioType> CREATOR = new Creator<AVProgramAudioType>() {
        /**
         * create service from parcel
         * @param in
         * @return AVProgramAudioType
         */
        @Override
        public AVProgramAudioType createFromParcel(Parcel in) {
            return new AVProgramAudioType(in);
        }

        /**
         * create service array
         * @param size
         * @return AVProgramAudioType[]
         */
        @Override
        public AVProgramAudioType[] newArray(int size) {
            return new AVProgramAudioType[size];
        }
    };

    /**
     * Get Iso369 language code
     * @return  Iso369 language code
     */
    @Override
    public String getIso639_language_code() {
        return _Iso639_language_code;
    }

    /**
     * Set Iso369 language code
     * @param language
     */
    public void setIso639_language_code(String language){
        _Iso639_language_code = language;
    }

    /**
     * Get audio type
     * @return  audio type
     */
    @Override
    public int getAudioType() {
        return audioType;
    }

    /**
     * Set audio type
     * @param type audio type
     */
    public void setAudioType(int type){
        audioType = type;
    }
}
