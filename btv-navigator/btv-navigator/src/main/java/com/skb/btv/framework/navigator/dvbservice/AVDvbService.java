
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;

import android.os.Parcel;
import android.os.Parcelable;

import com.skb.btv.framework.navigator.xmlUtils.XmlUtilBooleanYnConverter;
import com.skb.btv.framework.navigator.xmlUtils.XmlUtilCommaListConverter;
import com.skb.btv.framework.navigator.xmlUtils.XmlUtilStringConverter;
import com.tickaroo.tikxml.annotation.Attribute;
import com.tickaroo.tikxml.annotation.Xml;

import java.util.ArrayList;


/**
 * AVDvbService Class
 *
 */

@Xml(name = "Channel")
public class AVDvbService implements IAVDvbService, Parcelable {

    private static final String TAG =  "AVDvbService";

    @Attribute(name = "sid")
    int sid;

    @Attribute(name = "ch")
    int ch;

    @Attribute(name = "cname", converter = XmlUtilStringConverter.class)
    String cname;

    @Attribute(name = "ip")
    String ip;

    @Attribute(name = "port")
    int port;

    @Attribute(name = "vpid")
    int vpid;

    @Attribute(name = "apid")
    int apid;

    @Attribute(name = "apid2")
    int apid2;

    @Attribute(name = "ppid")
    int ppid;

    @Attribute(name = "vst")
    int vst;

    @Attribute(name = "ast")
    int ast;

    @Attribute(name = "ast2")
    int ast2;

    @Attribute(name = "seg_id")
    int seq_Id;

    @Attribute(name = "res")
    int res;

    @Attribute(name = "pay")
    int pay;

    @Attribute(name = "sample")
    int sample;

    @Attribute(name = "rating")
    int rating;

    @Attribute(name = "image")
    String image;

    @Attribute(name = "category")
    int category;

    @Attribute(name = "genre")
    int genre;

    @Attribute(name = "runningStatus")
    int runningStatus;

    @Attribute(name = "product", converter = XmlUtilCommaListConverter.class)
    ArrayList<String> product;

    @Attribute(name = "ca_id")
    int ca_Id;

    @Attribute(name = "ca_pid")
    int ca_Pid;

    @Attribute(name = "localAreaCode")
    int localAreaCode;

    @Attribute(name = "channelUri", converter = XmlUtilStringConverter.class)
    String channelUri;

    @Attribute(name = "minor_channel_number")
    int minor_channel_number;

    @Attribute(name = "xpg_url", converter = XmlUtilStringConverter.class)
    String xpg_url;

    @Attribute(name = "ctype")
    int ctype;

    // TODO
    @Attribute(name = "tsid")
    int tsid;

    // TODO
    @Attribute(name = "onid")
    int onid;

    //
    ArrayList<ElementaryStream> elementaryStreamList;

    String channelCallsign;

    @Attribute(name = "opid")
    int opid = 0;

    @Attribute(name = "sign_lang_url", converter = XmlUtilStringConverter.class)
    String sign_lang_url;

    public AVDvbService() {}

    protected AVDvbService(Parcel in) {
        sid = in.readInt();
        ch = in.readInt();
        cname = in.readString();
        ip = in.readString();
        port = in.readInt();
        vpid = in.readInt();
        apid = in.readInt();
        apid2 = in.readInt();
        ppid = in.readInt();
        vst = in.readInt();
        ast = in.readInt();
        ast2 = in.readInt();
        seq_Id = in.readInt();
        res = in.readInt();
        pay = in.readInt();
        sample = in.readInt();
        rating = in.readInt();
        image = in.readString();
        category = in.readInt();
        genre = in.readInt();
        runningStatus = in.readInt();
        product = in.createStringArrayList();
        ca_Id = in.readInt();
        ca_Pid = in.readInt();
        localAreaCode = in.readInt();
        channelUri = in.readString();
        minor_channel_number = in.readInt();
        xpg_url = in.readString();
        tsid = in.readInt();
        onid = in.readInt();
        ctype = in.readInt();
        elementaryStreamList = in.createTypedArrayList(ElementaryStream.CREATOR);
        channelCallsign = in.readString();
        opid = in.readInt();
        sign_lang_url = in.readString();
    }

    public static final Creator<AVDvbService> CREATOR = new Creator<AVDvbService>() {
        /**
         * create service from parcel
         * @param in
         * @return AVDvbService
         */
        @Override
        public AVDvbService createFromParcel(Parcel in) {
            return new AVDvbService(in);
        }

        /**
         * create service array
         * @param size
         * @return AVDvbService[]
         */
        @Override
        public AVDvbService[] newArray(int size) {
            return new AVDvbService[size];
        }
    };

    /**
     * describe Contents
     * @return int
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write To Parcel
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(sid);
        dest.writeInt(ch);
        dest.writeString(cname);
        dest.writeString(ip);
        dest.writeInt(port);
        dest.writeInt(vpid);
        dest.writeInt(apid);
        dest.writeInt(apid2);
        dest.writeInt(ppid);
        dest.writeInt(vst);
        dest.writeInt(ast);
        dest.writeInt(ast2);
        dest.writeInt(seq_Id);
        dest.writeInt(res);
        dest.writeInt(pay);
        dest.writeInt(sample);
        dest.writeInt(rating);
        dest.writeString(image);
        dest.writeInt(category);
        dest.writeInt(genre);
        dest.writeInt(runningStatus);
        dest.writeStringList(product);
        dest.writeInt(ca_Id);
        dest.writeInt(ca_Pid);
        dest.writeInt(localAreaCode);
        dest.writeString(channelUri);
        dest.writeInt(minor_channel_number);
        dest.writeString(xpg_url);
        dest.writeInt(tsid);
        dest.writeInt(onid);
        dest.writeInt(ctype);
        dest.writeTypedList(elementaryStreamList);
        dest.writeString(channelCallsign);
        dest.writeInt(opid);
        dest.writeString(sign_lang_url);
    }

    /**
     * Get sid
     * @return  sid
     */
    @Override
    public int getSid() {
        return sid;
    }

    /**
     * Set sid
     * @param _sid
     */
    public void setSid(int _sid){
        sid = _sid;
    }

    /**
     * Get channel
     * @return  channel
     */
    @Override
    public int getCh() {
        return ch;
    }

    /**
     * Set channel
     * @param _ch
     */
    public void setCh(int _ch){
        ch = _ch;
    }

    /**
     * Get channel name
     * @return  channel name
     */
    @Override
    public String getName() {
        return cname;
    }

    /**
     * Set channel name
     * @param _name
     */
    public void setName(String _name){
        cname = _name;
    }

    /**
     * Get channel ip
     * @return  channel ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * Set ip
     * @param _ip
     */
    public void setIp(String _ip){
        ip = _ip;
    }

    /**
     * Get channel port
     * @return  channel port
     */
    public int getPort() {
        return port;
    }

    /**
     * Set port
     * @param _port
     */
    public void setPort(int _port){
        port = _port;
    }

    /**
     * Get channel apid
     * @return  channel apid
     */
    public int getApid(){
        return apid;
    }

    /**
     * Set apid
     * @param _apid
     */
    public void setApid(int _apid){
        apid = _apid;
    }

    /**
     * Get channel apid2
     * @return  channel apid2
     */
    public int getApid2(){
        return apid2;
    }

    /**
     * Set apid2
     * @param _apid2
     */
    public void setApid2(int _apid2){
        apid2 = _apid2;
    }

    /**
     * Get channel ppid
     * @return  channel ppid
     */
    @Override
	public int getPpid(){
		return ppid;
	}

    /**
     * Set ppid
     * @param _ppid
     */
    public void setPpid(int _ppid){
        ppid = _ppid;
    }

    /**
     * Get channel vpid
     * @return  channel vpid
     */
    public int getVpid(){
        return vpid;
    }

    /**
     * Set vpid
     * @param _vpid
     */
    public void setVpid(int _vpid){
        vpid = _vpid;
    }

    /**
     * Get channel vst
     * @return  channel vst
     */
    public int getVst(){
        return vst;
    }

    /**
     * Set vst
     * @param _vst
     */
    public void setVst(int _vst){
        vst = _vst;
    }

    /**
     * Get channel ast
     * @return  channel ast
     */
    public int getAst(){
        return ast;
    }

    /**
     * Set ast
     * @param _ast
     */
    public void setAst(int _ast){
        ast = _ast;
    }

    /**
     * Get channel ast2
     * @return  channel ast2
     */
    public int getAst2(){
        return ast2;
    }

    /**
     * Set ast2
     * @param _ast2
     */
    public void setAst2(int _ast2){
        ast2 = _ast2;
    }

    /**
     * Get channel seq_Id
     * @return  channel seq_Id
     */
    @Override
    public int getSeqId() {
        return seq_Id;
    }

    /**
     * Set seq id
     * @param _seqId
     */
    public void setSeqId(int _seqId){
        seq_Id = _seqId;
    }

    /**
     * Get channel res
     * @return  channel res
     */
    @Override
    public int getRes() {
        return res;
    }

    /**
     * Set res
     * @param _res
     */
    public void setRes(int _res){
        res = _res;
    }

    /**
     * Get channel pay or free
     * @return  channel pay or free
     */
    @Override
    public boolean isPay() {
        return (pay > 0);
    }

    /**
     * Set pay or free
     * @param _pay
     */
    public void setPay(int _pay){
        pay = _pay;
    }

    /**
     * Get channel sample time
     * @return  channel sample time
     */
    @Override
    public int getSample() {
        return sample;
    }

    /**
     * Set sample time
     * @param _sample
     */
    public void setSample(int _sample){
        sample = _sample;
    }

    /**
     * Get channel rating
     * @return  channel rating
     */
    @Override
    public int getRating() {
        return rating;
    }

    /**
     * Set rating
     * @param _rating
     */
    public void setRating(int _rating){
        rating = _rating;
    }

    /**
     * Get channel image path
     * @return  channel image path
     */
    @Override
    public String getImagePath() {
        return image;
    }

    /**
     * Set image path
     * @param path
     */
    public void setImagePath(String path) {
        image = path;
    }

    /**
     * Get channel category
     * @return  channel category
     */
    @Override
    public int getCategory() {
        return category;
    }

    /**
     * Set category
     * @param _category
     */
    public void setCategory(int _category){
        category = _category;
    }

    /**
     * Get channel genre
     * @return  channel genre
     */
    @Override
    public int getGenre() {
        return genre;
    }

    /**
     * Set genre
     * @param _genre
     */
    public void setGenre(int _genre){
        genre = _genre;
    }

    /**
     * Get channel runningStatus
     * @return  channel runningStatus
     */
    @Override
    public int getRunningStatus() {
        return runningStatus;
    }

    /**
     * Set running status
     * @param _runningStatus
     */
    public void setRunningStatus(int _runningStatus){
        runningStatus = _runningStatus;
    }

    /**
     * Get channel product
     * @return  channel product list
     */
    @Override
    public ArrayList<String> getProduct() {
        return product;
    }

    /**
     * Set product list
     * @param list
     */
    public void setProduct(ArrayList<String> list){
        product = list;
    }

    /**
     * Get channel ca_Id
     * @return  channel ca_Id
     */
    public int getCaId() {
        return ca_Id;
    }

    /**
     * Set cas id
     * @param _casId
     */
    public void setCaId(int _casId){
        ca_Id = _casId;
    }

    /**
     * Get channel ca_Pid
     * @return  channel ca_Pid
     */
    public int getCaPid() {
        return ca_Pid;
    }

    /**
     * Set cas pid
     * @param _casPid
     */
    public  void setCaPid(int _casPid){
        ca_Pid = _casPid;
    }

    /**
     * Get channel localAreaCode
     * @return  channel localAreaCode
     */
    @Override
    public int getLocalAreaCode() {
        return localAreaCode;
    }

    /**
     * Set local area code
     * @param _localAreaCode
     */
    public void setLocalAreaCode(int _localAreaCode) {
        localAreaCode = _localAreaCode;
    }

    /**
     * Get channel channelUri
     * @return  channel channelUri
     */
    @Override
    public String getChannelUri() {
        return channelUri;
    }

    /**
     * Set channel url
     * @param uri
     */
    public void setChannelUri(String uri){
        channelUri = uri;
    }

    /**
     * Get channel tsid
     * @return  channel tsid
     */
    public int getTsid() {
        return tsid;
    }

    /**
     * Set channel tsid
     * @param _tsid
     */
    public void setTsid(int _tsid){
        tsid = _tsid;
    }

    /**
     * Get channel onid
     * @return  channel onid
     */
    public int getOnid() {
        return onid;
    }

    /**
     * Set channel onid
     * @param _onid
     */
    public void setOnid(int _onid){
        onid = _onid;
    }

    /**
     * Get channel type
     * @return  channel type
     */
    @Override
    public int getChannelType() {
        return ctype;
    }

    /**
     * Set channel type
     * @param type
     */
    public void setChannelType(int type){
        ctype = type;
    }

    /**
     * Get sub channel type
     * @return sub channel type
     */
    @Override
    public int getSubChannel() {
        return minor_channel_number;
    }

    /**
     * Set sub channel type
     * @param ch
     */
    public void setSubChannel(int ch) {
        minor_channel_number = ch;
    }

    /**
     * Get virtual vod uri
     * @return virtual vod uri
     */
    @Override
    public String getVirtualVodUri() {
        return xpg_url;
    }

    /**
     * Set virtual vod uri
     * @param uri
     */
    public void setVirtualVodUri(String uri) {
        xpg_url = uri;
    }


    /**
     * Get channel elementary steam list
     * @return  channel elementary steam list
     */
    @Override
    public ArrayList<ElementaryStream> getElementaryStreamList() {
        if (elementaryStreamList == null) {
            elementaryStreamList = new ArrayList<>();
            if (vpid > 0) {
                elementaryStreamList.add(new ElementaryStream(vpid, vst));
            }
            if (apid > 0) {
                elementaryStreamList.add(new ElementaryStream(apid, ast));
            }
            if (apid2 > 0) {
                elementaryStreamList.add(new ElementaryStream(apid2, ast2));
            }
        }
        return elementaryStreamList;
    }

    /**
     * Set channel elementary stream list
     * @param list
     */
    public void setElementaryStreamList(ArrayList<ElementaryStream> list){
        elementaryStreamList = list;
    }

    /**
     * Add channel elementary stream list
     * @param es
     */
    public void addElementaryStreamList(ElementaryStream es){
        elementaryStreamList.add(es);
    }

    /**
     * Get channel Callsign
     * @return  channelCallsign
     */
    @Override
    public String getChannelCallsign() {
        if(channelCallsign == null || channelCallsign.isEmpty()){
            return cname.toUpperCase();
        }
        return channelCallsign.toUpperCase();
    }

    /**
     * Set channel call sign
     * @param callsign
     */
    public void setChannelCallsign(String callsign){
        channelCallsign = callsign;
    }

    /**
     * Get channel opid
     * @return  channel opid
     */
    @Override
    public int getOcPid() {
        return opid;
    }

    /**
     * Set channel op id
     * @param pid
     */
    public void setOcPid(int pid){
        opid = pid;
    }

    /**
     * Get channel sign_lang_url
     * @return  channel sign_lang_url
     */
    @Override
    public String getSignLangVideoUrl(){
        return sign_lang_url;
    }

    /**
     * Set sign lang video url
     * @param url
     */
    public void setSignLangVideoUrl(String url){
        sign_lang_url = url;
    }
}