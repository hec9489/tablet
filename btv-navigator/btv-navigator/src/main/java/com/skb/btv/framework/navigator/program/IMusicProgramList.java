
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.program;

import java.util.ArrayList;


/**
 * IMusicProgramList Interface
 *
 */

public interface IMusicProgramList {
    boolean isReady();
    ArrayList<MusicProgram> getMusicProgramList();
}
