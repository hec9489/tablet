
// Copyright 2019 SK Broadband Co., LTD.

package com.skb.btv.framework.navigator.dvbservice;

import java.util.List;


/**
 * IMultiViewLogicalCell Interface
 *
 */

public interface IMultiViewLogicalCell {
    int getAudioPID();
    int getCellBaseX();
    int getCellBaseY();
    String getCellChannelName();
    int getChannelNumber();
    int getCellHeight();
    int getCellLinkageInfo();
    String getCellName();
    int getServiceID();
    int getCellWidth();
    String getMultiViewChannelUri();
    int getDivisor();
    List<ElementaryStream> getElementaryStreamList();
    int getGroupID();
    int getGroupOrder();
    String getIpAddress();
    int getIso639LanguageCode();
    int getLogicalCellId();
    int getLogicalCellPresentationInfo();
    int getPcrPid();
    int getPositionType();
    int getRes();
    int getPort();
}
