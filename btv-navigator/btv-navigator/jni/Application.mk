
NDK_PROJECT_PATH := $(call my-dir)/..
#NDK_TOOLCHAIN_VERSION := 4.8

navigator-root-dir:=$(NDK_PROJECT_PATH)

APP_BUILD_SCRIPT:=$(call my-dir)/Android.mk
APP_PLATFORM := android-16
#APP_PLATFORM := android-8

#APP_CPPFLAGS += -frtti

#APP_STL := gnustl_static
#APP_STL := stlport_static
APP_STL := c++_static

APP_ABI := armeabi-v7a

BUILD_ICONV=0
BUILD_PREBUIT_ICONV=1