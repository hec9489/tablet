# script expect navigator-root-dir variable to be set by parent !


# libqi_epgmonitor.so
MODULE_NAME := libqi_epgmonitor
include $(navigator-root-dir)/modules/prebuilt/common_shared_prebuilt.mk

# libiconv.so
ifeq ($(BUILD_ICONV),1)
include $(navigator-root-dir)/modules/native/libs/iconv/Android.mk
endif

ifeq ($(BUILD_PREBUIT_ICONV),1)
MODULE_NAME := libiconv
include $(navigator-root-dir)/modules/prebuilt/common_shared_prebuilt.mk
endif

# libtuner
include $(navigator-root-dir)/modules/native/epg/tuner/Android.mk

# libdvbsi_epg4mosaic
include $(navigator-root-dir)/modules/native/epg/epg4mosaic/Android.mk

# libdvbsi_epg4music
include $(navigator-root-dir)/modules/native/epg/epg4music/Android.mk

# libdvbsi_epgdef
include $(navigator-root-dir)/modules/native/epg/epgdef/Android.mk

# libbtvdvbsidata
include $(navigator-root-dir)/modules/native/dvbsidatamanager/Android.mk

# libsqlite3_wrapper
include $(navigator-root-dir)/modules/native/sqlite3_wrapper/Android.mk

# skb_navigator
include $(navigator-root-dir)/modules/native/Android.mk

