package com.skb.framework.btvplayerui;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.skb.btv.framework.navigator.program.AVProgram;
import com.skb.btv.framework.navigator.program.AVPrograms;

import java.util.ArrayList;

public class TestProgram extends Activity implements View.OnClickListener{

    private  final static String TAG = "TestProgram";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_test_program);

        Button program_back = findViewById(R.id.program_back);
        program_back.setOnClickListener(this);

        ArrayList<String> items1 = new ArrayList<>();
        ArrayList<String> items2 = new ArrayList<>();
        ArrayList<String> items3 = new ArrayList<>();
        ArrayList<String> items4 = new ArrayList<>();

        ArrayList<AVPrograms> program_items = getIntent().getParcelableArrayListExtra("items");

        for(AVProgram tempValue : program_items.get(0).getAVProgramList()){
            items1.add(tempValue.getName() +" / " +tempValue.getStartTime());
        }
        TextView tv1 = findViewById(R.id.ch_num1);
        tv1.setText("CH : "+program_items.get(0).getSid());
        for(AVProgram tempValue : program_items.get(1).getAVProgramList()){
            items2.add(tempValue.getName() +" / " +tempValue.getStartTime());
        }
        TextView tv2 = findViewById(R.id.ch_num2);
        tv2.setText("CH : "+program_items.get(1).getSid());
        for(AVProgram tempValue : program_items.get(2).getAVProgramList()){
            items3.add(tempValue.getName() +" / " +tempValue.getStartTime());
        }
        TextView tv3 = findViewById(R.id.ch_num3);
        tv3.setText("CH : "+program_items.get(2).getSid());
        for(AVProgram tempValue : program_items.get(3).getAVProgramList()){
            items4.add(tempValue.getName() +" / " +tempValue.getStartTime());
        }
        TextView tv4 = findViewById(R.id.ch_num4);
        tv4.setText("CH : " + program_items.get(3).getSid());

        Log.d(TAG, "TV1: " + items1.size() + " TV2: " + items2.size() +" TV3: " + items3.size() +" TV4: " + items4.size());
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items1){

            @Override
            public View getView(int position, View convertView, ViewGroup parent)
            {
                View view = super.getView(position, convertView, parent);
                TextView tv = view.findViewById(android.R.id.text1);
                tv.setTextColor(Color.BLACK);
                return view;
            }
        };

        ListView listview1 = findViewById(R.id.listview1);
        listview1.setAdapter(adapter1);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items2){

            @Override
            public View getView(int position, View convertView, ViewGroup parent)
            {
                View view = super.getView(position, convertView, parent);
                TextView tv = view.findViewById(android.R.id.text1);
                tv.setTextColor(Color.BLACK);
                return view;
            }
        };

        ListView listview2 = findViewById(R.id.listview2);
        listview2.setAdapter(adapter2);

        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items3){

            @Override
            public View getView(int position, View convertView, ViewGroup parent)
            {
                View view = super.getView(position, convertView, parent);
                TextView tv = view.findViewById(android.R.id.text1);
                tv.setTextColor(Color.BLACK);
                return view;
            }
        };

        ListView listview3 = findViewById(R.id.listview3);
        listview3.setAdapter(adapter3);

        ArrayAdapter<String> adapter4 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items4){

            @Override
            public View getView(int position, View convertView, ViewGroup parent)
            {
                View view = super.getView(position, convertView, parent);
                TextView tv = view.findViewById(android.R.id.text1);
                tv.setTextColor(Color.BLACK);
                return view;
            }
        };

        ListView listview4 = findViewById(R.id.listview4);
        listview4.setAdapter(adapter4);
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick" +v.getId());
        switch (v.getId()){
            case R.id.program_back:
                finish();
                break;
        }
    }
}
