package com.skb.framework.btvplayerui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.skb.btv.framework.media.DvbServicePlayer;
import com.skb.btv.framework.media.VodPlayer;
import com.skb.btv.framework.navigator.BtvNavigator;
import com.skb.btv.framework.navigator.DvbSIService;
import com.skb.btv.framework.navigator.IBtvNavigator;
import com.skb.btv.framework.navigator.OnDvbSIListener;
import com.skb.btv.framework.navigator.common.Utils;
import com.skb.btv.framework.navigator.dvbservice.AVDvbService;
import com.skb.btv.framework.navigator.dvbservice.AVDvbServiceList;
import com.skb.btv.framework.navigator.dvbservice.IAVDvbService;
import com.skb.btv.framework.navigator.dvbservice.MultiViewDvbService;
import com.skb.btv.framework.navigator.dvbservice.MultiViewDvbServiceList;
import com.skb.btv.framework.navigator.dvbservice.MultiViewLogicalCell;
import com.skb.btv.framework.navigator.dvbservice.MusicDvbService;
import com.skb.btv.framework.navigator.dvbservice.MusicDvbServiceList;
import com.skb.btv.framework.navigator.program.AVProgram;
import com.skb.btv.framework.navigator.program.AVProgramList;
import com.skb.btv.framework.navigator.program.AVPrograms;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private final String TAG = "MainActivity";

    private Context mContext;

    private SurfaceView mIPTVSurfaceView;
    private SurfaceHolder mIPTVSurfaceHolder;
    private SurfaceView mIPTVSurfaceView_Pip;
    private SurfaceHolder mIPTVSurfaceHolder_Pip;
    private TextureView mCCTextureView;
    private TextView mTvCh;
    private Spinner mChannelSpinner;
    private Spinner mChannelSpinner_Pip;
    private ChannelAdapter mChannelAdapter;
    private ChannelAdapter mChannelAdapter_Pip;
    private TextView mProgramInfoTextView;
    private TextView mProgramInfoTextView_Pip;
    private Button mBtnPip;
    private Button mBtnMultiview;
    private Button mBtnMultiviewProgram;

    private boolean isPIPShowing = false;
    private boolean isMultiView = false;
    private int mCurrentPosition = 0;
    private int mCurrentPosition_Pip = 0;
    private int mCurrentChannelSID;
    private int mCurrentChannelSID_Pip;
    private int mChannel;
    private String mMultiViewChannel;
    private String mMultiViewURI;
    private List<AVDvbService> mChannelData;
    private List<AVProgram> mProgramList;
    private List<AVProgram> mProgramList_Pip;
    private List<MultiViewDvbService> mMultiViewChannelData;
    private List<MusicDvbService> mMusicChannelData;

    private Handler mHandler = new Handler();

    private DvbSIService mDvbSIService;
    private DvbServicePlayer mMainTVMediaPlayer;
    private DvbServicePlayer mPipTVMediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mContext = this;
        mDvbSIService = BtvNavigator.getDvbSIService(this);
        mDvbSIService.setOnDvbSIListener(onDvbSIListener);
        mProgramList = new ArrayList<>();
        mProgramList_Pip = new ArrayList<>();

        mIPTVSurfaceView = findViewById(R.id.sv_player);
        mIPTVSurfaceView.setKeepScreenOn(true);
        mIPTVSurfaceHolder = mIPTVSurfaceView.getHolder();
        mIPTVSurfaceHolder.addCallback(onMainSurfaceCallback);
        mIPTVSurfaceHolder.setFormat(PixelFormat.TRANSPARENT);

        mCCTextureView = findViewById(R.id.trv_cc);

        mChannelAdapter = new ChannelAdapter(this);
        mChannelSpinner = findViewById(R.id.s_channel);
        mChannelSpinner.setAdapter(mChannelAdapter);
        mChannelSpinner.setOnItemSelectedListener(onChannelItemSelectedListener);
        mProgramInfoTextView = findViewById(R.id.tv_programInfo);

        mIPTVSurfaceView_Pip = findViewById(R.id.sv_player_pip);
        mIPTVSurfaceView_Pip.setKeepScreenOn(true);
        mIPTVSurfaceHolder_Pip = mIPTVSurfaceView_Pip.getHolder();
        mIPTVSurfaceHolder_Pip.addCallback(onPipSurfaceCallback);
        mIPTVSurfaceHolder_Pip.setFormat(PixelFormat.TRANSPARENT);

        mTvCh = findViewById(R.id.tv_channel);

        mBtnPip = findViewById(R.id.btn_pip);
        mBtnPip.setOnClickListener(this);
        mChannelAdapter_Pip = new ChannelAdapter(this);
        mChannelSpinner_Pip = findViewById(R.id.s_channel_pip);
        mChannelSpinner_Pip.setAdapter(mChannelAdapter_Pip);
        mChannelSpinner_Pip.setOnItemSelectedListener(onChannelItemSelectedListener);
        mProgramInfoTextView_Pip = findViewById(R.id.tv_programInfo_pip);

        mBtnMultiview = findViewById(R.id.btn_multiview);
        mBtnMultiview.setOnClickListener(this);
        mBtnMultiviewProgram = findViewById(R.id.btn_multiviewprogram);
        mBtnMultiviewProgram.setOnClickListener(this);

        findViewById(R.id.iv_up).setOnClickListener(this);
        findViewById(R.id.iv_down).setOnClickListener(this);
        findViewById(R.id.iv_up_pip).setOnClickListener(this);
        findViewById(R.id.iv_down_pip).setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mBtnMultiviewProgram.setSelected(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mMainTVMediaPlayer != null) {
            mMainTVMediaPlayer.release();
        }
        if (mDvbSIService != null) {
            mDvbSIService.clearOnDvbSIListener(onDvbSIListener);
            mDvbSIService.release();
        }
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick" + v.getId());
        int id = v.getId();
        switch (id) {
            case R.id.iv_up:
                moveChannel(true, false);
                break;

            case R.id.iv_down:
                moveChannel(false, false);
                break;

            case R.id.btn_pip:
                if (isPIPShowing) {
                    setPIPChange(false);
                } else {
                    setPIPChange(true);
                }
                break;

            case R.id.iv_up_pip:
                moveChannel(true, true);
                break;

            case R.id.iv_down_pip:
                moveChannel(false, true);
                break;

            case R.id.btn_multiview:
                if (!isMultiView) {
                    displayMultiviewSelecteAlertDialog();
                } else {
                    showMultiView(false, mCurrentPosition);
                }
                break;

            case R.id.btn_multiviewprogram:
                ArrayList<AVPrograms> items = new ArrayList<>();
                AVProgramList program_result = null;
                if (mDvbSIService != null) {
                    program_result = mDvbSIService.getMultiAVProgramList(mMultiViewChannel);
                }
                if (program_result != null && program_result.getAVProgramsList() != null && program_result.getAVProgramsList().size() > 0) {
                    items.addAll(program_result.getAVProgramsList());
                }
                Log.d(TAG, "program_result: " + program_result + " items: " + items);
                Intent intent_program = new Intent(mContext, TestProgram.class);
                intent_program.putParcelableArrayListExtra("items", items);
                startActivity(intent_program);
                mBtnMultiviewProgram.setSelected(true);
                break;
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyUp KeyEvent=" + event.getKeyCode());
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_CHANNEL_UP:
                moveChannel(true, isPIPShowing);
                break;

            case KeyEvent.KEYCODE_CHANNEL_DOWN:
                moveChannel(false, isPIPShowing);
                break;

            case KeyEvent.KEYCODE_0:
            case KeyEvent.KEYCODE_1:
            case KeyEvent.KEYCODE_2:
            case KeyEvent.KEYCODE_3:
            case KeyEvent.KEYCODE_4:
            case KeyEvent.KEYCODE_5:
            case KeyEvent.KEYCODE_6:
            case KeyEvent.KEYCODE_7:
            case KeyEvent.KEYCODE_8:
            case KeyEvent.KEYCODE_9:
                mHandler.removeCallbacks(mRunSetCh);
                mChannel = 10 * mChannel + event.getKeyCode() - KeyEvent.KEYCODE_0;
                mHandler.postDelayed(mRunSetCh, 2000);
                if (mTvCh != null) {
                    mTvCh.setText(MessageFormat.format("{0}", mChannel));
                }
                break;

        }
        return super.onKeyUp(keyCode, event);
    }

    private void moveChannel(boolean isNext, boolean isPip) {
        int idx, count;
        if (isPip) {
            idx = mChannelSpinner_Pip.getSelectedItemPosition();
            count = mChannelSpinner_Pip.getCount();
        } else {
            idx = mChannelSpinner.getSelectedItemPosition();
            count = mChannelSpinner.getCount();
        }
        if (isNext) {
            if (++idx >= count) {
                idx = 0;
            }
        } else {
            if (--idx < 0) {
                idx = count - 1;
            }
        }
        //Log.d(TAG, "moveChannel - call setTV or setPIP. isPIPShowing : " + isPIPShowing);
        if (isPip) {
            mCurrentPosition_Pip = idx;
            Log.d(TAG, "===========   moveChannel - call setPIP. isPIPShowing : " + isPIPShowing);
            if (isPIPShowing) {
                setPIP(idx);
            } else {
                mChannelSpinner_Pip.setSelection(idx);
            }
        } else {
            setTV(idx);
        }
    }

    private void setTV(int index) {
        Log.d(TAG, "setTV - index : " + index + ", mChannelData : " + (mChannelData != null));
        if (mChannelData == null
            || (mChannelData.get(index).getChannelType() == IAVDvbService.AVDVBSERVICE_CHANNEL_TYPE_MUSIC_CHANNEL && mMusicChannelData == null)) {
            return;
        }
        Log.d(TAG, "setTV - ChannelType: " + mChannelData.get(index).getChannelType());
        if(mMainTVMediaPlayer != null) {
            String uri;
            if (mChannelData.get(index).getChannelType() == IAVDvbService.AVDVBSERVICE_CHANNEL_TYPE_MUSIC_CHANNEL) {
                uri = mMusicChannelData.get(0).getChannelUri();
            } else {
                uri = mChannelData.get(index).getChannelUri() + "&lf=1";
            }
            mMainTVMediaPlayer.tune(uri);
            mMainTVMediaPlayer.prepareAsync();
            if (mDvbSIService != null) {
//                mDvbSIService.setCurrentChannelUri(uri);
            }
        }
        mCurrentPosition = index;
        mChannelSpinner.setSelection(index);
        mCurrentChannelSID = mChannelData.get(index).getSid();
        moveProgram();
    }

    private void moveProgram() {
        if(mChannelData.get(mCurrentPosition).getChannelType() == IAVDvbService.AVDVBSERVICE_CHANNEL_TYPE_MUSIC_CHANNEL) {
            mProgramInfoTextView.setText(mMusicChannelData.get(0).getTitle());
        } else {
            AVProgramList program_result = BtvNavigator.getDvbSIService(this).getAVProgramList(mCurrentChannelSID);
            if (program_result != null && program_result.getAVProgramsList() != null && program_result.getAVProgramsList().size() > 0) {
                if (mProgramList != null) mProgramList.clear();
                if (program_result.getAVProgramsList().get(0) != null && program_result.getAVProgramsList().get(0).getAVProgramList() != null) {
                    mProgramList.addAll(program_result.getAVProgramsList().get(0).getAVProgramList());
                    int curRunningProgram = getCurrentRunningProgram(mProgramList);
                    if (curRunningProgram != -1) {
                        String programName = mProgramList.get(curRunningProgram).getName();
                        mProgramInfoTextView.setText(programName);
                        Log.d(TAG, "moveProgramPopUp() Program: " + mProgramInfoTextView.getText());
                    }
                }
            }
        }
    }

    private void setPIPChange(boolean isShow) {
        Log.d(TAG, "setPIPChange - isShow : " + isShow + ", pip pos : " + mCurrentPosition_Pip);
        mBtnPip.setSelected(isShow);
        isPIPShowing = isShow;
        if (isShow) {
            mBtnPip.setText("PIP OFF");
            if (mCurrentPosition_Pip == -1) {
                mCurrentPosition_Pip = 0;
            }
            if (mIPTVSurfaceView_Pip.getVisibility() != View.VISIBLE) {
                mIPTVSurfaceView_Pip.setVisibility(View.VISIBLE);
            } else {
                Log.d(TAG, "============== setPIPChange2 - call setPIP. pos : " + " => " + mCurrentPosition_Pip);
                setPIP(mCurrentPosition_Pip);
            }
        } else {
            mBtnPip.setText("PIP ON");
            mIPTVSurfaceView_Pip.setVisibility(View.INVISIBLE);
            if (mPipTVMediaPlayer != null) {
                mPipTVMediaPlayer.reset();
            }
        }
    }

    private void setPIP(int index) {
        Log.d(TAG, "setPIP - index : " + index);
        if (mChannelData == null || index < 0
                || (mChannelData.get(index).getChannelType() == IAVDvbService.AVDVBSERVICE_CHANNEL_TYPE_MUSIC_CHANNEL && mMusicChannelData == null)) {
            return;
        }

        Log.d(TAG, "setPIP - call setDataSource channel : " + mChannelData.get(index).getChannelUri());
        if (mPipTVMediaPlayer != null) {
            String uri;
            if(mChannelData.get(index).getChannelType() == IAVDvbService.AVDVBSERVICE_CHANNEL_TYPE_MUSIC_CHANNEL) {
                uri = mMusicChannelData.get(0).getChannelUri();
            } else {
                uri = mChannelData.get(index).getChannelUri();
            }
            mPipTVMediaPlayer.tune(uri);
            mPipTVMediaPlayer.prepareAsync();
            if (mDvbSIService != null) {
                mDvbSIService.setCurrentChannelUri(uri);
            }
        }
        mChannelSpinner_Pip.setSelection(index);
        mCurrentChannelSID_Pip = mChannelData.get(index).getSid();
        moveProgram_Pip();
    }

    private void moveProgram_Pip() {
        if (isPIPShowing) {
            if(mChannelData.get(mCurrentPosition_Pip).getChannelType() == IAVDvbService.AVDVBSERVICE_CHANNEL_TYPE_MUSIC_CHANNEL) {
                mProgramInfoTextView_Pip.setText(mMusicChannelData.get(0).getTitle());
            } else {
                AVProgramList programPip_result = null;
                programPip_result = mDvbSIService.getAVProgramList(mCurrentChannelSID_Pip);
                if (programPip_result != null && programPip_result.getAVProgramsList() != null && programPip_result.getAVProgramsList().size() > 0) {
                    if (mProgramList_Pip != null) {
                        mProgramList_Pip.clear();
                        mProgramList_Pip.addAll(programPip_result.getAVProgramsList().get(0).getAVProgramList());
                        int curRunningProgram = getCurrentRunningProgram(mProgramList_Pip);
                        if (curRunningProgram != -1) {
                            String programName = mProgramList_Pip.get(curRunningProgram).getName();
                            mProgramInfoTextView_Pip.setText(programName);
                        }
                    }
                }
            }
        }
    }

    private int getCurrentRunningProgram(List<AVProgram> programList) {
        int retIndex = -1;
        Calendar calendar = Calendar.getInstance();
        long reqST = calendar.getTimeInMillis();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        long reqET = calendar.getTimeInMillis();
        if (programList != null && programList.size() > 0) {
            for (AVProgram entity : programList) {
                retIndex++;
                long proST = entity.getStartDate().getTime();
                long proET = entity.getEndDate().getTime();
                if ((reqST <= proST && reqET >= proET)
                        || (reqST > proST && reqET >= proET && reqST < proET)
                        || (reqST <= proST && reqET < proET && reqET > proST)
                        || (reqST > proST && reqET < proET)) {
                    break;
                }
            }
        }
        Log.d(TAG, "getCurrentRunningProgram_Pip() index : " + retIndex);
        return retIndex;
    }

    private void displayMultiviewSelecteAlertDialog() {
        CharSequence[] multichannelname = new CharSequence[mMultiViewChannelData.size()];
        for (int i = 0; i < mMultiViewChannelData.size(); i++) {
            multichannelname[i] = mMultiViewChannelData.get(i).getGroupName();
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("멀티 채널 선택하세요.")
                .setNegativeButton("취소", null)
                .setItems(multichannelname, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        displayMultiView(mMultiViewChannelData.get(which));
                    }
                })
                .show();
    }

    private void displayMultiView(MultiViewDvbService multiview) {
        List<MultiViewLogicalCell> logicalCellList = mDvbSIService.getMultiViewLogicalCellList(multiview.getGroupID()).getMultiViewLogicalCellList();
        StringBuilder channelList = new StringBuilder();
        if (logicalCellList != null && logicalCellList.size() > 0) {
            if (isPIPShowing) {
                Log.d(TAG, "============== displayMultiView - call setPIPChange. pos : " + mPipTVMediaPlayer.getCurrentPosition() + " => " + mCurrentPosition_Pip);
                mChannelSpinner_Pip.setSelection(mCurrentPosition_Pip);
                setPIPChange(false);
            }

            for (MultiViewLogicalCell entity : logicalCellList) {
                channelList.append("").append(entity.getChannelNumber()).append("|");
            }
            channelList = new StringBuilder(channelList.substring(0, channelList.toString().lastIndexOf('|')));
            mMultiViewChannel = channelList.toString();
            if (mDvbSIService != null) {
                mDvbSIService.setCurrentMultiChannel(channelList.toString());
                mMultiViewURI = mDvbSIService.makeMultiViewUri(logicalCellList);
            }
            int ch = Integer.parseInt(channelList.substring(channelList.toString().lastIndexOf('|') + 1));
            Log.e(TAG, "multiview - displayMultiView - channel count : " + mChannelData.size()
                    + ", to find ch : " + ch);
            int pos = 0;
            for (int i = 0; i < mChannelData.size(); i++) {
                if (ch == mChannelData.get(i).getCh()) {
                    pos = i;
                }
            }
            Log.e(TAG, "multiview - displayMultiView - isPIPShowing : " + isPIPShowing
                    + ", pos : " + pos + ", mMultiViewURI : " + mMultiViewURI);
            showMultiView(true, pos);
        }
        mBtnMultiviewProgram.setVisibility(View.VISIBLE);
    }

    private void showMultiView(boolean isShow, int pos) {
        Log.d(TAG, "showMultiView - isShow : " + isShow + ", pip pos : " + mCurrentPosition_Pip
                + ", curr pos : " + mCurrentPosition);
        if (isShow) {
            isMultiView = true;
            mCurrentPosition_Pip = pos;
//            mLinearMain.setVisibility(View.GONE);
            mBtnPip.setEnabled(false);
            setMultiTV();
            setPIPChange(true);
            setAudioFocus(0);
            mBtnMultiview.setText("Multi\nOFF");
        } else {
            isMultiView = false;
            mChannelSpinner_Pip.setSelection(mCurrentPosition_Pip);
//            mChannelSpinner.setSelection(mCurrentPosition);
            if (isPIPShowing) {
                setPIPChange(false);
            }
            setTV(mCurrentPosition);
            setAudioFocus(1);
            mBtnMultiview.setText("Multi\nON");
            mBtnMultiviewProgram.setVisibility(View.GONE);
//            mLinearMain.setVisibility(View.VISIBLE);
            mBtnPip.setEnabled(true);
        }
        mBtnMultiview.setSelected(isShow);
    }

    private void setMultiTV() {
        Log.d(TAG, "setMultiTV - mMultiViewURI : " + mMultiViewURI + ", mChannelData : " + mChannelData);
        if (mChannelData == null) {
            return;
        }

        if (mMultiViewURI != null && !mMultiViewURI.isEmpty()) {
            mMainTVMediaPlayer.tune(mMultiViewURI);
            mMainTVMediaPlayer.prepareAsync();
//            try {
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        mMainTVMediaPlayer.prepareAsync();
//                    }
//                }).start();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        }
    }

    private void setAudioFocus(int index) {
        Log.d(TAG, "setAudioFocus- index : " + index
                + ", is pip playing : " + ((mPipTVMediaPlayer != null) ? mPipTVMediaPlayer.isPlaying() : "null")
                + ", is pip showing : " + isPIPShowing + ", pip player : " + mPipTVMediaPlayer
                + ", is playing : " + ((mMainTVMediaPlayer != null) ? mMainTVMediaPlayer.isPlaying() : "null"));
        if (isPIPShowing) {
            if (index == 0) {
                if (mPipTVMediaPlayer != null) {
                    mPipTVMediaPlayer.selectTrack(0);
                    mPipTVMediaPlayer.setVolume(1.0f);
                }
            } else {
                if (mMainTVMediaPlayer != null) {
                    mMainTVMediaPlayer.selectTrack(index - 1);
                    mMainTVMediaPlayer.setVolume(1.0f);
                }
            }
        } else {
            if (mMainTVMediaPlayer != null) {
                mMainTVMediaPlayer.selectTrack(0);
                mMainTVMediaPlayer.setVolume(1.0f);
            }
        }
    }

    SurfaceHolder.Callback onMainSurfaceCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            Log.d(TAG, "onMainSurfaceCallback - surfaceCreated");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mMainTVMediaPlayer = new DvbServicePlayer(MainActivity.this);
                    mMainTVMediaPlayer.setDisplay(mIPTVSurfaceHolder);
                    mMainTVMediaPlayer.setOnPreparedListener(new DvbServicePlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(DvbServicePlayer mediaPlayer) {
                            String chInfo = "CH No. " + mChannelData.get(mCurrentPosition).getCh() + "   -  " + mChannelData.get(mCurrentPosition).getName();
                            Log.d(TAG, "playerCallback - onPrepared, chInfo:" + chInfo + ", mp : " + mediaPlayer);

                            mMainTVMediaPlayer.start();
                        }
                    });

                    mMainTVMediaPlayer.setOnInfoListener(new DvbServicePlayer.OnInfoListener() {
                        @Override
                        public boolean onInfo(DvbServicePlayer mp, int what, int extra) {
                            Log.d(TAG, "playerCallback - onInfo, what:" + what + ", mp : " + mp);
                            if (what == 7001) {
                                // Video Pid changed
                                // extra => 변경된 Pid 값
                            } else if (what == 7002) {
                                // audio Pid changed
                                // extra => 변경된 Pid 값
                            }
                            return false;
                        }
                    });
                    mMainTVMediaPlayer.setOnCompletionListener(new DvbServicePlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(DvbServicePlayer mp) {
                            Log.d(TAG, "playerCallback - onCompletion, mp:" + mp);
                        }
                    });
                    mMainTVMediaPlayer.setOnErrorListener(new DvbServicePlayer.OnErrorListener() {
                        @Override
                        public boolean onError(DvbServicePlayer mp, int what, int extra) {
                            Log.e(TAG, "playerCallback - onError, what:" + what + ", mp : " + mp);
                            return false;
                        }
                    });
                    mCCTextureView.setKeepScreenOn(true);
                    mCCTextureView.setSurfaceTextureListener(new CanvasListener());

                    mChannelSpinner.setSelection(mCurrentPosition);
                }
            });
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Log.d(TAG, "onMainSurfaceCallback - surfaceChanged, channel count : "
                    + ((mChannelData != null) ? mChannelData.size() : -1) + ", mMainTVMediaPlayer : " + mMainTVMediaPlayer);

            if (mChannelData == null)
                return;

            for (int i = 0; i < mChannelData.size(); i++) {
                AVDvbService channel = mChannelData.get(i);
                if (channel.getName().equals("JTBC GOLF")) {
                    mCurrentPosition = i;
                    break;
                }
            }
            Log.d(TAG, "onMainSurfaceCallback - surfaceChanged, mCurrentPosition : " + mCurrentPosition);
            if (mMainTVMediaPlayer != null) {
                // Start Add dvbsi scenario - KGH
                // setTV(mCurrentPosition);
                if (mChannelData.get(mCurrentPosition).getChannelType() != IAVDvbService.AVDVBSERVICE_CHANNEL_TYPE_MUSIC_CHANNEL) {
                    setTV(mCurrentPosition);
                }
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            Log.d(TAG, "onMainSurfaceCallback - surfaceDestroyed");
        }
    };

    private class CanvasListener implements TextureView.SurfaceTextureListener {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface,
                                              int width, int height) {
            Log.d(TAG, "CanvasListener");
            mMainTVMediaPlayer.setCCDisplay(mCCTextureView);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return true;
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface,
                                                int width, int height) {
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        }
    }

    SurfaceHolder.Callback onPipSurfaceCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            Log.d(TAG, "onPipSurfaceCallback - surfaceCreated, old mPipTVMediaPlayer : " + mPipTVMediaPlayer);
            mPipTVMediaPlayer = new DvbServicePlayer(MainActivity.this, 1);
            mPipTVMediaPlayer.setDisplay(mIPTVSurfaceHolder_Pip);
//            mPipTVMediaPlayer.setSurfaceViewLayout(mIPTVSurfaceView_Pip, mIPTVSurfaceView_Pip);
            mPipTVMediaPlayer.setOnInfoListener(new DvbServicePlayer.OnInfoListener() {
                @Override
                public boolean onInfo(DvbServicePlayer mp, int what, int extra) {
                    Log.d(TAG, "playerCallback - onInfo, what:" + what + ", mp : " + mp);
                    if (what == 10000 && extra == 20008) {
                        mPipTVMediaPlayer.setVolume(1.0f);
                        mMainTVMediaPlayer.setVolume(0);
                    }
                    return false;
                }
            });
            mPipTVMediaPlayer.setOnCompletionListener(new DvbServicePlayer.OnCompletionListener() {
                @Override
                public void onCompletion(DvbServicePlayer mp) {
                    Log.d(TAG, "playerCallback - onCompletion, mp : " + mp);
                }
            });
            mPipTVMediaPlayer.setOnErrorListener(new DvbServicePlayer.OnErrorListener() {
                @Override
                public boolean onError(DvbServicePlayer mp, int what, int extra) {
                    Log.e(TAG, "playerCallback - onError, what:" + what + ", mp : " + mp);
                    return false;
                }
            });

            mPipTVMediaPlayer.setOnPreparedListener(new DvbServicePlayer.OnPreparedListener() {
                @Override
                public void onPrepared(DvbServicePlayer mediaPlayer) {
                    String chInfo = "CH No. " + mChannelData.get(mCurrentPosition_Pip).getCh() + "   -  " + mChannelData.get(mCurrentPosition_Pip).getName();
                    Log.d(TAG, "playerCallback - onPrepared, channel info : " + chInfo + ", mp : " + mediaPlayer);

                    mPipTVMediaPlayer.start();
                }
            });

            if (isPIPShowing) {
                Log.d(TAG, "============== surfaceCreated - call setPIP. pos : " + mPipTVMediaPlayer.getCurrentPosition() + " => " + mCurrentPosition_Pip);
                setPIP(mCurrentPosition_Pip);
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Log.d(TAG, "onPipSurfaceCallback - surfaceChanged");
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            Log.d(TAG, "onPipSurfaceCallback - surfaceDestroyed");
        }
    };

    AdapterView.OnItemSelectedListener onChannelItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
            Log.d(TAG, "onChannelItemSelectedListener "
                    + ", is s_channel : " + (parent.getId() == R.id.s_channel) + ", Item:" + parent.getId());

            if (mChannelData == null) {
                return;
            }

            int vId = parent.getId();
            if (vId == R.id.s_channel) {
                mCurrentPosition = i;
                setTV(i);
            } else if (vId == R.id.s_channel_pip) {
                mCurrentPosition_Pip = i;
                setPIP(i);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private Runnable mRunSetCh = new Runnable() {
        @Override
        public void run() {
            Log.d("channel", "set channel : " + mChannel);
            if(mChannelData == null) {
                if (mTvCh != null) {
                    mTvCh.setText("");
                }
                mChannel = 0;
                return;
            }

            if (mChannel <= 0) {
                mChannel = 0;
                if (mTvCh != null) {
                    mTvCh.setText("");
                }
                return;
            }

            int idx = -1;
            for (int i = 0; i < mChannelData.size(); i++) {
                AVDvbService channel = mChannelData.get(i);
                if (channel.getCh() == mChannel) {
                    idx = i;
                    break;
                }
            }

            if (idx != -1) {
                Log.d("channel", "Ch idx: " + idx);
                if (isPIPShowing) {
                    if (idx != mChannelSpinner_Pip.getSelectedItemPosition()) {
                        Log.d(TAG, "============== RunSetCh - call setPIP");
                        setPIP(idx);
                    }
                } else {
                    if (idx != mChannelSpinner.getSelectedItemPosition()) {
                        setTV(idx);
                    }
                }
            }

            if (mTvCh != null) {
                mTvCh.setText("");
            }
            mChannel = 0;
        }
    };

    public OnDvbSIListener onDvbSIListener = new OnDvbSIListener() {
        @Override
        public void onDvbSIUpdated(int dvbsiType, final String extraData) {
            Log.d(TAG, "onDvbSIUpdated() dvbsiType : " + dvbsiType + ", extra : " + extraData);
            if (dvbsiType == IBtvNavigator.DVBSI_TYPE_UPDATE_PROGRAMS) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String extraString = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            extraString = json.getString(IBtvNavigator.DVBSI_JSONOBJECT_INFO);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        AVProgramList program_result;
                        program_result = mDvbSIService.getAVProgramList(mCurrentChannelSID);
                        Log.d("onDvbSIUpdated()", "program_result : " + program_result);
                        if (program_result != null && program_result.getAVProgramsList() != null && program_result.getAVProgramsList().size() > 0) {
                            if (mProgramList != null) {
                                mProgramList.clear();
                                mProgramList.addAll(program_result.getAVProgramsList().get(0).getAVProgramList());
                                mProgramInfoTextView.setText(mProgramList.get(getCurrentRunningProgram(mProgramList)).getName());
                                Log.d("onDvbSIUpdated()", "mProgramList size: " + mProgramList.size() + " mProgramInfoTextView: " + mProgramInfoTextView.getText());
                            }
                        }

                        program_result = mDvbSIService.getAVProgramList(mCurrentChannelSID_Pip);
                        Log.d("onDvbSIUpdated()", "program_result : " + program_result);
                        if (program_result != null && program_result.getAVProgramsList() != null && program_result.getAVProgramsList().size() > 0) {
                            if (mProgramList_Pip != null) {
                                mProgramList_Pip.clear();
                                mProgramList_Pip.addAll(program_result.getAVProgramsList().get(0).getAVProgramList());
                                mProgramInfoTextView_Pip.setText(mProgramList_Pip.get(getCurrentRunningProgram(mProgramList_Pip)).getName());
                                Log.d("onDvbSIUpdated()", "mProgramList_Pip size: " + mProgramList_Pip.size() + " mProgramInfoTextView_Pip: " + mProgramInfoTextView_Pip.getText());
                            }
                        }
                    }
                });
            } else if (dvbsiType == IBtvNavigator.DVBSI_TYPE_UPDATE_MULTI_CHANNELS) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String extraString = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            extraString = json.getString(IBtvNavigator.DVBSI_JSONOBJECT_INFO);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        MultiViewDvbServiceList multiViewChannelList = mDvbSIService.getMultiViewDvbServiceList();
                        if (multiViewChannelList != null && multiViewChannelList.isReady()) {
                            mMultiViewChannelData = new ArrayList<>(multiViewChannelList.getList());
                        }
                    }
                });
            }

            if (mChannelData == null) {
                AVDvbServiceList channelList = mDvbSIService.getAVDvbServiceList();

                if (channelList != null && channelList.isReady()) {
                    mChannelData = new ArrayList<>(channelList.getList());
                    Log.d(TAG, "onDvbSIUpdated() mChannelData size: " + mChannelData.size());
                    for (AVDvbService service : mChannelData) {
                        Log.i(TAG, "mChannelData - " + service.getCh() + "[" + service.getSid() + "] : " + service.getName());
                    }
                    if (mChannelAdapter != null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mChannelAdapter.setList(mChannelData);
                                mChannelAdapter.notifyDataSetChanged();
                                if (mChannelAdapter_Pip != null) {
                                    mChannelAdapter_Pip.setList(mChannelData);
                                    mChannelAdapter_Pip.notifyDataSetChanged();
                                }

                                if (isPIPShowing) {
                                    setPIP(mCurrentPosition_Pip);
                                } else {
                                    setTV(mCurrentPosition);
                                }
//                                mMultiChannelAdapter.setList(mChannelData);
//                                mMultiChannelAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                }
            }

            Log.d(TAG, "onDvbSIUpdated() mMultiViewChannelData: " + mMultiViewChannelData);
            if(mMultiViewChannelData == null) {
                MultiViewDvbServiceList multiViewChannelList = mDvbSIService.getMultiViewDvbServiceList();
                Log.d(TAG, "onDvbSIUpdated() multiViewChannelList: " + multiViewChannelList);
                if (multiViewChannelList != null && multiViewChannelList.isReady()) {
                    Log.d(TAG, "onDvbSIUpdated() multiViewChannelList size: " + multiViewChannelList.getList().size());
                    mMultiViewChannelData = new ArrayList<>(multiViewChannelList.getList());
                }
            }

            if (mMusicChannelData == null) {
                MusicDvbServiceList musicChannelList = mDvbSIService.getMusicDvbServiceList();
                Log.d(TAG, "onDvbSIUpdated() musicChannelList : " + musicChannelList);
                if (musicChannelList != null && musicChannelList.isReady()) {
                    mMusicChannelData = new ArrayList<>(musicChannelList.getList());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mChannelData != null && mChannelData.get(mCurrentPosition).getChannelType() == IAVDvbService.AVDVBSERVICE_CHANNEL_TYPE_MUSIC_CHANNEL) {
                                setTV(mCurrentPosition);
                            }
                        }
                    });
                }
            }
        }
    };

}