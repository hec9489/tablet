package com.skb.framework.btvplayerui;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.skb.btv.framework.navigator.dvbservice.AVDvbService;

import java.util.List;

public class ChannelAdapter extends BaseAdapter {

    private static final String TAG =  "ChannelAdapter";

    private Context mContext = null;
    private List<AVDvbService> mListData = null;


    public ChannelAdapter(Context mContext) {
        super();
        Log.d(TAG, "ChannelAdapter");
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        Log.d(TAG, "getCount");
        return mListData == null ? 0 : mListData.size();
    }

    @Override
    public Object getItem(int position) {
        Log.d(TAG, "getItem");
        return mListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        Log.d(TAG, "getItemId");
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d(TAG, "getView");

        SubViewHolder holder;

        if (convertView == null) {
            holder = new SubViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listitem_channel, parent, false);

            holder.mText = convertView.findViewById(R.id.tv_title);
            holder.mImage = convertView.findViewById(R.id.iv_folder);

            convertView.setLayoutParams(new ListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 75));
            convertView.setTag(holder);

        } else {
            holder = (SubViewHolder) convertView.getTag();
        }

        String chInfo = "CH " + mListData.get(position).getCh() + "  " + mListData.get(position).getName();

        // specific process
        holder.mText.setText(chInfo);

        return convertView;
    }

    public void remove(int position) {
        Log.d(TAG, "remove");
        //mListData.remove(position);
    }

    public void setList(List<AVDvbService> list) {
        Log.d(TAG, "setList");
        this.mListData = list;
    }

    public List<AVDvbService> getList() {
        return mListData;
    }

    public class SubViewHolder
    {
        public TextView mText;
        public ImageView mImage;
    }

}
