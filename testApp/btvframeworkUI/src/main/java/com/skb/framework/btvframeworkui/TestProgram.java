package com.skb.framework.btvframeworkui;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.skb.btv.framework.navigator.program.AVProgram;
import com.skb.btv.framework.navigator.program.AVPrograms;
import com.skb.btv.framework.log.SLog;

import java.util.ArrayList;

public class TestProgram extends Activity implements View.OnClickListener{

    private  final static String TAG = "TestProgram";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SLog.d(TAG, "onCreate");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_test_program);

        Button program_back = (Button) findViewById(R.id.program_back);
        program_back.setOnClickListener(this);

        ArrayList<String> items1 = new ArrayList<String>();
        ArrayList<String> items2 = new ArrayList<String>();
        ArrayList<String> items3 = new ArrayList<String>();
        ArrayList<String> items4 = new ArrayList<String>();

        ArrayList<AVPrograms> program_items = new ArrayList<AVPrograms>();
        program_items = getIntent().getParcelableArrayListExtra("items");

        //for(Program tempValue : program_items){
        //    if()
         //   items.add(tempValue.getName() +" / " +tempValue.getStartTime());
        //
        // }


        for(AVProgram tempValue : program_items.get(0).getAVProgramList()){
            items1.add(tempValue.getName() +" / " +tempValue.getStartTime());
        }
        TextView tv1 = (TextView)findViewById(R.id.ch_num1);
        tv1.setText("CH : "+program_items.get(0).getSid());
        for(AVProgram tempValue : program_items.get(1).getAVProgramList()){
            items2.add(tempValue.getName() +" / " +tempValue.getStartTime());
        }
        TextView tv2 = (TextView)findViewById(R.id.ch_num2);
        tv2.setText("CH : "+program_items.get(1).getSid());
        for(AVProgram tempValue : program_items.get(2).getAVProgramList()){
            items3.add(tempValue.getName() +" / " +tempValue.getStartTime());
        }
        TextView tv3 = (TextView)findViewById(R.id.ch_num3);
        tv3.setText("CH : "+program_items.get(2).getSid());
        for(AVProgram tempValue : program_items.get(3).getAVProgramList()){
            items4.add(tempValue.getName() +" / " +tempValue.getStartTime());
        }
        TextView tv4 = (TextView)findViewById(R.id.ch_num4);
        tv4.setText("CH : "+program_items.get(3).getSid());
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items1){

            @Override
            public View getView(int position, View convertView, ViewGroup parent)
            {
                View view = super.getView(position, convertView, parent);
                TextView tv = (TextView) view.findViewById(android.R.id.text1);
                tv.setTextColor(Color.BLACK);
                return view;
            }
        };

        ListView listview1 = (ListView) findViewById(R.id.listview1);
        listview1.setAdapter(adapter1);

        ArrayAdapter adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items2){

            @Override
            public View getView(int position, View convertView, ViewGroup parent)
            {
                View view = super.getView(position, convertView, parent);
                TextView tv = (TextView) view.findViewById(android.R.id.text1);
                tv.setTextColor(Color.BLACK);
                return view;
            }
        };

        ListView listview2 = (ListView) findViewById(R.id.listview2);
        listview2.setAdapter(adapter2);

        ArrayAdapter adapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items3){

            @Override
            public View getView(int position, View convertView, ViewGroup parent)
            {
                View view = super.getView(position, convertView, parent);
                TextView tv = (TextView) view.findViewById(android.R.id.text1);
                tv.setTextColor(Color.BLACK);
                return view;
            }
        };

        ListView listview3 = (ListView) findViewById(R.id.listview3);
        listview3.setAdapter(adapter3);

        ArrayAdapter adapter4 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items4){

            @Override
            public View getView(int position, View convertView, ViewGroup parent)
            {
                View view = super.getView(position, convertView, parent);
                TextView tv = (TextView) view.findViewById(android.R.id.text1);
                tv.setTextColor(Color.BLACK);
                return view;
            }
        };

        ListView listview4 = (ListView) findViewById(R.id.listview4);
        listview4.setAdapter(adapter4);
    }

    @Override
    public void onClick(View v) {
        SLog.d(TAG, "onClick" +v.getId());
        switch (v.getId()){
            case R.id.program_back:
                finish();
                break;
        }
    }
}
