package com.skb.framework.btvframeworkui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.skb.btv.framework.core.BtvFramework;
import com.skb.btv.framework.core.IBtvFramework;
import com.skb.btv.framework.avcontrol.OnHdmiPluggedListener;
import com.skb.btv.framework.security.OnCasInfoListener;
import com.skb.btv.framework.log.SLog;
import com.skb.btv.framework.avcontrol.AVControlManager;
import com.skb.btv.framework.security.CasManager;
import com.skb.btv.framework.network.BtvNetworkInfo;
import com.skb.btv.framework.network.NetworkManager;
import com.skb.btv.framework.network.OnNetworkCheckListener;
import com.skb.btv.framework.network.OnQIListener;
import com.skb.btv.framework.peripheral.PeripheralManager;
import com.skb.btv.framework.system.CompanionData;
import com.skb.btv.framework.system.SystemManager;

import static android.media.AudioManager.FLAG_SHOW_UI;
import static com.skb.btv.framework.core.IBtvFramework.BTVFRM_CONST_POWER_STATE_RUN;
import static com.skb.btv.framework.core.IBtvFramework.BTVFRM_CONST_POWER_STATE_SLEEP;

public class TestTvservice extends Activity implements View.OnClickListener, AudioManager.OnAudioFocusChangeListener {

    private static final String TAG =  "TestTvservice";
	private static final String TESTTAG =  " BtvTest TestTvservice";
    private static final String PREFIX =  " TVSERVICE::";

    BtvFramework btvFramework = null;
    AVControlManager mAVControlManager = null;
    CasManager mCasManager = null;
    NetworkManager mNetworkManager = null;
    PeripheralManager mPeripheralManager = null;
    SystemManager mSystemManager = null;
    AudioFocusRequest mAudioFocusRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_test_tvservice);

        btvFramework = BtvFramework.getInstance(this);
        mAVControlManager = (AVControlManager)btvFramework.getBtvService(BtvFramework.AVCONTROL_SERVICE);
        mCasManager = (CasManager)btvFramework.getBtvService(BtvFramework.CAS_SERVICE);
        mNetworkManager = (NetworkManager)btvFramework.getBtvService(BtvFramework.NETWORK_SERVICE);
        mPeripheralManager = (PeripheralManager)btvFramework.getBtvService(BtvFramework.PERIPHERAL_SERVICE);
        mSystemManager = (SystemManager)btvFramework.getBtvService(BtvFramework.SYSTEM_SERVICE);

        Button tvservice_back = (Button) findViewById(R.id.tvservice_back);
        tvservice_back.setOnClickListener(this);


        // AVControlService
        final ArrayAdapter<String> avcontrolArrayList = new ArrayAdapter<String>(this, R.layout.custom_spinner_dropdown_item);
        avcontrolArrayList.add("getDisplayResolution");
        avcontrolArrayList.add("getDisplaySupportedResolution");
        avcontrolArrayList.add("setDisplayResolution");
        avcontrolArrayList.add("getHDCPStatus");
        avcontrolArrayList.add("setHDCPStatus");
        avcontrolArrayList.add("getCECStatus");
        avcontrolArrayList.add("setCECStatus");
        avcontrolArrayList.add("getHDCPVersion");
        avcontrolArrayList.add("setHDCPVersion");
        avcontrolArrayList.add("getHDRDisplayMode");
        avcontrolArrayList.add("setHDRDisplayMode");
        avcontrolArrayList.add("getHDRDisplaySupported");
        avcontrolArrayList.add("setHdmiPluggedListener");
        avcontrolArrayList.add("getHdmiEdid");
        avcontrolArrayList.add("getVideoScalingMode");
        avcontrolArrayList.add("setVideoScalingMode");
        avcontrolArrayList.add("getGraphicBrightness");
        avcontrolArrayList.add("setGraphicBrightness");
        avcontrolArrayList.add("getGraphicContrast");
        avcontrolArrayList.add("setGraphicContrast");
        avcontrolArrayList.add("getGraphicHue");
        avcontrolArrayList.add("setGraphicHue");
        avcontrolArrayList.add("getGraphicSaturation");
        avcontrolArrayList.add("setGraphicSaturation");
        avcontrolArrayList.add("getVideoAspectRatio");
        avcontrolArrayList.add("setVideoAspectRatio");
        avcontrolArrayList.add("enableMic");
        avcontrolArrayList.add("getAudioOutputMode");
        avcontrolArrayList.add("setAudioOutputMode");
        avcontrolArrayList.add("getMuteMode");
        avcontrolArrayList.add("setMuteMode");
        avcontrolArrayList.add("changeAudioLanguage");
        avcontrolArrayList.add("setSoundEffect");
        avcontrolArrayList.add("getSoundEffect");
        avcontrolArrayList.add("isStreamMute");
        avcontrolArrayList.add("setStreamMute");
        avcontrolArrayList.add("getStreamVolume");
        avcontrolArrayList.add("setStreamVolume");
        avcontrolArrayList.add("isBluetoothA2dpOn");
        avcontrolArrayList.add("requestAudioFocus");
        avcontrolArrayList.add("abandonAudioFocusRequest");
        Button avcontrol_spinner = (Button) findViewById(R.id.avcontrol_spinner);
        avcontrol_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog("AVControlService", avcontrolArrayList, avcontrolOnItemSelectedListener);
            }
        });

        // CasService
        final ArrayAdapter<String> casArrayList = new ArrayAdapter<String>(this, R.layout.custom_spinner_dropdown_item);
        casArrayList.add("getCasInfo");
        casArrayList.add("clearCasInfo");
        Button cas_spinner = (Button) findViewById(R.id.cas_spinner);
        cas_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog("CasService", casArrayList,  casOnItemSelectedListener);
            }
        });

        // NetworkService
        final ArrayAdapter<String> networkArrayList = new ArrayAdapter<String>(this, R.layout.custom_spinner_dropdown_item);
        networkArrayList.add("getMacAddress");
        networkArrayList.add("getNetworkInfo");
        networkArrayList.add("setNetworkInfo");
        networkArrayList.add("CheckNetwork");
        networkArrayList.add("setNetworkSleep");
        networkArrayList.add("getQIData");
        Button network_spinner = (Button) findViewById(R.id.network_spinner);
        network_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog("NetworkService", networkArrayList,  networkOnItemSelectedListener);
            }
        });

        // PeripheralService
        final ArrayAdapter<String> peripheralArrayList = new ArrayAdapter<String>(this, R.layout.custom_spinner_dropdown_item);
        peripheralArrayList.add("setLedState");
        peripheralArrayList.add("setLedStateForVolume");
        peripheralArrayList.add("setLedStateBeamformingDegree");
        peripheralArrayList.add("isNuguMicEnabled");
        peripheralArrayList.add("enableNuguMic");
        Button peripheral_spinner = (Button) findViewById(R.id.peripheral_spinner);
        peripheral_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog("PeripheralService", peripheralArrayList,  peripheralOnItemSelectedListener);
            }
        });

        // SystemService
        final ArrayAdapter<String> systemArrayList = new ArrayAdapter<String>(this, R.layout.custom_spinner_dropdown_item);
        systemArrayList.add("getFirmwareUpdateDate");
        systemArrayList.add("getFirmwareVersion");
        systemArrayList.add("getFullFirmwareVersion");
        systemArrayList.add("getModelName");
        systemArrayList.add("initCertification");
        systemArrayList.add("factoryReset");
        systemArrayList.add("setACLUnlock");
        systemArrayList.add("getCompanionProperties");
        systemArrayList.add("getCompanionProperty");
        systemArrayList.add("getCompanionPropertyInt");
        systemArrayList.add("setCompanionProperties");
        systemArrayList.add("setCompanionProperty");
        systemArrayList.add("setCompanionPropertyInt");
        systemArrayList.add("setMousePointerVisible");
        systemArrayList.add("getFreeDiskSpace");
        systemArrayList.add("getTotalDiskSpace");
        systemArrayList.add("optimizeMemory");
        systemArrayList.add("reboot");
        systemArrayList.add("rebootRecovery");
        systemArrayList.add("sleep");
        systemArrayList.add("wakeup");
        systemArrayList.add("powerOff");
        systemArrayList.add("tvPowerOff");
        systemArrayList.add("getPowerState");
        systemArrayList.add("passiveStandby");
        Button system_spinner = (Button)findViewById(R.id.system_spinner);
        system_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog("SystemService", systemArrayList, systemOnItemSelectedListener);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvservice_back:
                finish();
                break;
        }
    }

    /**
     * avcontrol
     */
    AdapterView.OnItemClickListener avcontrolOnItemSelectedListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String itemType = "AVControlService";
            String resultInfo = "";
            boolean bInit = false;
            String selectedItems = parent.getItemAtPosition(position).toString();
            if(TextUtils.isEmpty(selectedItems) || (selectedItems=="AVControlService"))
                bInit = true;

            if (!bInit)
                SLog.d(TAG, PREFIX + itemType + " START : " + selectedItems);

            switch (selectedItems){
                case "getDisplayResolution": {
                    int result = mAVControlManager.getAVControlServiceDisplayOutput().getDisplayResolution();
                    resultInfo = String.format("RESULT = %d", result);
                }
                break;

                case "getDisplaySupportedResolution": {
                    String result = mAVControlManager.getAVControlServiceDisplayOutput().getDisplaySupportedResolution();
                    resultInfo = String.format("RESULT[TODO] = %s", result);
                }
                break;

                case "setDisplayResolution": {
                    int resultTemp = mAVControlManager.getAVControlServiceDisplayOutput().getDisplayResolution();
                    boolean result = mAVControlManager.getAVControlServiceDisplayOutput().setDisplayResolution((resultTemp==1)?0:1);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "getHDCPStatus": {
                    boolean result = mAVControlManager.getAVControlServiceDisplayOutput().getHDCPStatus();
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "setHDCPStatus": {
                    resultInfo = String.format("RESULT = SKIP");
                    /*
                    boolean resultTemp = mAVControlManager.getAVControlServiceDisplayOutput().getHDCPStatus();
                    boolean result = mAVControlManager.getAVControlServiceDisplayOutput().setHDCPStatus(resultTemp);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                    */
                }
                break;

                case "getCECStatus": {
                    boolean result = mAVControlManager.getAVControlServiceDisplayOutput().getCECStatus();
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "setCECStatus": {
                    boolean resultTemp = mAVControlManager.getAVControlServiceDisplayOutput().getCECStatus();
                    boolean result = mAVControlManager.getAVControlServiceDisplayOutput().setCECStatus(resultTemp);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "getHDCPVersion": {
                    int result = mAVControlManager.getAVControlServiceDisplayOutput().getHDCPVersion();
                    resultInfo = String.format("RESULT = %d", result);
                }
                break;

                case "setHDCPVersion": {
                    resultInfo = String.format("RESULT = SKIP[reboot]");
                    /*
                    int resultTemp = mAVControlManager.getAVControlServiceDisplayOutput().getHDCPVersion();
                    boolean result = mAVControlManager.getAVControlServiceDisplayOutput().setHDCPVersion(resultTemp);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                    */
                }
                break;

                case "getHDRDisplayMode": {
                    int result = mAVControlManager.getAVControlServiceDisplayOutput().getHDRDisplayMode();
                    resultInfo = String.format("RESULT = %d", result);
                }
                break;

                case "setHDRDisplayMode": {
                    int resultTemp = mAVControlManager.getAVControlServiceDisplayOutput().getHDRDisplayMode();
                    boolean result = mAVControlManager.getAVControlServiceDisplayOutput().setHDRDisplayMode((resultTemp==1)?0:1);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "getHDRDisplaySupported": {
                    boolean result = mAVControlManager.getAVControlServiceDisplayOutput().getHDRDisplaySupported();
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "setHdmiPluggedListener": {
                    OnHdmiPluggedListener tListener = new OnHdmiPluggedListener() {
                        @Override
                        public void onHdmiPluggedUpdate(int pluggedState) {
                            SLog.d(TESTTAG, PREFIX + "OnHdmiPluggedListener <<<< onHdmiPluggedUpdate : " + pluggedState);

                        }
                    };
                    boolean result = mAVControlManager.getAVControlServiceDisplayOutput().setHdmiPluggedListener(tListener);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "getHdmiEdid": {
                    String result = mAVControlManager.getAVControlServiceDisplayOutput().getHdmiEdid();
                    resultInfo = String.format("RESULT = %s", result);
                }
                break;

                case "getVideoScalingMode": {
                    int result = mAVControlManager.getAVControlServiceVideoCompositor().getVideoScalingMode();
                    resultInfo = String.format("RESULT = %d", result);
                }
                break;

                case "setVideoScalingMode": {
                    int resultTemp = mAVControlManager.getAVControlServiceVideoCompositor().getVideoScalingMode();
                    boolean result = mAVControlManager.getAVControlServiceVideoCompositor().setVideoScalingMode(resultTemp);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "getGraphicBrightness": {
                    int result = mAVControlManager.getAVControlServiceVideoCompositor().getGraphicBrightness();
                    resultInfo = String.format("RESULT = %d", result);
                }
                break;

                case "setGraphicBrightness": {
                    int resultTemp = mAVControlManager.getAVControlServiceVideoCompositor().getGraphicBrightness();
                    boolean result = mAVControlManager.getAVControlServiceVideoCompositor().setGraphicBrightness(resultTemp);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "getGraphicContrast": {
                    int result = mAVControlManager.getAVControlServiceVideoCompositor().getGraphicContrast();
                    resultInfo = String.format("RESULT = %d", result);
                }
                break;

                case "setGraphicContrast": {
                    int resultTemp = mAVControlManager.getAVControlServiceVideoCompositor().getGraphicContrast();
                    boolean result = mAVControlManager.getAVControlServiceVideoCompositor().setGraphicContrast(resultTemp);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "getGraphicHue": {
                    int result = mAVControlManager.getAVControlServiceVideoCompositor().getGraphicHue();
                    resultInfo = String.format("RESULT = %d", result);
                }
                break;

                case "setGraphicHue": {
                    int resultTemp = mAVControlManager.getAVControlServiceVideoCompositor().getGraphicHue();
                    boolean result = mAVControlManager.getAVControlServiceVideoCompositor().setGraphicHue(resultTemp);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "getGraphicSaturation": {
                    int result = mAVControlManager.getAVControlServiceVideoCompositor().getGraphicSaturation();
                    resultInfo = String.format("RESULT = %d", result);
                }
                break;

                case "setGraphicSaturation": {
                    int resultTemp = mAVControlManager.getAVControlServiceVideoCompositor().getGraphicSaturation();
                    boolean result = mAVControlManager.getAVControlServiceVideoCompositor().setGraphicSaturation(resultTemp);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "getVideoAspectRatio": {
                    int result = mAVControlManager.getAVControlServiceVideoCompositor().getVideoAspectRatio();
                    resultInfo = String.format("RESULT = %d", result);
                }
                break;

                case "setVideoAspectRatio": {
                    int resultTemp = mAVControlManager.getAVControlServiceVideoCompositor().getVideoAspectRatio();
                    boolean result = mAVControlManager.getAVControlServiceVideoCompositor().setVideoAspectRatio(resultTemp);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "enableMic": {
                    boolean result = mAVControlManager.getAVControlServiceAudioMixer().enableMic(false);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "getAudioOutputMode": {
                    boolean result = mAVControlManager.getAVControlServiceAudioMixer().getAudioOutputMode();
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "setAudioOutputMode": {
                    boolean resultTemp = mAVControlManager.getAVControlServiceAudioMixer().getAudioOutputMode();
                    boolean result = mAVControlManager.getAVControlServiceAudioMixer().setAudioOutputMode(!resultTemp);
                    boolean resultNew = mAVControlManager.getAVControlServiceAudioMixer().getAudioOutputMode();
                    mAVControlManager.getAVControlServiceAudioMixer().setAudioOutputMode(!resultTemp);
                    resultInfo = String.format("RESULT = %s", (resultTemp!=resultNew)?"true":"false");
                }
                break;

                case "getMuteMode": {
                    boolean result = mAVControlManager.getAVControlServiceAudioMixer().getMuteMode();
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "setMuteMode": {
                    boolean resultTemp = mAVControlManager.getAVControlServiceAudioMixer().getMuteMode();
                    boolean result = mAVControlManager.getAVControlServiceAudioMixer().setMuteMode(resultTemp);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "changeAudioLanguage": {
                    int pid = 0;
                    int codec = 0;
                    String strUri = "";
                    boolean result = mAVControlManager.getAVControlServiceAudioMixer().changeAudioLanguage(pid, codec, strUri);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "setSoundEffect": {
                    int resultTemp = mAVControlManager.getAVControlServiceAudioMixer().getSoundEffect();
                    boolean result = mAVControlManager.getAVControlServiceAudioMixer().setSoundEffect(resultTemp);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "getSoundEffect": {
                    int result = mAVControlManager.getAVControlServiceAudioMixer().getSoundEffect();
                    resultInfo = String.format("RESULT = %d", result);
                }
                break;

                case "isStreamMute": {
                    boolean result = mAVControlManager.getAVControlServiceAudioMixer().isStreamMute();
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "setStreamMute": {
                    mAVControlManager.getAVControlServiceAudioMixer().setStreamMute(
                            !mAVControlManager.getAVControlServiceAudioMixer().isStreamMute(), FLAG_SHOW_UI);
                    resultInfo = String.format("Set Mute = %s", mAVControlManager.getAVControlServiceAudioMixer().isStreamMute()?"true":"false");
                }
                break;

                case "getStreamVolume": {
                    int result = mAVControlManager.getAVControlServiceAudioMixer().getStreamVolume();
                    resultInfo = String.format("RESULT = %d", result);
                }
                break;

                case "setStreamVolume": {
                    int vol = mAVControlManager.getAVControlServiceAudioMixer().getStreamVolume();
                    if (vol == 0) {
                        vol = 20;
                    } else if (vol <= 20) {
                        vol = 40;
                    } else if (vol <= 40) {
                        vol = 0;
                    }
                    mAVControlManager.getAVControlServiceAudioMixer().setStreamVolume(vol, FLAG_SHOW_UI);
                    resultInfo = String.format("RESULT = %d", mAVControlManager.getAVControlServiceAudioMixer().getStreamVolume());
                }
                break;

                case "isBluetoothA2dpOn": {
                    boolean result = mAVControlManager.getAVControlServiceAudioMixer().isBluetoothA2dpOn();
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "requestAudioFocus": {
                    AudioAttributes playbackAttributes = new AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_MEDIA)
                            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                            .build();
                    mAudioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                            .setAudioAttributes(playbackAttributes)
                            .setAcceptsDelayedFocusGain(true)
                            .setOnAudioFocusChangeListener(TestTvservice.this)
                            .build();
                    int result = mAVControlManager.getAVControlServiceAudioMixer().requestAudioFocus(mAudioFocusRequest);
                    resultInfo = String.format("RESULT = %d (fail : 0, granted : 1)", result);
                }
                break;

                case "abandonAudioFocusRequest": {
                    if (mAudioFocusRequest != null) {
                        int result = mAVControlManager.getAVControlServiceAudioMixer().abandonAudioFocusRequest(mAudioFocusRequest);
                        resultInfo = String.format("RESULT = %d (fail : 0, granted : 1)", result);
                    } else {
                        resultInfo = "requestAudioFocus first";
                    }
                }
                break;

                default:
                    resultInfo = String.format("RESULT = Undefined");
                    break;
            }

            if (!bInit) {
                Toast.makeText(getApplicationContext(), resultInfo, Toast.LENGTH_SHORT).show();
                SLog.d(TAG, PREFIX + itemType + " END : " + selectedItems + " : " + resultInfo);
            }
        }
    };

    @Override
    public void onAudioFocusChange(int focusChange) {
        Toast.makeText(getApplicationContext(), "onAudioFocusChange : " + focusChange, Toast.LENGTH_SHORT).show();
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                break;
        }
    }

    /**
     * cas
     */
    AdapterView.OnItemClickListener casOnItemSelectedListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String itemType = "CasService";
            String resultInfo = "";
            boolean bInit = false;
            String selectedItems = parent.getItemAtPosition(position).toString();
            if(TextUtils.isEmpty(selectedItems) || (selectedItems=="CasService"))
                bInit = true;

            if (!bInit)
                SLog.d(TAG, PREFIX + itemType + " START : " + selectedItems);

            switch (selectedItems){
                case "getCasInfo": {
                    OnCasInfoListener tlistener = new OnCasInfoListener() {
                        @Override
                        public void onCasInfoUpdate(String info) {
                            SLog.d(TESTTAG, PREFIX + "OnCasInfoListener <<<< onCasInfoUpdate : " + info);
                        }
                    };

                    boolean result = mCasManager.getCasServiceCasInfo().openCasListener(tlistener);

                    if(result){
                        mCasManager.getCasServiceCasInfo().getCasInfo();

                        result =  mCasManager.getCasServiceCasInfo().closeCasListener(tlistener);
                    }

                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "clearCasInfo": {
                    mCasManager.getCasServiceCasInfo().clearCasInfo();
                    resultInfo = String.format("RESULT = %s", "void");

                }
                break;

                default:
                    resultInfo = String.format("RESULT = Undefined");
                    break;
            }

            if (!bInit) {
                Toast.makeText(getApplicationContext(), resultInfo, Toast.LENGTH_SHORT).show();
                SLog.d(TAG, PREFIX + itemType + " END : " + selectedItems + " : " + resultInfo);
            }
        }
    };

    /**
     * network
     */
    AdapterView.OnItemClickListener networkOnItemSelectedListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String itemType = "NetworkService";
            String resultInfo = "";
            boolean bInit = false;
            String selectedItems = parent.getItemAtPosition(position).toString();
            if(TextUtils.isEmpty(selectedItems) || (selectedItems=="NetworkService"))
                bInit = true;

            if (!bInit)
                SLog.d(TAG, PREFIX + itemType + " START : " + selectedItems);

            switch (selectedItems){
                case "getMacAddress": {
                    String result = mNetworkManager.getNetworkServiceEthernet().getMacAddress();
                    resultInfo = String.format("RESULT = %s", result);
                }
                break;

                case "getNetworkInfo": {
                    BtvNetworkInfo result = mNetworkManager.getNetworkServiceEthernet().getNetworkInfo();
                    if(result != null) {
                        resultInfo = String.format("type = %s, ip = %s, gateway = %s, subnet = %s, dns1 = %s, dns2 = %s"
                                , result.getType(), result.getIp(), result.getGateway(), result.getSubnet(), result.getDns1(), result.getDns2());
                    }
                    else{
                        resultInfo = String.format("RESULT = %s", "null");
                    }
                }
                break;

                case "setNetworkInfo": {
                    BtvNetworkInfo netowrkInfo = mNetworkManager.getNetworkServiceEthernet().getNetworkInfo();
                    String info = String.format("type = %s, ip = %s, gateway = %s, subnet = %s, dns1 = %s, dns2 = %s"
                            , netowrkInfo.getType(), netowrkInfo.getIp(), netowrkInfo.getGateway(), netowrkInfo.getSubnet(), netowrkInfo.getDns1(), netowrkInfo.getDns2());
                    SLog.d(TESTTAG, PREFIX + "     : REF = " + info);

                    boolean result = false;
                    if(netowrkInfo != null)
                        result = mNetworkManager.getNetworkServiceEthernet().setNetworkInfo(netowrkInfo);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "CheckNetwork": {
                    OnNetworkCheckListener tlistener = new OnNetworkCheckListener() {
                        @Override
                        public void onNetworkUpdate(int networkUpdate) {
                            SLog.d(TESTTAG, PREFIX + "OnNetworkCheckListener <<<< onNetworkUpdate : " + networkUpdate);
                            if(networkUpdate== IBtvFramework.BTVFRM_CONST_NETWORK_OK)
                            {
                            }
                        }
                    };

                    BtvNetworkInfo networkInfo = mNetworkManager.getNetworkServiceEthernet().getNetworkInfo();
                    String info = String.format("type = %s, ip = %s, gateway = %s, subnet = %s, dns1 = %s, dns2 = %s"
                            , networkInfo.getType(), networkInfo.getIp(), networkInfo.getGateway(), networkInfo.getSubnet(), networkInfo.getDns1(), networkInfo.getDns2());
                    SLog.d(TESTTAG, PREFIX + "     : REF = " + info);

                    if(networkInfo != null){
                        mNetworkManager.getNetworkServiceEthernet().startCheckNetwork(0, "", 0, tlistener);
                        try {
                            Thread.sleep(3000);
                        }
                        catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        mNetworkManager.getNetworkServiceEthernet().stopCheckNetwork(tlistener);
                        try {
                            Thread.sleep(1000);
                        }
                        catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    resultInfo = String.format("RESULT =  %s", "void");
                }
                break;

                case "getQIData": {
                    OnQIListener tlistener = new OnQIListener() {
                        @Override
                        public void onQiUpdate(String qiData) {
                            SLog.d(TESTTAG, PREFIX + "OnQIListener <<<< onQiUpdate : " + qiData);
                        }
                    };

                    int type = 106;
                    boolean result = mNetworkManager.getNetworkServiceQulityInfo().startQiData(type,tlistener);

                    try {
                        Thread.sleep(1000);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    result = mNetworkManager.getNetworkServiceQulityInfo().stopQiData(tlistener);

                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                default:
                    resultInfo = String.format("RESULT = Undefined");
                    break;
            }


            if (!bInit) {
                Toast.makeText(getApplicationContext(), resultInfo, Toast.LENGTH_SHORT).show();
                SLog.d(TAG, PREFIX + itemType + " END : " + selectedItems + " : " + resultInfo);
            }
        }
    };

    /**
     * peripheral
     */
    AdapterView.OnItemClickListener peripheralOnItemSelectedListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String itemType = "PeripheralService";
            String resultInfo = "";
            boolean bInit = false;
            String selectedItems = parent.getItemAtPosition(position).toString();
            if(TextUtils.isEmpty(selectedItems) || (selectedItems=="PeripheralService"))
                bInit = true;

            if (!bInit)
                SLog.d(TAG, PREFIX + itemType + " START : " + selectedItems);

            switch (selectedItems){
                case "setLedState": {
                    boolean result = mPeripheralManager.getPeripheralServiceLEDControl().setLedState(IBtvFramework.BTVFRM_CONST_LED_TYPE_STATE,IBtvFramework.BTVFRM_CONST_LED_STATE_BLINK);
                    resultInfo = String.format("RESULT = %s", result?"true":"false" );
                }
                break;

                case "setLedStateForVolume": {
                    boolean result = mPeripheralManager.getPeripheralServiceLEDControl().setLedStateForVolume(IBtvFramework.BTVFRM_CONST_LED_TYPE_AILED,IBtvFramework.BTVFRM_CONST_LED_STATE_SPEAKER_VOL_UP, 1);
                    resultInfo = String.format("RESULT = %s", result?"true":"false" );
                }
                break;

                case "setLedStateBeamformingDegree": {
                    boolean result = mPeripheralManager.getPeripheralServiceLEDControl().setLedStateBeamformingDegree(IBtvFramework.BTVFRM_CONST_LED_TYPE_AILED, IBtvFramework.BTVFRM_CONST_LED_STATE_LISTEN_ACTIVE, 0);
                    resultInfo = String.format("RESULT = %s", result?"true":"false" );
                }
                break;

                case "isNuguMicEnabled": {
                    boolean result = mPeripheralManager.getPeripheralServiceNUGUDeviceControl().isNuguMicEnabled();
                    resultInfo = String.format("RESULT = %s", result?"true":"false" );
                }
                break;

                case "enableNuguMic": {
                    boolean resultTemp = mPeripheralManager.getPeripheralServiceNUGUDeviceControl().isNuguMicEnabled();
                    boolean result = mPeripheralManager.getPeripheralServiceNUGUDeviceControl().enableNuguMic(resultTemp?1:0);
                    resultInfo = String.format("RESULT = %s", result?"true":"false" );
                }
                break;

                default:
                    resultInfo = String.format("RESULT = Undefined");
                    break;
            }

            if (!bInit) {
                Toast.makeText(getApplicationContext(), resultInfo, Toast.LENGTH_SHORT).show();
                SLog.d(TAG, PREFIX + itemType + " END : " + selectedItems + " : " + resultInfo);
            }
        }
    };

    /**
     * system
     */
    AdapterView.OnItemClickListener systemOnItemSelectedListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String itemType = "SystemService";
            String resultInfo = "";
            boolean bInit = false;
            String selectedItems = parent.getItemAtPosition(position).toString();
            if(TextUtils.isEmpty(selectedItems) || (selectedItems=="SystemService"))
                bInit = true;

            if (!bInit)
                SLog.d(TAG, PREFIX + itemType + " START : " + selectedItems);

            switch (selectedItems){
                case "getFirmwareUpdateDate": {
                    String result = mSystemManager.getSystemServiceDeviceInfo().getFirmwareUpdateDate();
                    resultInfo = String.format("RESULT = %s", result);
                }
                break;

                case "getFirmwareVersion": {
                    String result = mSystemManager.getSystemServiceDeviceInfo().getFirmwareVersion();
                    resultInfo = String.format("RESULT = %s", result);
                }
                break;

                case "getFullFirmwareVersion": {
                    String result = mSystemManager.getSystemServiceDeviceInfo().getFullFirmwareVersion();
                    resultInfo = String.format("RESULT = %s", result);
                }
                break;

                case "getModelName": {
                    String result = mSystemManager.getSystemServiceDeviceInfo().getModelName();
                    resultInfo = String.format("RESULT = %s", result);
                }
                break;

                case "initCertification": {
                    resultInfo = String.format("RESULT = SKIP");
                    /*
                    boolean result = mSystemManager.getSystemServiceDeviceInfo().initCertification();
                    resultInfo = String.format("RESULT = %s", result?"true":"false" );
                    */
                }
                break;

                case "factoryReset": {
                    resultInfo = String.format("RESULT = SKIP");
                    /*
                    boolean result = mSystemManager.getSystemServiceDeviceInfo().factoryReset();
                    resultInfo = String.format("RESULT = %s", result?"true":"false" );
                    */
                }
                break;

                case "setACLUnlock": {
                    mSystemManager.getSystemServiceDeviceInfo().setACLUnlock();
                    resultInfo = String.format("RESULT[TODO] = %s", "void" );
                }
                break;

                case "getCompanionProperties": {
                    CompanionData companionData = mSystemManager.getSystemServiceDeviceInfo().getCompanionProperties();
                    resultInfo = String.format("ChannelName = %s, ChannelNum = %d, GenreCode = %d, PlayMode = %d, WatchingTV = %d"
                            , companionData.getChannelName(), companionData.getChannelNum(), companionData.getGenreCode(), companionData.getPlayMode(), companionData.getWatchingTV());
                }
                break;

                case "getCompanionProperty": {
                    String result = mSystemManager.getSystemServiceDeviceInfo().getCompanionProperty(IBtvFramework.COMPANION_PROP_CHANNELNAME);
                    resultInfo = String.format("RESULT = %s", result );
                }
                break;

                case "getCompanionPropertyInt": {
                    int result = mSystemManager.getSystemServiceDeviceInfo().getCompanionPropertyInt(IBtvFramework.COMPANION_PROP_CHANNELNUM);
                    resultInfo = String.format("RESULT = %d", result );
                }
                break;

                case "setCompanionProperties": {
                    CompanionData companionData = mSystemManager.getSystemServiceDeviceInfo().getCompanionProperties();
                    boolean result = mSystemManager.getSystemServiceDeviceInfo().setCompanionProperties(companionData);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "setCompanionProperty": {
                    String resultTemp = mSystemManager.getSystemServiceDeviceInfo().getCompanionProperty(IBtvFramework.COMPANION_PROP_CHANNELNAME);
                    boolean result = mSystemManager.getSystemServiceDeviceInfo().setCompanionProperty(IBtvFramework.COMPANION_PROP_CHANNELNAME, "Test");
                    mSystemManager.getSystemServiceDeviceInfo().getCompanionProperty(IBtvFramework.COMPANION_PROP_CHANNELNAME);
                    mSystemManager.getSystemServiceDeviceInfo().setCompanionProperty(IBtvFramework.COMPANION_PROP_CHANNELNAME, resultTemp);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "setCompanionPropertyInt": {
                    int resultTemp = mSystemManager.getSystemServiceDeviceInfo().getCompanionPropertyInt(IBtvFramework.COMPANION_PROP_CHANNELNUM);
                    boolean result = mSystemManager.getSystemServiceDeviceInfo().setCompanionPropertyInt(IBtvFramework.COMPANION_PROP_CHANNELNUM, resultTemp+1);
                    mSystemManager.getSystemServiceDeviceInfo().getCompanionPropertyInt(IBtvFramework.COMPANION_PROP_CHANNELNUM);
                    mSystemManager.getSystemServiceDeviceInfo().setCompanionPropertyInt(IBtvFramework.COMPANION_PROP_CHANNELNUM, resultTemp);
                    resultInfo = String.format("RESULT = %s", result?"true":"false");
                }
                break;

                case "setMousePointerVisible": {
                    mSystemManager.getSystemServiceDeviceInfo().setMousePointerVisible(true);

                    try {
                        Thread.sleep(3000);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    mSystemManager.getSystemServiceDeviceInfo().setMousePointerVisible(false);

                    try {
                        Thread.sleep(3000);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    mSystemManager.getSystemServiceDeviceInfo().setMousePointerVisible(true);

                    resultInfo = String.format("RESULT = %s", "void");
                }
                break;

                case "getFreeDiskSpace": {
                    String result = mSystemManager.getSystemServiceStorage().getFreeDiskSpace();
                    resultInfo = String.format("RESULT = %s", result);
                }
                break;

                case "getTotalDiskSpace": {
                    String result = mSystemManager.getSystemServiceStorage().getTotalDiskSpace();
                    resultInfo = String.format("RESULT = %s", result);
                }
                break;

                case "optimizeMemory": {
                    mSystemManager.getSystemServiceMemory().optimizeMemory();
                    resultInfo = String.format("RESULT = %s", "void" );
                }
                break;

                case "reboot": {
                    resultInfo = String.format("RESULT = SKIP");
                    /*
                    boolean result = mSystemManager.getSystemServicePower().reboot();
                    resultInfo = String.format("RESULT = %s", result?"true":"false" );
                    */
                }
                break;

                case "rebootRecovery": {
                    resultInfo = String.format("RESULT = SKIP");
                    /*
                    boolean result = mSystemManager.getSystemServicePower().rebootRecovery("");
                    resultInfo = String.format("RESULT = %s", result?"true":"false" );
                    */
                }
                break;

                case "sleep": {
                    boolean result = mSystemManager.getSystemServicePower().sleep();
                    resultInfo = String.format("RESULT = %s", result?"true":"false" );
                }
                break;

                case "wakeup": {
                   boolean result = mSystemManager.getSystemServicePower().wakeup();
                   resultInfo = String.format("RESULT = %s", result?"true":"false" );
                }
                break;

                case "powerOff": {
                    resultInfo = String.format("RESULT = SKIP");
                    /*
                    boolean result = mSystemManager.getSystemServicePower().powerOff(2);
                    resultInfo = String.format("RESULT = %s", result?"true":"false" );
                    */
                }
                break;

                case "tvPowerOff": {
                    resultInfo = String.format("RESULT = SKIP");
                    /*
                    boolean result = mSystemManager.getSystemServicePower().tvPowerOff();
                    resultInfo = String.format("RESULT = %s", result?"true":"false" );
                    */
                }
                break;

                case "getPowerState" : {
                    int result = mSystemManager.getSystemServicePower().getPowerState();
                    resultInfo = String.format("RESULT = %s"
                            , result == BTVFRM_CONST_POWER_STATE_RUN ? "RUN state" :
                                    result == BTVFRM_CONST_POWER_STATE_SLEEP ? "SLEEP state" : "INVALID");
                }
                break;

                case "passiveStandby" : {
                    boolean result = mSystemManager.getSystemServicePower().passiveStandby(1);
                    resultInfo = String.format("RESULT = %s", result?"true":"false" );
                }
                break;

                default:
                    resultInfo = String.format("RESULT = Undefined");
                    break;
            }

            if (!bInit) {
                Toast.makeText(getApplicationContext(), resultInfo, Toast.LENGTH_SHORT).show();
                SLog.d(TAG, PREFIX + itemType + " END : " + selectedItems + " : " + resultInfo);
            }
        }
    };

    private void showAlertDialog(String title, final ArrayAdapter<String> listItems, AdapterView.OnItemClickListener listener){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);

        int checkedItem = 0; //this will checked the item when user open the dialog
        builder.setAdapter(listItems, null);
        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getListView().setOnItemClickListener(listener);
    }
}
