package com.skb.framework.btvframeworkui;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.skb.btv.framework.navigator.program.AVProgram;
import com.skb.btv.framework.log.SLog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

//import android.sptek.stb.servicecore.si.program.ProgramInfo;

public class ProgramAdapter extends BaseAdapter {

    private static final String TAG =  "ProgramAdapter";

    private Context mContext = null;
    private List<AVProgram> mListData = null;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.KOREAN);


    public ProgramAdapter(Context mContext) {
        super();
        SLog.d(TAG, "ProgramAdapter");
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        SLog.d(TAG, "getCount");
        return mListData == null ? 0 : mListData.size();
    }

    @Override
    public Object getItem(int position) {
        SLog.d(TAG, "getItem");
        return mListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        SLog.d(TAG, "getItemId");
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SLog.d(TAG, "getView");

        SubViewHolder holder;

        if (convertView == null) {
            holder = new SubViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listitem_channel, null);

            holder.mText = (TextView) convertView.findViewById(R.id.tv_title);
            holder.mImage = (ImageView) convertView.findViewById(R.id.iv_folder);

            convertView.setLayoutParams(new ListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 75));
            convertView.setTag(holder);

        } else {
            holder = (SubViewHolder) convertView.getTag();
        }

        Date starttime = mListData.get(position).getStartDate();
        Date endtime = mListData.get(position).getEndDate();
        String title = mListData.get(position).getName();
        String startdate = simpleDateFormat.format(starttime);
        String enddate = simpleDateFormat.format(endtime);
        String progInfo = title+"("+startdate+"~"+enddate + ")";// + mListData.get(position).getProgramName();

        // specific process
        holder.mText.setText(progInfo);

        return convertView;
    }

    public void remove(int position) {
        SLog.d(TAG, "remove");
        mListData.remove(position);
    }

    public void setList(List<AVProgram> list) {
        this.mListData = list;
    }

    public List<AVProgram> getList() {
        return mListData;
    }

    public class SubViewHolder
    {
        public TextView mText;
        public ImageView mImage;
    }

}
