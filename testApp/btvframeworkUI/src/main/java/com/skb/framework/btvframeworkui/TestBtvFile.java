package com.skb.framework.btvframeworkui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.skb.btv.framework.core.BtvFramework;
import com.skb.btv.framework.log.SLog;
import com.skb.btv.framework.resource.BtvFile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class TestBtvFile extends Activity implements View.OnClickListener {
    private static final String TAG = "TestBtvFile";
    private static final String TESTTAG = "BtvTest";
    private static final String PREFIX = "TestBtvFile:";

    private Context mContext;

    private EditText mFileName;
    private Button mBtnOpen, mBtnClose;
    private Button mBtnExists, mBtnCreateNewFile, mBtnDelete;
    private Button mBtnWrite, mBtnRead;

    private BtvFile mBtvFile = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_test_btvfile);

        mContext = this;

        Button btnBack = (Button) findViewById(R.id.btvfile_back_button);
        btnBack.setOnClickListener(this);

        mFileName = (EditText) findViewById(R.id.btvfile_filename);
        mFileName.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    mBtnOpen.setEnabled(true);
                } else {
                    mBtnOpen.setEnabled(false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing
            }

            @Override
            public void afterTextChanged(Editable s) {
                //do nothing
            }
        });

        mBtnOpen = (Button) findViewById(R.id.btvfile_open_button);
        mBtnOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBtvFile = BtvFramework.BtvFile(mContext, getFilePath());
                SLog.d(TESTTAG, PREFIX + "BtvFile() - A new BtvFile instance is created (" + getFilePath() + ")");
                Toast.makeText(mContext.getApplicationContext(), "A new BtvFile instance is created", Toast.LENGTH_LONG).show();

                mFileName.setEnabled(false);

                mBtnOpen.setEnabled(false);
                mBtnClose.setEnabled(true);

                mBtnExists.setEnabled(true);
                mBtnCreateNewFile.setEnabled(true);
                mBtnDelete.setEnabled(true);
            }
        });

        mBtnClose = (Button) findViewById(R.id.btvfile_close_button);
        mBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBtvFile = null;

                mFileName.setText("");
                mFileName.setEnabled(true);

                mBtnOpen.setEnabled(true);
                mBtnClose.setEnabled(false);

                mBtnExists.setEnabled(false);
                mBtnCreateNewFile.setEnabled(false);
                mBtnDelete.setEnabled(false);

                mBtnWrite.setEnabled(false);
                mBtnRead.setEnabled(false);
            }
        });

        mBtnExists = (Button) findViewById(R.id.btvfile_exists_button);
        mBtnExists.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBtvFile != null) {
                    if (mBtvFile.exists()) {
                        SLog.d(TESTTAG, PREFIX + "exists() - The file exists");
                        Toast.makeText(mContext.getApplicationContext(), "The file exists", Toast.LENGTH_LONG).show();

                        mBtnWrite.setEnabled(true);
                        mBtnRead.setEnabled(true);
                    } else {
                        SLog.d(TESTTAG, PREFIX + "exists() - The file does not exist");
                        Toast.makeText(mContext.getApplicationContext(), "The file does not exist", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        mBtnCreateNewFile = (Button) findViewById(R.id.btvfile_createnewfile_button);
        mBtnCreateNewFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBtvFile != null) {
                    try {
                        if (mBtvFile.createNewFile()) {
                            SLog.d(TESTTAG, PREFIX + "createNewFile() - The file is created successfully");
                            Toast.makeText(mContext.getApplicationContext(), "The file is created successfully", Toast.LENGTH_LONG).show();

                            mBtnWrite.setEnabled(true);
                            mBtnRead.setEnabled(true);
                        } else {
                            SLog.d(TESTTAG, PREFIX + "createNewFile() - The file already exists");
                            Toast.makeText(mContext.getApplicationContext(), "The file already exists", Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        SLog.d(TESTTAG, PREFIX + "createNewFile() - IOException occurred (invalid path)");
                        Toast.makeText(mContext.getApplicationContext(), "IOException occurred (invalid path)", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        mBtnDelete = (Button) findViewById(R.id.btvfile_delete_button);
        mBtnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBtvFile != null) {
                    if (mBtvFile.delete()) {
                        SLog.d(TESTTAG, PREFIX + "delete() - The file is deleted successfully");
                        Toast.makeText(mContext.getApplicationContext(), "The file is deleted successfully", Toast.LENGTH_LONG).show();
                    } else {
                        SLog.d(TESTTAG, PREFIX + "delete() - The file deletion failed");
                        Toast.makeText(mContext.getApplicationContext(), "The file deletion failed", Toast.LENGTH_LONG).show();
                    }

                    mBtnWrite.setEnabled(false);
                    mBtnRead.setEnabled(false);
                }
            }
        });

        mBtnWrite = (Button) findViewById(R.id.btvfile_write_button);
        mBtnWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBtvFile != null) {
                    if (mBtvFile.exists()) {
                        String str = "abcdefghijklmnopqrstuvwxyz";

                        BufferedWriter bufWriter = null;
                        try {
                            bufWriter = new BufferedWriter(new FileWriter(mBtvFile, true));
                            bufWriter.write(str);
                            bufWriter.newLine();
                            bufWriter.flush();
                            bufWriter.close();
                            bufWriter = null;

                            SLog.d(TESTTAG, PREFIX + "write() - " + str);
                            Toast.makeText(mContext.getApplicationContext(), "Write : " + str, Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            if (bufWriter != null) {
                                try {
                                    bufWriter.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } else {
                        Toast.makeText(mContext.getApplicationContext(), "The file does not exist", Toast.LENGTH_LONG).show();

                        mBtnWrite.setEnabled(false);
                        mBtnRead.setEnabled(false);
                    }
                }
            }
        });
        mBtnRead = (Button) findViewById(R.id.btvfile_read_button);
        mBtnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBtvFile != null) {
                    if (mBtvFile.exists()) {
                        StringBuilder sb = new StringBuilder();

                        BufferedReader bufReader = null;
                        try {
                            bufReader = new BufferedReader(new FileReader(mBtvFile));
                            String line;
                            while ((line = bufReader.readLine()) != null) {
                                sb.append(line).append("\n");
                            }
                            bufReader.close();
                            bufReader = null;

                            SLog.d(TESTTAG, PREFIX + "read() - " + sb);
                            Toast.makeText(mContext.getApplicationContext(), "Read : " + sb, Toast.LENGTH_LONG).show();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                if (bufReader != null) {
                                    bufReader.close();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        Toast.makeText(mContext.getApplicationContext(), "The file does not exist", Toast.LENGTH_LONG).show();

                        mBtnWrite.setEnabled(false);
                        mBtnRead.setEnabled(false);
                    }
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btvfile_back_button:
                mBtvFile = null;
                finish();
                break;
        }
    }

    private String getFilePath() {
        return getExternalFilesDir(null).getAbsolutePath() + File.separator + mFileName.getText().toString();
    }
}
