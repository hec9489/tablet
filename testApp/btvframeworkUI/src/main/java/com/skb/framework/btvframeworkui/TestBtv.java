package com.skb.framework.btvframeworkui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.skb.btv.framework.core.BtvFramework;
import com.skb.btv.framework.property.ChangePropertyListener;
import com.skb.btv.framework.property.PropertyManager;
import com.skb.btv.framework.log.SLog;
import com.skb.btv.framework.avcontrol.AVControlManager;
import com.skb.btv.framework.peripheral.PeripheralManager;
import com.skb.btv.framework.system.SystemManager;
import com.skb.btv.framework.network.NetworkManager;

public class TestBtv extends Activity implements View.OnClickListener {

    private static final String TAG =  "TestBtv";
    private static final String TESTTAG =  "BtvTest TestBtv";

    BtvFramework btvFramework = null;
    PropertyManager mPropertyManager = null;
    AVControlManager mAVControlManager = null;
    PeripheralManager mPeripheralManager = null;
    SystemManager mSystemManager = null;
    NetworkManager mNetworkManager = null;

    //해상도 변경
    public static final int DISPLAY_MODE_AUTO = 11;
    public static final int DISPLAY_MODE_720P = 1;
    public static final int DISPLAY_MODE_1080I = 3;
    public static final int DISPLAY_MODE_1080P = 5;
    public static final int DISPLAY_MODE_3840x2160P60HZ = 10;
    //화면비율 변경
    public static final int SCALING_MODE_ORIGINAL = 0;
    public static final int SCALING_MODE_SCREEN = 1;
    //오디오 출력
    public static final int HDMI_AUDIO_MODE_AUTO = 0;
    public static final int HDMI_AUDIO_MODE_PCM = 1;

    //해상도 변경
    private RadioButton mRadioScreenSizeAuto;       // 자동
    private RadioButton mRadioScreenSize720p;       // 720p
    private RadioButton mRadioScreenSize1080i;      // 1080i
    private RadioButton mRadioScreenSize1080p;      // 1080p
    private RadioButton mRadioScreenSize2160p;      // 2160p

    //화면비율 변경
    private RadioButton mRadioRatioOriginal;        // 원본비율
    private RadioButton mRadioRatioScreen;          // 화면비율

    //TV색상 변경
    private Button mColorBrightnessValue;       // 밝기
    private Button mColorBrightnessUp;          // 밝기 업
    private Button mColorBrightnessDown;        // 밝기 다운
    private Button mColorContrastValue;         // 명함
    private Button mColorContrastUp;            // 명함 업
    private Button mColorContrastDown;          // 명함 다운
    private Button mColorSaturationValue;       // 선명도
    private Button mColorSaturationUp;          // 선명도 업
    private Button mColorSaturationDown;        // 선명도 다운
    private Button mColorHueValue;               // 색상
    private Button mColorHueUp;          // 색상 업
    private Button mColorHueDown;        // 색상 다운
    private int mColorBrightness = 50;              // 밝기 0~100
    private int mColorContrast = 50;                // 명함 0~100
    private int mColorSaturation = 50;              // 선명도 0~100
    private int mColorHue = 50;                     // 색상 0~100

    //HDR화질 자동변환 설정
    private RadioButton mRadioHDRAutoEnable;        // 자동변환 활성화
    private RadioButton mRadioHDRAutoDisable;       // 자동변환 비활성화
    private int mHDRAutoType = 0;                   // 0:활성화, 1:비활성화

    //오디오 출력
    private RadioButton mRadioAudioOutputNormal;    // 일반음질
    private RadioButton mRadioAudioOutputDolby;     // DOLBY
    private int mAudioOutputType = 0;               // 0:일반음질, 1:dolby

    //시각장애인용 화면해설방송
    private RadioButton mRadioCommentaryEnable;     // 해설 사용함
    private RadioButton mRadioCommentaryDisable;    // 해설 사용안함
    private int mCommentaryType = 0;                // 0:사용함, 1:사용안함

    //청각장애인용 자막방송
    private RadioButton mRadioSubtitleEnable;       // 자막 사용함
    private RadioButton mRadioSubtitleDisable;      // 자막 사용안함
    private int mSubtitleType = 0;                  // 0:사용함, 1:사용안함

    //TV전원제어[HDMI]
    private RadioButton mRadioTVPowerEnable;        // 전원 사용함
    private RadioButton mRadioTVPowerDisable;       // 전원 사용안함

    //SW업데이트
    private TextView mTextSWVersion;                // SW 버전
    private Button mBtnSWUpdate;                    // SW 업데이트

    //리모컨 한글 키패드 설정
    private RadioButton mRadioKeypadNew;            // 키패드 신규
    private RadioButton mRadioKeypadOriginal;       // 키패드 기존
    private int mKeypadType = 0;                    // 0:신규, 1:기존

    //셋톱박스 자동 리부팅
    private RadioButton mRadioStbAutoRebootEnable;  // 자동리부팅 사용
    private RadioButton mRadioStbAutoRebootDisable; // 자동리부팅 사용안함
    private int mStbAutoReboot = 0;                 // 0:사용함, 1: 사용안함

    //공장 초기화
    private Button mBtnFactoryReset;                // 공장 초기화

    //최적화
    private Button mBtnDefragmentMemory;           // 최적화

    //기기정보
    private TextView mTextStbInfo;                  // 모델명,사용자ID,SW버전,MAC,XPG버전,용량

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SLog.d(TAG, "onCreate");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_btv_test);

        Button button_ok = (Button) findViewById(R.id.button_ok);
        Button button_tvservice = (Button) findViewById(R.id.button_tvservice);
        Button button_dvdsi = (Button) findViewById(R.id.button_dvdsi);
        Button button_btvfile = (Button) findViewById(R.id.button_btvfile);
        button_ok.setOnClickListener(this);
        button_tvservice.setOnClickListener(this);
        button_dvdsi.setOnClickListener(this);
        button_btvfile.setOnClickListener(this);

        button_ok.requestFocus();

        btvFramework = BtvFramework.getInstance(this);
        mAVControlManager = (AVControlManager)btvFramework.getBtvService(BtvFramework.AVCONTROL_SERVICE);
        mAVControlManager.setManagerReadyListener(new BtvFramework.ManagerReadyStatusListener() {
            @Override
            public void onManagerReady(Object object, int status) {
                if(status == BtvFramework.MANAGER_READY_STATUS){
                    setUi();
                }
            }
        });
        mPeripheralManager = (PeripheralManager)btvFramework.getBtvService(BtvFramework.PERIPHERAL_SERVICE);
        mPeripheralManager.setManagerReadyListener(new BtvFramework.ManagerReadyStatusListener() {
            @Override
            public void onManagerReady(Object object, int status) {
                if(status == BtvFramework.MANAGER_READY_STATUS){
                    setUi();
                }
            }
        });
        mSystemManager = (SystemManager)btvFramework.getBtvService(BtvFramework.SYSTEM_SERVICE);
        mSystemManager.setManagerReadyListener(new BtvFramework.ManagerReadyStatusListener() {
            @Override
            public void onManagerReady(Object object, int status) {
                if(status == BtvFramework.MANAGER_READY_STATUS){
                    setUi();
                }
            }
        });
        mNetworkManager = (NetworkManager)btvFramework.getBtvService(BtvFramework.NETWORK_SERVICE);
        mNetworkManager.setManagerReadyListener(new BtvFramework.ManagerReadyStatusListener() {
            @Override
            public void onManagerReady(Object object, int status) {
                if(status == BtvFramework.MANAGER_READY_STATUS){
                    setUi();
                }
            }
        });
        mPropertyManager = (PropertyManager)btvFramework.getBtvService(BtvFramework.PROPERTY_SERVICE);
        mPropertyManager.setManagerReadyListener(new BtvFramework.ManagerReadyStatusListener() {
            @Override
            public void onManagerReady(Object object, int status) {
                if(status == BtvFramework.MANAGER_READY_STATUS){
                    setUi();
                }
            }
        });


        //해상도 변경
        mRadioScreenSizeAuto = (RadioButton)findViewById(R.id.radio_screen_size_auto);
        mRadioScreenSize720p = (RadioButton)findViewById(R.id.radio_screen_size_720p);
        mRadioScreenSize1080i = (RadioButton)findViewById(R.id.radio_screen_size_1080i);
        mRadioScreenSize1080p = (RadioButton)findViewById(R.id.radio_screen_size_1080p);
        mRadioScreenSize2160p = (RadioButton)findViewById(R.id.radio_screen_size_2160p);
        mRadioScreenSizeAuto.setOnClickListener(onScreenSizeClickListener);
        mRadioScreenSize720p.setOnClickListener(onScreenSizeClickListener);
        mRadioScreenSize1080i.setOnClickListener(onScreenSizeClickListener);
        mRadioScreenSize1080p.setOnClickListener(onScreenSizeClickListener);
        mRadioScreenSize2160p.setOnClickListener(onScreenSizeClickListener);

        //화면비율 변경
        mRadioRatioOriginal = (RadioButton)findViewById(R.id.radio_screen_ratio_original);
        mRadioRatioScreen = (RadioButton)findViewById(R.id.radio_screen_ratio_screen);
        mRadioRatioOriginal.setOnClickListener(onScreenRatioClickListener);
        mRadioRatioScreen.setOnClickListener(onScreenRatioClickListener);

        //TV색상 변경
        mColorBrightnessValue = (Button)findViewById(R.id.color_brightness_value);
        mColorBrightnessUp = (Button)findViewById(R.id.color_brightness_up);
        mColorBrightnessDown = (Button)findViewById(R.id.color_brightness_down);
        mColorContrastValue = (Button)findViewById(R.id.color_contrast_value);
        mColorContrastUp = (Button)findViewById(R.id.color_contrast_up);
        mColorContrastDown = (Button)findViewById(R.id.color_contrast_down);
        mColorSaturationValue = (Button)findViewById(R.id.color_saturation_value);
        mColorSaturationUp = (Button)findViewById(R.id.color_saturation_up);
        mColorSaturationDown = (Button)findViewById(R.id.color_saturation_down);
        mColorHueValue = (Button)findViewById(R.id.color_hue_value);
        mColorHueUp = (Button)findViewById(R.id.color_hue_up);
        mColorHueDown = (Button)findViewById(R.id.color_hue_down);
        mColorBrightnessUp.setOnClickListener(onScreenColorChangeListener);
        mColorBrightnessDown.setOnClickListener(onScreenColorChangeListener);
        mColorContrastUp.setOnClickListener(onScreenColorChangeListener);
        mColorContrastDown.setOnClickListener(onScreenColorChangeListener);
        mColorSaturationUp.setOnClickListener(onScreenColorChangeListener);
        mColorSaturationDown.setOnClickListener(onScreenColorChangeListener);
        mColorHueUp.setOnClickListener(onScreenColorChangeListener);
        mColorHueDown.setOnClickListener(onScreenColorChangeListener);

        //HDR화질 자동변환 설정
        mRadioHDRAutoEnable = (RadioButton)findViewById(R.id.radio_hdr_auto_enable);
        mRadioHDRAutoDisable = (RadioButton)findViewById(R.id.radio_hdr_auto_disable);
        mRadioHDRAutoEnable.setOnClickListener(onHDRAutoClickListener);
        mRadioHDRAutoDisable.setOnClickListener(onHDRAutoClickListener);

        //오디오 출력
        mRadioAudioOutputNormal = (RadioButton)findViewById(R.id.radio_audio_output_normal);
        mRadioAudioOutputDolby = (RadioButton)findViewById(R.id.radio_audio_output_dolby);
        mRadioAudioOutputNormal.setOnClickListener(onAudioOutputClickListener);
        mRadioAudioOutputDolby.setOnClickListener(onAudioOutputClickListener);

        //시각장애인용 화면해설방송
        mRadioCommentaryEnable = (RadioButton)findViewById(R.id.radio_commentary_enable);
        mRadioCommentaryDisable = (RadioButton)findViewById(R.id.radio_commentary_disable);
        mRadioCommentaryEnable.setOnClickListener(onCommentaryClickListener);
        mRadioCommentaryDisable.setOnClickListener(onCommentaryClickListener);

        //청각장애인용 자막방송
        mRadioSubtitleEnable = (RadioButton)findViewById(R.id.radio_subtitle_enable);
        mRadioSubtitleDisable = (RadioButton)findViewById(R.id.radio_subtitle_disable);
        mRadioSubtitleEnable.setOnClickListener(onSubtitleClickListener);
        mRadioSubtitleDisable.setOnClickListener(onSubtitleClickListener);

        //TV전원제어[HDMI]
        mRadioTVPowerEnable = (RadioButton)findViewById(R.id.radio_power_enable);
        mRadioTVPowerDisable = (RadioButton)findViewById(R.id.radio_power_disable);
        mRadioTVPowerEnable.setOnClickListener(onTVPowerClickListener);
        mRadioTVPowerDisable.setOnClickListener(onTVPowerClickListener);

        //SW업데이트
        mTextSWVersion = (TextView)findViewById(R.id.text_sw_version);
        mBtnSWUpdate = (Button)findViewById(R.id.btn_sw_update);
        mBtnSWUpdate.setOnClickListener(onSWUpdateClickListener);

        //리모컨 한글 키패드 설정
        mRadioKeypadNew = (RadioButton)findViewById(R.id.radio_keypad_new);
        mRadioKeypadOriginal = (RadioButton)findViewById(R.id.radio_keypad_original);
        mRadioKeypadNew.setOnClickListener(onKeypadClickListener);
        mRadioKeypadOriginal.setOnClickListener(onKeypadClickListener);

        //셋톱박스 자동 리부팅
        mRadioStbAutoRebootEnable = (RadioButton)findViewById(R.id.radio_stb_auto_reboot_enable);
        mRadioStbAutoRebootDisable = (RadioButton)findViewById(R.id.radio_stb_auto_reboot_disable);
        mRadioStbAutoRebootEnable.setOnClickListener(onStbAutoRebootClickListener);
        mRadioStbAutoRebootDisable.setOnClickListener(onStbAutoRebootClickListener);

        //공장 초기화
        mBtnFactoryReset = (Button)findViewById(R.id.btn_factory_reset);
        mBtnFactoryReset.setOnClickListener(onFactoryResetClickListener);

        //최적화
        mBtnDefragmentMemory = (Button)findViewById(R.id.btn_defragment_memory);
        mBtnDefragmentMemory.setOnClickListener(onDefragmentMemoryClickListener);

        //기기정보
        mTextStbInfo = (TextView)findViewById(R.id.text_stb_info);

        setUi();
    }

    private void setUi() {
        SLog.d(TAG, "setUi");
        setScreenSize(getScreenSize());
        setScreenRatio(getScreenRatio());
        setTVColor();

        SLog.d(TESTTAG, "getHDRDisplayMode start");
        mHDRAutoType = mAVControlManager.getAVControlServiceDisplayOutput().getHDRDisplayMode();
        SLog.d(TESTTAG, "getHDRDisplayMode end result : [" + mHDRAutoType + "]");

        setHDRAuto(mHDRAutoType);

        getAudioOutput();

        boolean getBoolHearingImpaired = mPropertyManager.get(mPropertyManager.BTV_PROPERTY,"HEARING_IMPAIRED",false);
        if(getBoolHearingImpaired){
            mCommentaryType = 0;
        } else {
            mCommentaryType = 1;
        }
        setCommentary(mCommentaryType);

        boolean getBoolVisionimpaired = mPropertyManager.get(mPropertyManager.BTV_PROPERTY,"VISION_IMPAIRED",false);
        if(getBoolVisionimpaired){
            mSubtitleType = 0;
        } else {
            mSubtitleType = 1;
        }
        setSubtitle(mSubtitleType);

        setTVPower(getTVPower());

        setKeypad(mKeypadType);

        setStbAutoReboot(mStbAutoReboot);

        setStbInfo();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        SLog.d(TAG, "onTouchEvent");
        if(event.getAction()==MotionEvent.ACTION_OUTSIDE){
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        SLog.d(TAG, "onClick" +v.getId());
        switch (v.getId()){
            case R.id.button_ok:
                finish();
                break;
            case R.id.button_tvservice:
                Intent intent_tvservice = new Intent(this, TestTvservice.class);
                startActivity(intent_tvservice);
                break;
            case R.id.button_dvdsi:
                Intent intent_dvdsi = new Intent(this, TestDvdsi.class);
                startActivity(intent_dvdsi);
                break;
            case R.id.button_btvfile:
                Intent intent_btvfile = new Intent(this, TestBtvFile.class);
                startActivity(intent_btvfile);
                break;
        }
    }

    /**
     * 해상도 변경 클릭
     */
    RadioButton.OnClickListener onScreenSizeClickListener = new RadioButton.OnClickListener(){
        @Override
        public void onClick(View v) {
            SLog.d(TAG, "onScreenSizeClickListener onClick " +v.getId());
            int id = v.getId();
            if(id == R.id.radio_screen_size_auto){
                SLog.d(TESTTAG, "setDisplayResolutionAUTO start");
                boolean screen_size_auto = mAVControlManager.getAVControlServiceDisplayOutput().setDisplayResolution(DISPLAY_MODE_AUTO);
                Toast.makeText(getApplicationContext(),"screen_size_auto",Toast.LENGTH_LONG).show();
                SLog.d(TESTTAG, "setDisplayResolutionAUTO end result : [" + screen_size_auto + "]");
            }else if(id == R.id.radio_screen_size_720p){
                SLog.d(TESTTAG, "setDisplayResolution720P start");
                boolean screen_size_720p = mAVControlManager.getAVControlServiceDisplayOutput().setDisplayResolution(DISPLAY_MODE_720P);
                Toast.makeText(getApplicationContext(),"screen_size_720p",Toast.LENGTH_LONG).show();
                SLog.d(TESTTAG, "setDisplayResolution720P end result : [" + screen_size_720p + "]");
            }else if(id == R.id.radio_screen_size_1080i){
                SLog.d(TESTTAG, "setDisplayResolution1080I start");
                boolean screen_size_1080i = mAVControlManager.getAVControlServiceDisplayOutput().setDisplayResolution(DISPLAY_MODE_1080I);
                Toast.makeText(getApplicationContext(),"screen_size_1080i",Toast.LENGTH_LONG).show();
                SLog.d(TESTTAG, "setDisplayResolution1080I end result : [" + screen_size_1080i + "]");
            }else if(id == R.id.radio_screen_size_1080p){
                SLog.d(TESTTAG, "setDisplayResolution1080P start");
                boolean screen_size_1080p = mAVControlManager.getAVControlServiceDisplayOutput().setDisplayResolution(DISPLAY_MODE_1080P);
                Toast.makeText(getApplicationContext(),"screen_size_1080p",Toast.LENGTH_LONG).show();
                SLog.d(TESTTAG, "setDisplayResolution1080P end result : [" + screen_size_1080p + "]");
            }else if(id == R.id.radio_screen_size_2160p){
                SLog.d(TESTTAG, "setDisplayResolution3840x2160P60HZ start");
                boolean screen_size_2160p = mAVControlManager.getAVControlServiceDisplayOutput().setDisplayResolution(DISPLAY_MODE_3840x2160P60HZ);
                Toast.makeText(getApplicationContext(),"screen_size_2160p",Toast.LENGTH_LONG).show();
                SLog.d(TESTTAG, "setDisplayResolution3840x2160P60HZ end result : [" + screen_size_2160p + "]");
            }
            setScreenSize(getScreenSize());
        }
    };

    /**
     * 해상도 가져오기
     *
     * @return DISPLAY_MODE_AUTO, DISPLAY_MODE_720P, DISPLAY_MODE_1080I, DISPLAY_MODE_1080P, DISPLAY_MODE_3840x2160P30HZ
     */
    private int getScreenSize(){
        SLog.d(TAG, "getScreenSize");
        SLog.d(TESTTAG, "getDisplayResolution start");
        int getDisplayResolution = mAVControlManager.getAVControlServiceDisplayOutput().getDisplayResolution();
        SLog.d(TESTTAG, "getDisplayResolution end result : [" + getDisplayResolution + "]");
        return getDisplayResolution;
    }

    /**
     * 해상도 설정
     *
     * @param type DISPLAY_MODE_AUTO, DISPLAY_MODE_720P, DISPLAY_MODE_1080I, DISPLAY_MODE_1080P, DISPLAY_MODE_3840x2160P30HZ
     */
    private void setScreenSize(int type){
        SLog.d(TAG, "setScreenSize");
        mRadioScreenSizeAuto.setChecked(false);
        mRadioScreenSize720p.setChecked(false);
        mRadioScreenSize1080i.setChecked(false);
        mRadioScreenSize1080p.setChecked(false);
        mRadioScreenSize2160p.setChecked(false);
        if(type == DISPLAY_MODE_AUTO){
            mRadioScreenSizeAuto.setChecked(true);
        }else if(type == DISPLAY_MODE_720P){
            mRadioScreenSize720p.setChecked(true);
        }else if(type == DISPLAY_MODE_1080I){
            mRadioScreenSize1080i.setChecked(true);
        }else if(type == DISPLAY_MODE_1080P){
            mRadioScreenSize1080p.setChecked(true);
        }else if(type == DISPLAY_MODE_3840x2160P60HZ){
            mRadioScreenSize2160p.setChecked(true);
        }
    }

    /**
     * 화면비율 변경 클릭
     */
    RadioButton.OnClickListener onScreenRatioClickListener = new RadioButton.OnClickListener(){
        @Override
        public void onClick(View v) {
            SLog.d(TAG, "onScreenRatioClickListener onClick" +v.getId());
            int id = v.getId();
            if(id == R.id.radio_screen_ratio_original){
                SLog.d(TESTTAG, "setVideoScalingModeORIGINAL start");
                boolean screen_ratio_original = mAVControlManager.getAVControlServiceVideoCompositor().setVideoScalingMode(SCALING_MODE_ORIGINAL);
                Toast.makeText(getApplicationContext(),"screen_ratio_original",Toast.LENGTH_LONG).show();
                SLog.d(TESTTAG, "setVideoScalingModeORIGINAL end result : [" + screen_ratio_original + "]");
            }else if(id == R.id.radio_screen_ratio_screen){
                SLog.d(TESTTAG, "setVideoScalingModeSCREEN start");
                boolean screen_ratio_screen = mAVControlManager.getAVControlServiceVideoCompositor().setVideoScalingMode(SCALING_MODE_SCREEN);
                Toast.makeText(getApplicationContext(),"screen_ratio_screen",Toast.LENGTH_LONG).show();
                SLog.d(TESTTAG, "setVideoScalingModeSCREEN end result : [" + screen_ratio_screen + "]");
            }
            setScreenRatio(getScreenRatio());
        }
    };

    /**
     * 화면비율 가져오기
     *
     * @return ITVSettings.SCALING_MODE_ORIGINAL, ITVSettings.SCALING_MODE_SCREEN
     */
    private int getScreenRatio(){
        SLog.d(TAG, "getScreenRatio");
        SLog.d(TESTTAG, "getVideoScalingMode start");
        int getVideoScalingMode = mAVControlManager.getAVControlServiceVideoCompositor().getVideoScalingMode();
        SLog.d(TESTTAG, "getVideoScalingMode end result : [" + getVideoScalingMode + "]");
        return getVideoScalingMode;
    }

    /**
     * 화면비율 설정
     *
     * @param type ITVSettings.SCALING_MODE_ORIGINAL:원본비율,
     *             ITVSettings.SCALING_MODE_SCREEN:화면비율
     */
    private void setScreenRatio(int type){
        SLog.d(TAG, "setScreenRatio");
        mRadioRatioOriginal.setChecked(false);
        mRadioRatioScreen.setChecked(false);
        if(type == SCALING_MODE_ORIGINAL){
            mRadioRatioOriginal.setChecked(true);
        }else if(type == SCALING_MODE_SCREEN){
            mRadioRatioScreen.setChecked(true);
        }

        SLog.d(TESTTAG, "setVideoScalingMode start");
        boolean setVideoScalingMode = mAVControlManager.getAVControlServiceVideoCompositor().setVideoScalingMode(type);
        SLog.d(TESTTAG, "setVideoScalingMode end result : [" + setVideoScalingMode + "]");
    }

    /**
     * TV색상 변경 클릭
     */
    View.OnClickListener onScreenColorChangeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SLog.d(TAG, "onScreenColorChangeListener onClick" +v.getId());
            getTVColor();
            int id = v.getId();
            if(id == R.id.color_brightness_up){
                SLog.d(TESTTAG, "setGraphicBrightnessUP start");
                boolean color_brightness_up = mAVControlManager.getAVControlServiceVideoCompositor().setGraphicBrightness(mColorBrightness +1);
                Toast.makeText(getApplicationContext(),"color_brightness_up",Toast.LENGTH_LONG).show();
                SLog.d(TESTTAG, "setGraphicBrightnessUP end result : [" + color_brightness_up + "]");
            }else if(id == R.id.color_brightness_down){
                SLog.d(TESTTAG, "setGraphicBrightnessDOWN start");
                boolean color_brightness_down = mAVControlManager.getAVControlServiceVideoCompositor().setGraphicBrightness(mColorBrightness -1);
                Toast.makeText(getApplicationContext(),"color_brightness_down",Toast.LENGTH_LONG).show();
                SLog.d(TESTTAG, "setGraphicBrightnessDOWN end result : [" + color_brightness_down + "]");
            }else if(id == R.id.color_contrast_up){
                SLog.d(TESTTAG, "setGraphicContrastUP start");
                boolean color_contrast_up = mAVControlManager.getAVControlServiceVideoCompositor().setGraphicContrast(mColorContrast +1);
                Toast.makeText(getApplicationContext(),"color_contrast_up",Toast.LENGTH_LONG).show();
                SLog.d(TESTTAG, "setGraphicContrastUP end result : [" + color_contrast_up + "]");
            }else if(id == R.id.color_contrast_down){
                SLog.d(TESTTAG, "setGraphicContrastDOWN start");
                boolean color_contrast_down = mAVControlManager.getAVControlServiceVideoCompositor().setGraphicContrast(mColorContrast -1);
                Toast.makeText(getApplicationContext(),"color_contrast_down",Toast.LENGTH_LONG).show();
                SLog.d(TESTTAG, "setGraphicContrastDOWN end result : [" + color_contrast_down + "]");
            }else if(id == R.id.color_saturation_up){
                SLog.d(TESTTAG, "setGraphicSaturationUP start");
                boolean color_saturation_up = mAVControlManager.getAVControlServiceVideoCompositor().setGraphicSaturation(mColorSaturation +1);
                Toast.makeText(getApplicationContext(),"color_saturation_up",Toast.LENGTH_LONG).show();
                SLog.d(TESTTAG, "setGraphicSaturationUP end result : [" + color_saturation_up + "]");
            }else if(id == R.id.color_saturation_down){
                SLog.d(TESTTAG, "setGraphicSaturationDOWN start");
                boolean color_saturation_down = mAVControlManager.getAVControlServiceVideoCompositor().setGraphicSaturation(mColorSaturation -1);
                Toast.makeText(getApplicationContext(),"color_saturation_down",Toast.LENGTH_LONG).show();
                SLog.d(TESTTAG, "setGraphicSaturationDOWN end result : [" + color_saturation_down + "]");
            }else if(id == R.id.color_hue_up){
                SLog.d(TESTTAG, "setGraphicHueUP start");
                boolean color_hue_up = mAVControlManager.getAVControlServiceVideoCompositor().setGraphicHue(mColorHue +1);
                Toast.makeText(getApplicationContext(),"color_hue_up",Toast.LENGTH_LONG).show();
                SLog.d(TESTTAG, "setGraphicHueUP end result : [" + color_hue_up + "]");
            }else if(id == R.id.color_hue_down){
                SLog.d(TESTTAG, "setGraphicHueDOWN start");
                boolean color_hue_down = mAVControlManager.getAVControlServiceVideoCompositor().setGraphicHue(mColorHue -1);
                Toast.makeText(getApplicationContext(),"color_hue_down",Toast.LENGTH_LONG).show();
                SLog.d(TESTTAG, "setGraphicHueDOWN end result : [" + color_hue_down + "]");
            }

            setTVColor();
        }
    };

    /**
     * TV색상 가져오기
     */
    private void getTVColor(){
        SLog.d(TAG, "getTVColor");

        SLog.d(TESTTAG, "getGraphicBrightness start");
        mColorBrightness = mAVControlManager.getAVControlServiceVideoCompositor().getGraphicBrightness();
        SLog.d(TESTTAG, "getGraphicBrightness end result : [" + mColorBrightness + "]");

        SLog.d(TESTTAG, "getGraphicContrast start");
        mColorContrast = mAVControlManager.getAVControlServiceVideoCompositor().getGraphicContrast();
        SLog.d(TESTTAG, "getGraphicContrast end result : [" + mColorContrast + "]");

        SLog.d(TESTTAG, "getGraphicSaturation start");
        mColorSaturation = mAVControlManager.getAVControlServiceVideoCompositor().getGraphicSaturation();
        SLog.d(TESTTAG, "getGraphicSaturation end result : [" + mColorSaturation + "]");

        SLog.d(TESTTAG, "getGraphicHue start");
        mColorHue = mAVControlManager.getAVControlServiceVideoCompositor().getGraphicHue();
        SLog.d(TESTTAG, "getGraphicHue end result : [" + mColorHue + "]");

    }

    /**
     * TV색상 변경
     */
    private void setTVColor(){
        SLog.d(TAG, "setTVColor");
        getTVColor();

        mColorBrightnessValue.setText(Integer.toString(mColorBrightness));
        mColorContrastValue.setText(Integer.toString(mColorContrast));
        mColorSaturationValue.setText(Integer.toString(mColorSaturation));
        mColorHueValue.setText(Integer.toString(mColorHue));

    }

    /**
     * HDR화질 자동변환 설정 클릭
     */
    RadioButton.OnClickListener onHDRAutoClickListener = new RadioButton.OnClickListener(){
        @Override
        public void onClick(View v) {
            SLog.d(TAG, "onHDRAutoClickListener onClick" +v.getId());
            int id = v.getId();
            if(id == R.id.radio_hdr_auto_enable){
                setHDRAuto(0);
                Toast.makeText(getApplicationContext(),"hdr_auto_enable",Toast.LENGTH_LONG).show();
            }else if(id == R.id.radio_hdr_auto_disable){
                setHDRAuto(1);
                Toast.makeText(getApplicationContext(),"hdr_auto_disable",Toast.LENGTH_LONG).show();
            }
        }
    };

    /**
     * HDR화질 자동변환 설정
     * @param type 0:활성화, 1:비활성화
     */
    private void setHDRAuto(int type){
        SLog.d(TAG, "setHDRAuto");
        mRadioHDRAutoEnable.setChecked(false);
        mRadioHDRAutoDisable.setChecked(false);
        if(type == 0){
            mRadioHDRAutoEnable.setChecked(true);
        }else if(type == 1){
            mRadioHDRAutoDisable.setChecked(true);
        }

        SLog.d(TESTTAG, "setHDRDisplayMode start");
        boolean setHDRDisplayMode = mAVControlManager.getAVControlServiceDisplayOutput().setHDRDisplayMode(type);
        SLog.d(TESTTAG, "setHDRDisplayMode end result : [" + setHDRDisplayMode + "]");

        mHDRAutoType = type;
    }

    /**
     * 오디오 출력 클릭
     */
    RadioButton.OnClickListener onAudioOutputClickListener = new RadioButton.OnClickListener(){
        @Override
        public void onClick(View v) {
            SLog.d(TAG, "onAudioOutputClickListener onClick" +v.getId());
            int id = v.getId();
            if(id == R.id.radio_audio_output_normal){
                SLog.d(TESTTAG, "setHDMIAudioModePCM start");
                boolean setHDMIAudioMode = mAVControlManager.getAVControlServiceAudioMixer().setAudioOutputMode(true);
                SLog.d(TESTTAG, "setHDMIAudioModePCM end result : [" + setHDMIAudioMode + "]");

                setAudioOutput(0);
                Toast.makeText(getApplicationContext(),"audio_output_normal",Toast.LENGTH_LONG).show();
            }else if(id == R.id.radio_audio_output_dolby){
                SLog.d(TESTTAG, "setHDMIAudioModeAUTO start");
                boolean setHDMIAudioMode = mAVControlManager.getAVControlServiceAudioMixer().setAudioOutputMode(false);
                SLog.d(TESTTAG, "setHDMIAudioModeAUTO end result : [" + setHDMIAudioMode + "]");

                setAudioOutput(1);
                Toast.makeText(getApplicationContext(),"audio_output_dolby",Toast.LENGTH_LONG).show();
            }
        }
    };

    private void getAudioOutput(){
        SLog.d(TAG, "getAudioOutput");
        SLog.d(TESTTAG, "getHDMIAudioMode start");
        boolean audioType = mAVControlManager.getAVControlServiceAudioMixer().getAudioOutputMode();
        SLog.d(TESTTAG, "getHDMIAudioMode end result : [" + audioType + "]");
        if(audioType ){ // 일반
            setAudioOutput(0);
        }else { // Dolby
            setAudioOutput(1);
        }

    }

    /**
     * 오디오 출력 설정
     * @param type 0:일반음질, 1:dolby
     */
    private void setAudioOutput(int type){
        SLog.d(TAG, "setAudioOutput");
        mRadioAudioOutputNormal.setChecked(false);
        mRadioAudioOutputDolby.setChecked(false);
        if(type == 0){
            mRadioAudioOutputNormal.setChecked(true);
        }else if(type == 1){
            mRadioAudioOutputDolby.setChecked(true);
        }
        mAudioOutputType = type;
    }

    /**
     * 시각장애인용 화면해설방송 클릭
     */
    RadioButton.OnClickListener onCommentaryClickListener = new RadioButton.OnClickListener(){
        @Override
        public void onClick(View v) {
            SLog.d(TAG, "onCommentaryClickListener onClick" +v.getId());
            int id = v.getId();
            if(id == R.id.radio_commentary_enable){
                SLog.d(TESTTAG, "setBoolHearingImpairedTrue start");
                boolean setBoolHearingImpairedTrue = mPropertyManager.set(mPropertyManager.BTV_PROPERTY,"HEARING_IMPAIRED",true);
                SLog.d(TESTTAG, "setBoolHearingImpairedTrue end result : [" + setBoolHearingImpairedTrue + "]");

                setCommentary(0);
                Toast.makeText(getApplicationContext(),"commentary_enable",Toast.LENGTH_LONG).show();
            }else if(id == R.id.radio_commentary_disable){
                SLog.d(TESTTAG, "setBoolHearingImpairedFalse start");
                boolean setBoolHearingImpairedFalse = mPropertyManager.set(mPropertyManager.BTV_PROPERTY,"HEARING_IMPAIRED", false);
                SLog.d(TESTTAG, "setBoolHearingImpairedFalse end result : [" + setBoolHearingImpairedFalse + "]");

                setCommentary(1);
                Toast.makeText(getApplicationContext(),"commentary_disable",Toast.LENGTH_LONG).show();
            }
        }
    };

    /**
     * 시각장애인용 화면해설방송 설정
     *
     * @param type 0:사용함, 1:사용안함
     */
    private void setCommentary(int type){
        SLog.d(TAG, "setCommentary");
        mRadioCommentaryEnable.setChecked(false);
        mRadioCommentaryDisable.setChecked(false);
        if(type == 0){
            mRadioCommentaryEnable.setChecked(true);
        }else if(type == 1){
            mRadioCommentaryDisable.setChecked(true);
        }
    }

    /**
     * 청각장애인용 자막방송 클릭
     */
    RadioButton.OnClickListener onSubtitleClickListener = new RadioButton.OnClickListener(){
        @Override
        public void onClick(View v) {
            SLog.d(TAG, "onSubtitleClickListener onClick" +v.getId());
            int id = v.getId();
            if(id == R.id.radio_subtitle_enable){
                SLog.d(TESTTAG, "setBoolVisionimpairedTrue start");
                boolean setBoolVisionimpairedTrue = mPropertyManager.set(mPropertyManager.BTV_PROPERTY,"VISION_IMPAIRED",true);
                SLog.d(TESTTAG, "setBoolVisionimpairedTrue end result : [" + setBoolVisionimpairedTrue + "]");

                setSubtitle(0);
                Toast.makeText(getApplicationContext(),"subtitle_enable",Toast.LENGTH_LONG).show();
            }else if(id == R.id.radio_subtitle_disable){
                SLog.d(TESTTAG, "setBoolVisionimpairedFalse start");
                boolean setBoolVisionimpairedFalse = mPropertyManager.set(mPropertyManager.BTV_PROPERTY,"VISION_IMPAIRED",false);
                SLog.d(TESTTAG, "setBoolVisionimpairedFalse end result : [" + setBoolVisionimpairedFalse + "]");

                setSubtitle(1);
                Toast.makeText(getApplicationContext(),"subtitle_disable",Toast.LENGTH_LONG).show();
            }
        }
    };

    /**
     * 청각장애인용 자막방송 설정
     *
     * @param type 0:사용함, 1:사용안함
     */
    private void setSubtitle(int type){
        SLog.d(TAG, "setSubtitle");
        mRadioSubtitleEnable.setChecked(false);
        mRadioSubtitleDisable.setChecked(false);
        if(type == 0){
            mRadioSubtitleEnable.setChecked(true);
        }else if(type == 1){
            mRadioSubtitleDisable.setChecked(true);
        }

    }

    /**
     * TV전원제어 클럭
     */
    RadioButton.OnClickListener onTVPowerClickListener = new RadioButton.OnClickListener(){
        @Override
        public void onClick(View v) {
            SLog.d(TAG, "onTVPowerClickListener onClick" +v.getId());
            int id = v.getId();
            if(id == R.id.radio_power_enable){
                SLog.d(TESTTAG, "setCECStatusTrue start");
                boolean setCECStatusTrue = mAVControlManager.getAVControlServiceDisplayOutput().setCECStatus(true);
                Toast.makeText(getApplicationContext(),"power_enable",Toast.LENGTH_LONG).show();
                SLog.d(TESTTAG, "setCECStatusTrue end result : [" + setCECStatusTrue + "]");
            }else if(id == R.id.radio_power_disable){
                SLog.d(TESTTAG, "setCECStatusFalse start");
                boolean setCECStatusFalse = mAVControlManager.getAVControlServiceDisplayOutput().setCECStatus(false);
                Toast.makeText(getApplicationContext(),"power_disable",Toast.LENGTH_LONG).show();
                SLog.d(TESTTAG, "setCECStatusFalse end result : [" + setCECStatusFalse + "]");
            }
            setTVPower(getTVPower());
        }
    };

    /**
     * TV전원제어 설정
     *
     * @param type true:사용함, false:사용안
     *             함
     */
    private void setTVPower(boolean type){
        SLog.d(TAG, "setTVPower");
        mRadioTVPowerEnable.setChecked(false);
        mRadioTVPowerDisable.setChecked(false);
        if(type == true){
            mRadioTVPowerEnable.setChecked(true);
        }else if(type == false){
            mRadioTVPowerDisable.setChecked(true);
        }
    }

    private boolean getTVPower(){
        SLog.d(TAG, "getTVPower");

        SLog.d(TESTTAG, "getCECStatus start");
        boolean getCECStatus = mAVControlManager.getAVControlServiceDisplayOutput().getCECStatus();
        SLog.d(TESTTAG, "getCECStatus end result : [" + getCECStatus + "]");

        return getCECStatus;
    }

    /**
     * SW 업데이트
     */
    View.OnClickListener onSWUpdateClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SLog.d(TAG, "onSWUpdateClickListener onClick" +v.getId());
            Toast.makeText(getApplicationContext(),"SWUpdate",Toast.LENGTH_LONG).show();

            SLog.d(TESTTAG, "setLedState start");
            boolean setLedState = mPeripheralManager.getPeripheralServiceLEDControl().setLedState(0,0);
            SLog.d(TESTTAG, "setLedState end result : [" + setLedState + "]");

            SLog.d(TESTTAG, "sleep start");
            boolean sleep = mSystemManager.getSystemServicePower().sleep();
            SLog.d(TESTTAG, "sleep end result : [" + sleep + "]");
        }
    };

    /**
     * 리모컨 한글 키패드 설정
     */
    RadioButton.OnClickListener onKeypadClickListener = new RadioButton.OnClickListener(){
        @Override
        public void onClick(View v) {
            SLog.d(TAG, "onKeypadClickListener onClick" +v.getId());
            int id = v.getId();
            if(id == R.id.radio_keypad_new){
                setKeypad(0);
                Toast.makeText(getApplicationContext(),"keypad_new",Toast.LENGTH_LONG).show();
            }else if(id == R.id.radio_keypad_original){
                setKeypad(1);
                Toast.makeText(getApplicationContext(),"keypad_original",Toast.LENGTH_LONG).show();
            }
        }
    };

    /**
     * 리모컨 한글 키패드 설정
     *
     * @param type 0:신규, 1:기존
     */
    private void setKeypad(int type){
        SLog.d(TAG, "setKeypad");
        mRadioKeypadNew.setChecked(false);
        mRadioKeypadOriginal.setChecked(false);
        if(type == 0){
            mRadioKeypadNew.setChecked(true);
        }else if(type == 1){
            mRadioKeypadOriginal.setChecked(true);
        }
        mKeypadType = type;
    }

    /**
     * 셋톱박스 자동 리부팅 설정
     */
    RadioButton.OnClickListener onStbAutoRebootClickListener = new RadioButton.OnClickListener(){
        @Override
        public void onClick(View v) {
            SLog.d(TAG, "onStbAutoRebootClickListener onClick" +v.getId());
            int id = v.getId();
            if(id == R.id.radio_stb_auto_reboot_enable){
                setStbAutoReboot(0);
                Toast.makeText(getApplicationContext(),"stb_auto_reboot_enable",Toast.LENGTH_LONG).show();
            }else if(id == R.id.radio_stb_auto_reboot_disable){
                setStbAutoReboot(1);
                Toast.makeText(getApplicationContext(),"stb_auto_reboot_disable",Toast.LENGTH_LONG).show();
            }
        }
    };

    /**
     * 셋톱박스 자동 리부팅 설정
     *
     * @param type 0:사용함, 1:사용안함
     */
    private void setStbAutoReboot(int type){
        SLog.d(TAG, "setStbAutoReboot");
        mRadioStbAutoRebootEnable.setChecked(false);
        mRadioStbAutoRebootDisable.setChecked(false);
        if(type == 0){
            mRadioStbAutoRebootEnable.setChecked(true);
        }else if(type == 1){
            mRadioStbAutoRebootDisable.setChecked(true);
        }
        mStbAutoReboot = type;
    }

    /**
     * 공장 초기화
     */
    View.OnClickListener onFactoryResetClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SLog.d(TAG, "onFactoryResetClickListener onClick" +v.getId());
            Toast.makeText(getApplicationContext(),"factory_reset",Toast.LENGTH_LONG).show();

            SLog.d(TESTTAG, "factoryReset start");
            boolean factoryReset = mSystemManager.getSystemServiceDeviceInfo().factoryReset();
            SLog.d(TESTTAG, "factoryReset end result : [" + factoryReset + "]");
        }
    };

    /**
     * 최적화
     */
    View.OnClickListener onDefragmentMemoryClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SLog.d(TAG, "onDefragmentMemoryClickListener onClick" +v.getId());
            Toast.makeText(getApplicationContext(),"defragment_memory",Toast.LENGTH_LONG).show();

            SLog.d(TESTTAG, "defragmentMemory start");
            mSystemManager.getSystemServiceMemory().optimizeMemory();
            SLog.d(TESTTAG, "defragmentMemory end result : ");
        }
    };

    /**
     * 기기정보 설정
     */
    private void setStbInfo(){
        SLog.d(TAG, "setStbInfo");
        try {
            SLog.d(TESTTAG, "getModelName start");
            String model = mSystemManager.getSystemServiceDeviceInfo().getModelName(); // 모델명
            SLog.d(TESTTAG, "getModelName end result : [" + model + "]");

            String userId = ""; // 사용자ID

            SLog.d(TESTTAG, "getFirmwareVersion start");
            String swVersion = mSystemManager.getSystemServiceDeviceInfo().getFirmwareVersion(); // SW버전
            SLog.d(TESTTAG, "getFirmwareVersion end result : [" + swVersion + "]");

            SLog.d(TESTTAG, "getMacAddress start");
            String mac = mNetworkManager.getNetworkServiceEthernet().getMacAddress(); // MAC
            SLog.d(TESTTAG, "getMacAddress end result : [" + mac + "]");

            String xpg = ""; // XPG버전

            SLog.d(TESTTAG, "getFreeDispSpace start");
            String getFreeDispSpace = mSystemManager.getSystemServiceStorage().getFreeDiskSpace(); // 미 사용중인 용량
            SLog.d(TESTTAG, "getFreeDispSpace end result : [" + getFreeDispSpace + "]");

            SLog.d(TESTTAG, "getTotalDiskSpace start");
            String getTotalDiskSpace = mSystemManager.getSystemServiceStorage().getTotalDiskSpace(); // 전체 용량
            SLog.d(TESTTAG, "getTotalDiskSpace end result : [" + getTotalDiskSpace + "]");

            String disk = getFreeDispSpace + "/" + getTotalDiskSpace + "GB"; // 용량

            SLog.d(TESTTAG, "getFirmwareUpdateDate start");
            String swBuildDate = mSystemManager.getSystemServiceDeviceInfo().getFirmwareUpdateDate(); // SW빌드Date
            SLog.d(TESTTAG, "getFirmwareUpdateDate end result : [" + swBuildDate + "]");

            StringBuffer sb = new StringBuffer();
            sb.append("model : " + model + "\n");
            sb.append("userId : " + userId + "\n");
            sb.append("SWVersion : " + swVersion + "\n");
            sb.append("mac : " + mac + "\n");
            sb.append("xpg : " + xpg + "\n");
            sb.append("disk : " + disk + "\n");
//            sb.append("ip : " + mNetworkInfo.getmIp() + "\n");
//            sb.append("Gateway : " + mNetworkInfo.getmGateway() + "\n");
//            sb.append("dns1 : " + mNetworkInfo.getmDns1() + "\n");
//            sb.append("Subnet : " + mNetworkInfo.getmSubnet() + "\n");
//            sb.append("dns2 : " + mNetworkInfo.getmDns2() + "\n");
            sb.append("swBuildDate : " + swBuildDate + "\n");

            mTextStbInfo.setText(sb.toString());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
