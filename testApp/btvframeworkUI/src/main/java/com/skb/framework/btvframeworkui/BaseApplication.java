package com.skb.framework.btvframeworkui;


import android.app.Application;
import android.os.Environment;

import com.skb.btv.framework.log.SLog;

import java.io.File;


/**
 * Created by jangjaehun on 2017. 3. 21..
 * MultiDexApplication
 * ref : http://wtwoo.tistory.com/entry/%EB%A9%94%EC%86%8C%EB%93%9C-%EA%B0%9C%EC%88%98-%EC%A0%9C%ED%95%9C-65K-%ED%95%B4%EA%B2%B0-%EB%B0%A9%EB%B2%95
 */

public class BaseApplication extends Application {

    private static final String TAG =  "BaseApplication";

    private static BaseApplication globalContext;

    @Override
    public void onCreate() {
        super.onCreate();
        SLog.d(TAG, "onCreate");
        globalContext = this;

        init();

    }

    private void init() {
        SLog.d(TAG, "init");
        String rootPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        File dataDirectory = new File(rootPath+"/Android/data/android.sptek.stb.iptvplayer/");
        if(!dataDirectory.exists()) {
            if(dataDirectory.mkdirs()) {
                SLog.i(TAG, "dataDirectory make success");
            }
        }
    }

    public static BaseApplication getGlobalContext() {
        SLog.d(TAG, "getGlobalContext");
        return globalContext;
    }



    @Override
    public void onLowMemory() {
        super.onLowMemory();
        SLog.d(TAG, "onLowMemory");
    }

}
