package com.skb.framework.btvframeworkui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.skb.btv.framework.core.BtvFramework;
import com.skb.btv.framework.core.IBtvFramework;
import com.skb.btv.framework.navigator.DvbSIService;
import com.skb.btv.framework.navigator.IBtvNavigator;
import com.skb.btv.framework.navigator.OnDvbSIListener;
import com.skb.btv.framework.navigator.dvbservice.AVDvbService;
import com.skb.btv.framework.navigator.dvbservice.AVDvbServiceList;
import com.skb.btv.framework.navigator.dvbservice.MultiViewDvbService;
import com.skb.btv.framework.navigator.dvbservice.MultiViewDvbServiceList;
import com.skb.btv.framework.navigator.dvbservice.MultiViewLogicalCell;
import com.skb.btv.framework.navigator.dvbservice.MultiViewLogicalCellList;
import com.skb.btv.framework.navigator.dvbservice.MusicDvbService;
import com.skb.btv.framework.navigator.dvbservice.MusicDvbServiceList;
import com.skb.btv.framework.navigator.product.Product;
import com.skb.btv.framework.navigator.product.ProductList;
import com.skb.btv.framework.navigator.program.AVProgramList;
import com.skb.btv.framework.navigator.program.AVPrograms;
import com.skb.btv.framework.navigator.program.MusicProgramList;
import com.skb.btv.framework.navigator.program.MusicProgram;
import com.skb.btv.framework.navigator.program.AVProgram;
import com.skb.btv.framework.log.SLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class TestDvdsi extends Activity implements View.OnClickListener{

    private final static String TESTTAG = "BtvTest";

    BtvFramework btvFramework = null;
    //DvbServiceManager mDvbSIManager = null;
    private DvbSIService mDvbSIService;
    private List<MultiViewLogicalCell> logicalCellList;

    private OnDvbSIListener onDvbSIListener = new OnDvbSIListener(){
        @Override
        public void onDvbSIUpdated(int dvbsiType, final String extraData) {
            SLog.d(TESTTAG, "[Result] onDvbSIUpdated() : "+extraData);
            if(dvbsiType == IBtvFramework.DVBSI_TYPE_UPDATE_CHANNEL){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String channelListVersion ="";
                        String extraString = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            extraString = json.getString(IBtvFramework.DVBSI_JSONOBJECT_INFO);
                            channelListVersion = json.getString(IBtvFramework.DVBSI_JSONOBJECT_DVBSERVICEVERSION);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(mDvbSIService != null){
                            AVDvbServiceList channelList = mDvbSIService.getAVDvbServiceList();
                            int channelType =  channelList.getList().get(0).getChannelType();
                            if(channelList != null && channelList.isReady()){
                                // TODO update av channel
                            }
                        }
                    }
                });
            }else if(dvbsiType == IBtvFramework.DVBSI_TYPE_UPDATE_PROGRAMS){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String extraString = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            extraString = json.getString(IBtvFramework.DVBSI_JSONOBJECT_INFO);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(mDvbSIService != null){
                            int currentChannelNumber = 3; // current channel
                            AVProgramList avProgramList = mDvbSIService.getAVProgramList(currentChannelNumber);
                            if(avProgramList != null && avProgramList.isReady()){
                                // TODO update program av channel
                            }
                        }
                    }
                });
            }else if(dvbsiType == IBtvFramework.DVBSI_TYPE_UPDATE_MUSIC_CHANNEL){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String extraString = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            extraString = json.getString(IBtvFramework.DVBSI_JSONOBJECT_INFO);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(mDvbSIService != null){
                            MusicDvbServiceList channelList = mDvbSIService.getMusicDvbServiceList();
                            if(channelList != null && channelList.isReady()){
                                // TODO update btv music channel
                            }
                        }
                    }
                });
            } else if(dvbsiType == IBtvFramework.DVBSI_TYPE_UPDATE_MUSIC_PROGRAMS) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int audioid = -1;
                        String extraString = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            extraString = json.getString(IBtvFramework.DVBSI_JSONOBJECT_INFO);
                            audioid = json.getInt(IBtvFramework.DVBSI_JSONOBJECT_AUDIOPID);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(mDvbSIService != null){
                            List<MusicProgram> musicprogram_result = mDvbSIService.getMusicProgramList(audioid).getMusicProgramList();
                            if (musicprogram_result != null && musicprogram_result.size() > 0) {
                                // TODO update music channel program
                            }
                        }
                    }
                });

            }else if(dvbsiType == IBtvFramework.DVBSI_TYPE_UPDATE_MULTI_CHANNELS){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String extraString = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            extraString = json.getString(IBtvFramework.DVBSI_JSONOBJECT_INFO);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(mDvbSIService != null){
                            MultiViewDvbServiceList multiViewDvbServiceChList = mDvbSIService.getMultiViewDvbServiceList();
                            if(multiViewDvbServiceChList != null && multiViewDvbServiceChList.isReady()){
                                // TODO update multiview channel
                            }

                        }
                    }
                });
            }else if(dvbsiType == IBtvFramework.DVBSI_TYPE_CHANGE_PROGRAM){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int channel_num = -1;
                        String extraString = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            extraString = json.getString(IBtvFramework.DVBSI_JSONOBJECT_INFO);
                            channel_num = json.getInt(IBtvFramework.DVBSI_JSONOBJECT_CHANNEL_NUM);
                        }catch(JSONException e){
                            e.printStackTrace();
                        }
                        // TODO change only one program
                    }
                });
            } else if(dvbsiType == IBtvFramework.DVBSI_TYPE_CHANGE_PROGRAMS){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String sid_num = "";
                        String extraString = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            extraString= json.getString(IBtvFramework.DVBSI_JSONOBJECT_INFO);
                            sid_num = json.getString(IBtvNavigator.DVBSI_JSONOBJECT_SID_NUM);//IBtvFramework.DVBSI_JSONOBJECT_CHANNEL_NUM);
                        }catch(JSONException e){
                            e.printStackTrace();
                        }

                        if(sid_num.isEmpty() == false) {
                            // TODO change multi program
                        }else {
                            // TODO Product list
                        }
                    }
                });
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_test_dvdsi);

        btvFramework = BtvFramework.getInstance(this);
        //mDvbSIManager = (DvbServiceManager)btvFramework.getBtvService(BtvFramework.DVBSI_SERVICE);
        //mDvbSIManager.setOnDvbSIListener(onDvbSIListener);
        mDvbSIService = DvbSIService.getInstance(this);
        mDvbSIService.setOnDvbSIListener(onDvbSIListener);
        SLog.d(TESTTAG, "setOnDvbSIListener() Start");

        Button dvdsi_back = (Button) findViewById(R.id.dvdsi_back);
        dvdsi_back.setOnClickListener(this);

        Button dvdsi_getcahnnellist = (Button) findViewById(R.id.dvdsi_getchannellist);
        Button dvdsi_getMusicChannelList = (Button) findViewById(R.id.dvdsi_getmusicchannelList);
        Button dvdsi_getMultiViewChannelList = (Button) findViewById(R.id.dvdsi_getMultiViewList);
        Button dvdsi_getchannelsid = (Button)findViewById(R.id.dvdsi_getchannelsid);
        Button dvdsi_getlogicalcelllist = (Button)findViewById(R.id.dvdsi_getlogicalcelllist);
        Button dvdsi_makemultiviewuri = (Button)findViewById(R.id.dvdsi_makemultiviewuri);
        Button dvdsi_setmultichannel = (Button)findViewById(R.id.dvdsi_setmultichannel);
        Button dvdsi_getProductList = (Button) findViewById(R.id.dvdsi_getProductList);
        Button dvdsi_getprograminfoofchannel = (Button)findViewById(R.id.dvdsi_getprograminfoofchannel);
        Button dvdsi_getguideinfoofcurrentchannel = (Button)findViewById(R.id.dvdsi_getguideinfoofcurrentchannel);
        Button dvdsi_getguideinfocurrenttime = (Button)findViewById(R.id.dvdsi_getguideinfocurrenttime);
        Button dvdsi_getpartialguidelistbyindex = (Button)findViewById(R.id.dvdsi_getpartialguidelistbyindex);
        Button dvdsi_getmusicchannelprogramlistfrompid = (Button)findViewById(R.id.dvdsi_getmusicchannelprogramlistfrompid);
        Button dvdsi_getmultichannelprogramlist = (Button)findViewById(R.id.dvdsi_getmultichannelprogramlist);
        Button dvdsi_setregioncode = (Button)findViewById(R.id.dvdsi_setregioncode);
        Button dvdsi_setsegid = (Button)findViewById(R.id.dvdsi_setsegid);

        dvdsi_getcahnnellist.setOnClickListener(this);
        dvdsi_getMusicChannelList.setOnClickListener(this);
        dvdsi_getMultiViewChannelList.setOnClickListener(this);
        dvdsi_getchannelsid.setOnClickListener(this);
        dvdsi_getlogicalcelllist.setOnClickListener(this);
        dvdsi_makemultiviewuri.setOnClickListener(this);
        dvdsi_setmultichannel.setOnClickListener(this);
        dvdsi_getProductList.setOnClickListener(this);
        dvdsi_getprograminfoofchannel.setOnClickListener(this);
        dvdsi_getguideinfoofcurrentchannel.setOnClickListener(this);
        dvdsi_getguideinfocurrenttime.setOnClickListener(this);
        dvdsi_getpartialguidelistbyindex.setOnClickListener(this);
        dvdsi_getmusicchannelprogramlistfrompid.setOnClickListener(this);
        dvdsi_getmultichannelprogramlist.setOnClickListener(this);
        dvdsi_setregioncode.setOnClickListener(this);
        dvdsi_setsegid.setOnClickListener(this);
    }

    @Override
    protected void onDestroy(){
        if(mDvbSIService != null) {
            mDvbSIService.clearOnDvbSIListener(onDvbSIListener);
            SLog.d(TESTTAG, "clearOnDvbSIListener() End");
        }
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.dvdsi_back:
                finish();
                break;

            case R.id.dvdsi_getchannellist:
                SLog.d(TESTTAG, "1. 채널정보 가져오기 Start");
                AVDvbServiceList channelList = mDvbSIService.getAVDvbServiceList();
                if(channelList != null && channelList.isReady()){
                    List<AVDvbService> channel = channelList.getList();
                    if(channel != null && channel.size() > 0) {
                        for (AVDvbService entity : channel) {
                            SLog.d(TESTTAG, "채널 정보 [CH : " + entity.getCh() + "] "
                                    + "channel type : " + entity.getChannelType() + ", sid : " + entity.getSid()
                                    + ", sub ch : " + entity.getSubChannel() + ", " + entity.getName()
                                    + ", virtual vod url : " + entity.getVirtualVodUri());
                        }
                        Toast.makeText(this, "채널 정보("+channel.size()+")를 획득하였습니다.", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(this, "데이터 로딩중입니다. 몇 초 이후에 시도해주세요", Toast.LENGTH_SHORT).show();
                }
                SLog.d(TESTTAG, "1. 채널 정보 획득 End");
                break;

            case R.id.dvdsi_getmusicchannelList:
                SLog.d(TESTTAG, "2. Btv 뮤직 채널 가져오기  Start");
                MusicDvbServiceList musicChannelList = mDvbSIService.getMusicDvbServiceList();
                if(musicChannelList != null && musicChannelList.isReady()){
                    List<MusicDvbService> musicChannel = musicChannelList.getList();
                    if(musicChannel != null && musicChannel.size() > 0){
                        for(MusicDvbService entity : musicChannel) {
                            SLog.d(TESTTAG, "Btv 뮤직채널 [audio id : " + entity.getApid() + "] "
                                    + "audio ch : " + entity.getCh() + ", audio ch type : "
                                    + entity.getAudioChannelType() + ", " + entity.getTitle());
                        }
                        Toast.makeText(this, "BTV 뮤직 채널("+musicChannel.size()+")를 획득하였습니다.", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(this, "데이터 로딩중입니다. 몇 초 이후에 시도해주세요", Toast.LENGTH_SHORT).show();
                }
                SLog.d(TESTTAG, "2. Btv 뮤직 채널 가져오기 End");
                break;

            case R.id.dvdsi_getMultiViewList:
                SLog.d(TESTTAG, "3. 멀티뷰 채널 가져오기 Start");
                MultiViewDvbServiceList multiviewList = mDvbSIService.getMultiViewDvbServiceList();
                if(multiviewList != null && multiviewList.isReady()){
                    List<MultiViewDvbService> multiviewChannel = multiviewList.getList();
                    if(multiviewChannel != null && multiviewChannel.size() > 0){
                        for(MultiViewDvbService entity : multiviewChannel) {
                            SLog.d(TESTTAG, "멀티뷰 채널 [SID : " + entity.getSid() + "] " + entity.getGroupName());
                        }
                        Toast.makeText(this, "멀티뷰 채널("+multiviewChannel.size()+")를 획득하였습니다.", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(this, "데이터 로딩중입니다. 몇 초 이후에 시도해주세요", Toast.LENGTH_SHORT).show();
                }
                SLog.d(TESTTAG, "3. 멀티뷰 채널 가져오기 End");
                break;

            case R.id.dvdsi_getchannelsid:
                {
                    SLog.d(TESTTAG, "4. 채널의 sid 가져오기 Start");
                    Toast.makeText(TestDvdsi.this, "지원하지 않는 기능입니다.", Toast.LENGTH_SHORT).show();
                    /*LayoutInflater li = LayoutInflater.from(TestDvdsi.this);
                    View promptsView = li.inflate(R.layout.dialog_input_channel, null);
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TestDvdsi.this, R.style.AlertDialogCustom);
                    alertDialogBuilder.setView(promptsView);

                    final EditText userInput = (EditText) promptsView.findViewById(R.id.Input_ch);
                    userInput.setInputType(0);
                    alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        int returnSid = mDvbSIService.getDvbServiceSid(Integer.valueOf(userInput.getText().toString()));
                                        SLog.d(TESTTAG, "getChannelSid() [SID : " + returnSid + "]");
                                        Toast.makeText(TestDvdsi.this, "Sid(" + returnSid + ")를 획득하였습니다.", Toast.LENGTH_SHORT).show();
                                        SLog.d(TESTTAG, "4. 채널의 sid 가져오기 End");
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        SLog.d(TESTTAG, "4. 채널의 sid 가져오기 End");
                                        dialog.cancel();
                                    }
                                });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();*/
                }
                break;

            case R.id.dvdsi_getlogicalcelllist:
            {
                SLog.d(TESTTAG, "5. 멀티뷰 상세채널 Start");
                LayoutInflater li = LayoutInflater.from(TestDvdsi.this);
                View promptsView = li.inflate(R.layout.dialog_input_channel, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TestDvdsi.this, R.style.AlertDialogCustom);
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView.findViewById(R.id.Input_ch);
                userInput.setInputType(0);
                TextView tv = (TextView)promptsView.findViewById(R.id.textView1);
                tv.setText("멀티채널 101~105 중에 하나를 입력해주세요");
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        MultiViewLogicalCellList multiViewLogicalCellList = mDvbSIService.getMultiViewLogicalCellList(Integer.valueOf(userInput.getText().toString()));
                                        if(multiViewLogicalCellList != null && multiViewLogicalCellList.isReady()){
                                            logicalCellList = multiViewLogicalCellList.getMultiViewLogicalCellList();
                                            if(logicalCellList != null && logicalCellList.size() > 0){
                                                for(MultiViewLogicalCell entity : logicalCellList){
                                                    SLog.d(TESTTAG, "멀티뷰 상세채널() [Ch : "
                                                            + entity.getChannelNumber() + "] " + entity.getCellName() + ", service id : " + entity.getServiceID());
                                                }
                                                Toast.makeText(TestDvdsi.this, "멀티뷰 서브 채널("+logicalCellList.size()+")를 획득하였습니다.", Toast.LENGTH_SHORT).show();
                                            }
                                        }else {
                                            Toast.makeText(TestDvdsi.this, "데이터 로딩중입니다. 몇 초 이후에 시도해주세요", Toast.LENGTH_SHORT).show();
                                        }
                                        SLog.d(TESTTAG, "5. 멀티뷰 상세채널 End");
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        SLog.d(TESTTAG, "5. 멀티뷰 상세채널 End");
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
            break;

            case R.id.dvdsi_makemultiviewuri:
                SLog.d(TESTTAG, "6. 멀티뷰 URI 생성 Start");
                if(logicalCellList != null && logicalCellList.size() > 0){
                    String multiview_uri = mDvbSIService.makeMultiViewUri(logicalCellList);
                    SLog.d(TESTTAG, "멀티뷰 URI [URI] : " + multiview_uri );
                    Toast.makeText(TestDvdsi.this, "URI 생성이 완료되었습니다.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(TestDvdsi.this, "5번 테스트를 진행한 이후 시도해주세요", Toast.LENGTH_SHORT).show();
                }
                SLog.d(TESTTAG, "6. 멀티뷰 URI 생성 End");
                break;

            case R.id.dvdsi_setmultichannel:
                SLog.d(TESTTAG, "7. 현재 SID 정보 설정(11|12|240|133) Start");//5|7|9|11) Start");
                mDvbSIService.setCurrentMultiChannel("11|12|240|133");//"5|7|9|11");
                SLog.d(TESTTAG, "setCurrentViewChannel() [sid List] : " + "11|12|240|133");//"5|7|9|11");
                Toast.makeText(TestDvdsi.this, "채널을 설정하였습니다.", Toast.LENGTH_SHORT).show();
                SLog.d(TESTTAG, "7. 현재 SID 정보 설정 End");
                break;

            case R.id.dvdsi_getProductList:
                SLog.d(TESTTAG, "8. Product 가져오기 Start");
                ProductList productList = mDvbSIService.getProductList();
                if(productList != null && productList.isReady()){
                    List<Product> products = productList.getProductList();
                    if(products != null && products.size() > 0){
                        for(Product entity : products) {
                            SLog.d(TESTTAG, "Product List[code : " + entity.getCode() + "] " + entity.getName());
                        }
                        Toast.makeText(this, "제품군("+products.size()+")를 획득하였습니다.", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(this, "데이터 로딩중입니다. 몇 초 이후에 시도해주세요", Toast.LENGTH_SHORT).show();
                }
                SLog.d(TESTTAG, "8. Product 가져오기 End");
                break;

            case R.id.dvdsi_getprograminfoofchannel:
            {
                SLog.d(TESTTAG, "9. 채널의 프로그램 정보 Start");
                LayoutInflater li = LayoutInflater.from(TestDvdsi.this);
                View promptsView = li.inflate(R.layout.dialog_input_channel, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TestDvdsi.this, R.style.AlertDialogCustom);
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView.findViewById(R.id.Input_ch);
                userInput.setInputType(0);
                TextView tv = (TextView)promptsView.findViewById(R.id.textView1);
                tv.setText("SID 를 입력하세요");//"채널을 입력하세요");
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        AVProgramList avProgramList = mDvbSIService.getAVProgramList(Integer.valueOf(userInput.getText().toString()));
                                        if(avProgramList != null && avProgramList.isReady()){
                                            List<AVPrograms> programs = avProgramList.getAVProgramsList();
                                            if(programs != null && programs.size() > 0){
                                                SLog.d(TESTTAG, "================ [SID : " + userInput.getText().toString() + "]============================");
                                                for(AVProgram entity : programs.get(0).getAVProgramList()){
                                                    SLog.d(TESTTAG, "프로그램 정보() [Title : " + entity.getName() + "] " + entity.getActors() + " vodid : " + entity.getVodId()
                                                            + ", start : " + entity.getStartTime() + ", end : " + entity.getEndTime() + " [linkinfo startTime " + entity.getAVProgramLinkInfo().get(0).getDisplayStartDate() + " endTime " + entity.getAVProgramLinkInfo().get(0).getDisplayEndDate() + " linkedService "+ entity.getAVProgramLinkInfo().get(0).getLinkedServiceText() + "] ");
                                                }
                                                Toast.makeText(TestDvdsi.this, "채널의 프로그램 정보("+programs.get(0).getAVProgramList().size()+")를 획득하였습니다.", Toast.LENGTH_SHORT).show();
                                            }else {
                                                Toast.makeText(TestDvdsi.this, "데이터 로딩중이거나 없는 채널입니다. 몇 초 이후에 시도해주세요", Toast.LENGTH_SHORT).show();
                                            }
                                        }else {
                                            Toast.makeText(TestDvdsi.this, "데이터 로딩중이거나 없는 채널입니다. 몇 초 이후에 시도해주세요", Toast.LENGTH_SHORT).show();
                                        }
                                        SLog.d(TESTTAG, "9. 채널의 프로그램 정보 End");
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        SLog.d(TESTTAG, "9. 채널의 프로그램 정보 End");
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
            break;

            case R.id.dvdsi_getguideinfoofcurrentchannel:
            {
                SLog.d(TESTTAG, "10. 현재 시간 이후 채널 프로그램 가져오기 Start");
                LayoutInflater li = LayoutInflater.from(TestDvdsi.this);
                View promptsView = li.inflate(R.layout.dialog_input_channel, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TestDvdsi.this, R.style.AlertDialogCustom);
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView.findViewById(R.id.Input_ch);
                userInput.setInputType(0);
                TextView tv = (TextView)promptsView.findViewById(R.id.textView1);
                tv.setText("SID 를 입력하세요");//"채널을 입력하세요");
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        AVProgramList avProgramList = mDvbSIService.getCurrentAVProgram(Integer.valueOf(userInput.getText().toString()));
                                        if(avProgramList != null && avProgramList.isReady()){
                                            List<AVPrograms> programs = avProgramList.getAVProgramsList();
                                            if(programs != null && programs.size() > 0){
                                                SLog.d(TESTTAG, "================ [SID : " + userInput.getText().toString() + "]============================");
                                                for(AVProgram entity : programs.get(0).getAVProgramList()){
                                                    SLog.d(TESTTAG, "프로그램 정보() [Title : " + entity.getName() + "] " + entity.getActors()
                                                            + ", start : " + entity.getStartTime() + ", end : " + entity.getEndTime());
                                                }
                                                Toast.makeText(TestDvdsi.this, "채널의 프로그램 정보("+programs.get(0).getAVProgramList().size()+")를 획득하였습니다.", Toast.LENGTH_SHORT).show();
                                            }else {
                                                Toast.makeText(TestDvdsi.this, "데이터 로딩중이거나 없는 채널입니다. 몇 초 이후에 시도해주세요", Toast.LENGTH_SHORT).show();
                                            }
                                        }else {
                                            Toast.makeText(TestDvdsi.this, "데이터 로딩중이거나 없는 채널입니다. 몇 초 이후에 시도해주세요", Toast.LENGTH_SHORT).show();
                                        }
                                        SLog.d(TESTTAG, "10. 현재 시간 이후 채널 프로그램 가져오기 End");
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        SLog.d(TESTTAG, "10. 현재 시간 이후 채널 프로그램 가져오기 End");
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
            break;

            case R.id.dvdsi_getguideinfocurrenttime:
            {
                SLog.d(TESTTAG, "11. 시간 범위내(~5시간) 채널프로그램 가져오기 Start");
                LayoutInflater li = LayoutInflater.from(TestDvdsi.this);
                View promptsView = li.inflate(R.layout.dialog_input_channel, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TestDvdsi.this, R.style.AlertDialogCustom);
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView.findViewById(R.id.Input_ch);
                TextView tv = (TextView)promptsView.findViewById(R.id.textView1);
                tv.setText("SID 를 입력하세요");//"채널을 입력하세요");
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Date startTime = Calendar.getInstance().getTime();
                                        Date endTime = new Date(startTime.getTime()+(5*60*60*1000));
                                        AVProgramList avProgramList = mDvbSIService.getMultiAVProgramListByTime(userInput.getText().toString(), startTime, endTime );
                                        if(avProgramList != null && avProgramList.isReady()){
                                            List<AVPrograms> programs = avProgramList.getAVProgramsList();
                                            if(programs != null && programs.size() > 0){
                                                SLog.d(TESTTAG, "================ [SID : " + userInput.getText().toString() + "]============================");
                                                for(AVProgram entity : programs.get(0).getAVProgramList()){
                                                    SLog.d(TESTTAG, "프로그램 정보() [Title : " + entity.getName() + "] " + entity.getActors()
                                                            + ", start : " + entity.getStartTime() + ", end : " + entity.getEndTime());
                                                }
                                                Toast.makeText(TestDvdsi.this, "채널의 프로그램 정보("+programs.get(0).getAVProgramList().size()+")를 획득하였습니다.", Toast.LENGTH_SHORT).show();
                                            }else {
                                                Toast.makeText(TestDvdsi.this, "데이터 로딩중이거나 없는 채널입니다. 몇 초 이후에 시도해주세요", Toast.LENGTH_SHORT).show();
                                            }
                                        }else {
                                            Toast.makeText(TestDvdsi.this, "데이터 로딩중이거나 없는 채널입니다. 몇 초 이후에 시도해주세요", Toast.LENGTH_SHORT).show();
                                        }
                                        SLog.d(TESTTAG, "11. 시간 범위내(~5시간) 채널프로그램 가져오기 End");
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        SLog.d(TESTTAG, "11. 시간 범위내(~5시간) 채널프로그램 가져오기 End");
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
            break;

            case R.id.dvdsi_getpartialguidelistbyindex:
            {
                SLog.d(TESTTAG, "12. 현재시간 요청한갯수(5)만큼 채널프로그램 가져오기 Start");
                LayoutInflater li = LayoutInflater.from(TestDvdsi.this);
                View promptsView = li.inflate(R.layout.dialog_input_channel, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TestDvdsi.this, R.style.AlertDialogCustom);
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView.findViewById(R.id.Input_ch);
                TextView tv = (TextView)promptsView.findViewById(R.id.textView1);
                tv.setText("SID 를 입력하세요");
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        AVProgramList avProgramList = mDvbSIService.getPartialProgramListByCount(Integer.valueOf(userInput.getText().toString()), 5 );
                                        if(avProgramList != null && avProgramList.isReady()){
                                            List<AVPrograms> programs = avProgramList.getAVProgramsList();
                                            if(programs != null && programs.size() > 0){
                                                SLog.d(TESTTAG, "================ [SID : " + userInput.getText().toString() + "]============================");
                                                for(AVProgram entity : programs.get(0).getAVProgramList()){
                                                    SLog.d(TESTTAG, "프로그램 정보() [Title : " + entity.getName() + "] " + entity.getActors()
                                                            + ", start : " + entity.getStartTime() + ", end : " + entity.getEndTime());
                                                }
                                                Toast.makeText(TestDvdsi.this, "채널의 프로그램 정보("+programs.get(0).getAVProgramList().size()+")를 획득하였습니다.", Toast.LENGTH_SHORT).show();
                                            }else {
                                                Toast.makeText(TestDvdsi.this, "데이터 로딩중이거나 없는 채널입니다. 몇 초 이후에 시도해주세요", Toast.LENGTH_SHORT).show();
                                            }
                                        }else {
                                            Toast.makeText(TestDvdsi.this, "데이터 로딩중이거나 없는 채널입니다. 몇 초 이후에 시도해주세요", Toast.LENGTH_SHORT).show();
                                        }
                                        SLog.d(TESTTAG, "12. 현재시간 요청한갯수(5)만큼 채널프로그램 가져오기 End");
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        SLog.d(TESTTAG, "12. 현재시간 요청한갯수(5)만큼 채널프로그램 가져오기 End");
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
            break;

            case R.id.dvdsi_getmusicchannelprogramlistfrompid:
                SLog.d(TESTTAG, "13. 뮤직 채널(2911) 프로그램 Start");
                MusicProgramList musicProgramList = mDvbSIService.getMusicProgramList(2901);
                SLog.d(TESTTAG, "musicProgramList : " + musicProgramList + ", is ready : "
                        + ((musicProgramList != null) ? musicProgramList.isReady() : "null"));
                if(musicProgramList != null && musicProgramList.isReady()){
                    List<MusicProgram> musicPrograms = musicProgramList.getMusicProgramList();
                    if(musicPrograms != null && musicPrograms.size() > 0){
                        for(MusicProgram entity : musicPrograms) {
                            SLog.d(TESTTAG, "최신 인기가요 프로그램 [Title : " + entity.getName() + "] [Singer : " + entity.getSinger());
                        }
                        Toast.makeText(TestDvdsi.this, "최신 인기가요 프로그램 획득("+musicPrograms.size()+")", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(TestDvdsi.this, "Btv 뮤직 프로그램가 없거나 로딩중입니다.", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(TestDvdsi.this, "Btv 뮤직 프로그램가 없거나 로딩중입니다.", Toast.LENGTH_SHORT).show();
                }
                SLog.d(TESTTAG, "13. 뮤직 채널(2911) 프로그램 End");
                break;

            case R.id.dvdsi_getmultichannelprogramlist:

                SLog.d(TESTTAG, "14. 멀티 채널 프로그램 Start");
                String chList = "";
                if(logicalCellList != null && logicalCellList.size() > 0){
                    for(MultiViewLogicalCell entity : logicalCellList){
                        chList+=""+entity.getServiceID()+"|";
                    }
                    AVProgramList avProgramList = mDvbSIService.getMultiAVProgramList(chList);
                    if(avProgramList != null && avProgramList.isReady()) {
                        List<AVPrograms> multiPrograms = avProgramList.getAVProgramsList();
                        if (multiPrograms != null && multiPrograms.size() > 0) {
                            for (int i = 0; i < multiPrograms.size(); i++) {
                                int ch = multiPrograms.get(i).getSid();
                                List<AVProgram> avProgram = multiPrograms.get(i).getAVProgramList();
                                SLog.d(TESTTAG, "============================ [CH : " + ch + "]=============================");
                                if (avProgram != null) {
                                    for (AVProgram entity : avProgram) {
                                        SLog.d(TESTTAG, "채널프로그램 [ch : " + ch + "] : " + entity.getName());
                                    }
                                }
                            }
                            Toast.makeText(TestDvdsi.this, "다중 채널의 프로그램 정보 (" + multiPrograms.size() + ")를 획득하였습니다.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(TestDvdsi.this, "데이터를 로딩중이거나 데이터가 존재하지 않습니다.", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(TestDvdsi.this, "데이터를 로딩중이거나 데이터가 존재하지 않습니다", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(TestDvdsi.this, "멀티뷰 채널 관련 테스트 메뉴를 진행한 이후 시도해주세요", Toast.LENGTH_SHORT).show();
                }
                 SLog.d(TESTTAG, "14. 멀티 채널 프로그램 End");
                break;

            case R.id.dvdsi_setregioncode:
                SLog.d(TESTTAG, "15. region code 설정 Start");
                mDvbSIService.setRegionCode("1|41|61|231|200|260");
                SLog.d(TESTTAG, "setRegionCode() [region Code : " + "1|41|61|231|200|260" + "]");
                Toast.makeText(this, "region code 설정되었습니다.", Toast.LENGTH_SHORT).show();
                mDvbSIService.setRegionCode("Reset");
                SLog.d(TESTTAG, "15. region code 설정 End");
                break;

            case R.id.dvdsi_setsegid:
                SLog.d(TESTTAG, "16. segid 설정 Start");
                mDvbSIService.setSegId(1);
                SLog.d(TESTTAG, "setSegId() [Code : " + "1" + "]");
                Toast.makeText(this, "segid 설정되었습니다.", Toast.LENGTH_SHORT).show();
                SLog.d(TESTTAG, "16. segid 설정 End");
                mDvbSIService.setSegId(-1);
                break;
        }
    }
}
