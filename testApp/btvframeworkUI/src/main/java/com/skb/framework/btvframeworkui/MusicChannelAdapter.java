package com.skb.framework.btvframeworkui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import com.skb.btv.framework.navigator.dvbservice.MusicDvbService;

import java.util.List;

public class MusicChannelAdapter extends BaseAdapter {

    private String mTAG = getClass().getSimpleName().trim();

    private Context mContext = null;
    private List<MusicDvbService> mListData = null;

    public MusicChannelAdapter(Context mContext) {
        super();
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mListData == null ? 0 : mListData.size();
    }

    @Override
    public Object getItem(int position) {
        return mListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MusicChannelAdapter.SubViewHolder holder;

        if (convertView == null) {
            holder = new MusicChannelAdapter.SubViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listitem_channel, null);

            holder.mText = (TextView) convertView.findViewById(R.id.tv_title);
            holder.mImage = (ImageView) convertView.findViewById(R.id.iv_folder);

            convertView.setLayoutParams(new ListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 75));
            convertView.setTag(holder);

        } else {
            holder = (MusicChannelAdapter.SubViewHolder) convertView.getTag();
        }

        //String chInfo = "ch : " + mListData.get(position).getCh() + "   , " + mListData.get(position).getTitle();
        String chInfo = mListData.get(position).getTitle();

        // specific process
        holder.mText.setText(chInfo);
        return convertView;
    }

    public void setList(List<MusicDvbService> list) {
        this.mListData = list;
    }

    public List<MusicDvbService> getList() {
        return mListData;
    }

    public class SubViewHolder
    {
        public TextView mText;
        public ImageView mImage;
    }
}
