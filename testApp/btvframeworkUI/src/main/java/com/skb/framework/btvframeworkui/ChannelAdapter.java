package com.skb.framework.btvframeworkui;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.skb.btv.framework.navigator.dvbservice.AVDvbService;
import com.skb.btv.framework.log.SLog;

import java.util.List;

//import android.sptek.stb.servicecore.si.channel.ChannelInfo;

public class ChannelAdapter extends BaseAdapter {

    private static final String TAG =  "ChannelAdapter";

    private Context mContext = null;
    private List<AVDvbService> mListData = null;


    public ChannelAdapter(Context mContext) {
        super();
        SLog.d(TAG, "ChannelAdapter");
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        SLog.d(TAG, "getCount");
        return mListData == null ? 0 : mListData.size();
    }

    @Override
    public Object getItem(int position) {
        SLog.d(TAG, "getItem");
        return mListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        SLog.d(TAG, "getItemId");
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SLog.d(TAG, "getView");

        SubViewHolder holder;

        if (convertView == null) {
            holder = new SubViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listitem_channel, null);

            holder.mText = (TextView) convertView.findViewById(R.id.tv_title);
            holder.mImage = (ImageView) convertView.findViewById(R.id.iv_folder);

            convertView.setLayoutParams(new ListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 75));
            convertView.setTag(holder);

        } else {
            holder = (SubViewHolder) convertView.getTag();
        }

        String chInfo = "ch : " + mListData.get(position).getCh() + "   " + mListData.get(position).getName();

        // specific process
        holder.mText.setText(chInfo);

        return convertView;
    }

    public void remove(int position) {
        SLog.d(TAG, "remove");
        //mListData.remove(position);
    }

    public void setList(List<AVDvbService> list) {
        SLog.d(TAG, "setList");
        this.mListData = list;
    }

    public List<AVDvbService> getList() {
        return mListData;
    }

    public class SubViewHolder
    {
        public TextView mText;
        public ImageView mImage;
    }

}
