package com.skb.framework.btvframeworkui;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.graphics.SurfaceTexture;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.skb.btv.framework.core.BtvFramework;
import com.skb.btv.framework.navigator.BtvNavigator;
import com.skb.btv.framework.navigator.IBtvNavigator;
import com.skb.btv.framework.navigator.OnDsmccListener;
import com.skb.btv.framework.navigator.OnDvbSIListener;
import com.skb.btv.framework.navigator.dvbservice.AVDvbService;
import com.skb.btv.framework.navigator.dvbservice.product;
import com.skb.btv.framework.navigator.dvbservice.AVDvbServiceList;
import com.skb.btv.framework.navigator.dvbservice.IAVDvbService;
import com.skb.btv.framework.navigator.dvbservice.MultiViewLogicalCell;
import com.skb.btv.framework.navigator.dvbservice.MultiViewDvbService;
import com.skb.btv.framework.navigator.dvbservice.MultiViewDvbServiceList;
import com.skb.btv.framework.navigator.dvbservice.MusicDvbService;
import com.skb.btv.framework.navigator.dvbservice.MusicDvbServiceList;
import com.skb.btv.framework.navigator.product.ProductList;
import com.skb.btv.framework.navigator.program.AVProgram;
import com.skb.btv.framework.navigator.program.AVProgramList;
import com.skb.btv.framework.navigator.program.AVPrograms;
import com.skb.btv.framework.navigator.program.MusicProgram;
import com.skb.btv.framework.navigator.DvbSIService;
import com.skb.btv.framework.log.SLog;
import com.skb.btv.framework.media.DvbServicePlayer;
import com.skb.btv.framework.media.SignalEvent;
import com.skb.btv.framework.media.VodPlayer;
import com.skb.btv.framework.media.WebVTT;
import com.skb.btv.framework.property.ChangePropertyListener;
import com.skb.btv.framework.property.PropertyManager;
import com.skb.btv.framework.media.core.MediaPlayer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends Activity implements View.OnClickListener {
    private static final String TAG =  "MainActivity";

    private Context mContext;

    private TextView mVersionInfo;
    private DvbServicePlayer mainTVMediaPlayer;
    private VodPlayer mainVodMediaPlayer;

    private LinearLayout mLinearMain;
    private SurfaceView mIPTVSurfaceView = null;
    private SurfaceHolder mIPTVSurfaceHolder = null;
    private List<String> mZappingTimeData;
    private List<AVDvbService> mChannelData;
    private Spinner mChannelSpinner;
    private int mCurrentPosition = 0;
    private TextView mChannelTitle;

    private Timer mTimer = null;
    private TimerTask mTask = null;
    private boolean isTimerAlive = false;
    private Spinner mZappingSpinner;
    private int mZappingTime = 10000;

    private ImageView mImagePlay;
    private ImageView mImagePause;

    private DvbServicePlayer pipMediaPlayer;

    private SurfaceView mIPTVSurfaceView_Pip = null;
    private SurfaceHolder mIPTVSurfaceHolder_Pip = null;
    private List<AVDvbService> mChannelData_Pip;
    private Spinner mChannelSpinner_Pip;

    private int mCurrentPosition_Pip = 0;
    private int mMultiViewPosition_Pip = 0;
    private TextView mChannelTitle_Pip;

    private Timer mTimer_Pip = null;
    private TimerTask mTask_Pip = null;
    private boolean isTimerAlive_Pip = false;

    private Spinner mZappingSpinner_Pip;
    private int mZappingTime_Pip = 10000;

    private Button mBtnPip;
    private ImageView mImagePlay_Pip;
    private ImageView mImagePause_Pip;
    private ImageView mImageWatermark;

    private boolean isFirstStart = true;
    private boolean isPIPShowing = false;
    private boolean isVODPlaying = false;
    private boolean isPaused = false;
    private boolean isMultiView = false;
    private boolean isWatermarkStarted = false;

    private int tempPosition = 0;
    private MediaPlayer.TRICK_SPEED trickSpeed = MediaPlayer.TRICK_SPEED.X010;

    private Button mBtnAudioChangeLanguage;
    private Button mBtnMultiview;

    private BroadcastReceiver siChangedReceiver;

    /**
     * Start Add dvbsi scenario - KGH
     */
    private ChannelAdapter channelAdapter;
    private List<MusicDvbService> mMusicChannelData;
    private Spinner mMusicChannelSpinner;
    private MusicChannelAdapter musicChannelAdapter;
    private int mMusicChannelCurrentPosition = 0;
    private int currentChannelNumber;
    private int currentChannelNumber_Pip;
    private List<AVProgram> mProgramList = new ArrayList<AVProgram>();
    private List<AVProgram> mProgramList_Pip = new ArrayList<AVProgram>();
    private Handler mHandler = new Handler();
    private int musichChannelAudioPid = 0;

    private List<MusicProgram> mMusicProgramList = new ArrayList<MusicProgram>();
    private Spinner mMusicChannelProgramInfo;
    private MusicProgramAdapter musicProgramAdapter;
    private List<MultiViewDvbService> mMultiViewChannelData = new ArrayList<MultiViewDvbService>();
    private  String mMultiViewURI;
    private int currentRunningProgramIndex = 0, currentMusicRunningProgramIndex = 0;
    private int currentRunningProgramIndex_Pip = 0;

    private PropertyManager mPropertyManager;
    private Button btnVOD, btnFile;
    private Button btnMultiviewProgram;
    private String multiViewChannel;
    /**
     * End - KGH
     */

    private Spinner mChannelProgramInfo;
    private Spinner mChannelProgramInfo_Pip;
    private ProgramAdapter programAdapter;
    private ProgramAdapter programAdapter_Pip;

    private TextureView mCCTextureView = null;
    private SurfaceHolder mCCSurfaceHolder = null;

    private int currentAudioFocus;

    //private DvbServiceManager siManager;
    private DvbSIService mDvbSIService;
    private boolean mIsUseLocalInstance = false;

    private int mChannel;
    private TextView mTvCh;
    private Runnable mRunSetCh = new Runnable() {
        @Override
        public void run() {
            SLog.d("channel", "set channel : " + mChannel);
            if (mChannel <= 0) {
                mChannel = 0;
                if (mTvCh != null) {
                    mTvCh.setText("");
                }
                return;
            }
            int idx = -1;
            for (int i=0; i<mChannelData.size(); i++) {
                AVDvbService channel = mChannelData.get(i);
                if (channel.getCh() == mChannel) {
                    idx = i;
                    break;
                }
            }
            mChannel = 0;
            if (idx != -1) {
                if (isPIPShowing) {
                    if (idx != mChannelSpinner_Pip.getSelectedItemPosition()) {
                        SLog.d(TAG, "============== RunSetCh - call setPIP");
                        setPIP(idx);
                    }
                } else {
                    if (idx != mChannelSpinner.getSelectedItemPosition()) {
                        setTV(idx);
                    }
                }
            }
            if (mTvCh != null) {
                mTvCh.setText("");
            }
        }
    };

    /**
     * Start Add dvbsi scenario - KGH
     */

    public OnDsmccListener onDsmccListener = new OnDsmccListener(){
        @Override
        public void onDsmccEvent(String path, com.skb.btv.framework.navigator.SignalEvent sigevent){
            SLog.d(TAG, "Dvbsi - onDsmccEvent, path:" + path + ", event : " + sigevent);
        }
    };

    public OnDvbSIListener onDvbSIListener = new OnDvbSIListener(){
        @Override
        public void onDvbSIUpdated(int dvbsiType, final String extraData) {
            SLog.d(TAG, "onDvbSIUpdated() dvbsiType : " + dvbsiType + ", extra : "+extraData);
            if(dvbsiType == IBtvNavigator.DVBSI_TYPE_UPDATE_CHANNEL){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String channelListVersion ="";
                        String extraString = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            extraString = json.getString(IBtvNavigator.DVBSI_JSONOBJECT_INFO);
                            channelListVersion = json.getString(IBtvNavigator.DVBSI_JSONOBJECT_DVBSERVICEVERSION);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        SLog.d("KGH", "version : "+channelListVersion);
                        AVDvbServiceList channelList = null;
                        if (mIsUseLocalInstance) {
                            if (mDvbSIService != null) {
                                channelList = mDvbSIService.getAVDvbServiceList();
                            }
                        } else {
                            channelList = BtvNavigator.getDvbSIService(mContext).getAVDvbServiceList();
                        }
                        if (channelList != null && channelList.isReady()) {
                            mChannelData = Utils.channelParser(MainActivity.this, channelList);
                            for (int i = 0; i < mChannelData.size(); i++) {
                                SLog.e("channel list", "onDvbSIUpdated - " + mChannelData.get(i));
                            }
                            mChannelData_Pip = mChannelData;
                            channelAdapter.setList(mChannelData);
                            channelAdapter.notifyDataSetChanged();
                            mChannelSpinner.setSelection(mCurrentPosition);
                        }
                    }
                });
            }else if(dvbsiType == IBtvNavigator.DVBSI_TYPE_UPDATE_PROGRAMS){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String extraString = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            extraString = json.getString(IBtvNavigator.DVBSI_JSONOBJECT_INFO);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        AVProgramList program_result = null;
                        if (mIsUseLocalInstance) {
                            if (mDvbSIService != null) {
                                program_result = mDvbSIService.getAVProgramList(currentChannelNumber);
                            }
                        } else {
                            program_result = BtvNavigator.getDvbSIService(mContext).getAVProgramList(currentChannelNumber);
                        }
                        if (program_result != null && program_result.getAVProgramsList() != null && program_result.getAVProgramsList().size() > 0) {
                            if (mProgramList != null) mProgramList.clear();
                            if (program_result.getAVProgramsList().get(0).getAVProgramList() != null) {
                                for (AVProgram tempValue : program_result.getAVProgramsList().get(0).getAVProgramList()) {
                                    mProgramList.add(tempValue);
                                }
                            }
                            programAdapter.setList(mProgramList);
                            programAdapter.notifyDataSetChanged();
                            try {
                                currentRunningProgramIndex = getCurrentRunningProgram();
                                mChannelProgramInfo.setSelection(currentRunningProgramIndex);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (isPIPShowing) {
                            AVProgramList programPip_result = null;
                            if (mIsUseLocalInstance) {
                                if (mDvbSIService != null) {
                                    programPip_result = mDvbSIService.getAVProgramList(currentChannelNumber_Pip);
                                }
                            } else {
                                programPip_result = BtvNavigator.getDvbSIService(mContext).getAVProgramList(currentChannelNumber_Pip);
                            }
                            if (programPip_result != null && programPip_result.getAVProgramsList() != null && programPip_result.getAVProgramsList().size() > 0) {
                                if (mProgramList_Pip != null) mProgramList_Pip.clear();
                                for (AVProgram tempValue : programPip_result.getAVProgramsList().get(0).getAVProgramList()) {
                                    mProgramList_Pip.add(tempValue);
                                }
                                programAdapter_Pip.setList(mProgramList_Pip);
                                programAdapter_Pip.notifyDataSetChanged();
                                currentRunningProgramIndex_Pip = getCurrentRunningProgram_Pip();
                                mChannelProgramInfo_Pip.setSelection(currentRunningProgramIndex_Pip);
                            }
                        }
                    }
                });
            }else if(dvbsiType == IBtvNavigator.DVBSI_TYPE_UPDATE_MUSIC_CHANNEL){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String extraString = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            extraString = json.getString(IBtvNavigator.DVBSI_JSONOBJECT_INFO);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        MusicDvbServiceList channelList = null;
                        if (mIsUseLocalInstance) {
                            if (mDvbSIService != null) {
                                channelList = mDvbSIService.getMusicDvbServiceList();
                            }
                        } else {
                            channelList = BtvNavigator.getDvbSIService(mContext).getMusicDvbServiceList();
                        }
                        if(channelList != null && channelList.isReady()){
                            mMusicChannelData = Utils.musicChannelParser(MainActivity.this, channelList);
                            musicChannelAdapter.setList(mMusicChannelData);
                            musicChannelAdapter.notifyDataSetChanged();
                            mMusicChannelSpinner.setSelection(mMusicChannelCurrentPosition);
                        }
                    }
                });
            } else if(dvbsiType == IBtvNavigator.DVBSI_TYPE_UPDATE_MUSIC_PROGRAMS) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int audioid = -1;
                        try {
                            JSONObject json = new JSONObject(extraData);
                            audioid = json.getInt(IBtvNavigator.DVBSI_JSONOBJECT_AUDIOPID);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        List<MusicProgram> musicprogram_result = null;
                        if (mIsUseLocalInstance) {
                            if (mDvbSIService != null) {
                                musicprogram_result = mDvbSIService.getMusicProgramList(audioid).getMusicProgramList();
                            }
                        } else {
                            musicprogram_result = BtvNavigator.getDvbSIService(mContext).getMusicProgramList(audioid).getMusicProgramList();
                        }
                        if (musicprogram_result != null && musicprogram_result.size() > 0) {
                            if (mMusicProgramList != null) mMusicProgramList.clear();
                            for (MusicProgram entity : musicprogram_result) {
                                mMusicProgramList.add(entity);
                            }
                            musicProgramAdapter.setList(mMusicProgramList);
                            musicProgramAdapter.notifyDataSetChanged();
                            currentMusicRunningProgramIndex = getCurrentRunningMusicProgram();
                            mMusicChannelProgramInfo.setSelection(currentMusicRunningProgramIndex);
                            long gap = mMusicProgramList.get(currentMusicRunningProgramIndex).getEndDate().getTime() - System.currentTimeMillis();
                            mHandler.removeCallbacks(updateMusicProgramRunnable);
                            mHandler.postDelayed(updateMusicProgramRunnable, gap + 1000);
                        }
                    }
                });

            }else if(dvbsiType == IBtvNavigator.DVBSI_TYPE_UPDATE_MULTI_CHANNELS){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String extraString = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            extraString = json.getString(IBtvNavigator.DVBSI_JSONOBJECT_INFO);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        MultiViewDvbServiceList multiViewChannelList = null;
                        if (mIsUseLocalInstance) {
                            if (mDvbSIService != null) {
                                multiViewChannelList = mDvbSIService.getMultiViewDvbServiceList();
                            }
                        } else {
                            multiViewChannelList = BtvNavigator.getDvbSIService(mContext).getMultiViewDvbServiceList();
                        }
                        if(multiViewChannelList != null && multiViewChannelList.isReady()){
                            mMultiViewChannelData = Utils.multiViewChannelParser(MainActivity.this, multiViewChannelList);
                        }
                    }
                });
            }else if(dvbsiType == IBtvNavigator.DVBSI_TYPE_CHANGE_PROGRAM){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int sid = -1;
                        try {
                            JSONObject json = new JSONObject(extraData);
                            sid = json.getInt(IBtvNavigator.DVBSI_JSONOBJECT_SID_NUM);//IBtvFramework.DVBSI_JSONOBJECT_CHANNEL_NUM);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        AVProgramList program_result = null;
                        if (mIsUseLocalInstance) {
                            if (mDvbSIService != null) {
                                program_result = mDvbSIService.getAVProgramList(sid);
                            }
                        } else {
                            program_result = BtvNavigator.getDvbSIService(mContext).getAVProgramList(sid);
                        }
                        if (mProgramList != null) {
                            mProgramList.clear();
                        }

                        if (program_result != null && program_result.getAVProgramsList() != null && program_result.getAVProgramsList().size() > 0) {
                            for (AVProgram tempValue : program_result.getAVProgramsList().get(0).getAVProgramList()) {
                                mProgramList.add(tempValue);
                            }
                            programAdapter.setList(mProgramList);
                            programAdapter.notifyDataSetChanged();
                            currentRunningProgramIndex = getCurrentRunningProgram();
                            mChannelProgramInfo.setSelection(currentRunningProgramIndex);
                        }
                    }
                });
            } else if(dvbsiType == IBtvNavigator.DVBSI_TYPE_CHANGE_PROGRAMS){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String sidNumbers = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            sidNumbers = json.getString(IBtvNavigator.DVBSI_JSONOBJECT_SID_NUM);//DVBSI_JSONOBJECT_CHANNEL_NUM);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(sidNumbers.isEmpty()) {
                            ProductList product_result = null;
                            if (mIsUseLocalInstance) {
                                if (mDvbSIService != null) {
                                    product_result = mDvbSIService.getProductList();
                                }
                            } else {
                                product_result = BtvNavigator.getDvbSIService(mContext).getProductList();
                            }
                            if (product_result != null && product_result.getProductList() != null) {
                                if (product_result.getProductList().size() > 0) {
                                    SLog.d(TAG, "Product Count : " + product_result.getProductList().size());
                                }
                            }
                        } else {
                            AVProgramList program_result = null;
                            if (mIsUseLocalInstance) {
                                if (mDvbSIService != null) {
                                    program_result = mDvbSIService.getMultiAVProgramList(sidNumbers);
                                }
                            } else {
                                program_result = BtvNavigator.getDvbSIService(mContext).getMultiAVProgramList(sidNumbers);
                            }
                            if (program_result != null && program_result.getAVProgramsList() != null && program_result.getAVProgramsList().size() > 0) {
                                SLog.d(TAG, "Program change channel Count : " + program_result.getAVProgramsList().size());
                            }
                        }
                    }
                });
            }
        }
    };

    private Runnable updateMusicProgramRunnable = new Runnable() {
        @Override
        public void run() {
            if (musicProgramAdapter != null) {
                List<MusicProgram> musicprogram_result = null;
                if (mIsUseLocalInstance) {
                    if (mDvbSIService != null) {
                        musicprogram_result = mDvbSIService.getMusicProgramList(musichChannelAudioPid).getMusicProgramList();
                    }
                } else {
                    musicprogram_result = BtvNavigator.getDvbSIService(mContext).getMusicProgramList(musichChannelAudioPid).getMusicProgramList();
                }
                if (musicprogram_result != null && musicprogram_result.size() > 0) {
                    for(MusicProgram entity : musicprogram_result) {
                        mMusicProgramList.add(entity);
                    }
                    musicProgramAdapter.setList(mMusicProgramList);
                    musicProgramAdapter.notifyDataSetChanged();
                    currentMusicRunningProgramIndex = getCurrentRunningMusicProgram();
                    mMusicChannelProgramInfo.setSelection(currentMusicRunningProgramIndex);
                    long gap = mMusicProgramList.get(currentMusicRunningProgramIndex).getEndDate().getTime()- System.currentTimeMillis();
                    mHandler.removeCallbacks(updateMusicProgramRunnable);
                    mHandler.postDelayed(updateMusicProgramRunnable, gap+1000);
                }
            }
        }
    };
	
    AdapterView.OnItemSelectedListener onMusicChannelItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
            if(isFirstStart == true){
                isFirstStart = false;
                return;
            }

            int vId = parent.getId();
            if(vId == R.id.m_channel){
                mMusicChannelCurrentPosition = i;

                if(mainTVMediaPlayer != null){
                    setMusicTV(mMusicChannelCurrentPosition);
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {}
    };
    /**
     * End - KGH
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SLog.d(TAG, "onCreate");
        setContentView(R.layout.activity_player_iptv);

        mContext = this;

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        BtvFramework btvFramework = BtvFramework.getInstance(this);

        // Start Add dvbsi scenario - KGH
        mPropertyManager = (PropertyManager)btvFramework.getBtvService(BtvFramework.PROPERTY_SERVICE);
        mPropertyManager.setManagerReadyListener(new BtvFramework.ManagerReadyStatusListener() {
            @Override
            public void onManagerReady(Object object, int status) {

                if(status == BtvFramework.MANAGER_READY_STATUS)
                {
                    SLog.d(TAG, "mPropertyManager is ready");
                    PropertyManager propertymgr = (PropertyManager)object;
                    propertymgr.setChangePropertyListener(new ChangePropertyListener() {
                        @Override
                        public void ChangePropertyData(int type, String key) {
                            String stringType;
                            if(type==0){
                                stringType = "SYSTEM_PROPERTY";
                            } else if(type==1) {
                                stringType = "BTV_PROPERTY";
                            } else {
                                stringType = String.valueOf(type);
                            }
                            SLog.d(TAG, "ChangePropertyData type=" + stringType + ", key=" + key);
                        }
                    });
                }
            }
        });
        // End KGH

        mDvbSIService = BtvNavigator.getDvbSIService(mContext);//DvbSIService.getInstance(mContext);
        SLog.d(TAG, "onCreate - dvbSIService : " + mDvbSIService);
        mDvbSIService.setOnDsmccListener(onDsmccListener);

        AVDvbServiceList channelList = null;
        MusicDvbServiceList musicChannelList = null;
        MultiViewDvbServiceList multiViewChannelList = null;
        if (mIsUseLocalInstance) {
            if (mDvbSIService != null) {
                channelList = mDvbSIService.getAVDvbServiceList();
                musicChannelList = mDvbSIService.getMusicDvbServiceList();
                multiViewChannelList = mDvbSIService.getMultiViewDvbServiceList();
            }
        } else {
            channelList = BtvNavigator.getDvbSIService(mContext).getAVDvbServiceList();
            musicChannelList = BtvNavigator.getDvbSIService(mContext).getMusicDvbServiceList();
            multiViewChannelList = BtvNavigator.getDvbSIService(mContext).getMultiViewDvbServiceList();
        }
        if(channelList != null && channelList.isReady()){
            mChannelData = Utils.channelParser(MainActivity.this, channelList);
            mChannelData_Pip = mChannelData;
            for (int i=0; i<mChannelData.size(); i++) {
                SLog.i(TAG, "onCreate - mChannelData - av [" + i + "] : "+ mChannelData.get(i).getCh() + ", " + mChannelData.get(i).getName());
            }
        }
        SLog.i("channel list", "onCreate - mChannelData - music channelList ready : " + ((musicChannelList != null)? musicChannelList.isReady() : "null"));
        if(musicChannelList != null && musicChannelList.isReady()){
            mMusicChannelData = Utils.musicChannelParser(MainActivity.this, musicChannelList);
            for (int i=0; i<mMusicChannelData.size(); i++) {
                SLog.i(TAG, "onCreate - music [" + i + "] : "+ mMusicChannelData.get(i).getCh_no() + ", " + mMusicChannelData.get(i).getTitle());
            }
        }
        SLog.i("channel list", "onCreate - mChannelData - multi channelList ready : " + ((multiViewChannelList != null)? multiViewChannelList.isReady() : "null"));
        if(multiViewChannelList != null && multiViewChannelList.isReady()){
            mMultiViewChannelData = Utils.multiViewChannelParser(MainActivity.this, multiViewChannelList);
            for (int i=0; i<mMultiViewChannelData.size(); i++) {
                SLog.i(TAG, "onCreate - multiview [" + i + "] : "+ mMultiViewChannelData.get(i).getGroupID() + ", " + mMultiViewChannelData.get(i).getGroupName());
            }
        }

        mainUi();
    }

    public void mainUi(){
        SLog.d(TAG, "mainUi");
        mTvCh = findViewById(R.id.tv_channel);
        mVersionInfo = (TextView) findViewById(R.id.tv_version);

//        SIService si = ServiceCoreAPI.getService(ServiceCoreAPI.SI_SERVICE);
//        si.connect(MainActivity.this);
//        RetMultiviewChannelList list = si.getMultiviewChannelList();

//        mDvbSIManager.setManagerReadyListener(new ManagerReadyStatusListener() {
//            @Override
//            public void onManagerReady(Object object, int status) {
//                if(status == BtvFramework.MANAGER_READY_STATUS){
//                    loadChannels();
//                }
//            }
//        });
        setZappingTime();

        // Main Channel
        channelAdapter = new ChannelAdapter(this);
        SLog.i("channel list", "mainUi - channel count : " + ((mChannelData != null) ? mChannelData.size() : "null"));
        /*for (int i=0; i<mChannelData.size(); i++) {
            SLog.e("channel list", "mainUi - channelList  [" + i + "] : "+ mChannelData.get(i).getCh() + ", " + mChannelData.get(i).getName());
        }*/
        channelAdapter.setList(mChannelData);

        mLinearMain = (LinearLayout)findViewById(R.id.linear_main);

        mChannelSpinner = (Spinner) findViewById(R.id.s_channel);
        mChannelSpinner.setAdapter(channelAdapter);
        mChannelSpinner.setOnItemSelectedListener(onChannelItemSelectedListener);

        mZappingSpinner = (Spinner) findViewById(R.id.s_zapping);
        mZappingSpinner.setAdapter(new ArrayAdapter<String>(this, R.layout.listitem_zapping, mZappingTimeData));
        mZappingSpinner.setOnItemSelectedListener(onZappingItemSelectedListener);

        mChannelTitle = (TextView) findViewById(R.id.tv_title_bottom);

        mIPTVSurfaceView = (SurfaceView) findViewById(R.id.sv_video_view);
        mIPTVSurfaceHolder = mIPTVSurfaceView.getHolder();
        mIPTVSurfaceView.setKeepScreenOn(true);
        //obd 보드가 달라서 임시로 막음
        mIPTVSurfaceHolder.addCallback(onMainSurfaceCallback);
        mIPTVSurfaceHolder.setFormat(PixelFormat.TRANSPARENT);

        // PIP Channel
        ChannelAdapter channelAdapter_Pip = new ChannelAdapter(this);
        channelAdapter_Pip.setList(mChannelData_Pip);

        mChannelSpinner_Pip = (Spinner) findViewById(R.id.s_channel_pip);
        mChannelSpinner_Pip.setAdapter(channelAdapter_Pip);
        mChannelSpinner_Pip.setOnItemSelectedListener(onChannelItemSelectedListener);

        mZappingSpinner_Pip = (Spinner) findViewById(R.id.s_zapping_pip);
        mZappingSpinner_Pip.setAdapter(new ArrayAdapter<String>(this, R.layout.listitem_zapping, mZappingTimeData));
        mZappingSpinner_Pip.setOnItemSelectedListener(onZappingItemSelectedListener);

        mChannelTitle_Pip = (TextView) findViewById(R.id.tv_title_bottom_pip);

        mIPTVSurfaceView_Pip = (SurfaceView) findViewById(R.id.sv_pip_view);
        mIPTVSurfaceHolder_Pip = mIPTVSurfaceView_Pip.getHolder();
        mIPTVSurfaceView_Pip.setKeepScreenOn(true);
        mIPTVSurfaceHolder_Pip.addCallback(onPipSurfaceCallback);
        mIPTVSurfaceHolder_Pip.setFormat(PixelFormat.TRANSPARENT);

        mImagePlay = (ImageView)findViewById(R.id.iv_play);
        mImagePause = (ImageView)findViewById(R.id.iv_pause);
        mImagePlay.setOnClickListener(this);
        mImagePause.setOnClickListener(this);

        mImagePlay_Pip = (ImageView)findViewById(R.id.iv_play_pip);
        mImagePause_Pip = (ImageView)findViewById(R.id.iv_pause_pip);
        mImagePlay_Pip.setOnClickListener(this);
        mImagePause_Pip.setOnClickListener(this);
        mBtnPip = (Button)findViewById(R.id.btn_pip);
        mBtnPip.setOnClickListener(this);

        findViewById(R.id.btn_watermark).setOnClickListener(this);
        mImageWatermark = findViewById(R.id.img_watermark);

        mBtnAudioChangeLanguage = (Button)findViewById(R.id.btn_audio_language);
        mBtnAudioChangeLanguage.setOnClickListener(this);

        mBtnMultiview = (Button)findViewById(R.id.btn_multiview);
        mBtnMultiview.setOnClickListener(this);

        findViewById(R.id.iv_up).setOnClickListener(this);
        findViewById(R.id.iv_down).setOnClickListener(this);
        findViewById(R.id.iv_up_pip).setOnClickListener(this);
        findViewById(R.id.iv_down_pip).setOnClickListener(this);

        findViewById(R.id.btn_vod).setOnClickListener(this);
        findViewById(R.id.btn_file).setOnClickListener(this);

//        TVSystem tvSystem = ServiceCoreAPI.getService(ServiceCoreAPI.TV_SYSTEM);
//        tvSystem.connect(MainActivity.this);
//        tvSystem.getEthernet().setCheckNetworkListener(new CheckNetworkListener() {
//            @Override
//            public void onCheckNetwork(int checkNetworkData) {
//                //updateEthernetStatus((Button)findViewById(R.id.btn_ip));
//
//            }
//        });
//        tvSystem.getEthernet().checkNetwork(0, null, 0);
//        tvSystem.setProperty(TVSystem.PROPERTY_STB_ID, "{9E78FA79-99DD-11E7-804E-5D8B5259997F}");
//        tvSystem.setProperty(TVSystem.PROPERTY_ETHERNET_MAC, "00:10:43:56:22:00");
        //String prp = tvSystem.getProperty(TVSystem.PROPERTY_STB_ID);

//        NetworkMap networkMap =  tvSystem.getEthernet().getEthernetStatus();

        TextView tvIp = findViewById(R.id.text_ip);
//        tvIp.setText(networkMap.getIp());

        updateEthernetStatus((Button)findViewById(R.id.btn_ip));

        setVersion();

        mChannelProgramInfo = (Spinner) findViewById(R.id.s_programInfo);
        mChannelProgramInfo_Pip = (Spinner) findViewById(R.id.s_programInfo_pip);
        programAdapter = new ProgramAdapter(MainActivity.this);
        programAdapter_Pip = new ProgramAdapter(MainActivity.this);
        mChannelProgramInfo.setAdapter(programAdapter);
        mChannelProgramInfo_Pip.setAdapter(programAdapter_Pip);

        /**
         * Start Add dvbsi scenario - KGH
         */
        mChannelProgramInfo_Pip.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                parent.setSelection(currentRunningProgramIndex_Pip);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        musicChannelAdapter = new MusicChannelAdapter(this);
        mMusicChannelSpinner = (Spinner)findViewById(R.id.m_channel);
        mMusicChannelSpinner.setOnItemSelectedListener(onMusicChannelItemSelectedListener);
        musicChannelAdapter.setList(mMusicChannelData);
        mMusicChannelSpinner.setAdapter(musicChannelAdapter);
        mChannelProgramInfo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                parent.setSelection(currentRunningProgramIndex);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mMusicChannelProgramInfo = (Spinner) findViewById(R.id.s_musicprogramInfo);
        musicProgramAdapter = new MusicProgramAdapter(MainActivity.this);
        mMusicChannelProgramInfo.setAdapter(musicProgramAdapter);
        mMusicChannelProgramInfo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                parent.setSelection(currentMusicRunningProgramIndex);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnMultiviewProgram = (Button)findViewById(R.id.btn_multiviewprogram);
        btnMultiviewProgram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<AVPrograms> items = new ArrayList<AVPrograms>();
                AVProgramList program_result = null;
                if (mIsUseLocalInstance) {
                    if (mDvbSIService != null) {
                        program_result = mDvbSIService.getMultiAVProgramList(multiViewChannel);
                    }
                } else {
                    program_result = BtvNavigator.getDvbSIService(mContext).getMultiAVProgramList(multiViewChannel);
                }
                if (program_result != null && program_result.getAVProgramsList() != null && program_result.getAVProgramsList().size() > 0) {
                    for (AVPrograms programs : program_result.getAVProgramsList()) {
                        items.add(programs);
                    }
                }
                Intent intent_program = new Intent(mContext, TestProgram.class);
                intent_program.putParcelableArrayListExtra("items", items);
                startActivity(intent_program);
            }
        });
        /**
         * End - KGH
         */
//        updateProgramInfo();
/*
        siChangedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent != null && intent.getAction().equals(ISIServiceMsg.ACTION_SIService)) {
                    String[] obj = intent.getStringArrayExtra(ISIServiceMsg.I_MAINMSG);
                    if(obj != null){
                        String msg = "";
                        String value = "";

                        for(int i=0; i<obj.length; i++){
                            if(i == 0){
                                msg = obj[i];
                            }else if(i == 1){
                                value = obj[i];
                                break;
                            }
                        }

                        switch (msg) {
                            case ISIServiceMsg.I_SUBMSG_CHANGE_PROGRAM:
                                loadChannels();
                            case ISIServiceMsg.I_SUBMSG_CHANGE_PROGRAMS:
                            case ISIServiceMsg.I_SUBMSG_UPDATE_CHANNEL_INFO:
                            case ISIServiceMsg.I_SUBMSG_UPDATE_PROGRAM_INFO:
                                updateProgramInfo();
                                break;
                        }
                    }
                }
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ISIServiceMsg.ACTION_SIService);
        registerReceiver(siChangedReceiver, intentFilter);
*/
        Button btnCC = findViewById(R.id.btn_cc);
/*
        String bCC = tvSystem.getSystem().getProperty(ITVSystem.PROPERTY_HEARING_IMPAIRED);
        if(bCC.equals("true")) {
            btnCC.setText("CC On");
        } else {
            btnCC.setText("CC Off");
        }

        Button btnVisionCC = findViewById(R.id.btn_vision_cc);
        String bVisionCC = tvSystem.getSystem().getProperty(ITVSystem.PROPERTY_VISION_IMPAIRED);
        if(bVisionCC.equals("true")) {
            btnVisionCC.setText("Vision On");
        } else {
            btnVisionCC.setText("Vision Off");
        }
*/

        btnVOD = (Button)findViewById(R.id.btn_vod);
        btnFile = (Button)findViewById(R.id.btn_file);
        btnMultiviewProgram = (Button)findViewById(R.id.btn_multiviewprogram);
    }
/*
    private void updateProgramInfo() {
        SIService si = ServiceCoreAPI.getService(ServiceCoreAPI.SI_SERVICE);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date today = calendar.getTime();

        calendar.add( Calendar.DATE, 1 );
        Date tomorrow = calendar.getTime();

        Calendar calendar2 = Calendar.getInstance();
        calendar2.add(Calendar.DAY_OF_YEAR, 1);
        if(mChannelData != null) {
            RetProgramList pl = si.getMultiChannelProgramListByTime("" + mChannelData.get(mCurrentPosition).getChannelNum(), today, tomorrow);
            if(pl != null && pl.getList() != null && pl.getList().size() > 0) {
                programAdapter.setList(pl.getList().get(0).getProgramInfoList());
                programAdapter.notifyDataSetChanged();
            }
        }

        if(mChannelData_Pip != null) {
            if(mChannelData_Pip != null) {
                RetProgramList pl = si.getMultiChannelProgramListByTime("" + mChannelData_Pip.get(mCurrentPosition_Pip).getChannelNum(), today, tomorrow);
                if(pl != null && pl.getList() != null && pl.getList().size() > 0) {
                    programAdapter_Pip.setList(pl.getList().get(0).getProgramInfoList());
                    programAdapter_Pip.notifyDataSetChanged();
                }
            }
        }
    }
*/
    @Override
    protected void onResume() {
        super.onResume();
        SLog.d(TAG, "onResume");
    }

    private void loadChannels() {  
        SLog.d(TAG, "loadChannels");
		//Add dvbsi scenario - KGH
        //mChannelData = Utils.channelParser(this,result);
        //mChannelData_Pip = mChannelData;
        //mMusicChannelData = musicResult.getChannelList();  
		// End KGH

        mainUi();

        /*
        SIService si = ServiceCoreAPI.getService(ServiceCoreAPI.SI_SERVICE);
        if(si.getChannelList() != null) {
            mChannelData = si.getChannelList().getList();

            int previousCh = 0;

            for (Iterator<ChannelInfo> iterator = mChannelData.iterator(); iterator.hasNext(); ) {
                ChannelInfo item = iterator.next();
                if (item.getChannelNum() == previousCh) {
                    iterator.remove();
                    continue;
                }
                previousCh = item.getChannelNum();
            }
        }

        mChannelData_Pip = mChannelData;
        */
    }

    private void setZappingTime(){
        SLog.d(TAG, "setZappingTime");
        mZappingTimeData = new ArrayList<String>();
        mZappingTimeData.add("10 sec");
        mZappingTimeData.add("5 sec");
        mZappingTimeData.add("3 sec");
        mZappingTimeData.add("1 sec");
    }

    SurfaceHolder.Callback onMainSurfaceCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            SLog.d(TAG, "onMainSurfaceCallback - surfaceCreated");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mainTVMediaPlayer = new DvbServicePlayer(MainActivity.this);
                    mainTVMediaPlayer.setDisplay(mIPTVSurfaceHolder);
//                    mainMediaPlayer.setSurfaceViewLayout(mIPTVSurfaceView, mIPTVSurfaceView);
                    mainTVMediaPlayer.setOnPreparedListener(new DvbServicePlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(DvbServicePlayer mediaPlayer) {
                            String chInfo = "CH No. " + mChannelData.get(mCurrentPosition).getCh() + "   -  " + mChannelData.get(mCurrentPosition).getName();
                            mChannelTitle.setText(chInfo);
                            SLog.d(TAG, "playerCallback - onPrepared, chInfo:" + chInfo + ", mp : " + mediaPlayer);

                            mainTVMediaPlayer.start();
                        }
                    });

                    mainTVMediaPlayer.setOnInfoListener(new DvbServicePlayer.OnInfoListener() {
                        @Override
                        public boolean onInfo(DvbServicePlayer mp, int what, int extra) {
                            SLog.d(TAG, "playerCallback - onInfo, what:" + what + ", mp : " + mp);
                            if(what == 7001){
                                // Video Pid changed
                                // extra => 변경된 Pid 값
                            }else if(what == 7002){
                                // audio Pid changed
                                // extra => 변경된 Pid 값
                            }
                            return false;
                        }
                    });
                    mainTVMediaPlayer.setOnCompletionListener(new DvbServicePlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(DvbServicePlayer mp) {
                            SLog.d(TAG, "playerCallback - onCompletion, mp:" + mp);
                        }
                    });
                    mainTVMediaPlayer.setOnErrorListener(new DvbServicePlayer.OnErrorListener() {
                        @Override
                        public boolean onError(DvbServicePlayer mp, int what, int extra) {
                            SLog.e(TAG, "playerCallback - onError, what:" + what + ", mp : " + mp);
                            return false;
                        }
                    });
                    mainTVMediaPlayer.setOnDsmccListener(new DvbServicePlayer.OnDsmccListener() {
                        @Override
                        public void onDsmccEvent(DvbServicePlayer mp, String path, SignalEvent sigevent) {
                            SLog.d(TAG, "playerCallback - onDsmccEvent, path:" + path + ", event : " + sigevent);
                        }
                    });
                    mCCTextureView = (TextureView)findViewById(R.id.sv_cc_view);
//                    mCCSurfaceHolder = mCCSurfaceView.getHolder();
                    mCCTextureView.setKeepScreenOn(true);
                    mCCTextureView.setSurfaceTextureListener(new CanvasListener());
//                    mCCTextureView.setZOrderMediaOverlay(true);
//                    mCCSurfaceHolder.addCallback(onCCSurfaceCallback);

                    mChannelSpinner.setSelection(mCurrentPosition);
                    //setTV(mCurrentPosition);

                    mainVodMediaPlayer = new VodPlayer(MainActivity.this);
                    mainVodMediaPlayer.setDisplay(mIPTVSurfaceHolder);
                    mainVodMediaPlayer.setOnPreparedListener(new VodPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(VodPlayer mediaPlayer) {
                            mainVodMediaPlayer.start();
                        }
                    });

                    mainVodMediaPlayer.setOnInfoListener(new VodPlayer.OnInfoListener() {
                        @Override
                        public boolean onInfo(VodPlayer mp, int what, int extra) {
                            SLog.i("IPTV_CALLBACK", "onInfo  what:" + what + ", extra:" + extra);
                            if(extra == 20001){
                                ArrayList<String> list = mainVodMediaPlayer.getLanguageList(2);
                                SLog.i("IPTV_CALLBACK", "list.size():" + list.size());
                                if(list.size() > 0) {
                                    for (String info : list) {
                                        SLog.i("IPTV_CALLBACK", "language:" + info);
                                    }
                                    mainVodMediaPlayer.setTrackLanguage(2, list.get(0));
                                }
                            }
                            return false;
                        }
                    });
                    mainVodMediaPlayer.setOnCompletionListener(new VodPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(VodPlayer mp) {
                        }
                    });
                    mainVodMediaPlayer.setOnErrorListener(new VodPlayer.OnErrorListener() {
                        @Override
                        public boolean onError(VodPlayer mp, int what, int extra) {
                            return false;
                        }
                    });
                }
            });
        }
        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            SLog.d(TAG, "onMainSurfaceCallback - surfaceChanged, channel count : "
                    + ((mChannelData!=null)?mChannelData.size():-1) + ", mainTVMediaPlayer : " + mainTVMediaPlayer);
            for (int i=0; i<mChannelData.size(); i++) {
                AVDvbService channel = mChannelData.get(i);
                if (channel.getName().equals("JTBC GOLF")) {
                    mCurrentPosition = i;
                    break;
                }
            }
            if(mainTVMediaPlayer != null) {
                // Start Add dvbsi scenario - KGH
                // setTV(mCurrentPosition);
                if (mChannelData != null && mChannelData.get(mCurrentPosition).getChannelType() != IAVDvbService.AVDVBSERVICE_CHANNEL_TYPE_MUSIC_CHANNEL) {
                    setTV(mCurrentPosition);
                }
            }
        }
        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            SLog.d(TAG, "onMainSurfaceCallback - surfaceDestroyed");
        }
    };

    private class CanvasListener implements TextureView.SurfaceTextureListener {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface,
                                              int width, int height) {
            SLog.d(TAG, "CanvasListener");
            mainTVMediaPlayer.setCCDisplay(mCCTextureView);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return true;
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface,
                                                int width, int height) {
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        }
    }

    SurfaceHolder.Callback onCCSurfaceCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            SLog.d(TAG, "onCCSurfaceCallback");
            //mainMediaPlayer.setCCDisplay(holder);
        }
        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}
        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {}
    };

    SurfaceHolder.Callback onPipSurfaceCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            SLog.d(TAG, "onPipSurfaceCallback - surfaceCreated, old pipMediaPlayer : " + pipMediaPlayer);
            pipMediaPlayer = new DvbServicePlayer(MainActivity.this, 1);
            pipMediaPlayer.setDisplay(mIPTVSurfaceHolder_Pip);
//            pipMediaPlayer.setSurfaceViewLayout(mIPTVSurfaceView_Pip, mIPTVSurfaceView_Pip);
            pipMediaPlayer.setOnInfoListener(new DvbServicePlayer.OnInfoListener() {
                @Override
                public boolean onInfo(DvbServicePlayer mp, int what, int extra) {
                    SLog.d(TAG, "playerCallback - onInfo, what:" + what + ", mp : " + mp);
                    if (what == 10000 && extra == 20008) {
                        pipMediaPlayer.setVolume(1.0f);
                        mainTVMediaPlayer.setVolume(0);
                    }
                    return false;
                }
            });
            pipMediaPlayer.setOnCompletionListener(new DvbServicePlayer.OnCompletionListener() {
                @Override
                public void onCompletion(DvbServicePlayer mp) {
                    SLog.d(TAG, "playerCallback - onCompletion, mp : " + mp);
                }
            });
            pipMediaPlayer.setOnErrorListener(new DvbServicePlayer.OnErrorListener() {
                @Override
                public boolean onError(DvbServicePlayer mp, int what, int extra) {
                    SLog.e(TAG, "playerCallback - onError, what:" + what + ", mp : " + mp);
                    return false;
                }
            });

            pipMediaPlayer.setOnPreparedListener(new DvbServicePlayer.OnPreparedListener() {
                @Override
                public void onPrepared(DvbServicePlayer mediaPlayer) {
                    String chInfo = "CH No. " +  mChannelData_Pip.get(mCurrentPosition_Pip).getCh() + "   -  " + mChannelData_Pip.get(mCurrentPosition_Pip).getName();
                    mChannelTitle_Pip.setText(chInfo);
                    SLog.d(TAG, "playerCallback - onPrepared, channel info : " + chInfo + ", mp : " + mediaPlayer);

                    pipMediaPlayer.start();
                }
            });

            if(isPIPShowing == true){
                SLog.d(TAG, "============== surfaceCreated - call setPIP. pos : " + pipMediaPlayer.getCurrentPosition() + " => " + mCurrentPosition_Pip);
                setPIP(mCurrentPosition_Pip);
            }
        }
        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            SLog.d(TAG, "onPipSurfaceCallback - surfaceChanged");
        }
        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            SLog.d(TAG, "onPipSurfaceCallback - surfaceDestroyed");
        }
    };

    private void setTV(int index) {
        SLog.d(TAG, "setTV - index : " + index + ", isVODPlaying : " + isVODPlaying + ", mChannelData : " + (mChannelData!=null));
        if (mChannelData == null) {
            return;
        }
        if(isVODPlaying) {
            mainVodMediaPlayer.reset();
            mainTVMediaPlayer.reset();
        }

        mChannelSpinner.setSelection(index);

        mainTVMediaPlayer.setDataSource(mChannelData.get(index).getChannelUri() + "&lf=1");
        //" port="49220" vpid="1718" apid="2718" apid2="0" ppid="1718" vst="36" ast="15"
        //mainMediaPlayer.setDataSource("skbiptv://239.192.81.36:49220?ch=7&ci=0&cp=0&pp=1718&vp=1718&vc=36&ap=2718&ac=15" + "&lf=1");
        //mainMediaPlayer.setDataSource("skbiptv://239.192.47.7:49220?ch=7&ci=0&cp=0&pp=1522&vp=1522&vc=27&ap=2522&ac=15" + "&lf=1");

        //mainMediaPlayer.setDataSource("skbiptv://239.192.67.99:49220?ch=7&ci=0&cp=0&pp=62&vp=62&vc=36&ap=63&ac=129" + "&lf=1");
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mainTVMediaPlayer.prepareAsync();
                }
            }).start();
        } catch(Exception e) {
            e.printStackTrace();
        }

        // Start Add dvbsi scenario - KGH
        String currentChannel = mChannelData.get(index).getChannelUri();
        if (mIsUseLocalInstance) {
            if (mDvbSIService != null) {
                mDvbSIService.setCurrentChannelUri(currentChannel);
            }
        } else {
            BtvNavigator.getDvbSIService(mContext).setCurrentChannelUri(currentChannel);
        }
        // End KGH

        if(mainTVMediaPlayer.isDualAudio() == true){
            mBtnAudioChangeLanguage.setVisibility(View.VISIBLE);
            mBtnAudioChangeLanguage.setText("Audio\n" + mainTVMediaPlayer.isMainAudio());
        }else{
            mBtnAudioChangeLanguage.setVisibility(View.GONE);
        }

        isVODPlaying = false;
		//Add dvbsi scenario - KGH
		//mChannelTitle.setVisibility(View.GONE);
        //mChannelTitle_Pip.setVisibility(View.GONE);
        //mChannelProgramInfo.setVisibility(View.VISIBLE);
        //mChannelProgramInfo_Pip.setVisibility(View.VISIBLE);
        mChannelTitle.setVisibility(View.INVISIBLE);
        mChannelTitle_Pip.setVisibility(View.INVISIBLE);
        mChannelProgramInfo.setVisibility(View.VISIBLE);
        mChannelProgramInfo_Pip.setVisibility(View.INVISIBLE);
        btnVOD.setVisibility(View.VISIBLE);
        btnFile.setVisibility(View.VISIBLE);
		// End KGH
//        updateProgramInfo();

        moveProgramPopUp(index);
    }

    /**
     * Start Add dvbsi scenario - KGH
     */
    private void setMusicTV(int index){
        if (mMusicChannelData == null) {
            return;
        }
        if (isVODPlaying) {
            SLog.i(TAG, "setTV isVODPlaying : " + isVODPlaying);
            mainVodMediaPlayer.reset();
            mainTVMediaPlayer.reset();

        }
        MusicDvbService currentAudioChannel = mMusicChannelData.get(index);
        mMusicChannelSpinner.setSelection(index);
        mainTVMediaPlayer.tune(currentAudioChannel.getChannelUri() + "&lf=1");

        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mainTVMediaPlayer.prepareAsync();
                }
            }).start();
        }catch (Exception e){
            e.printStackTrace();
        }

        if (mIsUseLocalInstance) {
            if (mDvbSIService != null) {
                mDvbSIService.setCurrentChannelUri(currentAudioChannel.getChannelUri());
            }
        } else {
            BtvNavigator.getDvbSIService(mContext).setCurrentChannelUri(currentAudioChannel.getChannelUri());
        }

        isVODPlaying = false;
        mMusicChannelSpinner.setVisibility(View.VISIBLE);
        mMusicChannelProgramInfo.setVisibility(View.VISIBLE);
        mChannelProgramInfo.setVisibility(View.GONE);
        mChannelTitle.setVisibility(View.INVISIBLE);
		mChannelTitle_Pip.setVisibility(View.INVISIBLE);
        btnVOD.setVisibility(View.GONE);
        btnFile.setVisibility(View.GONE);

        moveMusicProgramPopUp(index);
    }


    private void moveMusicProgramPopUp(int index){
        musichChannelAudioPid = mMusicChannelData.get(index).getApid();
        List<MusicProgram> audioPid_program = null;
        if (mIsUseLocalInstance) {
            if (mDvbSIService != null) {
                audioPid_program = mDvbSIService.getMusicProgramList(musichChannelAudioPid).getMusicProgramList();
            }
        } else {
            audioPid_program = BtvNavigator.getDvbSIService(mContext).getMusicProgramList(musichChannelAudioPid).getMusicProgramList();
        }
        if(audioPid_program != null && audioPid_program.size() >0){
            if(mMusicProgramList != null) mMusicProgramList.clear();
            for(MusicProgram entity : audioPid_program){
                    mMusicProgramList.add(entity);
            }
            musicProgramAdapter.setList(mMusicProgramList);
            musicProgramAdapter.notifyDataSetChanged();
            currentMusicRunningProgramIndex = getCurrentRunningMusicProgram();
            mMusicChannelProgramInfo.setSelection(currentMusicRunningProgramIndex);
            long gap = mMusicProgramList.get(currentMusicRunningProgramIndex).getEndDate().getTime()- System.currentTimeMillis();
            mHandler.removeCallbacks(updateMusicProgramRunnable);
            mHandler.postDelayed(updateMusicProgramRunnable, gap+1000);
        } /*else {
            mHandler.postDelayed(testRunnable, 1000);
        }*/
    }

    private int getCurrentRunningProgram(){
        int retIndex = -1;
        Calendar calendar = Calendar.getInstance();
        long reqST = calendar.getTimeInMillis();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        long reqET = calendar.getTimeInMillis();
        if(mProgramList != null && mProgramList.size() > 0){
            for(AVProgram entity : mProgramList) {
                retIndex++;
                long proST = entity.getStartDate().getTime();
                long proET = entity.getEndDate().getTime();
                if ((reqST <= proST && reqET >= proET)
                        || (reqST > proST && reqET >= proET && reqST < proET)
                        || (reqST <= proST && reqET < proET && reqET > proST)
                        || (reqST > proST && reqET < proET))
                {
                    break;
                }
            }
        }
        SLog.d(TAG, "getCurrentRunningProgram() index : "+retIndex);
        return retIndex;
    }

    private int getCurrentRunningProgram_Pip(){
        int retIndex = -1;
        Calendar calendar = Calendar.getInstance();
        long reqST = calendar.getTimeInMillis();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        long reqET = calendar.getTimeInMillis();
        if(mProgramList_Pip != null && mProgramList_Pip.size() > 0){
            for(AVProgram entity : mProgramList_Pip) {
                retIndex++;
                long proST = entity.getStartDate().getTime();
                long proET = entity.getEndDate().getTime();
                if ((reqST <= proST && reqET >= proET)
                        || (reqST > proST && reqET >= proET && reqST < proET)
                        || (reqST <= proST && reqET < proET && reqET > proST)
                        || (reqST > proST && reqET < proET))
                {
                    break;
                }
            }
        }
        SLog.d(TAG, "getCurrentRunningProgram_Pip() index : "+retIndex);
        return retIndex;
    }


    private int getCurrentRunningMusicProgram(){
        int retIndex = -1;
        Calendar calendar = Calendar.getInstance();
        long reqST = calendar.getTimeInMillis();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        long reqET = calendar.getTimeInMillis();
        if(mMusicProgramList != null && mMusicProgramList.size() > 0){
            for(MusicProgram entity : mMusicProgramList) {
                retIndex++;
                long proST = entity.getStartDate().getTime();
                long proET = entity.getEndDate().getTime();
                if ((reqST <= proST && reqET >= proET)
                        || (reqST > proST && reqET >= proET && reqST < proET)
                        || (reqST <= proST && reqET < proET && reqET > proST)
                        || (reqST > proST && reqET < proET))
                {
                    break;
                }
            }
        }
        SLog.d(TAG, "getCurrentRunningMusicProgram() index : "+retIndex);
        return retIndex;
    }

    private void displayMultiviewSelecteAlertDialog(){
        CharSequence [] multichannelname = new CharSequence [mMultiViewChannelData.size()];
        for(int i=0; i< mMultiViewChannelData.size(); i++){
            multichannelname[i] = mMultiViewChannelData.get(i).getGroupName();
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("멀티 채널 선택하세요.");
        builder.setItems(multichannelname, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                displayMultiView(mMultiViewChannelData.get(which));
            }
        });
        builder.setNegativeButton("취소", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void displayMultiView(MultiViewDvbService multiview){
        List<MultiViewLogicalCell> logicalCellList = null;
        AVProgramList program_result = null;
        if (mIsUseLocalInstance) {
            if (mDvbSIService != null) {
                logicalCellList = mDvbSIService.getMultiViewLogicalCellList(multiview.getGroupID()).getMultiViewLogicalCellList();
            }
        } else {
            logicalCellList = BtvNavigator.getDvbSIService(mContext).getMultiViewLogicalCellList(multiview.getGroupID()).getMultiViewLogicalCellList();
        }
        String channelList="";
        if(logicalCellList != null && logicalCellList.size() > 0) {
            if(isPIPShowing == true){
                SLog.d(TAG, "============== displayMultiView - call setPIPChange. pos : " + pipMediaPlayer.getCurrentPosition() + " => " + mCurrentPosition_Pip);
                mChannelSpinner_Pip.setSelection(mCurrentPosition_Pip);
                setPIPChange(false);
            }

            for (MultiViewLogicalCell entity : logicalCellList) {
                channelList += "" + entity.getChannelNumber() + "|";
            }
            channelList = channelList.substring(0, channelList.lastIndexOf('|'));
            multiViewChannel= channelList;
            if (mIsUseLocalInstance) {
                if (mDvbSIService != null) {
                    mDvbSIService.setCurrentMultiChannel(channelList);
                    mMultiViewURI = mDvbSIService.makeMultiViewUri(logicalCellList);
                }
            } else {
                BtvNavigator.getDvbSIService(mContext).setCurrentMultiChannel(channelList);
                mMultiViewURI = BtvNavigator.getDvbSIService(mContext).makeMultiViewUri(logicalCellList);
            }
            int ch = Integer.valueOf(channelList.substring(channelList.lastIndexOf('|') + 1));
            SLog.e(TAG, "multiview - displayMultiView - channel count : " + mChannelData_Pip.size()
                    + ", to find ch : " + ch);
            int pos = 0;
            for (int i=0; i<mChannelData_Pip.size(); i++) {
                if (ch == mChannelData_Pip.get(i).getCh()) {
                    pos = i;
                }
            }
            SLog.e(TAG, "multiview - displayMultiView - isPIPShowing : " + isPIPShowing
                    + ", pos : " + pos + ", mMultiViewURI : " + mMultiViewURI);
            showMultiView(true, pos);
        }
        btnMultiviewProgram.setVisibility(View.VISIBLE);
    }
    /**
     * End KGH
     */

    /**
     * 프로그램 정보
     * @param index +1이 해당 채널 번호
     */
    private void moveProgramPopUp(int index){
        currentChannelNumber = mChannelData.get(index).getCh();
        AVProgramList program_result = null;
        if (mIsUseLocalInstance) {
            if (mDvbSIService != null) {
                program_result = mDvbSIService.getAVProgramList(currentChannelNumber);
            }
        } else {
            program_result = BtvNavigator.getDvbSIService(mContext).getAVProgramList(currentChannelNumber);
        }
        if(program_result != null && program_result.getAVProgramsList() != null && program_result.getAVProgramsList().size() > 0){
            if(mProgramList != null) mProgramList.clear();
            if(program_result.getAVProgramsList().get(0) != null && program_result.getAVProgramsList().get(0).getAVProgramList() != null) {
                for (AVProgram tempValue : program_result.getAVProgramsList().get(0).getAVProgramList()) {
                    mProgramList.add(tempValue);
                }
                programAdapter.setList(mProgramList);
                programAdapter.notifyDataSetChanged();
                currentRunningProgramIndex = getCurrentRunningProgram();
                mChannelProgramInfo.setSelection(currentRunningProgramIndex);
            }
        }
        mMusicChannelSpinner.setVisibility(View.GONE);
        mMusicChannelProgramInfo.setVisibility(View.GONE);
        mChannelProgramInfo.setVisibility(View.VISIBLE);
    }

    private void setMultiTV() {
        SLog.d(TAG, "setMultiTV - mMultiViewURI : " + mMultiViewURI + ", mChannelData : " + mChannelData);
        if (mChannelData == null) {
            return;
        }
		// Add dvbsi scenario - KGH
        /*
        if(siManager != null){
            List<MultiViewChannel> multiViewChannel = siManager.getMultiViewChannelList().getMultiViewChannelList();
            if(multiViewChannel != null && multiViewChannel.size() > 0){
                String uri = siManager.makeMultiViewUri(multiViewChannel.get(0).getLogicalCellInfoList());
                mainMediaPlayer.setDataSource(uri);
            }
        } else  mainMediaPlayer.setDataSource("skbsmv://reserved/?view1=skbiptv://239.192.67.99:49220?ch=7&ci=0&cp=0&pp=62&vp=62&vc=36&ap=63&ac=129&view2=skbiptv://239.192.67.100:49220?ch=9&ci=0&cp=0&pp=52&vp=52&vc=36&ap=53&ac=129&view3=skbiptv://239.192.67.101:49220?ch=11&ci=0&cp=0&pp=1215&vp=1215&vc=36&ap=1216&ac=129");
        */
        if(mMultiViewURI != null && mMultiViewURI.isEmpty() == false) {
            mainTVMediaPlayer.tune(mMultiViewURI);
            try {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mainTVMediaPlayer.prepareAsync();
                    }
                }).start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        mBtnAudioChangeLanguage.setVisibility(View.GONE);
        mChannelProgramInfo.setVisibility(View.GONE);
        mMusicChannelProgramInfo.setVisibility(View.GONE);
		// End KGH
    }

    private void setPIP(int index) {
        SLog.d(TAG, "setPIP - index : " + index);
        if (mChannelData_Pip == null || index < 0) {
            return;
        }

        if(isVODPlaying) {
            pipMediaPlayer.reset();
        }
//
//        pipMediaPlayer.setDataSource("skbvod://cf2.hanafostv.com:555/adv/c22195-140912175323.ts");
//        pipMediaPlayer.prepareAsync();
//        pipMediaPlayer.start();
//
//        return;


//        if(pipMediaPlayer == null){
//            pipMediaPlayer = new MediaPlayer(1);
//            pipMediaPlayer.setDisplay(mIPTVSurfaceHolder_Pip);
//            pipMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                @Override
//                public void onPrepared(MediaPlayer mediaPlayer) {
//                    String chInfo = "CH No. " + mChannelData.get(mCurrentPosition_Pip).getChannelNum() + "   -  " + mChannelData.get(mCurrentPosition_Pip).getChannelName();
//                    mChannelTitle_Pip.setText(chInfo);
//
//                    pipMediaPlayer.start();
//                }
//            });
//        }

        SLog.d(TAG, "setPIP - call setDataSource channel : " + mChannelData_Pip.get(index).getChannelUri());
        pipMediaPlayer.setDataSource(mChannelData_Pip.get(index).getChannelUri());
        mChannelSpinner_Pip.setSelection(index);
//        updateProgramInfo();
        currentChannelNumber_Pip = mChannelData_Pip.get(index).getCh();
        pipMediaPlayer.prepareAsync();
        moveProgramPopUp_Pip(index);
    }

	// Add dvbsi scenario - KGH
    private void moveProgramPopUp_Pip(int index){
        if(isPIPShowing) {
            AVProgramList programPip_result = null;
            if (mIsUseLocalInstance) {
                if (mDvbSIService != null) {
                    programPip_result = mDvbSIService.getAVProgramList(currentChannelNumber_Pip);
                }
            } else {
                programPip_result = BtvNavigator.getDvbSIService(mContext).getAVProgramList(currentChannelNumber_Pip);
            }
            if (programPip_result != null && programPip_result.getAVProgramsList() != null && programPip_result.getAVProgramsList().size() > 0) {
                if (mProgramList_Pip != null) mProgramList_Pip.clear();
                for (AVProgram tempValue : programPip_result.getAVProgramsList().get(0).getAVProgramList()) {
                    mProgramList_Pip.add(tempValue);
                }
                programAdapter_Pip.setList(mProgramList_Pip);
                programAdapter_Pip.notifyDataSetChanged();
                currentRunningProgramIndex_Pip = getCurrentRunningProgram_Pip();
                mChannelProgramInfo_Pip.setSelection(currentRunningProgramIndex_Pip);
                mChannelProgramInfo_Pip.setVisibility(View.VISIBLE);
            }
        }
    }
	// End KGH

    private void setVOD(final String uri) {

        SLog.d(TAG, "setVOD");
        trickSpeed = MediaPlayer.TRICK_SPEED.X010;
        mainTVMediaPlayer.reset();
        mainVodMediaPlayer.reset();

        mainVodMediaPlayer.setOnWebVTTUpdateListener(new VodPlayer.OnWebVTTUpdateListener() {
            @Override
            public void onWebVTTUpdated(VodPlayer mp, List<WebVTT> webVTTList) {
                SLog.d(TAG, "onWebVTTUpdated");
                for(WebVTT info : webVTTList) {
                    SLog.i("IPTV_CALLBACK", "start time:" + info.startTime + ", end time:" + info.endTime + ", text:" + info.text);
                }
            }
        });

        mainVodMediaPlayer.setDataSource(uri);
        mChannelTitle.setText("VOD : x" + trickSpeed.getSpeedRatio());

        mainVodMediaPlayer.prepare();
        mainVodMediaPlayer.start();

        isVODPlaying = true;
        isPaused = false;

        mChannelTitle.setVisibility(View.VISIBLE);
        mChannelTitle_Pip.setVisibility(View.VISIBLE);
        mChannelProgramInfo.setVisibility(View.GONE);
        mChannelProgramInfo_Pip.setVisibility(View.INVISIBLE);  // Add dvbsi scenario - KGH
    }

    @Override
    public void onClick(View v) {
        SLog.d(TAG, "onClick" +v.getId());
        int id = v.getId();
        switch (id) {
            case R.id.btn_watermark: {
                SLog.d(TAG, "onClickOverlay");
                BtvNavigator.getWatermarkService(mContext).setStbId((short)0, 0xabcdef12);
                BtvNavigator.getWatermarkService(mContext).setStubEmbedding(3);
                BtvNavigator.getWatermarkService(mContext).setActive(true);
                BtvNavigator.getWatermarkService(mContext).setWatermarkView(mImageWatermark);
                if (isWatermarkStarted) {
                    BtvNavigator.getWatermarkService(mContext).stopWatermark();
                } else {
                    BtvNavigator.getWatermarkService(mContext).startWatermark();
                }
                isWatermarkStarted = !isWatermarkStarted;
                BtvNavigator.getWatermarkService(mContext).release();
                break;
            }
            case R.id.iv_up:
                moveChannel(true, false);
                /*if(isPIPShowing == false){
                    mCurrentPosition++;
                    if (mCurrentPosition > mChannelData.size() - 1) {
                        mCurrentPosition = 0;
                    }
                    mChannelSpinner.setSelection(mCurrentPosition);
                    //setTV(mCurrentPosition);
                }*/
                break;
            case R.id.iv_down:
                moveChannel(false, false);
                /*if(isPIPShowing == false){
                    mCurrentPosition--;
                    if (mCurrentPosition < 0) {
                        mCurrentPosition = mChannelData.size() - 1;
                    }
                    mChannelSpinner.setSelection(mCurrentPosition);
                    //setTV(mCurrentPosition);
                }*/
                break;
            case R.id.iv_play:
                //if(isPIPShowing == false){
                    playTimerStart(mTimer, mTask, 0);
                //}
                break;
            case R.id.iv_pause:
                playTimerStop(mTimer, mTask, 0);
                break;
            case R.id.btn_pip:

                Parcel request = Parcel.obtain();//mainMediaPlayer.newRequest();
                Parcel reply = Parcel.obtain();
                request.writeInt(1105);
                request.setDataPosition(0);
                mainVodMediaPlayer.invoke(request, reply);
                long pts = reply.readLong();
                if(isPIPShowing) {
                    setPIPChange(false);
                } else {
                    setPIPChange(true);
                }
                break;
            case R.id.iv_up_pip:
                moveChannel(true, true);
                /*if(isPIPShowing) {
                    mCurrentPosition_Pip++;
                    if (mCurrentPosition_Pip > mChannelData_Pip.size() - 1) {
                        mCurrentPosition_Pip = 0;
                    }
                    mChannelSpinner_Pip.setSelection(mCurrentPosition_Pip);
                    //setPIP(mCurrentPosition_Pip);
                }*/
                break;
            case R.id.iv_down_pip:
                moveChannel(false, true);
                /*if(isPIPShowing) {
                    mCurrentPosition_Pip--;
                    if (mCurrentPosition_Pip < 0) {
                        mCurrentPosition_Pip = mChannelData_Pip.size() - 1;
                    }
                    mChannelSpinner_Pip.setSelection(mCurrentPosition_Pip);
                    //setPIP(mCurrentPosition_Pip);
                }*/
                break;
            case R.id.iv_play_pip:
                if(isPIPShowing == true){
                    playTimerStart(mTimer_Pip, mTask_Pip, 1);
                }
                break;
            case R.id.iv_pause_pip:
                playTimerStop(mTimer_Pip, mTask_Pip,1);
                break;
            case R.id.btn_vod: {


                //setVOD("skbvod://cf2.hanafostv.com:555/adv/c22195-140912175323.ts");
                //setVOD("skbhttps://reserved/?contents=https://ecdnedge-poc.hanafostv.com:443/vod/01010377_201910281319_00000000000/pa2My2mO0gStNt4szJ%2Fej6QRiT4wkhlryQXQKZusl1bvjkfFEhQqiEeV7gHzCltbP7Lc6HNz2O1nXk8eZLXvuPOz8VU%2BEojqdeaGiWEz6FW0ggijqxy0rC%2FqS%2Bpb%2FhZ3LCcBoTmwI9OpGPG8i72FNxZsRAbHIVg2fnicIRxYstoPj%2BOaDP%2BV94XVwdemIgCyAd5J5t3PhjeENcm5sHU6%2FK4zqawkPRCka9zxi10sK%2FGbdHk3BAXRin9jXCcxJcW%2BiDc0VDmCkLTCPLtb1bOkCID8GEpZokJSaHKaTbfZpTLziBA%2BED6WWJ2gtHUl58%2FBGgcO3s4lIv6hpS2VoerxiXY6Zlrp1e0YUwW%2FJOn40iLjU%2FesKqN4Y3V16fqNFPYfJhnqMiarQoGw%2FM8G%2BoLCNw%2FHdUBZ%2Beek77d449UpCkFvT%2FcRFi2M8MdXWo1cdagaD4JzWQNXVpkz6T%2BJCSf%2BSz89AW4b0P5buxhjrZ1Jjg5gIdZrxWUUJoigJ15fSnEQlV4ncQsiQbgrC2OTaCIs88yoadUuvyploRBU1Yma%2FgMrFAL%2BOPg5rx%2FLyRmEjKo4H2WmahsNZrWJnIS1Q1Al2SMMUC2sxUgtJRSFNcOMHOGNVFTzwkFatDQVkOH8zNMZh9f8H95zpAAZ4%2B1Pn2XVbeuj1XE9LxfngfwF0LUwD0yktsqArYAWWes9XMlMZRWwdP6noO2NtnrDr7uH9BiNG0G1K5bUIY6aT5jQm%2Fvr%2BrHgzLpb%2FcWRF%2FDIEEIqvqRWV%2FI5ywHztUN8PwRtMMecfZYL%2B5un5t7t27mqH0ZzlFVN6AC11NAW6RWJ9es%2BfDuYZkXkc5IMcLbtmhztJj%2F%2BoQWReKMOfONOV8f7UKYwuYSQ2IeW35aKZDEZqGFoXWkA/CD1000153533_20181004095402.m3u8&license=https://ecdnlicense.hanafostv.com/vod/01010377_201910281319_00000000000/pa2My2mO0gStNt4szJ%2Fej6QRiT4wkhlryQXQKZusl1bvjkfFEhQqiEeV7gHzCltbP7Lc6HNz2O1nXk8eZLXvuPOz8VU%2BEojqdeaGiWEz6FW0ggijqxy0rC%2FqS%2Bpb%2FhZ3LCcBoTmwI9OpGPG8i72FNxZsRAbHIVg2fnicIRxYstoPj%2BOaDP%2BV94XVwdemIgCyAd5J5t3PhjeENcm5sHU6%2FK4zqawkPRCka9zxi10sK%2FGbdHk3BAXRin9jXCcxJcW%2BiDc0VDmCkLTCPLtb1bOkCID8GEpZokJSaHKaTbfZpTLziBA%2BED6WWJ2gtHUl58%2FBGgcO3s4lIv6hpS2VoerxiXY6Zlrp1e0YUwW%2FJOn40iLjU%2FesKqN4Y3V16fqNFPYfJhnqMiarQoGw%2FM8G%2BoLCNw%2FHdUBZ%2Beek77d449UpCkFvT%2FcRFi2M8MdXWo1cdagaD4JzWQNXVpkz6T%2BJCSf%2BSz89AW4b0P5buxhjrZ1Jjg5gIdZrxWUUJoigJ15fSnEQlV4ncQsiQbgrC2OTaCIs88yoadUuvyploRBU1Yma%2FgMrFAL%2BOPg5rx%2FLyRmEjKo4H2WmahsNZrWJnIS1Q1Al2SMMUC2sxUgtJRSFNcOMHOGNVFTzwkFatDQVkOH8zNMZh9f8H95zpAAZ4%2B1Pn2XVbeuj1XE9LxfngfwF0LUwD0yktsqArYAWWes9XMlMZRWwdP6noO2NtnrDr7uH9BiNG0G1K5bUIY6aT5jQm%2Fvr%2BrHgzLpb%2FcWRF%2FDIEEIqvqRWV%2FI5ywHztUN8PwRtMMecfZYL%2B5un5t7t27mqH0ZzlFVN6AC11NAW6RWJ9es%2BfDuYZkXkc5IMcLbtmhztJj%2F%2BoQWReKMOfONOV8f7UKYwuYSQ2IeW35aKZDEZqGFoXWkA/CD1000153533_20181004095402.m3u8");
                setVOD("skbhttps://reserved/?contents=https://ecdnedge-poc.hanafostv.com:443/vod/01010377_201911071035_00000000002/pa2My2mO0gRVdWPQmhWNbjq7UOXXAqn39yN5bMT99Bc%2B%2BOmgNpoVoW0azZUFoZZgetQsojLBqjcwKdMpTIktgYUFHNFEGc2yD%2FymTAuiZElNz5TOxFYPlnSrRoShThxaXvhgFoA5Xxvd11VGPXU0SEiK%2B2z8VRR9ml5IzstTuchfJ%2Fwp%2FssdMsFJKGPPdO04kgsqYpQPlnrVBaEdP0klKTZ44wMOBmvPAY%2F6VeFUIUv8uAohI3r8PqP9qYyk6KSFNFMPF2LUPAUHpgbwt%2BIkLYjc0MQNPQhXMBl9KgRb41jvKIGzqVfcUUQRVRD5CDlFqOkFBh6geTzcx%2FLrbSKybKUAFR1GVeZlLaUgnp%2Fz50mto3ZYduI8eAABEoRO98%2Bop%2B%2Brt8qajc3dAbCiOnf5ZI3vk1zgYaU5x2r4YoH3x%2FWA42cGi1NFyF11VAkUrmp6jWMyKVFASTRSPze4wiU7o2kYb3mhaenD7pPx%2FFH88PovmKYxwQY1jktfTJYBWE%2BN%2BOouy3hRsYwbnU54WZEOzoSII8MLkkDPofXfrmS0ZwSMhsTdhExZfzXC6MN4M53WdqkyovkiGsB0%2Bzg05m5wS0fME1vtR5%2FShEgwOWq37bPbVFcTT5VXL%2B1XZb02LpOsB6e1iX103nPqqHtR2sW%2BA9N2R5UxEZZRM41bQlx3cFKSoVOeJB2PLx6zJg%2F3OYt24UWtcbvBf%2FnRdC86Q69Xxbcc2pPKqWFe24rlBwnhmT7fq0rPWrEQm01d%2BeD70%2B4S4VqudtQX%2BW3JNRl2yPNWTXjCm60wpCVx1GqmSYpBsZnogRqzv5K%2BCF4nTqjHU4z28WRSwHLrgHygaNneEFRtVoGXo0rPNovJBDWXuTVGmUnjSBdj3M3eZWX6VavrBApL/CD1000153533_20181004095402.m3u8&license=https://ecdnlicense.hanafostv.com/vod/01010377_201911071035_00000000002/pa2My2mO0gRVdWPQmhWNbjq7UOXXAqn39yN5bMT99Bc%2B%2BOmgNpoVoW0azZUFoZZgetQsojLBqjcwKdMpTIktgYUFHNFEGc2yD%2FymTAuiZElNz5TOxFYPlnSrRoShThxaXvhgFoA5Xxvd11VGPXU0SEiK%2B2z8VRR9ml5IzstTuchfJ%2Fwp%2FssdMsFJKGPPdO04kgsqYpQPlnrVBaEdP0klKTZ44wMOBmvPAY%2F6VeFUIUv8uAohI3r8PqP9qYyk6KSFNFMPF2LUPAUHpgbwt%2BIkLYjc0MQNPQhXMBl9KgRb41jvKIGzqVfcUUQRVRD5CDlFqOkFBh6geTzcx%2FLrbSKybKUAFR1GVeZlLaUgnp%2Fz50mto3ZYduI8eAABEoRO98%2Bop%2B%2Brt8qajc3dAbCiOnf5ZI3vk1zgYaU5x2r4YoH3x%2FWA42cGi1NFyF11VAkUrmp6jWMyKVFASTRSPze4wiU7o2kYb3mhaenD7pPx%2FFH88PovmKYxwQY1jktfTJYBWE%2BN%2BOouy3hRsYwbnU54WZEOzoSII8MLkkDPofXfrmS0ZwSMhsTdhExZfzXC6MN4M53WdqkyovkiGsB0%2Bzg05m5wS0fME1vtR5%2FShEgwOWq37bPbVFcTT5VXL%2B1XZb02LpOsB6e1iX103nPqqHtR2sW%2BA9N2R5UxEZZRM41bQlx3cFKSoVOeJB2PLx6zJg%2F3OYt24UWtcbvBf%2FnRdC86Q69Xxbcc2pPKqWFe24rlBwnhmT7fq0rPWrEQm01d%2BeD70%2B4S4VqudtQX%2BW3JNRl2yPNWTXjCm60wpCVx1GqmSYpBsZnogRqzv5K%2BCF4nTqjHU4z28WRSwHLrgHygaNneEFRtVoGXo0rPNovJBDWXuTVGmUnjSBdj3M3eZWX6VavrBApL/CD1000153533_20181004095402.m3u8");
            }
                break;
            case R.id.btn_file: {
                setVOD("skbfile:///data/btv_home/DATA/adv/ID_main.ts");
            }
                break;
            case R.id.btn_audio_language:
                if(mainTVMediaPlayer !=  null){
                    mainTVMediaPlayer.switchAudio();
                    mBtnAudioChangeLanguage.setText("Audio\n" + mainTVMediaPlayer.isMainAudio());
                }
                break;
            case R.id.btn_multiview:
			//Add dvbsi scenario - KGH
                if(!isMultiView) {
                    displayMultiviewSelecteAlertDialog();
                } else {
                    showMultiView(false);
                }
                /*
                if(isMultiView == true){
                    showMultiView(false);
                }else {
                    showMultiView(true);

                    if(isPIPShowing == true){
                        mChannelSpinner_Pip.setSelection(mCurrentPosition_Pip);
                        setPIPChange(false);
                    }
                }
                */
				// End KGH
                break;
        }
    }
    private void playBtnVisibility(ImageView play, ImageView pause, boolean visible) {
		SLog.d(TAG, "playBtnVisibility");
        if(play != null && pause != null) {  // Add dvbsi scenario - KGH 
            play.setVisibility(View.GONE);
            pause.setVisibility(View.GONE);
            if (visible) {
                play.setVisibility(View.VISIBLE);
                play.requestFocus();
            } else {
                pause.setVisibility(View.VISIBLE);
                pause.requestFocus();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        SLog.d(TAG, "onPause");
        playTimerStop(mTimer, mTask,0);
        playTimerStop(mTimer_Pip, mTask_Pip,1);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SLog.d(TAG, "onDestroy");
        if(mainTVMediaPlayer != null) {
            mainTVMediaPlayer.release();
        }

        if(mainVodMediaPlayer != null) {
            mainVodMediaPlayer.release();
        }

        if(pipMediaPlayer != null) {
            pipMediaPlayer.release();
        }

//        SIService si = ServiceCoreAPI.getService(ServiceCoreAPI.SI_SERVICE);
//        si.disconnect();
        if(siChangedReceiver != null) unregisterReceiver(siChangedReceiver);  // Add dvbsi scenario - KGH
		//Add dvbsi scenario - KGH
		// END KGH
        BtvFramework.getInstance(this).release();
        mDvbSIService.clearOnDsmccListener(onDsmccListener);
        BtvNavigator.getDvbSIService(this).release();
    }

    private void playTimerStart(Timer timer, TimerTask task, final int type) {
        SLog.d(TAG, "playTimerStart");
        int zappingTime = 10000;
        if(type == 0){
            zappingTime = mZappingTime;
        }else if(type == 1){
            zappingTime = mZappingTime_Pip;
        }

        task = new TimerTask() {
            public void run() {
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(type == 0){
                                if (isTimerAlive) {
                                    mCurrentPosition++;
                                    if (mCurrentPosition > mChannelData.size() - 1) {
                                        mCurrentPosition = 0;
                                    }
                                    mChannelSpinner.setSelection(mCurrentPosition);
                                    SLog.d(TAG, "===================   timer task - call setTV");
                                    setTV(mCurrentPosition);
                                    isTimerAlive = false;
                                }
                                isTimerAlive = true;
                            }else if(type == 1){
                                if (isTimerAlive_Pip) {
                                    mCurrentPosition_Pip++;
                                    if (mCurrentPosition_Pip > mChannelData_Pip.size() - 1) {
                                        mCurrentPosition_Pip = 0;
                                    }
                                    mChannelSpinner_Pip.setSelection(mCurrentPosition_Pip);
                                    SLog.d(TAG, "===================   timer task - call setPIP");
                                    setPIP(mCurrentPosition_Pip);
                                    isTimerAlive_Pip = false;
                                }
                                isTimerAlive_Pip = true;
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        timer = new Timer();
        timer.schedule(task, 0, zappingTime);
        if(type == 0){
            mTimer = timer;
            mTask = task;
            playBtnVisibility(mImagePlay, mImagePause, false);
        }else if(type == 1){
            mTimer_Pip = timer;
            mTask_Pip = task;
            playBtnVisibility(mImagePlay_Pip, mImagePause_Pip, false);
        }
    }

    private void playTimerStop(Timer timer, TimerTask task, int type) {
        SLog.d(TAG, "playTimerStop");

        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (task != null) {
            task.cancel();
            task = null;
        }

        if(type == 0){
            isTimerAlive = false;
            playBtnVisibility(mImagePlay, mImagePause, true);
        }else if(type == 1){
            isTimerAlive_Pip = false;
            playBtnVisibility(mImagePlay_Pip, mImagePause_Pip, true);
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        SLog.d(TAG, "onKeyUp KeyEvent=" +event.getKeyCode());
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_CHANNEL_UP:
                moveChannel(true, isPIPShowing);
                /*mCurrentPosition++;

                if (mCurrentPosition > mChannelData.size() - 1) {
                    mCurrentPosition = 0;
                }
                mChannelSpinner.setSelection(mCurrentPosition);
                setTV(mCurrentPosition);*/
                break;
            case KeyEvent.KEYCODE_CHANNEL_DOWN:
                moveChannel(false, isPIPShowing);
                /*mCurrentPosition--;

                if (mCurrentPosition < 0) {
                    mCurrentPosition = mChannelData.size() - 1;
                }
                mChannelSpinner.setSelection(mCurrentPosition);
                setTV(mCurrentPosition);*/
                break;
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                if(trickSpeed == MediaPlayer.TRICK_SPEED.X010) {
                    if(isPaused) {
                        isPaused = false;
                        mainVodMediaPlayer.start();
                    } else {
                        mainVodMediaPlayer.pause();
                        isPaused = true;
                    }
                } else {
                    trickSpeed = MediaPlayer.TRICK_SPEED.X010;
                    mainVodMediaPlayer.trick(trickSpeed);
                    mChannelTitle.setText("VOD : x" + trickSpeed.getSpeedRatio());
                }
                break;
            case KeyEvent.KEYCODE_MEDIA_STOP:
                mChannelSpinner.setSelection(mCurrentPosition);
                setTV(mCurrentPosition);
                break;
            case KeyEvent.KEYCODE_MEDIA_REWIND:
                if(isVODPlaying) {
                    trickSpeed = trickSpeed.prev();
                    mainVodMediaPlayer.trick(trickSpeed);
                    mChannelTitle.setText("VOD : x" + trickSpeed.getSpeedRatio());
                }
                break;
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                if(isVODPlaying) {
                    trickSpeed = trickSpeed.next();
                    mainVodMediaPlayer.trick(trickSpeed);
                    mChannelTitle.setText("VOD : x" + trickSpeed.getSpeedRatio());
                }
                break;
            case KeyEvent.KEYCODE_MEDIA_NEXT:
                if(isVODPlaying) {
                    tempPosition = tempPosition + 300;
                    mainVodMediaPlayer.seekTo(tempPosition);
                }
                break;
            case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                if(isVODPlaying) {
                    tempPosition = tempPosition - 300;
                    if(tempPosition < 0) {
                        tempPosition = 0;
                    }
                    mainVodMediaPlayer.seekTo(tempPosition);
                }
                break;
            case KeyEvent.KEYCODE_0:
            case KeyEvent.KEYCODE_1:
            case KeyEvent.KEYCODE_2:
            case KeyEvent.KEYCODE_3:
            case KeyEvent.KEYCODE_4:
            case KeyEvent.KEYCODE_5:
            case KeyEvent.KEYCODE_6:
            case KeyEvent.KEYCODE_7:
            case KeyEvent.KEYCODE_8:
            case KeyEvent.KEYCODE_9:
                mHandler.removeCallbacks(mRunSetCh);
                mChannel = 10 * mChannel + event.getKeyCode() - KeyEvent.KEYCODE_0;
                mHandler.postDelayed(mRunSetCh, 2000);
                if (mTvCh != null) {
                    mTvCh.setText("" + mChannel);
                }
                break;

        }
        return super.onKeyUp(keyCode, event);
    }

    private void moveChannel(boolean isNext, boolean isPip) {
        int idx, count;
        if (isPip) {
            idx = mChannelSpinner_Pip.getSelectedItemPosition();
            count = mChannelSpinner_Pip.getCount();
        } else {
            idx = mChannelSpinner.getSelectedItemPosition();
            count = mChannelSpinner.getCount();
        }
        if (isNext) {
            if (++idx >= count) {
                idx = 0;
            }
        } else {
            if (--idx < 0) {
                idx = count - 1;
            }
        }
        //SLog.d(TAG, "moveChannel - call setTV or setPIP. isPIPShowing : " + isPIPShowing);
        if (isPip) {
            mCurrentPosition_Pip = idx;
            SLog.d(TAG, "===========   moveChannel - call setPIP. isPIPShowing : " + isPIPShowing);
            if (isPIPShowing) {
                setPIP(idx);
            } else {
                mChannelSpinner_Pip.setSelection(idx);
            }
        } else {
            mCurrentPosition = idx;
            setTV(idx);
        }
    }

    private void setVersion(){
        SLog.d(TAG, "setVersion");
        mVersionInfo.setText("Ver : " + Utils.getAppVersionName(mContext));
    }

    public void showPipExceptionToast(){
        SLog.d(TAG, "showPipExceptionToast");
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_pip_exception, (ViewGroup) findViewById(R.id.linear_toast));
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    AdapterView.OnItemSelectedListener onChannelItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
            SLog.d(TAG, "onChannelItemSelectedListener isFirstStart : " + isFirstStart
                    + ", is s_channel : " + (parent.getId() == R.id.s_channel) + ", Item:" +parent.getId());
            if(isFirstStart == true){
                isFirstStart = false;
                return;
            }

            int vId = parent.getId();
            if(vId == R.id.s_channel){

                mCurrentPosition = i;
//                if (mCurrentPosition > mChannelData.size() - 1) {
//                    mCurrentPosition = 0;
//                }

                if(mainTVMediaPlayer != null){
                    // Start Add dvbsi scenario - KGH
                    // setTV(mCurrentPosition);
                    if(mChannelData != null && mChannelData.get(mCurrentPosition).getChannelType() != IAVDvbService.AVDVBSERVICE_CHANNEL_TYPE_MUSIC_CHANNEL){
                        setTV(mCurrentPosition);
                    }else {
                        setMusicTV(0);
                    }
                    // End KGH
                }
            }else if(vId == R.id.s_channel_pip){
//                if (mCurrentPosition_Pip > mChannelData_Pip.size() - 1) {
//                    mCurrentPosition_Pip = 0;
//                }
                AVProgramList o = new AVProgramList();
                if(pipMediaPlayer != null && isPIPShowing == true){
                    SLog.d(TAG, "onItemSelected - pos : " + i + ", " + mCurrentPosition_Pip);
                    if (i != mCurrentPosition_Pip) {
                        SLog.d(TAG, "============== onItemSelected - call setPIP");
                        setPIP(i);
                    }
                }
                mCurrentPosition_Pip = i;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {}
    };

    AdapterView.OnItemSelectedListener onZappingItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
            SLog.d(TAG, "onZappingItemSelectedListener Item" +parent.getId());
            int vId = parent.getId();
            if(vId == R.id.s_zapping){
                if (i == 0) {
                    mZappingTime = 10000;
                } else if (i == 1) {
                    mZappingTime = 5000;
                } else if (i == 2) {
                    mZappingTime = 3000;
                } else if (i == 3) {
                    mZappingTime = 1000;
                }
            }else if(vId == R.id.s_zapping_pip){
                if (i == 0) {
                    mZappingTime_Pip = 10000;
                } else if (i == 1) {
                    mZappingTime_Pip = 5000;
                } else if (i == 2) {
                    mZappingTime_Pip = 3000;
                } else if (i == 3) {
                    mZappingTime_Pip = 1000;
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {}
    };


    private void setPIPChange(boolean isShow){
        SLog.d(TAG, "setPIPChange - isShow : " + isShow + ", pip pos : " + mCurrentPosition_Pip);
        if(isShow == true) {
            if(mImagePause.getVisibility() == View.VISIBLE){
                playTimerStop(mTimer, mTask, 0);
            }
            //mChannelSpinner.setEnabled(false);
            //mLinearMain.setVisibility(View.GONE);

            isPIPShowing = true;
            if (mCurrentPosition_Pip == -1) {
                mCurrentPosition_Pip = 0;
            }
            if (mIPTVSurfaceView_Pip.getVisibility() != View.VISIBLE) {
                mIPTVSurfaceView_Pip.setVisibility(View.VISIBLE);

                //SLog.d(TAG, "============== setPIPChange - call setPIP. pos : " + pipMediaPlayer.getCurrentPosition() + " => " + mCurrentPosition_Pip);
                //if (pipMediaPlayer.getCurrentPosition() != mCurrentPosition_Pip)
                //setPIP(mCurrentPosition_Pip);
            } else {
                SLog.d(TAG, "============== setPIPChange2 - call setPIP. pos : " + pipMediaPlayer.getCurrentPosition() + " => " + mCurrentPosition_Pip);
                //if (pipMediaPlayer.getCurrentPosition() != mCurrentPosition_Pip)
                setPIP(mCurrentPosition_Pip);
            }

            mBtnPip.setText("PIP OFF");
//            showPipExceptionToast();
        } else {
            if(mImagePause_Pip.getVisibility() == View.VISIBLE){
                playTimerStop(mTimer_Pip, mTask_Pip,1);
            }
            mChannelSpinner.setEnabled(true);
            mLinearMain.setVisibility(View.VISIBLE);
            mChannelProgramInfo_Pip.setVisibility(View.INVISIBLE);  // Add dvbsi scenario - KGH

            isPIPShowing = false;
            mIPTVSurfaceView_Pip.setVisibility(View.INVISIBLE);
            if(pipMediaPlayer != null){
                pipMediaPlayer.reset();
                pipMediaPlayer = null;
            }
            mBtnPip.setText("PIP ON");

            String chInfo = "CH No. " + mChannelData.get(mCurrentPosition_Pip).getCh() + "   -  " + mChannelData.get(mCurrentPosition_Pip).getName();
            mChannelTitle_Pip.setText(chInfo);
            mainTVMediaPlayer.setVolume(1.0f);
        }
    }

    private void showMultiView(boolean isShow){
        showMultiView(isShow, mCurrentPosition);
    }
    private void showMultiView(boolean isShow, int pos){
        SLog.d(TAG, "showMultiView - isShow : " + isShow + ", pip pos : " + mCurrentPosition_Pip
                + ", backup pip pos : " + mMultiViewPosition_Pip + ", curr pos : " + mCurrentPosition);
        if(isShow == true){
            isMultiView = true;
            mMultiViewPosition_Pip = mCurrentPosition_Pip;
            mCurrentPosition_Pip = pos;//mCurrentPosition;
            mLinearMain.setVisibility(View.GONE);
            mBtnPip.setEnabled(false);
            stopZapping();
            setMultiTV();
            setPIPChange(true);
            setAudioFocus(0);
            mBtnMultiview.setText("Multi\nOFF");
        }else if(isShow == false){
            isMultiView = false;
            mCurrentPosition_Pip = mMultiViewPosition_Pip;
            mChannelSpinner_Pip.setSelection(mCurrentPosition_Pip);
//            mChannelSpinner.setSelection(mCurrentPosition);
            if(isPIPShowing == true){
                setPIPChange(false);
            }
            setTV(mCurrentPosition);
            setAudioFocus(1);
            mBtnMultiview.setText("Multi\nON");
            btnMultiviewProgram.setVisibility(View.GONE);
            mLinearMain.setVisibility(View.VISIBLE);
            mBtnPip.setEnabled(true);
        }
    }

    private void stopZapping(){
        SLog.d(TAG, "stopZapping");
        playTimerStop(mTimer, mTask, 0);
        playTimerStop(mTimer_Pip, mTask_Pip,1);
    }

    public void onClickTest(View view) {
        SLog.d(TAG, "onClickTest");
        //obd 임시 권한요청 나중에 시스템앱으로 넘어갈때 제거
        checkVerify();
//        Intent intent = new Intent(this, TestBtv.class);
//        startActivity(intent);
    }

    public void onClickCC(View view) {
        SLog.d(TAG, "onClickCC");
        Button btn = (Button)view;
//        TVSystem tvSystem = ServiceCoreAPI.getService(ServiceCoreAPI.TV_SYSTEM);
//        if(btn.getText().equals("CC On")) {
//            btn.setText("CC Off");
//            tvSystem.getSystem().setProperty(ITVSystem.PROPERTY_HEARING_IMPAIRED, "false");
//        } else {
//            btn.setText("CC On");
//            tvSystem.getSystem().setProperty(ITVSystem.PROPERTY_HEARING_IMPAIRED, "true");
//        }
    }

    public void onClickVisionCC(View view) {
        SLog.d(TAG, "onClickVisionCC");
        Button btn = (Button)view;
//        TVSystem tvSystem = ServiceCoreAPI.getService(ServiceCoreAPI.TV_SYSTEM);
//        if(btn.getText().equals("Vision On")) {
//            btn.setText("Vision Off");
//            tvSystem.getSystem().setProperty(ITVSystem.PROPERTY_VISION_IMPAIRED, "false");
//        } else {
//            btn.setText("Vision On");
//            tvSystem.getSystem().setProperty(ITVSystem.PROPERTY_VISION_IMPAIRED, "true");
//        }
    }

    public void onClickChangeEthernet(View view) {
        SLog.d(TAG, "onClickChangeEthernet");
        Button btn = (Button)view;
//        TVSystem tvSystem = ServiceCoreAPI.getService(ServiceCoreAPI.TV_SYSTEM);
//        NetworkMap networkMap =  tvSystem.getEthernet().getEthernetStatus();
//
//        if(networkMap.getType().equals("dhcp")) {
//            networkMap.setType("manual");
//            networkMap.setIp("192.168.0.221");
//            networkMap.setSubnet("255.255.255.0");
//            networkMap.setGateway("192.168.0.9");
//            networkMap.setDns("210.220.163.82", "219.250.36.130");
//        } else {
//            networkMap.setType("dhcp");
//        }
//
//        tvSystem.getEthernet().setEthernetValue(networkMap);
//        updateEthernetStatus(btn);
    }

    private void updateEthernetStatus(final Button btn) {
        SLog.d(TAG, "updateEthernetStatus");
        btn.postDelayed(new Runnable() {
            @Override
            public void run() {
//                TVSystem tvSystem = ServiceCoreAPI.getService(ServiceCoreAPI.TV_SYSTEM);
//                NetworkMap networkMap =  tvSystem.getEthernet().getEthernetStatus();
//                btn.setText(networkMap.toString());
            }
        }, 1000);
    }

    public void onChangeAudioFocus(View view) {
        SLog.d(TAG, "onChangeAudioFocus - currentAudioFocus : " + currentAudioFocus);
        currentAudioFocus = (currentAudioFocus +1) % 4;
        setAudioFocus(currentAudioFocus);
    }

    private void setAudioFocus(int index) {
        SLog.d(TAG, "setAudioFocus- index : " + index + ", currentAudioFocus : "
                + currentAudioFocus + ", is pip playing : " + ((pipMediaPlayer!=null)?pipMediaPlayer.isPlaying():"null")
                + ", is pip showing : " + isPIPShowing + ", pip player : " + pipMediaPlayer
                + ", is playing : " + ((mainTVMediaPlayer!=null)?mainTVMediaPlayer.isPlaying():"null"));
        if (isPIPShowing) {
            if (index == 0) {
                if (pipMediaPlayer != null) {
                    /*if (mainTVMediaPlayer != null) {
                        mainTVMediaPlayer.setVolume(0);
                    }
                    */pipMediaPlayer.selectTrack(0);
                    pipMediaPlayer.setVolume(1.0f);
                }
            } else {
                if (mainTVMediaPlayer != null) {// && mainTVMediaPlayer.isPlaying()) {
                    mainTVMediaPlayer.selectTrack(index - 1);
                    mainTVMediaPlayer.setVolume(1.0f);
                }
            }
        } else {
            if (mainTVMediaPlayer != null) {// && mainTVMediaPlayer.isPlaying()) {
                index = 0;
                mainTVMediaPlayer.selectTrack(0);
                mainTVMediaPlayer.setVolume(1.0f);
            }
        }
        /*switch (index) {
            case 0:
                if(pipMediaPlayer != null && pipMediaPlayer.isPlaying()) {
                    pipMediaPlayer.selectTrack(0);
                } else {
                    index = 1;
                    if(mainVodMediaPlayer != null && mainVodMediaPlayer.isPlaying()) {
                        mainVodMediaPlayer.selectTrack(currentAudioFocus - 1);
                    }
                }
                break;
            default:
                if(mainVodMediaPlayer != null && mainVodMediaPlayer.isPlaying()) {
                    mainVodMediaPlayer.selectTrack(currentAudioFocus - 1);
                }
                break;
        }*/

        currentAudioFocus = index;
        Button btn = (Button)findViewById(R.id.btn_change_audio_focus);
        btn.setText("F:" + currentAudioFocus);
    }

    private long time= 0;
    @Override
    public void onBackPressed() {
        SLog.d(TAG, "onBackPressed");
        if(System.currentTimeMillis()-time>=2000){
            time=System.currentTimeMillis();
            Toast.makeText(getApplicationContext(),"한번 더 누르면 종료",Toast.LENGTH_SHORT).show();
        }else if(System.currentTimeMillis()-time<2000){
            finish();
        }
    }

//    @TargetApi(Build.VERSION_CODES.M)
//    public void checkVerify() {
//        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
//                        checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//            // Should we show an explanation?
//            if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE))
//            {
//                // ...
//            }
//            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
//        }
//    }

    private static final int STORAGE = 1;

    @TargetApi(Build.VERSION_CODES.M)
    public void checkVerify() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(this, TestBtv.class);
            startActivity(intent);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode){

            case STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(this, TestBtv.class);
                    startActivity(intent);
                }
                break;
        }
    }
}
