package com.skb.framework.navigatorui;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skb.btv.framework.navigator.BtvNavigator;
import com.skb.btv.framework.navigator.dvbservice.AVDvbService;
import com.skb.btv.framework.navigator.program.AVProgram;
import com.skb.btv.framework.navigator.program.AVProgramList;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MultiChannelAdapter extends RecyclerView.Adapter<MultiChannelAdapter.MultiChannelViewHolder> {
    private final String TAG = "MultiChannelAdapter";
    private List<AVDvbService> mListData;
    private Context mContext;
    SimpleDateFormat mSimpleDateFormat;

    MultiChannelAdapter(Context context) {
        mContext = context;
        mListData = new ArrayList<>();
        mSimpleDateFormat = new SimpleDateFormat("HH:mm", Locale.KOREAN);
    }

    @NonNull
    @Override
    public MultiChannelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.listitem_multi_channel, parent, false);

        return new MultiChannelViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MultiChannelViewHolder holder, int position) {
        AVDvbService channel = mListData.get(position);
        Log.d(TAG, "onBindViewHolder() position: " + position);
        String chInfo = channel.getCh() + " " + channel.getName();
        holder.tvChannel.setText(chInfo);

        StringBuilder progInfo = new StringBuilder();
        int sid = channel.getSid();
        AVProgramList programList = BtvNavigator.getDvbSIService(mContext).getAVProgramList(sid);
        if (programList != null && programList.getAVProgramsList() != null && programList.getAVProgramsList().size() > 0) {
            Log.d(TAG, "onBindViewHolder() getAVProgramsList size: " + programList.getAVProgramsList().size());
            ArrayList<AVProgram> programs = programList.getAVProgramsList().get(0).getAVProgramList();
            if (programs != null) {
                Date starttime;
                Date endtime;
                String title;
                String startdate;
                String enddate;
                for(AVProgram program : programs) {
                    starttime = program.getStartDate();
                    endtime = program.getEndDate();
                    title = program.getName();
                    startdate = mSimpleDateFormat.format(starttime);
                    enddate = mSimpleDateFormat.format(endtime);

                    progInfo.append(title).append("(").append(startdate).append("~").append(enddate).append(") ");
                }
            }
        }
        holder.tvPrograms.setText(progInfo.toString());
    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    public void setList(List<AVDvbService> list) {
        mListData = list;
    }

    public static class MultiChannelViewHolder extends RecyclerView.ViewHolder {
        TextView tvChannel;
        TextView tvPrograms;

        public MultiChannelViewHolder(@NonNull View itemView) {
            super(itemView);

            tvChannel = itemView.findViewById(R.id.tv_channel);
            tvPrograms = itemView.findViewById(R.id.tv_programs);
        }
    }

}
