/*
* Copyright (C) 2013 SK Telecom 
*              ARAON Augmented Reality Platform
*              Smart Device Lab. Network R&D Institute. 
*              SK Telecom All Rights Reserved. 
*
* NOTICE: All information contained herein is,
* and remains the proprietary of SK Telecom and its suppliers, if any.
* The Intellectual and technical concepts contained herein are
* proprietary to SK Telecom and its supplier and may be covered by 
* South Korea and Foreign patents, patents in process, and are protected
* by trade secret or copyright laws.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from SK Telecom.
*
*/

package com.skb.framework.navigatorui;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.util.TypedValue;

import com.skb.btv.framework.navigator.dvbservice.AVDvbService;
import com.skb.btv.framework.navigator.dvbservice.AVDvbServiceList;
import com.skb.btv.framework.navigator.dvbservice.MultiViewDvbService;
import com.skb.btv.framework.navigator.dvbservice.MultiViewDvbServiceList;
import com.skb.btv.framework.navigator.dvbservice.MusicDvbService;
import com.skb.btv.framework.navigator.dvbservice.MusicDvbServiceList;

import java.util.ArrayList;

public class Utils {

    private static final String TAG =  "Utils";

    public static int getDpToPixel(Context context, int DP) {
        Log.d(TAG, "getDpToPixel");
        float px = 0;
        try {
            px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DP,
                    context.getResources().getDisplayMetrics());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (int) px;
    }

    public static String getAppVersionName(Context context){
        Log.d(TAG, "getAppVersionName");
        String version = "";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }


//    public static ArrayList<ChannelInfo> channelParser(Context context) {
//        ArrayList<ChannelInfo> arrayList = new ArrayList<ChannelInfo>();
//        InputStream is = context.getResources().openRawResource(R.raw.channels);
//
////        File file = new File("/data/btv_home/dvbsi/channel.xml");
////        try {
////            is = new FileInputStream(file);
////        } catch (FileNotFoundException e) {
////            e.printStackTrace();
////            return arrayList;
////        }
//
//        // xmlPullParser
//        try {
//            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
//            XmlPullParser parser = factory.newPullParser();
//            parser.setInput(new InputStreamReader(is, "UTF-8"));
//            int eventType = parser.getEventType();
//            ChannelInfo channel = null;
//
//            while (eventType != XmlPullParser.END_DOCUMENT) {
//                switch (eventType) {
//                    case XmlPullParser.START_TAG:
//                        String startTag = parser.getName();
//
//                        if (startTag.equals("ListVersion")) {
//                            parser.getAttributeValue(null, "");
//                        }
//
//                        if (startTag.equals("Channel")) {
//                            channel = new ChannelInfo();
//                            channel.setCh(Integer.parseInt(parser.getAttributeValue(null, "ch").toString()));
//                            channel.setSid(Integer.parseInt(parser.getAttributeValue(null, "sid").toString()));
//                            channel.setCname(parser.getAttributeValue(null, "cname"));
//                            channel.setIp(parser.getAttributeValue(null, "ip"));
//                            channel.setPort(Integer.parseInt(parser.getAttributeValue(null, "port").toString()));
//                            channel.setVpid(Integer.parseInt(parser.getAttributeValue(null, "vpid").toString()));
//                            channel.setApid(Integer.parseInt(parser.getAttributeValue(null, "apid").toString()));
//                            channel.setApid2(Integer.parseInt(parser.getAttributeValue(null, "apid2").toString()));
//                            channel.setPpid(Integer.parseInt(parser.getAttributeValue(null, "ppid").toString()));
//                            channel.setVst(Integer.parseInt(parser.getAttributeValue(null, "vst").toString()));
//                            channel.setAst(Integer.parseInt(parser.getAttributeValue(null, "ast").toString()));
//                            channel.setRes(Integer.parseInt(parser.getAttributeValue(null, "res").toString()));
//                            channel.setPay(Integer.parseInt(parser.getAttributeValue(null, "pay").toString()));
//                            channel.setSample(Integer.parseInt(parser.getAttributeValue(null, "sample").toString()));
//                            channel.setRating(Integer.parseInt(parser.getAttributeValue(null, "rating").toString()));
//                            channel.setImage(parser.getAttributeValue(null, "image"));
//                            channel.setProduct(parser.getAttributeValue(null, "product"));
//                            channel.setCa_id(Integer.parseInt(parser.getAttributeValue(null, "ca_id").toString()));
//                            channel.setCa_pid(Integer.parseInt(parser.getAttributeValue(null, "ca_pid").toString()));
//                            channel.setLocalAreaCode(Integer.parseInt(parser.getAttributeValue(null, "localAreaCode").toString()));
//                        }
//
//                        break;
//                    case XmlPullParser.END_TAG:
//                        String endTag = parser.getName();
//                        if (endTag.equals("Channel")) {
//                            arrayList.add(channel);
//                        }
//                        break;
//                }
//                eventType = parser.next();
//            }
//
//        } catch (XmlPullParserException e) {
//            e.printStackTrace();
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return arrayList;
//    }


    public static ArrayList<AVDvbService> channelParser(Context context, AVDvbServiceList result) {
        Log.d(TAG, "channelParser");
        return new ArrayList<>(result.getList());
    }

    /**
     * Start Add dvbsi scenario - KGH
     */

    public static ArrayList<MusicDvbService> musicChannelParser(Context context, MusicDvbServiceList data) {
        Log.d(TAG, "musicChannelParser");
        return new ArrayList<>(data.getList());
    }

    public static ArrayList<MultiViewDvbService> multiViewChannelParser(Context context, MultiViewDvbServiceList data){
        Log.d(TAG, "multiViewChannelParser");
        return new ArrayList<>(data.getList());
    }

    /**
     * End KGH
     */

}
