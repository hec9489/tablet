package com.skb.framework.navigatorui;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.graphics.SurfaceTexture;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.skb.btv.framework.navigator.BtvNavigator;
import com.skb.btv.framework.navigator.IBtvNavigator;
import com.skb.btv.framework.navigator.OnDsmccListener;
import com.skb.btv.framework.navigator.OnDvbSIListener;
import com.skb.btv.framework.navigator.dvbservice.AVDvbService;
import com.skb.btv.framework.navigator.dvbservice.AVDvbServiceList;
//import com.skb.btv.framework.navigator.dvbservice.IAVDvbService;
import com.skb.btv.framework.navigator.dvbservice.MultiViewLogicalCell;
import com.skb.btv.framework.navigator.dvbservice.MultiViewDvbService;
import com.skb.btv.framework.navigator.dvbservice.MultiViewDvbServiceList;
import com.skb.btv.framework.navigator.dvbservice.MusicDvbService;
import com.skb.btv.framework.navigator.dvbservice.MusicDvbServiceList;
import com.skb.btv.framework.navigator.product.ProductList;
import com.skb.btv.framework.navigator.program.AVProgram;
import com.skb.btv.framework.navigator.program.AVProgramList;
import com.skb.btv.framework.navigator.program.AVPrograms;
import com.skb.btv.framework.navigator.program.MusicProgram;
import com.skb.btv.framework.navigator.DvbSIService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "MainActivity";

    private Context mContext;

    private TextView mVersionInfo;
    private List<String> mZappingTimeData;
    private List<AVDvbService> mChannelData;
    private Spinner mChannelSpinner;
    private int mCurrentPosition = 0;
    private TextView mChannelTitle;

    private Timer mTimer = null;
    private TimerTask mTask = null;
    private boolean isTimerAlive = false;
    private Spinner mZappingSpinner;
    private int mZappingTime = 10000;

    private ImageView mImagePlay;
    private ImageView mImagePause;

//    private List<AVDvbService> mChannelData_Pip;
//    private Spinner mChannelSpinner_Pip;

//    private int mCurrentPosition_Pip = 0;
//    private int mMultiViewPosition_Pip = 0;
//    private TextView mChannelTitle_Pip;

//    private Timer mTimer_Pip = null;
//    private TimerTask mTask_Pip = null;
//    private boolean isTimerAlive_Pip = false;

//    private Spinner mZappingSpinner_Pip;
//    private int mZappingTime_Pip = 10000;

//    private Button mBtnPip;
//    private ImageView mImagePlay_Pip;
//    private ImageView mImagePause_Pip;
//    private ImageView mImageWatermark;

    private boolean isFirstStart = true;
//    private boolean isPIPShowing = false;
//    private boolean isMultiView = false;

//    private Button mBtnMultiview;

    /**
     * Start Add dvbsi scenario - KGH
     */
    private ChannelAdapter channelAdapter;
//    private ChannelAdapter channelAdapter_Pip;
    private List<MusicDvbService> mMusicChannelData;
    private Spinner mMusicChannelSpinner;
    private MusicChannelAdapter musicChannelAdapter;
    private int mMusicChannelCurrentPosition = 0;
    private int currentChannelSid;
//    private int currentChannelSid_Pip;
    private List<AVProgram> mProgramList = new ArrayList<AVProgram>();
//    private List<AVProgram> mProgramList_Pip = new ArrayList<AVProgram>();
    private Handler mHandler = new Handler();
    private int musichChannelAudioPid = 0;

    private List<MusicProgram> mMusicProgramList = new ArrayList<MusicProgram>();
    private Spinner mMusicChannelProgramInfo;
    private MusicProgramAdapter musicProgramAdapter;

//    private List<MultiViewDvbService> mMultiViewChannelData = new ArrayList<>();
//    private String mMultiViewURI;
    private int currentRunningProgramIndex = 0, currentMusicRunningProgramIndex = 0;
//    private int currentRunningProgramIndex_Pip = 0;

//    private Button btnMultiviewProgram;
//    private String multiViewChannel;
    /**
     * End - KGH
     */

    private Spinner mChannelProgramInfo;
//    private Spinner mChannelProgramInfo_Pip;
    private ProgramAdapter programAdapter;
    private ProgramAdapter programAdapter_Pip;

    private TextView mTvDsmccEvent;
    private RecyclerView mRvMultiChannel;
    private MultiChannelAdapter mMultiChannelAdapter;

    //private DvbServiceManager siManager;
    private DvbSIService mDvbSIService;
    private boolean mIsUseLocalInstance = false;

    private int mChannel;
    private TextView mTvCh;
    private Runnable mRunSetCh = new Runnable() {
        @Override
        public void run() {
            Log.d("channel", "set channel : " + mChannel);
            if (mChannel <= 0) {
                mChannel = 0;
                if (mTvCh != null) {
                    mTvCh.setText("");
                }
                return;
            }

            int idx = -1;
            for (int i = 0; i < mChannelData.size(); i++) {
                AVDvbService channel = mChannelData.get(i);
                if (channel.getCh() == mChannel) {
                    idx = i;
                    break;
                }
            }

            if (idx != -1) {
                Log.d("channel", "Ch idx: " + idx);
                /*if (isPIPShowing) {
                    if (idx != mChannelSpinner_Pip.getSelectedItemPosition()) {
                        Log.d(TAG, "============== RunSetCh - call setPIP");
                        setPIP(idx);
                    }
                } else*/ {
                    if (idx != mChannelSpinner.getSelectedItemPosition()) {
                        if (mChannel == 311) {
                            mCurrentPosition = idx;
                            setMusicTV(mMusicChannelCurrentPosition);
                        } else {
                            setTV(idx);
                        }
                    }
                }
            }

            if (mTvCh != null) {
                mTvCh.setText("");
            }
            mChannel = 0;
        }
    };

    /**
     * Start Add dvbsi scenario - KGH
     */

    public OnDsmccListener onDsmccListener = new OnDsmccListener() {
        @Override
        public void onDsmccEvent(String path, com.skb.btv.framework.navigator.SignalEvent sigevent) {
            Log.d(TAG, "onDsmccEvent() path:" + path + ", event : " + sigevent);
            File dir = new File(path);
            String[] files = dir.list();
            StringBuffer info = new StringBuffer();

            info.append("Path: ").append(path)
                .append("\nEventType: ").append(sigevent.getmEventType())
                .append("\nChannelNum: ").append(sigevent.getmChannelNum());

            if (files.length > 0) {
                info.append("\nFiles:");
                for (String file : files) {
                    info.append(" ").append(file);
                    Log.d(TAG, "onDsmccEvent() file:" + file);
                }
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTvDsmccEvent.setText(info);
                }
            });
        }
    };

    public OnDvbSIListener onDvbSIListener = new OnDvbSIListener() {
        @Override
        public void onDvbSIUpdated(int dvbsiType, final String extraData) {
            Log.d(TAG, "onDvbSIUpdated() dvbsiType : " + dvbsiType + ", extra : " + extraData);
            Log.d(TAG, "onDvbSIUpdated() getAVDvbServiceList : " + BtvNavigator.getDvbSIService(mContext).getAVDvbServiceList());
            if (dvbsiType == IBtvNavigator.DVBSI_TYPE_UPDATE_CHANNEL) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String channelListVersion = "";
                        String extraString = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            extraString = json.getString(IBtvNavigator.DVBSI_JSONOBJECT_INFO);
                            channelListVersion = json.getString(IBtvNavigator.DVBSI_JSONOBJECT_DVBSERVICEVERSION);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("KGH", "version : " + channelListVersion);
                        AVDvbServiceList channelList = null;
                        if (mIsUseLocalInstance) {
                            if (mDvbSIService != null) {
                                channelList = mDvbSIService.getAVDvbServiceList();
                            }
                        } else {
                            channelList = BtvNavigator.getDvbSIService(mContext).getAVDvbServiceList();
                        }
                        if (channelList != null && channelList.isReady()) {
                            mChannelData = Utils.channelParser(MainActivity.this, channelList);
                            for (int i = 0; i < mChannelData.size(); i++) {
                                Log.i(TAG, "onDvbSIUpdated() ch - " + mChannelData.get(i).getCh());
                            }
                            channelAdapter.setList(mChannelData);
                            channelAdapter.notifyDataSetChanged();
                            mChannelSpinner.setSelection(mCurrentPosition);
                            // pip
//                            mChannelData_Pip = mChannelData;
//                            channelAdapter_Pip.setList(mChannelData_Pip);
//                            channelAdapter_Pip.notifyDataSetChanged();
                        }
                    }
                });
            } else if (dvbsiType == IBtvNavigator.DVBSI_TYPE_UPDATE_PROGRAMS) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String extraString = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            extraString = json.getString(IBtvNavigator.DVBSI_JSONOBJECT_INFO);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        AVProgramList program_result = null;
                        if (mIsUseLocalInstance) {
                            if (mDvbSIService != null) {
                                program_result = mDvbSIService.getAVProgramList(currentChannelSid);
                            }
                        } else {
                            program_result = BtvNavigator.getDvbSIService(mContext).getAVProgramList(currentChannelSid);
                        }
                        Log.d("onDvbSIUpdated()", "program_result : " + program_result);
                        if (program_result != null && program_result.getAVProgramsList() != null && program_result.getAVProgramsList().size() > 0) {
                            if (mProgramList != null) {
                                mProgramList.clear();
                                mProgramList.addAll(program_result.getAVProgramsList().get(0).getAVProgramList());
                                Log.d("onDvbSIUpdated()", "mProgramList size: " + mProgramList.size());
                                programAdapter.setList(mProgramList);
                                programAdapter.notifyDataSetChanged();
                                currentRunningProgramIndex = getCurrentRunningProgram();
                                mChannelProgramInfo.setSelection(currentRunningProgramIndex);
                                mMultiChannelAdapter.notifyDataSetChanged();
                            }
                        }
//                        if (isPIPShowing) {
//                            AVProgramList programPip_result = null;
//                            if (mIsUseLocalInstance) {
//                                if (mDvbSIService != null) {
//                                    programPip_result = mDvbSIService.getAVProgramList(currentChannelSid_Pip);
//                                }
//                            } else {
//                                programPip_result = BtvNavigator.getDvbSIService(mContext).getAVProgramList(currentChannelSid_Pip);
//                            }
//                            if (programPip_result != null && programPip_result.getAVProgramsList() != null && programPip_result.getAVProgramsList().size() > 0) {
//                                if (mProgramList_Pip != null) {
//                                    mProgramList_Pip.clear();
//                                    mProgramList_Pip.addAll(programPip_result.getAVProgramsList().get(0).getAVProgramList());
//                                    programAdapter_Pip.setList(mProgramList_Pip);
//                                    programAdapter_Pip.notifyDataSetChanged();
//                                    currentRunningProgramIndex_Pip = getCurrentRunningProgram_Pip();
//                                    mChannelProgramInfo_Pip.setSelection(currentRunningProgramIndex_Pip);
//                                }
//                            }
//                        }
                    }
                });
            } else if (dvbsiType == IBtvNavigator.DVBSI_TYPE_UPDATE_MUSIC_CHANNEL) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String extraString = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            extraString = json.getString(IBtvNavigator.DVBSI_JSONOBJECT_INFO);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        MusicDvbServiceList channelList = null;
                        if (mIsUseLocalInstance) {
                            if (mDvbSIService != null) {
                                channelList = mDvbSIService.getMusicDvbServiceList();
                            }
                        } else {
                            channelList = BtvNavigator.getDvbSIService(mContext).getMusicDvbServiceList();
                        }
                        if (channelList != null && channelList.isReady()) {
                            mMusicChannelData = Utils.musicChannelParser(MainActivity.this, channelList);
                            musicChannelAdapter.setList(mMusicChannelData);
                            musicChannelAdapter.notifyDataSetChanged();
                            mMusicChannelSpinner.setSelection(mMusicChannelCurrentPosition);
                        }
                    }
                });
            } else if (dvbsiType == IBtvNavigator.DVBSI_TYPE_UPDATE_MUSIC_PROGRAMS) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int audioid = -1;
                        try {
                            JSONObject json = new JSONObject(extraData);
                            audioid = json.getInt(IBtvNavigator.DVBSI_JSONOBJECT_AUDIOPID);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        List<MusicProgram> musicprogram_result = null;
                        if (mIsUseLocalInstance) {
                            if (mDvbSIService != null) {
                                musicprogram_result = mDvbSIService.getMusicProgramList(audioid).getMusicProgramList();
                            }
                        } else {
                            musicprogram_result = BtvNavigator.getDvbSIService(mContext).getMusicProgramList(audioid).getMusicProgramList();
                        }
                        if (musicprogram_result != null && musicprogram_result.size() > 0) {
                            if (mMusicProgramList != null) {
                                mMusicProgramList.clear();
                                for (MusicProgram entity : musicprogram_result) {
                                    Log.i("onDvbSIUpdated()", "mMusicProgramList: " + entity.getName());
                                }
                                mMusicProgramList.addAll(musicprogram_result);
                                musicProgramAdapter.setList(mMusicProgramList);
                                musicProgramAdapter.notifyDataSetChanged();
                            }
                            currentMusicRunningProgramIndex = getCurrentRunningMusicProgram();
                            mMusicChannelProgramInfo.setSelection(currentMusicRunningProgramIndex);
                            long gap = mMusicProgramList.get(currentMusicRunningProgramIndex).getEndDate().getTime() - System.currentTimeMillis();
                            mHandler.removeCallbacks(updateMusicProgramRunnable);
                            mHandler.postDelayed(updateMusicProgramRunnable, gap + 1000);
                        }
                    }
                });

            } else if (dvbsiType == IBtvNavigator.DVBSI_TYPE_UPDATE_MULTI_CHANNELS) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String extraString = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            extraString = json.getString(IBtvNavigator.DVBSI_JSONOBJECT_INFO);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        MultiViewDvbServiceList multiViewChannelList = null;
                        if (mIsUseLocalInstance) {
                            if (mDvbSIService != null) {
                                multiViewChannelList = mDvbSIService.getMultiViewDvbServiceList();
                            }
                        } else {
                            multiViewChannelList = BtvNavigator.getDvbSIService(mContext).getMultiViewDvbServiceList();
                        }
                        if (multiViewChannelList != null && multiViewChannelList.isReady()) {
//                            mMultiViewChannelData = Utils.multiViewChannelParser(MainActivity.this, multiViewChannelList);
                        }
                    }
                });
            } else if (dvbsiType == IBtvNavigator.DVBSI_TYPE_CHANGE_PROGRAM) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int sid = -1;
                        try {
                            JSONObject json = new JSONObject(extraData);
                            sid = json.getInt(IBtvNavigator.DVBSI_JSONOBJECT_SID_NUM);  //IBtvFramework.DVBSI_JSONOBJECT_CHANNEL_NUM);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        AVProgramList program_result = null;
                        if (mIsUseLocalInstance) {
                            if (mDvbSIService != null) {
                                program_result = mDvbSIService.getAVProgramList(sid);
                            }
                        } else {
                            program_result = BtvNavigator.getDvbSIService(mContext).getAVProgramList(sid);
                        }
                        if (mProgramList != null) {
                            mProgramList.clear();
                        }

                        if (program_result != null && program_result.getAVProgramsList() != null && program_result.getAVProgramsList().size() > 0) {
                            mProgramList.addAll(program_result.getAVProgramsList().get(0).getAVProgramList());
                            programAdapter.setList(mProgramList);
                            programAdapter.notifyDataSetChanged();
                            currentRunningProgramIndex = getCurrentRunningProgram();
                            mChannelProgramInfo.setSelection(currentRunningProgramIndex);
                            mMultiChannelAdapter.notifyDataSetChanged();
                        }
                    }
                });
            } else if (dvbsiType == IBtvNavigator.DVBSI_TYPE_CHANGE_PROGRAMS) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String sidNumbers = "";
                        try {
                            JSONObject json = new JSONObject(extraData);
                            sidNumbers = json.getString(IBtvNavigator.DVBSI_JSONOBJECT_SID_NUM);    //DVBSI_JSONOBJECT_CHANNEL_NUM);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (sidNumbers.isEmpty()) {
                            ProductList product_result = null;
                            if (mIsUseLocalInstance) {
                                if (mDvbSIService != null) {
                                    product_result = mDvbSIService.getProductList();
                                }
                            } else {
                                product_result = BtvNavigator.getDvbSIService(mContext).getProductList();
                            }
                            if (product_result != null && product_result.getProductList() != null) {
                                if (product_result.getProductList().size() > 0) {
                                    Log.d(TAG, "Product Count : " + product_result.getProductList().size());
                                }
                            }
                        } else {
                            AVProgramList program_result = null;
                            if (mIsUseLocalInstance) {
                                if (mDvbSIService != null) {
                                    program_result = mDvbSIService.getMultiAVProgramList(sidNumbers);
                                }
                            } else {
                                program_result = BtvNavigator.getDvbSIService(mContext).getMultiAVProgramList(sidNumbers);
                            }
                            if (program_result != null && program_result.getAVProgramsList() != null && program_result.getAVProgramsList().size() > 0) {
                                Log.d(TAG, "Program change channel Count : " + program_result.getAVProgramsList().size());
                            }
                        }
                    }
                });
            }

            if (mChannelData == null) {
                AVDvbServiceList channelList = null;
                if (mIsUseLocalInstance) {
                    if (mDvbSIService != null) {
                        channelList = mDvbSIService.getAVDvbServiceList();
                    }
                } else {
                    channelList = BtvNavigator.getDvbSIService(mContext).getAVDvbServiceList();
                }

                if (channelList != null && channelList.isReady()) {
                    mChannelData = Utils.channelParser(MainActivity.this, channelList);
//                    mChannelData_Pip = mChannelData;
                    Log.d(TAG, "onDvbSIUpdated() mChannelData size: " + mChannelData.size());
                    for(AVDvbService service : mChannelData) {
                        Log.i(TAG, "onDvbSIUpdated() - mChannelData - " + service.getCh() + "[" + service.getSid() + "] : " + service.getName());
                    }
                    if (channelAdapter != null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                channelAdapter.setList(mChannelData);
                                channelAdapter.notifyDataSetChanged();

//                                channelAdapter_Pip.setList(mChannelData_Pip);
//                                channelAdapter_Pip.notifyDataSetChanged();
                                /*if (isPIPShowing) {
                                    setPIP(mCurrentPosition_Pip);
                                } else */{
                                    setTV(mCurrentPosition);
                                }
                                mMultiChannelAdapter.setList(mChannelData);
                                mMultiChannelAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                }
            }

            if (mMusicChannelData == null) {
                MusicDvbServiceList musicChannelList = null;
                if (mIsUseLocalInstance) {
                    if (mDvbSIService != null) {
                        musicChannelList = mDvbSIService.getMusicDvbServiceList();
                    }
                } else {
                    musicChannelList = BtvNavigator.getDvbSIService(mContext).getMusicDvbServiceList();
                }
                Log.d(TAG, "onDvbSIUpdated() musicChannelList : " + musicChannelList);
                if (musicChannelList != null && musicChannelList.isReady()) {
                    mMusicChannelData = Utils.musicChannelParser(MainActivity.this, musicChannelList);
                    musicChannelAdapter.setList(mMusicChannelData);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            musicChannelAdapter.notifyDataSetChanged();
                            if (mChannelData != null && mChannelData.get(mCurrentPosition).getCh() == 311) {
                                setMusicTV(mMusicChannelCurrentPosition);
                            }
                            for (int i = 0; i < mMusicChannelData.size(); i++) {
                                Log.i(TAG, "onDvbSIUpdated() - mMusicChannelData [" + i + "] : " + mMusicChannelData.get(i).getCh_no() + ", " + mMusicChannelData.get(i).getTitle());
                            }
                        }
                    });
                }
            }
        }
    };

    private Runnable updateMusicProgramRunnable = new Runnable() {
        @Override
        public void run() {
            if (musicProgramAdapter != null) {
                List<MusicProgram> musicprogram_result = null;
                if (mIsUseLocalInstance) {
                    if (mDvbSIService != null) {
                        musicprogram_result = mDvbSIService.getMusicProgramList(musichChannelAudioPid).getMusicProgramList();
                    }
                } else {
                    musicprogram_result = BtvNavigator.getDvbSIService(mContext).getMusicProgramList(musichChannelAudioPid).getMusicProgramList();
                }
                if (musicprogram_result != null && musicprogram_result.size() > 0) {
                    mMusicProgramList.addAll(musicprogram_result);
                    musicProgramAdapter.setList(mMusicProgramList);
                    musicProgramAdapter.notifyDataSetChanged();
                    currentMusicRunningProgramIndex = getCurrentRunningMusicProgram();
                    mMusicChannelProgramInfo.setSelection(currentMusicRunningProgramIndex);
                    long gap = mMusicProgramList.get(currentMusicRunningProgramIndex).getEndDate().getTime() - System.currentTimeMillis();
                    mHandler.removeCallbacks(updateMusicProgramRunnable);
                    mHandler.postDelayed(updateMusicProgramRunnable, gap + 1000);
                }
            }
        }
    };

    AdapterView.OnItemSelectedListener onMusicChannelItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
            if (isFirstStart) {
                isFirstStart = false;
                return;
            }

            mMusicChannelCurrentPosition = i;
            setMusicTV(i);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    /**
     * End - KGH
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate mContext: " + mContext);

        setContentView(R.layout.activity_player_iptv);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mContext = this;
        mDvbSIService = BtvNavigator.getDvbSIService(mContext);
        mDvbSIService.setOnDvbSIListener(onDvbSIListener);
        mDvbSIService.setOnDsmccListener(onDsmccListener);

        AVDvbServiceList channelList = null;
        MusicDvbServiceList musicChannelList = null;
        MultiViewDvbServiceList multiViewChannelList = null;
        if (mIsUseLocalInstance) {
            if (mDvbSIService != null) {
                channelList = mDvbSIService.getAVDvbServiceList();
                musicChannelList = mDvbSIService.getMusicDvbServiceList();
                multiViewChannelList = mDvbSIService.getMultiViewDvbServiceList();
            }
        } else {
            channelList = BtvNavigator.getDvbSIService(mContext).getAVDvbServiceList();
            musicChannelList = BtvNavigator.getDvbSIService(mContext).getMusicDvbServiceList();
            multiViewChannelList = BtvNavigator.getDvbSIService(mContext).getMultiViewDvbServiceList();
        }
        Log.i(TAG, "onCreate - mDvbSIService: " + mDvbSIService + " channelList: " + channelList);
        if (channelList != null && channelList.isReady()) {
            mChannelData = Utils.channelParser(MainActivity.this, channelList);
//            mChannelData_Pip = mChannelData;
            for (int i = 0; i < mChannelData.size(); i++) {
                Log.i(TAG, "onCreate - mChannelData - av [" + i + "] : " + mChannelData.get(i).getCh() + ", " + mChannelData.get(i).getName());
            }
        }
        Log.i(TAG, "onCreate - music channelList ready : " + ((musicChannelList != null) ? musicChannelList.isReady() : "null"));
        if (musicChannelList != null && musicChannelList.isReady()) {
            mMusicChannelData = Utils.musicChannelParser(MainActivity.this, musicChannelList);
            for (int i = 0; i < mMusicChannelData.size(); i++) {
                Log.i(TAG, "onCreate - music [" + i + "] : " + mMusicChannelData.get(i).getCh_no() + ", " + mMusicChannelData.get(i).getTitle());
            }
        }
        Log.i(TAG, "onCreate - multi channelList ready : " + ((multiViewChannelList != null) ? multiViewChannelList.isReady() : "null"));
//        if (multiViewChannelList != null && multiViewChannelList.isReady()) {
//            mMultiViewChannelData = Utils.multiViewChannelParser(MainActivity.this, multiViewChannelList);
//            for (int i = 0; i < mMultiViewChannelData.size(); i++) {
//                Log.i(TAG, "onCreate - multiview [" + i + "] : " + mMultiViewChannelData.get(i).getGroupID() + ", " + mMultiViewChannelData.get(i).getGroupName());
//            }
//        }

        mainUi();

    }

    public void mainUi() {
        Log.d(TAG, "mainUi");

        mTvCh = findViewById(R.id.tv_channel);
        mVersionInfo = (TextView) findViewById(R.id.tv_version);

//        SIService si = ServiceCoreAPI.getService(ServiceCoreAPI.SI_SERVICE);
//        si.connect(MainActivity.this);
//        RetMultiviewChannelList list = si.getMultiviewChannelList();

//        mDvbSIManager.setManagerReadyListener(new ManagerReadyStatusListener() {
//            @Override
//            public void onManagerReady(Object object, int status) {
//                if(status == BtvFramework.MANAGER_READY_STATUS){
//                    loadChannels();
//                }
//            }
//        });
        setZappingTime();

        // Main Channel
        channelAdapter = new ChannelAdapter(this);
        if (mChannelData != null) {
            Log.e(TAG, "mainUi - channel count : " + mChannelData.size());
            /*for (int i=0; i<mChannelData.size(); i++) {
                Log.e("channel list", "mainUi - channelList  [" + i + "] : "+ mChannelData.get(i).getCh() + ", " + mChannelData.get(i).getName());
            }*/
            channelAdapter.setList(mChannelData);
        }

        mChannelSpinner = (Spinner) findViewById(R.id.s_channel);
        mChannelSpinner.setAdapter(channelAdapter);
        mChannelSpinner.setOnItemSelectedListener(onChannelItemSelectedListener);

        mZappingSpinner = (Spinner) findViewById(R.id.s_zapping);
        mZappingSpinner.setAdapter(new ArrayAdapter<String>(this, R.layout.listitem_zapping, mZappingTimeData));
        mZappingSpinner.setOnItemSelectedListener(onZappingItemSelectedListener);

        mChannelTitle = (TextView) findViewById(R.id.tv_title_bottom);

        // PIP Channel
//        channelAdapter_Pip = new ChannelAdapter(this);
//        if (mChannelData_Pip != null) {
//            channelAdapter_Pip.setList(mChannelData_Pip);
//        }
//        mChannelSpinner_Pip = (Spinner) findViewById(R.id.s_channel_pip);
//        mChannelSpinner_Pip.setAdapter(channelAdapter_Pip);
//        mChannelSpinner_Pip.setOnItemSelectedListener(onChannelItemSelectedListener);
//
//        mZappingSpinner_Pip = (Spinner) findViewById(R.id.s_zapping_pip);
//        mZappingSpinner_Pip.setAdapter(new ArrayAdapter<String>(this, R.layout.listitem_zapping, mZappingTimeData));
//        mZappingSpinner_Pip.setOnItemSelectedListener(onZappingItemSelectedListener);
//
//        mChannelTitle_Pip = (TextView) findViewById(R.id.tv_title_bottom_pip);

        mImagePlay = (ImageView) findViewById(R.id.iv_play);
        mImagePause = (ImageView) findViewById(R.id.iv_pause);
        mImagePlay.setOnClickListener(this);
        mImagePause.setOnClickListener(this);

//        mImagePlay_Pip = (ImageView) findViewById(R.id.iv_play_pip);
//        mImagePause_Pip = (ImageView) findViewById(R.id.iv_pause_pip);
//        mImagePlay_Pip.setOnClickListener(this);
//        mImagePause_Pip.setOnClickListener(this);
//        mBtnPip = (Button) findViewById(R.id.btn_pip);
//        mBtnPip.setOnClickListener(this);
//
//        mBtnMultiview = (Button) findViewById(R.id.btn_multiview);
//        mBtnMultiview.setOnClickListener(this);

        findViewById(R.id.iv_up).setOnClickListener(this);
        findViewById(R.id.iv_down).setOnClickListener(this);
//        findViewById(R.id.iv_up_pip).setOnClickListener(this);
//        findViewById(R.id.iv_down_pip).setOnClickListener(this);

        setVersion();

        mChannelProgramInfo = (Spinner) findViewById(R.id.s_programInfo);
        mChannelProgramInfo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                parent.setSelection(currentRunningProgramIndex);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        programAdapter = new ProgramAdapter(MainActivity.this);
        mChannelProgramInfo.setAdapter(programAdapter);

//        mChannelProgramInfo_Pip = (Spinner) findViewById(R.id.s_programInfo_pip);
//        programAdapter_Pip = new ProgramAdapter(MainActivity.this);
//        mChannelProgramInfo_Pip.setAdapter(programAdapter_Pip);

        /**
         * Start Add dvbsi scenario - KGH
         */
//        mChannelProgramInfo_Pip.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                parent.setSelection(currentRunningProgramIndex_Pip);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

        musicChannelAdapter = new MusicChannelAdapter(this);
        musicChannelAdapter.setList(mMusicChannelData);
        mMusicChannelSpinner = (Spinner) findViewById(R.id.m_channel);
        mMusicChannelSpinner.setOnItemSelectedListener(onMusicChannelItemSelectedListener);
        mMusicChannelSpinner.setAdapter(musicChannelAdapter);
        mMusicChannelProgramInfo = (Spinner) findViewById(R.id.s_musicprogramInfo);
        musicProgramAdapter = new MusicProgramAdapter(MainActivity.this);
        mMusicChannelProgramInfo.setAdapter(musicProgramAdapter);
        mMusicChannelProgramInfo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                parent.setSelection(currentMusicRunningProgramIndex);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        btnMultiviewProgram = (Button) findViewById(R.id.btn_multiviewprogram);
//        btnMultiviewProgram.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AVProgramList program_result = null;
//                if (mIsUseLocalInstance) {
//                    if (mDvbSIService != null) {
//                        program_result = mDvbSIService.getMultiAVProgramList(multiViewChannel);
//                    }
//                } else {
//                    program_result = BtvNavigator.getDvbSIService(mContext).getMultiAVProgramList(multiViewChannel);
//                }
//                if (program_result != null && program_result.getAVProgramsList() != null && program_result.getAVProgramsList().size() > 0) {
//                    ArrayList<AVPrograms> items = new ArrayList<>(program_result.getAVProgramsList());
//                }
//            }
//        });
        /**
         * End - KGH
         */

//        btnMultiviewProgram = (Button) findViewById(R.id.btn_multiviewprogram);
        // End KGH

        // DSMCC
        mTvDsmccEvent = findViewById(R.id.tv_dsmcc_event);
        // Multi Channel
        mRvMultiChannel = findViewById(R.id.rv_multi_channel);
        mRvMultiChannel.setLayoutManager(new LinearLayoutManager(this));
        mRvMultiChannel.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        mMultiChannelAdapter = new MultiChannelAdapter(this);
        if(mChannelData != null) {
            mMultiChannelAdapter.setList(mChannelData);
        }
        mRvMultiChannel.setAdapter(mMultiChannelAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    private void loadChannels() {
        Log.d(TAG, "loadChannels");
        //Add dvbsi scenario - KGH
        //mChannelData = Utils.channelParser(this,result);
        //mChannelData_Pip = mChannelData;
        //mMusicChannelData = musicResult.getChannelList();  
        // End KGH

        mainUi();
    }

    private void setZappingTime() {
        Log.d(TAG, "setZappingTime");
        mZappingTimeData = new ArrayList<String>();
        mZappingTimeData.add("10 sec");
        mZappingTimeData.add("5 sec");
        mZappingTimeData.add("3 sec");
        mZappingTimeData.add("1 sec");
    }

    private void setTV(int index) {
        Log.d(TAG, "setTV - index : " + index + ", mChannelData : " + (mChannelData != null));
        if (mChannelData == null) {
            return;
        }

        mChannelSpinner.setSelection(index);

        // Start Add dvbsi scenario - KGH
        String currentChannel = mChannelData.get(index).getChannelUri();
        if (mIsUseLocalInstance) {
            if (mDvbSIService != null) {
                mDvbSIService.setCurrentChannelUri(currentChannel);
            }
        } else {
            BtvNavigator.getDvbSIService(mContext).setCurrentChannelUri(currentChannel);
        }
        // End KGH

        mChannelTitle.setVisibility(View.INVISIBLE);
//        mChannelTitle_Pip.setVisibility(View.INVISIBLE);
        mChannelProgramInfo.setVisibility(View.VISIBLE);
//        mChannelProgramInfo_Pip.setVisibility(View.INVISIBLE);
        // End KGH

        moveProgramPopUp(index);
    }

    /**
     * Start Add dvbsi scenario - KGH
     */
    private void setMusicTV(int index) {
        mChannelSpinner.setSelection(mCurrentPosition);

        if (mMusicChannelData == null) {
            return;
        }

        mMusicChannelSpinner.setSelection(index);
        MusicDvbService currentAudioChannel = mMusicChannelData.get(index);
        if (mIsUseLocalInstance) {
            if (mDvbSIService != null) {
                mDvbSIService.setCurrentChannelUri(currentAudioChannel.getChannelUri());
            }
        } else {
            BtvNavigator.getDvbSIService(mContext).setCurrentChannelUri(currentAudioChannel.getChannelUri());
        }

        mMusicChannelSpinner.setVisibility(View.VISIBLE);
        mMusicChannelProgramInfo.setVisibility(View.VISIBLE);
        mChannelProgramInfo.setVisibility(View.GONE);
        mChannelTitle.setVisibility(View.INVISIBLE);
//        mChannelTitle_Pip.setVisibility(View.INVISIBLE);

        moveMusicProgramPopUp(index);
    }


    private void moveMusicProgramPopUp(int index) {
        musichChannelAudioPid = mMusicChannelData.get(index).getApid();
        List<MusicProgram> audioPid_program = null;
        if (mIsUseLocalInstance) {
            if (mDvbSIService != null) {
                audioPid_program = mDvbSIService.getMusicProgramList(musichChannelAudioPid).getMusicProgramList();
            }
        } else {
            audioPid_program = BtvNavigator.getDvbSIService(mContext).getMusicProgramList(musichChannelAudioPid).getMusicProgramList();
        }
        Log.i(TAG, "setMusicTV() audioPid_program: " + audioPid_program);
        if (audioPid_program != null && audioPid_program.size() > 0) {
            if (mMusicProgramList != null) mMusicProgramList.clear();
            for (MusicProgram entity : audioPid_program) {
                Log.i(TAG, "setMusicTV() MusicProgram: " + entity.getName());
            }
            mMusicProgramList.addAll(audioPid_program);
            musicProgramAdapter.setList(mMusicProgramList);
            musicProgramAdapter.notifyDataSetChanged();
            currentMusicRunningProgramIndex = getCurrentRunningMusicProgram();
            mMusicChannelProgramInfo.setSelection(currentMusicRunningProgramIndex);
            long gap = mMusicProgramList.get(currentMusicRunningProgramIndex).getEndDate().getTime() - System.currentTimeMillis();
            mHandler.removeCallbacks(updateMusicProgramRunnable);
            mHandler.postDelayed(updateMusicProgramRunnable, gap + 1000);
        } /*else {
            mHandler.postDelayed(testRunnable, 1000);
        }*/
    }

    private int getCurrentRunningProgram() {
        int retIndex = -1;
        Calendar calendar = Calendar.getInstance();
        long reqST = calendar.getTimeInMillis();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        long reqET = calendar.getTimeInMillis();
        if (mProgramList != null && mProgramList.size() > 0) {
            for (AVProgram entity : mProgramList) {
                retIndex++;
                long proST = entity.getStartDate().getTime();
                long proET = entity.getEndDate().getTime();
                if ((reqST <= proST && reqET >= proET)
                        || (reqST > proST && reqET >= proET && reqST < proET)
                        || (reqST <= proST && reqET < proET && reqET > proST)
                        || (reqST > proST && reqET < proET)) {
                    break;
                }
            }
        }
        Log.d(TAG, "getCurrentRunningProgram() index : " + retIndex);
        return retIndex;
    }

//    private int getCurrentRunningProgram_Pip() {
//        int retIndex = -1;
//        Calendar calendar = Calendar.getInstance();
//        long reqST = calendar.getTimeInMillis();
//        calendar.add(Calendar.DAY_OF_YEAR, 1);
//        long reqET = calendar.getTimeInMillis();
//        if (mProgramList_Pip != null && mProgramList_Pip.size() > 0) {
//            for (AVProgram entity : mProgramList_Pip) {
//                retIndex++;
//                long proST = entity.getStartDate().getTime();
//                long proET = entity.getEndDate().getTime();
//                if ((reqST <= proST && reqET >= proET)
//                        || (reqST > proST && reqET >= proET && reqST < proET)
//                        || (reqST <= proST && reqET < proET && reqET > proST)
//                        || (reqST > proST && reqET < proET)) {
//                    break;
//                }
//            }
//        }
//        Log.d(TAG, "getCurrentRunningProgram_Pip() index : " + retIndex);
//        return retIndex;
//    }

    private int getCurrentRunningMusicProgram() {
        int retIndex = -1;
        Calendar calendar = Calendar.getInstance();
        long reqST = calendar.getTimeInMillis();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        long reqET = calendar.getTimeInMillis();
        if (mMusicProgramList != null && mMusicProgramList.size() > 0) {
            for (MusicProgram entity : mMusicProgramList) {
                retIndex++;
                long proST = entity.getStartDate().getTime();
                long proET = entity.getEndDate().getTime();
                if ((reqST <= proST && reqET >= proET)
                        || (reqST > proST && reqET >= proET && reqST < proET)
                        || (reqST <= proST && reqET < proET && reqET > proST)
                        || (reqST > proST && reqET < proET)) {
                    break;
                }
            }
        }
        Log.d(TAG, "getCurrentRunningMusicProgram() index : " + retIndex);
        return retIndex;
    }

//    private void displayMultiviewSelecteAlertDialog() {
//        CharSequence[] multichannelname = new CharSequence[mMultiViewChannelData.size()];
//        for (int i = 0; i < mMultiViewChannelData.size(); i++) {
//            multichannelname[i] = mMultiViewChannelData.get(i).getGroupName();
//        }
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("멀티 채널 선택하세요.");
//        builder.setItems(multichannelname, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                displayMultiView(mMultiViewChannelData.get(which));
//            }
//        });
//        builder.setNegativeButton("취소", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//        builder.show();
//    }

//    private void displayMultiView(MultiViewDvbService multiview) {
//        List<MultiViewLogicalCell> logicalCellList = null;
//        AVProgramList program_result = null;
//        if (mIsUseLocalInstance) {
//            if (mDvbSIService != null) {
//                logicalCellList = mDvbSIService.getMultiViewLogicalCellList(multiview.getGroupID()).getMultiViewLogicalCellList();
//            }
//        } else {
//            logicalCellList = BtvNavigator.getDvbSIService(mContext).getMultiViewLogicalCellList(multiview.getGroupID()).getMultiViewLogicalCellList();
//        }
//        StringBuilder channelList = new StringBuilder();
//        if (logicalCellList != null && logicalCellList.size() > 0) {
//            if (isPIPShowing) {
//                Log.d(TAG, "============== displayMultiView - call setPIPChange. pos : " + " => " + mCurrentPosition_Pip);
//                mChannelSpinner_Pip.setSelection(mCurrentPosition_Pip);
//                setPIPChange(false);
//            }
//
//            for (MultiViewLogicalCell entity : logicalCellList) {
//                channelList.append(entity.getChannelNumber()).append("|");
//            }
//            channelList.deleteCharAt(channelList.length()-1);
//            multiViewChannel = channelList.toString();
//            if (mIsUseLocalInstance) {
//                if (mDvbSIService != null) {
//                    mDvbSIService.setCurrentMultiChannel(channelList.toString());
//                    mMultiViewURI = mDvbSIService.makeMultiViewUri(logicalCellList);
//                }
//            } else {
//                BtvNavigator.getDvbSIService(mContext).setCurrentMultiChannel(channelList.toString());
//                mMultiViewURI = BtvNavigator.getDvbSIService(mContext).makeMultiViewUri(logicalCellList);
//            }
//
//            int ch = Integer.parseInt(Character.toString(channelList.charAt(channelList.length()-1)));
//            Log.e(TAG, "multiview - displayMultiView - channel count : " + mChannelData_Pip.size()
//                    + ", to find ch : " + ch);
//            int pos = 0;
//            for (int i = 0; i < mChannelData_Pip.size(); i++) {
//                if (ch == mChannelData_Pip.get(i).getCh()) {
//                    pos = i;
//                }
//            }
//            Log.e(TAG, "multiview - displayMultiView - isPIPShowing : " + isPIPShowing
//                    + ", pos : " + pos + ", mMultiViewURI : " + mMultiViewURI);
//            showMultiView(true, pos);
//        }
//        btnMultiviewProgram.setVisibility(View.VISIBLE);
//    }
    /**
     * End KGH
     */

    /**
     * 프로그램 정보
     *
     * @param index +1이 해당 채널 번호
     */
    private void moveProgramPopUp(int index) {
        currentChannelSid = mChannelData.get(index).getSid();    //mChannelData.get(index).getCh();
        Log.d(TAG, "moveProgramPopUp - channel sid: " + currentChannelSid);
        AVProgramList program_result = null;
        if (mIsUseLocalInstance) {
            if (mDvbSIService != null) {
                program_result = mDvbSIService.getAVProgramList(currentChannelSid);
            }
        } else {
            program_result = BtvNavigator.getDvbSIService(mContext).getAVProgramList(currentChannelSid);
        }
        if (program_result != null && program_result.getAVProgramsList() != null && program_result.getAVProgramsList().size() > 0) {
            if (mProgramList != null) mProgramList.clear();
            if (program_result.getAVProgramsList().get(0) != null && program_result.getAVProgramsList().get(0).getAVProgramList() != null) {
//                for (AVProgram tempValue : program_result.getAVProgramsList().get(0).getAVProgramList()) {
//                    mProgramList.add(tempValue);
//                }
                mProgramList.addAll(program_result.getAVProgramsList().get(0).getAVProgramList());
                programAdapter.setList(mProgramList);
                programAdapter.notifyDataSetChanged();
                currentRunningProgramIndex = getCurrentRunningProgram();
                mChannelProgramInfo.setSelection(currentRunningProgramIndex);
                Log.d(TAG, "moveProgramPopUp - mProgramList size: " + mProgramList.size());
            }
        }
        mMusicChannelSpinner.setVisibility(View.GONE);
        mMusicChannelProgramInfo.setVisibility(View.GONE);
        mChannelProgramInfo.setVisibility(View.VISIBLE);
    }

//    private void setMultiTV() {
//        Log.d(TAG, "setMultiTV - mMultiViewURI : " + mMultiViewURI + ", mChannelData : " + mChannelData);
//        if (mChannelData == null) {
//            return;
//        }
//        // Add dvbsi scenario - KGH
//        /*
//        if(siManager != null){
//            List<MultiViewChannel> multiViewChannel = siManager.getMultiViewChannelList().getMultiViewChannelList();
//            if(multiViewChannel != null && multiViewChannel.size() > 0){
//                String uri = siManager.makeMultiViewUri(multiViewChannel.get(0).getLogicalCellInfoList());
//                mainMediaPlayer.setDataSource(uri);
//            }
//        } else  mainMediaPlayer.setDataSource("skbsmv://reserved/?view1=skbiptv://239.192.67.99:49220?ch=7&ci=0&cp=0&pp=62&vp=62&vc=36&ap=63&ac=129&view2=skbiptv://239.192.67.100:49220?ch=9&ci=0&cp=0&pp=52&vp=52&vc=36&ap=53&ac=129&view3=skbiptv://239.192.67.101:49220?ch=11&ci=0&cp=0&pp=1215&vp=1215&vc=36&ap=1216&ac=129");
//        */
//        if (mMultiViewURI != null && mMultiViewURI.isEmpty() == false) {
//
//        }
//
//        mChannelProgramInfo.setVisibility(View.GONE);
//        mMusicChannelProgramInfo.setVisibility(View.GONE);
//        // End KGH
//    }
//
//    private void setPIP(int index) {
//        Log.d(TAG, "setPIP - index : " + index);
//        if (mChannelData_Pip == null || index < 0) {
//            return;
//        }

//        if (isVODPlaying) {
//
//        }

//        pipMediaPlayer.setDataSource("skbvod://cf2.hanafostv.com:555/adv/c22195-140912175323.ts");
//        pipMediaPlayer.prepareAsync();
//        pipMediaPlayer.start();
//
//        return;


//        if(pipMediaPlayer == null){
//            pipMediaPlayer = new MediaPlayer(1);
//            pipMediaPlayer.setDisplay(mIPTVSurfaceHolder_Pip);
//            pipMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                @Override
//                public void onPrepared(MediaPlayer mediaPlayer) {
//                    String chInfo = "CH No. " + mChannelData.get(mCurrentPosition_Pip).getChannelNum() + "   -  " + mChannelData.get(mCurrentPosition_Pip).getChannelName();
//                    mChannelTitle_Pip.setText(chInfo);
//
//                    pipMediaPlayer.start();
//                }
//            });
//        }

//        Log.d(TAG, "setPIP - call setDataSource channel : " + mChannelData_Pip.get(index).getChannelUri());
//        mChannelSpinner_Pip.setSelection(index);
////        updateProgramInfo();
//        currentChannelSid_Pip = mChannelData_Pip.get(index).getSid();    //mChannelData_Pip.get(index).getCh();
//        moveProgramPopUp_Pip(index);
//    }

    // Add dvbsi scenario - KGH
//    private void moveProgramPopUp_Pip(int index) {
//        if (isPIPShowing) {
//            AVProgramList programPip_result = null;
//            if (mIsUseLocalInstance) {
//                if (mDvbSIService != null) {
//                    programPip_result = mDvbSIService.getAVProgramList(currentChannelSid_Pip);
//                }
//            } else {
//                programPip_result = BtvNavigator.getDvbSIService(mContext).getAVProgramList(currentChannelSid_Pip);
//            }
//            if (programPip_result != null && programPip_result.getAVProgramsList() != null && programPip_result.getAVProgramsList().size() > 0) {
//                if (mProgramList_Pip != null) {
//                    mProgramList_Pip.clear();
//                    mProgramList_Pip.addAll(programPip_result.getAVProgramsList().get(0).getAVProgramList());
//                    programAdapter_Pip.setList(mProgramList_Pip);
//                    programAdapter_Pip.notifyDataSetChanged();
//                    currentRunningProgramIndex_Pip = getCurrentRunningProgram_Pip();
//                    mChannelProgramInfo_Pip.setSelection(currentRunningProgramIndex_Pip);
//                    mChannelProgramInfo_Pip.setVisibility(View.VISIBLE);
//                }
//            }
//        }
//    }
    // End KGH

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick" + v.getId());
        int id = v.getId();
        switch (id) {
            case R.id.iv_up:
                moveChannel(true, false);
                break;
            case R.id.iv_down:
                moveChannel(false, false);
                break;
            case R.id.iv_play:
                playTimerStart(mTimer, mTask, 0);
                break;
            case R.id.iv_pause:
                playTimerStop(mTimer, mTask, 0);
                break;
//            case R.id.btn_pip:
//                if (isPIPShowing) {
//                    setPIPChange(false);
//                } else {
//                    setPIPChange(true);
//                }
//                break;
//            case R.id.iv_up_pip:
//                moveChannel(true, true);
//                break;
//            case R.id.iv_down_pip:
//                moveChannel(false, true);
//                break;
//            case R.id.iv_play_pip:
//                if (isPIPShowing) {
//                    playTimerStart(mTimer_Pip, mTask_Pip, 1);
//                }
//                break;
//            case R.id.iv_pause_pip:
//                playTimerStop(mTimer_Pip, mTask_Pip, 1);
//                break;
//            case R.id.btn_multiview:
//                //Add dvbsi scenario - KGH
//                if (!isMultiView) {
//                    displayMultiviewSelecteAlertDialog();
//                } else {
//                    showMultiView(false);
//                }
//                // End KGH
//                break;
        }
    }

    private void playBtnVisibility(ImageView play, ImageView pause, boolean visible) {
        Log.d(TAG, "playBtnVisibility");
        if (play != null && pause != null) {  // Add dvbsi scenario - KGH
            play.setVisibility(View.GONE);
            pause.setVisibility(View.GONE);
            if (visible) {
                play.setVisibility(View.VISIBLE);
                play.requestFocus();
            } else {
                pause.setVisibility(View.VISIBLE);
                pause.requestFocus();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        playTimerStop(mTimer, mTask, 0);
//        playTimerStop(mTimer_Pip, mTask_Pip, 1);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");

        mDvbSIService.clearOnDvbSIListener(onDvbSIListener);
        mDvbSIService.clearOnDsmccListener(onDsmccListener);
        BtvNavigator.getDvbSIService(this).release();
    }

    private void playTimerStart(Timer timer, TimerTask task, final int type) {
        Log.d(TAG, "playTimerStart");
        int zappingTime = 10000;
        if (type == 0) {
            zappingTime = mZappingTime;
        } else if (type == 1) {
//            zappingTime = mZappingTime_Pip;
        }

        task = new TimerTask() {
            public void run() {
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (type == 0) {
                                if (isTimerAlive) {
                                    mCurrentPosition++;
                                    if (mCurrentPosition > mChannelData.size() - 1) {
                                        mCurrentPosition = 0;
                                    }
                                    mChannelSpinner.setSelection(mCurrentPosition);
                                    Log.d(TAG, "===================   timer task - call setTV");
                                    setTV(mCurrentPosition);
                                    isTimerAlive = false;
                                }
                                isTimerAlive = true;
                            } /*else if (type == 1) {
                                if (isTimerAlive_Pip) {
                                    mCurrentPosition_Pip++;
                                    if (mCurrentPosition_Pip > mChannelData_Pip.size() - 1) {
                                        mCurrentPosition_Pip = 0;
                                    }
                                    mChannelSpinner_Pip.setSelection(mCurrentPosition_Pip);
                                    Log.d(TAG, "===================   timer task - call setPIP");
                                    setPIP(mCurrentPosition_Pip);
                                    isTimerAlive_Pip = false;
                                }
                                isTimerAlive_Pip = true;
                            }*/
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        timer = new Timer();
        timer.schedule(task, 0, zappingTime);
        if (type == 0) {
            mTimer = timer;
            mTask = task;
            playBtnVisibility(mImagePlay, mImagePause, false);
        } /*else if (type == 1) {
            mTimer_Pip = timer;
            mTask_Pip = task;
            playBtnVisibility(mImagePlay_Pip, mImagePause_Pip, false);
        }*/
    }

    private void playTimerStop(Timer timer, TimerTask task, int type) {
        Log.d(TAG, "playTimerStop");

        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (task != null) {
            task.cancel();
            task = null;
        }

        if (type == 0) {
            isTimerAlive = false;
            playBtnVisibility(mImagePlay, mImagePause, true);
        } /*else if (type == 1) {
            isTimerAlive_Pip = false;
            playBtnVisibility(mImagePlay_Pip, mImagePause_Pip, true);
        }*/
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyUp KeyEvent=" + event.getKeyCode());
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_CHANNEL_UP:
                moveChannel(true, false);
                break;
            case KeyEvent.KEYCODE_CHANNEL_DOWN:
                moveChannel(false, false);
                break;
//            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
////                if(trickSpeed == MediaPlayer.TRICK_SPEED.X010) {
////                    if(isPaused) {
////                        isPaused = false;
////                        mainVodMediaPlayer.start();
////                    } else {
////                        mainVodMediaPlayer.pause();
////                        isPaused = true;
////                    }
////                } else {
////                    trickSpeed = MediaPlayer.TRICK_SPEED.X010;
////                    mainVodMediaPlayer.trick(trickSpeed);
////                    mChannelTitle.setText("VOD : x" + trickSpeed.getSpeedRatio());
////                }
//                break;
//            case KeyEvent.KEYCODE_MEDIA_STOP:
//                mChannelSpinner.setSelection(mCurrentPosition);
//                setTV(mCurrentPosition);
//                break;
            case KeyEvent.KEYCODE_0:
            case KeyEvent.KEYCODE_1:
            case KeyEvent.KEYCODE_2:
            case KeyEvent.KEYCODE_3:
            case KeyEvent.KEYCODE_4:
            case KeyEvent.KEYCODE_5:
            case KeyEvent.KEYCODE_6:
            case KeyEvent.KEYCODE_7:
            case KeyEvent.KEYCODE_8:
            case KeyEvent.KEYCODE_9:
                mHandler.removeCallbacks(mRunSetCh);
                mChannel = 10 * mChannel + event.getKeyCode() - KeyEvent.KEYCODE_0;
                mHandler.postDelayed(mRunSetCh, 2000);
                if (mTvCh != null) {
                    mTvCh.setText("" + mChannel);
                }
                break;

        }
        return super.onKeyUp(keyCode, event);
    }

    private void moveChannel(boolean isNext, boolean isPip) {
        int idx = 0, count = 0;
        if (isPip) {
//            idx = mChannelSpinner_Pip.getSelectedItemPosition();
//            count = mChannelSpinner_Pip.getCount();
        } else {
            idx = mChannelSpinner.getSelectedItemPosition();
            count = mChannelSpinner.getCount();
        }
        if (isNext) {
            if (++idx >= count) {
                idx = 0;
            }
        } else {
            if (--idx < 0) {
                idx = count - 1;
            }
        }
        Log.d(TAG, "moveChannel - idx : " + idx);
        if (isPip) {
//            mCurrentPosition_Pip = idx;
//            Log.d(TAG, "===========   moveChannel - call setPIP. isPIPShowing : " + isPIPShowing);
//            if (isPIPShowing) {
//                setPIP(idx);
//            } else {
//                mChannelSpinner_Pip.setSelection(idx);
//            }
        } else {
            mCurrentPosition = idx;
            if (mChannelData.get(idx).getCh() == 311) {
                setMusicTV(mMusicChannelCurrentPosition);
            } else {
                setTV(idx);
            }
        }
    }

    private void setVersion() {
        Log.d(TAG, "setVersion");
        String version = "Ver : " + Utils.getAppVersionName(mContext);
        mVersionInfo.setText(version);
    }

    public void showPipExceptionToast() {
        Log.d(TAG, "showPipExceptionToast");
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_pip_exception, (ViewGroup) findViewById(R.id.linear_toast));
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    AdapterView.OnItemSelectedListener onChannelItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
            Log.d(TAG, "onChannelItemSelectedListener isFirstStart : " + isFirstStart
                    + ", is s_channel : " + (parent.getId() == R.id.s_channel) + ", Item:" + parent.getId());
            if (isFirstStart) {
                isFirstStart = false;
                return;
            }

            int vId = parent.getId();
            if (vId == R.id.s_channel) {

                mCurrentPosition = i;
                if (mChannelData.get(i).getCh() == 311) {
                    setMusicTV(mMusicChannelCurrentPosition);
                } else {
                    setTV(i);
                }
//                if (mCurrentPosition > mChannelData.size() - 1) {
//                    mCurrentPosition = 0;
//                }

//                if(mainTVMediaPlayer != null){
//                    // Start Add dvbsi scenario - KGH
//                    // setTV(mCurrentPosition);
//                    if(mChannelData != null && mChannelData.get(mCurrentPosition).getChannelType() != IAVDvbService.AVDVBSERVICE_CHANNEL_TYPE_MUSIC_CHANNEL){
//                        setTV(mCurrentPosition);
//                    }else {
//                        setMusicTV(0);
//                    }
//                    // End KGH
//                }
            } /*else if (vId == R.id.s_channel_pip) {
                if (mCurrentPosition_Pip > mChannelData_Pip.size() - 1) {
                    mCurrentPosition_Pip = 0;
                }
                AVProgramList o = new AVProgramList();
                if(pipMediaPlayer != null && isPIPShowing == true){
                    Log.d(TAG, "onItemSelected - pos : " + i + ", " + mCurrentPosition_Pip);
                    if (i != mCurrentPosition_Pip) {
                        Log.d(TAG, "============== onItemSelected - call setPIP");
                        setPIP(i);
                    }
                }
                mCurrentPosition_Pip = i;
                setPIP(i);
            }*/
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    AdapterView.OnItemSelectedListener onZappingItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
            Log.d(TAG, "onZappingItemSelectedListener Item" + parent.getId());
            int vId = parent.getId();
            if (vId == R.id.s_zapping) {
                if (i == 0) {
                    mZappingTime = 10000;
                } else if (i == 1) {
                    mZappingTime = 5000;
                } else if (i == 2) {
                    mZappingTime = 3000;
                } else if (i == 3) {
                    mZappingTime = 1000;
                }
            } /*else if (vId == R.id.s_zapping_pip) {
                if (i == 0) {
                    mZappingTime_Pip = 10000;
                } else if (i == 1) {
                    mZappingTime_Pip = 5000;
                } else if (i == 2) {
                    mZappingTime_Pip = 3000;
                } else if (i == 3) {
                    mZappingTime_Pip = 1000;
                }
            }*/
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };


//    private void setPIPChange(boolean isShow) {
//        Log.d(TAG, "setPIPChange - isShow : " + isShow + ", pip pos : " + mCurrentPosition_Pip);
//        if (isShow) {
//            if (mImagePause.getVisibility() == View.VISIBLE) {
//                playTimerStop(mTimer, mTask, 0);
//            }
//            //mChannelSpinner.setEnabled(false);
//            //mLinearMain.setVisibility(View.GONE);
//
//            isPIPShowing = true;
//            if (mCurrentPosition_Pip == -1) {
//                mCurrentPosition_Pip = 0;
//            }
//
//            Log.d(TAG, "============== setPIPChange2 - call setPIP. pos : " + " => " + mCurrentPosition_Pip);
//            setPIP(mCurrentPosition_Pip);
//
//            mBtnPip.setText("PIP OFF");
//        } else {
//            if (mImagePause_Pip.getVisibility() == View.VISIBLE) {
//                playTimerStop(mTimer_Pip, mTask_Pip, 1);
//            }
//            mChannelProgramInfo_Pip.setVisibility(View.INVISIBLE);  // Add dvbsi scenario - KGH
//
//            isPIPShowing = false;
//            mBtnPip.setText("PIP ON");
//
//            String chInfo = "CH No. " + mChannelData.get(mCurrentPosition_Pip).getCh() + "   -  " + mChannelData.get(mCurrentPosition_Pip).getName();
//            mChannelTitle_Pip.setText(chInfo);
//        }
//    }

//    private void showMultiView(boolean isShow) {
//        showMultiView(isShow, mCurrentPosition);
//    }
//
//    private void showMultiView(boolean isShow, int pos) {
//        Log.d(TAG, "showMultiView - isShow : " + isShow + ", pip pos : " + mCurrentPosition_Pip
//                + ", backup pip pos : " + mMultiViewPosition_Pip + ", curr pos : " + mCurrentPosition);
//        if (isShow) {
//            isMultiView = true;
//            mMultiViewPosition_Pip = mCurrentPosition_Pip;
//            mCurrentPosition_Pip = pos;
//            mBtnPip.setEnabled(false);
//            stopZapping();
//            setMultiTV();
//            setPIPChange(true);
//            mBtnMultiview.setText("Multi\nOFF");
//        } else {
//            isMultiView = false;
//            mCurrentPosition_Pip = mMultiViewPosition_Pip;
//            mChannelSpinner_Pip.setSelection(mCurrentPosition_Pip);
////            mChannelSpinner.setSelection(mCurrentPosition);
//            if (isPIPShowing) {
//                setPIPChange(false);
//            }
//            setTV(mCurrentPosition);
//            mBtnMultiview.setText("Multi\nON");
//            btnMultiviewProgram.setVisibility(View.GONE);
//            mBtnPip.setEnabled(true);
//        }
//    }

//    private void stopZapping() {
//        Log.d(TAG, "stopZapping");
//        playTimerStop(mTimer, mTask, 0);
//        playTimerStop(mTimer_Pip, mTask_Pip, 1);
//    }

    public void onClickTest(View view) {
        Log.d(TAG, "onClickTest");
        //obd 임시 권한요청 나중에 시스템앱으로 넘어갈때 제거
//        checkVerify();
        Intent intent = new Intent(this, TestDvdsi.class);
        startActivity(intent);
    }

    private long time = 0;
    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed");
        if (System.currentTimeMillis() - time >= 2000) {
            time = System.currentTimeMillis();
            Toast.makeText(getApplicationContext(), "한번 더 누르면 종료", Toast.LENGTH_SHORT).show();
        } else if (System.currentTimeMillis() - time < 2000) {
            finish();
        }
    }
}
