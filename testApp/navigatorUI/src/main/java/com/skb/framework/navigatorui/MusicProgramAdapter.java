package com.skb.framework.navigatorui;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.skb.btv.framework.navigator.program.MusicProgram;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MusicProgramAdapter extends BaseAdapter {

    private static final String TAG =  "MusicProgramAdapter";

    private Context mContext = null;
    private List<MusicProgram> mListData = null;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.KOREAN);


    public MusicProgramAdapter(Context mContext) {
        super();
        Log.d(TAG, "MusicProgramAdapter");
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        Log.d(TAG, "getCount");
        return mListData == null ? 0 : mListData.size();
    }

    @Override
    public Object getItem(int position) {
        Log.d(TAG, "getItem");
        return mListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        Log.d(TAG, "getItemId");
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d(TAG, "getView");

        MusicProgramAdapter.SubViewHolder holder;

        if (convertView == null) {
            holder = new MusicProgramAdapter.SubViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listitem_channel, null);

            holder.mText = (TextView) convertView.findViewById(R.id.tv_title);
            holder.mImage = (ImageView) convertView.findViewById(R.id.iv_folder);

            convertView.setLayoutParams(new ListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 75));
            convertView.setTag(holder);

        } else {
            holder = (MusicProgramAdapter.SubViewHolder) convertView.getTag();
        }

        Date starttime = mListData.get(position).getStartDate();
        Date endtime = mListData.get(position).getEndDate();
        String title = mListData.get(position).getName();
        String singer = mListData.get(position).getSinger();
        String startdate = simpleDateFormat.format(starttime);
        String enddate = simpleDateFormat.format(endtime);
        String progInfo = title+"/"+singer+"("+startdate+"~"+enddate + ")";

        // specific process
        holder.mText.setText(progInfo);

        return convertView;
    }

    public void remove(int position) {
        Log.d(TAG, "remove");
        mListData.remove(position);
    }

    public void setList(List<MusicProgram> list) {
        this.mListData = list;
    }

    public List<MusicProgram> getList() {
        return mListData;
    }

    public class SubViewHolder
    {
        public TextView mText;
        public ImageView mImage;
    }

}
