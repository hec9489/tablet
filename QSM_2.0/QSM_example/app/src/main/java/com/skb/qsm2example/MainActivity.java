package com.skb.qsm2example;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.skb.qsm2lib.QSM2Report;
import com.skb.qsm2lib.Values.Application.CONTENTS_TYPE;
import com.skb.qsm2lib.Values.Command.ACTION;
import com.skb.qsm2lib.Values.Command.CMD;
import com.skb.qsm2lib.Values.Device.AUDIO_FORMAT_VERSION;
import com.skb.qsm2lib.Values.Device.BATTERY_3STEP;
import com.skb.qsm2lib.Values.Device.BT_PROFILE;
import com.skb.qsm2lib.Values.Device.BUTTON_TYPE;
import com.skb.qsm2lib.Values.Device.DEVICE_STATUS;
import com.skb.qsm2lib.Values.Application.GNB_ID;
import com.skb.qsm2lib.Values.Device.I_SET;
import com.skb.qsm2lib.Values.Device.I_SET_CODESET;
import com.skb.qsm2lib.Values.Device.RCU_STATUS;
import com.skb.qsm2lib.Values.Device.SOUND_TYPE;
import com.skb.qsm2lib.Values.Device.VIDEO_FORMAT;
import com.skb.qsm2lib.Values.Device.VIDEO_SCAN_TYPE;
import com.skb.qsm2lib.Values.LOG_INFO_T;
import com.skb.qsm2lib.Values.Device.LOG_TYPE;
import com.skb.qsm2lib.Values.Device.NETWORK_TYPE;
import com.skb.qsm2lib.Values.Application.SERVER_NAME;
import com.skb.qsm2lib.Values.Device.SERVICE_MODE;
import com.skb.qsm2lib.Values.SN_PARAM;
import com.skb.qsm2lib.Values.Device.STB_UPGRADE;
import com.skb.qsm2lib.Values.Application.VOD_WATCH_TYPE;
import com.skb.qsm2lib.Values.SN_PARAM;
import com.skb.qsm2lib.Values.TRANS_TYPE;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getName();
    private Button mBtnAppication, mBtnDevice, mBtnCommon, mBtnINITTransferType, mBtnTransferType, mWatermark;
    private QSM2Report mQSM2Report;
    private static final int MEMWATCH_BUILD = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private String TEST_MAC = "e8:12:65:92:3d:ba";  // ?뚯뒪?몄떆 mac ?섏젙 ?꾩슂

    private int SERVER_LIST_TYPE_STB = 1;
    private int SERVER_LIST_TYPE_DEV = 2;
    private int SERVER_LIST_TYPE_PRD = 3;

    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    Toolbar qsmToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        qsmToolbar = (Toolbar) findViewById(R.id.qsm_toolbar);
        setSupportActionBar(qsmToolbar);

        mBtnAppication = findViewById(R.id.button_application);
        mBtnDevice = findViewById(R.id.button_device);
        mBtnCommon = findViewById(R.id.button_common);
        mBtnINITTransferType = findViewById(R.id.button_init_transfer_type);
        mBtnTransferType = findViewById(R.id.button_transfer_type);
        mWatermark = findViewById(R.id.button_watermark);


        initButtonAction();

        mQSM2Report = new QSM2Report(getApplicationContext());

        /* normal mode */
        /*if(!mQSM2Report.initialize()){
            Log.d(TAG, "failed to initialize QSM");
        }*/

        /* receive command mode : need to set callback function */
        QSM2Report.OnReceiveCallBack callback = new QSM2Report.OnReceiveCallBack(){
            @Override
            public void onReceiveData(int cmd, byte[] action, int action_size, String data){
                Log.d(TAG, "QSM : received data in onReceiveData. action_size="+action_size);
                switch(cmd){
                    case CMD.SET_WATER_MARK:
                        /* VISIBILITY */
                        if(action[0] == ACTION.VISIBILITY_ENABLE)
                            Log.d(TAG, "QSM : received SET_WATER_MARK VISIBILITY : ENABLE");
                        else if(action[0] == ACTION.VISIBILITY_DISABLE)
                            Log.d(TAG, "QSM : received SET_WATER_MARK VISIBILITY : DISABLE");
                        else if(action[0] == ACTION.VISIBILITY_ENABLE_BY_FORCE)
                            Log.d(TAG, "QSM : received SET_WATER_MARK VISIBILITY : ENABLE_BY_FORCE");

                        /* LIVE */
                        if(action[1] == ACTION.LIVE_ON)
                            Log.d(TAG, "QSM : received SET_WATER_MARK LIVE : ON");
                        else if(action[1] == ACTION.LIVE_OFF)
                            Log.d(TAG, "QSM : received SET_WATER_MARK LIVE : OFF");
                        break;
                    default:
                        break;
                }
            }
        };

        if(!mQSM2Report.initialize(callback)){
            Log.d(TAG, "failed to initialize QSM");
        }

        if(MEMWATCH_BUILD == 1) verifyStoragePermissions(MainActivity.this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.stb:
                if(mQSM2Report.changeServer(SERVER_LIST_TYPE_STB)) {
                    Toast.makeText(getApplicationContext(), "STB Server change is complete!\r\nPlease restart after finishing", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getApplicationContext(), "Server change failed", Toast.LENGTH_LONG).show();
                }
                return true;

            case R.id.dev:
                if(mQSM2Report.changeServer(SERVER_LIST_TYPE_DEV)) {
                    Toast.makeText(getApplicationContext(), "DEV Server change is complete!\r\nPlease restart after finishing", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getApplicationContext(), "Server change failed", Toast.LENGTH_LONG).show();
                }
                return true;

            case R.id.prd:
                if(mQSM2Report.changeServer(SERVER_LIST_TYPE_PRD)) {
                    Toast.makeText(getApplicationContext(), "PRD Server change is complete!\r\nPlease restart after finishing", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getApplicationContext(), "Server change failed", Toast.LENGTH_LONG).show();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(
                activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
	
    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        mQSM2Report.release();
        super.onDestroy();
    }

        @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
    }

    private void initButtonAction(){

        mBtnAppication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Application Test");
                exampleForApplication();
            }
        });
        mBtnDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Device Test");
                exampleForDevice();
            }
        });
        mBtnCommon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Common Test");
                exampleForCommon();
            }
        });

        mBtnINITTransferType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Transfer INIT type test");
                exampleForTransferINIT_Type();
            }
        });

        mBtnTransferType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Transfer type test");
                exampleForTransferCOLDBOOTING_Type();
            }
        });

        mWatermark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Requet watermark info");
                exampleForRequestWatermarkInfo();
            }
        });
    }

    /* Application */
    private void exampleForApplication(){

        /* usage ===================================================
        mQSM2Report.start(); //create internal JSON object
        mQSM2Report.put(SN.A001, LOG_INFO_T.APP_NATIVE);
        mQSM2Report.put(...);
        .....
        mQSM2Report.report(); //send JSON string to SKB Log service
        ============================================================*/

        /* All test */
        mQSM2Report.start();
        mQSM2Report.put(SN_PARAM.A001_log_info_type, LOG_INFO_T.APP_NATIVE);
        mQSM2Report.put(SN_PARAM.A002_log_time,QSM2Report.getLogTime());
        mQSM2Report.put(SN_PARAM.A003_webui_version,"5.1.3.152");
        mQSM2Report.put(SN_PARAM.A004_app_release_version,"B tv : 5.1.3");
        mQSM2Report.put(SN_PARAM.A005_subapp_release_version,"kids or senior version");
        mQSM2Report.put(SN_PARAM.A006_server_name, SERVER_NAME.AMS);
        mQSM2Report.put(SN_PARAM.A007_page_id, "/common/play/synopsis");
        mQSM2Report.put(SN_PARAM.A008_menu_id, "NM0010001002");
        mQSM2Report.put(SN_PARAM.A009_gnb_id, GNB_ID.U5_01);
        mQSM2Report.put(SN_PARAM.A010_sub_gnb_id, "kids_GNB menu string");
        mQSM2Report.put(SN_PARAM.A011_vod_watch_type, VOD_WATCH_TYPE.START);
        mQSM2Report.put(SN_PARAM.A012_error_text, "TypeError: Cannot read property 'gnb_typ_cd' of null\n" +
                "at VueComponent.mounted (Home.vue?76f2:218)\n" +
                "at invokeWithErrorHandling (vue.runtime.esm.js?2b0e:1854)\n" +
                "at callHook (vue.runtime.esm.js?2b0e:4219)\n" +
                "at Object.insert (vue.runtime.esm.js?2b0e:3139)\n" +
                "at invokeInsertHook (vue.runtime.esm.js?2b0e:6346)\n" +
                "at VueComponent.patch [as __patch__] (vue.runtime.esm.js?2b0e:6565)\n" +
                "at VueComponent.Vue._update (vue.runtime.esm.js?2b0e:3948)\n" +
                "at VueComponent.updateComponent (vue.runtime.esm.js?2b0e:4066)\n" +
                "at Watcher.get (vue.runtime.esm.js?2b0e:4479)\n" +
                "at Watcher.run (vue.runtime.esm.js?2b0e:4554)\n");

        mQSM2Report.put(SN_PARAM.A013_error_description, "http://agw.sk-iptv.com:8080/ui5web/v513/synopsis/SynopsisView?sris_id=CS11025656&epsd_id=CE1000346631&menu_id=NM1000024051\n");

        /* contents_info */
        JSONObject json = new JSONObject();
        try {
            json.put("title","기생충");
            json.put("contents_type", CONTENTS_TYPE.VOD);
            json.put("genre","영화");
            json.put("genre_code","MG0000000001");
            json.put("paid",1);
            json.put("purchase",0);
            json.put("episode_id","MM000138");
            json.put("product_id","9C076971-98F3-45R5-ABF0-85E444499DFD");
            json.put("purchase_type","ppv");
            json.put("monthly_pay","season(드라마)");
            json.put("running_time",3600);
            json.put("list_price",3000);
            json.put("payment_price",2500);
            json.put("channel",11);
            mQSM2Report.put(SN_PARAM.A014_contents_info,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        Log.d(TAG, mQSM2Report.getReportString());
        mQSM2Report.report();
    }

    /* Device*/
    private void exampleForDevice(){
        /* usage ===================================================
        mQSM2Report.start(); //create JSON object
        mQSM2Report.put(SN.D001, LOG_INFO_T.DEVICE);
        mQSM2Report.put(...);
        .....
        mQSM2Report.report(); //send JSON string to SKB Log service
        ============================================================*/
        mQSM2Report.start(); //create JSON object

        mQSM2Report.put(SN_PARAM.D001_log_info_type, LOG_INFO_T.DEVICE);
        mQSM2Report.put(SN_PARAM.D002_log_time, QSM2Report.getLogTime());

        /* device_info */
        JSONObject json = new JSONObject();
        try {
            json.put("device_status",DEVICE_STATUS.ON);
            json.put("epg_version", "xxxxxxxxxxxxx");
            json.put("auth",0);
            json.put("iptv_area","mbs=1^kbs=41^sbs=61");
            json.put("network_type", NETWORK_TYPE.ETHERNET);
            mQSM2Report.put(SN_PARAM.D003_device_info,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        /* system_info */
        json = new JSONObject();
        try {
            json.put("cpu_usage",67);
            json.put("cpu_limit", 95);
            json.put("memory_size",256);
            json.put("memory_usage",97);
            json.put("memory_limit", 50);
            json.put("disk_size", 250);
            json.put("disk_usage",28);
            json.put("disk_limit",95);
            json.put("running_time", 86400);
            mQSM2Report.put(SN_PARAM.D004_system_info,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        /* network_info */
        json = new JSONObject();
        try {
            json.put("network_mode",0);
            json.put("dhcp_mode", 1);
            json.put("ip_address","0.0.0.0");
            json.put("subnet_mask","0.0.0.0");
            json.put("gateway","0.0.0.0");
            json.put("dns_1st","0.0.0.0");
            json.put("dns_2nd","0.0.0.0");

            mQSM2Report.put(SN_PARAM.D005_network_info,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        mQSM2Report.put(SN_PARAM.D006_service_mode, SERVICE_MODE.KIDITV);
        mQSM2Report.put(SN_PARAM.D007_stb_upgrade, STB_UPGRADE.STATUS_900);
        mQSM2Report.put(SN_PARAM.D008_hdmi_plug_inout, 1);
        mQSM2Report.put(SN_PARAM.D009_hdmi_hdcp_version, "2.2");

        /* device_info_hdmi */
        json = new JSONObject();
        try {
            json.put("manufacturer_id","xxxxxxx");
            json.put("manufacturer_week", "xxxxxx");
            json.put("product_code","xxxxxx");
            json.put("serial_id","xxxxxx");
            json.put("screen_resolution","xxxxxx");
            json.put("max_resolution","xxxxxx");
            json.put("sound_type", SOUND_TYPE.HDMI);
            json.put("monitor_name","xxxxxx");
            json.put("CEC_conf","xxxxxx");
            json.put("EDID_value","xxxxxx");
            json.put("s_bbrate",50);

            mQSM2Report.put(SN_PARAM.D010_device_info_hdmi,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        /* device_info_rcu */
        json = new JSONObject();
        try {
            json.put("rcu_model","BRM_BT10");
            json.put("rcu_fw_version", "0140");
            json.put("rcu_mac_address","e8:12:65:92:3d:ba");
            json.put("rcu_ir_model","0x90");
            json.put("rcu_ir_manufacturer","movon");
            json.put("battery_3step", BATTERY_3STEP.Normal);
            json.put("battery_bas", 90);
            json.put("bt_pairing_info","xxxxxx");
            json.put("bt_connection_status",0);
            json.put("quick_set",1);
            json.put("quick_set_codeset","T2731");
            json.put("i_set", I_SET.set_auto);
            json.put("i_set_codeset", I_SET_CODESET.codeset_237);
            json.put("button_type", BUTTON_TYPE.BT);

            mQSM2Report.put(SN_PARAM.D011_device_info_rcu,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        /* device_info_rcu_error */
        json = new JSONObject();
        try {
            json.put("function","xxxxx");
            json.put("status", RCU_STATUS.RCU_success);
            json.put("description","xxxxxx");
            json.put("error","xxxxxxx");
            json.put("error_description","xxxxx");

            mQSM2Report.put(SN_PARAM.D012_device_info_rcu_error,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        /* device_info_bt */
        json = new JSONObject();
        try {
            json.put("bt_device_name","xxxxx");
            json.put("bt_mac", "e8:12:65:92:3d:ba");
            json.put("bt_device_profile", BT_PROFILE.HID);
            json.put("bt_status",1);

            mQSM2Report.put(SN_PARAM.D013_device_info_bt,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        mQSM2Report.put(SN_PARAM.D014_user_config, "xxxxxxxxxxxxx");

        /* contents_info */
        json = new JSONObject();
        try {
            json.put("title","xxxxx");
            json.put("contents_type", "VOD");
            json.put("genre", "xxxx");
            json.put("genre_code","MG0000001");
            json.put("episode_id", "MM000138");
            json.put("product_id", "9C076971-98-xxxxxxxx");
            json.put("running_time",3600);

            mQSM2Report.put(SN_PARAM.D015_contents_info,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        /* video_info */
        json = new JSONObject();
        try {
            json.put("video_format", VIDEO_FORMAT.AVC);
            json.put("profile", "High@L4");
            json.put("format_config", "xxxx");
            json.put("bitrate_mode",0);
            json.put("max_bitrate", 10);
            json.put("width", 1920);
            json.put("height",1080);
            json.put("aspect_ratio", "16:9");
            json.put("frame_rate", 29.97);
            json.put("color_space", "YUV");
            json.put("chroma_sub_sampling","4:2:0");
            json.put("bit_depth", 10);
            json.put("scan_type", VIDEO_SCAN_TYPE.Interlace);
            json.put("scan_order","Top Field First");
            json.put("colour_primaries","BT.709");
            json.put("transfer_characteristics", "BT.709");
            json.put("matrix_coefficient", "BT.709");
            json.put("Video Quality",95);

            mQSM2Report.put(SN_PARAM.D016_video_info,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        /* audio_info */
        json = new JSONObject();
        try {
            json.put("audio_format","AAC LC");
            json.put("format_version", AUDIO_FORMAT_VERSION.Version_2);
            json.put("muxing_mode","ADTS");
            json.put("channel",2);
            json.put("channel_layout","L R");
            json.put("sampling_rate",48.0);
            json.put("frame_rate",46.875);

            mQSM2Report.put(SN_PARAM.D017_audio_info,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        /* error_status_info */
        json = new JSONObject();
        try {
            json.put("error_duration",0);
            json.put("error_repeat", 0);
            json.put("check_ts","xxxxxxxx");

            mQSM2Report.put(SN_PARAM.D018_error_status_info,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        /* server_check_result */
        json = new JSONObject();
        try {
            json.put("server_name","CSS");
            json.put("server_ip", "0.0.0.0");
            json.put("retry_cnt",1);
            json.put("error_code", "xxxxxxxx");
            json.put("error_msg","xxxxxxxx");

            mQSM2Report.put(SN_PARAM.D019_server_check_result,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        /* btv_error */
        json = new JSONObject();
        try {
            json.put("error_code","xxxxxxxx");
            json.put("message", "xxxxxxxx");

            mQSM2Report.put(SN_PARAM.D020_btv_error,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        /* vod_error */
        json = new JSONObject();
        try {
            json.put("error_status",1);
            json.put("message", "xxxxxxxx");

            mQSM2Report.put(SN_PARAM.D021_vod_error,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        mQSM2Report.put(SN_PARAM.D022_signature, "xxxxxxxxxxxxx");

        Log.d(TAG, mQSM2Report.getReportString());
        mQSM2Report.report();

    }

    /* Common */
    private void exampleForCommon(){

        /* usage ===================================================
        mQSM2Report.start(); //create JSON object
        mQSM2Report.put(SN.C001, QSM2Report.getLogTime());
        mQSM2Report.put(...);
        .....
        mQSM2Report.report(); //send JSON string to SKB Log service
        ============================================================*/
        mQSM2Report.start(); //create JSON object

        mQSM2Report.put(SN_PARAM.C001_client_send_time, QSM2Report.getLogTime());
        mQSM2Report.put(SN_PARAM.C002_mac_address, TEST_MAC);
        mQSM2Report.put(SN_PARAM.C003_device_id, "F192A949-C399-11E3-92F5-2FDE1C200B52");
        mQSM2Report.put(SN_PARAM.C004_zip_code, "12345");
        mQSM2Report.put(SN_PARAM.C005_client_ip, "0.0.0.0");

        JSONObject json = new JSONObject();
        try {
            json.put("device_model","XXXXXX");
            json.put("manufacturer","XXXXX");
            json.put("OS","android");
            json.put("OS_version","9.1");
            mQSM2Report.put(SN_PARAM.C006_device_base_info,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        json = new JSONObject();
        try {
            json.put("ui_version","1.0");
            json.put("fw_version", "1.0");
            json.put("nugu_version","1.0");
            json.put("browser_name", "chrome");
            json.put("browser_version","1.0");

            mQSM2Report.put(SN_PARAM.C007_version_info,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        json = new JSONObject();
        try {
            json.put("service_name","XXXXXX");
            json.put("log_type", LOG_TYPE.DEV);
            mQSM2Report.put(SN_PARAM.C008_log_info,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }
        Log.d(TAG, mQSM2Report.getReportString());

        mQSM2Report.report();
    }

    private void exampleForTransferINIT_Type() {

        /* usage ===================================================
        mQSM2Report.start(TRANS_TYPE.INIT); //create internal JSON object with Transfer type
        mQSM2Report.put(SN.A001, LOG_INFO_T.APP_NATIVE);
        mQSM2Report.put(...);
        .....
        mQSM2Report.report(); //send JSON string to SKB Log service
        ============================================================*/

        /* Transfer Type : INIT **********************************************/
        mQSM2Report.start(TRANS_TYPE.INIT);
        mQSM2Report.put(SN_PARAM.C002_mac_address, TEST_MAC);
        mQSM2Report.put(SN_PARAM.C003_device_id, "{F192A949-C399-11E3-92F5-2FDE1C200B52}");
        mQSM2Report.put(SN_PARAM.C004_zip_code, "12345");
        mQSM2Report.put(SN_PARAM.C005_client_ip, "0.0.0.0");

        JSONObject json = new JSONObject();
        try {
            json.put("device_model","BLE-TS100");
            json.put("manufacturer","Lenovo");
            json.put("OS","android");
            json.put("OS_version","9.1");
            mQSM2Report.put(SN_PARAM.C006_device_base_info,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        json = new JSONObject();
        try {
            json.put("ui_version","1.0");
            json.put("fw_version", "1.0");
            json.put("nugu_version","1.0");
            json.put("browser_name", "chrome");
            json.put("browser_version","1.0");

            mQSM2Report.put(SN_PARAM.C007_version_info,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        json = new JSONObject();
        try {
            json.put("service_name","XXXXXX");
            json.put("log_type", LOG_TYPE.DEV);
            mQSM2Report.put(SN_PARAM.C008_log_info,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }
        Log.d(TAG, mQSM2Report.getReportString());
        mQSM2Report.report();
    }

    private void exampleForTransferCOLDBOOTING_Type() {

        /* usage ===================================================
        mQSM2Report.start(TRANS_TYPE.COLD_BOOTING); //create internal JSON object with Transfer type
        mQSM2Report.put(SN.A001, LOG_INFO_T.APP_NATIVE);
        mQSM2Report.put(...);
        .....
        mQSM2Report.report(); //send JSON string to SKB Log service
        ============================================================*/

        /* Transfer Type : COLD_BOOTING ***************************************************/
        mQSM2Report.start(TRANS_TYPE.COLD_BOOTING);

        mQSM2Report.put(SN_PARAM.D001_log_info_type, LOG_INFO_T.DEVICE);
        mQSM2Report.put(SN_PARAM.D002_log_time, QSM2Report.getLogTime());

        /* device_info */
        JSONObject json = new JSONObject();
        try {
            json.put("device_status",DEVICE_STATUS.ON);
            json.put("epg_version", "xxxxxxxxxxxxx");
            json.put("auth",0);
            json.put("iptv_area","mbs=1^kbs=41^sbs=61");
            json.put("network_type", NETWORK_TYPE.ETHERNET);
            mQSM2Report.put(SN_PARAM.D003_device_info,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        /* network_info */
        json = new JSONObject();
        try {
            json.put("network_mode",0);
            json.put("dhcp_mode", 1);
            json.put("ip_address","0.0.0.0");
            json.put("subnet_mask","0.0.0.0");
            json.put("gateway","0.0.0.0");
            json.put("dns_1st","0.0.0.0");
            json.put("dns_2nd","0.0.0.0");

            mQSM2Report.put(SN_PARAM.D005_network_info,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }

        /* device_info_rcu */
        json = new JSONObject();
        try {
            json.put("rcu_model","BRM_BT10");
            json.put("rcu_fw_version", "0140");
            json.put("rcu_mac_address","e8:12:65:92:3d:ba");
            json.put("rcu_ir_model","0x90");
            json.put("rcu_ir_manufacturer","movon");
            json.put("battery_3step", BATTERY_3STEP.Normal);
            json.put("battery_bas", 90);
            json.put("bt_pairing_info","xxxxxx");
            json.put("bt_connection_status",0);
            json.put("quick_set",1);
            json.put("quick_set_codeset","T2731");
            json.put("i_set", I_SET.set_auto);
            json.put("i_set_codeset", I_SET_CODESET.codeset_237);
            json.put("button_type", BUTTON_TYPE.BT);

            mQSM2Report.put(SN_PARAM.D011_device_info_rcu,json);
        }catch(JSONException ex){
            ex.printStackTrace();
        }
        Log.d(TAG, mQSM2Report.getReportString());

        mQSM2Report.report();
    }

    private void exampleForRequestWatermarkInfo() {
        Log.d(TAG,"QSM Report exampleForRequestWatermarkInfo");
        mQSM2Report.sendRequest(CMD.REQUEST_WATER_MARK);
    }
}
