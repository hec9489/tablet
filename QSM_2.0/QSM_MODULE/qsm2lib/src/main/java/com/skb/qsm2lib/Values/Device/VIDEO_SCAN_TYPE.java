package com.skb.qsm2lib.Values.Device;

/* VIDEO SCAN_TYPE */
public class VIDEO_SCAN_TYPE{
    public static final String Progressive = "Progressive";
    public static final String Interlace = "Interlace";
    public static final String MBAFF = "MBAFF";
}