package com.skb.qsm2lib.Values.Device;

/* Log type */
public class LOG_TYPE{
    public static final String DEV = "DEV";
    public static final String BMT = "BMT";
    public static final String PRODUCTION = "PRODUCTION";
}