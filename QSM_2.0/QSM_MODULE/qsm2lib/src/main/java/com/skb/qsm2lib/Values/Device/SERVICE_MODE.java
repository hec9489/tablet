package com.skb.qsm2lib.Values.Device;

/* SERVICE MODE */
public class SERVICE_MODE{
    public static final String SLP = "SLP";
    public static final String HOM = "HOM";
    public static final String ITV = "ITV";
    public static final String VOD = "VOD";
    public static final String VAS = "VAS";
    public static final String APP = "APP";
    public static final String KIDHOM = "KIDHOM";
    public static final String KIDITV = "KIDITV";
    public static final String KIDVOD = "KIDVOD";
    public static final String MVIEW = "MVIEW";
}