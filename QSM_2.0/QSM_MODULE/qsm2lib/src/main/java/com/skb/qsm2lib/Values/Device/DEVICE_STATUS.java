package com.skb.qsm2lib.Values.Device;

/* DEVICE STATUS */
public class DEVICE_STATUS{
    public static final String ON = "ON";
    public static final String OFF = "OFF";
    public static final String RESET = "RESET";
    public static final String SLEEP = "SLEEP";
}