package com.skb.qsm2lib;

import com.skb.qsm2lib.Values.DEVICE_INFO_T;
import com.skb.qsm2lib.QSM2Report;

import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class QSM2Transfer {

    private final String TAG = this.getClass().getName();
    private LocalSocket mSender = null;
    private OutputStream mOutputStream = null;
    private InputStream mInputStream = null;
    private String LOCAL_SOCKET_NAME = "/dev/socket/qsm_server";
    private QSM2Report.OnReceiveCallBack mCallBack = null;

    //cracker@yiwoosolution.co.kr
    static {
        System.loadLibrary("qsm_server");
		native_init();
    }
    private static native int native_init();
    private native int native_release();
    private native int sendToserver(String dataJSON);

    /* PUBLIC */
    public boolean connect(){
        //cracker@yiwoosolution.co.kr
        if(DEVICE_INFO_T.TABLET == 0)
        {
            try {
                mSender = new LocalSocket();
                LocalSocketAddress address = new LocalSocketAddress(LOCAL_SOCKET_NAME,
                        LocalSocketAddress.Namespace.FILESYSTEM);
    
                mSender.connect(address);
                if(mSender.isConnected()){
                    mOutputStream = mSender.getOutputStream();
                }else{
                    return false;
                }
            } catch (IOException ex) {
                Log.wtf("QSM IOEXCEPTION", ex);
                return false;
            }
        }
        Log.d(TAG, "connect : succuss");
        return true;
    }

    public boolean connectForReceive(){
        //cracker@yiwoosolution.co.kr
        if(DEVICE_INFO_T.TABLET == 0)
        {
            try {
                mSender = new LocalSocket();
                LocalSocketAddress address = new LocalSocketAddress(LOCAL_SOCKET_NAME,
                        LocalSocketAddress.Namespace.FILESYSTEM);
    
                mSender.connect(address);
                if(mSender.isConnected()){
                    mOutputStream = mSender.getOutputStream();
                    mInputStream = mSender.getInputStream();
                }else{
                    return false;
                }
            } catch (IOException ex) {
                Log.wtf("QSM IOEXCEPTION", ex);
                return false;
            }
        }
        
        Log.d(TAG, "connectForReceive : succuss");
		return true;
    }

    public boolean connectForReceive(QSM2Report.OnReceiveCallBack callback){
        Log.d(TAG, "connectForReceive : succuss");
		mCallBack = callback;
		return true;
    }
	
    public int sendData(byte[] data)
    {
        //cracker@yiwoosolution.co.kr
        if(DEVICE_INFO_T.TABLET == 0)
        {
            if(mSender == null || mOutputStream == null){
                Log.d(TAG, "QSM sendData failed. socket is null");
                return -1;
            }
            if(!mSender.isConnected()){
                Log.d(TAG, "QSM sendData - socket is not connected");
                if(!connect()){
                    Log.d(TAG, "QSM sendData - fail to connect");
                    return -1;
                }
            }
    
            try {
                mOutputStream.write(data);
                Log.d(TAG, "QSM : send data to QSM service - byte data size ="+data.length);
				return data.length;
            }catch (IOException ex) {
                Log.wtf("QSM IOEXCEPTION", ex);
    
                // skb qsm log service was restarted. so we have to reconnect
                if(!reconnectAndResend(data)){
                    Log.d(TAG, "QSM sendData - fail to retry");
                    return -1;
                }
            }
        }
		else
		{
            String strJson = new String(data);
            if(sendToserver(strJson) >= 0)
            {
                Log.d(TAG, "sendData length = " + data.length);
                return data.length;
            }
            else
            {
                Log.d(TAG, "sendData error");
                return -1;
            }
		}
		return -1;
    }

    public boolean isConnected(){
        return mSender.isConnected();
    }

    public void disconnect(){
	    //cracker@yiwoosolution.co.kr
        if(DEVICE_INFO_T.TABLET == 0)
        {
            if(mSender == null || mOutputStream == null){
                Log.d(TAG, "QSM disconnect failed. socket is null");
                return;
            }
            Log.d(TAG, "QSM disconnected");
            try {
                mOutputStream.close();
                mInputStream.close();
                mSender.close();
                mOutputStream = null;
                mInputStream = null;
                mSender = null;
            }catch (IOException ex) {
                Log.wtf("QSM IOEXCEPTION", ex);
            }
        }
    }

    public InputStream getInputStream(){
        return mInputStream;
    }

    /* PRIVATE */
    private boolean reconnectAndResend(byte[] data){
        disconnect();
        if(!connect()){
            Log.d(TAG, "QSM sendData - faile to connect");
            return false;
        }
        // retry to send data
        try {
            mOutputStream.write(data);
        }catch (IOException e) {
            Log.wtf("QSM IOEXCEPTION", e);
            return false;
        }
        return true;
    }

    //cracker@yiwoosolution.co.kr
    public void readFromJniCallBack(String strData, int size)
    {
        byte[] data = new byte[10];
        if(mCallBack != null)
       	{
             Log.d(TAG, "QSM readFromJniCallBack: call callback function");

             try {
                 data = strData.getBytes("UTF-8");
             }catch(Exception e){
                 Log.e(TAG, "readFromJniCallBack : error="+e.toString());
             }
			 	
             byte action[] = new byte[10];
             for(int i=1; i<size;i++){
                 action[i-1] = data[i];
             }
             mCallBack.onReceiveData(data[0], action, size-1,null);
        }
    }
	
}
