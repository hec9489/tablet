package com.skb.qsm2lib.Values.Application;

/* Server name */
public class SERVER_NAME{
    public static final String AMS = "AMS";
    public static final String VASS  = "VASS";
    public static final String IIS = "IIS";
    public static final String NPS = "NPS";
    public static final String MenuNavi = "MenuNavi";
    public static final String DSM = "DSM";
    public static final String RMS = "RMS";
    public static final String TRS = "TRS";
    public static final String OPMS_ADS = "OPMS(ADS)";
    public static final String PNS = "PNS";
    public static final String ECDN_DRM = "ECDN DRM";
    public static final String MeTV = "MeTV";
    public static final String SDS = "SDS";
    public static final String NSRVS = "NSRVS";
    public static final String CA = "CA";
    public static final String STB_BMT = "STB BMT";
}