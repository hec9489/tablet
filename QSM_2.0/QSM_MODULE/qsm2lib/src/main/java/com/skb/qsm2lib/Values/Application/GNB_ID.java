package com.skb.qsm2lib.Values.Application;

/* GNB ID */
public class GNB_ID{
    public static final String VIEW_ALL = "VIEW_ALL";
    public static final String U5_01 = "U5_01"; // MY
    public static final String U5_02 = "U5_02"; // 월정액
    public static final String U5_03 = "U5_03"; // 홈
    public static final String U5_04 = "U5_04"; // 영화.시리즈
    public static final String U5_05 = "U5_05"; // TV다시보기
    public static final String U5_06 = "U5_06"; // 애니
    public static final String U5_07 = "U5_07"; // 키즈
    public static final String U5_08 = "U5_08"; // 다큐.라이프
    public static final String U5_09 = "U5_09"; // TV앱
    public static final String U5_10 = "U5_10"; // 시니어
    public static final String U5_11 = "U5_11"; // 검색
}