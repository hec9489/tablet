package com.skb.qsm2lib.Values.Application;

/* VOD watch type */
public class VOD_WATCH_TYPE{
    public static final String EMPTY = "";
    public static final String START = "START";
    public static final String PAUSE = "PAUSE";
    public static final String STOP = "STOP";
}