package com.skb.qsm2lib;

import com.skb.qsm2lib.QSM2Transfer;
import com.skb.qsm2lib.Values.TRANS_TYPE;
import com.skb.qsm2lib.Values.DEVICE_INFO_T;

import org.json.JSONObject;
import org.json.JSONException;
import android.util.Log;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class QSM2Report {
    public interface OnReceiveCallBack{
        void onReceiveData(int cmd, byte[] action, int action_size, String data);
    }
    private final String TAG = this.getClass().getName();
    private static final String DATE_FORMAT = "yyyyMMddHHmmss.sss";

    private QSM2Transfer mQSM2Transfer;
    private JSONObject mReportJSON = null;
    private boolean mIsQSMConnected;
    private String mTransType = null;
    private ReceiveRunnable mReceiveRunnable = null;
    private Thread mReceiveThread = null;
    public OnReceiveCallBack mCallBack = null;

    public boolean initialize(){
        mQSM2Transfer = new QSM2Transfer();
        if(!mQSM2Transfer.connect()){
            Log.d(TAG, "QSM Report : failed to initialize QSM connection");
            mIsQSMConnected = false;
            return false;
        }
        Log.d(TAG, "QSM Report : succuss to initialize");
        mIsQSMConnected = true;
        return true;
    }

    public boolean initialize(OnReceiveCallBack callBack){
        mQSM2Transfer = new QSM2Transfer();
        mCallBack = callBack;

        //cracker@yiwoosolution.co.kr
        if(DEVICE_INFO_T.TABLET == 0)
        {
            stopThread();
    
            mReceiveRunnable = new ReceiveRunnable(this);
            mReceiveThread = new Thread(mReceiveRunnable);
            mReceiveThread.start();

            if(!mQSM2Transfer.connectForReceive()){
                Log.d(TAG, "QSM Report : failed to initialize QSM connection");
                mIsQSMConnected = false;
                return false;
            }
        }
		else
		{
            if(!mQSM2Transfer.connectForReceive(mCallBack)){
                Log.d(TAG, "QSM Report : failed to initialize QSM connection");
                mIsQSMConnected = false;
                return false;
            }
		}

        Log.d(TAG, "QSM Report : succuss to initialize");
        mIsQSMConnected = true;
        return true;
    }

    public void release(){
        Log.d(TAG, "QSM Report : release");
        mQSM2Transfer.disconnect();
        mIsQSMConnected = false;

        stopThread();

        mReceiveThread = null;
        mReceiveRunnable = null;
        Log.d(TAG, "QSM Report : release end");
    }

    public int report(){
        int ret = -1;
        if(!mIsQSMConnected){
            if(!mQSM2Transfer.connect()){
                Log.d(TAG, "QSM Report : failed to reinitialize QSM connection");
                mIsQSMConnected = false;
                mReportJSON = null;
                mTransType = null;
                return -1;
            }
            mIsQSMConnected = true;
        }
        try {
            String totalString;
            if(mTransType != null) {
                totalString = mTransType;
                totalString += ":";
            }else{
                totalString = "notype:";
            }
            totalString += mReportJSON.toString();
            ret = mQSM2Transfer.sendData(totalString.getBytes("UTF-8"));
        }catch(Exception e){
            Log.e(TAG, "QSM Report : error="+e.toString());
        }
        mReportJSON = null;
        mTransType = null;
        return ret;
    }

    public void start(){
        mReportJSON = new JSONObject();
    }

    public void start(String transType){
        mReportJSON = new JSONObject();
        mTransType = transType;
        Log.d(TAG,"["+transType+"]");
    }

    public void put(String number, String data){
        if(mReportJSON == null){
            Log.d(TAG, "QSM Report : JSON object is null");
            return;
        }
        try {
            mReportJSON.put(number, data);
        }catch(JSONException ex){
            ex.printStackTrace();
        }
    }

    public void put(String number, int data){
        if(mReportJSON == null){
            Log.d(TAG, "QSM Report : JSON object is null");
            return;
        }
        try {
            mReportJSON.put(number, data);
        }catch(JSONException ex){
            ex.printStackTrace();
        }
    }

    public void put(String number, JSONObject data){
        if(mReportJSON == null){
            Log.d(TAG, "QSM Report : JSON object is null");
            return;
        }
        try {
            mReportJSON.put(number, data);
        }catch(JSONException ex){
            ex.printStackTrace();
        }
    }

    public String getReportString(){
        return mReportJSON.toString();
    }

    public QSM2Transfer getQSM2Transfer(){
        return mQSM2Transfer;
    }

    static public String getLogTime() {
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.KOREA);
        return sdf.format(date);
    }

    /* PRIVATE */
    private void stopThread() {
        //cracker@yiwoosolution.co.kr
        if(DEVICE_INFO_T.TABLET == 0)
        {
            Log.d(TAG, "QSM Report : stop previous thread");
            if (mReceiveRunnable != null) {
                mReceiveRunnable.stopThread();
                if (mReceiveThread != null && mReceiveThread.isAlive()) {
                    try {
                        mReceiveThread.join();
                    } catch (Exception e) {
                    }
                }
            }
            Log.d(TAG, "QSM Report : stop previous thread - success");
        }
    }

    public int sendRequest(int cmd){
        int ret = -1;

        try {
            String totalString;
            totalString = TRANS_TYPE.REQUEST;
            totalString += ":";
            totalString += cmd;
            ret = mQSM2Transfer.sendData(totalString.getBytes("UTF-8"));
        }catch(Exception e){
            Log.e(TAG, "QSM Report : sendRequest error="+e.toString());
        }
        return ret;
    }
}

class ReceiveRunnable implements Runnable{

    private static final String TAG = com.skb.qsm2lib.ReceiveRunnable.class.getName();
    private boolean mRun =false;
    private QSM2Report mQSM2Report;
    private InputStream mInputStream = null;
    private byte[] mReadBuffer = new byte[1024];

    public ReceiveRunnable(QSM2Report context){
        mQSM2Report = context;
        mInputStream = mQSM2Report.getQSM2Transfer().getInputStream();
        Log.d(TAG, "QSM ReceiveThread Create");
    }

    public void stopThread(){
        mRun = false;
    }

    @Override
    public void run() {
        int ret;
        Log.d(TAG, "QSM ReceiveThread: Thread start");
        mRun = true;

        while(mRun){
            try {
                if(!mQSM2Report.getQSM2Transfer().isConnected() || mInputStream == null) {
                    if(mInputStream !=null) {
                        mInputStream.close();
                        mInputStream = null;
                    }
                    if (!mQSM2Report.getQSM2Transfer().connectForReceive()) {
                        Log.d(TAG, "QSM ReceiveThread : failed to connect QSM server");
                        delayMiliTime(1000*2);
                        continue;
                    }
                    mInputStream = mQSM2Report.getQSM2Transfer().getInputStream();
                }
                if(mInputStream != null) {
                    ret = mInputStream.read(mReadBuffer);
                    Log.d(TAG, "QSM ReceiveThread: read ret="+ret);
                    if (ret == 0) {
                        Log.d(TAG, "QSM ReceiveThread: socket closed");
                        mInputStream.close();
                        mInputStream = null;
                    }else if(ret > 1){
                        if(mQSM2Report.mCallBack != null) {
                            Log.d(TAG, "QSM ReceiveThread: call callback function");
                            byte action[] = new byte[10];
                            for(int i=1; i<ret;i++){
                                action[i-1] = mReadBuffer[i];
                            }
                            mQSM2Report.mCallBack.onReceiveData(mReadBuffer[0], action, ret-1,null);
                        }
                    }else if(ret < 0){
                        mInputStream.close();
                        mInputStream = null;
                    }
                }else{
                    delayMiliTime(1000*2);
                    if(mInputStream!=null) {
                        mInputStream.close();
                        mInputStream = null;
                    }
                }
            }catch(Exception e){
                Log.wtf("QSM EXCEPTION", e);
            }
        }
        Log.d(TAG, "QSM ReceiveThread: Thread exit");
    }

    /* PRIVATE */
    private void delayMiliTime(int time){
        try {
            Thread.sleep(time);
        }catch(java.lang.InterruptedException e){}
    }
}
