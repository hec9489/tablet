#ifndef __LOGAGENT_DEF_H__
#define __LOGAGENT_DEF_H__

#ifdef __cplusplus
extern "C" {
#endif

#define	SOH	0x01	// Start Of Heading
#define	STX	0x02	// Start Of Text
#define	RS	0x1E	// Record Seperator
#define	US	0x1F	// Unit Seperator

// protocol header 15bytes: SOH(1) + STX(1) + responseYN(1) + eventcode(5) + bodyLength(7)
#define	HEADER_SIZE	15

#define	LOG_AGENT_SERVER	"127.0.0.1"
#define	LOG_AGENT_PORT		58010

#ifdef __cplusplus
}
#endif

#endif // __LOGAGENT_DEF_H__
