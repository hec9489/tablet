/*
 * Log Agent로 이벤트를 전송하는 기능을 수행
 * Create Date: 2016-07-29
 * Last Update: 2016-09-26
 * Version: 1.0
 */

#ifndef __LOGAGENT_CLI_H__
#define __LOGAGENT_CLI_H__

#ifdef __cplusplus
extern "C" {
#endif


typedef struct _EventItem {
	char *key;
	char *value;
	struct _EventItem *next;
} EventItem;


/* event code 정의
 *
 * 100: push event
 * 200: iSQMS event
 * 201: iSQMS reboot event
 * 300: UI event
 */
typedef struct _EventItemList {
	int eventcode;	// 범위: 0~99999
	EventItem *head;
	EventItem *tail;
} EventItemList;


/* 이벤트 리스트 초기화
 * return: 0=성공, -1:실패
 */
int initList(EventItemList *list, int eventcode);


/* 이벤트 리스트 메모리 해지
 */
void clearList(EventItemList *list);


/* 이벤트 리스트에 전송할 항목 구성
 * return: 0=성공, -1=실패
 */
int addItem(EventItemList *list, const char *key, const char *value);


/* Log Agent로 이벤트 전송 함수
 *
 * timeout: 이벤트 전송 후 Log Agent로 부터의 응답 대기 여부 및 대기 시간
 *   0 = 응답 대기 없이 리턴
 *   0 초과 = 지정 시간동안 대기 후 응답 없으면 리턴 (sec)
 *   iSQMS reboot 이벤트(eventcode:201)는 timeout이 0~10이 입력 되어도 최소 대기 시간 10초로 설정된다.
 *
 * return: 0 = 성공
 *        -1 = 기타에러(전송실패)
 *        -2 = 통신실패(전송실패)
 *        -3 = 전송 후 응답 timeout발생 (timeout이 0 초과값이 입력된 경우에 발생)
 *        -4 = list가 비어 있음
 *        -5 = 이벤트코드 범위초과
 *        -6 = 전송데이터 크기 초과 (최대 10000bytes)
 */
int sendEvent(EventItemList *list, int timeout);


#ifdef __cplusplus
}
#endif

#endif	// __LOGAGENT_CLI_H__
