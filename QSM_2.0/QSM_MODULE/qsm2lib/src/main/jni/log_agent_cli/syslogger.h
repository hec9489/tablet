/*
 * syslogger.h
 *
 *  Created on: 2016. 9. 19.
 *      Author: 김 정회
 */

#ifndef __SYSLOGGER_H__
#define __SYSLOGGER_H__

#include <stdio.h>

#define LOG_AGENT_LOG_TAG	"LogAgent2Lib"
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#define _ANDROID_OS_

enum LOGTYPE_ENUM {
	LOGTYPE_VERBOSE = 0,
	LOGTYPE_DEBUG,
	LOGTYPE_INFO,
	LOGTYPE_NOTICE,
	LOGTYPE_WARN,
	LOGTYPE_ERROR,
	LOGTYPE_FATAL,
	LOGYTPE_ALERT,
	LOGTYPE_EMERG,
	NUM_LOGTYPE_ENUM
};

static const char LOGTYPE_STR[NUM_LOGTYPE_ENUM][10] = {
	"Verb }",
	"Debug}",
	"Info }",
	"Noti }",
	"Warn }",
	"Error}",
	"Crit }",
	"Alert}",
	"Emerg}"
};

#define PRINT_SYSLOG(logtype, format, ...) \
		fprintf(stdout, "%s %s: %s: "format"\n", LOGTYPE_STR[logtype], __FILENAME__, __FUNCTION__, ##__VA_ARGS__)

#ifdef _DEBUG
#define	PRINT_SYSLOG_D		PRINT_SYSLOG
#endif

#ifdef _DEBUG1
#define	PRINT_SYSLOG_D1		PRINT_SYSLOG
#endif

#ifdef _DEBUG2
#define	PRINT_SYSLOG_D2		PRINT_SYSLOG
#endif

#ifndef PRINT_SYSLOG_D
#define PRINT_SYSLOG_D(x, y, ...)       ((void)(x))
#endif

#ifndef PRINT_SYSLOG_D1
#define	PRINT_SYSLOG_D1(x, y, ...)       ((void)(x))
#endif

#ifndef PRINT_SYSLOG_D2
#define	PRINT_SYSLOG_D2(x, y, ...)      ((void)(x))
#endif

#ifdef _ANDROID_OS_
	#include <android/log.h>

	static const int LOGTYPE_MAP[NUM_LOGTYPE_ENUM] = {
		ANDROID_LOG_VERBOSE,	// LOGTYPE_VERBOSE
		ANDROID_LOG_DEBUG,  	// LOGTYPE_DEBUG
		ANDROID_LOG_INFO,   	// LOGTYPE_INFO
		ANDROID_LOG_WARN,   	// LOGTYPE_NOTICE
		ANDROID_LOG_WARN,   	// LOGTYPE_WARN
		ANDROID_LOG_ERROR,  	// LOGTYPE_ERROR
		ANDROID_LOG_FATAL,  	// LOGTYPE_FATAL
		ANDROID_LOG_FATAL,  	// LOGYTPE_ALERT
		ANDROID_LOG_FATAL   	// LOGTYPE_EMERG
	};

	/* 로그가 logcat으로 출럭됨
	 * 확인 : logcat -v threadtime -s LogAgentLib
	 */
	#define WRITE_SYSLOG(logtype, format, ...) { \
			__android_log_print(LOGTYPE_MAP[logtype], LOG_AGENT_LOG_TAG, "%s: %s: "format, __FILENAME__, __FUNCTION__, ##__VA_ARGS__); \
			PRINT_SYSLOG_D(logtype, format, ##__VA_ARGS__); }
#elif defined _LEGACY_OS_
	#include <syslog.h>

	static const int LOGTYPE_MAP[NUM_LOGTYPE_ENUM] = {
		LOG_DEBUG,		// LOGTYPE_VERBOSE
		LOG_DEBUG,		// LOGTYPE_DEBUG
		LOG_INFO,		// LOGTYPE_INFO
		LOG_NOTICE,		// LOGTYPE_NOTICE
		LOG_WARNING,	// LOGTYPE_WARN
		LOG_ERR,		// LOGTYPE_ERROR
		LOG_CRIT,		// LOGTYPE_FATAL
		LOG_ALERT,		// LOGYTPE_ALERT
		LOG_EMERG		// LOGTYPE_EMERG
	};

	/* 로그가 syslog로 출력됨
	 * 확인: busybox syslogd -n
	 *     tail -f /var/log/messages | grep LogAgentLib
	 */
	#define WRITE_SYSLOG(logtype, format, ...) { \
			syslog(LOGTYPE_MAP[logtype], "%s: %s: %s: "format, LOG_AGENT_LOG_TAG, __FILENAME__, __FUNCTION__, ##__VA_ARGS__); \
			PRINT_SYSLOG_D(logtype, format, ##__VA_ARGS__); }
#else
	/* 로그가 stdout으로 출력됨
	 */
	#define WRITE_SYSLOG	PRINT_SYSLOG
#endif

#endif // __SYSLOGGER_H__
