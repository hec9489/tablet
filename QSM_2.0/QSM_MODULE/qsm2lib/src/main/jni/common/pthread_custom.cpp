/*****************************************************************
** SPTek created
******************************************************************/

#define LOG_TAG "skb_log"
#define LOG_NDEBUG 0

#include <stdio.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <stdlib.h>

#ifdef NDK_BUILD
#include <log.h>
#else
#include <utils/Log.h>
#endif

/*******************************************
* GLOBAL FUNCTIONS
********************************************/ 
int pthread_create_detatched(pthread_t *newthread, const pthread_attr_t * /*no used */, 
                      void *(*start_routine) (void *), void *arg)
{
    int ret;
    pthread_attr_t attr;

    if( pthread_attr_init(&attr) != 0 ){
        LOGI("QSM service: pthread_create_detatched-pthread_attr_init failed");
        ret = pthread_create(newthread,NULL, start_routine, (void*) arg);        			
    }else{
        if( pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED) != 0 ){
            LOGI("QSM service: pthread_create_detatched -detatch failed");
            ret = pthread_create(newthread,NULL, start_routine, (void*) arg);        
        }else{      
            ret = pthread_create(newthread,&attr, start_routine, (void*) arg);        
        }
    }
    return ret;
}
