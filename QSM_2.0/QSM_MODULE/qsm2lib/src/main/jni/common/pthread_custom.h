/*****************************************************************
** SPTek created
******************************************************************/

#ifndef PTHREAD_CUSTOM_H
#define PTHREAD_CUSTOM_H

#include <pthread.h>

#ifndef TEMP_FAILURE_RETRY
#define TEMP_FAILURE_RETRY(exp)            \
  ({                                       \
    __typeof__(exp) _rc;                   \
    do {                                   \
      _rc = (exp);                         \
    } while (_rc == -1 && errno == EINTR); \
    _rc;                                   \
  })
#endif

int pthread_create_detatched(pthread_t *newthread, const pthread_attr_t *attr, 
                      void *(*start_routine) (void *), void *arg);


#endif //PTHREAD_CUSTOM_H
