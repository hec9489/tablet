/*****************************************************************
** SPTek created
** Shared ring buffer to send user and kernel logs to remote server.
******************************************************************/

#ifndef SKB_SHARED_BUFFER_H
#define SKB_SHARED_BUFFER_H

#ifndef TEMP_FAILURE_RETRY
#define TEMP_FAILURE_RETRY(exp)            \
  ({                                       \
    __typeof__(exp) _rc;                   \
    do {                                   \
      _rc = (exp);                         \
    } while (_rc == -1 && errno == EINTR); \
    _rc;                                   \
  })
#endif

int   log_init_shared_memory(void);
void log_destory_shared_memory(void);

void log_shared_mutex_lock(void);
void log_shared_mutex_unlock(void);
int   log_shared_put_data(char* data, int size);
int   log_shared_get_data(char* data);
int   log_shared_put_qsm_data(char* data, int size);
int   log_shared_get_qsm_data(char* data);
int   log_shared_get_buffer_usage(void);
void log_shared_reset_qsm_buffer();

#endif
