/*****************************************************************
** SPTek created
** This daemon gets kernel and user messages and send them to remote server
** and get memory information and put it into user log and send it to remote server 
******************************************************************/

#define LOG_TAG "skb_qsm"
#define LOG_NDEBUG 0

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <time.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <fstream>
#include "shared_buffer.h"
#include "module_qsm_server.h"
#include <json/json.h>
#include <cutils/sockets.h>

#include <sys/system_properties.h>

#ifdef __cplusplus
extern "C" {
#include "./mqtt/src/MQTTClient.h"
}
#endif

#include "module_qsm_log_switch.h"
#include <pthread_custom.h>

#ifdef NDK_BUILD
#include <log.h>
#else
#include <utils/Log.h>
#endif

using namespace std;

/*******************************************
* DEFINE
********************************************/
#define BUFFER_SIZE 1024*4
#define MQTT_BUFFER_SIZE 1024*200
#define MAX_WAIT_TIME_IN_SECONDS 60
#define MAX_TRY_CONNECT_COUNT 3

#define SERVER_LIST_FILE
#define SEND_DATA_TO_SERVER
#define RANDOM_TIME_RECONNECT

//#define TEST_SERVER
//#define TEST_LOG_SWITCH
//#define TEST_WATERMARK

#ifdef TEST_SERVER
#define ADDRESS     "tcp://mqtt.eclipse.org:1883"
#define CLIENTID    "ExampleClientPub"
#define TOPIC          "MQTT Examples"
#define PAYLOAD     "Hello World!"
#define QOS         1
#else
#define ADDRESS     "tcp://qsmlog-dev.hanafostv.com:1883"
#define MQTT_BROKER_URL = "tcp://qsmlog-dev.hanafostv.com:1883"
#define CLIENTID    "STB"
#define USER_NAME "stb"
#define USER_PWD  "b878c2a832961389f0d5fb4e61b67a13abf2ed8bfd1667304646fcb3e12a46cf"

#define TOPIC_CHECK       "skb/btv/qsm/check"
#define TOPIC_LOG       "skb/btv/qsm/log"
//#define TOPIC_SUBSCRIBE    "skb/btv/stb/8d:8c:97:bb:ad:88"
#define TOPIC_SUBSCRIBE    "skb/btv/stb"
#define PAYLOAD     "ERROR: test"
#define QOS         0
#define QOS_SUBSCRIBE         2
#endif
#define QOS_AT_MOST_ONCE    0
#define QOS_AT_LEAST_ONCE   1
#define QOS_EXACTLY_ONCE    2

#define TIMEOUT                      10000L
#define CONNECTION_COUNT  10
#define LOG_BUF_SIZE             1024                                                                
#define MIN(a,b) a < b ? a : b

#define QSM_SOCKET_NAME "/dev/socket/qsm_server"
#define QSM_PROD_SERVER "tcp://qsmlog-live.hanafostv.com:1883"

#define MAX_SUBSCRIBE_TOPIC_CNT     4

//#define DEBUG_JSON_SEND_LOG
#ifdef DEBUG_JSON_SEND_LOG
#define LOG_SPLIT(x) \
    int len = strlen(x); \
    int pos = 0; \
    char buf[1025]; \
    char log[1024*100]; \
    bool first = true; \
    len = MIN(strlen(x), 1024*100-1); \
    strncpy(log, x, len); \
    log[len] = '\0'; \
    while(len > 0){ \
        int short_len = len; \
        if(len > 1000){ \
             short_len = 1000; \
        } \
        strncpy(buf, log+pos, short_len); \
        buf[short_len] = '\0'; \
        pos +=short_len; \
        len -=  short_len; \
        if(first){ \
            first = false; \
            LOGI("QSM : Send message =%s", buf); \
        } \
        else{ \
            LOGI("%s", buf); \
        }\
  }
#else
#define LOG_SPLIT(x)
#endif

/*******************************************
* LOCAL & GLOBAL VARIABLES
********************************************/
extern volatile bool gRun;
extern volatile bool g_setup_qsm_server_done;
extern volatile bool g_setup_qsm_send_thread_done;

static int g_socket_fd = -1;
static pthread_cond_t g_tx_condition;
static pthread_mutex_t g_tx_mutex;
static pthread_cond_t g_reconnect_condition;
static pthread_mutex_t g_reconnect_mutex;
static pthread_mutex_t g_connection_mutex;

pthread_t g_server_qsm_thread;
pthread_t g_server_qsm_get_log_thread;
pthread_t g_server_qsm_send_thread;

static MQTTClient g_mqtt_client;
static MQTTClient_deliveryToken g_delivered_token;

static int    g_init_type_offset;
bool g_init_type_received;

bool g_is_sent_log_switch_info;
bool g_is_sent_test_log_switch_info;
bool g_is_parsed_log_switch_info;
volatile bool g_is_connection_restricted;
extern pthread_mutex_t g_signature_mutex;

pthread_cond_t g_sleep_condition;
pthread_mutex_t g_sleep_mutex;
volatile bool g_sleep_status;
#ifdef RANDOM_TIME_RECONNECT
bool g_first_connection_try = true;
#endif

/* multi-connection */
static int socket_arr[10];
pthread_t g_tid[CONNECTION_COUNT];
pthread_mutex_t g_multi_socket_mutex;

static SUBSCRIBE_ITEM_T* g_subscribe_topic;
static int g_subscribe_topic_cnt;
bool g_is_sent_watermark_cmd;

/* commnad for client */
extern WATERMARK_STATUS_T g_watermark_status;

#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
extern void qsm_server_send_data_cb(char* data, int size);
#endif/*TABLET_BUILD*/

/*******************************************
* LOCAL FUNCTION
********************************************/
static int s_mqtt_start(void);
static int s_mqtt_end(void);
static int s_mqtt_subscribe(char* topic, int qos);
static int s_mqtt_publish(char* topic, char* body, int size, int qos, bool log_on = false);
static void s_qsm_init_process(char* p_received);

bool is_my_topic(char *topicName)
{
    if (g_subscribe_topic != NULL)
    {
        for (int i = 0; i < g_subscribe_topic_cnt; i++)
        {
            if (0 == strcmp(topicName, g_subscribe_topic[i].topic_name))
            {
                return true;
            }
        }
    }
    return false;
}
void release()
{
    if (g_subscribe_topic != NULL)
    {
        free(g_subscribe_topic);
        g_subscribe_topic = NULL;
    }
}

static int s_mqtt_is_connected(void)
{
    int ret;
    ret =  MQTTClient_isConnected(g_mqtt_client);
    return ret;
}

void s_mqtt_delivered(void * /*context*/, MQTTClient_deliveryToken dt)
{
    LOGI("QSM MQTT : Message with token value %d delivery confirmed\n", dt);
    g_delivered_token = dt;
}

int s_mqtt_msg_arrvd(void * /*context*/, char *topicName, int /*topicLen*/, MQTTClient_message *message)
{
    LOGI("QSM MQTT : Message arrived\n");
    LOGI("QSM MQTT : topic: %s\n", (topicName != NULL) ? topicName : "NULL");
    LOGI("QSM MQTT : message: len=%d\n", (message != NULL) ? message->payloadlen : -1);
    /*LOGI("QSM MQTT : message: len=%d : msg=%s\n"
        , (message != NULL) ? message->payloadlen : -1
        , (message != NULL) ? (char*)message->payload : "NULL");*/

    if (NULL == topicName || 0 == strlen(topicName) || NULL == message) {
        LOGE("QSM service:s_mqtt_msg_arrvd param error %p, %p", topicName, message);
        return 1;
    }

    if (!s_mqtt_is_connected()) {
        LOGE("QSM service: s_mqtt_msg_arrvd : MQTT is not connected");
        return 1;
    }

    /* Parse QSM JSON */
    if (is_my_topic(topicName)) {
        char* data = qsm_log_switch_handle_mqtt_msg((char*)message->payload);
        if (data != NULL) {
            int ret;
            if (strlen(data) < 710) {
                ret = s_mqtt_publish((char*)TOPIC_CHECK, data, strlen(data), 0, true);
            } else {
                ret = s_mqtt_publish((char*)TOPIC_CHECK, data, strlen(data), 0, false);
            }
            if (ret < 0) {
                LOGE("QSM service:send failed, %d", ret);
            }
        }
    } else {
        LOGE("Not subscribed topic : %s", topicName);
    }

    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}

void s_mqtt_connlost(void * /*context*/, char *cause)
{
    LOGI("QSM MQTT : Connection lost\n");
    LOGI("QSM MQTT : cause: %s\n", cause);

    pthread_mutex_lock(&g_tx_mutex);
    pthread_cond_signal(&g_tx_condition);
    pthread_mutex_unlock(&g_tx_mutex);

  /*pthread_mutex_lock(&g_connection_mutex);
    s_mqtt_end();
    pthread_mutex_unlock(&g_connection_mutex);*/
}

void get_mqtt_server_address(char* buf, int buf_len) {
    if (NULL == buf || buf_len <= 0) {
        return;
    }

    memset (buf, 0, buf_len);

#ifdef SERVER_LIST_FILE 
    std::ifstream infile("/data/btv_home/config/server-list.conf");
    std::string line;
    while (std::getline(infile, line)) {
        char* ptr = (char*)line.c_str();
        char* temp = NULL;
        if (strstr(ptr, "<server id=\"qsm2.0\"") != NULL) {
            temp = strstr(ptr, "address=\"");
            if (temp != NULL) {
                temp += strlen("address=\"");
                while (temp != NULL && *temp == ' ') {
                    temp++;
                }
                while (temp != NULL && *temp != '"') {
                    *buf++ = *temp++;
                }
                temp = strstr(ptr, "port=\"");
                temp += strlen("port=\"");
                if (temp != NULL) {
                    *buf++ = ':';
                    while (temp != NULL && *temp != '"') {
                        *buf++ = *temp++;
                    }
                }
            }
        }
    }
#else
    memcpy(buf, ADDRESS, strlen(ADDRESS));
#endif
}

static int s_mqtt_start(void)
{
    if(g_is_connection_restricted){
        LOGI( "QSM s_mqtt_start - g_is_connection_restricted == true");
        return -1;
    }
    
    LOGI("QSM MQTT : s_mqtt_start");
    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
    int rc;
    char subscribe_buf[50] = {0,};
    char* mac_addr = qsm_log_switch_get_mac_addr();
    char model_name[50] = {0,};

    pthread_mutex_lock(&g_connection_mutex);

    LOGI("QSM MQTT : mac_addr : %s", (mac_addr != NULL) ? mac_addr : "NULL");
    if(s_mqtt_is_connected()){
        s_mqtt_end();
    }

    char address[128] = {0,};
    get_mqtt_server_address(address, 128);
    LOGI("QSM MQTT : server addr : %s", address);
    if (0 == strlen(address)) {
        LOGE("QSM MQTT : Failed to get server address => Set Default Server\n");
        strcpy(address, QSM_PROD_SERVER);
    }
    if (NULL == mac_addr) {
        rc = MQTTClient_create(&g_mqtt_client, address, "temp_client_id",
            MQTTCLIENT_PERSISTENCE_NONE, NULL);
    } else {
        rc = MQTTClient_create(&g_mqtt_client, address, mac_addr,
            MQTTCLIENT_PERSISTENCE_NONE, NULL);
    }
    if (rc != MQTTCLIENT_SUCCESS)
    {
         LOGE("QSM MQTT : Failed to create client, return code %d\n", rc);
         pthread_mutex_unlock(&g_connection_mutex);
         return -1;
    }

    if ((rc = MQTTClient_setCallbacks(g_mqtt_client, NULL, s_mqtt_connlost, s_mqtt_msg_arrvd, s_mqtt_delivered)) != MQTTCLIENT_SUCCESS)
    {
        LOGE("QSM MQTT : Failed to set callback, return code %d\n", rc);
        pthread_mutex_unlock(&g_connection_mutex);
        return -1;
    }

    conn_opts.keepAliveInterval = 60;
    conn_opts.cleansession = 1;

    /* MQTT Connection Timeout */
    conn_opts.connectTimeout = 3;

#ifndef TEST_SERVER
    conn_opts.username = USER_NAME;
    conn_opts.password = USER_PWD;
#endif
    LOGI("QSM MQTT : call connect");
    if ((rc = MQTTClient_connect(g_mqtt_client, &conn_opts)) != MQTTCLIENT_SUCCESS)
    {
        LOGE("QSM MQTT : Failed to connect, return code %d\n", rc);
        pthread_mutex_unlock(&g_connection_mutex);
        return -1;
    }

    LOGI("QSM MQTT : connect ok");

    int i = 0;
    if (g_subscribe_topic != NULL)
    {
        free(g_subscribe_topic);
    }

#ifdef TEST_SERVER
    g_subscribe_topic_cnt = 1;
    g_subscribe_topic = (SUBSCRIBE_ITEM_T*)calloc(g_subscribe_topic_cnt, sizeof(SUBSCRIBE_ITEM_T));
    strcpy(g_subscribe_topic[i].topic_name, TOPIC_SUBSCRIBE);
    g_subscribe_topic[i].qos_type = QOS_SUBSCRIBE;
    s_mqtt_subscribe(g_subscribe_topic[i].topic_name, g_subscribe_topic[i].qos_type);
    i++;
    //s_mqtt_subscribe((char*)TOPIC, QOS);
#else
    g_subscribe_topic_cnt = 1;
    int len = __system_property_get("ro.product.model", model_name);
    if (len > 0) {
        g_subscribe_topic_cnt += 2;
    } else {
        LOGE("QSM MQTT : __system_property_get model error ");
    }
    if (mac_addr != NULL) {
        g_subscribe_topic_cnt++;
    }
    g_subscribe_topic = (SUBSCRIBE_ITEM_T*)calloc(MAX_SUBSCRIBE_TOPIC_CNT, sizeof(SUBSCRIBE_ITEM_T));
    /*"skb/btv/stb" subscribe */
    strcpy(g_subscribe_topic[i].topic_name, TOPIC_SUBSCRIBE);
    g_subscribe_topic[i].qos_type = QOS_SUBSCRIBE;
    s_mqtt_subscribe(g_subscribe_topic[i].topic_name, g_subscribe_topic[i].qos_type);
    i++;

    if (len > 0) {
        /*"skb/btv/stb/AI2" subscribe */
        memset(g_subscribe_topic[i].topic_name, 0, sizeof(g_subscribe_topic[i].topic_name));
        if (0 == strcmp(model_name, "BIP-AI100") || 0 == strcmp(model_name, "BID-AI100")) {
            sprintf(g_subscribe_topic[i].topic_name,"%s/%s", TOPIC_SUBSCRIBE, "AI2");
        }else if (0 == strcmp(model_name, "BKO-AI700")){
            sprintf(g_subscribe_topic[i].topic_name,"%s/%s", TOPIC_SUBSCRIBE, "AI");
        } else if (0 == strcmp(model_name, "BFX-AT100") || 0 == strcmp(model_name, "BFX-UA300")
                || 0 == strcmp(model_name, "BIP-UW200") || 0 == strcmp(model_name, "BID-AT200")) {
            sprintf(g_subscribe_topic[i].topic_name,"%s/%s", TOPIC_SUBSCRIBE, "Smart3");
        } else if (0 == strcmp(model_name, "BAS-AT800") || 0 == strcmp(model_name, "BKO-AT800")) {
            sprintf(g_subscribe_topic[i].topic_name,"%s/%s", TOPIC_SUBSCRIBE, "Smart2");
        } else if (0 == strcmp(model_name, "BHX-S100") || 0 == strcmp(model_name, "BKO-S200")) {
            sprintf(g_subscribe_topic[i].topic_name,"%s/%s", TOPIC_SUBSCRIBE, "Smart1");
        }else if (0 == strcmp(model_name, "BFX-UH200")){
            sprintf(g_subscribe_topic[i].topic_name,"%s/%s", TOPIC_SUBSCRIBE, "UHD4");
        } else if (0 == strcmp(model_name, "BKO-UH600") || 0 == strcmp(model_name, "BHX-UH600")) {
            sprintf(g_subscribe_topic[i].topic_name,"%s/%s", TOPIC_SUBSCRIBE, "UHD3");
        } else if (0 == strcmp(model_name, "BHX-UH400") || 0 == strcmp(model_name, "BKO-UH400")) {
            sprintf(g_subscribe_topic[i].topic_name,"%s/%s", TOPIC_SUBSCRIBE, "UHD2");
        }else if (0 == strcmp(model_name, "BHX-UH200")){
            sprintf(g_subscribe_topic[i].topic_name,"%s/%s", TOPIC_SUBSCRIBE, "UHD1");
        }else if (0 == strcmp(model_name, "BKO-UA500")){
            sprintf(g_subscribe_topic[i].topic_name,"%s/%s", TOPIC_SUBSCRIBE, "Surround");
        }
        if (strlen(g_subscribe_topic[i].topic_name) > 0) {
            g_subscribe_topic[i].qos_type = QOS_SUBSCRIBE;
            s_mqtt_subscribe(g_subscribe_topic[i].topic_name, g_subscribe_topic[i].qos_type);

            /*"skb/btv/stb/AI2/BIP(B)-AI100" subscribe */
            memset(g_subscribe_topic[i+1].topic_name, 0, sizeof(g_subscribe_topic[i+1].topic_name));
            sprintf(g_subscribe_topic[i+1].topic_name,"%s/%s",g_subscribe_topic[i].topic_name, model_name);
            g_subscribe_topic[i+1].qos_type = QOS_SUBSCRIBE;
            s_mqtt_subscribe(g_subscribe_topic[i+1].topic_name, g_subscribe_topic[i+1].qos_type);
            i += 2;
        } else {
            LOGE("QSM MQTT : invalid model : %s", model_name);
        }
    }

    if (mac_addr != NULL) {
        // ex) "skb/btv/stb/8d:8c:97:bb:ad:88"
        memset(g_subscribe_topic[i].topic_name, 0, sizeof(g_subscribe_topic[i].topic_name));
        sprintf(g_subscribe_topic[i].topic_name,"%s/%s",TOPIC_SUBSCRIBE, mac_addr);
        g_subscribe_topic[i].qos_type = QOS_SUBSCRIBE;
        s_mqtt_subscribe(g_subscribe_topic[i].topic_name, g_subscribe_topic[i].qos_type);
        i++;
    }

    LOGI("QSM MQTT : subscribe count : %d", g_subscribe_topic_cnt);

#endif // TEST_SERVER

    pthread_mutex_unlock(&g_connection_mutex);

    return 0;
}

static int s_mqtt_publish(char* topic, char* body, int size, int qos, bool log_on)
{
    MQTTClient_message pubmsg = MQTTClient_message_initializer;
    MQTTClient_deliveryToken token;
    int rc;

    pthread_mutex_lock(&g_connection_mutex);
    if(!s_mqtt_is_connected()){
        LOGI("QSM:  s_qsm_mqtt_send failed. mqtt is not connected!");
        pthread_mutex_unlock(&g_connection_mutex);
        return -1;
    }

    LOGI("QSM:  s_mqtt_publish - topic=%s, data size=%d", topic, size);

    if (log_on) {
        LOG_SPLIT(body);
    }

    pubmsg.payload = body;
    pubmsg.payloadlen = size;
    pubmsg.qos = qos;
    pubmsg.retained = 0;

#ifdef SEND_DATA_TO_SERVER  
    if ((rc = MQTTClient_publishMessage(g_mqtt_client, (char*)topic, &pubmsg, &token)) != MQTTCLIENT_SUCCESS)
    {
         LOGE("Failed to publish message, return code %d\n", rc);
         pthread_mutex_unlock(&g_connection_mutex);
         return -1;
    }

    LOGI("QSM: mqtt_publish - send data to server(data size = %d). Waiting for up to %d seconds for publication\n"
            "on topic %s for client with ClientID: %s\n",
            size, (int)(TIMEOUT/1000), (char*)topic, (char*)CLIENTID);
    
    rc = MQTTClient_waitForCompletion(g_mqtt_client, token, TIMEOUT);
    LOGI("QSM Message with delivery token %d delivered - ret : %d\n", token, rc);
#else
    LOGI("QSM: mqtt_publish - data size = %d\n", size);
#endif

    pthread_mutex_unlock(&g_connection_mutex);
    return 0;
}   

static int s_mqtt_subscribe(char* topic, int qos)
{
    int rc;
    
    if(!s_mqtt_is_connected()){
        LOGE("MQTT : s_mqtt_subscribe failed : mqtt is not connected!");
        return -1;
    }

    if ((rc = MQTTClient_subscribe(g_mqtt_client, topic, qos)) != MQTTCLIENT_SUCCESS)
    {
        LOGE("MQTT : Failed to subscribe - %s, return code %d\n", topic, rc);
        rc = EXIT_FAILURE;
        return -1;
    }
    else
    {
        LOGI("MQTT : Subscribe(%s) OK",topic);
    }
  
    return 0;
}

static int s_mqtt_unsubscribe(char* topic)
{
    int rc;
    
    if(!s_mqtt_is_connected()){
        LOGE("MQTT : s_mqtt_unsubscribe failed : mqtt is not connected!");
        return -1;
    }

    if ((rc = MQTTClient_unsubscribe(g_mqtt_client, topic)) != MQTTCLIENT_SUCCESS)
    {
        LOGE("MQTT : Failed to unsubscribe - %s, return code %d\n", topic, rc);
        return -1;
    }
    else
    {
        LOGI("MQTT : s_mqtt_unsubscribe(%s) OK",topic);     
    }
  
    return 0;
}

static int s_mqtt_end(void)
{
    int rc;    
    
    if(!s_mqtt_is_connected()){
        LOGI("QSM : s_mqtt_end - no connection status");
        return 0;
    }
    
    if ((rc = MQTTClient_disconnect(g_mqtt_client, 10000)) != MQTTCLIENT_SUCCESS){
        LOGE("QSM : MQTT Failed to disconnect, return code %d\n", rc);
        return -1;
    }else{
       LOGI("QSM : MQTT disconnect ok");
    }
    
    MQTTClient_destroy(&g_mqtt_client); 
    release();
    return 0;
}

static bool s_qsm_sleep_cond_wait(void ) {

    bool ret = false;
    
    pthread_mutex_lock(&g_sleep_mutex);
    if(g_sleep_status){
        LOGI("QSM s_qsm_sleep_cond_wait : sleep status - call mqtt end");
        s_mqtt_end();
        LOGI("QSM s_qsm_sleep_cond_wait : enter condition sleep wait mode");
        log_shared_reset_qsm_buffer();
        pthread_cond_wait(&g_sleep_condition, &g_sleep_mutex);
        LOGI("QSM s_qsm_sleep_cond_wait : exit condition sleep wait mode");
        ret = true;
    }
    pthread_mutex_unlock(&g_sleep_mutex);

    return ret;
}

static void s_qsm_tx_cond_timed_wait(void ) {
    struct timespec max_wait = {0, 0};
    
    clock_gettime(CLOCK_REALTIME, &max_wait);
    max_wait.tv_sec += MAX_WAIT_TIME_IN_SECONDS;

    pthread_mutex_lock(&g_tx_mutex);
    pthread_cond_timedwait(&g_tx_condition, &g_tx_mutex, &max_wait);
    pthread_mutex_unlock(&g_tx_mutex);
}

static int s_qsm_put_socket(int socket_num){
    int i;
    
    pthread_mutex_lock(&g_multi_socket_mutex);
    for(i=0; i<CONNECTION_COUNT; i++){
        if(socket_arr[i] == 0){
            socket_arr[i] = socket_num;
            pthread_mutex_unlock(&g_multi_socket_mutex);
            return i;
        }
    }
    pthread_mutex_unlock(&g_multi_socket_mutex);
    return -1;  
}

static int s_qsm_get_socket_index(int socket_num){
    int i;

    pthread_mutex_lock(&g_multi_socket_mutex);
    for(i=0; i<CONNECTION_COUNT; i++){
        if(socket_arr[i] == socket_num){
            pthread_mutex_unlock(&g_multi_socket_mutex);
            return i;
        }
    }
    pthread_mutex_unlock(&g_multi_socket_mutex); 
    return -1;  
}

static void s_qsm_clear_socket(int socket_num){
    int i;

    pthread_mutex_lock(&g_multi_socket_mutex);
    for(i=0; i<CONNECTION_COUNT; i++){
        if(socket_arr[i] == socket_num){
            socket_arr[i] = 0;
            LOGI("QSM service: s_qsm_clear_socket fd=%d, index=%d",socket_num, i);
            break;
        }
    }
    pthread_mutex_unlock(&g_multi_socket_mutex);
}

static void s_qsm_request_process(int cmd){

    int request = cmd - '0';
    char send_buf[100];

    switch(request){
        case REQUEST_WATERMARK:
            LOGI("QSM service: REQUEST_WATERMARK : %d, %d"
                , g_watermark_status.visibility, g_watermark_status.live);
            /*if(g_watermark_status.visibility== 0){
                char buf[10] = {0,};                                
                int size = qsm_read_file((char*)"/data/btv_home/config/watermark.conf", buf, 9);
                if (size > 1){
                    g_watermark_status.visibility = buf[0]-'0';
                    g_watermark_status.live = buf[1]-'0';                                   
                    LOGI("QSM service: watermark info visibility=%d, live=%d", g_watermark_status.visibility, g_watermark_status.live);                                     
                } else {
                    LOGE("QSM service: watermark info does not exist.");
                    return;
                }
            }*/
            send_buf[0] = SET_WATERMARK;
            send_buf[1] = g_watermark_status.visibility;
            send_buf[2] = g_watermark_status.live;
            {
				#ifdef TABLET_BUILD
                int cnt = qsm_send_command_to_client_cb(send_buf, 3);
                LOGI("QSM service: set watermark info client cb cnt : %d", cnt);
				#else
                int cnt = qsm_send_command_to_client(send_buf, 3);
                LOGI("QSM service: set watermark info client socket cnt : %d", cnt);
				#endif/**/
            }
            break;
        default:
            break;
    }
}

#ifdef RANDOM_TIME_RECONNECT
static void s_qsm_reconnect_process(void){
    if(!g_first_connection_try){
        time_t t;
        int wait_min, wait_sec;

        srand((unsigned) time(&t));
        wait_min = rand() % 60;
        wait_sec = rand() % 60;
        
        LOGI("QSM service: enter into wait status for reconnect. wait_min=%d, wait_sec=%d",wait_min, wait_sec);

        pthread_mutex_lock(&g_reconnect_mutex);
        LOGI( "QSM s_qsm_reconnect_process - set g_is_connection_restricted true");
        g_is_connection_restricted = true;

        struct timespec max_wait = {0, 0};
        clock_gettime(CLOCK_REALTIME, &max_wait);
        max_wait.tv_sec += (wait_min * 60) + wait_sec;

        pthread_cond_timedwait(&g_reconnect_condition, &g_reconnect_mutex, &max_wait);
        LOGI( "QSM s_qsm_reconnect_process - set g_is_connection_restricted false");
        g_is_connection_restricted = false;

        pthread_mutex_unlock(&g_reconnect_mutex);
        //sleep(wait_min*60+wait_sec);

        LOGI("QSM service: wait status end");
    }
    LOGI( "QSM s_qsm_reconnect_process - set g_first_connection_try false");
    g_first_connection_try = false;
}
#endif

static void* s_qsm_recv_thread_for_client(void* arg) {    

    int ret;
    char read_buf[BUFFER_SIZE];
    char type_buf[40];
    int newSocket = *((int *)arg);
    
    while(gRun){

        for (;;) {
            ret = TEMP_FAILURE_RETRY(read(newSocket, read_buf, BUFFER_SIZE-1));
            LOGI("QSM service: Multi-socket [recv_thread] read data size(socket=%d, index=%d)=%d, g_is_connection_restricted=%d"
                , newSocket, s_qsm_get_socket_index(newSocket),ret, g_is_connection_restricted);
            if (ret < 0) {
                LOGI("QSM service: Multi-socket [recv_thread] socket[%d](%d) read err : %s"
                    , s_qsm_get_socket_index(newSocket), newSocket, strerror(errno));
                goto CLOSE;
            }else if (ret == 0) {
                LOGI("QSM service: Multi-socket [recv_thread] socket[%d](%d) closed", s_qsm_get_socket_index(newSocket), newSocket);
                goto CLOSE;
            }
            
            if(ret > 0){
                memset(type_buf, 0, sizeof(type_buf));
                for(int i=0; i< ret; i++){
                    type_buf[i] = read_buf[i];
                    if(read_buf[i] == ':'){
                        type_buf[i] = '\0';
                        break;
                    }
                }
                
                /*LOGI("QSM read data - Multi-socket [recv_thread] type_buf : %s, g_is_connection_restricted : %d"
                    , type_buf, g_is_connection_restricted);*/

                /* we have to keep INIT data */
                if (g_is_connection_restricted && strcmp(type_buf,"INIT")!=0
                        && strcmp(type_buf, "SLEEP_MODE") != 0 && strcmp(type_buf,"WAKEUP_MODE") != 0){
                    continue;
                }

                if (strcmp(type_buf, "SLEEP_MODE") == 0) {
#ifdef RANDOM_TIME_RECONNECT
                    if (g_is_connection_restricted) {
                        LOGI( "QSM Multi-socket [recv_thread] g_is_connection_restricted : true - set g_sleep_status true");
                        pthread_mutex_lock(&g_sleep_mutex);
                        g_sleep_status = true;
                        pthread_mutex_unlock(&g_sleep_mutex);
                        
                        pthread_mutex_lock(&g_reconnect_mutex);
                        pthread_cond_signal(&g_reconnect_condition);
                        pthread_mutex_unlock(&g_reconnect_mutex);
                        continue;
                    }
#endif
                } else if(strcmp(type_buf,"WAKEUP_MODE")==0){
                    LOGI( "QSM Multi-socket [recv_thread] received Type is WAKEUP_MODE : wake up service - g_sleep_status : %d QSMversion : %s"
                        , g_sleep_status, QSM_VERSION);
#ifdef RANDOM_TIME_RECONNECT
                    g_first_connection_try = true;
#endif
                    pthread_mutex_lock(&g_sleep_mutex);
                    if(g_sleep_status){
                        g_sleep_status = false;
                        pthread_cond_signal(&g_sleep_condition);
                    }
                    pthread_mutex_unlock(&g_sleep_mutex);
                }
                
                LOGI("QSM [recv_thread] read data - put qsm msg : %s", type_buf);
                ret =log_shared_put_qsm_data(read_buf, ret);
                if(ret <= 0){
                    LOGI("QSM service: [recv_thread] failed to put data into buffer. ret =%d", ret);
                }else{
                    if (strcmp(type_buf, "INIT") == 0) {
                        g_init_type_received = true;
                    }
                    LOGI("QSM [recv_thread] read data - after put msg : wake up send thread");
                    /* wake up send thread */
                    pthread_mutex_lock(&g_tx_mutex);
                    pthread_cond_signal(&g_tx_condition);
                    pthread_mutex_unlock(&g_tx_mutex);
                }
            }
        }
    }

CLOSE:
    LOGI("QSM : Multi-socket [recv_thread] s_qsm_recv_thread_for_client thread(%d) end", newSocket); 
    if(newSocket > 0){
        TEMP_FAILURE_RETRY(close(newSocket));   
        s_qsm_clear_socket(newSocket);
        newSocket = 0;
    }
    
    return NULL;
}

#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
int s_qsm_recv_for_client(const char* data, int length) {    

    int ret;
    char read_buf[BUFFER_SIZE];
    char type_buf[40];

	if(length > BUFFER_SIZE) return -1;

#if 0
	memcpy(read_buf, data, length);

    memset(type_buf, 0, sizeof(type_buf));
    for(int i=0; i< ret; i++){
        type_buf[i] = read_buf[i];
        if(read_buf[i] == ':'){
            type_buf[i] = '\0';
            break;
        }
    }

    if (strcmp(type_buf, "SLEEP_MODE") == 0) {
#ifdef RANDOM_TIME_RECONNECT
        if (g_is_connection_restricted) {
            LOGI( "QSM Multi-socket [recv_thread] g_is_connection_restricted : true - set g_sleep_status true");
            pthread_mutex_lock(&g_sleep_mutex);
            g_sleep_status = true;
            pthread_mutex_unlock(&g_sleep_mutex);
            
            pthread_mutex_lock(&g_reconnect_mutex);
            pthread_cond_signal(&g_reconnect_condition);
            pthread_mutex_unlock(&g_reconnect_mutex);
            continue;
        }
#endif
    } else if(strcmp(type_buf,"WAKEUP_MODE")==0){
        LOGI( "QSM Multi-socket [recv_thread] received Type is WAKEUP_MODE : wake up service - g_sleep_status : %d QSMversion : %s"
            , g_sleep_status, QSM_VERSION);
#ifdef RANDOM_TIME_RECONNECT
        g_first_connection_try = true;
#endif
        pthread_mutex_lock(&g_sleep_mutex);
        if(g_sleep_status){
            g_sleep_status = false;
            pthread_cond_signal(&g_sleep_condition);
        }
        pthread_mutex_unlock(&g_sleep_mutex);
    }

    LOGI("QSM [recv_thread] read data - put qsm msg : %s", type_buf);
#endif

    ret =log_shared_put_qsm_data(read_buf, ret);
    if(ret <= 0){
        LOGI("QSM service: [recv_thread] failed to put data into buffer. ret =%d", ret);
    }else{
        if (strcmp(type_buf, "INIT") == 0) {
            g_init_type_received = true;
        }
        LOGI("QSM [recv_thread] read data - after put msg : wake up send thread");
        /* wake up send thread */
        pthread_mutex_lock(&g_tx_mutex);
        pthread_cond_signal(&g_tx_condition);
        pthread_mutex_unlock(&g_tx_mutex);
    }

	return ret;
}
#endif/*TABLET_BUILD*/

static void* s_qsm_get_log_thread(void* ) {

    int ret;
    char buf[2048];
    
    LOGI("QSM s_qsm_get_log_thread start");

    while(gRun){
        ret = log_shared_get_data(buf);
        if(ret > 0){
            qsm_log_switch_check_signature(buf, ret);
        }else if(ret == 0){
            sleep(1);
        }else{
            LOGE("QSM s_qsm_get_log_thread : log_shared_get_data failed");
        }
    }

    LOGI("QSM s_qsm_get_log_thread end");
    return NULL;
}

int check_mac_addr_topic() {
    int idx_mac_addr = -1;
    for (int i = 0; i < g_subscribe_topic_cnt; i++) {
        if (strlen(g_subscribe_topic[i].topic_name) == strlen(TOPIC_SUBSCRIBE) + 1 + 17) {
            bool topic_mac = true;
            char * ptr = g_subscribe_topic[i].topic_name + strlen(TOPIC_SUBSCRIBE) + 1;
            for (int j = 0; j < 17; j++) {
                char ch = *(ptr + j);
                if ((j % 3) == 2) {
                    if (ch != ':') {
                        topic_mac = false;
                        break;
                    }
                } else {
                    if (!((ch >= 'a' && ch <= 'f') || (ch >= 'A' && ch <= 'F') || (ch >= '0' && ch <= '9'))) {
                        topic_mac = false;
                        break;
                    }
                }
            }
            if (topic_mac) {
                LOGI("QSM check_mac_addr_topic - found mac address topic[%d] : %s", i, g_subscribe_topic[i].topic_name);
                idx_mac_addr = i;
                break;
            }
        }
    }
    return idx_mac_addr;
}

static void* s_qsm_send_thread(void* ) {    

    int ret, type_len, json_pos, json_len;
    char read_buf[BUFFER_SIZE];
    char* p_received, *json_data, *type;  
    int retry_count = MAX_TRY_CONNECT_COUNT;
    
    g_setup_qsm_send_thread_done = true;

    //sleep(60);
    
    LOGI("QSM s_qsm_send_thread : start");
    while(gRun){

#ifdef RANDOM_TIME_RECONNECT
        LOGI("QSM s_qsm_send_thread - g_init_type_received : %d, s_mqtt_is_connected : %d, g_sleep_status : %d, g_first_connection_try : %d"
            , g_init_type_received, s_mqtt_is_connected(), g_sleep_status, g_first_connection_try);
#else
        LOGI("QSM s_qsm_send_thread - g_init_type_received : %d, s_mqtt_is_connected : %d, g_sleep_status : %d"
            , g_init_type_received, s_mqtt_is_connected(), g_sleep_status);
#endif
        if (!g_init_type_received) {
            if (qsm_log_switch_read_init_json() < 0){
                LOGI("QSM service - send_thread : INIT type message was not received");
                s_qsm_tx_cond_timed_wait();
                continue;
            } else {
                LOGW("QSM service - send_thread : INIT type message was not received. but read from file");
            }
        }
        s_qsm_sleep_cond_wait();

        /* When mqtt is disconnected, try to connect */
        if(!s_mqtt_is_connected()){

#ifdef RANDOM_TIME_RECONNECT
            s_qsm_reconnect_process();

            if (g_sleep_status) {
                continue;
            }
            /* connect to QSM service and send TOPIC */
            if(s_mqtt_start()){
                LOGE("QSM service: s_qsm_send_thread - MQTT control thread : failed to start mqtt connection");
            }
#else
            if (retry_count > 0) {
                /* connect to QSM service and send TOPIC */
                if(s_mqtt_start()){
                    LOGE("QSM service: s_qsm_send_thread - MQTT control thread : failed to start mqtt connection - retry cnt : %d", retry_count - 1);
                    retry_count--;
                }
                sleep(5);
            } else {
                g_is_connection_restricted = true;
                LOGI("QSM service: s_qsm_send_thread - connection was restricted. we will not retry to connect to QSM server");
                sleep(3600);
            }
#endif
        }else{

#ifndef RANDOM_TIME_RECONNECT
            retry_count = MAX_TRY_CONNECT_COUNT;
#endif

            while(true){
                if(s_qsm_sleep_cond_wait()){
#ifndef RANDOM_TIME_RECONNECT
                    // need to reconnect with new  MAX_TRY_CONNECT_COUNT
                    retry_count = MAX_TRY_CONNECT_COUNT;
#endif
                    break;
                }
                
                if(!s_mqtt_is_connected()){
                    break;
                }

                qsm_log_switch_send_cold_boot();
                qsm_log_switch_check_update_status();

                json_pos = -1;

                ret = log_shared_get_qsm_data(read_buf);
                p_received = read_buf;
                read_buf[ret] = '\0';
                
                if(ret > 0){
                  
                    for(int i=0; i< ret; i++){
                        if(p_received[i] == ':'){
                            json_pos = i+1;
                            break;
                        }
                    }
                    if(json_pos < 1){
                        LOGI("QSM service - send_thread : received data is wrong");
                        continue;
                    }
                    p_received[json_pos-1] = '\0';
                    type_len = strlen(p_received);
                    LOGI("QSM service - send_thread : type of received mesage = %s(len=%d)", p_received, type_len);

                    type = p_received;
                    p_received += json_pos;
                    if(strcmp(type,"INIT")==0){
                        if(g_init_type_received){
                            LOGI("QSM service - send_thread : got INIT type again"); 
                        }else{
                            g_init_type_received = true;
                            LOGI("QSM service - send_thread : got INIT type");
                        }
                        s_qsm_init_process(p_received);
                    }else if(strncmp(type,"REQUEST",7)==0){
                        s_qsm_request_process(p_received[0]);
                    } else {
                        json_data = qsm_log_switch_parse_others(type, p_received, &json_len);
                        if (json_data != NULL) {
                            LOGI("QSM s_qsm_send_thread - call qsm_send. type : %s", type);
                            if(qsm_send(json_data, json_len, 0) < 0){
                                break;
                            }
                        } else {
                            LOGI("QSM s_qsm_send_thread - not exist valid data : skip sending event. type : %s", type);
                        }
                    }
                }else if(ret == 0){
                    //LOGI("QSM service - send_thread : s_qsm_send_thread s_qsm_tx_cond_timed_wait");
                    s_qsm_tx_cond_timed_wait();
                }else{
                    LOGE("QSM service - send_thread : log_shared_get_data failed");
                }
            }
        }
    }

    LOGI("QSM service - s_qsm_send_thread end : call s_mqtt_end");
    s_mqtt_end();
    g_setup_qsm_send_thread_done = false;
    
    LOGI("QSM MQTT : send thread end");
    return NULL;
}

static void* s_qsm_log_server(void* ) {

    int ret, data_socket, i=0;    
    
    const char socketName[] = "qsm_server";

    memset(socket_arr, 0, sizeof(int)*CONNECTION_COUNT);
    memset(g_tid, 0, sizeof(pthread_t)*CONNECTION_COUNT); 
    
    g_socket_fd = android_get_control_socket(socketName);
    if (g_socket_fd < 0) {
        LOGE("QSM: socket error=%s", strerror(errno));
        goto END;
    }

    LOGI("QSM service: listen start");

    ret = listen(g_socket_fd, CONNECTION_COUNT);
    if (ret < 0) {
        LOGE("QSM service: listen: %s", strerror(errno));        
        goto CLOSE;
    }
    LOGI("QSM: Listen OK");

    while(gRun){

        LOGI("QSM service: Multi-socket Wait for connection");
        data_socket = accept(g_socket_fd, NULL, NULL);
        if (data_socket < 0) {            
            LOGI("QSM service: Multi-socket accept failed: %s", strerror(errno));
            goto CLOSE;
        }
        LOGI("QSM service: Multi-socket Accept client");

        int index = s_qsm_put_socket(data_socket);
        if(index < 0){
            LOGI("QSM service: Multi-socket socket queue is full");
            close(data_socket);
            continue;
        }
        LOGI("QSM service: Multi-socket client accepted - index : %d, socket : %d", index, data_socket);
        pthread_create_detatched( &g_tid[index], NULL, s_qsm_recv_thread_for_client, (void*) &data_socket);        
    }

CLOSE:

    LOGI("QSM service: close log server thread");
    pthread_mutex_lock(&g_multi_socket_mutex);
    while(i < CONNECTION_COUNT){
        if(socket_arr[i]!=0){
            pthread_join(g_tid[i++],NULL);
        }
        socket_arr[i]=0;
    }
    pthread_mutex_unlock(&g_multi_socket_mutex);
    
    if(g_socket_fd > 0){
        TEMP_FAILURE_RETRY(close(g_socket_fd));
        g_socket_fd = -1;
    }

    TEMP_FAILURE_RETRY(unlink(QSM_SOCKET_NAME));    
    
END:
    LOGI("QSM service:  server end");
    g_setup_qsm_server_done = false;
    return NULL;
}


static void s_qsm_init_process(char* p_received) {
    char topic_name_org[SUBSCRIBE_TOPIC_MAX_LEN] = {0, };
    int idx_mac_address = check_mac_addr_topic();
    if (idx_mac_address == -1) {
        if (g_subscribe_topic_cnt >= 0 && g_subscribe_topic_cnt < MAX_SUBSCRIBE_TOPIC_CNT) {
            idx_mac_address = g_subscribe_topic_cnt;
            memset(g_subscribe_topic + idx_mac_address, 0, sizeof(SUBSCRIBE_ITEM_T));
        } else {
            LOGI("QSM service - send_thread : cannot find mac address topic & invalid g_subscribe_topic_cnt : %d"
                , g_subscribe_topic_cnt);
        }
    }

    int json_len = 0;
    char* json_data = qsm_log_switch_parse_INIT(p_received, &json_len);
    if (json_data && idx_mac_address != -1) {
        char* new_mac_addr = qsm_log_switch_get_mac_addr();
        if (new_mac_addr && strlen(new_mac_addr)) {
            char new_topic_name[SUBSCRIBE_TOPIC_MAX_LEN] = {0, };
            sprintf(new_topic_name, "%s/%s", TOPIC_SUBSCRIBE, new_mac_addr);
            if (0 != strcmp(g_subscribe_topic[idx_mac_address].topic_name, new_topic_name)) {
                if (strlen(g_subscribe_topic[idx_mac_address].topic_name)) {
                    s_mqtt_unsubscribe(g_subscribe_topic[idx_mac_address].topic_name);
                } else {
                    g_subscribe_topic_cnt++;
                    LOGI("QSM service - send_thread : subscribe mac address topic to add [%d] %s, cnt : %d"
                        , idx_mac_address, new_topic_name, g_subscribe_topic_cnt);
                }
                strcpy(g_subscribe_topic[idx_mac_address].topic_name, new_topic_name);
                g_subscribe_topic[idx_mac_address].qos_type = QOS_SUBSCRIBE;
                s_mqtt_subscribe(g_subscribe_topic[idx_mac_address].topic_name
                    , g_subscribe_topic[idx_mac_address].qos_type);
            } else {
                LOGI("QSM service - send_thread : already subscribing mac address topic [%d] %s"
                    , idx_mac_address, new_topic_name);
            }
        } else { // not exist valid mac address in INIT message
            LOGE("QSM s_qsm_send_thread - invalid mac address in INIT message : old index of mac : %d", idx_mac_address);
        }
    } else {
        LOGE("QSM service - send_thread : qsm_log_switch_parse_INIT FAIL");
        g_init_type_received = false;
    }
}

/*******************************************
* GLOBAL FUNCTION
********************************************/
void qsm_start_send_thread(void){
    
    LOGI("QSM: call qsm_start_control_thread");

    g_setup_qsm_send_thread_done = true;
    pthread_create_detatched( &g_server_qsm_send_thread, NULL, s_qsm_send_thread, (void*) NULL);
}

void qsm_start_log_server(void){
    
    LOGI("QSM: call qsm_start_log_server");

    qsm_log_switch_init();

    g_setup_qsm_server_done = true;
#ifndef TABLET_BUILD  //cracker@yiwoosolution.co.kr
    pthread_create_detatched( &g_server_qsm_thread, NULL, s_qsm_log_server, (void*) NULL);
#endif/*TABLET_BUILD*/
    pthread_create_detatched( &g_server_qsm_get_log_thread, NULL, s_qsm_get_log_thread, (void*) NULL);    
}

void qsm_thread_join(void){
    pthread_join(g_server_qsm_get_log_thread, NULL);    
    pthread_join(g_server_qsm_send_thread, NULL);

#ifndef TABLET_BUILD //cracker@yiwoosolution.co.kr	
    pthread_join(g_server_qsm_thread, NULL);
#endif/*TABLET_BUILD*/
}

int qsm_send(char* data, int size, int qos) {
    int ret;
    
    if (!s_mqtt_is_connected()) {
        LOGE("QSM service: qsm_send : failed to send - MQTT is not connected");
        return -1;
    }

    ret =s_mqtt_publish((char*)TOPIC_LOG, data, size, qos);
    if ( ret < 0) {
        LOGE("QSM service:send failed");
        return -1;
    }
    return 0;
}

int qsm_send_command_to_client(char* data, int size){
    int i, ret;     
    int send_count = 0;
    
    for(i=0; i<CONNECTION_COUNT; i++){
        pthread_mutex_lock(&g_multi_socket_mutex);      
        if(socket_arr[i] != 0){
            ret = TEMP_FAILURE_RETRY(write(socket_arr[i], data, size));
            pthread_mutex_unlock(&g_multi_socket_mutex);            
            //LOGI("QSM service: qsm_send_command cmd=%d, action=%d, ret=%d, socket index=%d",cmd, action, ret, i);
            if (ret < 0) {
                LOGI("QSM service: qsm_send_command write socket[%d] %d : %s(%d)"
                    , i, socket_arr[i], strerror(errno), errno);
                if (0 == strcmp(strerror(errno), "Broken pipe")) {
                    s_qsm_clear_socket(socket_arr[i]);
                }
                continue;
            }else if (ret == 0) {
                LOGI("QSM service: qsm_send_command socket[%d] %d closed", i, socket_arr[i]);
                return -1;
            }
            send_count++;
        }else{
            pthread_mutex_unlock(&g_multi_socket_mutex);
        }
    }
    return send_count;
}

#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
int qsm_send_command_to_client_cb(char* data, int size){
    int send_count = 0;
    
	qsm_server_send_data_cb(data, size);
    send_count++;
    
    return send_count;
}
#endif/*TABLET_BUILD*/

int qsm_is_connected(void){
    if (!s_mqtt_is_connected()) {
        return 0;
    }
    return 1;
}
