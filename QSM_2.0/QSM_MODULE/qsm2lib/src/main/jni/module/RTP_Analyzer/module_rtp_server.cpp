/*****************************************************************
** SPTek created
******************************************************************/

#define LOG_TAG "RTP_server"
#define LOG_NDEBUG 0

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <time.h>
#include <sys/fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>

#ifdef NDK_BUILD
#include <log.h>
#else
#include <utils/Log.h>
#endif

#include "shared_buffer.h"

using namespace std;

/*******************************************
* DEFINE
********************************************/
#define MIN(a,b) a<b ? a:b
#define BUFFER_READ_SIZE 1024*2
#define BUFFER_SIZE BUFFER_READ_SIZE+10
#define BUFFER_OFFSET 4 //"RTP:"

/*******************************************
* LOCAL VARIABLES
********************************************/
extern volatile bool g_rtp_thread_done;

/*******************************************
* LOCAL FUNCTION
********************************************/

//#define REQ_FIFO  "/data/btv_home/run/qsm_fifo"
#define REQ_FIFO    "/data/btv_home/tmp/qsm_fifo"

void sRTP_server_unlink(char *fifo)
{
    if (fifo) {
        unlink(fifo);
    }
}

int sRTP_server_init(const char *fifo)
{
    if (!fifo) {
        return -1;
    }

    return mkfifo(fifo, 0666);
}

int sRTP_server_open(const char *fifo, int flags)
{
    if (!fifo) {
        return -1;
    }

    return open(fifo, flags, 0);
}

void sRTP_server_close(int fd, const char *fifo)
{
    if (fd > 0) {
        close(fd);
    }

    if (fifo) {
        unlink(fifo);
    }
}

int sRTP_server_recv(int fd, char *buf, int len)
{
  int ret;

  if (!buf || (len <= 0) || (fd <= 0)) {
      return -1;
  }

  ret = TEMP_FAILURE_RETRY(read(fd, buf, len));
  if (ret == -1) {
      if (errno == EAGAIN) {
          ret = -2;
      } else {
          ret = -1;
      }
  }
  return ret;
}


/*******************************************
* GLOBAL FUNCTION
********************************************/
void* module_RTP_analyzer_thread(void* )
{
    int  fd = 0;
    int  ret = 0;

    fd_set rfds;
    struct timeval tv;

    char g_buf[BUFFER_SIZE] = {0,};

    g_rtp_thread_done = true;

    LOGI("RTP_analyzer : thread start");

    strcat(g_buf,"RTP:");

    sRTP_server_unlink((char*)REQ_FIFO);

    if (sRTP_server_init(REQ_FIFO) == -1) {
        LOGE("RTP_analyzer :  (%s) init failed.", (char*)REQ_FIFO);
        goto end;
    }

    fd = sRTP_server_open(REQ_FIFO, O_RDWR | O_NONBLOCK);
    if (fd == -1) {
        LOGE("RTP_analyzer : (%s) open failed.", (char*)REQ_FIFO);
        goto end;
    }

    while (1) {
        FD_ZERO(&rfds);
        FD_SET(fd, &rfds);
        tv.tv_sec = 60;
        tv.tv_usec = 0;

        ret = select(fd + 1, &rfds, NULL, NULL, NULL);

        if (ret == -1) {
            LOGE("RTP_analyzer : select failed.");
            break;
        }
        else if (ret == 0) {
            //LOGI("RTP_analyzer : No data within 60 seconds.");
            continue;
        }
        else {
            if (FD_ISSET(fd, &rfds)) {

                ret = sRTP_server_recv(fd, g_buf+BUFFER_OFFSET, BUFFER_READ_SIZE);
                if (ret > 0) {
                    g_buf[ret+BUFFER_OFFSET] = '\0';
                    // for debug and test log not release version
                    LOGI("RTP_analyzer : recv (%s) len(%d) log only for debug \n", g_buf, ret);
                    ret = log_shared_put_qsm_data(g_buf, ret+BUFFER_OFFSET);
                    if(ret <= 0){
                        LOGI("RTP_analyzer: [recv_thread] failed to put data into buffer. ret =%d", ret);
                    }
                }
                else {
                    if (ret == -2){
                        LOGW("RTP_analyzer : (%s) EWOULDBLOCK. ret(%d)", (char*)REQ_FIFO, ret);
                    }
                    else {
                        LOGE("RTP_analyzer : (%s) read failed.", (char*)REQ_FIFO);
                        break;
                    }
                }
            }
        }
    }

end:
    sRTP_server_close(fd, (char*)REQ_FIFO);
    g_rtp_thread_done = false;

    LOGI("RTP_analyzer : thread end.");
    return 0;
}

