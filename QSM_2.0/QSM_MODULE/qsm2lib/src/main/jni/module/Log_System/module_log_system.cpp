/*****************************************************************
** SPTek created
** This daemon gets kernel and user messages and send them to remote server
** and get memory information and put it into user log and send it to remote server 
******************************************************************/

#define LOG_TAG "LOG_SYSTEM"
#define LOG_NDEBUG 0

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/un.h>
#include <errno.h>

#include <time.h>

#include <shared_buffer.h>
#include <json/json.h>
#include "module_qsm_server.h"
#include "module_qsm_log_switch.h"

#ifdef NDK_BUILD
#include <log.h>
#else
#include <utils/Log.h>
#endif

#include <sys/system_properties.h>
#include <sys/wait.h>


using namespace std;

/*******************************************
* DEFINE
********************************************/
#define MIN(a,b) a<b ? a:b

#define READ   0
#define WRITE  1

#define BUFFER_SIZE 1024

/*******************************************
* GLOBAL VARIABLES
********************************************/
extern volatile bool gRun;
extern volatile bool g_is_connection_restricted;
extern volatile bool g_sleep_status;
extern volatile bool g_log_system_done;

static FILE *fp;
static pid_t pid;
static pthread_mutex_t g_mutex;

/*******************************************
* LOCAL FUNCTION
********************************************/

static FILE * popen2(string command, string type, int & pid)
{
    pid_t child_pid;
    int fd[2];
    pipe(fd);

    if((child_pid = fork()) == -1)
    {
        LOGI("QSM : Logcat thread - popen2 error (%s)\n", strerror(errno));
        return NULL;
    }

    /* child process */
    if (child_pid == 0)
    {
        if (type == "r")
        {
            TEMP_FAILURE_RETRY(close(fd[READ]));    //Close the READ end of the pipe since the child's fd is write-only
            TEMP_FAILURE_RETRY(dup2(fd[WRITE], 1)); //Redirect stdout to pipe
        }
        else
        {
            TEMP_FAILURE_RETRY(close(fd[WRITE]));    //Close the WRITE end of the pipe since the child's fd is read-only
            TEMP_FAILURE_RETRY(dup2(fd[READ], 0));   //Redirect stdin to pipe
        }

        setpgid(child_pid, child_pid); //Needed so negative PIDs can kill children of /bin/sh
        char model_name[50] = {0,};
        __system_property_get("ro.product.model", model_name);

        if (0 == strcmp(model_name, "BIP-AI100") || 0 == strcmp(model_name, "BID-AI100")||
            0 == strcmp(model_name, "BFX-AT100") || 0 == strcmp(model_name, "BFX-UA300")||
            0 == strcmp(model_name, "BIP-UW200") || 0 == strcmp(model_name, "BID-AT200")||
            0 == strcmp(model_name, "BFX-UH200"))
        {
            execl("/bin/sh", "/bin/sh", "-c", command.c_str(), NULL);
        }
        else
        {
            execl("/system/bin/busybox", "sh", "-c", "logcat", NULL);
        }
        return NULL;
    }
    else
    {
        if (type == "r")
        {
            TEMP_FAILURE_RETRY(close(fd[WRITE])); //Close the WRITE end of the pipe since parent's fd is read-only
        }
        else
        {
            TEMP_FAILURE_RETRY(close(fd[READ])); //Close the READ end of the pipe since parent's fd is write-only
        }
    }

    pid = child_pid;

    if (type == "r")
    {
        return fdopen(fd[READ], "r");
    }

    return fdopen(fd[WRITE], "w");
}

static int pclose2(FILE * fp, pid_t pid)
{
    int stat = 0;
  
    TEMP_FAILURE_RETRY(fclose(fp));

    if(pid > 0){      
        while (waitpid(pid, &stat, 0) == -1)
        {
            if (errno != EINTR)
            {
                stat = -1;
                break;
            }
        }
    }
    return stat;
}

static void slogcat_release(void){
    pthread_mutex_lock(&g_mutex); 
    if(pid > 0){
        kill(pid, SIGTERM/*SIGKILL*/);
    }
  
    if(fp){ 
        int ret =TEMP_FAILURE_RETRY(pclose2(fp, pid));
        //LOGI("QSM : Logcat state is %d(%s)\n", ret, strerror(errno));

        fp = NULL;
    }
    pid = -1;   
    pthread_mutex_unlock(&g_mutex);
}

static void slogcat_sigpipe_handler(int /*unused*/)
{
    slogcat_release();
}

bool need_logcat_log_parsing_at_old_os(char* model_name) {
    bool need_parsing = false;

    // need check more model
    if ( 0 == strcmp(model_name, "BKO-UH400") || 0 == strcmp(model_name, "BHX-UH400")
         || 0 == strcmp(model_name, "BHX-UH200") ) {
        need_parsing = true;
    }

    return need_parsing;
}

/*******************************************
* GLOBAL FUNCTION
********************************************/
void* module_log_system_thread(void* ) {

    int ret, len;
    char buff[BUFFER_SIZE];

    g_log_system_done = true;
  
    slogcat_release();
  
    fp = NULL;
    pid = -1;
    
    LOGI("QSM : Logcat thread - wait for qsm mqtt connection");
    while(!qsm_is_connected()){
          sleep(10);
          if(!gRun){
              LOGI("QSM : Logcat thread end - gRun is false");
              return NULL;
          }
    }
    LOGI("QSM : Logcat thread - start thread");

    signal(SIGPIPE, slogcat_sigpipe_handler);
    
    while(gRun){

        //fp = popen2("logcat -b all 2>/dev/null", "r", pid);
        fp = popen2("logcat 2>/dev/null", "r", pid);
        if(fp == NULL || pid < 0)
        {
            slogcat_release();
            LOGW("QSM : Logcat thread end - popen failed. thread end");
            return NULL;
        }
        LOGI("QSM : Logcat start thread popen success : %d", pid);

        char model_name[50] = {0,};
        __system_property_get("ro.product.model", model_name);
        //LOGI("QSM : Logcat start thread __system_property_get model_name[%s]", model_name); // only test log

        buff[0]='\0';   
        while(fp && fgets(buff, BUFFER_SIZE, fp) != NULL)
        {
            char* str_msg=NULL;
            char* str;

            if(g_is_connection_restricted || !qsm_log_switch_is_signature_on() || !gRun
                || !qsm_is_connected() || g_sleep_status){
                LOGI("QSM : Logcat thread - connection was restricted(%d) or sleep status(%d) or signature on(%d) or qsm_is_connected(%d) or gRun(%d). this thread will be terminated, pid=%d"
                    , g_is_connection_restricted, g_sleep_status, qsm_log_switch_is_signature_on()
                    , qsm_is_connected(), gRun, pid);
                goto THREAD_END;
            }

            buff[BUFFER_SIZE-1] = '\0';
            if(strlen(buff) <= 0) continue;

            str = strstr(buff, ":");
            if(str){
                char* str_second = strstr(str+1, ":");

                if (need_logcat_log_parsing_at_old_os(model_name) == true) {
                    str_second = str;
                }

                if(str_second){
                    str_msg = strstr(str_second+1, ":");

                    if (need_logcat_log_parsing_at_old_os(model_name) == true) {
                        str_msg = str;
                    }

                    if(!str_msg){
                        continue;
                    }
                    
                    str_msg++; //':'
                    
                    len = strlen(str_msg);                    
                    while(len > 0){
                        char tmp = *str_msg;
             
                        if(tmp == ' '){
                            str_msg++;
                            len--;
                            continue;
                        }
                        else if(tmp == '\0'){
                         len = 0;
                            break;
                        }
                        break;
                    }
                    
                    if(len <= 0){
                        continue;
                    }
                }else{
                    continue;
                }
            }else{
                continue;
            }

            ret = log_shared_put_data(str_msg, strlen(str_msg));
            if(ret==0){
                //LOGW("QSM : Logcat buffer is full");
            }

            buff[0]='\0';
        }

        slogcat_release();
        //LOGI("QSM :  Logcat state is %d(%s)\n", ret, strerror(errno));

        sleep(5);
        LOGW("QSM : Logcat thread - logcat again");
    }

THREAD_END:

    g_log_system_done = false;
    slogcat_release();
    
    LOGI("QSM : Logcat thread end");
    return NULL;
}

