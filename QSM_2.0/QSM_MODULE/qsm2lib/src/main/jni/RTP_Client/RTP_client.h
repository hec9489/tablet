
#ifndef RTP_CLIENT_H
#define RTP_CLIENT_H

/*****************************************
DEFINE
******************************************/
#define SUCCESS                     0
#define ERROR_IO                    -1
#define ERROR_WOULDBLOCK            -2
#define ERROR_FD                    -3
#define ERROR_PARAM                 -4
#define ERROR_SHORT_RTP_LENGTH    -100
#define ERROR_WRONG_RTP_PACKET    -101

/*****************************************
GLOBAL FUNCTIONS
******************************************/
#ifdef __cplusplus
extern "C" {
#endif

int RTP_client_init(void);
int RTP_client_send(char *buf, int len);
int RTP_client_release(void);

#ifdef __cplusplus
}
#endif
#endif //RTP_CLIENT_H
