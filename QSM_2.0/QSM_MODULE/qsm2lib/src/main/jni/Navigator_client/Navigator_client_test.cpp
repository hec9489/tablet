
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/socket.h>
#include <errno.h>
#include <linux/socket.h>
#include <time.h>

#include "Navigator_client.h"

int main(int argc __unused, char **argv __unused)
{
    int count = 60;
	
    printf("Navigator client test start\n");

    if(Navigator_client_init() < 0){
        printf("Navigator client : Navigator_client_init failed\n");
        return 0;  
    }

    while(count-- > 0){
        if(Navigator_client_send((char*)"Navigator Client data", sizeof("Navigator Client data")) < 0){
            printf("Navigator client : Navigator_client_send -failed\n");
        }else{
            printf("Navigator client : Navigator_client_send -success(%d)\n",count);
        }
        sleep(3);
    }
   
    Navigator_client_release();
    printf("Navigator client  finished\n");
    return 0;
}
