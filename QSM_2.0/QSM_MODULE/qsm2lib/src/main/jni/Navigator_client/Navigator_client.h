
#ifndef NAVIGATOR_CLIENT_H
#define NAVIGATOR_CLIENT_H

/*****************************************
GLOBAL FUNCTIONS
******************************************/
int   Navigator_client_init(void);
int   Navigator_client_send(char* data, int size);
void Navigator_client_release(void);

#endif //NAVIGATOR_CLIENT_H