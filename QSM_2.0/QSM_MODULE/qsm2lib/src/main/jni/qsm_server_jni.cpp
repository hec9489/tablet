/*****************************************************************
** SPTek created
** This daemon gets kernel and user messages and send them to remote server
** and get memory information and put it into user log and send it to remote server 
******************************************************************/

#define LOG_TAG "QSM Server"
#define LOG_NDEBUG 0

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/socket.h>
#include <errno.h>

#include <netlink/netlink.h>
#include <netlink/utils.h>
#include <netlink/addr.h>
#include <netlink/attr.h>
#include <netlink/msg.h>
#include <linux/socket.h>

#include <netlink-private/object-api.h>
#include <netlink-private/types.h>
#include <netlink/socket.h>
#include <time.h>

#include <sys/system_properties.h>
#include <netinet/in.h> 
#include <arpa/inet.h> 

#include "shared_buffer.h"
#include <json/json.h>
#include "module_qsm_server.h"
#include "module_qsm_log_switch.h"
#include "module_rtp_server.h"

#ifdef NDK_BUILD
#include <log.h>
#else
#include <utils/Log.h>
#endif

#include <pthread_custom.h>

#include <jni.h>
#include "utils/misc.h"

using namespace std;

/*******************************************
* DEFINE
********************************************/
#define MAX_PAYLOAD 1024
#define MAX_TCP_PAYLOAD MAX_PAYLOAD*2
//#define PORT 8443 //2000
//#define IP "210.217.178.234" //"192.168.0.147"
#define MIN(a,b) a<b ? a:b
#define ANDROID_LOG_MEM_INFO 20
#define WAIT_FOR_BUFFER

#define KERNEL_LOG_INTO_USER_LOG
//#define ERROR_FATAL_LOG_INTO_KERNEL_LOG

//#define SEND_SIGNATURE_TEST_MESSAGE

#define WAIT_TIME 60

enum{
 KERN_EMERG=0,
 KERN_ALERT,
 KERN_CRIT,
 KERN_ERR,
 KERN_WARNING,
 KERN_NOTICE,
 KERN_INFO,
 KERN_DEBUG
};

/*******************************************
* GLOBAL VARIABLES
********************************************/

volatile bool gRun;

volatile bool g_setup_qsm_server_done;
volatile bool g_setup_qsm_send_thread_done;
volatile bool g_log_system_done;
volatile bool g_rtp_thread_done;

static pthread_t gLogcatThread;
static pthread_t gRTPServerThread;

struct sockaddr_nl src_addr, dest_addr;
struct nlmsghdr *nlh = NULL;
struct iovec iov;
int sock_fd;
struct msghdr msg;

#ifdef KERNEL_LOG_INTO_USER_LOG
static pthread_t gKernelLogSystemThread;
volatile bool g_kernel_log_system_thread_done;
#endif

extern volatile bool g_sleep_status;
extern volatile bool g_is_connection_restricted;

static const char *classQsm2Transfer = "com/skb/qsm2lib/QSM2Transfer";

static JavaVM* gJavaVM = NULL;

JNIEnv *Qsm_env;
jobject Qsm_obj;

/*******************************************
* LOCAL FUNCTION
********************************************/
extern void* module_log_system_thread(void* );

#ifdef KERNEL_LOG_INTO_USER_LOG
extern void* module_kernel_log_system_thread(void* );
extern void module_stop_kernel_log_system_thread(void);
#endif

void qsm_server_send_data_cb(char* data, int size)
{
    static jmethodID cb = NULL;
	jstring javaString = Qsm_env->NewStringUTF(data);
    jclass cls = Qsm_env->GetObjectClass(Qsm_obj);
    
    if(cb == NULL)
    { 
      cb = Qsm_env->GetMethodID(cls, "readFromJniCallBack", "(Ljava/lang/String;I)V"); 
      if(cb == NULL) return;
    } 
    
    LOGI("qsm_server_send_data_cb");
    Qsm_env->CallVoidMethod(Qsm_obj, cb, javaString, (jint)size); 
}

static jint qsm_server_init(JNIEnv *env, jobject obj)
{
#ifdef KERNEL_LOG_INTO_USER_LOG
    char value[PROP_VALUE_MAX];
    int debug;
#endif

    LOGI("SKB QSM log server start QSMversion : %s", QSM_VERSION);
    
    if(log_init_shared_memory() < 0){
        LOGE("SKB QSM log server : log_init_shared_memory failed");
        return -1;
    }

	Qsm_env = env;
	Qsm_obj = obj;

    gRun = true;

    if (qsm_rtp_checker_on() == true){
        qsm_init_zero_rtp_send_interval();
        qsm_clear_rtp_status();
    }

    /* When control thread was died, try to restart it */
    if(!g_setup_qsm_send_thread_done){
        qsm_start_send_thread();
    }

    /* When log server thread was died, try to restart it */
    if(!g_setup_qsm_server_done){
        qsm_start_log_server();
    }
	
    if(!g_sleep_status && !g_is_connection_restricted){
        qsm_log_switch_check_system_info();
        log_shared_get_buffer_usage();
        qsm_log_switch_send_info_after_one_day();

        if (qsm_rtp_checker_on() == true && qsm_log_switch_is_Rtp_on() == true){
            qsm_send_rtp_status();
            if(!g_rtp_thread_done){
                pthread_create_detatched(&gRTPServerThread, NULL, module_RTP_analyzer_thread, NULL);
            }
        }

        if (qsm_log_switch_is_signature_on() && qsm_is_connected()) {
            if(!g_log_system_done){
                pthread_create_detatched(&gLogcatThread, NULL, module_log_system_thread, NULL);
            }

#ifdef KERNEL_LOG_INTO_USER_LOG
            LOGI("QSM module_kernel_log_system_thread : g_kernel_log_system_thread_done=%d"
                , g_kernel_log_system_thread_done);
            if(!g_kernel_log_system_thread_done){
                pthread_create_detatched(&gKernelLogSystemThread, NULL, module_kernel_log_system_thread, NULL);
            }
#endif
        }
    }

#ifdef SEND_SIGNATURE_TEST_MESSAGE
    //LOGE("Background color: [0]     Background opacity: [0]  ''");
    LOGE("RPC 1916VDEC:ClearPort takes too much time (> 5000 mS)");
    //LOGE("RX DMA FULL 1859 - ethernet/marvell/mvneta.c");
#endif

    return 0;
}

static jint qsm_server_release(JNIEnv *env, jobject obj)
{
    LOGI("SKB QSM log server - end of main loop");
    gRun = false;

    pthread_join(gRTPServerThread, NULL);
    pthread_join(gLogcatThread, NULL);
    qsm_thread_join();

    log_destory_shared_memory();  
	
    LOGI("SKB QSM log server finished");
    return 0;
}

static jint qsm_server_send_data(JNIEnv *env, jobject obj, jstring dataJSON)
{
    int ret;
	
    if (dataJSON == NULL)
    {
        LOGI("[%s][%d] IllegalArgumentException", __FUNCTION__, __LINE__);
        return -1;
    }

    const char *tmp = env->GetStringUTFChars(dataJSON, NULL);
    int size = env->GetStringUTFLength(dataJSON);
    if ((tmp == NULL) || (size == 0))
    {
        return -1;
    }

	ret = s_qsm_recv_for_client(tmp, size);

    env->ReleaseStringUTFChars(dataJSON, tmp);
    tmp = NULL;	

	return ret;
}

static JNINativeMethod gMethods[] = {
        {"native_init", "()I", (void*)qsm_server_init},
        {"native_release", "()I", (void*)qsm_server_release},
        {"sendToserver", "(Ljava/lang/String;)I", (void*)qsm_server_send_data}
};

jint JNI_OnLoad(JavaVM* vm, void* /* reserved */)
{
    JNIEnv* env = NULL;
    jint result = -1;

    LOGI("[%s] called", __func__);

    if (vm->GetEnv((void**)&env, JNI_VERSION_1_6) != JNI_OK)
    {
        LOGI("[%s][%d] GetEnv falied", __func__, __LINE__);
        return JNI_FALSE;
    }

    jclass clazz = env->FindClass(classQsm2Transfer);
    if (clazz == NULL) {
        LOGI("[%s][%d] Failed to find %s", __func__, __LINE__, classQsm2Transfer);
        return JNI_FALSE;
    }

    if (env->RegisterNatives(clazz, gMethods, NELEM(gMethods)) < 0) {
        LOGI("[%s][%d] Failed to QSM Server native registration", __func__, __LINE__);
        return JNI_FALSE;
    }

    gJavaVM = vm;

    result = JNI_VERSION_1_6;
    return result;
}

