LOCAL_PATH := $(call my-dir)

# ================================================
# MQTT lib
# ================================================

include $(CLEAR_VARS)
mqtt_path := module/QSM/mqtt/src
mqtt_includes := $(LOCAL_PATH)/module/QMSmqtt/src \
    $(LOCAL_PATH)/include

#external/boringssl/include \

mqtt_common := \
	$(mqtt_path)/MQTTProtocolClient.c \
	$(mqtt_path)/Tree.c \
	$(mqtt_path)/Heap.c \
	$(mqtt_path)/MQTTPacket.c \
	$(mqtt_path)/Clients.c \
	$(mqtt_path)/Thread.c \
	$(mqtt_path)/utf-8.c \
	$(mqtt_path)/StackTrace.c \
	$(mqtt_path)/MQTTProtocolOut.c \
	$(mqtt_path)/Socket.c \
	$(mqtt_path)/Log.c \
	$(mqtt_path)/Messages.c \
	$(mqtt_path)/LinkedList.c \
	$(mqtt_path)/MQTTPersistence.c \
	$(mqtt_path)/MQTTPacketOut.c \
	$(mqtt_path)/SocketBuffer.c \
	$(mqtt_path)/MQTTPersistenceDefault.c \
	$(mqtt_path)/MQTTProperties.c \
	$(mqtt_path)/MQTTTime.c \
	$(mqtt_path)/WebSocket.c \

mqtt_src := \
	$(mqtt_path)/MQTTClient.c \
	$(mqtt_path)/SSLSocket.c \
	$(mqtt_path)/Base64.c \
	$(mqtt_path)/SHA1.c \

include $(CLEAR_VARS)
LOCAL_MODULE    := mqtt_static
#LOCAL_VENDOR_MODULE := true
#LOCAL_SHARED_LIBRARIES := libqsm_ssl
LOCAL_WHOLE_STATIC_LIBRARIES := crypto_static ssl_static
LOCAL_LDLIBS := -ldl
LOCAL_EXPORT_C_INCLUDE_DIRS := $(mqtt_path)
LOCAL_C_INCLUDES:= $(mqtt_includes)
LOCAL_CFLAGS += -DOPENSSL -DHIGH_PERFORMANCE
LOCAL_SRC_FILES :=  $(mqtt_common) $(mqtt_src)
include $(BUILD_STATIC_LIBRARY)

# ================================================
# icuuc
# ================================================

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
        common/icu4c/source/common/appendable.cpp \
        common/icu4c/source/common/bmpset.cpp \
        common/icu4c/source/common/brkeng.cpp \
        common/icu4c/source/common/brkiter.cpp \
        common/icu4c/source/common/bytesinkutil.cpp \
        common/icu4c/source/common/bytestream.cpp \
        common/icu4c/source/common/bytestriebuilder.cpp \
        common/icu4c/source/common/bytestrie.cpp \
        common/icu4c/source/common/bytestrieiterator.cpp \
        common/icu4c/source/common/caniter.cpp \
        common/icu4c/source/common/chariter.cpp \
        common/icu4c/source/common/charstr.cpp \
        common/icu4c/source/common/cmemory.cpp \
        common/icu4c/source/common/cstr.cpp \
        common/icu4c/source/common/cstring.cpp \
        common/icu4c/source/common/cwchar.cpp \
        common/icu4c/source/common/dictbe.cpp \
        common/icu4c/source/common/dictionarydata.cpp \
        common/icu4c/source/common/dtintrv.cpp \
        common/icu4c/source/common/edits.cpp \
        common/icu4c/source/common/filteredbrk.cpp \
        common/icu4c/source/common/filterednormalizer2.cpp \
        common/icu4c/source/common/icudataver.cpp \
        common/icu4c/source/common/icuplug.cpp \
        common/icu4c/source/common/listformatter.cpp \
        common/icu4c/source/common/loadednormalizer2impl.cpp \
        common/icu4c/source/common/locavailable.cpp \
        common/icu4c/source/common/locbased.cpp \
        common/icu4c/source/common/locdispnames.cpp \
        common/icu4c/source/common/locdspnm.cpp \
        common/icu4c/source/common/locid.cpp \
        common/icu4c/source/common/loclikely.cpp \
        common/icu4c/source/common/locmap.cpp \
        common/icu4c/source/common/locresdata.cpp \
        common/icu4c/source/common/locutil.cpp \
        common/icu4c/source/common/messagepattern.cpp \
        common/icu4c/source/common/normalizer2.cpp \
        common/icu4c/source/common/normalizer2impl.cpp \
        common/icu4c/source/common/normlzr.cpp \
        common/icu4c/source/common/parsepos.cpp \
        common/icu4c/source/common/patternprops.cpp \
        common/icu4c/source/common/pluralmap.cpp \
        common/icu4c/source/common/propname.cpp \
        common/icu4c/source/common/propsvec.cpp \
        common/icu4c/source/common/punycode.cpp \
        common/icu4c/source/common/putil.cpp \
        common/icu4c/source/common/rbbi_cache.cpp \
        common/icu4c/source/common/rbbi.cpp \
        common/icu4c/source/common/rbbidata.cpp \
        common/icu4c/source/common/rbbinode.cpp \
        common/icu4c/source/common/rbbirb.cpp \
        common/icu4c/source/common/rbbiscan.cpp \
        common/icu4c/source/common/rbbisetb.cpp \
        common/icu4c/source/common/rbbistbl.cpp \
        common/icu4c/source/common/rbbitblb.cpp \
        common/icu4c/source/common/resbund_cnv.cpp \
        common/icu4c/source/common/resbund.cpp \
        common/icu4c/source/common/resource.cpp \
        common/icu4c/source/common/ruleiter.cpp \
        common/icu4c/source/common/schriter.cpp \
        common/icu4c/source/common/serv.cpp \
        common/icu4c/source/common/servlk.cpp \
        common/icu4c/source/common/servlkf.cpp \
        common/icu4c/source/common/servls.cpp \
        common/icu4c/source/common/servnotf.cpp \
        common/icu4c/source/common/servrbf.cpp \
        common/icu4c/source/common/servslkf.cpp \
        common/icu4c/source/common/sharedobject.cpp \
        common/icu4c/source/common/simpleformatter.cpp \
        common/icu4c/source/common/stringpiece.cpp \
        common/icu4c/source/common/stringtriebuilder.cpp \
        common/icu4c/source/common/uarrsort.cpp \
        common/icu4c/source/common/ubidi.cpp \
        common/icu4c/source/common/ubidiln.cpp \
        common/icu4c/source/common/ubidi_props.cpp \
        common/icu4c/source/common/ubiditransform.cpp \
        common/icu4c/source/common/ubidiwrt.cpp \
        common/icu4c/source/common/ubrk.cpp \
        common/icu4c/source/common/ucase.cpp \
        common/icu4c/source/common/ucasemap.cpp \
        common/icu4c/source/common/ucasemap_titlecase_brkiter.cpp \
        common/icu4c/source/common/ucat.cpp \
        common/icu4c/source/common/uchar.cpp \
        common/icu4c/source/common/ucharstriebuilder.cpp \
        common/icu4c/source/common/ucharstrie.cpp \
        common/icu4c/source/common/ucharstrieiterator.cpp \
        common/icu4c/source/common/uchriter.cpp \
        common/icu4c/source/common/ucln_cmn.cpp \
        common/icu4c/source/common/ucmndata.cpp \
        common/icu4c/source/common/ucnv2022.cpp \
        common/icu4c/source/common/ucnv_bld.cpp \
        common/icu4c/source/common/ucnvbocu.cpp \
        common/icu4c/source/common/ucnv_cb.cpp \
        common/icu4c/source/common/ucnv_cnv.cpp \
        common/icu4c/source/common/ucnv.cpp \
        common/icu4c/source/common/ucnv_ct.cpp \
        common/icu4c/source/common/ucnvdisp.cpp \
        common/icu4c/source/common/ucnv_err.cpp \
        common/icu4c/source/common/ucnv_ext.cpp \
        common/icu4c/source/common/ucnvhz.cpp \
        common/icu4c/source/common/ucnv_io.cpp \
        common/icu4c/source/common/ucnvisci.cpp \
        common/icu4c/source/common/ucnvlat1.cpp \
        common/icu4c/source/common/ucnv_lmb.cpp \
        common/icu4c/source/common/ucnvmbcs.cpp \
        common/icu4c/source/common/ucnvscsu.cpp \
        common/icu4c/source/common/ucnvsel.cpp \
        common/icu4c/source/common/ucnv_set.cpp \
        common/icu4c/source/common/ucnv_u16.cpp \
        common/icu4c/source/common/ucnv_u32.cpp \
        common/icu4c/source/common/ucnv_u7.cpp \
        common/icu4c/source/common/ucnv_u8.cpp \
        common/icu4c/source/common/ucol_swp.cpp \
        common/icu4c/source/common/ucurr.cpp \
        common/icu4c/source/common/udata.cpp \
        common/icu4c/source/common/udatamem.cpp \
        common/icu4c/source/common/udataswp.cpp \
        common/icu4c/source/common/uenum.cpp \
        common/icu4c/source/common/uhash.cpp \
        common/icu4c/source/common/uhash_us.cpp \
        common/icu4c/source/common/uidna.cpp \
        common/icu4c/source/common/uinit.cpp \
        common/icu4c/source/common/uinvchar.cpp \
        common/icu4c/source/common/uiter.cpp \
        common/icu4c/source/common/ulist.cpp \
        common/icu4c/source/common/ulistformatter.cpp \
        common/icu4c/source/common/uloc.cpp \
        common/icu4c/source/common/uloc_keytype.cpp \
        common/icu4c/source/common/uloc_tag.cpp \
        common/icu4c/source/common/umapfile.cpp \
        common/icu4c/source/common/umath.cpp \
        common/icu4c/source/common/umutex.cpp \
        common/icu4c/source/common/unames.cpp \
        common/icu4c/source/common/unifiedcache.cpp \
        common/icu4c/source/common/unifilt.cpp \
        common/icu4c/source/common/unifunct.cpp \
        common/icu4c/source/common/uniset_closure.cpp \
        common/icu4c/source/common/uniset.cpp \
        common/icu4c/source/common/uniset_props.cpp \
        common/icu4c/source/common/unisetspan.cpp \
        common/icu4c/source/common/unistr_case.cpp \
        common/icu4c/source/common/unistr_case_locale.cpp \
        common/icu4c/source/common/unistr_cnv.cpp \
        common/icu4c/source/common/unistr.cpp \
        common/icu4c/source/common/unistr_props.cpp \
        common/icu4c/source/common/unistr_titlecase_brkiter.cpp \
        common/icu4c/source/common/unormcmp.cpp \
        common/icu4c/source/common/unorm.cpp \
        common/icu4c/source/common/uobject.cpp \
        common/icu4c/source/common/uprops.cpp \
        common/icu4c/source/common/uresbund.cpp \
        common/icu4c/source/common/ures_cnv.cpp \
        common/icu4c/source/common/uresdata.cpp \
        common/icu4c/source/common/usc_impl.cpp \
        common/icu4c/source/common/uscript.cpp \
        common/icu4c/source/common/uscript_props.cpp \
        common/icu4c/source/common/uset.cpp \
        common/icu4c/source/common/usetiter.cpp \
        common/icu4c/source/common/uset_props.cpp \
        common/icu4c/source/common/ushape.cpp \
        common/icu4c/source/common/usprep.cpp \
        common/icu4c/source/common/ustack.cpp \
        common/icu4c/source/common/ustrcase.cpp \
        common/icu4c/source/common/ustrcase_locale.cpp \
        common/icu4c/source/common/ustr_cnv.cpp \
        common/icu4c/source/common/ustrenum.cpp \
        common/icu4c/source/common/ustrfmt.cpp \
        common/icu4c/source/common/ustring.cpp \
        common/icu4c/source/common/ustr_titlecase_brkiter.cpp \
        common/icu4c/source/common/ustrtrns.cpp \
        common/icu4c/source/common/ustr_wcs.cpp \
        common/icu4c/source/common/utext.cpp \
        common/icu4c/source/common/utf_impl.cpp \
        common/icu4c/source/common/util.cpp \
        common/icu4c/source/common/util_props.cpp \
        common/icu4c/source/common/utrace.cpp \
        common/icu4c/source/common/utrie2_builder.cpp \
        common/icu4c/source/common/utrie2.cpp \
        common/icu4c/source/common/utrie.cpp \
        common/icu4c/source/common/uts46.cpp \
        common/icu4c/source/common/utypes.cpp \
        common/icu4c/source/common/uvector.cpp \
        common/icu4c/source/common/uvectr32.cpp \
        common/icu4c/source/common/uvectr64.cpp \
        common/icu4c/source/common/wintz.cpp \
        common/icu4c/source/stubdata/stubdata.cpp

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/include \
    $(LOCAL_PATH)/common/icu4c/source/common

LOCAL_MODULE:= icuuc_static
LOCAL_CLANG := false

LOCAL_CPPFLAGS := -Werror \
                -D_REENTRANT \
                -DU_COMMON_IMPLEMENTATION \
                -O3 \
                -fvisibility=hidden \
                -Wno-sign-compare \
                -Wno-unused-function \
                -Wno-unused-parameter \
                -Wno-missing-field-initializers \
                -Wno-deprecated-declarations \
                -static-libstdc++ \
                -fexceptions \
                -frtti \
                -fPIC \
                -DPIC

LOCAL_CFLAGS := -Werror -fPIC -DPIC

include $(BUILD_STATIC_LIBRARY)

# ================================================
# XML2
# ================================================

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
        common/libxml2/SAX.c \
        common/libxml2/entities.c \
        common/libxml2/encoding.c \
        common/libxml2/error.c \
        common/libxml2/parserInternals.c \
        common/libxml2/parser.c \
        common/libxml2/tree.c \
        common/libxml2/hash.c \
        common/libxml2/list.c \
        common/libxml2/xmlIO.c \
        common/libxml2/xmlmemory.c \
        common/libxml2/uri.c \
        common/libxml2/valid.c \
        common/libxml2/xlink.c \
        common/libxml2/debugXML.c \
        common/libxml2/xpath.c \
        common/libxml2/xpointer.c \
        common/libxml2/xinclude.c \
        common/libxml2/DOCBparser.c \
        common/libxml2/catalog.c \
        common/libxml2/globals.c \
        common/libxml2/threads.c \
        common/libxml2/c14n.c \
        common/libxml2/xmlstring.c \
        common/libxml2/buf.c \
        common/libxml2/xmlregexp.c \
        common/libxml2/xmlschemas.c \
        common/libxml2/xmlschemastypes.c \
        common/libxml2/xmlunicode.c \
        common/libxml2/xmlreader.c \
        common/libxml2/relaxng.c \
        common/libxml2/dict.c \
        common/libxml2/SAX2.c \
        common/libxml2/xmlwriter.c \
        common/libxml2/legacy.c \
        common/libxml2/chvalid.c \
        common/libxml2/pattern.c \
        common/libxml2/xmlsave.c \
        common/libxml2/xmlmodule.c \
        common/libxml2/schematron.c

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/include \
    $(LOCAL_PATH)/include/libxml \
    $(LOCAL_PATH)/common/libxml2/include

LOCAL_MODULE:= xml2_static
LOCAL_CLANG := false

LOCAL_CFLAGS := -Werror \
                -DLIBXML_THREAD_ENABLED=1 \
                -Wno-error=ignored-attributes \
                -Wno-missing-field-initializers \
                -Wno-self-assign \
                -Wno-sign-compare \
                -Wno-tautological-pointer-compare \
                -Wno-unused-function \
                -Wno-unused-parameter \
                -fvisibility=hidden \
                -Wno-implicit-function-declaration

include $(BUILD_STATIC_LIBRARY)

# ================================================
# JSON
# ================================================

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
        common/lib_json/json_reader.cpp \
        common/lib_json/json_value.cpp \
        common/lib_json/json_writer.cpp

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/common/lib_json

LOCAL_MODULE:= json_static

LOCAL_CPPFLAGS := -Werror -std=c++11 -fexceptions -frtti -DNDK_BUILD -DJSON_USE_EXCEPTION=0

include $(BUILD_STATIC_LIBRARY)

# ================================================
# NL
# ================================================

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
        common/lib_nl/lib/cache.c \
        common/lib_nl/lib/data.c \
        common/lib_nl/lib/nl.c \
        common/lib_nl/lib/cache_mngr.c \
        common/lib_nl/lib/addr.c \
        common/lib_nl/lib/socket.c \
        common/lib_nl/lib/fib_lookup/lookup.c \
        common/lib_nl/lib/fib_lookup/request.c \
        common/lib_nl/lib/msg.c \
        common/lib_nl/lib/object.c \
        common/lib_nl/lib/attr.c \
        common/lib_nl/lib/utils.c \
        common/lib_nl/lib/cache_mngt.c \
        common/lib_nl/lib/handlers.c \
        common/lib_nl/lib/genl/ctrl.c \
        common/lib_nl/lib/genl/mngt.c \
        common/lib_nl/lib/genl/family.c \
        common/lib_nl/lib/genl/genl.c \
        common/lib_nl/lib/route/rtnl.c \
        common/lib_nl/lib/route/route_utils.c \
        common/lib_nl/lib/netfilter/nfnl.c \
        common/lib_nl/lib/error.c \
        common/lib_nl/lib/version.c \
        common/lib_nl/lib/hash.c \
        common/lib_nl/lib/hashtable.c


LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/include \
    $(LOCAL_PATH)/include/linux-private

LOCAL_MODULE:= ln_static
LOCAL_CLANG := false

LOCAL_CFLAGS := -Werror \
                -Wno-implicit-function-declaration \
                -D_BSD_SOURCE \
                -UNDEBUG \
                -D_GNU_SOURCE \
                -DSYSCONFDIR=\"\\\"/etc/libnl\\\"\"

ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
LOCAL_CFLAGS   += -DARM64_V8A_BUILD
endif

include $(BUILD_STATIC_LIBRARY)

# ================================================
# libcutils
# ================================================

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
        common/libcutils/android_get_control_file.cpp \
        common/libcutils/fs.cpp \
        common/libcutils/multiuser.cpp \
        common/libcutils/socket_inaddr_any_server_unix.cpp \
        common/libcutils/socket_local_client_unix.cpp \
        common/libcutils/socket_local_server_unix.cpp \
        common/libcutils/socket_network_client_unix.cpp \
        common/libcutils/sockets_unix.cpp \
        common/libcutils/str_parms.cpp \
        common/libcutils/config_utils.cpp \
        common/libcutils/fs_config.cpp \
        common/libcutils/canned_fs_config.cpp \
        common/libcutils/hashmap.cpp \
        common/libcutils/iosched_policy.cpp \
        common/libcutils/load_file.cpp \
        common/libcutils/native_handle.cpp \
        common/libcutils/open_memstream.c \
        common/libcutils/record_stream.cpp \
        common/libcutils/sched_policy.cpp \
        common/libcutils/sockets.cpp \
        common/libcutils/strdup16to8.cpp \
        common/libcutils/strdup8to16.cpp \
        common/libcutils/strlcpy.c \
        common/libcutils/threads.cpp \
        #common/libcutils/android_reboot.cpp \
        common/libcutils/ashmem-dev.cpp \
        common/libcutils/klog.cpp \
        common/libcutils/partition_utils.cpp \
        common/libcutils/qtaguid.cpp \
        common/libcutils/uevent.cpp


ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
LOCAL_SRC_FILES+= common/libcutils/arch-arm64/android_memset.S
else
LOCAL_SRC_FILES+= common/libcutils/arch-arm/memset32.S
endif

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/include \
    $(LOCAL_PATH)/include/libc/include \
    $(LOCAL_PATH)/common/libcutils/include

LOCAL_MODULE:= cutils_static
LOCAL_CLANG := false

LOCAL_CPPFLAGS := -Werror -std=c++11 -fexceptions -frtti

include $(BUILD_STATIC_LIBRARY)

# ================================================
# crypto
# ================================================

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
        common/boringssl/err_data.c \
        common/boringssl/src/crypto/asn1/a_bitstr.c \
        common/boringssl/src/crypto/asn1/a_bool.c \
        common/boringssl/src/crypto/asn1/a_d2i_fp.c \
        common/boringssl/src/crypto/asn1/a_dup.c \
        common/boringssl/src/crypto/asn1/a_enum.c \
        common/boringssl/src/crypto/asn1/a_gentm.c \
        common/boringssl/src/crypto/asn1/a_i2d_fp.c \
        common/boringssl/src/crypto/asn1/a_int.c \
        common/boringssl/src/crypto/asn1/a_mbstr.c \
        common/boringssl/src/crypto/asn1/a_object.c \
        common/boringssl/src/crypto/asn1/a_octet.c \
        common/boringssl/src/crypto/asn1/a_print.c \
        common/boringssl/src/crypto/asn1/a_strnid.c \
        common/boringssl/src/crypto/asn1/a_time.c \
        common/boringssl/src/crypto/asn1/a_type.c \
        common/boringssl/src/crypto/asn1/a_utctm.c \
        common/boringssl/src/crypto/asn1/a_utf8.c \
        common/boringssl/src/crypto/asn1/asn1_lib.c \
        common/boringssl/src/crypto/asn1/asn1_par.c \
        common/boringssl/src/crypto/asn1/asn_pack.c \
        common/boringssl/src/crypto/asn1/f_enum.c \
        common/boringssl/src/crypto/asn1/f_int.c \
        common/boringssl/src/crypto/asn1/f_string.c \
        common/boringssl/src/crypto/asn1/tasn_dec.c \
        common/boringssl/src/crypto/asn1/tasn_enc.c \
        common/boringssl/src/crypto/asn1/tasn_fre.c \
        common/boringssl/src/crypto/asn1/tasn_new.c \
        common/boringssl/src/crypto/asn1/tasn_typ.c \
        common/boringssl/src/crypto/asn1/tasn_utl.c \
        common/boringssl/src/crypto/asn1/time_support.c \
        common/boringssl/src/crypto/base64/base64.c \
        common/boringssl/src/crypto/bio/bio.c \
        common/boringssl/src/crypto/bio/bio_mem.c \
        common/boringssl/src/crypto/bio/connect.c \
        common/boringssl/src/crypto/bio/fd.c \
        common/boringssl/src/crypto/bio/file.c \
        common/boringssl/src/crypto/bio/hexdump.c \
        common/boringssl/src/crypto/bio/pair.c \
        common/boringssl/src/crypto/bio/printf.c \
        common/boringssl/src/crypto/bio/socket.c \
        common/boringssl/src/crypto/bio/socket_helper.c \
        common/boringssl/src/crypto/bn_extra/bn_asn1.c \
        common/boringssl/src/crypto/bn_extra/convert.c \
        common/boringssl/src/crypto/buf/buf.c \
        common/boringssl/src/crypto/bytestring/asn1_compat.c \
        common/boringssl/src/crypto/bytestring/ber.c \
        common/boringssl/src/crypto/bytestring/cbb.c \
        common/boringssl/src/crypto/bytestring/cbs.c \
        common/boringssl/src/crypto/chacha/chacha.c \
        common/boringssl/src/crypto/cipher_extra/cipher_extra.c \
        common/boringssl/src/crypto/cipher_extra/derive_key.c \
        common/boringssl/src/crypto/cipher_extra/e_aesctrhmac.c \
        common/boringssl/src/crypto/cipher_extra/e_aesgcmsiv.c \
        common/boringssl/src/crypto/cipher_extra/e_chacha20poly1305.c \
        common/boringssl/src/crypto/cipher_extra/e_null.c \
        common/boringssl/src/crypto/cipher_extra/e_rc2.c \
        common/boringssl/src/crypto/cipher_extra/e_rc4.c \
        common/boringssl/src/crypto/cipher_extra/e_ssl3.c \
        common/boringssl/src/crypto/cipher_extra/e_tls.c \
        common/boringssl/src/crypto/cipher_extra/tls_cbc.c \
        common/boringssl/src/crypto/cmac/cmac.c \
        common/boringssl/src/crypto/conf/conf.c \
        common/boringssl/src/crypto/cpu-aarch64-linux.c \
        common/boringssl/src/crypto/cpu-arm-linux.c \
        common/boringssl/src/crypto/cpu-arm.c \
        common/boringssl/src/crypto/cpu-intel.c \
        common/boringssl/src/crypto/cpu-ppc64le.c \
        common/boringssl/src/crypto/crypto.c \
        common/boringssl/src/crypto/curve25519/spake25519.c \
        common/boringssl/src/crypto/dh/check.c \
        common/boringssl/src/crypto/dh/dh.c \
        common/boringssl/src/crypto/dh/dh_asn1.c \
        common/boringssl/src/crypto/dh/params.c \
        common/boringssl/src/crypto/digest_extra/digest_extra.c \
        common/boringssl/src/crypto/dsa/dsa.c \
        common/boringssl/src/crypto/dsa/dsa_asn1.c \
        common/boringssl/src/crypto/ec_extra/ec_asn1.c \
        common/boringssl/src/crypto/ecdh/ecdh.c \
        common/boringssl/src/crypto/ecdsa_extra/ecdsa_asn1.c \
        common/boringssl/src/crypto/engine/engine.c \
        common/boringssl/src/crypto/err/err.c \
        common/boringssl/src/crypto/evp/digestsign.c \
        common/boringssl/src/crypto/evp/evp.c \
        common/boringssl/src/crypto/evp/evp_asn1.c \
        common/boringssl/src/crypto/evp/evp_ctx.c \
        common/boringssl/src/crypto/evp/p_dsa_asn1.c \
        common/boringssl/src/crypto/evp/p_ec.c \
        common/boringssl/src/crypto/evp/p_ec_asn1.c \
        common/boringssl/src/crypto/evp/p_ed25519.c \
        common/boringssl/src/crypto/evp/p_ed25519_asn1.c \
        common/boringssl/src/crypto/evp/p_rsa.c \
        common/boringssl/src/crypto/evp/p_rsa_asn1.c \
        common/boringssl/src/crypto/evp/pbkdf.c \
        common/boringssl/src/crypto/evp/print.c \
        common/boringssl/src/crypto/evp/scrypt.c \
        common/boringssl/src/crypto/evp/sign.c \
        common/boringssl/src/crypto/ex_data.c \
        common/boringssl/src/crypto/fipsmodule/bcm.c \
        common/boringssl/src/crypto/fipsmodule/is_fips.c \
        common/boringssl/src/crypto/hkdf/hkdf.c \
        common/boringssl/src/crypto/lhash/lhash.c \
        common/boringssl/src/crypto/mem.c \
        common/boringssl/src/crypto/obj/obj.c \
        common/boringssl/src/crypto/obj/obj_xref.c \
        common/boringssl/src/crypto/pem/pem_all.c \
        common/boringssl/src/crypto/pem/pem_info.c \
        common/boringssl/src/crypto/pem/pem_lib.c \
        common/boringssl/src/crypto/pem/pem_oth.c \
        common/boringssl/src/crypto/pem/pem_pk8.c \
        common/boringssl/src/crypto/pem/pem_pkey.c \
        common/boringssl/src/crypto/pem/pem_x509.c \
        common/boringssl/src/crypto/pem/pem_xaux.c \
        common/boringssl/src/crypto/pkcs7/pkcs7.c \
        common/boringssl/src/crypto/pkcs7/pkcs7_x509.c \
        common/boringssl/src/crypto/pkcs8/p5_pbev2.c \
        common/boringssl/src/crypto/pkcs8/pkcs8.c \
        common/boringssl/src/crypto/pkcs8/pkcs8_x509.c \
        common/boringssl/src/crypto/poly1305/poly1305.c \
        common/boringssl/src/crypto/poly1305/poly1305_arm.c \
        common/boringssl/src/crypto/poly1305/poly1305_vec.c \
        common/boringssl/src/crypto/pool/pool.c \
        common/boringssl/src/crypto/rand_extra/deterministic.c \
        common/boringssl/src/crypto/rand_extra/forkunsafe.c \
        common/boringssl/src/crypto/rand_extra/fuchsia.c \
        common/boringssl/src/crypto/rand_extra/rand_extra.c \
        common/boringssl/src/crypto/rand_extra/windows.c \
        common/boringssl/src/crypto/rc4/rc4.c \
        common/boringssl/src/crypto/refcount_c11.c \
        common/boringssl/src/crypto/refcount_lock.c \
        common/boringssl/src/crypto/rsa_extra/rsa_asn1.c \
        common/boringssl/src/crypto/stack/stack.c \
        common/boringssl/src/crypto/thread.c \
        common/boringssl/src/crypto/thread_none.c \
        common/boringssl/src/crypto/thread_pthread.c \
        common/boringssl/src/crypto/thread_win.c \
        common/boringssl/src/crypto/x509/a_digest.c \
        common/boringssl/src/crypto/x509/a_sign.c \
        common/boringssl/src/crypto/x509/a_strex.c \
        common/boringssl/src/crypto/x509/a_verify.c \
        common/boringssl/src/crypto/x509/algorithm.c \
        common/boringssl/src/crypto/x509/asn1_gen.c \
        common/boringssl/src/crypto/x509/by_dir.c \
        common/boringssl/src/crypto/x509/by_file.c \
        common/boringssl/src/crypto/x509/i2d_pr.c \
        common/boringssl/src/crypto/x509/rsa_pss.c \
        common/boringssl/src/crypto/x509/t_crl.c \
        common/boringssl/src/crypto/x509/t_req.c \
        common/boringssl/src/crypto/x509/t_x509.c \
        common/boringssl/src/crypto/x509/t_x509a.c \
        common/boringssl/src/crypto/x509/x509.c \
        common/boringssl/src/crypto/x509/x509_att.c \
        common/boringssl/src/crypto/x509/x509_cmp.c \
        common/boringssl/src/crypto/x509/x509_d2.c \
        common/boringssl/src/crypto/x509/x509_def.c \
        common/boringssl/src/crypto/x509/x509_ext.c \
        common/boringssl/src/crypto/x509/x509_lu.c \
        common/boringssl/src/crypto/x509/x509_obj.c \
        common/boringssl/src/crypto/x509/x509_r2x.c \
        common/boringssl/src/crypto/x509/x509_req.c \
        common/boringssl/src/crypto/x509/x509_set.c \
        common/boringssl/src/crypto/x509/x509_trs.c \
        common/boringssl/src/crypto/x509/x509_txt.c \
        common/boringssl/src/crypto/x509/x509_v3.c \
        common/boringssl/src/crypto/x509/x509_vfy.c \
        common/boringssl/src/crypto/x509/x509_vpm.c \
        common/boringssl/src/crypto/x509/x509cset.c \
        common/boringssl/src/crypto/x509/x509name.c \
        common/boringssl/src/crypto/x509/x509rset.c \
        common/boringssl/src/crypto/x509/x509spki.c \
        common/boringssl/src/crypto/x509/x_algor.c \
        common/boringssl/src/crypto/x509/x_all.c \
        common/boringssl/src/crypto/x509/x_attrib.c \
        common/boringssl/src/crypto/x509/x_crl.c \
        common/boringssl/src/crypto/x509/x_exten.c \
        common/boringssl/src/crypto/x509/x_info.c \
        common/boringssl/src/crypto/x509/x_name.c \
        common/boringssl/src/crypto/x509/x_pkey.c \
        common/boringssl/src/crypto/x509/x_pubkey.c \
        common/boringssl/src/crypto/x509/x_req.c \
        common/boringssl/src/crypto/x509/x_sig.c \
        common/boringssl/src/crypto/x509/x_spki.c \
        common/boringssl/src/crypto/x509/x_val.c \
        common/boringssl/src/crypto/x509/x_x509.c \
        common/boringssl/src/crypto/x509/x_x509a.c \
        common/boringssl/src/crypto/x509v3/pcy_cache.c \
        common/boringssl/src/crypto/x509v3/pcy_data.c \
        common/boringssl/src/crypto/x509v3/pcy_lib.c \
        common/boringssl/src/crypto/x509v3/pcy_map.c \
        common/boringssl/src/crypto/x509v3/pcy_node.c \
        common/boringssl/src/crypto/x509v3/pcy_tree.c \
        common/boringssl/src/crypto/x509v3/v3_akey.c \
        common/boringssl/src/crypto/x509v3/v3_akeya.c \
        common/boringssl/src/crypto/x509v3/v3_alt.c \
        common/boringssl/src/crypto/x509v3/v3_bcons.c \
        common/boringssl/src/crypto/x509v3/v3_bitst.c \
        common/boringssl/src/crypto/x509v3/v3_conf.c \
        common/boringssl/src/crypto/x509v3/v3_cpols.c \
        common/boringssl/src/crypto/x509v3/v3_crld.c \
        common/boringssl/src/crypto/x509v3/v3_enum.c \
        common/boringssl/src/crypto/x509v3/v3_extku.c \
        common/boringssl/src/crypto/x509v3/v3_genn.c \
        common/boringssl/src/crypto/x509v3/v3_ia5.c \
        common/boringssl/src/crypto/x509v3/v3_info.c \
        common/boringssl/src/crypto/x509v3/v3_int.c \
        common/boringssl/src/crypto/x509v3/v3_lib.c \
        common/boringssl/src/crypto/x509v3/v3_ncons.c \
        common/boringssl/src/crypto/x509v3/v3_pci.c \
        common/boringssl/src/crypto/x509v3/v3_pcia.c \
        common/boringssl/src/crypto/x509v3/v3_pcons.c \
        common/boringssl/src/crypto/x509v3/v3_pku.c \
        common/boringssl/src/crypto/x509v3/v3_pmaps.c \
        common/boringssl/src/crypto/x509v3/v3_prn.c \
        common/boringssl/src/crypto/x509v3/v3_purp.c \
        common/boringssl/src/crypto/x509v3/v3_skey.c \
        common/boringssl/src/crypto/x509v3/v3_sxnet.c \
        common/boringssl/src/crypto/x509v3/v3_utl.c \
        common/boringssl/src/third_party/fiat/curve25519.c \
        common/boringssl/src/crypto/curve25519/asm/x25519-asm-arm.S \
        common/boringssl/src/crypto/poly1305/poly1305_arm_asm.S

ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
LOCAL_SRC_FILES+= \
        common/boringssl/linux-aarch64/crypto/chacha/chacha-armv8.S\
        common/boringssl/linux-aarch64/crypto/fipsmodule/aesv8-armx64.S\
        common/boringssl/linux-aarch64/crypto/fipsmodule/armv8-mont.S\
        common/boringssl/linux-aarch64/crypto/fipsmodule/ghashv8-armx64.S\
        common/boringssl/linux-aarch64/crypto/fipsmodule/sha1-armv8.S\
        common/boringssl/linux-aarch64/crypto/fipsmodule/sha256-armv8.S\
        common/boringssl/linux-aarch64/crypto/fipsmodule/sha512-armv8.S
else
LOCAL_SRC_FILES+= \
        common/boringssl/linux-arm/crypto/chacha/chacha-armv4.S \
        common/boringssl/linux-arm/crypto/fipsmodule/aes-armv4.S \
        common/boringssl/linux-arm/crypto/fipsmodule/aesv8-armx32.S \
        common/boringssl/linux-arm/crypto/fipsmodule/armv4-mont.S \
        common/boringssl/linux-arm/crypto/fipsmodule/bsaes-armv7.S \
        common/boringssl/linux-arm/crypto/fipsmodule/ghash-armv4.S \
        common/boringssl/linux-arm/crypto/fipsmodule/ghashv8-armx32.S \
        common/boringssl/linux-arm/crypto/fipsmodule/sha1-armv4-large.S \
        common/boringssl/linux-arm/crypto/fipsmodule/sha256-armv4.S \
        common/boringssl/linux-arm/crypto/fipsmodule/sha512-armv4.S
endif

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/include \
    $(LOCAL_PATH)/include/libc/include \
    $(LOCAL_PATH)/common/boringssl/src/crypto \
    $(LOCAL_PATH)/common/boringssl/src/include

LOCAL_MODULE:= crypto_static
LOCAL_CLANG := false
LOCAL_SHORT_COMMANDS := true

LOCAL_CFLAGS := -Werror \
                -fvisibility=hidden \
                -DBORINGSSL_SHARED_LIBRARY\
                -DBORINGSSL_IMPLEMENTATION \
                -DOPENSSL_SMALL \
                -D_XOPEN_SOURCE=700 \
                -Wno-unused-parameter \
                -DBORINGSSL_ANDROID_SYSTEM

LOCAL_ASFLAGS := -march=armv8-a+crypto -no-integrated-as

include $(BUILD_STATIC_LIBRARY)

# ================================================
# ssl
# ================================================

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
        common/boringssl/src/ssl/bio_ssl.cc \
        common/boringssl/src/ssl/custom_extensions.cc \
        common/boringssl/src/ssl/d1_both.cc \
        common/boringssl/src/ssl/d1_lib.cc \
        common/boringssl/src/ssl/d1_pkt.cc \
        common/boringssl/src/ssl/d1_srtp.cc \
        common/boringssl/src/ssl/dtls_method.cc \
        common/boringssl/src/ssl/dtls_record.cc \
        common/boringssl/src/ssl/handoff.cc \
        common/boringssl/src/ssl/handshake.cc \
        common/boringssl/src/ssl/handshake_client.cc \
        common/boringssl/src/ssl/handshake_server.cc \
        common/boringssl/src/ssl/s3_both.cc \
        common/boringssl/src/ssl/s3_lib.cc \
        common/boringssl/src/ssl/s3_pkt.cc \
        common/boringssl/src/ssl/ssl_aead_ctx.cc \
        common/boringssl/src/ssl/ssl_asn1.cc \
        common/boringssl/src/ssl/ssl_buffer.cc \
        common/boringssl/src/ssl/ssl_cert.cc \
        common/boringssl/src/ssl/ssl_cipher.cc \
        common/boringssl/src/ssl/ssl_file.cc \
        common/boringssl/src/ssl/ssl_key_share.cc \
        common/boringssl/src/ssl/ssl_lib.cc \
        common/boringssl/src/ssl/ssl_privkey.cc \
        common/boringssl/src/ssl/ssl_session.cc \
        common/boringssl/src/ssl/ssl_stat.cc \
        common/boringssl/src/ssl/ssl_transcript.cc \
        common/boringssl/src/ssl/ssl_versions.cc \
        common/boringssl/src/ssl/ssl_x509.cc \
        common/boringssl/src/ssl/t1_enc.cc \
        common/boringssl/src/ssl/t1_lib.cc \
        common/boringssl/src/ssl/tls13_both.cc \
        common/boringssl/src/ssl/tls13_client.cc \
        common/boringssl/src/ssl/tls13_enc.cc \
        common/boringssl/src/ssl/tls13_server.cc \
        common/boringssl/src/ssl/tls_method.cc \
        common/boringssl/src/ssl/tls_record.cc

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/include \
    $(LOCAL_PATH)/include/libc/include \
    $(LOCAL_PATH)/common/boringssl/src/crypto \
    $(LOCAL_PATH)/common/boringssl/src/include

LOCAL_MODULE:= ssl_static
LOCAL_CLANG := false
LOCAL_SHORT_COMMANDS := true

LOCAL_CPPFLAGS := -Werror \
                -fvisibility=hidden \
                -DBORINGSSL_SHARED_LIBRARY\
                -DBORINGSSL_IMPLEMENTATION \
                -DOPENSSL_SMALL \
                -D_XOPEN_SOURCE=700 \
                -Wno-unused-parameter \
                -DBORINGSSL_ANDROID_SYSTEM

LOCAL_ASFLAGS := -march=armv8-a+crypto -no-integrated-as

include $(BUILD_STATIC_LIBRARY)

# ================================================
# RTP client library
# ================================================

include $(CLEAR_VARS)

LOCAL_MODULE:= rtp_client
LOCAL_SRC_FILES:= \
        RTP_Client/RTP_client.c

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/include \
    $(LOCAL_PATH)/common

LOCAL_CPPFLAGS := -Werror -DNDK_BUILD
LOCAL_LDLIBS := -llog -lc

include $(BUILD_SHARED_LIBRARY)


# ================================================
# Navigator client library
# ================================================

include $(CLEAR_VARS)

LOCAL_MODULE:= Navigator_client
LOCAL_SRC_FILES:= \
        Navigator_client/Navigator_client.cpp

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/include \
    $(LOCAL_PATH)/common

LOCAL_CPPFLAGS := -Werror -DNDK_BUILD
LOCAL_LDLIBS := -llog -lc -landroid

include $(BUILD_SHARED_LIBRARY)

# ================================================
# Navigator client test
# ================================================

include $(CLEAR_VARS)

LOCAL_MODULE:= Navigator_client_test
LOCAL_SRC_FILES:= \
        Navigator_client/Navigator_client_test.cpp

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/include \
    $(LOCAL_PATH)/common

LOCAL_CPPFLAGS := -Werror

LOCAL_SHARED_LIBRARIES := libNavigator_client

include $(BUILD_EXECUTABLE)

# ================================================
# qsm server
# ================================================

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
        qsm_server_jni.cpp \
        common/shared_buffer.cpp \
        common/pthread_custom.cpp \
        module/Log_System/module_log_system.cpp \
        module/Log_System/module_kernel_log_system.cpp \
        module/QSM/module_qsm_server.cpp \
        module/QSM/module_qsm_log_switch.cpp \
        module/QSM/wumanber/WuManber.cpp \
        module/QSM/wumanber/mwm.c \
        module/RTP_Analyzer/module_rtp_server.cpp \
        log_agent_cli/logagentcli.c

LOCAL_WHOLE_STATIC_LIBRARIES := \
        ln_static \
        json_static \
        cutils_static \
        xml2_static \
        mqtt_static \
        icuuc_static

LOCAL_LDLIBS := -llog -lc -landroid -ldl
LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/include \
	$(LOCAL_PATH)/module/QSM/mqtt \
	$(LOCAL_PATH)/module/QSM/ \
	$(LOCAL_PATH)/common \
	$(LOCAL_PATH)/module/Log_System \
	$(LOCAL_PATH)/module/QSM/include \
	$(LOCAL_PATH)/module/QSM/wumanber \
	$(LOCAL_PATH)/module/RTP_Analyzer \
	$(LOCAL_PATH)/log_agent_cli

LOCAL_MODULE:= qsm_server
#LOCAL_32_BIT_ONLY := true
#LOCAL_VENDOR_MODULE := true

#LOCAL_INIT_RC := qsm_server.rc

LOCAL_CPPFLAGS := -Werror -fexceptions -frtti -DNDK_BUILD -static-libstdc++  -latomic
LOCAL_CFLAGS := -Werror  -DNDK_BUILD -DTABLET_BUILD
#-Wall -std=c++11

ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
LOCAL_CPPFLAGS += -DARM64_V8A_BUILD
LOCAL_CFLAGS   += -DARM64_V8A_BUILD
endif

#LOCAL_LDFLAGS := -Wl,--hash-style=both

#include $(BUILD_EXECUTABLE)
include $(BUILD_SHARED_LIBRARY)
