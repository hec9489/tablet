#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/select.h>
#include <errno.h>

#ifdef NDK_BUILD
#include <log.h>
#else
#include <utils/Log.h>
#endif

#include "logagentcli.h"
#include "logagentdef.h"
#include "syslogger.h"


int sendData(const char *data, int len, int timeout);


int
initList(EventItemList *_list, int _eventcode)
{
	if(!_list) {
		return -1;
	}

	memset(_list, 0x00, sizeof(EventItemList));

	if(_eventcode<0 || _eventcode>99999) {
		_list->eventcode = -1;
		return -1;
	}

	_list->eventcode = _eventcode;

	return 0;
}

void
clearList(EventItemList *_list)
{
	if(!_list || !_list->head) {
		return;
	}

	EventItem *item = _list->head;

	do {
PRINT_SYSLOG_D1(LOGTYPE_VERBOSE, "free: key_value: %s=%s", (char *)(item->key ? item->key : ""), (char *)(item->value ? item->value : ""));
		if(item->key) {
			free(item->key);
		}
		if(item->value) {
			free(item->value);
		}

		EventItem *next = item->next;
		free(item);
		item = next;
	} while(item);

	_list->head = NULL;
	_list->tail = NULL;
}

int
addItem(EventItemList *_list, const char *_key, const char *_value)
{
	if(!_list || !_key) {
		WRITE_SYSLOG(LOGTYPE_ERROR, "value is null: list=%s, key=%s", (_list ? "exists" : "null"), (_key ? _key : "null"));
		return -1;
	}

	EventItem *item = (EventItem *)malloc(sizeof(EventItem));
	if(!item) {
		WRITE_SYSLOG(LOGTYPE_FATAL, "malloc() failed: errno=%d", errno);
		return -1;
	}

	memset(item, 0x00, sizeof(EventItem));

	item->key = malloc(strlen(_key)+1);
	if(!item->key) {
		WRITE_SYSLOG(LOGTYPE_FATAL, "malloc() failed: errno=%d", errno);
		free(item);
		return -1;
	}
	sprintf(item->key, "%s", _key);

	if(_value) {
		item->value = malloc(strlen(_value)+1);
		if(!item->value) {
			WRITE_SYSLOG(LOGTYPE_FATAL, "malloc() failed: errno=%d", errno);
			free(item->key);
			free(item);
			return -1;
		}
		sprintf(item->value, "%s", _value);
	} else {
		item->value = NULL;
	}
PRINT_SYSLOG_D1(LOGTYPE_VERBOSE, "add: key_value: %s=%s", _key, (char *)(_value==NULL ? "" : _value));

	if(_list->head==NULL) {
		_list->head = item;
		_list->tail = item;
	} else {
		_list->tail->next = item;
		_list->tail = item;
	}

	return 0;
}

int
sendEvent(EventItemList *_list, int _timeout)
{
	int result;

	if(!_list || !_list->head) {
		result = -4;
		WRITE_SYSLOG(LOGTYPE_ERROR, "send fail: result=%d: value is null: list=%s, list->head=%s",
				result, (_list ? "exists" : "null"), (_list->head ? "exists" : "null"));
		return result;
	}

	WRITE_SYSLOG(LOGTYPE_INFO, "send event: eventcode=%d, timeout=%d", _list->eventcode, _timeout);

	int headSize = HEADER_SIZE;
	int dataSize = headSize;	// head size

	for(EventItem *item = _list->head; ; item=item->next) {
		WRITE_SYSLOG(LOGTYPE_VERBOSE, "key_value: %s=%s", item->key, (char *)(item->value?item->value:""));

		if(item->key) {
			dataSize += strlen(item->key);
			dataSize ++; // US

			dataSize += item->value==NULL ? 0 : strlen(item->value);
		}


		if(item->next==NULL) {
			break;
		}

		if(item->key) {
			dataSize ++; // RS
		}
	}

	if(_list->eventcode<0 || _list->eventcode>99999) {
		result = -5;
		WRITE_SYSLOG(LOGTYPE_ERROR, "send fail: eventcode=%d, result=%d", _list->eventcode, result);
		return result;
	}

	if(dataSize>10000) {
		result = -6;
		WRITE_SYSLOG(LOGTYPE_ERROR, "send fail: eventcode=%d, size=%d, result=%d", _list->eventcode, dataSize, result);
		return result;
	}

	char *pData = (char *)malloc(dataSize+1);
	if(!pData) {
		WRITE_SYSLOG(LOGTYPE_FATAL, "malloc() failed: errno=%d: eventcode=%d, result=%d", errno, _list->eventcode, result);
		return -1;
	}

	char *p = pData;
	pData[0] = 0x00;

	// iSQMS reboot event
	if(_list->eventcode==201 && _timeout<10) {
		_timeout = 10;
	}
    // reboot ?´ë˛¤?¸ë? ?ě¸?ęł  ?ëľ???ę¸°íě§ ?ë??
	else {
		_timeout = 0;
	}

	// header 15bytes: SOH(1) + STX(1) + responseYN(1) + eventcode(5) + bodyLength(7)
	sprintf(p, "%c%c%d%05d%07d", SOH, STX, (_timeout>0 ? 1 : 0), _list->eventcode, dataSize-headSize);
	p += headSize;

	for(EventItem *item = _list->head; ; item=item->next) {
		if(item->key) {
			sprintf(p, "%s", item->key);
			p += strlen(item->key);

			*p = US;
			p ++;

			if(item->value) {
				sprintf(p, "%s", item->value);
				p += strlen(item->value);
			}
		}

		if(item->next==NULL) {
			break;
		}

		if(item->key) {
			*p = RS;
			p ++;
		}
	}

	*p = 0x00;

	result = sendData(pData, dataSize, _timeout);

	free(pData);

	if(result==0) {
		WRITE_SYSLOG(LOGTYPE_INFO, "send success: eventcode=%d, timeout=%d, result=%d", _list->eventcode, _timeout, result);
	} else {
		WRITE_SYSLOG(LOGTYPE_ERROR, "send fail: eventcode=%d, timeout=%d, result=%d", _list->eventcode, _timeout, result);
	}

	return result;
}

int
sendData(const char *_data, int _len, int _timeout)
{
	WRITE_SYSLOG(LOGTYPE_DEBUG, "length=%d, timeout=%d", _len, _timeout);
PRINT_SYSLOG_D1(LOGTYPE_VERBOSE, "data=%s", (char *)(_data ? _data : ""));

	struct sockaddr_in sin;
#ifdef ARM64_V8A_BUILD // arm64-v8a build error fix
	socklen_t sinlen = sizeof(struct sockaddr_in);
#else
	int sinlen = sizeof(struct sockaddr_in);
#endif

	int sockfd;

	// initialize socket
	if((sockfd=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1) {
		WRITE_SYSLOG(LOGTYPE_ERROR, "socket() failed: errno=%d", errno);
		return -2;
	}

	// initialize server addr
	memset((char *) &sin, 0, sizeof(struct sockaddr_in));
	sin.sin_family = AF_INET;
	sin.sin_port = htons(LOG_AGENT_PORT);
	if((sin.sin_addr.s_addr = inet_addr(LOG_AGENT_SERVER))==-1) {
		WRITE_SYSLOG(LOGTYPE_ERROR, "inet_addr() failed: errno=%d", errno);
		return -2;
	}

	int wlen = 0;
	while(_len>0) {
		int n;
		if((n=sendto(sockfd, &_data[wlen], (_len > 1000 ? 1000 : _len), 0, (struct sockaddr *) &sin, sinlen))==-1) {
			WRITE_SYSLOG(LOGTYPE_ERROR, "sendto() failed: errno=%d", errno);
			close(sockfd);
			return -2;
		}
		_len -= n;
		wlen += n;
PRINT_SYSLOG_D(LOGTYPE_DEBUG, "sendLen=%d, writeLen=%d, leftLen=%d", n, wlen, _len);
	}

	if(_timeout>0) {
		WRITE_SYSLOG(LOGTYPE_DEBUG, "waiting for response");

        fd_set fds;
		FD_ZERO (&fds);
		FD_SET (sockfd, &fds);

		struct timeval tv;
		tv.tv_sec  = _timeout;
		tv.tv_usec = 0;

		int ret = select (sockfd+1, &fds, 0, 0, &tv);

		// select error
		if(ret<0) {
			WRITE_SYSLOG(LOGTYPE_WARN, "receive response failed: select() failed: errno=%d", errno);
			close(sockfd);
			return -3;
		}
		// timeout
		else if(ret==0) {
			WRITE_SYSLOG(LOGTYPE_WARN, "receive time: timeout=%d", _timeout);
			close(sockfd);
			return -3;

		}
		// normal
		else {
			char buf[1024];
			int bufsize = 1024;
			int n = recvfrom(sockfd, buf, bufsize, 0, (struct sockaddr *) &sin, &sinlen);

			WRITE_SYSLOG(LOGTYPE_DEBUG, "receive response: OK");
		}
	}

	close(sockfd);

	return 0;
}
