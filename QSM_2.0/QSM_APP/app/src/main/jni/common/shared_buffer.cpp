/*****************************************************************
** SPTek created
** Shared ring buffer to send user and kernel logs to remote server.
******************************************************************/

#define LOG_TAG "skb_log"
#define LOG_NDEBUG 0

#include <stdio.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <stdlib.h>


#include <cutils/properties.h>

#include <json/json.h>

#ifdef NDK_BUILD
#include <log.h>
#else
#include <utils/Log.h>
#endif

using namespace std;
 
/*******************************************
* DEFINE
********************************************/
#define MIN(a,b) a<b ? a:b
 
typedef struct shm_struct {
    pthread_mutex_t mtx;
    int head;
    int tail;
    int buffer_size;
} shm_struct_t;

/*******************************************
* GLOBAL VARIABLES
********************************************/

/* for logd daemon */
static shm_struct_t shm_control;
static pthread_mutexattr_t mtx_attr;
static int shm_buf_size = PAGE_SIZE*1024;	
static char shm_buffer[PAGE_SIZE*1024];
static bool init_buffer_done;

/* for QSM */
static shm_struct_t shm_qsm_control;
static pthread_mutexattr_t mtx_qsm_attr;
static int shm_buf_qsm_size = PAGE_SIZE*1024;
static char shm_qsm_buffer[PAGE_SIZE*1024];
static bool init_qsm_buffer_done;

/*******************************************
* LOCAL FUNCTION
********************************************/

/*******************************************
* GLOBAL FUNCTIONS
********************************************/ 
int log_init_shared_memory(void)
{
    int ret;

    init_buffer_done = false;
    init_qsm_buffer_done = false;

   /* initialize buffer for logd daemon */ 
    memset(&shm_control, 0, sizeof(shm_struct_t));
    shm_control.buffer_size = shm_buf_size;
    init_buffer_done = true;

    /* initialize buffer for QSM */ 
    memset(&shm_qsm_control, 0, sizeof(shm_struct_t));
    shm_qsm_control.buffer_size = shm_buf_qsm_size;
    init_qsm_buffer_done = true;
	
    return 0;
}
 
void log_destory_shared_memory(void)
{
    if(!init_buffer_done){
        return;
    }
    init_buffer_done = false;

    /* for QSM */
    if(!init_qsm_buffer_done){
        return;
    }

    init_qsm_buffer_done = false;	
}

int log_shared_put_data(char* data, int size)
{
    int tail, new_tail, head;
    int buf_size;

    if(!init_buffer_done){
        return -1;
    }

    if (NULL == data || size <= 0) {
        return -1;
    }

    size +=4; /* first two bytes is magic code and two bytes is lengh */
	
    pthread_mutex_lock(&shm_control.mtx);
    tail = shm_control.tail;
    head = shm_control.head;
    buf_size = shm_control.buffer_size;
    new_tail = tail + size;

    //LOGE("put data : head=%d,tail=%d, new_tail=%d, buf_size=%d",head,tail, new_tail, buf_size);	
	
    if(head <= tail){
        if (new_tail+5 >= buf_size)
        {
            *(shm_buffer+tail) =  0xBA;
            *(shm_buffer+tail+1) =  0xDC;
            *(shm_buffer+tail+2) =  (size >> 8) & 0xFF;
            *(shm_buffer+tail+3) =  (size) & 0xFF;
			
            if(size >= head){
                //LOGI("put data : ring buffer overflow 1 head=%d,tail=%d, new_tail=%d, buf_size=%d",head,tail, new_tail, buf_size);				
                pthread_mutex_unlock(&shm_control.mtx);
                return 0;
            }
            *(shm_buffer) =  0xBA;
            *(shm_buffer+1) =  0xDC;
            *(shm_buffer+2) =  (size >> 8) & 0xFF;
            *(shm_buffer+3) =  (size) & 0xFF;
            memcpy(shm_buffer+4, data, size-4);
            shm_control.tail = size;
        }else{
            *(shm_buffer+tail) =  0xBA;
            *(shm_buffer+tail+1) =  0xDC;
            *(shm_buffer+tail+2) =  (size >> 8) & 0xFF;
            *(shm_buffer+tail+3) =  (size) & 0xFF;
            memcpy(shm_buffer+tail+4, data, size-4);
            shm_control.tail = new_tail;
        }
    }
    else{ // tail < head 
        if(new_tail >= head){ 
            //LOGI("put data : ring buffer overflow 2 head=%d,tail=%d, new_tail=%d, buf_size=%d",head,tail, new_tail, buf_size);				
            pthread_mutex_unlock(&shm_control.mtx);
            return 0;
        }
        *(shm_buffer+tail) =  0xBA;
        *(shm_buffer+tail+1) =  0xDC;
        *(shm_buffer+tail+2) =  (size >> 8) & 0xFF;
        *(shm_buffer+tail+3) =  (size) & 0xFF;
        memcpy(shm_buffer+tail+4, data, size-4);
        shm_control.tail = new_tail;
    }	
    pthread_mutex_unlock(&shm_control.mtx);
    
    //LOGI("put data : head=%d,tail=%d, new_tail=%d, buf_size=%d",head,tail, new_tail, buf_size);   
	
    return size;
}

int log_shared_get_data(char* p_data)
{
    int tail, new_head, head;
    int buf_size, size=0;

    if(!init_buffer_done){
        return -1;
    }
	
    pthread_mutex_lock(&shm_control.mtx);
    tail = shm_control.tail;
    head = shm_control.head;
    buf_size = shm_control.buffer_size;

    if(head == tail){
        pthread_mutex_unlock(&shm_control.mtx);
        usleep(1000*100);
        return 0;
    }
	
    if(head < tail){
        if(*(shm_buffer+head)!=0xBA || *(shm_buffer+head+1)!=0xDC){
            //LOGI("get data : wrong data 1. head(%d), tail(%d)\n", shm_control.head, shm_control.tail);			
            pthread_mutex_unlock(&shm_control.mtx);
            return -1;
        }
        size = ((int)(*(shm_buffer+head+2)) <<8) | *(shm_buffer+head+3);
        new_head = head + size;
		
        memcpy(p_data, shm_buffer+head+4, size-4);
        p_data[size-4]='\0';
        shm_control.head = new_head;
    }else{ // tail < head 
           
        if ((head + 5) > buf_size){
            
            if(*(shm_buffer)!=0xBA || *(shm_buffer+1)!=0xDC){
                //LOGI("get data : wrong data 2 head(%d), tail(%d)\n", shm_control.head, shm_control.tail);
                pthread_mutex_unlock(&shm_control.mtx);
                return -1;
            }
            size = ((int)(*(shm_buffer+2)) <<8) | *(shm_buffer+3);

            //LOGE("get data : head=%d,tail=%d, size=%d, buf_size=%d",head,tail, size, buf_size);				  			
            memcpy(p_data, shm_buffer+4, size-4);
            p_data[size-4]='\0';
            shm_control.head = size;
        }
        else
        {
            if(*(shm_buffer+head)!=0xBA || *(shm_buffer+head+1)!=0xDC){
                //LOGI("get data : wrong data 3 head(%d), tail(%d)\n", shm_control.head, shm_control.tail);
                pthread_mutex_unlock(&shm_control.mtx);
                return -1;
            }
            size = ((int)(*(shm_buffer+head+2)) <<8) | *(shm_buffer+head+3);
			
            new_head = head + size;
            if (new_head+5 >= buf_size){
                memcpy(p_data, shm_buffer+4, size-4);
                p_data[size-4]='\0';
                shm_control.head = size;
            }else{
                memcpy(p_data, shm_buffer+head+4, size-4);
                p_data[size-4]='\0'; 				
                shm_control.head = new_head;
            }
        }
    }	
    pthread_mutex_unlock(&shm_control.mtx);
    
    //LOGI("get data : head(%d), tail(%d)\n", shm_control.head, shm_control.tail);
    return size-4;
}

int log_shared_get_buffer_usage(void)
{
    int tail, usage, head;
    int buf_size, size=0;

    if(!init_buffer_done){
        return -1;
    }
	
    pthread_mutex_lock(&shm_control.mtx);
    tail = shm_control.tail;
    head = shm_control.head;
    buf_size = shm_control.buffer_size;

    if(head == tail){
        pthread_mutex_unlock(&shm_control.mtx);
        return 100;
    }
	
    if(head < tail){
        usage = tail-head;
    }else{ // tail < head 
        usage = head-tail;        
    }	
    pthread_mutex_unlock(&shm_control.mtx);
    
    LOGI("QSM buffer usage=%d(usage=%d, buf size=%d)\n", (usage*100/buf_size), usage, buf_size);
    return (usage*100/buf_size);
}

int log_shared_put_qsm_data(char* data, int size)
{
    int tail, new_tail, head;
    int buf_size;

    if(!init_qsm_buffer_done){
        return -1;
    }

    size +=4; /* first two bytes is magic code and two bytes is lengh */
	
    pthread_mutex_lock(&shm_qsm_control.mtx);
    tail = shm_qsm_control.tail;
    head = shm_qsm_control.head;
    buf_size = shm_qsm_control.buffer_size;
    new_tail = tail + size;

    //LOGI("QSM put data start: head=%d,tail=%d, new_tail=%d, buf_size=%d",head,tail, new_tail, buf_size);	
	
    if(head <= tail){
        if (new_tail+5 >= buf_size)
        {
            *(shm_qsm_buffer+tail) =  0xBA;
            *(shm_qsm_buffer+tail+1) =  0xDC;
            *(shm_qsm_buffer+tail+2) =  (size >> 8) & 0xFF;
            *(shm_qsm_buffer+tail+3) =  (size) & 0xFF;			
			
            if(size >= head){
                //LOGI("QSM put data : ring buffer overflow 1 head=%d,tail=%d, new_tail=%d, buf_size=%d",head,tail, new_tail, buf_size);				
                pthread_mutex_unlock(&shm_qsm_control.mtx);
                return 0;
            }
            *(shm_qsm_buffer) =  0xBA;
            *(shm_qsm_buffer+1) =  0xDC;
            *(shm_qsm_buffer+2) =  (size >> 8) & 0xFF;
            *(shm_qsm_buffer+3) =  (size) & 0xFF;
            memcpy(shm_qsm_buffer+4, data, size-4);
            shm_qsm_control.tail = size;
        }else{
            *(shm_qsm_buffer+tail) =  0xBA;
            *(shm_qsm_buffer+tail+1) =  0xDC;
            *(shm_qsm_buffer+tail+2) =  (size >> 8) & 0xFF;
            *(shm_qsm_buffer+tail+3) =  (size) & 0xFF;
            memcpy(shm_qsm_buffer+tail+4, data, size-4);
            shm_qsm_control.tail = new_tail;
        }
    }
    else{ // tail < head 
        if(new_tail >= head){ 
            //LOGI("QSM put data : ring buffer overflow 2 head=%d,tail=%d, new_tail=%d, buf_size=%d",head,tail, new_tail, buf_size);				
            pthread_mutex_unlock(&shm_qsm_control.mtx);
            return 0;
        }
        *(shm_qsm_buffer+tail) =  0xBA;
        *(shm_qsm_buffer+tail+1) =  0xDC;
        *(shm_qsm_buffer+tail+2) =  (size >> 8) & 0xFF;
        *(shm_qsm_buffer+tail+3) =  (size) & 0xFF;
        memcpy(shm_qsm_buffer+tail+4, data, size-4);
        shm_qsm_control.tail = new_tail;
    }	
    pthread_mutex_unlock(&shm_qsm_control.mtx);
    
    //LOGI("QSM put data end: head=%d,tail=%d, new_tail=%d, buf_size=%d",head,tail, new_tail, buf_size);   
	
    return size;
}

int log_shared_get_qsm_data(char* p_data)
{
    int tail, new_head, head;
    int buf_size, size=0;

    if(!init_qsm_buffer_done){
        return -1;
    }
	
    pthread_mutex_lock(&shm_qsm_control.mtx);
    tail = shm_qsm_control.tail;
    head = shm_qsm_control.head;
    buf_size = shm_qsm_control.buffer_size;

    //LOGI("QSM get data : head=%d,tail=%d, buf_size=%d",head,tail,  buf_size);	

    if(head == tail){
        pthread_mutex_unlock(&shm_qsm_control.mtx);
        //usleep(1000*100);
        return 0;
    }
	
    if(head < tail){
        if(*(shm_qsm_buffer+head)!=0xBA || *(shm_qsm_buffer+head+1)!=0xDC){
            //LOGI("get data : wrong data 1. head(%d), tail(%d)\n", shm_qsm_control.head, shm_qsm_control.tail);			
            pthread_mutex_unlock(&shm_qsm_control.mtx);
            return -1;
        }
        size = ((int)(*(shm_qsm_buffer+head+2)) <<8) | *(shm_qsm_buffer+head+3);
        new_head = head + size;
		
        memcpy(p_data, shm_qsm_buffer+head+4, size-4);
        p_data[size-4]='\0';
        shm_qsm_control.head = new_head;
    }else{ // tail < head 
           
        if ((head + 5) > buf_size){
            
            if(*(shm_qsm_buffer)!=0xBA || *(shm_qsm_buffer+1)!=0xDC){
                //LOGI("get data : wrong data 2 head(%d), tail(%d)\n", shm_qsm_control.head, shm_qsm_control.tail);
                pthread_mutex_unlock(&shm_qsm_control.mtx);
                return -1;
            }
            size = ((int)(*(shm_qsm_buffer+2)) <<8) | *(shm_qsm_buffer+3);

            //LOGE("get data : head=%d,tail=%d, size=%d, buf_size=%d",head,tail, size, buf_size);				  			
            memcpy(p_data, shm_qsm_buffer+4, size-4);
            p_data[size-4]='\0';
            shm_qsm_control.head = size;
        }
        else
        {
            if(*(shm_qsm_buffer+head)!=0xBA || *(shm_qsm_buffer+head+1)!=0xDC){
                //LOGI("get data : wrong data 3 head(%d), tail(%d)\n", shm_qsm_control.head, shm_qsm_control.tail);
                pthread_mutex_unlock(&shm_qsm_control.mtx);
                return -1;
            }
            size = ((int)(*(shm_qsm_buffer+head+2)) <<8) | *(shm_qsm_buffer+head+3);
			
            new_head = head + size;
            if (new_head+5 >= buf_size){
                memcpy(p_data, shm_qsm_buffer+4, size-4);
                p_data[size-4]='\0';
                shm_qsm_control.head = size;
            }else{
                memcpy(p_data, shm_qsm_buffer+head+4, size-4);
                p_data[size-4]='\0'; 				
                shm_qsm_control.head = new_head;
            }
        }
    }
    pthread_mutex_unlock(&shm_qsm_control.mtx);
    
    //LOGI("QSM get data : head(%d), tail(%d)\n", shm_control.head, shm_control.tail);
    return size-4;
}

void log_shared_reset_qsm_buffer() {
    pthread_mutex_lock(&shm_qsm_control.mtx);
    shm_qsm_control.tail = 0;
    shm_qsm_control.head = 0;
    memset(shm_qsm_buffer, 0, shm_qsm_control.buffer_size);
    pthread_mutex_unlock(&shm_qsm_control.mtx);
}
