/*****************************************************************
** SPTek created
******************************************************************/

#define LOG_TAG "Navigator_client"
#define LOG_NDEBUG 0

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>

#include <time.h>

#ifdef NDK_BUILD
#include <log.h>
#else
#include <utils/Log.h>
#endif

#include "shared_buffer.h"

#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
#include <jni.h>
#include "utils/misc.h"
#endif/*TABLET_BUILD*/

using namespace std;

/*******************************************
* DEFINE
********************************************/
#define MIN(a,b) a<b ? a:b

#define SOCKET_NAME "qsm_server"

//#define TYPE_MAX_SIZE 10
//#define PAYLOAD_MAX_SIZE 1024*2
//#define BUFFER_SIZE PAYLOAD_MAX_SIZE+TYPE_MAX_SIZE
#define BUFFER_SIZE 1024*2

#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
#define INTERNAL_DIR                      "data/data"
#define QSM_SOCKET_DIR                    "btf/qsm/socket"
#endif/*TABLET_BUILD*/

/*******************************************
* LOCAL VARIABLES
********************************************/
static int data_socket;
static char g_buf[BUFFER_SIZE];

#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
static const char *classQsm2Transfer = "com/skb/qsm2lib/QSM2Transfer";
char packgeName[128];
#endif/*TABLET_BUILD*/

/*******************************************
* LOCAL FUNCTION
********************************************/

int sNavigator_client_setupClient(void) {
    char socket_name[108];
    struct sockaddr_un server_addr;     
    
#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
    data_socket = socket(PF_LOCAL, SOCK_STREAM, 0);
#else
    data_socket = socket(AF_UNIX, SOCK_STREAM, 0);
#endif/*TABLET_BUILD*/
    if(data_socket < 0) {
        LOGI("RTP_client: socket: %s", strerror(errno));
        return -1;
    }

    //memcpy(&socket_name[0], "\0", 1);
    //strcpy(&socket_name[1], SOCKET_NAME);
#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
    sprintf(socket_name, "/%s/%s/%s/%s", INTERNAL_DIR, packgeName, QSM_SOCKET_DIR, SOCKET_NAME);
#else
    strcpy(&socket_name[0], SOCKET_NAME);
#endif/*TABLET_BUILD*/
    
    memset(&server_addr, 0, sizeof(server_addr));
#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
    server_addr.sun_family = AF_LOCAL;
#else
    server_addr.sun_family = AF_UNIX;
#endif/*TABLET_BUILD*/
    strncpy(server_addr.sun_path, socket_name, sizeof(server_addr.sun_path) - 1);
    
    int ret = TEMP_FAILURE_RETRY(connect(data_socket, (const struct sockaddr *) &server_addr, sizeof(server_addr)));
    if (ret < 0) {
        LOGI("RTP_client:  connect: %s", strerror(errno));
        return -1;
    }
    
    LOGI("NAVIGATOR_client: Client Setup Complete data_socket(%d)",data_socket);
    return 0;
} 

void sNavigator_client_sighup_handler(int /*unused*/)
{
    if(data_socket > 0){
        TEMP_FAILURE_RETRY(close(data_socket));
        data_socket = -1;
    }
}

/*******************************************
* GLOBAL FUNCTION
********************************************/

static jint Navigator_client_init(JNIEnv *env, jobject obj, jstring packageName){
    const char *pkgName;

    data_socket = -1;
    
    signal(SIGHUP, sNavigator_client_sighup_handler);

    LOGI("RTP_client: init");

    pkgName = env->GetStringUTFChars(packageName, NULL);
	sprintf(packgeName, "%s", pkgName);

    if(sNavigator_client_setupClient() < 0){
        LOGI("RTP_client:  sRTP_client_setupClient failed");
        return -1; 
    }
    return 0;
}

int Navigator_client_send(char* data, int size){

    int ret;

    if(size <= 0 || data == NULL){
        LOGI("Navigator_client:  Navigator_client_send failed. data size is bigger than buffer size or data is NULL");
        return -1;
    }

    if(data_socket <= 0){
        if(sNavigator_client_setupClient() < 0){
            LOGI("Navigator_client:  reconnection failed");
            return -1;
        }
    }


    time_t     now = time(0);
    struct tm  tstruct;
    struct timeval te;

    char       buf[80];
    char time_buf[30]={0,};


    tstruct = *localtime(&now);
    gettimeofday(&te, NULL);
    int milliseconds = (int)(te.tv_usec/1000);

    strftime(buf, sizeof(buf), "%Y%m%d%H%M%S", &tstruct);
    sprintf(time_buf,"%s.%03d",buf, milliseconds);

    ret = snprintf(g_buf, BUFFER_SIZE-1, "Navigator:%s:%s",time_buf,data);
    if(ret <= 0){
        LOGI("Navigator_client:  snprintf failed.");
        return -1;
    }

    if(ret > BUFFER_SIZE){
        ret = BUFFER_SIZE;
    }
    g_buf[ret]='\0';

    if(data_socket > 0){
        ret = TEMP_FAILURE_RETRY(write(data_socket, g_buf, ret));
        if (ret < 0) {
            LOGI("Navigator_client: failed to send it to qsm server. write error: %s", strerror(errno));
        }
        LOGI("Navigator_client:  sent data to qsm server (size=%d)", ret);
        return ret;
    }
    return -1;  
}

void Navigator_client_release(void){
    if(data_socket > 0){
        TEMP_FAILURE_RETRY(close(data_socket));
        data_socket = -1;
    }
}


static JNINativeMethod gMethods[] = {
        {"navigator_init", "(Ljava/lang/String;)I", (void*)Navigator_client_init},
};

jint JNI_OnLoad(JavaVM* vm, void* /* reserved */)
{
    JNIEnv* env = NULL;
    jint result = -1;

    LOGI("[%s] navigator called", __func__);

    if (vm->GetEnv((void**)&env, JNI_VERSION_1_6) != JNI_OK)
    {
        LOGI("[%s][%d] GetEnv falied", __func__, __LINE__);
        return JNI_FALSE;
    }

    jclass clazz = env->FindClass(classQsm2Transfer);
    if (clazz == NULL) {
        LOGI("[%s][%d] Failed to find %s", __func__, __LINE__, classQsm2Transfer);
        return JNI_FALSE;
    }

    if (env->RegisterNatives(clazz, gMethods, NELEM(gMethods)) < 0) {
        LOGI("[%s][%d] Failed to QSM Server native registration", __func__, __LINE__);
        return JNI_FALSE;
    }

    result = JNI_VERSION_1_6;
    return result;
}

