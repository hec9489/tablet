
#ifndef NAVIGATOR_CLIENT_H
#define NAVIGATOR_CLIENT_H

/*****************************************
GLOBAL FUNCTIONS
******************************************/
#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
int   Navigator_client_init(char *packageName);
#else
int   Navigator_client_init(void);
#endif/*TABLET_BUILD*/
int   Navigator_client_send(char* data, int size);
void Navigator_client_release(void);

#endif //NAVIGATOR_CLIENT_H
