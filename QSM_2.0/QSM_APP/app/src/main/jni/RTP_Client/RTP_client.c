/*****************************************************************
** SPTek created
******************************************************************/

#define LOG_TAG "RTP_Client"
//#define LOG_NDEBUG 0

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <time.h>
#include <sys/fcntl.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/inotify.h>
#include <android/log.h>
#include "RTP_client.h"

/*******************************************
* for Debug Flag
********************************************/
static bool mIsDebugEnable     = false;//true;  // for debug and test log not release version
/*******************************************
* DEFINE
********************************************/

#define  RTP_LOG_TAG      "RTP"
#define  LOGUNK(...)  __android_log_print(ANDROID_LOG_UNKNOWN,RTP_LOG_TAG,__VA_ARGS__)
#define  LOGDEF(...)  __android_log_print(ANDROID_LOG_DEFAULT,RTP_LOG_TAG,__VA_ARGS__)
#define  LOGV(...)    __android_log_print(ANDROID_LOG_VERBOSE,RTP_LOG_TAG,__VA_ARGS__)
#define  LOGD(...)    __android_log_print(ANDROID_LOG_DEBUG,RTP_LOG_TAG,__VA_ARGS__)
#define  LOGI(...)    __android_log_print(ANDROID_LOG_INFO,RTP_LOG_TAG,__VA_ARGS__)
#define  LOGW(...)    __android_log_print(ANDROID_LOG_WARN,RTP_LOG_TAG,__VA_ARGS__)
#define  LOGE(...)    __android_log_print(ANDROID_LOG_ERROR,RTP_LOG_TAG,__VA_ARGS__)
#define  LOGF(...)    __android_log_print(ANDROID_FATAL_ERROR,RTP_LOG_TAG,__VA_ARGS__)
#define  LOGS(...)    __android_log_print(ANDROID_SILENT_ERROR,RTP_LOG_TAG,__VA_ARGS__)
#define  LOG_FOOT     __android_log_print(ANDROID_LOG_ERROR, RTP_LOG_TAG, "[%s:%d:%s]", __FILE__, __LINE__, __FUNCTION__)


//#define REQ_FIFO    "/data/btv_home/run/qsm_fifo"
#define REQ_FIFO         "/data/btv_home/tmp/qsm_fifo"
#define REQ_FIFO_PATH    "/data/btv_home/tmp/"
#define REQ_FIFO_NAME    "qsm_fifo"

#ifndef TEMP_FAILURE_RETRY
#define TEMP_FAILURE_RETRY(exp)            \
  ({                                       \
    __typeof__(exp) _rc;                   \
    do {                                   \
      _rc = (exp);                         \
    } while (_rc == -1 && errno == EINTR); \
    _rc;                                   \
  })
#endif

#define RTP_CLIENT_PACKET_MESSAGE_DUPLICATION   "100"  // "Sequence Number Duplication Packet"
#define RTP_CLIENT_PACKET_MESSAGE_REVERSE       "200"  // "Sequence Number Reverse Packet"
#define RTP_CLIENT_PACKET_MESSAGE_LOSS          "300"  // "Sequence Number Loss Packet"
#define RTP_CLIENT_PACKET_MESSAGE_JITTER        "400"  // "Time Stamp Jitter Packet"

#define RTP_CLIENT_CHECKER_MAX_SIZE        11  // RTP Client Checker 크기
#define RTP_CLIENT_MIN_LENS                 8  // RTP Header 최소 사이즈

#define RTP_CLIENT_SEND_MESSAGE_MAX_LEN 100+1  // Message Buffer 최대 길이

#define RTP_CLIENT_VALID_PACKET             0  // 정상 패킷
#define RTP_CLIENT_SAME_PACKET             -1  // 동일 패킷
#define RTP_CLIENT_REVERSE_PACKET          -2  // 역순 패킷
#define RTP_CLIENT_LOSS_PACKET             -3  // 패킷 유실
#define RTP_CLIENT_JITTER_PACKET           -4  // JITTER 패킷 : timestamp system 기준으로 rtp timestamp가 300ms를 벗어나면 Jitter 
#define RTP_CLIENT_INTERAL_ERROR           -5  // 내부 에러

#define RTP_DATA_WRITE_RETRY_COUNT         (10)

/*******************************************
* Data 구조체
********************************************/

typedef struct RTPClientChecker {
    unsigned int sequence_number;
    unsigned int time_stamp;
}RTPClientChecker_T;

/*******************************************
* LOCAL VARIABLES
********************************************/
static int g_fd = -1;
static pthread_mutex_t g_mutex;
static pthread_t gRTPFifoInotifyThread;

static volatile bool g_rtp_fifo_thread_done = false;

RTPClientChecker_T mRTPClientChecker[RTP_CLIENT_CHECKER_MAX_SIZE];
int                mRTPClinetCheckerIndex = 0;
char               mSendMessageBuffer[RTP_CLIENT_SEND_MESSAGE_MAX_LEN];

/*******************************************
* LOCAL FUNCTION
********************************************/

static void* inotify_RTP_fifo_thread(void* data)
{
    int ret;
    int fd;
    int wd;
    char buf[4096] __attribute__ ((aligned(__alignof__(struct inotify_event))));
    char *ptr;
    ssize_t size;
    const struct inotify_event *event;

    g_rtp_fifo_thread_done = true;

    LOGD("inotify_RTP_fifo_thread : thread start");

    fd = inotify_init();
    if (fd < 0) {
        LOGE("inotify_RTP_fifo_thread inotify_init failed");
        goto end;
    }

    wd = inotify_add_watch(fd, REQ_FIFO_PATH, IN_MODIFY | IN_CREATE | IN_DELETE);
    if (wd < 0) {
        LOGE("inotify_RTP_fifo_thread inotify_add_watch Failed to add watch [%s] [%s]", REQ_FIFO_PATH, strerror(errno));
        goto end;
    }

    while (1) {
        size = read(fd, buf, sizeof(buf));
        if (size == -1 && errno != EAGAIN) {
            perror("read");
            LOGE("inotify_RTP_fifo_thread failed read : %s", strerror(errno));
            goto end;
        }

        if (size <= 0)
            break;

        for (ptr = buf; ptr < buf + size; ptr += sizeof(struct inotify_event) + event->len) {
            event = (struct inotify_event *)ptr;
            if (event->mask & IN_CREATE) {
                if (event->len > 0) {
                    if (0 == strcmp(REQ_FIFO_NAME, REQ_FIFO_NAME)) {
                        RTP_client_release();
                        RTP_client_init();
                    }
                }
            }

            if(mIsDebugEnable == true) {
                if (event->mask & IN_CREATE)
                    LOGD("inotify_RTP_fifo_thread IN_CREATE ");
                if (event->mask & IN_DELETE)
                    LOGD("inotify_RTP_fifo_thread IN_DELETE ");
                if (event->len > 0) {
                    LOGD("inotify_RTP_fifo_thread : event->mask[%d] name = %s\n", event->mask, event->name);
                }
            }


        }
    }

end:
    ret = inotify_rm_watch(fd, wd);
    if (ret < 0) {
        LOGE("inotify_RTP_fifo_thread inotify_rm_watch Failed to rm watch [fd : %d] [wd : %d] [%s]", fd, wd, strerror(errno));
    }

    g_rtp_fifo_thread_done = false;

    LOGI("inotify_RTP_fifo_thread : thread end.");
    return 0;
}

static void sRTP_client_sigpipe_handler(int unused)
{
    pthread_mutex_lock(&g_mutex);
    if(g_fd >0){
        close(g_fd);
        g_fd = -1;
    }
    LOGE("sRTP_client_sigpipe_handler");
    pthread_mutex_unlock(&g_mutex);
}

static int sRTP_client_open(const char *fifo, int flags)
{
    if (!fifo) {
        return ERROR_PARAM;
    }

    if( true == mIsDebugEnable) {
        LOGD("sRTP_client_open start ");
    }

    return open(fifo, flags, 0);
}

static int sRTP_client_send(int fd, char *buf, int len)
{
    int ret = 0;
    int errno;

    if( true == mIsDebugEnable) {
        LOGD("sRTP_client_send start");
    }

    //ret = TEMP_FAILURE_RETRY(write(fd, buf, len));
    ret = write(fd, buf, len);
    //if (ret == -1) {
    if (ret <= 0) {
        if (errno == EAGAIN) {
            ret = ERROR_WOULDBLOCK;
        } 
    }

    if( true == mIsDebugEnable) {
        LOGD("sRTP_client_send end write return[%d]", ret);
    }

    return ret;
}

/*******************************************
* Private Method Start
********************************************/

/**
 RTP Packet 여부를 판단합니다.
 */

bool checkRTPPacket(char *header)
{
    if((unsigned int)((header[0] >> 6) & 0x03) == 2) {
        return true;
    }
    return false;
}

/**
 RTP Header의 Sequence Number(16bit)를 가져 옵니다.
 */
unsigned int getSequenceNumber(char *header)
{
    unsigned int sequenceNumber = 0;
    unsigned int value          = 0;
    char         *pValue        = (char *)&value;

    (void)memcpy(pValue + 2, &header[2], 2);
    sequenceNumber = ntohl(value);
    return sequenceNumber;
}

/**
 RTP Header의 Timestamp(32bit)를 가져 옵니다.
 */
unsigned int getTimeStamp(char *header)
{
    unsigned int timeStamp = 0;
    unsigned int value     = 0;
    char         *pValue   = (char *)&value;
    (void)memcpy(pValue, &header[4], 4);
    timeStamp = ntohl(value);
    return timeStamp;
}

/**
 * 현재 시스템 시간을 얻어 옵니다.
 */
unsigned int getSystemTime(void)
{
    unsigned int curTime = (unsigned int)time(NULL);
    return curTime;
}

/**
 * Jitter Packet인지 확인합니다. 추후에 계산식을 여기서 수정하시면 됩니다.
 */
bool isJitterPacket(unsigned int cur_timestamp, unsigned int pre_timestamp)
{
    if(cur_timestamp - pre_timestamp > 300) {
        return true;
    }
    return false;
}

/**
 * 현재 상태의 패킷을 로그로 출력합니다.
 */
void showChekerData(void)
{
    if(mIsDebugEnable == true) {
        // for(int index = mRTPClinetCheckerIndex-1; index >= 0; index--) {
        //     RTPClientChecker_T logChecker = mRTPClientChecker[index];
        //     LOGE("index[%d] sequence number[%u] time stamp[%u]", index, logChecker.sequence_number, logChecker.time_stamp);
        // }
    }
}

/**
 패킷을 검증합니다.
 */

int checkValidPacket(unsigned int sequenceNumber, unsigned int timestamp)
{
    int retValue = RTP_CLIENT_VALID_PACKET;

    //1. Checker MAX Size까지 데이터가 채워지지 않으면 데이터를 계속 채운다.
    if(mRTPClinetCheckerIndex < RTP_CLIENT_CHECKER_MAX_SIZE) {
        RTPClientChecker_T  *pChecker = &mRTPClientChecker[mRTPClinetCheckerIndex];
        if(pChecker == NULL) {
            return RTP_CLIENT_INTERAL_ERROR;
        }
        pChecker->sequence_number = sequenceNumber;
        pChecker->time_stamp      = timestamp;
        mRTPClinetCheckerIndex    = mRTPClinetCheckerIndex + 1;

        if(mIsDebugEnable == true) {
            if(mRTPClinetCheckerIndex == RTP_CLIENT_CHECKER_MAX_SIZE) {
                showChekerData();
            }
        }
        return retValue;
    }

    RTPClientChecker_T lastPacketChecker = mRTPClientChecker[mRTPClinetCheckerIndex-1];

    //2. 연속 패킷인 경우 정상으로 인식합니다. 더이상 체크를 하지 않습니다. 시퀀스는 최대값이 65535 이다.
    if(sequenceNumber == 0 || sequenceNumber - lastPacketChecker.sequence_number == 1) {
        (void)memmove(mRTPClientChecker, &mRTPClientChecker[1], sizeof(RTPClientChecker_T)*RTP_CLIENT_CHECKER_MAX_SIZE-1);
        RTPClientChecker_T *pChecker = &mRTPClientChecker[mRTPClinetCheckerIndex-1];
        pChecker->sequence_number  = sequenceNumber;
        pChecker->time_stamp       = timestamp;
        return retValue;
    }

    if(mIsDebugEnable == true) {
        LOGE("검증 시작: 시퀀스 넘버[%u], 타임 스템프[%u]", sequenceNumber, timestamp);
    }

    //3. Checker의 시퀀스 넘버와 수신한 Packet의 시퀀스 넘버를 비교하여 동일 패킷인지, 역패킷인지 검증한다.
    for(int index = mRTPClinetCheckerIndex-1; index >= 0; index--) {
        RTPClientChecker_T checker = mRTPClientChecker[index];
        if(sequenceNumber == checker.sequence_number) {
            LOGE("동일 패킷 정보 순서[%d], 현재 sequenceNumber[%u], 이전 sequence_number[%u]", index, sequenceNumber, checker.sequence_number);
            retValue = RTP_CLIENT_SAME_PACKET;
            break;
        } else if(sequenceNumber < checker.sequence_number) {
            if(mIsDebugEnable == true) {
                LOGE("역 패킷 정보 순서[%d], 현재 sequenceNumber[%u], 이전 sequence_number[%u]", index, sequenceNumber, checker.sequence_number);
            }
            retValue = RTP_CLIENT_REVERSE_PACKET;
            break;
        }
    }

    //3. 동일 패킷, Reverse 패킷인 경우 FIFO를 이용하여 데이터를 재 정렬한다.
    if(retValue == RTP_CLIENT_SAME_PACKET || retValue == RTP_CLIENT_REVERSE_PACKET) {
        (void)memmove(mRTPClientChecker, &mRTPClientChecker[1], sizeof(RTPClientChecker_T)*RTP_CLIENT_CHECKER_MAX_SIZE-1);
        RTPClientChecker_T *pChecker = &mRTPClientChecker[mRTPClinetCheckerIndex-1];
        pChecker->sequence_number  = sequenceNumber;
        pChecker->time_stamp       = timestamp;

        if(mIsDebugEnable == true) {
            showChekerData();
        }
        return retValue;
    }

    //4. Checker의 마지막 데이터와 수신된 데이터의 시퀀스를 비교하여 다음 순번이 아닌경우 Loss Packet으로 처리한다.
    if(sequenceNumber - lastPacketChecker.sequence_number != 1) {
        (void)memmove(mRTPClientChecker, &mRTPClientChecker[1], sizeof(RTPClientChecker_T)*RTP_CLIENT_CHECKER_MAX_SIZE-1);
        RTPClientChecker_T *pChecker = &mRTPClientChecker[mRTPClinetCheckerIndex-1];
        if(pChecker != NULL) {
            pChecker->sequence_number    = sequenceNumber;
            pChecker->time_stamp         = timestamp;
            retValue                     = RTP_CLIENT_LOSS_PACKET;
            if(mIsDebugEnable == true) {
                LOGE("패킷 유실 sequenceNumber[%u], 패킷차이[%u]", sequenceNumber, sequenceNumber - lastPacketChecker.sequence_number);
                showChekerData();
            }
            return retValue;
        }
    }

    //5. Jitter Packet은 이전 time stamp와 300ms 이상 차이나 나는지 확인하여 결정한다.
    if(isJitterPacket(timestamp, lastPacketChecker.time_stamp) == true) {
        (void)memmove(mRTPClientChecker, &mRTPClientChecker[1], sizeof(RTPClientChecker_T)*RTP_CLIENT_CHECKER_MAX_SIZE-1);
        RTPClientChecker_T *pChecker = &mRTPClientChecker[mRTPClinetCheckerIndex-1];
        if(pChecker != NULL) {
            pChecker->sequence_number    = sequenceNumber;
            pChecker->time_stamp         = timestamp;
            retValue                     = RTP_CLIENT_JITTER_PACKET;
            if(mIsDebugEnable == true) {
                LOGE("Jitter Packet timestamp[%u], time_stamp[%u] 타임 차이[%u]", timestamp, lastPacketChecker.time_stamp, timestamp - lastPacketChecker.time_stamp);
                showChekerData();
            }
            return retValue;
        }
    }

    //6. FIFO를 이용하여 데이터를 재 정렬한다.
    (void)memmove(mRTPClientChecker, &mRTPClientChecker[1], sizeof(RTPClientChecker_T)*RTP_CLIENT_CHECKER_MAX_SIZE-1);
    RTPClientChecker_T *pChecker = &mRTPClientChecker[mRTPClinetCheckerIndex-1];
    if(pChecker != NULL) {
        pChecker->sequence_number  = sequenceNumber;
        pChecker->time_stamp       = timestamp;
    }
    return retValue;
}

void initBuffer(void)
{
    memset(mRTPClientChecker,0,sizeof(mRTPClientChecker));
    mRTPClinetCheckerIndex =0;
}

/*******************************************
* Private Method End
********************************************/

/*******************************************
* GLOBAL FUNCTION
********************************************/

int RTP_client_init(void)
{
    signal(SIGPIPE, sRTP_client_sigpipe_handler);

    if( true == mIsDebugEnable) {
         LOGD("RTP_client_init");
    }

    pthread_mutex_lock(&g_mutex);
    g_fd = sRTP_client_open((char*)REQ_FIFO, O_WRONLY);
    pthread_mutex_unlock(&g_mutex);

    initBuffer();

    if(g_rtp_fifo_thread_done == false) {
        pthread_attr_t attr;

        if( pthread_attr_init(&attr) != 0 ) {
            if( true == mIsDebugEnable) {
                 LOGE("RTP_client_init pthread_attr_init fail");
            }
            goto end;
        }

        if( pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED) != 0 ) {
            if( true == mIsDebugEnable) {
                 LOGE("RTP_client_init pthread_attr_setdetachstate fail");
            }
            goto end;
        }

        if( pthread_create(&gRTPFifoInotifyThread,&attr, inotify_RTP_fifo_thread, NULL ) != 0 ) {
            if( true == mIsDebugEnable) {
                 LOGE("RTP_client_init inotify_RTP_fifo_thread pthread_create fail");
            }
            goto end;
        }

        LOGD("RTP_client_init inotify_RTP_fifo_thread start");

        if( pthread_attr_destroy(&attr) != 0 ) {
            if( true == mIsDebugEnable) {
                 LOGE("RTP_client_init pthread_attr_destroy fail");
            }
            goto end;
        }
    }

end:

    return g_fd;
}

int RTP_client_release(void)
{
    if( true == mIsDebugEnable) {
         LOGD("RTP_client_release");
    }

    pthread_mutex_lock(&g_mutex);
    if (g_fd > 0) {
        close(g_fd);
        g_fd = -1;
    }
    pthread_mutex_unlock(&g_mutex);
    return SUCCESS;
}

int RTP_client_send(char *buf, int len)
{
    int ret;

    if(g_fd < 0){
        return ERROR_FD;
    }
    /* check code for RTP start */

    // 1. Packet Buffer를 체크합니다.
    if(buf == NULL) {
        if(mIsDebugEnable == true) {
            LOGI("입력 버퍼에 데이터가 없습니다.");
        }
        return ERROR_PARAM;
    }

    // 2. Packet의 최소 길이를 체크 합니다.
    if(len < RTP_CLIENT_MIN_LENS) {
        if(mIsDebugEnable == true) {
            LOGI("길이가 부족합니다.");
        }

        return ERROR_PARAM;
    }

    // 3. Packet이 RTP Packet인지 체크 합니다.
    bool isRtpPacket = checkRTPPacket(buf);
    if(isRtpPacket == false) {
        if(mIsDebugEnable == true) {
            LOGI("RTP 패킷이 아닙니다..");
        }
        return ERROR_WRONG_RTP_PACKET;
    }

    //4. Sequence number를 가져 옵니다.
    unsigned int sequenceNumber = getSequenceNumber(buf);

    //5. Time Stamp를 가져 옵니다.
    unsigned int timeStamp = getTimeStamp(buf);

    //6. 패킷을 검증합니다.
    int validPacket = checkValidPacket(sequenceNumber, timeStamp);
    if(mIsDebugEnable == true) {
        if(validPacket != RTP_CLIENT_VALID_PACKET) {
            LOGI("RTP 패킷이 검증 결과[%d]", validPacket);
        }
    }

    if(validPacket == RTP_CLIENT_VALID_PACKET || validPacket == RTP_CLIENT_INTERAL_ERROR) {
        int retValue = SUCCESS;
        if(validPacket == RTP_CLIENT_INTERAL_ERROR) {
            retValue = ERROR_IO;
        }
        return retValue;
    }

    char         *pMessage = NULL;
    unsigned int size      = 0;
    if(validPacket == RTP_CLIENT_SAME_PACKET) {
        pMessage = RTP_CLIENT_PACKET_MESSAGE_DUPLICATION;
        size     = strlen(pMessage);
    } else if(validPacket == RTP_CLIENT_REVERSE_PACKET) {
        pMessage = RTP_CLIENT_PACKET_MESSAGE_REVERSE;
        size     = strlen(pMessage);
    } else if(validPacket == RTP_CLIENT_LOSS_PACKET) {
        pMessage = RTP_CLIENT_PACKET_MESSAGE_LOSS;
        size     = strlen(pMessage);
    } else {
        pMessage = RTP_CLIENT_PACKET_MESSAGE_JITTER;
        size     = strlen(pMessage);
    }

    if(size == 0) {
        if(mIsDebugEnable == true) {
            LOGD("전달 메시지 사이즈가 부족합니다.");
        }
        return RTP_CLIENT_INTERAL_ERROR;
    }

    pthread_mutex_lock(&g_mutex);
    if(g_fd < 0){
        pthread_mutex_unlock(&g_mutex);
        return ERROR_FD;
    }
    ret = sRTP_client_send(g_fd, pMessage, size);

    pthread_mutex_unlock(&g_mutex);

    if(ret <= 0){
        for(int i = 0 ; i < RTP_DATA_WRITE_RETRY_COUNT ; i++) {
            if(mIsDebugEnable == true) {
                LOGE("RTP_client_send sRTP_client_open and sRTP_client_send retry count[%d]", i + 1);
            }

            RTP_client_release();
            RTP_client_init();

            if(g_fd < 0){
                continue;
            }

            ret = sRTP_client_send(g_fd, pMessage, size);
            if(ret > 0){
                break;
            }
        }

        if(ret <= 0){
            LOGE("RTP sRTP_client_send error[%d]", ERROR_IO);
            return ERROR_IO;
        }
    }

    if(mIsDebugEnable == true) {
        LOGE("RTP client Send ret[%d], validPacket[%d], Message[%s], size[%d]", ret, validPacket, pMessage, size);
    }

    return SUCCESS;
}
