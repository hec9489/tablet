/*******************************************************************************
  MEMWATCH_MALLOC


  history

    20210417 cracker@yiwoosolution.co.kr, initial

*******************************************************************************/
#ifndef _MEMWATCH_MALLOC_H_
#define _MEMWATCH_MALLOC_H_


#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
// private variables
//------------------------------------------------------------------------------
#ifdef MEMWATCH_TEST
#include "memwatch.h"

#define MEMWATCH_STAT_INIT       MW_STAT_LINE
#define MEMWATCH_DEFAULT_MODE    MW_NML_DEFAULT
#define MEMWATCH_SIZE            (1024 * 1024)

#define malloc(s) memwatch_malloc((s))
#define strdup(s) memwatch_strdup(s)
#define realloc(p, s) memwatch_realloc((p), (s))
#define calloc(p,s) memwatch_calloc((p), (s))
#define free(p) memwatch_free((p))
#define MEMWATCH_INIT(p) memwatch_init(p)
#define MEMWATCH_LIMIT(p) memwatch_limit(p)
#define MEMWATCH_MODE(p) memwatch_mode(p)
#define MEMWATCH_STOP() memwatch_stop()
#define MEMWATCH_TRACE(p) memwatch_trace(p)
#define MEMWATCH_CHECK()   memwatch_check()
#endif/*MEMWATCH_TEST*/

#ifdef __cplusplus
} /* closing brace for extern "C" */
#endif

#endif // _MEMWATCH_MALLOC_H_

