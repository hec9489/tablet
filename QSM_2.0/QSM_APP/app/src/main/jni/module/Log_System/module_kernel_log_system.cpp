/*****************************************************************
** SPTek created
** This daemon gets kernel and user messages and send them to remote server
** and get memory information and put it into user log and send it to remote server 
******************************************************************/

#define LOG_TAG "QSM Log System(Kernel)"
#define LOG_NDEBUG 0

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/un.h>
#include <errno.h>

#include <time.h>

#include <sys/system_properties.h>
#include <shared_buffer.h>
#include <json/json.h>
#include "module_qsm_server.h"
#include "module_qsm_log_switch.h"

#ifdef NDK_BUILD
#include <log.h>
#else
#include <utils/Log.h>
#endif

#include <sys/wait.h>


using namespace std;

/*******************************************
* DEFINE
********************************************/

//#define ERROR_FATAL_LOG_INTO_KERNEL_LOG

#define MIN(a,b) a<b ? a:b

#define READ   0
#define WRITE  1

#define BUFFER_SIZE 1024

#define RESTART_SLEEP_TIME_SEC      1

#define LAST_SDK_VER_OLD_GET_KLOG   25

/*******************************************
* GLOBAL VARIABLES
********************************************/
static volatile bool sRun;
extern volatile bool g_is_connection_restricted;
extern volatile bool g_sleep_status;
extern volatile bool g_kernel_log_system_thread_done;

static FILE *fp;
static pid_t pid;
static pthread_mutex_t g_mutex;

static int s_last_log_month, s_last_log_day, s_last_log_hour, s_last_log_min, s_last_log_sec, s_last_log_ms;

/*******************************************
* LOCAL FUNCTION
********************************************/

static FILE * popen2(string command, string type, int & pid)
{
    pid_t child_pid;
    int fd[2];
    pipe(fd);

    if((child_pid = fork()) == -1)
    {
        LOGI("QSM Log System : Logcat kernel popen2 error (%s)\n", strerror(errno));
        return NULL;
    }

    /* child process */
    if (child_pid == 0)
    {
        if (type == "r")
        {
            TEMP_FAILURE_RETRY(close(fd[READ]));    //Close the READ end of the pipe since the child's fd is write-only
            TEMP_FAILURE_RETRY(dup2(fd[WRITE], 1)); //Redirect stdout to pipe
        }
        else
        {
            TEMP_FAILURE_RETRY(close(fd[WRITE]));    //Close the WRITE end of the pipe since the child's fd is read-only
            TEMP_FAILURE_RETRY(dup2(fd[READ], 0));   //Redirect stdin to pipe
        }

        setpgid(child_pid, child_pid); //Needed so negative PIDs can kill children of /bin/sh
        char model_name[50] = {0,};
        __system_property_get("ro.product.model", model_name);

        if (0 == strcmp(model_name, "BIP-AI100") || 0 == strcmp(model_name, "BID-AI100")||
            0 == strcmp(model_name, "BFX-AT100") || 0 == strcmp(model_name, "BFX-UA300")||
            0 == strcmp(model_name, "BIP-UW200") || 0 == strcmp(model_name, "BID-AT200")||
            0 == strcmp(model_name, "BFX-UH200"))
        {
            execl("/bin/sh", "/bin/sh", "-c", command.c_str(), NULL);
        }
        else
        {
             execl("/system/bin/busybox", "sh", "-c", "logcat", NULL);
        }
        return NULL;
    }
    else
    {
        if (type == "r")
        {
            TEMP_FAILURE_RETRY(close(fd[WRITE])); //Close the WRITE end of the pipe since parent's fd is read-only
        }
        else
        {
            TEMP_FAILURE_RETRY(close(fd[READ])); //Close the READ end of the pipe since parent's fd is write-only
        }
    }

    pid = child_pid;

    if (type == "r")
    {
        return fdopen(fd[READ], "r");
    }

    return fdopen(fd[WRITE], "w");
}

static int pclose2(FILE * fp, pid_t pid)
{
    int stat = 0;
  
    TEMP_FAILURE_RETRY(fclose(fp));

    if(pid > 0){      
        while (waitpid(pid, &stat, 0) == -1)
        {
            if (errno != EINTR)
            {
                stat = -1;
                break;
            }
        }
    }
    return stat;
}

static void slogcat_release(void){
    pthread_mutex_lock(&g_mutex); 
    if(pid > 0){
        kill(pid, SIGKILL);
    }
  
    if(fp){ 
        int ret =TEMP_FAILURE_RETRY(pclose2(fp, pid));
        //LOGI("QSM : Logcat state is %d(%s)\n", ret, strerror(errno));

        fp = NULL;
    }
    pid = -1;   
    pthread_mutex_unlock(&g_mutex);
}

static void slogcat_sigpipe_handler(int /*unused*/)
{
    LOGI("QSM Log System : Logcat kernel slogcat_sigpipe_handler");
    slogcat_release();
}

static int slogcat_get_android_version(){
    char value[PROP_VALUE_MAX];
    int ver;
    
    __system_property_get("ro.build.version.sdk", value);
    ver = atoi(value);
    LOGI("QSM Log System : Logcat kernel android version : %d", ver);
    return ver;
}

static bool s_logcat_check_duplicated_klog(char* buf, int os_ver) {
    char* str, *str_msg;
    int len;
    int s1,s2,s3,s4,s5,s6, s7, s8;
    char level;
    char log_buffer[1024];

    if(!buf){
        return false;
    }

    if (os_ver <= LAST_SDK_VER_OLD_GET_KLOG) {
        str = strstr(buf, "[");
        if (str) {
            if (NULL == str+1 || NULL == strstr(str + 1, "]")) {
                return false;
            }
            str++;
            while(' ' == *str) {
                str++;
            }
            if (*str) {
                int sec, us;
                if (2 == sscanf(buf, "%d.%d", &sec, &us)) {
                    long last_time = s_last_log_sec * 1000000 + s_last_log_ms;
                    long curr_time = sec * 1000000 + us;
                    if (curr_time < last_time) {
                        return true;
                    }
                    s_last_log_sec = sec;
                    s_last_log_ms = us;
                }
            }
        }
        return false;
    }
    //12-29 18:44:40.333  5320  7769 D
    if (sscanf(buf, "%2d-%2d %2d:%2d:%2d.%3d %5d %5d %1c %s"
        ,&s1, &s2, &s3, &s4, &s5, &s6, &s7, &s8, &level, log_buffer) != 10){
        return false;
    }
    log_buffer[1023] = 0;

    long multi_sec = 1000;
    long multi_min = 60 * multi_sec;
    long multi_hour = 60 * multi_min;
    long multi_day = 24 * multi_hour;
    long multi_month = 30 * multi_day;
    long curr_time = s1 * multi_month + s2 * multi_day + s3 * multi_hour + s4 * multi_min
        + s5 * multi_sec + s6;
    long last_time = s_last_log_month * multi_month + s_last_log_day * multi_day
         + s_last_log_hour * multi_hour + s_last_log_min * multi_min
         + s_last_log_sec * multi_sec + s_last_log_ms;
    if (1 == s1 && 12 == s_last_log_month) {
        curr_time += 12 * multi_month;
    }
    if (curr_time < last_time) {
        //LOGI("QSM Log System : s_logcat_check_duplicated_klog - skip kmsg, last check time : %2d-%2d %2d:%2d:%2d.%3d,   curr log time : %2d-%2d %2d:%2d:%2d.%3d"
            //, s_last_log_month, s_last_log_day, s_last_log_hour, s_last_log_min, s_last_log_sec, s_last_log_ms
            //, s1, s2, s3, s4, s5, s6);
        return true;
    }
    s_last_log_month = s1;
    s_last_log_day = s2;
    s_last_log_hour = s3;
    s_last_log_min = s4;
    s_last_log_sec = s5;
    s_last_log_ms = s6;

    return false;
}

static void slogcat_android_ver_28(char* buff){
    char* str, *str_msg;
    int len;    
    int s1,s2,s3,s4,s5,s6, s7, s8;
    char level;
    char log_buffer[200];
        
    if(!buff){
        return;
    }

    //12-29 18:44:40.333  5320  7769 D
    if (sscanf(buff, "%2d-%2d %2d:%2d:%2d.%3d %5d %5d %1c %s"
        ,&s1, &s2, &s3, &s4, &s5, &s6, &s7, &s8, &level, log_buffer) != 10){
        return;
    }

    if(log_buffer[0] == ':'){
        str = strstr(buff, ":");
        if(str){
            char* str_second = strstr(str+1, ":");
            if(str_second){
                str_msg = strstr(str_second+1, ":");
                if(!str_msg){
                    return;
                }
                
                str_msg++; //':'
                
                len = strlen(str_msg);                    
                while(len > 0){
                    char tmp = *str_msg;
         
                    if(tmp == ' '){
                        str_msg++;
                        len--;
                        continue;
                    }
                    else if(tmp == '\0'){
                     len = 0;
                        break;
                    }
                    break;
                }
                
                if(len <= 0){
                    return;
                }
#ifdef ERROR_FATAL_LOG_INTO_KERNEL_LOG
                if(memcmp(str_msg,(char*)"[user]:", 7) == 0){
                    return;
                }
#endif
                //LOGD("[kmsg] : %s", str_msg);
                log_shared_put_data(str_msg, strlen(str_msg));
            }
        }
    }else{
        str = strstr(buff, log_buffer);
        if(str){
#ifdef ERROR_FATAL_LOG_INTO_KERNEL_LOG
            if(memcmp(str,(char*)"[user]:", 7) == 0){
                return;
            }
#endif
            //LOGD("[kmsg] : %s", str);
            log_shared_put_data(str, strlen(str));
        }
    }
}

static void slogcat_android_ver_29(char* buff){
    char* str;
    int len;    
        
    if(!buff){
        return;
    }

    str = strstr(buff, "[");
    if(str){
        char* str_msg = strstr(str+1, "]");
        if(str_msg){
                                
            str_msg++; //']'
            
            len = strlen(str_msg);
            while(len > 0){
                char tmp = *str_msg;
     
                if(tmp == ' '){
                    str_msg++;
                    len--;
                    continue;
                }
                else if(tmp == '\0'){
                    len = 0;
                    break;
                }
                break;
            }                                       
            
            if(len < 7){
                return;
            }

#ifdef ERROR_FATAL_LOG_INTO_KERNEL_LOG
            if(memcmp(str_msg,(char*)"[user]:", 7) == 0){
                return;
            }
#endif
            //LOGD("[kmsg] : %s", str_msg);
            log_shared_put_data(str_msg, strlen(str_msg));
        }
    }
}

static void slogcat_handle_klog_msg(char* msg, int os_ver) {
    if (NULL == msg) {
        return;
    }
    if(os_ver >= 30){
        /* NEED TO CHECK */
        slogcat_android_ver_29(msg);
    }else if(os_ver == 29){
        /* android 10 */
        slogcat_android_ver_29(msg);
    }else if(os_ver == 28){
        /* android 9 */
        slogcat_android_ver_28(msg);
    }else {
        /* NEED TO CHECK */
        slogcat_android_ver_29(msg);
    }
}

static int slogcat_init(int sdk_ver) {
    fp = NULL;
    pid = -1;

    if (sdk_ver <= LAST_SDK_VER_OLD_GET_KLOG) {
        fp = popen("/system/bin/cat /proc/kmsg", "r");
        if (fp == NULL) {
            LOGW("QSM Log System : Logcat kernel thread end - popen failed. thread end");
            return -1;
        }
        LOGW("QSM Log System : logcat kernel open success fp(%p)", fp);
    } else {
        fp = popen2("logcat -b kernel 2>/dev/null", "r", pid);
        if(fp == NULL || pid < 0) {
            LOGW("QSM Log System : Logcat kernel thread end - popen failed. thread end");
            return -1;
        }
        LOGW("QSM Log System : logcat kernel open success pid(%d)", pid);
    }

    return 0;
}


static int slogcat_process_klog(int sdk_ver) {
    int ret = 0;
    char buff[BUFFER_SIZE] = {0,};

    if (-1 == slogcat_init(sdk_ver)) {
        LOGW("QSM Log System : Logcat kernel init fail");
        return -1;
    }

    while(sRun && fp && fgets(buff, BUFFER_SIZE, fp) != NULL)
    {
        if(g_is_connection_restricted ||  g_sleep_status || !qsm_log_switch_is_signature_on() || !qsm_is_connected() || !sRun){
            LOGI("QSM Log System : logcat kernel - connection restricted(%d) or sleep status(%d) or signature on(%d) or qsm_is_connected(%d) or sRun(%d)"
                , g_is_connection_restricted, g_sleep_status, qsm_log_switch_is_signature_on(), qsm_is_connected(), sRun);
            ret = -1;
            break;
        }

        if(strlen(buff) <= 0) {
            continue;
        }

        if (s_logcat_check_duplicated_klog(buff, sdk_ver)) {
            continue;
        }

        slogcat_handle_klog_msg(buff, sdk_ver);
    }

    slogcat_release();
    return ret;
}

/*******************************************
* GLOBAL FUNCTION
********************************************/
void* module_kernel_log_system_thread(void* ) {
    int sdk_version = slogcat_get_android_version();

    g_kernel_log_system_thread_done = true;

    slogcat_release();

    fp = NULL;
    pid = -1;
    sRun = true;

    LOGI("QSM Log System : Logcat kernel start thread");

    signal(SIGPIPE, slogcat_sigpipe_handler);
    
    while(sRun){
        if (-1 == slogcat_process_klog(sdk_version)) {
            break;
        }

        sleep(RESTART_SLEEP_TIME_SEC);
        LOGW("QSM Log System : Logcat kernel again - sRun : %d", sRun);
    }

    g_kernel_log_system_thread_done = false;

    LOGI("QSM Log System : Logcat kernel thread end");
    return NULL;
}

void module_stop_kernel_log_system_thread(void) {
    sRun = false;
    slogcat_release();  
}
