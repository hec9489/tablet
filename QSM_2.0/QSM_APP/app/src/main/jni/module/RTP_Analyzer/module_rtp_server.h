
#ifndef MODULE_RTP_SERVER_H
#define MODULE_RTP_SERVER_H

/*****************************************
GLOBAL FUNCTIONS
******************************************/

void* module_RTP_analyzer_thread(void* );

#endif //MODULE_RTP_SERVER_H