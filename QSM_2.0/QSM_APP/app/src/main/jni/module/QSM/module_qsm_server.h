
#ifndef SKB_QSM_SERVER_H
#define SKB_QSM_SERVER_H

/*=================================
DEFINE
==================================*/

#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
#define QSM_VERSION "qsm_server_0.9.7"
#else
#define QSM_VERSION "1.0.9"
#endif/*TABLET_BUILD*/

typedef enum LOG_TYPE{
    INT,
    FLOAT,
    STRING,
    JSON
}TYPE_T;

enum{
  SET_WATERMARK = 1,
    REQUEST_WATERMARK = SET_WATERMARK,
};
enum{
  LIVE_OFF = 0,
  LIVE_MIN = LIVE_OFF,
  LIVE_ON,
  LIVE_MAX = LIVE_ON
};
enum{
  VISIBILITY_ENABLE = 1,
  VISIBILITY_MIN = VISIBILITY_ENABLE,
  VISIBILITY_DISABLE,
  VISIBILITY_ENABLE_BY_FORCE,
  VISIBILITY_MAX = VISIBILITY_ENABLE_BY_FORCE
};

typedef struct WATERMARK_STATUS{
    char visibility;
    char live;
}WATERMARK_STATUS_T;

typedef struct RTP_STATUS{
    int32_t error_occur;
    int32_t pkt_duplication;
    int32_t pkt_loss;
    int32_t pkt_out_of_order;
    int32_t jitter_error;
}RTP_STATUS_T;

/*=================================
FUNCTIONS
==================================*/
void qsm_start_send_thread(void);
void qsm_start_log_server(void);
void qsm_thread_join(void);
int  qsm_send(char* data, int size, int qos);
int  qsm_send_command_to_client(char* data, int size);
int  qsm_is_connected(void);
void qsm_init_rtp_send_interval(void);
void qsm_init_zero_rtp_send_interval(void);
void qsm_clear_rtp_status(void);
void qsm_send_rtp_status(void);

#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
int s_qsm_recv_for_client(const char* data, int length);
int  qsm_send_command_to_client_cb(char* data, int size);
void s_qsm_init_data_parse(char *data);
#endif/*TABLET_BUILD*/

#endif //SKB_QSM_SERVER_H
