
#ifndef SKB_QSM_LOG_SWITCH_H
#define SKB_QSM_LOG_SWITCH_H

/*******************************************
* DEFINE
********************************************/
#define SIGNATURE_MAX_COUNT         2010
#define SIGNATURE_MAX_LEN           2048
#define JIRA_ID_MAX_LEN             32
#define SUBSCRIBE_TOPIC_MAX_LEN     64
#define LOG_TIME_MAX_LEN            32
#define SIG_PRINT_LOG_MAX_LEN       (JIRA_ID_MAX_LEN + SIGNATURE_MAX_LEN + 9)

#define SIG_LOG_DIVIDER             " ##### "
#define SIG_NO_JIRA_ID              "no_jira_id"
#define SIG_JIRA_ID_DIVIDER         " : "

#define CHECK_STB_LOG_SWITCH        0x1
#define CHECK_STB_WATERMARK         0x2
#define CHECK_STB_ALL               (CHECK_STB_LOG_SWITCH | CHECK_STB_WATERMARK)

typedef struct SIGNATURE_ITEM{
    char message[SIGNATURE_MAX_LEN];
    char found_log[SIGNATURE_MAX_LEN];
    char jira_id[JIRA_ID_MAX_LEN];
    char log_time[LOG_TIME_MAX_LEN];
    timeval time_base;
    short index;
    short count;
}SIGNATURE_ITEM_T;

typedef struct LOG_ITEM{
    char parameter_name[40];
    bool log_on;
}LOG_ITEM_T;

typedef struct LOG_ITEM2{
    char parameter_name[40];
    bool log_on;	
}LOG_SWITCH_T;

typedef struct SUBSCRIBE_ITEM {
    char topic_name[SUBSCRIBE_TOPIC_MAX_LEN];
    int qos_type;
} SUBSCRIBE_ITEM_T;


#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
#define INTERNAL_DIR                      "data/data"
#define APP_CONFIG_DIR                    "btf/config"
#define QSM_CONFIG_DIR                    "btf/qsm/config"
#define QSM_SOCKET_DIR                    "btf/qsm/socket"
#define SERVER_LIST_CONF                  "server-list.conf"
#define WATER_MARK_CONF                   "watermark.conf"
#define SIGNATURE_CONF                    "signature.conf"
#define QSM_INIT_DATA_JSON                "qsm_init_data.json"
#define QSM_LOG_SWITCH_DATA_JSON          "qsm_log_switch_data.json"
#define QSM_RTP_SEND_INTERVAL_MINUTES     "qsm_rtp_send_interval_minutes"
#define TVSERVICE_SETTING_PROPERTIES_XML  "tvservice-setting-properties.xml"
#define TVSERVICE_SYSTEM_PROPERTIES_XML   "tvservice-system-properties.xml"
#define HDMI_EDIT_XML                     "hdmi-edid.xml"
#define ISQMSAGENT_XML                    "isqmsAgent.xml"
#define UPGRADESVC_LOG                    "upgradesvc.log"
#endif/*TABLET_BUILD*/

/*******************************************
* GLOBAL FUNCTION
********************************************/
int     qsm_read_file(char* name, char* buffer, int len);
int     qsm_write_file(char* name, std::string data);
void   qsm_log_switch_init(void);
int      qsm_log_switch_check(char* dst);
char* qsm_log_switch_make_test_log_switch(void);
char* qsm_log_switch_parse_INIT(char* dst, int* len);
char* qsm_log_switch_parse_others(char* type, char* dst, int* len);
int     qsm_log_switch_read_init_json();
void   qsm_log_switch_check_system_info(void);
void   qsm_log_switch_send_cold_boot(void);
void   qsm_log_switch_check_update_status(void);
void qsm_log_switch_check_signature(const char* log, int len);
void   qsm_log_switch_send_info_after_one_day(void);
void qsm_navigator_client_send(char* buf);
void qsm_update_rtp_status(char* status);
char* qsm_log_switch_get_mac_addr(void);
#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
void qsm_log_switch_set_mac_addr(char *addr, int length);
char* qsm_log_switch_get_model_name(void);
void qsm_log_switch_set_model_name(char *model_name, int length);
#endif/*TABLET_BUILD*/
char* qsm_log_switch_handle_mqtt_msg(char* dst);
bool   qsm_log_switch_is_signature_on(void);
bool   qsm_log_switch_is_Navigator_on(void);
bool   qsm_log_switch_is_Rtp_on(void);
bool   qsm_rtp_checker_on(void);
#endif //SKB_QSM_LOG_SWITCH_H
