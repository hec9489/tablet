/*****************************************************************
** SPTek created
** This daemon gets kernel and user messages and send them to remote server
** and get memory information and put it into user log and send it to remote server
******************************************************************/

#define LOG_TAG "skb_qsm"
#define LOG_NDEBUG 0

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/un.h>
#include <errno.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <json/json.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <sys/system_properties.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

#include "xml2json.hpp"

#include "shared_buffer.h"
#include "module_qsm_server.h"
#include "module_qsm_log_switch.h"

#include "WuManber.h"
#include "mwm.h"

#ifdef NDK_BUILD
#include <log.h>
#include <sys/vfs.h>
  #define statvfs statfs
#else
#include <utils/Log.h>
#include <sys/statvfs.h>
#endif

#include <net/if.h>

#include "logagentcli.h"


using namespace std;

/*******************************************
* DEFINE
********************************************/

#define DEBUG_SIGNATURE_MSG
#define SIGNATURE_EVENT_TO_LOGAGENT
//#define USE_OS_MAC_ADDR

#define MIN(a,b) a < b ? a : b
//#define TEST_FOR_SIGNATURE_MODE
//#define SIGNATURE_TEST_MESSAGES /* hardcoded signature messages */
#define TIME_LIMIT (60*60*24)
#define SIGNATURE_ON
#define SIGNATURE_MVM
#define SIGNATURE_MODE_NONE     0
#define SIGNATURE_MODE_WM       1
#define SIGNATURE_MODE_MWM      2
#define SIGNATURE_MODE_MEMCMP   3
#define SIGNATURE_MODE_MAX      SIGNATURE_MODE_MEMCMP

//#define CHECK_SIGNATURE_SEARCH_TIME
#define UPDATE_CHECK_TIME_GAP_MIN 8

#define PREFIX_KEY_FOR_ORDER    "*PREFIX_KEY_ORDER:"
#define PREFIX_KEY_LEN          22

#define NAVIGATOR_TIME_LENGTH   18

#define DUP_LOG_SKIP_TIME_SEC   60

/*property file path*/
#define D014_SETTING_PROPERTY_FILE  "/data/btv_home/config/tvservice-setting-properties.xml"
#define D014_SYSTEM_PROPERTY_FILE   "/data/btv_home/config/tvservice-system-properties.xml"


/*tvservice-setting-properties.xml ?? default ?? ????*/
#define D014_SETTING_STR_DISPLAY_HDR_MODE       "DISPLAY_HDR_MODE"
#define D014_SETTING_STR_DISPLAY_RESOLUTION     "DISPLAY_RESOLUTION"
#define D014_SETTING_STR_GRAPHIC_BRIGHTNESS     "GRAPHIC_BRIGHTNESS"
#define D014_SETTING_STR_GRAPHIC_CONTRAST        "GRAPHIC_CONTRAST"
#define D014_SETTING_STR_GRAPHIC_HUE                 "GRAPHIC_HUE"
#define D014_SETTING_STR_GRAPHIC_SATURATION     "GRAPHIC_SATURATION"
#define D014_SETTING_STR_MIC_ECHOLEVEL              "MIC_ECHOLEVEL"
#define D014_SETTING_STR_MIC_ONOFF                     "MIC_ONOFF"
#define D014_SETTING_STR_MIC_VOLUME                   "MIC_VOLUME"
#define D014_SETTING_STR_SCREENOFFSET_W            "SCREENOFFSET_W"
#define D014_SETTING_STR_SCREENOFFSET_H             "SCREENOFFSET_H"
#define D014_SETTING_STR_SCREENOFFSET_X             "SCREENOFFSET_X"
#define D014_SETTING_STR_SCREENOFFSET_Y             "SCREENOFFSET_Y"
#define D014_SETTING_STR_SET_HDMIAUDIOMODE      "SET_HDMIAUDIOMODE"


/*tvservice-system-properties.xml ?? default ?? ????*/
#define D014_SYSTEM_STR_ADULTCLASS       "ADULTCLASS"
#define D014_SYSTEM_STR_ADULTMENU_       "ADULTMENU"
#define D014_SYSTEM_STR_AUTOHOMEMODE       "AUTOHOMEMODE"
#define D014_SYSTEM_STR_AUTOREBOOT       "AUTOREBOOT"
#define D014_SYSTEM_STR_AUTOSLEEP       "AUTOSLEEP"
#define D014_SYSTEM_STR_BPOINTDEDUCT       "BPOINTDEDUCT"
#define D014_SYSTEM_STR_CHARACTERID       "CHARACTERID"
#define D014_SYSTEM_STR_CHILDRENSEELIMIT       "CHILDRENSEELIMIT"
#define D014_SYSTEM_STR_CHILDRENSEELIMITTIME       "CHILDRENSEELIMITTIME"
#define D014_SYSTEM_STR_CONSECUTIVEPLAY       "CONSECUTIVEPLAY"

#define D014_SYSTEM_STR_FIRSTSCREENMODE       "FIRSTSCREENMODE"
#define D014_SYSTEM_STR_HOME_UI_VERSION       "HOME_UI_VERSION"
#define D014_SYSTEM_STR_IPTVMINIEPGVIEW       "IPTVMINIEPGVIEW"
#define D014_SYSTEM_STR_KIDS_SAFETY_PASSWORD       "KIDS_SAFETY_PASSWORD"
#define D014_SYSTEM_STR_KIDS_VIRGIN_ENTRY       "KIDS_VIRGIN_ENTRY"
#define D014_SYSTEM_STR_KIDS_SAFETY_MODE       "KIDS_SAFETY_MODE"
#define D014_SYSTEM_STR_LAST_VOLUME       "LAST_VOLUME"
#define D014_SYSTEM_STR_REMOCONTYPE       "REMOCONTYPE"
#define D014_SYSTEM_STR_PROPERTY_AUTO_REBOOT       "PROPERTY_AUTO_REBOOT"
#define D014_SYSTEM_STR_PROPERTY_B_POINT_PER_MONTH_DEDUCT       "PROPERTY_B_POINT_PER_MONTH_DEDUCT"

#define D014_SYSTEM_STR_PROPERTY_CONTENTS_UPDATE       "PROPERTY_CONTENTS_UPDATE"
#define D014_SYSTEM_STR_PROPERTY_ENDINGSYNOP       "PROPERTY_ENDINGSYNOP"
#define D014_SYSTEM_STR_PROPERTY_HDCP       "PROPERTY_HDCP"
#define D014_SYSTEM_STR_PROPERTY_HOME_PLAY_TYPE       "PROPERTY_HOME_PLAY_TYPE"
#define D014_SYSTEM_STR_PROPERTY_IP_SETTING       "PROPERTY_IP_SETTING"
#define D014_SYSTEM_STR_PROPERTY_LIVE_CHANNEL_INFO       "PROPERTY_LIVE_CHANNEL_INFO"
#define D014_SYSTEM_STR_PROPERTY_MOUSE_POINTER_VISIBLE       "PROPERTY_MOUSE_POINTER_VISIBLE"
#define D014_SYSTEM_STR_PROPERTY_NETWORK_MODE       "PROPERTY_NETWORK_MODE"
#define D014_SYSTEM_STR_PROPERTY_MULTIVIEW_SCREEN_TYPE       "PROPERTY_MULTIVIEW_SCREEN_TYPE"
#define D014_SYSTEM_STR_PROPERTY_REMOCON_KEYPAD_SETTING       "PROPERTY_REMOCON_KEYPAD_SETTING"

#define D014_SYSTEM_STR_PROPERTY_SEARCHED_WORD_LIST       "PROPERTY_SEARCHED_WORD_LIST"
#define D014_SYSTEM_STR_PROPERTY_STB_VER       "PROPERTY_STB_VER"
#define D014_SYSTEM_STR_PROPERTY_TV_POWER_CONTROL       "PROPERTY_TV_POWER_CONTROL"
#define D014_SYSTEM_STR_PROPERTY_WEATHER_WIDGET       "PROPERTY_WEATHER_WIDGET"
#define D014_SYSTEM_STR_PROPERTY_WM_ID       "PROPERTY_WM_ID"
#define D014_SYSTEM_STR_PURCHASECERTIFICATION       "PURCHASECERTIFICATION"
#define D014_SYSTEM_STR_STBID       "STBID"
#define D014_SYSTEM_STR_USERADDR       "USERADDR"
#define D014_SYSTEM_STR_USERID       "USERID"
#define D014_SYSTEM_STR_USERNAME       "USERNAME"

#define LOG_SWITCH_INFO_FILE        "/data/btv_home/config/qsm_log_switch_data.json"
#define LOG_SWITCH_WATERMARK_FILE   "/data/btv_home/config/watermark.conf"

/* RTP status */
#define PACKET_DUPLICATION     100
#define PACKET_OUT_OF_ORDER    200
#define PACKET_LOSS            300
#define JITTER_ERROR           400
#define RTP_SEND_INTERVAL_FILE        "/data/btv_home/config/qsm_rtp_send_interval_minnutes"
#define RTP_SEND_INTERVAL_DEFAUT_MINUTES 5

enum{
    device_info = 0,
    system_info,
    network_info,
    service_mode,
    stb_upgrade,
    hdmi_plug_inout,
    hdmi_hdcp_version,
    device_info_hdmi,
    device_info_rcu,
    device_info_rcu_error,
    device_info_bt,
    user_config,
    contents_info,
    /*video_info,
    audio_info,
    error_status_info,*/
    server_check_result,
    btv_error,
    vod_error,
    application_error,
    navigator_error,
    rtp_error,
    last_log_switch_item
};
enum {
    D003 = 0,
    D004,
    D005,
    D006,
    D007,
    D008,
    D009,
    D010,
    D011,
    D012,
    D013,
    D014,
    D015,
    /*D016,
    D017,
    D018,*/
    D019,
    D020,
    D021,
    Dxxx_app_err
};

/*******************************************
* LOCAL & GLOBAL VARIABLES
********************************************/

static std::string sOutputConfig;
static Json::Value subObj_d010(Json::objectValue);
static Json::Value subObj_d014_1(Json::objectValue);
static Json::Value subObj_d014_2(Json::objectValue);

char g_tot_time_buf[50];

//static Json::Value jsonSendRoot;
static Json::Value sJsonInitRoot;

int g_tot_mem_size;
int g_tot_disk_size;
int g_mem_limit;
int g_cpu_limit;
int g_disk_limit;

double g_start_time;
double g_one_day_check_time;
bool g_cold_boot_sent;
int g_signature_count;

LOG_ITEM_T g_log_switch_array[last_log_switch_item]={
  {"device_info", false},
  {"system_info", false},
  {"network_info", false},
  {"service_mode", false},
  {"stb_upgrade", false},
  {"hdmi_plug_inout", false},
  {"hdmi_hdcp_version", false},
  {"device_info_hdmi", false},
  {"device_info_rcu", false},
  {"device_info_rcu_error", false},
  {"device_info_bt", false},
  {"user_config", false},
  {"contents_info", false},
  /*{"video_info", false},
  {"audio_info", false},
  {"error_status_info", false},*/
  {"server_check_result", false},
  {"btv_error", false},
  {"vod_error", false},
  {"application_error", false},
  {"navigator_error", false},
  {"rtp_error", false},
};

#ifdef SIGNATURE_ON
#ifdef SIGNATURE_MVM
MWM_STRUCT    *g_mvm_inst;
#endif
SIGNATURE_ITEM_T* g_signatures;
pthread_mutex_t g_signature_mutex;
vector<const char*> g_search_patterns;
WuManber WuManberSearch;
#endif

std::string gJsonFromXml;
char g_mac_addr[100];
char g_device_id[100];
char g_file_read_buffer[1024*1024*2];

#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
char g_model_name[100];
#endif/*TABLET_BUILD*/

char keyArray_d010[100][2000];
char keyArray_d014_1[100][2000];
char keyArray_d014_2[100][2000];

extern volatile bool g_sleep_status;
extern pthread_cond_t g_sleep_condition;
extern pthread_mutex_t g_sleep_mutex;

extern bool g_init_type_received;
int g_signature_mode = SIGNATURE_MODE_NONE;
static bool g_update_check_done;
static int found_idx = -1;

static pthread_mutex_t s_json_data_mutex;

WATERMARK_STATUS_T g_watermark_status;

/* RTP status */
static RTP_STATUS_T g_rtp_status;
static int g_check_rtp_status_duration;
static int g_check_rtp_status_count;

#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
extern char qsmconfigDir[];
#endif/*TABLET_BUILD*/

/*******************************************
* LOCAL FUNCTION
********************************************/
static int  qsm_log_switch_read_log_switch_json();
static void qsm_log_switch_read_watermark_data();
static int  qsm_log_switch_parse_log_switch(Json::Value root);
static int  qsm_log_switch_parse_log_switch(char* dst, bool save_file = false);

static int  qsm_parse_recv_mqtt_msg(char* dst);

bool s_qsm_log_switch_is_all_off() {
    for (int i=0; i<last_log_switch_item; i++) {
        if (g_log_switch_array[i].log_on) {
            return false;
        }
    }
    return true;
}

void remove_prefix_key(std::string& str) {
    std::size_t pos = 0;
    while ((pos = str.find(PREFIX_KEY_FOR_ORDER, pos)) != std::string::npos) {
        str.erase(pos, PREFIX_KEY_LEN);
    }
}

static double now_sec(void) {
    struct timespec res;
    clock_gettime(CLOCK_REALTIME, &res);
    return res.tv_sec + (double) res.tv_nsec / 1e9;
}

static void time_to_string(char* buf, int len, timeval time) {
    if (NULL == buf || len < 20) {
        return;
    }

    struct tm *nowtm;
    char tmbuf[64];

    nowtm = localtime(&time.tv_sec);
    strftime(tmbuf, sizeof(tmbuf), "%Y%m%d%H%M%S", nowtm);
    sprintf(buf, "%s.%03d", tmbuf, (int)(time.tv_usec /1000));
    //LOGI("QSM time to string : %s", buf);
}

static const char* currentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    struct timeval te;

    char       buf[80];

    tstruct = *localtime(&now);
    gettimeofday(&te, NULL);
    int milliseconds = (int)(te.tv_usec/1000);

    strftime(buf, sizeof(buf), "%Y%m%d%H%M%S", &tstruct);
    sprintf(g_tot_time_buf,"%s.%03d",buf, milliseconds);

    return g_tot_time_buf;
}

static inline void skip_space(char** ptr) {
    while (ptr != NULL && *ptr != NULL && **ptr == ' ') {
        (*ptr)++;
    }
}

static inline void extract_numeric(char* ptr) {
    if (ptr != NULL && isdigit(*ptr)) {
        while (isdigit(*ptr)) {
            ptr++;
        }
        *ptr = 0;
    }
}

static char* getJSONFromXML(char* xml_data){
    char chars[] = "@#";

    gJsonFromXml = xml2json(xml_data);
    if (gJsonFromXml.empty()) {
        return NULL;
    }
    //LOGI("QSM getJSONFromXML xml2json : %d", gJsonFromXml.length());

    for (int i = 0; i < 2; ++i)
    {
        gJsonFromXml.erase (std::remove(gJsonFromXml.begin(), gJsonFromXml.end(), chars[i]), gJsonFromXml.end());
    }

    return (char*)gJsonFromXml.c_str();
}

static xmlChar * convertInput(const char *in, const char *encoding)
{
    xmlChar *out;
    int ret;
    int size;
    int out_size;
    int temp;
    xmlCharEncodingHandlerPtr handler;

    if (in == 0)
        return 0;

    handler = xmlFindCharEncodingHandler(encoding);

    if (!handler) {
        LOGI("QSM convertInput: no encoding handler found for '%s'\n",
               encoding ? encoding : "");
        return 0;
    }

    size = (int) strlen(in) + 1;
    out_size = size * 2 - 1;
    out = (unsigned char *) xmlMalloc((size_t) out_size);

    if (out != 0) {
        temp = size - 1;
        ret = handler->input(out, &out_size, (const xmlChar *) in, &temp);
        if ((ret < 0) || (temp - size + 1)) {
            if (ret < 0) {
                LOGI("QSM convertInput: conversion wasn't successful.\n");
            } else {
                LOGI
                    ("QSM convertInput: conversion wasn't successful. converted: %i octets.\n",
                     temp);
            }

            xmlFree(out);
            out = 0;
        } else {
            out = (unsigned char *) xmlRealloc(out, out_size + 1);
            if (out) {
                out[out_size] = 0;  /*null terminating out */
            }
            //LOGI("QSM convertInput: OK\n");
        }
    } else {
        LOGI("QSM convertInput: no mem\n");
    }

    return out;
}

static bool isUpdateSuccess(){
    int nFd, ret;
    char nBuffer[1024*400];
    bool isSuccess = false;

    memset(nBuffer, 0, sizeof(nBuffer));
    nFd = open("/cache/recovery/last_log", O_RDONLY);
    if(nFd < 0)
    {
        LOGI("isUpdateSuccess : fail to open /cache/recovery/last_log");
        return false;
    }

    ret = read(nFd, nBuffer, sizeof(nBuffer));
    close(nFd);

    if(ret > 0){
        nBuffer[ret]='\0';

        char* str = strstr(nBuffer, "SetProgress percentage[100]");
        if(str){
            char* str_second = strstr(str, "SetProgress percentage[100]");
            if(str_second){
                LOGI("QSM Update install success");
                isSuccess = true;
            }else{
                char* str_second = strstr(str, "Writing superblocks and filesystem accounting information: done");
                if(str_second){
                    LOGI("QSM Update install success");
                    isSuccess = true;
                }
            }
        }
        if (!isSuccess) {
            if(strstr(nBuffer, "RECOVERY SUCCESS") || strstr(nBuffer, "RECOVERY_SUCCESS")){
                LOGI("QSM Update install success found");
                isSuccess = true;
            }
        }
    }

    return isSuccess;
}

void s_qsm_log_switch_get_device_info_INIT_file(void)
{
    int ret, count;

#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
    char qsm_init_data_json[128];
	sprintf(qsm_init_data_json, "%s/%s", qsmconfigDir, QSM_INIT_DATA_JSON);
    std::ifstream file_input(qsm_init_data_json);
#else
    std::ifstream file_input("/data/btv_home/config/qsm_init_data.json");
#endif/*TABLET_BUILD*/
    Json::Reader reader;
    Json::Value root;

    if(file_input.is_open()){
        bool parsingSuccessful = reader.parse( file_input, root );
        file_input.close();
        if ( !parsingSuccessful)
        {
            LOGI("QSM : Failed to parse configuration from file : %s", reader.getFormatedErrorMessages().c_str());
            return;
        }

        for( Json::ValueIterator itr = root.begin() ; itr != root.end() ; itr++ ) {

            const char* key = (char*)itr.key().asString().c_str();

            if(strcmp((char*)key,"mac_address")==0){
                if((*itr).isString()){
                    strcpy(g_mac_addr, (*itr).asString().c_str());
                    LOGI("QSM : qsm_init_data file : mac_addr =%s", g_mac_addr);
                }
            }else if(strcmp((char*)key,"device_id")==0){
                if((*itr).isString()){
                    strcpy(g_device_id, (*itr).asString().c_str());
                    LOGI("QSM : qsm_init_data file : device_id =%s", g_device_id);
                }
            }
        }
    }
    else{
        LOGI("QSM : qsm_init_data file does not exist");
    }
}


bool   is_setting_property_key(char* keyString)
{
    if(strcmp(keyString,D014_SETTING_STR_DISPLAY_HDR_MODE) ==0 ||
       strcmp(keyString,D014_SETTING_STR_DISPLAY_RESOLUTION) ==0 ||
       strcmp(keyString,D014_SETTING_STR_GRAPHIC_BRIGHTNESS) ==0 ||
       strcmp(keyString,D014_SETTING_STR_GRAPHIC_CONTRAST) ==0 ||
       strcmp(keyString,D014_SETTING_STR_GRAPHIC_HUE) ==0 ||
       strcmp(keyString,D014_SETTING_STR_GRAPHIC_SATURATION) ==0 ||
       strcmp(keyString,D014_SETTING_STR_MIC_ECHOLEVEL) ==0 ||
       strcmp(keyString,D014_SETTING_STR_MIC_ONOFF) ==0 ||
       strcmp(keyString,D014_SETTING_STR_MIC_VOLUME) ==0 ||
       strcmp(keyString,D014_SETTING_STR_SCREENOFFSET_W) ==0 ||
       strcmp(keyString,D014_SETTING_STR_SCREENOFFSET_H) ==0 ||
       strcmp(keyString,D014_SETTING_STR_SCREENOFFSET_X) ==0 ||
       strcmp(keyString,D014_SETTING_STR_SCREENOFFSET_Y) ==0 ||
       strcmp(keyString,D014_SETTING_STR_SET_HDMIAUDIOMODE) ==0)

    {
        return true;
    }

    return false;
}

bool   is_system_property_key(char* keyString)
{
    if(strcmp(keyString,D014_SYSTEM_STR_ADULTCLASS) ==0 ||

       strcmp(keyString,D014_SYSTEM_STR_ADULTMENU_) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_AUTOHOMEMODE) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_AUTOREBOOT) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_AUTOSLEEP) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_BPOINTDEDUCT) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_CHARACTERID) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_CHILDRENSEELIMIT) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_CHILDRENSEELIMITTIME) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_CONSECUTIVEPLAY) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_FIRSTSCREENMODE) ==0 ||

       strcmp(keyString,D014_SYSTEM_STR_HOME_UI_VERSION) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_IPTVMINIEPGVIEW) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_KIDS_SAFETY_PASSWORD) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_KIDS_VIRGIN_ENTRY) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_KIDS_SAFETY_MODE) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_LAST_VOLUME) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_REMOCONTYPE) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_PROPERTY_AUTO_REBOOT) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_PROPERTY_B_POINT_PER_MONTH_DEDUCT) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_PROPERTY_CONTENTS_UPDATE) ==0 ||

       strcmp(keyString,D014_SYSTEM_STR_PROPERTY_ENDINGSYNOP) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_PROPERTY_HDCP) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_PROPERTY_HOME_PLAY_TYPE) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_PROPERTY_IP_SETTING) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_PROPERTY_LIVE_CHANNEL_INFO) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_PROPERTY_MOUSE_POINTER_VISIBLE) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_PROPERTY_NETWORK_MODE) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_PROPERTY_MULTIVIEW_SCREEN_TYPE) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_PROPERTY_REMOCON_KEYPAD_SETTING) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_PROPERTY_SEARCHED_WORD_LIST) ==0 ||

       strcmp(keyString,D014_SYSTEM_STR_PROPERTY_STB_VER) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_PROPERTY_TV_POWER_CONTROL) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_PROPERTY_WEATHER_WIDGET) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_PROPERTY_WM_ID) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_PURCHASECERTIFICATION) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_STBID) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_USERADDR) ==0 ||
       strcmp(keyString,D014_SYSTEM_STR_USERID) ==0 ||

       strcmp(keyString,D014_SYSTEM_STR_USERNAME) ==0)

    {
        return true;
    }

    return false;
}


void s_qsm_log_switch_parse_xml_json_d010(Json::Value& subRoot, char* dst)
{
    int ret, count;
    Json::Value root;
    Json::Value secondRoot;
    Json::Reader reader;
    std::string config_doc;
    bool parsingSuccessful;
    int index = 0;

    if (NULL == dst) {
        LOGE("QSM : parse d010 - invalid param NULL");
        return;
    }

    config_doc = dst;
    parsingSuccessful = reader.parse( config_doc, root );
    if ( !parsingSuccessful )
    {
        LOGE("QSM : Failed to parse configuration : %s", reader.getFormatedErrorMessages().c_str());
        return;
    }

    subRoot.clear ();

    /* remove "hdmi_edid" */
    Json::Value::iterator itr = root.begin() ;
    if(strcmp(itr.key().asString().c_str(),(char*)"hdmi_edid")==0){
        if((*itr).isString()){
            secondRoot = (*itr).asString().c_str();
        }
        else if( (*itr).isObject()) {
            Json::Value value = (*itr);
            secondRoot = value;
        }
    }else{
        LOGE("QSM : Failed to parse hdmi edit");
        return;
    }

    for( Json::Value::iterator itr = secondRoot.begin() ; itr != secondRoot.end() ; itr++ ) {

        strcpy((char*)&keyArray_d010[index][0], (char*)itr.key().asString().c_str());
        const char* key = (char*)&keyArray_d010[index++][0];

        if((*itr).isString()){
            subRoot[key] = (*itr).asString().c_str();
        } else if( (*itr).isBool() ) {
              subRoot[key] = (*itr).asBool();
        } else if( (*itr).isInt() ) {
            subRoot[key] = (*itr).asInt();
        } else if( (*itr).isUInt() ) {
            subRoot[key] = (*itr).asUInt();
        } else if( (*itr).isDouble() ) {
            subRoot[key] = (*itr).asDouble();
        } else if( (*itr).isArray() ) {
            subRoot[key] = Json::Value((*itr));
        }else if( (*itr).isNull() ) {
            //LOGE( "QSM Item value is null");
        }else if( (*itr).isObject() ) {
            Json::Value value = (*itr);
            subRoot[key] = value;
        }else{
            LOGE( "QSM Item value is invalid");
        }
    }

}

#if 1
void s_qsm_log_switch_parse_xml_json_d014_1(Json::Value& subRoot, char* dst)
{
    int ret, count;
    Json::Value root;
    Json::Value secondRoot;
    Json::Value lastRoot;
    Json::Reader reader;
    std::string config_doc;
    bool parsingSuccessful;
    int index = 0;

    if (NULL == dst) {
        LOGE("QSM : parse d014 - invalid param NULL");
        return;
    }

    config_doc = dst;
    parsingSuccessful = reader.parse( config_doc, root );
    if ( !parsingSuccessful )
    {
        LOGI("QSM : Failed to parse configuration : %s", reader.getFormatedErrorMessages().c_str());
        return;
    }

    subRoot.clear ();

#if 1
    /* remove "properties" */
    Json::Value::iterator itr = root.begin() ;
    if(strcmp(itr.key().asString().c_str(),(char*)"properties")==0){
        if((*itr).isString()){
            secondRoot = (*itr).asString().c_str();
        }
        else if( (*itr).isObject()) {
            Json::Value value = (*itr);
            secondRoot = value;
        }
    }else{
        LOGE("QSM : Failed to parse setting properties");
        return;
    }

    /* remove "entry" */
    for( Json::Value::iterator itr = secondRoot.begin() ; itr != secondRoot.end() ; itr++ ) {
        if(strcmp(itr.key().asString().c_str(),(char*)"entry")==0){
            if((*itr).isString()){
                lastRoot = (*itr).asString().c_str();
            }
            /*else if( (*itr).isObject()) {
                Json::Value value = (*itr);
                lastRoot = value;
            }*/else if( (*itr).isArray() ) {
                Json::Value value = (*itr);
                lastRoot = value;
            }
        }
    }
#endif

    for( Json::Value::iterator itr = lastRoot.begin() ; itr != lastRoot.end() ; itr++ ) {

        strcpy((char*)&keyArray_d014_1[index][0], (char*)itr.key().asString().c_str());
      //  const char* key = (char*)&keyArray_d014_1[index++][0];
        const char* key = (char*)&keyArray_d014_1[index][0];

        if((*itr).isString()){
            subRoot[key] = (*itr).asString().c_str();
        } else if( (*itr).isBool() ) {
              subRoot[key] = (*itr).asBool();
        } else if( (*itr).isInt() ) {
            subRoot[key] = (*itr).asInt();
        } else if( (*itr).isUInt() ) {
            subRoot[key] = (*itr).asUInt();
        } else if( (*itr).isDouble() ) {
            subRoot[key] = (*itr).asDouble();
        } else if( (*itr).isArray() ) {
            subRoot[key] = Json::Value((*itr));
        }else if( (*itr).isNull() ) {
            //LOGE( "QSM Item value is null");
        }else if( (*itr).isObject() ) {
            Json::Value value = (*itr);
            Json::Value::iterator last_itr = value.begin();

            if(!is_setting_property_key((char*)last_itr.key().asString().c_str()))
            {
                continue;
            }

            index++;
            subRoot[(char*)last_itr.key().asString().c_str()] = (*last_itr);
            //LOGE("QSM : sub root :  %s", (char*)last_itr.key().asString().c_str());
        }else{
            LOGE( "QSM Item value is invalid");
        }
    }
}
#else
void s_qsm_log_switch_parse_xml_json_d014_1(Json::Value& subRoot, char* dst)
{
    int ret, count;
    Json::Value root;
    Json::Reader reader;
    std::string config_doc;
    bool parsingSuccessful;
    int index = 0;

    if (NULL == dst) {
        LOGE("QSM : parse d014 - invalid param NULL");
        return;
    }

    config_doc = dst;
    parsingSuccessful = reader.parse( config_doc, root );
    if ( !parsingSuccessful )
    {
        LOGI("QSM : Failed to parse configuration : %s", reader.getFormatedErrorMessages().c_str());
        return;
    }

    subRoot.clear ();

    for( Json::Value::iterator itr = root.begin() ; itr != root.end() ; itr++ ) {

        strcpy((char*)&keyArray_d014_1[index][0], (char*)itr.key().asString().c_str());
        const char* key = (char*)&keyArray_d014_1[index++][0];

        if((*itr).isString()){
            subRoot[key] = (*itr).asString().c_str();
        } else if( (*itr).isBool() ) {
              subRoot[key] = (*itr).asBool();
        } else if( (*itr).isInt() ) {
            subRoot[key] = (*itr).asInt();
        } else if( (*itr).isUInt() ) {
            subRoot[key] = (*itr).asUInt();
        } else if( (*itr).isDouble() ) {
            subRoot[key] = (*itr).asDouble();
        } else if( (*itr).isArray() ) {
            subRoot[key] = Json::Value((*itr));
        }else if( (*itr).isNull() ) {
            //LOGE( "QSM Item value is null");
        }else if( (*itr).isObject() ) {
            Json::Value value = (*itr);
            subRoot[key] = value;
        }else{
            LOGE( "QSM Item value is invalid");
        }
    }
}
#endif

void s_qsm_log_switch_parse_xml_json_d014_2(Json::Value& subRoot, char* dst)
{
    int ret, count;
    Json::Value root;
    Json::Reader reader;
    std::string config_doc;
    bool parsingSuccessful;
    int index = 0;
    Json::Value secondRoot;
    Json::Value lastRoot;

    if (NULL == dst) {
        LOGE("QSM : parse d014-2 - invalid param NULL");
        return;
    }

    config_doc = dst;
    parsingSuccessful = reader.parse( config_doc, root );
    if ( !parsingSuccessful )
    {
        LOGI("QSM : Failed to parse configuration : %s", reader.getFormatedErrorMessages().c_str());
        return;
    }

    subRoot.clear ();

    /* remove "properties" */
    Json::Value::iterator itr = root.begin() ;
    if(strcmp(itr.key().asString().c_str(),(char*)"properties")==0){
        if((*itr).isString()){
            secondRoot = (*itr).asString().c_str();
        }
        else if( (*itr).isObject()) {
            Json::Value value = (*itr);
            secondRoot = value;
        }
    }else{
        LOGE("QSM : Failed to parse setting properties");
        return;
    }

    /* remove "entry" */
    for( Json::Value::iterator itr = secondRoot.begin() ; itr != secondRoot.end() ; itr++ ) {
        if(strcmp(itr.key().asString().c_str(),(char*)"entry")==0){
            if((*itr).isString()){
                lastRoot = (*itr).asString().c_str();
            }else if( (*itr).isArray() ) {
                Json::Value value = (*itr);
                lastRoot = value;
            }
        }
    }


    for( Json::Value::iterator itr = lastRoot.begin() ; itr != lastRoot.end() ; itr++ ) {

        strcpy((char*)&keyArray_d014_2[index][0], (char*)itr.key().asString().c_str());
//        const char* key = (char*)&keyArray_d014_2[index++][0];
        const char* key = (char*)&keyArray_d014_2[index][0];


        if((*itr).isString()){
            subRoot[key] = (*itr).asString().c_str();
        } else if( (*itr).isBool() ) {
              subRoot[key] = (*itr).asBool();
        } else if( (*itr).isInt() ) {
            subRoot[key] = (*itr).asInt();
        } else if( (*itr).isUInt() ) {
            subRoot[key] = (*itr).asUInt();
        } else if( (*itr).isDouble() ) {
            subRoot[key] = (*itr).asDouble();
        } else if( (*itr).isArray() ) {
            subRoot[key] = Json::Value((*itr));
        }else if( (*itr).isNull() ) {
            //LOGE( "QSM Item value is null");
        }else if( (*itr).isObject() ) {
//            Json::Value value = (*itr);
//            subRoot[key] = value;

            Json::Value value = (*itr);
            Json::Value::iterator last_itr = value.begin();

            if(!is_system_property_key((char*)last_itr.key().asString().c_str()))
            {
                continue;
            }

            index++;
            subRoot[(char*)last_itr.key().asString().c_str()] = (*last_itr);

//            LOGE("QSM : sub root :  %s", (char*)value.key().asString().c_str());

        }else{
            LOGE( "QSM Item value is invalid");
        }
    }

}
static int s_qsm_log_switch_get_mem_info_D004(void){

    int nFd, size;
    char *ptr = NULL;
    char nBuffer[1024];
    int mem_usage = 0;

    memset(nBuffer, 0, sizeof(nBuffer));
    nFd = open("/proc/meminfo", O_RDONLY);
    if(nFd >= 0) {
        size = read(nFd, nBuffer, sizeof(nBuffer));
        if (size <=0) {
            return 0;
        }
        close(nFd);
        nBuffer[size-1]='\0';

        char* ptrValue;
        ptr = strtok(nBuffer, "\n");
        int valTotal = 0, valFree = 0, valBuffer = 0, valCache = 0;
        int lenStrTotal = strlen("MemTotal:");
        int lenStrFree = strlen("MemFree:");
        int lenStrBuffer = strlen("Buffers:");
        int lenStrCache = strlen("Cached:");
        bool found_total = false, found_free = false, found_buffer = false, found_cache = false;

        while (ptr != NULL) {
            if (false == found_total) {
                if (0 == strncmp(ptr, "MemTotal:", lenStrTotal)) {
                    valTotal = 0;
                    ptrValue = ptr + lenStrTotal + 1;
                    if (NULL != ptrValue) {
                        skip_space(&ptrValue);
                        extract_numeric(ptrValue);
                        valTotal = atoi(ptrValue);
                        g_tot_mem_size = valTotal/(1024);
                        //LOGI("meminfo - MemTotal : %d", valTotal);
                    }
                    found_total = true;
                    goto CHECK_END;
                }
            }
            if (false == found_free) {
                if (0 == strncmp(ptr, "MemFree:", lenStrFree)) {
                    valFree = 0;
                    ptrValue = ptr + lenStrFree + 1;
                    if (NULL != ptrValue) {
                        skip_space(&ptrValue);
                        extract_numeric(ptrValue);
                        valFree = atoi(ptrValue);
                        //LOGI("meminfo - MemFree : %d", valFree);
                    }
                    found_free = true;
                    goto CHECK_END;
                }
            }
            if (false == found_buffer) {
                if (0 == strncmp(ptr, "Buffers:", lenStrBuffer)) {
                    valBuffer = 0;
                    ptrValue = ptr + lenStrBuffer + 1;
                    if (NULL != ptrValue) {
                        skip_space(&ptrValue);
                        extract_numeric(ptrValue);
                        valBuffer = atoi(ptrValue);
                        //LOGI("meminfo - Buffers : %d", valBuffer);
                    }
                    found_buffer = true;
                    goto CHECK_END;
                }
            }
            if (false == found_cache) {
                if (0 == strncmp(ptr, "Cached:", lenStrCache)) {
                    valCache = 0;
                    ptrValue = ptr + lenStrCache + 1;
                    if (NULL != ptrValue) {
                        skip_space(&ptrValue);
                        extract_numeric(ptrValue);
                        valCache = atoi(ptrValue);
                        //LOGI("meminfo - Cached : %d", valCache);
                    }
                    found_cache = true;
                    goto CHECK_END;
                }
            }
CHECK_END:
            //LOGI("meminfo - %s, found : %d, %d, %d, %d", ptr, valTotal, valFree, valBuffer, valCache);
            if (found_total && found_free && found_buffer && found_cache) {
                mem_usage = (int)((valTotal - valFree - valBuffer - valCache) * 100 / valTotal);
                //LOGI("QSM available meminfo : %3d%%", mem_usage);
                break;
            }
            ptr = strtok(NULL, "\n");
        }
    } else {
        LOGE("meminfo - open meminfo file fail");
    }

    return mem_usage;
}

static int s_qsm_log_switch_get_cpu_info_D004(void){

    int nFd,  size, cpu_usage = 0;
    int valUsage=0, valTotal = 0, lastValUsage = 0, lastValTotal = 0;
    char *ptr = NULL;
    char nBuffer[1024];
    int count = 2;

    while(count-- > 0){
        memset(nBuffer, 0, sizeof(nBuffer));
        nFd = open("/proc/stat", O_RDONLY);
        if(nFd >= 0) {
            size = read(nFd, nBuffer, sizeof(nBuffer));
            //LOGI("stat - read file size : %d", size);
            if (size <=0) {
                return 0;
            }
            close(nFd);
            nBuffer[size-1]='\0';

            char* ptrValue;
            ptr = strtok(nBuffer, "\n");
            if (strstr(ptr, "cpu") == ptr) {
                ptrValue = ptr + 3;
                valUsage = valTotal = 0;
                int value = 0;
                for (int i = 0; i < 3; i++) {
                    skip_space(&ptrValue);
                    extract_numeric(ptrValue);
                    value = atoi(ptrValue);
                    valUsage += value;
                    ptrValue += strlen(ptrValue) + 1;
                    //LOGI("stat - read value [%d] : %d, usage : %d", i, value, valUsage);
                }
                valTotal += valUsage;
                for (int i = 0; i < 4; i++) {
                    skip_space(&ptrValue);
                    extract_numeric(ptrValue);
                    value = atoi(ptrValue);
                    valTotal += value;
                    ptrValue += strlen(ptrValue) + 1;
                    //LOGI("stat - read value2 [%d] : %d, total : %d, usage : %d", i, value, valTotal, valUsage);
                }
            }
            if (lastValTotal != 0) {
                cpu_usage = (valUsage - lastValUsage) * 100 / (valTotal - lastValTotal);
                //LOGI("QSM stat - cpu usage %3d%%", cpu_usage);
            }
            lastValTotal = valTotal;
            lastValUsage = valUsage;
        } else {
            LOGE("stat - open stat file fail");
        }
        usleep(1000*400);
    }

    return cpu_usage;
}

static int s_qsm_log_switch_get_disk_info_D004(void){
    struct statvfs fiData;
    int disk_usage = 0;

    memset(&fiData, 0, sizeof(fiData));
    int ret = statvfs("/storage/emulated", &fiData);
    if (0 == ret) {
        if (0 == fiData.f_blocks) {
            ret = statvfs("/storage/sdcard0", &fiData);
            //LOGI("QSM available storage info - check sdcard0 : %llu / %llu, f_bsize : %u"
            //    , fiData.f_bfree, fiData.f_blocks, fiData.f_bsize);
        }
        if (0 == ret && fiData.f_blocks !=0 && fiData.f_bsize != 0  ) {
            disk_usage = (int)(fiData.f_blocks - fiData.f_bfree) * 100 / (int)fiData.f_blocks;
            //LOGI("QSM available storage info : %3d%%, %llu / %llu", disk_usage, fiData.f_bfree, fiData.f_blocks);
            g_tot_disk_size = (int)(fiData.f_blocks * fiData.f_bsize / (1024*1024*1024));
        } else {
#ifdef ARM64_V8A_BUILD // arm64-v8a build error fix
#else
            LOGE("QSM available emulated storage info error : ret=%d, blocks=%llu", ret, fiData.f_blocks);
#endif
        }
    }else{
        LOGE("QSM available emulated storage info : ret=%d",ret);
    }
    return disk_usage;
}

static bool s_qsm_log_switch_set_D004(Json::Value& jsonSendRoot, int mem_info = -1, int cpu_info = -1) {
    if (!g_log_switch_array[D004].log_on) {
        LOGI("qsm log_on is false - skip D004");
        return false;
    }

    int mem_usage, cpu_usage, disk_usage;
    Json::Value info;

    if (mem_info < 0) {
        mem_usage = s_qsm_log_switch_get_mem_info_D004();
    } else {
        mem_usage = mem_info;
    }
    if (cpu_info < 0) {
        cpu_usage = s_qsm_log_switch_get_cpu_info_D004();
    } else {
        cpu_usage = cpu_info;
    }
    disk_usage = s_qsm_log_switch_get_disk_info_D004();
    info["cpu_usage"] = cpu_usage;
    info["cpu_limit"] = g_cpu_limit;
    info["memory_size"] = g_tot_mem_size;
    info["memory_usage"] = mem_usage;
    info["memory_limit"] = g_mem_limit;
    info["disk_size"] = g_tot_disk_size;
    info["disk_usage"] = disk_usage;
    info["disk_limit"] = g_disk_limit;
    info["running_time"] = (int)(now_sec() - g_start_time);

    jsonSendRoot["system_info"] = info;

    LOGI( "QSM D0004 mem_usage=%d, cpu_usage=%d, disk_usage=%d", mem_usage, cpu_usage, disk_usage);

    return true;
}

static bool s_qsm_log_switch_set_D009(Json::Value& jsonSendRoot){
    if (!g_log_switch_array[D009].log_on) {
        LOGI("qsm log_on is false - skip D009");
        return false;
    }

    char value[PROP_VALUE_MAX] = {0, };

    int len = __system_property_get("sys.skb.stb_hdcp", value);
    if (0 == len) {
        strcpy(value, "null");
    }
    jsonSendRoot["hdmi_hdcp_version"] = value;
    LOGI( "QSM hdmi_hdcp_version=%s", value);

    return true;
}

static bool s_qsm_log_switch_set_D010(Json::Value& jsonSendRoot){
    if (!g_log_switch_array[D010].log_on) {
        LOGI("qsm log_on is false - skip D010");
        return false;
    }

    int ret;
    char buf[4096];

    /* wait for */
    sleep(1);

#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
    char hdmi_edit_xml[128];
	sprintf(hdmi_edit_xml, "%s/%s", qsmconfigDir, HDMI_EDIT_XML);
    ret = qsm_read_file((char*)hdmi_edit_xml, buf, 4095);
#else
    ret = qsm_read_file((char*)"/data/btv_home/tmp/hdmi-edid.xml", buf, 4095);
#endif/*TABLET_BUILD*/

    if(ret > 0){
#if 0
        jsonSendRoot["device_info_hdmi"]="testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest";
#else
        buf[ret] = '\0';
        char* str = (char*) convertInput(buf, "UTF-8");

        subObj_d010.clear();

        s_qsm_log_switch_parse_xml_json_d010(subObj_d010, getJSONFromXML(str));
        jsonSendRoot["device_info_hdmi"]=subObj_d010;
        xmlFree(str);
#endif
        return true;
    }else{
        LOGI( "QSM hdmi-edid.xml does not exist. HDMI is not inserted");
    }

    return false;
}


static bool s_qsm_log_switch_set_D014(Json::Value& jsonSendRoot){
    if (!g_log_switch_array[D014].log_on) {
        LOGI("qsm log_on is false - skip D014");
        return false;
    }

    int ret;
    char buf[20480] = {0,};
    Json::Value WriteRoot;
#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
    char tvservice_setting_properties_xml[128];
	sprintf(tvservice_setting_properties_xml, "%s/%s", qsmconfigDir, TVSERVICE_SETTING_PROPERTIES_XML);
    ret = qsm_read_file((char*)tvservice_setting_properties_xml, buf, 20479);
#else
    ret = qsm_read_file((char*)D014_SETTING_PROPERTY_FILE, buf, 20479);
#endif/*TABLET_BUILD*/
    if(ret > 0){
        buf[ret] = '\0';
        char* str = (char*) convertInput(buf, "UTF-8");
        if (str) {
            subObj_d014_1.clear();
            s_qsm_log_switch_parse_xml_json_d014_1(subObj_d014_1, getJSONFromXML(str));
            WriteRoot["setting_properties"]=subObj_d014_1;

            xmlFree(str);
        }
    }else{
        LOGE( "QSM tvservice-setting-properties.xml does not exist : %d", ret);
    }

#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
    char tvservice_system_properties_xml[128];
	sprintf(tvservice_system_properties_xml, "%s/%s", qsmconfigDir, TVSERVICE_SYSTEM_PROPERTIES_XML);
    ret = qsm_read_file((char*)tvservice_system_properties_xml, buf, 20479);
#else
    ret = qsm_read_file((char*)D014_SYSTEM_PROPERTY_FILE, buf, 20479);
#endif/*TABLET_BUILD*/
    if(ret > 0){
        buf[ret] = '\0';
        char* str = (char*) convertInput(buf, "UTF-8");
        if (str) {
            subObj_d014_2.clear();
            s_qsm_log_switch_parse_xml_json_d014_2(subObj_d014_2, getJSONFromXML(str));
            WriteRoot["system_properties"]=subObj_d014_2;
            xmlFree(str);
        }
    }else{
        LOGE( "QSM tvservice-system-properties.xml does not exist : %d", ret);
    }

    char visibility = g_watermark_status.visibility;
    char live = g_watermark_status.live;
    if (visibility >= VISIBILITY_MIN && visibility <= VISIBILITY_MAX
        && live >= LIVE_MIN && live <= LIVE_MAX) {
        Json::Value json_wm_prop;
        switch(visibility) {
            case VISIBILITY_ENABLE:
                json_wm_prop["visibility"] = "enable";
                break;
            case VISIBILITY_DISABLE:
                json_wm_prop["visibility"] = "disable";
                break;
            case VISIBILITY_ENABLE_BY_FORCE:
                json_wm_prop["visibility"] = "enable_by_force";
                break;
        }
        char live_buf[8] = {0,};
        if (live == LIVE_ON) {
            sprintf(live_buf, "%d", LIVE_ON);
            json_wm_prop["live"] = live_buf;
        } else {
            sprintf(live_buf, "%d", LIVE_OFF);
            json_wm_prop["live"] = live_buf;
        }
        LOGI("QSM service: s_qsm_log_switch_set_D014 - watermark info visibility=%d, live=%d"
            ,visibility, live);
        WriteRoot["watermark_properties"] = json_wm_prop;
    }

    jsonSendRoot["user_config"] = WriteRoot;

    return true;
}

static bool s_qsm_log_switch_check_switch(char* key){
    for(int i=0; i<last_log_switch_item; i++){
        if(strcmp(g_log_switch_array[i].parameter_name,key)==0){
            return g_log_switch_array[i].log_on;
        }
    }
    return true;
}

static void s_qsm_log_switch_status_dump(void)
{
#if 0
    LOGI("QSM : device_info=%d",g_log_switch_array[device_info].log_on);
    LOGI("QSM : system_info=%d",g_log_switch_array[system_info].log_on);
    LOGI("QSM : network_info=%d",g_log_switch_array[network_info].log_on);
    LOGI("QSM : service_mode=%d",g_log_switch_array[service_mode].log_on);
    LOGI("QSM : stb_upgrade=%d",g_log_switch_array[stb_upgrade].log_on);
    LOGI("QSM : hdmi_plug_inout=%d",g_log_switch_array[hdmi_plug_inout].log_on);
    LOGI("QSM : hdmi_hdcp_version=%d",g_log_switch_array[hdmi_hdcp_version].log_on);
    LOGI("QSM : device_info_hdmi=%d",g_log_switch_array[device_info_hdmi].log_on);
    LOGI("QSM : device_info_rcu=%d",g_log_switch_array[device_info_rcu].log_on);
    LOGI("QSM : device_info_rcu_error=%d",g_log_switch_array[device_info_rcu_error].log_on);
    LOGI("QSM : device_info_bt=%d",g_log_switch_array[device_info_bt].log_on);
    LOGI("QSM : user_config=%d",g_log_switch_array[user_config].log_on);
    LOGI("QSM : contents_info=%d",g_log_switch_array[contents_info].log_on);
    /*LOGI("QSM : video_info=%d",g_log_switch_array[video_info].log_on);
    LOGI("QSM : audio_info=%d",g_log_switch_array[audio_info].log_on);
    LOGI("QSM : error_status_info=%d",g_log_switch_array[error_status_info].log_on);*/
    LOGI("QSM : server_check_result=%d",g_log_switch_array[server_check_result].log_on);
    LOGI("QSM : btv_error=%d",g_log_switch_array[btv_error].log_on);
    LOGI("QSM : vod_error=%d",g_log_switch_array[vod_error].log_on);
    LOGI("QSM : application_error=%d",g_log_switch_array[application_error].log_on);
    LOGI("QSM : navigator_error=%d",g_log_switch_array[navigator_error].log_on);
    LOGI("QSM : rtp_error=%d",g_log_switch_array[rtp_error].log_on);
#endif
}

#ifdef USE_OS_MAC_ADDR
static void s_qsm_get_os_mac_address(char * addr, int len) {
    if (NULL == addr || len < 18) {
        return;
    }
    int fd;
    struct ifreq ifr;
    fd = socket(PF_INET, SOCK_DGRAM, 0);
    if (fd >= 0) {
        ifr.ifr_addr.sa_family = AF_INET;
        strcpy(ifr.ifr_name, "eth0");
        int ret = ioctl(fd, SIOCGIFHWADDR, &ifr);
        if (ret >= 0) {
            sprintf(addr,"%02x:%02x:%02x:%02x:%02x:%02x",
                (unsigned char)ifr.ifr_hwaddr.sa_data[0],
                (unsigned char)ifr.ifr_hwaddr.sa_data[1],
                (unsigned char)ifr.ifr_hwaddr.sa_data[2],
                (unsigned char)ifr.ifr_hwaddr.sa_data[3],
                (unsigned char)ifr.ifr_hwaddr.sa_data[4],
                (unsigned char)ifr.ifr_hwaddr.sa_data[5]);
            LOGI("QSM os mac addr : %s", addr);
        } else {
            LOGI("QSM fail to get os mac address : %d", ret);
        }
        close(fd);
    }
}
#endif

#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
static void s_qsm_log_switch_parse_second_root(Json::Value* secondRoot)
{
    char keyArray[100][30];
    int index = 0;

    for( Json::Value::iterator itr = secondRoot->begin() ; itr != secondRoot->end() ; itr++ ) {

        strcpy(&keyArray[index][0],(char*)itr.key().asString().c_str());
        const char* key = (char*)&keyArray[index++][0];

        if((*itr).isString()){
            if(strcmp((char*)key,"device_model")==0){
				memset(g_model_name, 0, sizeof(g_model_name));
                strcpy(g_model_name, (*itr).asString().c_str());
            }
        }
    }
}
#endif/*TABLET_BUILD*/

static void s_qsm_log_switch_parse_INIT_file(Json::Value* root)
{
    int ret, count;
    int index = 0;
    char keyArray[100][30];

    pthread_mutex_lock(&s_json_data_mutex);

    for( Json::ValueIterator itr = root->begin() ; itr != root->end() ; itr++ ) {

        strcpy(&keyArray[index][0],(char*)itr.key().asString().c_str());
        const char* key = (char*)&keyArray[index++][0];

        if((*itr).isString()){
#ifdef USE_OS_MAC_ADDR
            if (strcmp((char*)key,"mac_address") != 0) {
                sJsonInitRoot[key] = (*itr).asString().c_str();
            }
#else
            sJsonInitRoot[key] = (*itr).asString().c_str();
#endif
        } else if( (*itr).isBool() ) {
              sJsonInitRoot[key] = (*itr).asBool();
        } else if( (*itr).isInt() ) {
            sJsonInitRoot[key] = (*itr).asInt();
        } else if( (*itr).isUInt() ) {
            sJsonInitRoot[key] = (*itr).asUInt();
        } else if( (*itr).isDouble() ) {
            sJsonInitRoot[key] = (*itr).asDouble();
        }else if( (*itr).isObject() ) {
            sJsonInitRoot[key] = (*itr);
#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
            Json::Value secondRoot = (*itr);
            s_qsm_log_switch_parse_second_root(&secondRoot);
#endif/*TABLET_BUILD*/
        }else{
            LOGE( "QSM Item value is invalid");
        }
    }
#ifdef USE_OS_MAC_ADDR
    s_qsm_get_os_mac_address(g_mac_addr, 100);
    sJsonInitRoot["mac_address"] = g_mac_addr;
#endif

    pthread_mutex_unlock(&s_json_data_mutex);

    LOGI("QSM s_qsm_log_switch_parse_INIT_file - set mac address : %s , set device_model : %s", g_mac_addr, g_model_name);
}

static void s_qsm_log_switch_get_limit_info(xmlNode * a_node)
{
    xmlNode *cur_node = NULL;

    for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
        if (cur_node->type == XML_ELEMENT_NODE) {
            if(cur_node->name != NULL){

                //LOGE("QSM node type: Element, name[%s]\n", cur_node->name);
                if(strcmp((char*)cur_node->name,(char*)"CPU_LIMIT")==0){
                     g_cpu_limit = -1;
                }else if(strcmp((char*)cur_node->name,(char*)"MEM_LIMIT")==0){
                     g_mem_limit = -1;
                }else if(strcmp((char*)cur_node->name,(char*)"DISK_LIMIT")==0){
                     g_disk_limit = -1;
                }
            }
        }
        else if(cur_node->type == XML_TEXT_NODE)
        {
            if(cur_node->content != NULL && cur_node->content[0]!=' '){
                //LOGE("QSM  text[%s]\n", cur_node->content);
                if(g_cpu_limit == -1){
                    g_cpu_limit = atoi((char*)cur_node->content);
                }else if(g_mem_limit == -1){
                    g_mem_limit = atoi((char*)cur_node->content);
                }else if(g_disk_limit == -1){
                    g_disk_limit = atoi((char*)cur_node->content);
                }
            }
        }

        s_qsm_log_switch_get_limit_info(cur_node->children);
    }
}

void s_qsm_log_switch_get_limit(void){

    xmlDoc *doc = NULL;
    xmlNode *root_element = NULL;

#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
    char isqmsagent_xml[128];
	sprintf(isqmsagent_xml, "%s/%s", qsmconfigDir, ISQMSAGENT_XML);
    doc = xmlReadFile(isqmsagent_xml, NULL, 0);
#else
    doc = xmlReadFile("/data/btv_home/config/isqmsAgent.xml", NULL, 0);
#endif/*TABLET_BUILD*/

    if (doc == NULL) {
        LOGE("error: could not parse file : isqmsAgent.xml\n");

        /* use default value */
        g_cpu_limit = 90;
        g_mem_limit = 90;
        g_disk_limit = 90;
        return;
    }

    root_element = xmlDocGetRootElement(doc);
    s_qsm_log_switch_get_limit_info(root_element);
    LOGI("qsm_log_switch_init : cpu_limit = %d, mem_limit=%d, disk_limit=%d", g_cpu_limit, g_mem_limit, g_disk_limit);

    xmlFreeDoc(doc);
    xmlCleanupParser();
}

#ifdef SIGNATURE_ON
void qsm_log_switch_init_signature(Json::Value root) {
#ifdef ARM64_V8A_BUILD // arm64-v8a build error fix
#else
    if (root == NULL) {
        return;
    }
#endif

    pthread_mutex_lock(&g_signature_mutex);

#ifdef SIGNATURE_TEST_MESSAGES
    if (g_signatures != NULL)
    {
        free(g_signatures);
    }
    g_signature_count = 2000;
    g_signatures = (SIGNATURE_ITEM_T *)calloc(g_signature_count, sizeof(SIGNATURE_ITEM_T));
    for (int i=0; i< g_signature_count; i++) {
        sprintf(g_signatures[i].message, "MV[%d]_OSAL_SHM_Ref[%d]: assertion \"ret >= 0\" failed", i, i);
        sprintf(g_signatures[i].jira_id, "SKB-%d", i);
        g_signatures[i].index = i;
    }
    int i=0;
    strcpy(g_signatures[i++].message, "RPC 1916VDEC:ClearPort takes too much time (> 5000 mS)");
    strcpy(g_signatures[i++].message, "TEST_RPC 1916VDEC:ClearPort takes too much time (> 1000 mS)");
    /*for(int i=0; i<1998; i++){
        sprintf(g_signatures[i].message, "MV[%d]_OSAL_SHM_Ref[%d]: assertion \"ret >= 0\" failed",i,i);
        sprintf(g_signatures[i].jira_id, "SKB-%d",i);
    }
    strcpy(g_signatures[1998].message, "RPC 1916VDEC:ClearPort takes too much time (> 5000 mS)");
    strcpy(g_signatures[1998].jira_id, "SKB-1998");
    strcpy(g_signatures[1999].message, "TEST_RPC 1916VDEC:ClearPort takes too much time (> 1000 mS)");
    strcpy(g_signatures[1999].jira_id, "SKB-1999");
    LOGI("qsm ===========  push pattern length : %d", g_signature_count);*/
#else
    const Json::Value signature = root["log_switch"]["signature"];

    g_signature_count = MIN(SIGNATURE_MAX_COUNT, signature.size());
    LOGI("QSM signature count is %d",g_signature_count);

    if (g_signatures != NULL)
    {
        free(g_signatures);
        g_signatures = NULL;
    }
    if (g_signature_count > 0) {
        g_signatures = (SIGNATURE_ITEM_T *)calloc(g_signature_count, sizeof(SIGNATURE_ITEM_T));
        for ( int index = 0; index < g_signature_count; ++index )
        {
            strncpy(g_signatures[index].message, signature[index]["message"].asString().c_str(), SIGNATURE_MAX_LEN - 1);
            strncpy(g_signatures[index].jira_id, signature[index]["jira_id"].asString().c_str(), JIRA_ID_MAX_LEN - 1);
            g_signatures[index].index = index;
            //LOGI("QSM signature[%d]=%s", index, g_signatures[index].message); // only test log
        }
    }
#endif // SIGNATURE_TEST_MESSAGES

    LOGI("QSM : pattern push back");
    if (g_signature_mode == SIGNATURE_MODE_WM)
    {
        g_search_patterns.clear();
        WuManberSearch.Release();

        if (g_signature_count > 0) {
            for(int i=0; i<g_signature_count; i++)
            {
                g_search_patterns.push_back(g_signatures[i].message);
            }

            if(WuManberSearch.Initialize( g_search_patterns, false, true, true ) <0)
            {
                LOGE("QSM Logd signature fail to initialize WuManber");
            }
        }
    }
#ifdef SIGNATURE_MVM
    else if (g_signature_mode == SIGNATURE_MODE_MWM)
    {
        if (g_mvm_inst != NULL) {
            mwmFree(g_mvm_inst);
            g_mvm_inst = NULL;
        }
        if (g_signature_count > 0) {
            g_mvm_inst = (MWM_STRUCT*)mwmNew();
            if (g_mvm_inst != NULL) {
                for(int i=0; i<g_signature_count; i++)
                {
                    mwmAddPatternEx(g_mvm_inst, (unsigned char *)g_signatures[i].message
                        , strlen(g_signatures[i].message), 1/*no case*/, 0, 0, &g_signatures[i].index /*ID*/, i);
                }
                mwmPrepPatterns(g_mvm_inst);
                LOGI("qsm push pattern length : %d", mwmGetNumPatterns(g_mvm_inst));
            } else {
                LOGE("qsm mwmNew() FAIL");
            }
        }
    }
#endif // SIGNATURE_MVM

    pthread_mutex_unlock(&g_signature_mutex);
}
#endif // SIGNATURE_ON

/*******************************************
* GLOBAL FUNCTION
********************************************/

int qsm_read_file(char* name, char* buffer, int len){
    int ret;

    FILE* r =fopen(name, "r");
    if(r == NULL)
    {
        LOGI("qsm qsm_read_file() - fopen FAIL : %s (%s)", strerror(errno), name);
        return -1;
    }
    ret = (int)fread(buffer,1, len, r);
    fclose(r);
    return ret;
}

int qsm_write_file(char* name, std::string data){
    std::ofstream file_id;
    file_id.open(name);
    if(file_id.is_open()){
        file_id << data;
        file_id.close();
        return data.length();
    }
    return -1;
}

void qsm_log_switch_init(void)
{
    g_start_time = now_sec();
    g_one_day_check_time = g_start_time;

    s_qsm_log_switch_get_device_info_INIT_file();

    /* read system limit info from xml */
    s_qsm_log_switch_get_limit();

    /* test code for signature */
#ifdef TEST_FOR_SIGNATURE_MODE
    char buf[10] = {0,};
#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
    char signature_conf[128];
	sprintf(signature_conf, "%s/%s", qsmconfigDir, SIGNATURE_CONF);
    int size = qsm_read_file((char*)signature_conf, buf, 9);
#else
    int size = qsm_read_file((char*)"/data/btv_home/config/signature.conf", buf, 9);
#endif/*TABLET_BUILD*/
    if (size > 0){
        g_signature_mode = atoi(buf);
    } else {
        LOGI("QSM read signature config failed");
        g_signature_mode = SIGNATURE_MODE_WM;
    }
#else
    g_signature_mode = SIGNATURE_MODE_WM;
#endif
    LOGI("QSM signature mode=%d", g_signature_mode);

    int ret = qsm_log_switch_read_log_switch_json();

#ifdef SIGNATURE_TEST_MESSAGES
    if (ret < 0) {
        qsm_log_switch_init_signature(NULL);
    }
#endif

    qsm_log_switch_read_watermark_data();
}

int qsm_log_switch_check(char* dst)
{
    int ret;
    Json::Value root;
    Json::Reader reader;
    std::string config_doc = dst;

    if(dst == NULL){
        return -1;
    }

    bool parsingSuccessful = reader.parse( config_doc, root );
    if ( !parsingSuccessful )
    {
        LOGI("QSM : Failed to parse configuration : %s", reader.getFormatedErrorMessages().c_str());
        return -1;
    }

    char* check_stb = (char*)root["check_stb"].asString().c_str();
    if(check_stb != NULL && strcmp(check_stb,"log_switch")==0){
        return 0;
    }
    return -1;
}

char* qsm_log_switch_make_config_info(int check_stb) {
    LOGI("QSM : qsm_log_switch_make_config_info - param : 0x%x, mac address : %s"
        , check_stb, g_mac_addr[0] ? g_mac_addr : "null");
    if (0 > check_stb || 0 == (check_stb & CHECK_STB_ALL)) {
        return NULL;
    }
    Json::Value root;
    int idx = 0, sub_idx = 0;
    char new_key[60], new_key_sub[60];
    sprintf(new_key, "%s%03d:%s", PREFIX_KEY_FOR_ORDER, idx++, "mac_address");
    root[new_key] = g_mac_addr;//"8d:8c:97:bb:ad:88";
    sprintf(new_key, "%s%03d:%s", PREFIX_KEY_FOR_ORDER, idx++, "device_id");
    root[new_key] = g_device_id;//"{F192A949-C399-11E3-92F5-2FDE1C}";

    if (check_stb & CHECK_STB_LOG_SWITCH) {
        Json::Value log_switch;

        pthread_mutex_lock(&g_signature_mutex);
        for (sub_idx=0; sub_idx<last_log_switch_item; sub_idx++) {
            sprintf(new_key_sub, "%s%03d:%s", PREFIX_KEY_FOR_ORDER, sub_idx, g_log_switch_array[sub_idx].parameter_name);
            log_switch[new_key_sub] = (int)g_log_switch_array[sub_idx].log_on;
        }
        sprintf(new_key, "%s%03d:%s", PREFIX_KEY_FOR_ORDER, idx++, "log_switch");
        root[new_key] = log_switch;

        Json::Value signature(Json::arrayValue);
        char new_key_3rd[60];

        for (int i=0; i< g_signature_count; i++) {
            Json::Value info;
            sprintf(new_key_3rd, "%s%03d:%s", PREFIX_KEY_FOR_ORDER, 0, "message");
            info[new_key_3rd] = g_signatures[i].message;
            sprintf(new_key_3rd, "%s%03d:%s", PREFIX_KEY_FOR_ORDER, 1, "jira_id");
            info[new_key_3rd] = g_signatures[i].jira_id;
            signature.append(info);
        }
        pthread_mutex_unlock(&g_signature_mutex);

        sprintf(new_key_sub, "%s%03d:%s", PREFIX_KEY_FOR_ORDER, sub_idx, "signature");
        root[new_key][new_key_sub] = signature;
    }
    if (check_stb & CHECK_STB_WATERMARK) {
        Json::Value watermark;
        sub_idx = 0;
        sprintf(new_key_sub, "%s%03d:%s", PREFIX_KEY_FOR_ORDER, sub_idx++, "visibility");
        LOGI("QSM watermark info - visibility : %d, live : %d", g_watermark_status.visibility, g_watermark_status.live);
        if (VISIBILITY_ENABLE == g_watermark_status.visibility)
            watermark[new_key_sub] = "enable";
        else if (VISIBILITY_DISABLE== g_watermark_status.visibility)
            watermark[new_key_sub] = "disable";
        else if (VISIBILITY_ENABLE_BY_FORCE== g_watermark_status.visibility)
            watermark[new_key_sub] = "enable_by_force";
        sprintf(new_key_sub, "%s%03d:%s", PREFIX_KEY_FOR_ORDER, sub_idx++, "live");
        watermark[new_key_sub] = g_watermark_status.live;

        sprintf(new_key, "%s%03d:%s", PREFIX_KEY_FOR_ORDER, idx++, "watermark");
        root[new_key] = watermark;
    }

    sOutputConfig = "";
    Json::StyledWriter writer;
    sOutputConfig = writer.write( root );
    remove_prefix_key(sOutputConfig);

    LOGI("QSM : qsm_log_switch_make_config_info - log_switch : %s, watermark : %s"
        , (check_stb & CHECK_STB_LOG_SWITCH) ? "true" : "false"
        , (check_stb & CHECK_STB_WATERMARK) ? "true" : "false");

    return (char*)sOutputConfig.c_str();
}

int qsm_log_switch_read_log_switch_json()
{
#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
    char qsm_init_data_json[128];
	sprintf(qsm_init_data_json, "%s/%s", qsmconfigDir, QSM_LOG_SWITCH_DATA_JSON);
    std::ifstream ifs((char*)qsm_init_data_json);
#else	
    std::ifstream ifs((char*)LOG_SWITCH_INFO_FILE);
#endif/*TABLET_BUILD*/
    int ret = -1;

    if(ifs.is_open()) {
        ifs.seekg(0, ifs.end);
        int len = ifs.tellg();
        ifs.seekg (0, ifs.beg);
        char* data = new char[len+1];

        if (data != NULL) {
            memset(data, 0, len+1);
            ifs.read(data, len);
            if (ifs) {
                ifs.close();
                ret = qsm_log_switch_parse_log_switch(data);
                delete[] data;
                return ret;
            } else {
                LOGI("qsm_log_switch_read_log_switch_json - cannot read info file");
            }
            delete[] data;
        }
        ifs.close();
    } else {
        LOGI("qsm_log_switch_read_log_switch_json - cannot open info file");
    }
    return ret;
}

char* qsm_log_switch_handle_mqtt_msg(char* dst) {
    int ret = qsm_parse_recv_mqtt_msg(dst);
    LOGI("QSM qsm_parse_recv_mqtt_msg return : 0x%x", ret);
    char* data = NULL;
    data = qsm_log_switch_make_config_info(ret);
    return data;
}

int qsm_parse_recv_mqtt_msg(char* dst) {
    int ret = 0;
    Json::Value root;
    Json::Reader reader;
    std::string config_doc = dst;
#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
    char qsm_log_switch_data_json[128];
    char warter_mark_conf[128];
	sprintf(qsm_log_switch_data_json, "%s/%s", qsmconfigDir, QSM_LOG_SWITCH_DATA_JSON);
	sprintf(warter_mark_conf, "%s/%s", qsmconfigDir, WATER_MARK_CONF);
#endif/*TABLET_BUILD*/

    LOGI("QSM : qsm_parse_recv_mqtt_msg - data : %p", dst);

    if(dst == NULL){
        LOGE("QSM : qsm_parse_recv_mqtt_msg - Failed to parse : NULL parameter");
        return -1;
    }

    bool parsingSuccessful = reader.parse( config_doc, root );
    if ( !parsingSuccessful )
    {
        LOGE("QSM : qsm_parse_recv_mqtt_msg - Failed to parse : %s", reader.getFormatedErrorMessages().c_str());
        return -1;
    }

    for( Json::Value::iterator itr = root.begin() ; itr != root.end() ; itr++ ) {
        if (false == itr.key().isString()) {
            LOGE("QSM : qsm_parse_recv_mqtt_msg - Failed to parse : root key is not string");
            return -1;
        }
        char* str_key = (char*)itr.key().asString().c_str();
        if (0 == strcmp("log_switch", str_key)) {
            qsm_log_switch_parse_log_switch(root);
            Json::StyledWriter jsonWriter;
            Json::Value log_switch_root;
            log_switch_root["log_switch"] = root["log_switch"];
            LOGI("QSM qsm_parse_recv_mqtt_msg - save log_switch info to file");
            std::ofstream file_id;
#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
	        file_id.open((char*)qsm_log_switch_data_json);
#else	
            file_id.open((char*)LOG_SWITCH_INFO_FILE);
#endif/*TABLET_BUILD*/
            if(file_id.is_open()){
                file_id << jsonWriter.write(log_switch_root);
                file_id.close();
            } else {
#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
                LOGE("QSM qsm_parse_recv_mqtt_msg - file open fail : %s", (char*)qsm_log_switch_data_json);
#else	            
                LOGE("QSM qsm_parse_recv_mqtt_msg - file open fail : %s", (char*)LOG_SWITCH_INFO_FILE);
#endif/*TABLET_BUILD*/
            }
        } else if (0 == strcmp("watermark", str_key)) {
            LOGI("QSM qsm_parse_recv_mqtt_msg - save watermark info to file");
            std::ofstream file_id;
#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
            file_id.open(warter_mark_conf);
#else	            
            file_id.open(LOG_SWITCH_WATERMARK_FILE);
#endif/*TABLET_BUILD*/
            if(file_id.is_open()){
                char* str_value = (char*)root["watermark"].get("visibility", "").asString().c_str();
                char buf[3];
                if (0 == strcmp(str_value, "enable")) {
                    g_watermark_status.visibility = VISIBILITY_ENABLE;
                } else if (0 == strcmp(str_value, "disable")) {
                    g_watermark_status.visibility = VISIBILITY_DISABLE;
                } else if (0 == strcmp(str_value, "enable_by_force")) {
                    g_watermark_status.visibility = VISIBILITY_ENABLE_BY_FORCE;
                }
                g_watermark_status.live = root["watermark"].get("live", 0).asInt();
                LOGI("QSM qsm_parse_recv_mqtt_msg - watermark info visible: %d, live : %d"
                    , g_watermark_status.visibility, g_watermark_status.live);
                sprintf(buf, "%d%d", g_watermark_status.visibility, g_watermark_status.live);
                file_id << buf;
                file_id.close();

                // send watermark info to client
                char send_buf[3];
                send_buf[0] = SET_WATERMARK;
                send_buf[1] = g_watermark_status.visibility;
                send_buf[2] = g_watermark_status.live;
                int cnt = qsm_send_command_to_client(send_buf, 3);
                LOGI("QSM qsm_parse_recv_mqtt_msg - qsm_send_command_to_client socket cnt : %d", cnt);
            } else {
#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
				LOGE("QSM qsm_parse_recv_mqtt_msg - file open fail : %s", (char*)warter_mark_conf);
#else	            
				LOGE("QSM qsm_parse_recv_mqtt_msg - file open fail : %s", (char*)LOG_SWITCH_WATERMARK_FILE);
#endif/*TABLET_BUILD*/
            }
        } else if (0 == strcmp("check_stb", str_key)) {
            if (root["check_stb"].isArray()) {
                for( Json::Value::iterator itr_sub = root["check_stb"].begin() ; itr_sub != root["check_stb"].end() ; itr_sub++ ) {
                    if (false == (*itr_sub).isString()) {
                        LOGE("QSM : qsm_parse_recv_mqtt_msg - Failed to parse : check_stb key is not string");
                        continue;
                    }
                    char* str_sub_key = (char*)(*itr_sub).asString().c_str();
                    if (0 == strcmp(str_sub_key, "log_switch")) {
                        ret |= CHECK_STB_LOG_SWITCH;
                    } else if (0 == strcmp(str_sub_key, "watermark")) {
                        ret |= CHECK_STB_WATERMARK;
                    }
                }
            } else {
                LOGE("QSM : qsm_parse_recv_mqtt_msg - check_stb value is not array");
            }
        } else {
            LOGE("QSM : qsm_parse_recv_mqtt_msg - NOT supported key : %s", str_key);
        }
    }

    return ret;
}

void qsm_log_switch_read_watermark_data() {
    char buf[10] = {0,};
#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
    char warter_mark_conf[128];
	sprintf(warter_mark_conf, "%s/%s", qsmconfigDir, WATER_MARK_CONF);
    int size = qsm_read_file((char*)warter_mark_conf, buf, 9);
#else
    int size = qsm_read_file((char*)LOG_SWITCH_WATERMARK_FILE, buf, 9);
#endif/*TABLET_BUILD*/	
    if (size > 1){
        g_watermark_status.visibility = buf[0]-'0';
        g_watermark_status.live = buf[1]-'0';
        LOGI("QSM service: watermark info visibility=%d, live=%d"
            , g_watermark_status.visibility, g_watermark_status.live);
    } else {
        g_watermark_status.visibility = VISIBILITY_ENABLE;
        g_watermark_status.live = 1;
        LOGI("QSM service: watermark info does not exist.");
    }
}

void qsm_log_switch_set_onoff(Json::Value root)
{
    g_log_switch_array[device_info].log_on = (bool)root.get("device_info", 0).asInt();
    g_log_switch_array[system_info].log_on = (bool)root.get("system_info", 0).asInt();
    g_log_switch_array[network_info].log_on = (bool)root.get("network_info", 0).asInt();
    g_log_switch_array[service_mode].log_on = (bool)root.get("service_mode", 0).asInt();
    g_log_switch_array[stb_upgrade].log_on = (bool)root.get("stb_upgrade", 0).asInt();
    g_log_switch_array[hdmi_plug_inout].log_on = (bool)root.get("hdmi_plug_inout", 0).asInt();
    g_log_switch_array[hdmi_hdcp_version].log_on = (bool)root.get("hdmi_hdcp_version", 0).asInt();
    g_log_switch_array[device_info_hdmi].log_on = (bool)root.get("device_info_hdmi", 0).asInt();
    g_log_switch_array[device_info_rcu].log_on = (bool)root.get("device_info_rcu", 0).asInt();
    g_log_switch_array[device_info_rcu_error].log_on = (bool)root.get("device_info_rcu_error", 0).asInt();
    g_log_switch_array[device_info_bt].log_on = (bool)root.get("device_info_bt", 0).asInt();
    g_log_switch_array[user_config].log_on = (bool)root.get("user_config", 0).asInt();
    g_log_switch_array[contents_info].log_on = (bool)root.get("contents_info", 0).asInt();
    /*g_log_switch_array[video_info].log_on = (bool)root.get("video_info", 0).asInt();
    g_log_switch_array[audio_info].log_on = (bool)root.get("audio_info", 0).asInt();
    g_log_switch_array[error_status_info].log_on = (bool)root.get("error_status_info", 0).asInt();*/
    g_log_switch_array[server_check_result].log_on = (bool)root.get("server_check_result", 0).asInt();
    g_log_switch_array[btv_error].log_on = (bool)root.get("btv_error", 0).asInt();
    g_log_switch_array[vod_error].log_on = (bool)root.get("vod_error", 0).asInt();
    g_log_switch_array[application_error].log_on = (bool)root.get("application_error", 0).asInt();
    g_log_switch_array[navigator_error].log_on = (bool)root.get("navigator_error", 0).asInt();
    g_log_switch_array[rtp_error].log_on = (bool)root.get("rtp_error", 0).asInt();
}

int qsm_log_switch_parse_log_switch(Json::Value root)
{
    qsm_log_switch_set_onoff(root["log_switch"]);
    s_qsm_log_switch_status_dump();
    qsm_log_switch_init_signature(root);
    return 0;
}

int qsm_log_switch_parse_log_switch(char* dst, bool save_file)
{
    int ret;
    Json::Value root;
    Json::Reader reader;
    std::string config_doc = dst;

    LOGI("QSM : qsm_log_switch_parse_log_switch - save_file : %d, data : %p", save_file, dst);

    if(dst == NULL){
        return -1;
    }

    bool parsingSuccessful = reader.parse( config_doc, root );
    if ( !parsingSuccessful )
    {
        LOGI("QSM : Failed to parse configuration : %s", reader.getFormatedErrorMessages().c_str());
        return -1;
    }

    Json::Value::iterator itr = root.begin();
    LOGI("QSM parse_log_switch - root key : %s"
        , itr.key().isString() ? (char*)itr.key().asString().c_str() : "not string");

    Json::Value value;
    if (save_file) {
        if (itr.key().isString() ? 0 == strcmp("check_stb", (char*)itr.key().asString().c_str()) : 0) {
            if (root["check_stb"].isArray()
                && 0 == strcmp("log_switch", root["check_stb"][0].asString().c_str())) {
                return 1;
            } else {
                LOGE("QSM : check_stb value is not array");
            }
            return -1;
        }
    }

    if (root["log_switch"].empty()) {
        LOGE("QSM : qsm_log_switch_parse_log_switch - log_switch value is EMPTY. %d"
            , root["log_switch"].isNull());
        return -1;
    }

    g_log_switch_array[device_info].log_on = (bool)root["log_switch"].get("device_info", 0).asInt();
    g_log_switch_array[system_info].log_on = (bool)root["log_switch"].get("system_info", 0).asInt();
    g_log_switch_array[network_info].log_on = (bool)root["log_switch"].get("network_info", 0).asInt();
    g_log_switch_array[service_mode].log_on = (bool)root["log_switch"].get("service_mode", 0).asInt();
    g_log_switch_array[stb_upgrade].log_on = (bool)root["log_switch"].get("stb_upgrade", 0).asInt();
    g_log_switch_array[hdmi_plug_inout].log_on = (bool)root["log_switch"].get("hdmi_plug_inout", 0).asInt();
    g_log_switch_array[hdmi_hdcp_version].log_on = (bool)root["log_switch"].get("hdmi_hdcp_version", 0).asInt();
    g_log_switch_array[device_info_hdmi].log_on = (bool)root["log_switch"].get("device_info_hdmi", 0).asInt();
    g_log_switch_array[device_info_rcu].log_on = (bool)root["log_switch"].get("device_info_rcu", 0).asInt();
    g_log_switch_array[device_info_rcu_error].log_on = (bool)root["log_switch"].get("device_info_rcu_error", 0).asInt();
    g_log_switch_array[device_info_bt].log_on = (bool)root["log_switch"].get("device_info_bt", 0).asInt();
    g_log_switch_array[user_config].log_on = (bool)root["log_switch"].get("user_config", 0).asInt();
    g_log_switch_array[contents_info].log_on = (bool)root["log_switch"].get("contents_info", 0).asInt();
    /*g_log_switch_array[video_info].log_on = (bool)root["log_switch"].get("video_info", 0).asInt();
    g_log_switch_array[audio_info].log_on = (bool)root["log_switch"].get("audio_info", 0).asInt();
    g_log_switch_array[error_status_info].log_on = (bool)root["log_switch"].get("error_status_info", 0).asInt();*/
    g_log_switch_array[server_check_result].log_on = (bool)root["log_switch"].get("server_check_result", 0).asInt();
    g_log_switch_array[btv_error].log_on = (bool)root["log_switch"].get("btv_error", 0).asInt();
    g_log_switch_array[vod_error].log_on = (bool)root["log_switch"].get("vod_error", 0).asInt();
    g_log_switch_array[application_error].log_on = (bool)root["log_switch"].get("application_error", 0).asInt();
    g_log_switch_array[navigator_error].log_on = (bool)root["log_switch"].get("navigator_error", 0).asInt();
    g_log_switch_array[rtp_error].log_on = (bool)root["log_switch"].get("rtp_error", 0).asInt();

    s_qsm_log_switch_status_dump();

    qsm_log_switch_init_signature(root);

    if (save_file)
    {
        Json::StyledWriter jsonWriter;
        LOGI("QSM parse log switch : save to file");
        std::ofstream file_id;
#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
        char qsm_log_switch_data_json[128];
        sprintf(qsm_log_switch_data_json, "%s/%s", qsmconfigDir, QSM_LOG_SWITCH_DATA_JSON);
        file_id.open((char*)qsm_log_switch_data_json);
#else
        file_id.open((char*)LOG_SWITCH_INFO_FILE);
#endif/*TABLET_BUILD*/		
        if(file_id.is_open()){
            file_id << jsonWriter.write(root);
            file_id.close();
        } else {
            LOGI("QSM parse log switch : fail to open info file");
        }
    }

    return 0;
}

char* qsm_log_switch_parse_INIT(char* dst, int* len)
{
    int ret, count;
    Json::Value root;
    Json::StyledWriter jsonWriter;
    int index = 0;
    char keyArray[100][30];

    Json::Reader reader;
    std::string config_doc = dst;

    bool parsingSuccessful = reader.parse( config_doc, root );
    if ( !parsingSuccessful )
    {
        LOGI("QSM : Failed to parse configuration : %s", reader.getFormatedErrorMessages().c_str());
        return NULL;
    }

    pthread_mutex_lock(&s_json_data_mutex);

    sJsonInitRoot.clear ();

    for( Json::Value::iterator itr = root.begin() ; itr != root.end() ; itr++ ) {

        /*TODO : change malloc...*/
        strcpy(&keyArray[index][0],(char*)itr.key().asString().c_str());
        const char* key = (char*)&keyArray[index++][0];

        if((*itr).isString()){
            sJsonInitRoot[key] = (*itr).asString().c_str();
            //LOGE( "QSM INIT value = %s",(*itr).asString().c_str());
            if(strcmp((char*)key,"mac_address")==0){
#ifndef USE_OS_MAC_ADDR
                if((*itr).isString()){
                    LOGI("QSM : qsm_log_switch_parse_INIT - mac_addr = %s => %s"
                        , g_mac_addr, (*itr).asString().c_str());
                    strcpy(g_mac_addr, (*itr).asString().c_str());
                }
#endif
            }else if(strcmp((char*)key,"device_id")==0){
                if((*itr).isString()){
                    LOGI("QSM : qsm_log_switch_parse_INIT - device_id = %s => %s"
                        , g_device_id, (*itr).asString().c_str());
                    strcpy(g_device_id, (*itr).asString().c_str());
                }
            }
        } else if( (*itr).isBool() ) {
            sJsonInitRoot[key] = (*itr).asBool();
        } else if( (*itr).isInt() ) {
            sJsonInitRoot[key] = (*itr).asInt();
        } else if( (*itr).isUInt() ) {
            sJsonInitRoot[key] = (*itr).asUInt();
        } else if( (*itr).isDouble() ) {
            sJsonInitRoot[key] = (*itr).asDouble();
        } else if( (*itr).isArray() ) {
            sJsonInitRoot[key] = Json::Value((*itr));
        }else if( (*itr).isNull() ) {
            //LOGE( "QSM Item value is null");
        }else if( (*itr).isObject() ) {
            Json::Value value = (*itr);
            sJsonInitRoot[key] = value;
            //LOGE( "QSM INIT value is object");
        }else{
            LOGE( "QSM Item value is invalid");
        }
    }

#ifdef USE_OS_MAC_ADDR
    s_qsm_get_os_mac_address(g_mac_addr, 100);
    sJsonInitRoot["mac_address"] = g_mac_addr;
    LOGI("QSM qsm_log_switch_parse_INIT - set mac address : %s", g_mac_addr);
#endif

    sOutputConfig = "";
    sOutputConfig = jsonWriter.write( sJsonInitRoot );

    pthread_mutex_unlock(&s_json_data_mutex);

    *len = sOutputConfig.length();

    LOGI( "QSM INIT = %s",sOutputConfig.c_str());

    std::ofstream file_id;
#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
    char qsm_init_data_json[128];
	sprintf(qsm_init_data_json, "%s/%s", qsmconfigDir, QSM_INIT_DATA_JSON);	
    file_id.open((char*)qsm_init_data_json);
#else
    file_id.open((char*)"/data/btv_home/config/qsm_init_data.json");
#endif/*TABLET_BUILD*/
    if(file_id.is_open()){
        file_id << sOutputConfig;
        file_id.close();
    }

    return (char*)sOutputConfig.c_str();
}

char* qsm_log_switch_parse_others(char* type, char* dst, int* len)
{
    int ret, count;
    Json::Value root;

    Json::Reader reader;
    std::string config_doc = dst;
    int index = 0;
    char keyArray[500][30];

    if (dst == NULL) {
        LOGE("QSM : qsm_log_switch_parse_others invalid param - type : %p, : %p", type, dst);
        return NULL;
    }
    LOGI("QSM : qsm_log_switch_parse_others type : %s, : %s", (type != NULL) ? type : "NULL", dst);

    if (0 == strcmp(type, "APPLICATION")
        && false == g_log_switch_array[application_error].log_on) {
        LOGI("QSM : log switch application off - skip APPLICATION event");
        return NULL;
    } else if (0 == strcmp(type, "Navigator")){
        qsm_navigator_client_send(dst);
        return NULL;
    } else if (0 == strcmp(type, "RTP")){
        // for debug and test log not release version
        //LOGI("QSM s_qsm_send_thread - RTP message : %s", dst);
        qsm_update_rtp_status(dst);
        return NULL;
    }

    pthread_mutex_lock(&s_json_data_mutex);
    Json::Value jsonSendRoot = sJsonInitRoot;
    pthread_mutex_unlock(&s_json_data_mutex);

    s_qsm_log_switch_status_dump();

    bool parsingSuccessful = reader.parse( config_doc, root );
    if ( !parsingSuccessful )
    {
        LOGI("QSM : Failed to parse configuration : %s", reader.getFormatedErrorMessages().c_str());
        return NULL;
    }

    bool isExistValue = false;
    for( Json::Value::iterator  itr = root.begin() ; itr != root.end() ; itr++ ) {

        /*TODO : change malloc...*/
        strcpy(&keyArray[index][0],(char*)itr.key().asString().c_str());
        char* key = (char*)&keyArray[index++][0];

        /* check log switch on/off */
        if(!s_qsm_log_switch_check_switch(key)){
            LOGI("QSM : log switch off - skip key : %s", key);
            continue;
        }

        if (0 != strcmp(key, "log_info_type") && 0 != strcmp(key, "log_time")) {
            isExistValue = true;
        }

        if((*itr).isString()){
            jsonSendRoot[key] = (*itr).asString().c_str();
        } else if( (*itr).isBool() ) {
            jsonSendRoot[key] = (*itr).asBool();
        } else if( (*itr).isInt() ) {
            jsonSendRoot[key] = (*itr).asInt();
        } else if( (*itr).isUInt() ) {
            jsonSendRoot[key] = (*itr).asUInt();
        } else if( (*itr).isDouble() ) {
            jsonSendRoot[key] = (*itr).asDouble();
        }else if( (*itr).isObject() ) {
            jsonSendRoot[key] = (*itr);
        }else{
            LOGE( "QSM Item value is invalid");
        }
    }

    if (strcmp(type,"COLD_BOOTING")==0) {
        LOGI( "QSM qsm_log_switch_parse_others - Type is COLD_BOOTING");
        if (s_qsm_log_switch_set_D009(jsonSendRoot)) {
            isExistValue = true;
        }
        if (s_qsm_log_switch_set_D010(jsonSendRoot)) {
            isExistValue = true;
        }
        if (s_qsm_log_switch_set_D014(jsonSendRoot)) {
            isExistValue = true;
        }
    } else if (strcmp(type,"SLEEP_MODE")==0) {
        LOGI( "QSM qsm_log_switch_parse_others - Type is SLEEP_MODE");
        if (s_qsm_log_switch_set_D004(jsonSendRoot)) {
            isExistValue = true;
        }
        if (s_qsm_log_switch_set_D014(jsonSendRoot)) {
            isExistValue = true;
        }
        pthread_mutex_lock(&g_sleep_mutex);
        g_sleep_status = true;
        LOGI( "QSM qsm_log_switch_parse_others - set g_sleep_status true");
        pthread_mutex_unlock(&g_sleep_mutex);
    } else if (strcmp(type,"WAKEUP_MODE")==0) {
        LOGI( "QSM qsm_log_switch_parse_others - Type is WAKEUP_MODE");
        pthread_mutex_lock(&g_sleep_mutex);
        if(g_sleep_status){
            LOGI( "QSM qsm_log_switch_parse_others - set g_sleep_status false");
            g_sleep_status = false;
            pthread_cond_signal(&g_sleep_condition);
        }
        pthread_mutex_unlock(&g_sleep_mutex);
        if (s_qsm_log_switch_set_D014(jsonSendRoot)) {
            isExistValue = true;
        }
    } else if (strcmp(type,"HDMI_STATUS_CHANGED")==0) {
        LOGI( "QSM qsm_log_switch_parse_others - Type is HDMI_STATUS_CHANGED");
        if (s_qsm_log_switch_set_D009(jsonSendRoot)) {
            isExistValue = true;
        }
        if (s_qsm_log_switch_set_D010(jsonSendRoot)) {
            isExistValue = true;
        }
    }

    if (false == isExistValue) {
        return NULL;
    }

    jsonSendRoot["client_send_time"] = currentDateTime();

    sOutputConfig = "";
    Json::StyledWriter jsonWriter;
    sOutputConfig = jsonWriter.write( jsonSendRoot );

    *len = sOutputConfig.length();

    return (char*)sOutputConfig.c_str();
}

int qsm_log_switch_read_init_json()
{
#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
    char qsm_init_data_json[128];
	sprintf(qsm_init_data_json, "%s/%s", qsmconfigDir, QSM_INIT_DATA_JSON);
    std::ifstream file_input(qsm_init_data_json);
#else
    std::ifstream file_input("/data/btv_home/config/qsm_init_data.json");
#endif/*TABLET_BUILD*/
    Json::Reader reader;
    Json::Value root;

    if(file_input.is_open()){
        bool parsingSuccessful = reader.parse( file_input, root );
        file_input.close();
        if ( !parsingSuccessful)
        {
            LOGI("QSM : Failed to parse configuration from file : %s", reader.getFormatedErrorMessages().c_str());
            return -1;
        }
        s_qsm_log_switch_parse_INIT_file(&root);
        return 0;
    }
    return -1;
}

void qsm_log_switch_check_system_info(void) {

    int mem_usage, cpu_usage, disk_usage;

    mem_usage = s_qsm_log_switch_get_mem_info_D004();
    cpu_usage = s_qsm_log_switch_get_cpu_info_D004();
    disk_usage = s_qsm_log_switch_get_disk_info_D004();

    LOGI("QSM check system info : mem_usage=%d, cpu_usage=%d, disk_usage=%d",mem_usage,cpu_usage, disk_usage);

    if(g_mem_limit < mem_usage ||
        g_cpu_limit < cpu_usage){

        if(!g_init_type_received){
            if(qsm_log_switch_read_init_json() < 0){
                LOGI("QSM service: INIT type message was not received. so new message was removed - system info");
                return;
            }
            LOGI("QSM service: INIT type message was not received. but read from file - system info");
        }

        if (false == g_log_switch_array[system_info].log_on) {
            LOGI("QSM check system info : skip sending system_info - log switch off");
            return;
        }

        //jsonSendRoot.clear();
        Json::Value jsonSendRoot;

        pthread_mutex_lock(&s_json_data_mutex);
        jsonSendRoot = sJsonInitRoot;
        pthread_mutex_unlock(&s_json_data_mutex);

        jsonSendRoot["log_info_type"] = "Q1";
        jsonSendRoot["log_time"] = currentDateTime();
        s_qsm_log_switch_set_D004(jsonSendRoot, mem_usage, cpu_usage);
        jsonSendRoot["client_send_time"] = currentDateTime();

        sOutputConfig = "";
        Json::StyledWriter jsonWriter;
        sOutputConfig = jsonWriter.write( jsonSendRoot );
        qsm_send((char*)sOutputConfig.c_str(), (int)sOutputConfig.length(), 0);
    }
}

void qsm_log_switch_send_cold_boot(void) {

    if(!g_cold_boot_sent){

        LOGI("QSM qsm_log_switch_send_cold_boot  - g_cold_boot_sent : %d, g_init_type_received : %d"
            , g_cold_boot_sent, g_init_type_received);

        if(!g_init_type_received){
            if(qsm_log_switch_read_init_json() < 0){
                LOGI("QSM service: INIT type message was not received. so new message was removed - cold boot");
                return;
            }
            LOGI("QSM service: INIT type message was not received. but read from file - cold boot");
        }

        g_cold_boot_sent = true;
        if (s_qsm_log_switch_is_all_off()) {
            LOGI("QSM check system info : skip sending cold boot - all log switch off");
            return;
        }

        //jsonSendRoot.clear();
        Json::Value jsonSendRoot;

        pthread_mutex_lock(&s_json_data_mutex);
        jsonSendRoot = sJsonInitRoot;
        pthread_mutex_unlock(&s_json_data_mutex);

        jsonSendRoot["log_info_type"] = "Q1";
        jsonSendRoot["log_time"] = currentDateTime();
        s_qsm_log_switch_set_D009(jsonSendRoot);
        s_qsm_log_switch_set_D010(jsonSendRoot);
        s_qsm_log_switch_set_D014(jsonSendRoot);
        jsonSendRoot["client_send_time"] = currentDateTime();

        sOutputConfig = "";
        Json::StyledWriter jsonWriter;
        sOutputConfig = jsonWriter.write( jsonSendRoot );
        LOGI("QSM qsm_log_switch_send_cold_boot - call qsm_send");
        qsm_send((char*)sOutputConfig.c_str(), (int)sOutputConfig.length(), 0);
    } else {
        //LOGI("QSM qsm_log_switch_send_cold_boot - already sent event");
    }
}

#ifdef SIGNATURE_ON
#ifdef SIGNATURE_MVM
static int callback_match(void* id, int/* index*/, void * data)
{
    LOGI("QSM pattern matched: index=%d, data=%p\n"
        , (id != NULL) ? *((short *)id) : -1, data);
    found_idx = (id != NULL) ? *((short *)id) : -1;
    return 1;
}
#endif // SIGNATURE_MVM

static int qsm_log_switch_check_log(const char* log, int len)
{
    int ret = -1;
#ifdef CHECK_SIGNATURE_SEARCH_TIME
  struct timeval te1, te2;
#endif

    //LOGI("QSM qsm_log_switch_check_log - g_signature_mode =%d",g_signature_mode);

    if(g_signature_mode == SIGNATURE_MODE_NONE || g_signature_mode > SIGNATURE_MODE_MAX) {
        return -1;
    }

    if(NULL == log || 0 >= len || 0 >= g_signature_count){
        return -1;
    }

#ifdef CHECK_SIGNATURE_SEARCH_TIME
    gettimeofday(&te1, NULL);
#endif

    if (g_signature_mode == SIGNATURE_MODE_WM) {
        ret = WuManberSearch.Search(len, log, g_search_patterns);
        if(ret >= 0){
#ifdef CHECK_SIGNATURE_SEARCH_TIME
            gettimeofday(&te2, NULL);
            int gap = (int)((te2.tv_sec*1000*1000+ te2.tv_usec) - (te1.tv_sec*1000*1000+ te1.tv_usec));
            //LOGI("QSM WuManber : found signature [%d] len=%d, gap : %d"
                //, ret, len, (int)gap, g_signatures[ret].time_base.tv_sec, g_signatures[ret].time_base.tv_usec);
#else
            char buf[32] = {0,};
            time_to_string(buf, 32, g_signatures[ret].time_base);
            //LOGI("QSM WuManber : found signature [%d] len=%d, time : %ld, %ld : %s"
                //, ret, len, g_signatures[ret].time_base.tv_sec, g_signatures[ret].time_base.tv_usec, buf);
#endif
            return ret;
        }
    }
#ifdef SIGNATURE_MVM
    else if (g_signature_mode == SIGNATURE_MODE_MWM) {
        if (g_mvm_inst == NULL) {
            //LOGE("qsm qsm_log_switch_check_log - g_mvm_inst is not initialized yet");
            return -1;
        }
        ret = mwmSearch(g_mvm_inst, (unsigned char *)log, len, callback_match, NULL);
        if(ret > 0){
#ifdef CHECK_SIGNATURE_SEARCH_TIME
            gettimeofday(&te2, NULL);
            int gap = (int)((te2.tv_sec*1000*1000+ te2.tv_usec) - (te1.tv_sec*1000*1000+ te1.tv_usec));
            LOGI("QSM modified WuManber : found signature: index = %d, len=%d, gap : %d", found_idx, len, (int)gap);
#else
            LOGI("QSM modified WuManber : found signature: index = %d, len=%d", found_idx, len);
#endif
            ret = found_idx;
            found_idx = -1;
            return ret;
        }
    }
#endif /* SIGNATURE_MVM */
    else if (g_signature_mode == SIGNATURE_MODE_MEMCMP) {
        for ( int i = 0; i< g_signature_count; ++i )
        {
            int slen = MIN(len, (int)strlen(g_signatures[i].message));
            ret = memcmp(log, (char*)g_signatures[i].message, slen);
            if(ret == 0){
#ifdef CHECK_SIGNATURE_SEARCH_TIME
                gettimeofday(&te2, NULL);
                int gap = (int)((te2.tv_sec*1000*1000+ te2.tv_usec) - (te1.tv_sec*1000*1000+ te1.tv_usec));
                LOGI("QSM memcmp : found signature: signature[%d], len=%d, gap : %d)", i, slen, (int)gap);
#else
                LOGI("QSM memcmp : found signature: signature[%d], len=%d)", i, slen);
#endif
                return i;
            }
        }
    }
/*
#ifdef CHECK_SIGNATURE_SEARCH_TIME
    gettimeofday(&te2, NULL);
    int gap = (int)((te2.tv_sec*1000*1000+ te2.tv_usec) - (te1.tv_sec*1000*1000+ te1.tv_usec));
    LOGI("QSM Not found - signature process time for one log=%d, ret:%d", (int)gap , ret);
#else
    LOGE("QSM service: signature len=%d, len=%d", strlen(log), len);
#endif
*/

    return -1;
}
#else
int qsm_log_switch_check_log(char* , int )
{
    return -1;
}
#endif

#ifdef SIGNATURE_ON

#ifdef SIGNATURE_EVENT_TO_LOGAGENT
static void send_signature_event_to_logagent(const char* msg) {
    if (NULL == msg || 0 == strlen(msg)) {
        LOGE("QSM send_signature_event_to_logagent - invalid param : %p", msg);
        return;
    }
    //LOGI("QSM send_signature_event_to_logagent - msg : %s", msg);

    EventItemList list;
    initList(&list, 0);
    time_t ltime;
    struct tm t;
    char timestring[32];

    time(&ltime);
    localtime_r(&ltime, &t);
    strftime(timestring, sizeof(timestring), "%Y%m%d%H%M%S", &t);

    addItem(&list, "time", timestring);
    addItem(&list, "errorcode", "ME0040");
    addItem(&list, "errormessage", msg);
    int ret = sendEvent(&list, 0);
    LOGI("QSM send_signature_event_to_logagent - sendEvent ret : %d, msg : %s", ret, msg);

    clearList(&list);
}
#endif

static int make_sig_print_log(char* buf, int buf_size, int index) {
    if (NULL == buf || index < 0 || index >= SIGNATURE_MAX_COUNT) {
        return 0;
    }
    char *ptr = buf;
    int sig_len = strlen(g_signatures[index].message);
    int jira_id_len = strlen(g_signatures[index].jira_id);
    int min_buf_size = strlen(SIG_JIRA_ID_DIVIDER) + sig_len + strlen(SIG_LOG_DIVIDER) + 1;
    if (jira_id_len > 0) {
        if (buf_size < jira_id_len + min_buf_size) {
            LOGI("QSM make_sig_print_log - buffer is not enough: %d", buf_size);
            return 0;
        }
        sprintf(ptr, "%s%s", g_signatures[index].jira_id, SIG_JIRA_ID_DIVIDER);
        ptr += jira_id_len + strlen(SIG_JIRA_ID_DIVIDER);
    } else {
        if (buf_size < strlen(SIG_NO_JIRA_ID) + min_buf_size) {
            LOGI("QSM make_sig_print_log - buffer is not enough: %d", buf_size);
            return 0;
        }
        sprintf(ptr, "%s%s", SIG_NO_JIRA_ID, SIG_JIRA_ID_DIVIDER);
        ptr += strlen(SIG_NO_JIRA_ID) + strlen(SIG_JIRA_ID_DIVIDER);
    }
    strncpy(ptr, g_signatures[index].message, sig_len >> 1);
    ptr += sig_len >> 1;
    strncpy(ptr, SIG_LOG_DIVIDER, strlen(SIG_LOG_DIVIDER));
    ptr += strlen(SIG_LOG_DIVIDER);
    strcpy(ptr, g_signatures[index].message + (sig_len >> 1));

    return strlen(buf);
}

#ifdef DEBUG_SIGNATURE_MSG
static void print_signature_log(char* tag, int index) {
    char log[SIGNATURE_MAX_LEN + JIRA_ID_MAX_LEN + 64] = {0,};
    char* sig = strstr(g_signatures[index].found_log, g_signatures[index].message);
    int sig_len = strlen(g_signatures[index].message);
    if (sig && sig_len > 0) {
        int curr = sig - g_signatures[index].found_log;
        memcpy(log, g_signatures[index].found_log, curr);
        strcpy(log + curr, " [[[ ");
        curr += 5;
        int mod_sig_len = make_sig_print_log(log + curr, sizeof(log) - curr, index);
        curr += mod_sig_len;
        strcpy(log + curr, " ]]] ");
        curr += 5;
        if (sig + sig_len) {
            memcpy(log + curr, sig + sig_len, strlen(sig + sig_len));
        }
        LOGI("QSM %s - signature[%d] cnt : %d, time : %s, msg : %s"
            , tag, index, g_signatures[index].count, g_signatures[index].log_time, log);
    } else {
        LOGI("QSM %s - signature[%d] cnt : %d, time : %s"
            , tag, index, g_signatures[index].count, g_signatures[index].log_time);
    }
}
#endif

static void qsm_log_switch_send_signature(int index, int count = 0) {
    Json::Value signature;

    if(!g_init_type_received){
        if(qsm_log_switch_read_init_json() < 0){
            LOGI("QSM service: INIT type message was not received. so new message was removed - signature");
            return;
        }
        LOGI("QSM service: INIT type message was not received. but read from file - signature");
    }

    //jsonSendRoot.clear();
    Json::Value jsonSendRoot;

    pthread_mutex_lock(&s_json_data_mutex);
    jsonSendRoot = sJsonInitRoot;
    pthread_mutex_unlock(&s_json_data_mutex);

    jsonSendRoot["log_info_type"] = "Q1";
    jsonSendRoot["log_time"] = g_signatures[index].log_time;

    signature["jira_id"] = g_signatures[index].jira_id;
    signature["message"] = g_signatures[index].found_log;
    if (count > 0) {
        signature["count"] = count;
    }
    jsonSendRoot["signature"] = signature;
    jsonSendRoot["client_send_time"] = currentDateTime();

    sOutputConfig = "";
    Json::StyledWriter jsonWriter;
    sOutputConfig = jsonWriter.write( jsonSendRoot );

    LOGI("QSM qsm_log_switch_send_signature - call qsm_send [%d] cnt : %d, time : %s"
        , index, count, jsonSendRoot["log_time"].asString().c_str());

    if(qsm_send((char*)sOutputConfig.c_str(), (int)sOutputConfig.length(), 0)==0){
#ifdef DEBUG_SIGNATURE_MSG
        print_signature_log((char*)"QSM service: qsm_log_switch_send_signature - sent", index);
#else
        LOGI("QSM service: sent signature, index=%d", index);
#endif
#ifdef SIGNATURE_EVENT_TO_LOGAGENT
        char send_msg[SIG_PRINT_LOG_MAX_LEN] = {0,};
        make_sig_print_log(send_msg, SIG_PRINT_LOG_MAX_LEN, index);
        send_signature_event_to_logagent(send_msg);
#endif
    }else{
        LOGI("QSM service: failed to send signature, index=%d", index);
    }
}
#else
void qsm_log_switch_send_signature(int , const char* , int count = 0) {
}
#endif

static bool qsm_set_log_info(int sig_index, const char* log) {
    if (g_signatures && g_signature_count > 0 && sig_index >= 0 && sig_index < g_signature_count) {
        char buf[LOG_TIME_MAX_LEN] = {0,};
        if (0 == g_signatures[sig_index].count
            && 0 == g_signatures[sig_index].time_base.tv_sec && 0 == g_signatures[sig_index].time_base.tv_usec) {
            gettimeofday(&g_signatures[sig_index].time_base, NULL);
            time_to_string(g_signatures[sig_index].log_time, LOG_TIME_MAX_LEN, g_signatures[sig_index].time_base);
            memset(g_signatures[sig_index].found_log, 0, SIGNATURE_MAX_LEN);
            strncpy(g_signatures[sig_index].found_log, log, SIGNATURE_MAX_LEN - 1);
            return true;
        }
        timeval tv;
        gettimeofday(&tv, NULL);
        time_to_string(g_signatures[sig_index].log_time, LOG_TIME_MAX_LEN, tv);
        g_signatures[sig_index].count++;
#ifdef DEBUG_SIGNATURE_MSG
        //print_signature_log((char*)"qsm_set_log_info - duplicated log", sig_index);
#else
        //LOGI("QSM qsm_set_log_info - sig [%d] duplicated log added count : %d, time str : %s"
            //, sig_index, g_signatures[sig_index].count, g_signatures[sig_index].log_time);
#endif
    }
    return false;
}

static int qsm_send_dup_log() {
    if (g_signatures && g_signature_count > 0) {
        timeval curr_tv;
        gettimeofday(&curr_tv, NULL);
        for (int i = 0; i < g_signature_count; i++) {
            if (g_signatures[i].time_base.tv_sec > 0 && curr_tv.tv_sec - g_signatures[i].time_base.tv_sec >= DUP_LOG_SKIP_TIME_SEC) {
                if (g_signatures[i].count > 0) {
                    LOGI("QSM found duplicated log to send [%d] count : %d", i, g_signatures[i].count);
                    qsm_log_switch_send_signature(i, g_signatures[i].count);
                }
                memset(g_signatures[i].found_log, 0, SIGNATURE_MAX_LEN);
                memset(g_signatures[i].log_time, 0, LOG_TIME_MAX_LEN);
                g_signatures[i].time_base.tv_sec = 0;
                g_signatures[i].time_base.tv_usec = 0;
                g_signatures[i].count = 0;
            }
        }
    }
    return 0;
}

void qsm_log_switch_check_signature(const char* log, int len) {
    pthread_mutex_lock(&g_signature_mutex);
    int index = qsm_log_switch_check_log(log, len);
    if(index >= 0){
        //LOGI("QSM found sig index : %d, dup. cnt : %d, time : %ld, %ld", index, g_signatures[index].count
            //, g_signatures[index].time_base.tv_sec, g_signatures[index].time_base.tv_usec);
        if (qsm_set_log_info(index, log)) {
            LOGI("QSM found sig. [%d], send msg. with count 0", index);
            qsm_log_switch_send_signature(index);
        }
    }
    qsm_send_dup_log();
    pthread_mutex_unlock(&g_signature_mutex);
}

void qsm_log_switch_check_update_status(void) {
    struct stat result;
    bool  is_update_status = false;
    char temp[5];
    char buf_file[80];
    char buf_cur[80];
    struct tm  tstruct_now, tstruct_file, tstruct_update_start;
    int year, mon, day, hour, min, sec;
    bool have_info_to_send = false;
#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
    char upgradesvc_log[128];
	sprintf(upgradesvc_log, "%s/%s", qsmconfigDir, UPGRADESVC_LOG);
#endif/*TABLET_BUILD*/

    if(g_update_check_done){
        return;
    }

    //sleep(60);

    if(!g_init_type_received){
        if(qsm_log_switch_read_init_json() < 0){
            LOGI("QSM service: INIT type message was not received. so new message was removed - check update status");
            return;
        }
        LOGI("QSM service: INIT type message was not received. but read from file - check update status");
    }

#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
    if(stat((char*)upgradesvc_log, &result)==0)
#else
    if(stat((char*)"/data/btv_home/run/upgradesvc.log", &result)==0)
#endif/*TABLET_BUILD*/
    {
        g_update_check_done = true;

        if (false == g_log_switch_array[stb_upgrade].log_on) {
            LOGI("QSM check update status : skip sending stb_upgrade - log switch off");
            return;
        }

        time_t     now = time(0);

        tstruct_now = *localtime(&now);
        tstruct_file = *localtime(&result.st_mtime);

        strftime(buf_cur, sizeof(buf_cur), "%Y%m%d%H%M%S", &tstruct_now);
        strftime(buf_file, sizeof(buf_file), "%Y%m%d%H%M%S", &tstruct_file);

        LOGI("QSM service: check_update_status file time=%s, cur time=%s", buf_file, buf_cur);

        int gap = (int) (tstruct_now.tm_year - tstruct_file.tm_year);
        if(gap == 0){
              gap = (int) (tstruct_now.tm_mon - tstruct_file.tm_mon);
              if(gap == 0){
                  gap = (int) (tstruct_now.tm_mday - tstruct_file.tm_mday);
                  if(gap == 0){
                      gap = (int) (tstruct_now.tm_hour - tstruct_file.tm_hour);
                      if(gap == 0){
                          gap = (int) (tstruct_now.tm_min - tstruct_file.tm_min);
                          if(gap < UPDATE_CHECK_TIME_GAP_MIN){
                              LOGI("QSM check_update_status - upgradesvc log file was changed");
                              is_update_status = true;
                          }
                      }
                   }
              }
        }
    } else {
        LOGI("QSM check_update_status : stat upgradesvc log file error : %s", strerror(errno));
    }

    if(is_update_status){

        bool  found = false;
        char last_update_log[1024];
        int sdk_ver = 0;
        char upgrade_start_log[1024] = {0,};
        char value[PROP_VALUE_MAX];
        int ver;
        __system_property_get("ro.build.version.sdk", value);
        ver = atoi(value);

#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
        std::ifstream file(upgradesvc_log);
#else
        std::ifstream file("/data/btv_home/run/upgradesvc.log");
#endif/*TABLET_BUILD*/
        if (file.is_open()) {
            std::string line;
            if (sdk_ver >= 28) {
                strcpy(upgrade_start_log, "process app start by reason[RECOVERY]");
            } else {
                strcpy(upgrade_start_log, "# Recv [UP_MSG_DOWNLOAD_START] msg");
            }
            while (std::getline(file, line)) {
                char* next = strstr((char*)line.c_str(), upgrade_start_log);
                if(next){
                    found = true;
                    strcpy(last_update_log, (char*)line.c_str());
                }
            }
            file.close();
        } else {
            LOGI("QSM check_update_status : open upgradesvc log file error : %s", strerror(errno));
        }

        if(!found){
            LOGI("QSM service: check_update_status - no update log");
            return;
        }

        /* adjust time */
        tstruct_now.tm_year +=1900;
        tstruct_now.tm_mon +=1;
        if(tstruct_now.tm_min > 2){
            tstruct_now.tm_min -=2;
        }

        if (sscanf(last_update_log, "%1s %4d-%2d-%2d %2d:%2d:%2d",temp, &year, &mon, &day, &hour, &min, &sec) != 7)
            LOGE("check_update_status - Invalid string %s", last_update_log);
        else
            LOGI("check_update_status - last_update_log=%s", last_update_log);

        LOGI("check_update_status - now : year=%d,mon=%d, day=%d, hour=%d, min=%d",
            tstruct_now.tm_year, tstruct_now.tm_mon, tstruct_now.tm_mday,
            tstruct_now.tm_hour, tstruct_now.tm_min);
        LOGI("check_update_status - update start time : year=%d,mon=%d, day=%d, hour=%d, min=%d",
            year, mon, day, hour, min);

        int gap = (int) (tstruct_now.tm_year - year);
        if(gap == 0){
            gap = (int) (tstruct_now.tm_mon - mon);
            if(gap == 0){
                gap = (int) (tstruct_now.tm_mday - day);
                if(gap == 0){
                    gap = (int) (tstruct_now.tm_hour - hour);
                    if(gap == 0){
                        gap = (int) (tstruct_now.tm_min - min);
                        if(gap < UPDATE_CHECK_TIME_GAP_MIN){
                            LOGI("QSM check_update_status - update operation was started a few minutes ago");
                            have_info_to_send = true;
                        }
                    }
                }
            }
        }

        if(!have_info_to_send){
            return;
        }

        tstruct_update_start.tm_year = year-1900;
        tstruct_update_start.tm_mon = mon-1;
        tstruct_update_start.tm_mday = day;
        tstruct_update_start.tm_hour = hour;
        tstruct_update_start.tm_min = min;
        tstruct_update_start.tm_sec = sec;

        /* send start update message with "/data/btv_home/run/upgradesvc.log"'s update time */

        char time_string[80];
        strftime(time_string, sizeof(time_string), "%Y%m%d%H%M%S", &tstruct_update_start);
        sprintf(g_tot_time_buf,"%s.%03d",time_string, 0);
        LOGI("QSM service: check_update_status update time=%s, cur time=%s", time_string, buf_cur);

        //jsonSendRoot.clear();
        Json::Value jsonSendRoot;

        pthread_mutex_lock(&s_json_data_mutex);
        jsonSendRoot = sJsonInitRoot;
        pthread_mutex_unlock(&s_json_data_mutex);

        jsonSendRoot["log_info_type"] = "Q1";
        sprintf(g_tot_time_buf,"%s.%d",buf_file, 0);
        jsonSendRoot["log_time"] = g_tot_time_buf;
        if (g_log_switch_array[stb_upgrade].log_on) {
            jsonSendRoot["stb_upgrade"] = "000";
        } else {
            LOGI("qsm log_on is false - skip stb_upgrade");
        }
        jsonSendRoot["client_send_time"] = currentDateTime();

        sOutputConfig = "";
        Json::StyledWriter jsonWriter;
        sOutputConfig = jsonWriter.write( jsonSendRoot );
        LOGI("QSM qsm_log_switch_check_update_status time : %s - call qsm_send"
            , jsonSendRoot["log_time"].asString().c_str());
        if(qsm_send((char*)sOutputConfig.c_str(), (int)sOutputConfig.length(), 0) != 0){
            g_update_check_done = false;
            return;
        }

        /* send the result message of update with /cache/recovery/last_log */
        jsonSendRoot.clear();

        pthread_mutex_lock(&s_json_data_mutex);
        jsonSendRoot = sJsonInitRoot;
        pthread_mutex_unlock(&s_json_data_mutex);

        jsonSendRoot["log_info_type"] = "Q1";
        jsonSendRoot["log_time"] = currentDateTime();

        if (g_log_switch_array[stb_upgrade].log_on) {
            if(isUpdateSuccess()){
                jsonSendRoot["stb_upgrade"] = "100";
            }else{
                jsonSendRoot["stb_upgrade"] = "900";
            }
            jsonSendRoot["client_send_time"] = currentDateTime();
        } else {
            LOGI("qsm log_on is false - skip stb_upgrade");
        }

        sOutputConfig = "";
        Json::StyledWriter jsonWriterResult;
        sOutputConfig = jsonWriterResult.write( jsonSendRoot );
        LOGI("QSM qsm_log_switch_check_update_status - upgrade result : %s, time :%s - call qsm_send"
            , jsonSendRoot["stb_upgrade"].asString().c_str()
            , jsonSendRoot["log_time"].asString().c_str());
        if(qsm_send((char*)sOutputConfig.c_str(), (int)sOutputConfig.length(), 0) != 0){
            g_update_check_done = false;
        }
    }
}


void qsm_log_switch_send_info_after_one_day(void) {

    double now = now_sec();
    if((now - g_one_day_check_time) > TIME_LIMIT){

        if(!g_init_type_received){
            if(qsm_log_switch_read_init_json() < 0){
                LOGI("QSM service: INIT type message was not received. so new message was removed - info_after_one_day");
                return;
            }
            LOGI("QSM service: INIT type message was not received. but read from file - info_after_one_day");
        }

        if (false == g_log_switch_array[D014].log_on) {
            LOGI("QSM send info after one day : skip sending D014 - log switch off");
            return;
        }

        //jsonSendRoot.clear();
        Json::Value jsonSendRoot;

        pthread_mutex_lock(&s_json_data_mutex);
        jsonSendRoot = sJsonInitRoot;
        pthread_mutex_unlock(&s_json_data_mutex);

        jsonSendRoot["log_info_type"] = "Q1";
        jsonSendRoot["log_time"] = currentDateTime();
        s_qsm_log_switch_set_D014(jsonSendRoot);
        jsonSendRoot["client_send_time"] = currentDateTime();

        sOutputConfig = "";
        Json::StyledWriter jsonWriter;
        sOutputConfig = jsonWriter.write( jsonSendRoot );
        LOGI("QSM qsm_log_switch_send_info_after_one_day - call qsm_send");
        if(qsm_send((char*)sOutputConfig.c_str(), (int)sOutputConfig.length(), 0) == 0){
            LOGI("QSM send_info_after_one_day");
            g_one_day_check_time = now;
        }
    }
}

void qsm_navigator_client_send(char* buf){
    Json::Value info;

    if(!g_init_type_received){
        if(qsm_log_switch_read_init_json() < 0){
            LOGI("QSM service: INIT type message was not received. so new message was removed - navigator client");
            return;
        }
        LOGI("QSM service: INIT type message was not received. but read from file - navigator client");
    }
    // D026 navigator_error log switch
   if ( false == qsm_log_switch_is_Navigator_on()) {
        LOGI("QSM : log switch application off - skip Navigator APPLICATION event");
        return ;
    }

    char D002_Time[20] ={0,};
    strncpy(D002_Time,buf,18);

    //jsonSendRoot.clear();
    Json::Value jsonSendRoot;

    pthread_mutex_lock(&s_json_data_mutex);
    sJsonInitRoot["client_send_time"] = currentDateTime();
    jsonSendRoot = sJsonInitRoot;
    pthread_mutex_unlock(&s_json_data_mutex);

    jsonSendRoot["log_info_type"] = "Q1";
    jsonSendRoot["log_time"] = D002_Time;

    /*D026*/
    info["message"] = buf+NAVIGATOR_TIME_LENGTH+1;   /*Plus TIME_LENGTH */
    jsonSendRoot["navigator_error"] = info;

    sOutputConfig = "";
    Json::StyledWriter jsonWriter;
    sOutputConfig = jsonWriter.write( jsonSendRoot );
    qsm_send((char*)sOutputConfig.c_str(), (int)sOutputConfig.length(), 0);

    LOGI("QSM service: Navigator send data=%s", sOutputConfig.c_str());
}

void qsm_clear_rtp_status(void){
    memset(&g_rtp_status, 0, sizeof(g_rtp_status));
    LOGI("QSM service: qsm_clear_rtp_status");
}

void qsm_update_rtp_status(char* status){

    int status_num;

    if(!status){
        return;
    }
    g_rtp_status.error_occur = 1;

    status_num = atoi(status);
    switch(status_num){
        case PACKET_DUPLICATION:
            if(g_rtp_status.pkt_duplication < 2147483647) g_rtp_status.pkt_duplication++;
            break;
        case PACKET_LOSS:
            if(g_rtp_status.pkt_loss < 2147483647) g_rtp_status.pkt_loss++;
            break;
        case PACKET_OUT_OF_ORDER:
            if(g_rtp_status.pkt_out_of_order < 2147483647) g_rtp_status.pkt_out_of_order++;
            break;
        case JITTER_ERROR:
            if(g_rtp_status.jitter_error < 2147483647) g_rtp_status.jitter_error++;
            break;
    }
    // for debug and test log not release version
    LOGI("QSM service: qsm_update_rtp_status-status=%d, duplication=%d, loss=%d, outof_order=%d, jitter error=%d", status_num, g_rtp_status.pkt_duplication, g_rtp_status.pkt_loss, g_rtp_status.pkt_out_of_order, g_rtp_status.jitter_error);
}

void qsm_init_rtp_send_interval(void){

    char nBuffer[20];
#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
    char rtp_send_interval_file[128];
	sprintf(rtp_send_interval_file, "%s/%s", qsmconfigDir, QSM_RTP_SEND_INTERVAL_MINUTES);
    int fd = open(rtp_send_interval_file, O_RDONLY);
#else
    int fd = open(RTP_SEND_INTERVAL_FILE, O_RDONLY);
#endif/*TABLET_BUILD*/
    if(fd < 0)
    {
        std::ofstream file_id;
#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
        file_id.open((char*)rtp_send_interval_file);
#else
        file_id.open((char*)RTP_SEND_INTERVAL_FILE);
#endif/*TABLET_BUILD*/
        if(file_id.is_open()){
            file_id << RTP_SEND_INTERVAL_DEFAUT_MINUTES;
            file_id.close();
        } else {
#ifdef TABLET_BUILD //cracker@yiwoosolution.co.kr
            LOGE("QSM qsm_init_rtp_send_interval - file open fail : %s", (char*)rtp_send_interval_file);
#else        
            LOGE("QSM qsm_init_rtp_send_interval - file open fail : %s", (char*)RTP_SEND_INTERVAL_FILE);
#endif/*TABLET_BUILD*/
        }
        g_check_rtp_status_duration = RTP_SEND_INTERVAL_DEFAUT_MINUTES;
        LOGI("QSM qsm_init_rtp_send_interval - file open fail : %d file save default value", g_check_rtp_status_duration);
    }else{

        if(read(fd, nBuffer, sizeof(nBuffer)) <= 0) {
            g_check_rtp_status_duration = RTP_SEND_INTERVAL_DEFAUT_MINUTES;
            LOGI("QSM qsm_init_rtp_send_interval - file has no data : default value set[%d]", g_check_rtp_status_duration);
        }
        else {
            g_check_rtp_status_duration = atoi(nBuffer);
            // for debug and test log not release version
            LOGI("QSM qsm_init_rtp_send_interval - file has data : value set[%d]", g_check_rtp_status_duration);
        }
        close(fd);
    }

    g_check_rtp_status_count = g_check_rtp_status_duration;
}

void qsm_init_zero_rtp_send_interval(void){
    g_check_rtp_status_count = 0;
     LOGI("QSM qsm_init_zero_rtp_send_interval : value set[%d]", g_check_rtp_status_count);
}

void qsm_send_rtp_status(void){

    // for debug and test log not release version
    LOGI("QSM service: qsm_send_rtp_status  g_check_rtp_status_count=%d, g_rtp_status.error_occur=%d start for debug",g_check_rtp_status_count, g_rtp_status.error_occur);

    if( 0 < g_check_rtp_status_count){
        --g_check_rtp_status_count;
    }

    if(g_check_rtp_status_count == 0){
        //g_check_rtp_status_count = g_check_rtp_status_duration;
        qsm_init_rtp_send_interval();

        LOGI("QSM service: qsm_send_rtp_status g_check_rtp_status_count=%d, g_rtp_status.error_occur=%d check there are data to send",g_check_rtp_status_count, g_rtp_status.error_occur);
        if(g_rtp_status.error_occur != 0){
            if(!g_init_type_received){
                if(qsm_log_switch_read_init_json() < 0){
                    LOGI("QSM service: INIT type message was not received. so new message was removed - rtp status");
                    return;
                }
                LOGI("QSM service: INIT type message was not received. but read from file - rtp status");
            }

            // D027 rtp_error log switch
            if (false == qsm_log_switch_is_Rtp_on()) {
                LOGI("QSM : log switch application off - skip Rtp Log");
                qsm_clear_rtp_status();
                return ;
            }

            Json::Value info;

            //jsonSendRoot.clear();
            Json::Value jsonSendRoot;

            pthread_mutex_lock(&s_json_data_mutex);
            sJsonInitRoot["client_send_time"] = currentDateTime();
            jsonSendRoot = sJsonInitRoot;
            pthread_mutex_unlock(&s_json_data_mutex);

            info["packet_duplication"] = g_rtp_status.pkt_duplication;
            info["packet_loss"] = g_rtp_status.pkt_loss;
            info["packet_out_of_order"] = g_rtp_status.pkt_out_of_order;
            info["jitter_error"] = g_rtp_status.jitter_error;

            jsonSendRoot["rtp_error"] = info;

            sOutputConfig = "";
            Json::StyledWriter jsonWriter;
            sOutputConfig = jsonWriter.write( jsonSendRoot );
            qsm_send((char*)sOutputConfig.c_str(), (int)sOutputConfig.length(), 0);

            qsm_clear_rtp_status();

            LOGI("QSM service: qsm_send_rtp_status send data=%s", sOutputConfig.c_str());
        }
    }
}

char* qsm_log_switch_get_mac_addr(void){

    if(g_mac_addr[0] == 0){
        return NULL;
    }
    return g_mac_addr;
}

#ifdef TABLET_BUILD  //cracker@yiwoosolution.co.kr
void qsm_log_switch_set_mac_addr(char *addr, int length){

  memset(g_mac_addr, 0, 100);
  memcpy(g_mac_addr, addr, length);
}

char* qsm_log_switch_get_model_name(void){

    if(g_model_name[0] == 0){
        return NULL;
    }
    return g_model_name;
}

void qsm_log_switch_set_model_name(char *model_name, int length){

  memset(g_model_name, 0, 100);
  memcpy(g_model_name, model_name, length);
}
#endif/*TABLET_BUILD*/

bool   qsm_log_switch_is_signature_on(void){
    //LOGI("QSM qsm_log_switch_is_signature_on:g_signature_count=%d", g_signature_count);
    return (g_signature_count > 0);
}

bool qsm_log_switch_is_Navigator_on(void){
    return g_log_switch_array[navigator_error].log_on;
}

bool qsm_log_switch_is_Rtp_on(void){
    return g_log_switch_array[rtp_error].log_on;
}

bool qsm_rtp_checker_on(void){
    bool rtp_checker_on = false;

    char model_name[50] = {0,};
    __system_property_get("ro.product.model", model_name);

    if (0 == strcmp(model_name, "BIP-AI100") || 0 == strcmp(model_name, "BID-AI100")||
        0 == strcmp(model_name, "BFX-AT100") || 0 == strcmp(model_name, "BFX-UA300")||
        0 == strcmp(model_name, "BIP-UW200") || 0 == strcmp(model_name, "BID-AT200"))
    {
        rtp_checker_on = true;
    }

    return rtp_checker_on;
}
