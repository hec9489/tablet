package com.skb.qsm2agent;

import android.app.ActivityManager;
import android.app.Service;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;

import com.skb.qsm2lib.QSM2Report;
import com.skb.qsm2lib.Values.LOG_INFO_T;
import com.skb.qsm2lib.Values.SN_PARAM;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static android.media.AudioManager.ACTION_HDMI_AUDIO_PLUG;
import static android.media.AudioManager.EXTRA_AUDIO_PLUG_STATE;
import static com.skb.qsm2lib.Values.TRANS_TYPE.BT_PAIRING;
import static com.skb.qsm2lib.Values.TRANS_TYPE.HDMI_STATUS_CHANGED;
import static com.skb.qsm2lib.Values.TRANS_TYPE.INIT;

public class SkbLogService extends Service {

    private final static String TAG = SkbLogService.class.getSimpleName();

    private ActionReceiver actionReceiver = new ActionReceiver();

    public SkbLogService() {
    }

    /**
     * 최초 한번 호출됨
     */
    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        super.onCreate();

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_HDMI_AUDIO_PLUG); // HDMI Event
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED); //BlueTooth
//        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(actionReceiver, filter);

    }

    /*
        서비스가 호출 될때마다
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        unregisterReceiver(actionReceiver);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public class ActionReceiver extends BroadcastReceiver {

        private final String TAG = this.getClass().getName();

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "action : " + action);
            if (ACTION_HDMI_AUDIO_PLUG.equals(action)) {
                // EXTRA_AUDIO_PLUG_STATE: 0 - UNPLUG, 1 - PLUG
                int state = intent.getIntExtra(EXTRA_AUDIO_PLUG_STATE, -1);
                Map<String, Object> data = new HashMap<>();
                data.put(SN_PARAM.D001_log_info_type, LOG_INFO_T.DEVICE);
                data.put(SN_PARAM.D002_log_time, QSM2Report.getLogTime());
                data.put(SN_PARAM.D008_hdmi_plug_inout, state);
                sendData(HDMI_STATUS_CHANGED, data);
            } else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (device != null) {
                    BluetoothClass bluetoothClass = device.getBluetoothClass();
                    Log.d(TAG, "name : " + device.getName() + ", mac : " + device.getAddress() +
                            " desc= " + bluetoothClass.toString()
                            + " deviceClass= " + bluetoothClass.getDeviceClass()
                            + " majorDeviceClass= " + bluetoothClass.getMajorDeviceClass()
                            + " deviceProfile= "+ getDeviceProfile(bluetoothClass.getDeviceClass())
                            + " isRemocon= " + isRemocon(device.getAddress())
                    );
                    if (!isRemocon(device.getAddress())) {
                        Map<String, Object> data = new HashMap<>();
                        data.put(SN_PARAM.D001_log_info_type, LOG_INFO_T.DEVICE);
                        data.put(SN_PARAM.D002_log_time, QSM2Report.getLogTime());
                        try {
                            JSONObject json = new JSONObject();
                            json.put("bt_device_name", device.getName());
                            json.put("bt_mac", device.getAddress());
                            json.put("bt_device_profile", getDeviceProfile(bluetoothClass.getDeviceClass()));
                            json.put("bt_status", 1);
                            data.put(SN_PARAM.D013_device_info_bt, json);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        sendData(BT_PAIRING, data);
                    }
                }
            }
        }
    }

    private void sendData(String type, Map<String, Object> map) {
        QSM2Report report = new QSM2Report(getApplicationContext());
        if (!report.initialize()) {
            Log.d(TAG, "failed to initialize QSM");
            return;
        }
        report.start(type);
        for (String key : map.keySet()) {
            Object value = map.get(key);
            if (value instanceof Integer) {
                report.put(key, (Integer) value);
            } else if (value instanceof JSONObject) {
                report.put(key, (JSONObject) value);
            } else {
                report.put(key, (String) value);
            }
        }
        Log.d(TAG, report.getReportString());
        //파일 생성 시간 필요  1초 주기
        try {
            Thread.sleep(1200);
        } catch (Exception ignored){ }
        if (report.report() > 0) {
            Log.d(TAG, "report SUCCESS");
        } else {
            //리포트 실패시
            Log.d(TAG, "report FAIL");
        }
        //릴리즈하기 전 여유시간 1초 주기
        try {
            Thread.sleep(1000);
        } catch (Exception ignored){ }
        report.release();
    }

    public static boolean isServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo info : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(info.service.getClassName())) {
                Log.d(TAG, "ServiceRunning = true");
                return true;
            }
        }
        Log.d(TAG, "ServiceRunning = false");
        return false;
    }

    public static String getFileRead(String path) {
        BufferedReader buffered_reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            buffered_reader = new BufferedReader(new FileReader(path));
            String line;

            while ((line = buffered_reader.readLine()) != null) {
                Log.e(TAG, line);
                builder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (buffered_reader != null)
                    buffered_reader.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return builder.toString();
    }

    public static String getDateTime() {
        return new SimpleDateFormat("yyyyMMddHHmmss.SSS", Locale.getDefault()).format(new Date());
    }

    private String getDeviceProfile(int value) {
        String profile = "";
        if (value == BluetoothClass.Device.AUDIO_VIDEO_HIFI_AUDIO ||
                value == BluetoothClass.Device.AUDIO_VIDEO_HEADPHONES ||
                value == BluetoothClass.Device.AUDIO_VIDEO_LOUDSPEAKER ||
                value == BluetoothClass.Device.AUDIO_VIDEO_CAR_AUDIO
        ) {
            profile += "A2DP";
        }
        if (value == BluetoothClass.Device.AUDIO_VIDEO_HANDSFREE ||
                value == BluetoothClass.Device.AUDIO_VIDEO_WEARABLE_HEADSET ||
                value == BluetoothClass.Device.AUDIO_VIDEO_CAR_AUDIO
        ) {
            profile += "HEADSET";
        }
        if (value == BluetoothClass.Device.AUDIO_VIDEO_HIFI_AUDIO ||
                value == BluetoothClass.Device.AUDIO_VIDEO_SET_TOP_BOX ||
                value == BluetoothClass.Device.AUDIO_VIDEO_VCR
        ) {
            profile += "A2DP_SINK";
        }
        if (value == BluetoothClass.Device.COMPUTER_UNCATEGORIZED ||
                value == BluetoothClass.Device.COMPUTER_DESKTOP ||
                value == BluetoothClass.Device.COMPUTER_SERVER ||
                value == BluetoothClass.Device.COMPUTER_LAPTOP ||
                value == BluetoothClass.Device.COMPUTER_HANDHELD_PC_PDA ||
                value == BluetoothClass.Device.COMPUTER_PALM_SIZE_PC_PDA ||
                value == BluetoothClass.Device.COMPUTER_WEARABLE ||
                value == BluetoothClass.Device.PHONE_UNCATEGORIZED ||
                value == BluetoothClass.Device.PHONE_CELLULAR ||
                value == BluetoothClass.Device.PHONE_CORDLESS ||
                value == BluetoothClass.Device.PHONE_SMART ||
                value == BluetoothClass.Device.PHONE_MODEM_OR_GATEWAY ||
                value == BluetoothClass.Device.PHONE_ISDN
        ) {
            profile += "OPP";
        }
        if ((value & BluetoothClass.Device.Major.PERIPHERAL) == BluetoothClass.Device.Major.PERIPHERAL) {
            profile += "HID";
        }
        if ((value & BluetoothClass.Device.Major.NETWORKING) == BluetoothClass.Device.Major.NETWORKING) {
            profile += "PANU_NAP";
        }
        return profile;
    }

    private Boolean isRemocon(String address) {
        if (address == null) return false;
        String REMOCON_DEVICES = "20:73:3E|8C:08:8B|00:13:7B|14:4E:34|9C:AC:6D";
        String[] addresses = REMOCON_DEVICES.split("\\|");
        for (String value : addresses) {
            if (address.startsWith(value)) return true;
        }
        return false;
    }
}
