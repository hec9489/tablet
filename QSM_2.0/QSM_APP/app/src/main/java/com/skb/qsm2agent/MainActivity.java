package com.skb.qsm2agent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import static com.skb.qsm2agent.SkbLogService.isServiceRunning;

public class MainActivity extends Activity {

    private final String TAG = this.getClass().getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        // 서비스가 실행중 확인
        if (!isServiceRunning(this, SkbLogService.class)) {
            Log.d(TAG, "startService");
            startService(new Intent(this, SkbLogService.class));
        }
//        moveTaskToBack(true);
        finish();
    }

}
