package com.skb.qsm2agent;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BootReceiver extends BroadcastReceiver {

    private final String TAG = this.getClass().getSimpleName();

    @Override
    public void onReceive(final Context context, Intent intent) {
        String action = intent.getAction();
        Log.d(TAG, "action = " + action);
        // 부팅완료 경우에만 동작
        if (Intent.ACTION_BOOT_COMPLETED.equals(action)) {
//            new Handler().postDelayed(new Runnable() {
//                // 3초 후에 실행
//                @Override
//                public void run() {
//                    Toast.makeText(context, "-- BootReceiver.onReceive", Toast.LENGTH_LONG).show();
//                    // BackgroundService
//            Intent serviceLauncher = new Intent(context, SkbLogService.class);
            context.startService(new Intent(context, SkbLogService.class));
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                        context.startForegroundService(serviceLauncher);
//                    } else {
//                    }
//                }
//            }, 3000);

        }
    }


}


