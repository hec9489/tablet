package com.skb.qsm2lib.Values.Device;

/* NETWORK TYPE */
public class NETWORK_TYPE{
    public static final String ETHERNET = "ETHERNET";
    public static final String WIFI = "WIFI";
    public static final String LTE = "LTE";
    public static final String FIVE_G = "5G";
}