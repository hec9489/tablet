package com.skb.qsm2lib.Values.Command;

/* action */
public class ACTION{
    public static final int LIVE_OFF = 0;
    public static final int LIVE_ON = 1;

    public static final int VISIBILITY_ENABLE = 1;
    public static final int VISIBILITY_DISABLE = 2;
    public static final int VISIBILITY_ENABLE_BY_FORCE = 3;
}