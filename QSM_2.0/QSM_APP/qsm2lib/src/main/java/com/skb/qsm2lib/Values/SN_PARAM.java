package com.skb.qsm2lib.Values;

/* Standard Number */
public class SN_PARAM{

    /* Application */
    public static final String A001_log_info_type = "log_info_type";
    public static final String A002_log_time = "log_time";
    public static final String A003_webui_version = "webui_version";
    public static final String A004_app_release_version = "app_release_version";
    public static final String A005_subapp_release_version = "subapp_release_version";
    public static final String A006_server_name = "server_name";
    public static final String A007_page_id = "page_id";
    public static final String A008_menu_id = "menu_id";
    public static final String A009_gnb_id = "gnb_id";
    public static final String A010_sub_gnb_id = "sub_gnb_id";
    public static final String A011_vod_watch_type = "vod_watch_type";
    public static final String A012_error_text = "error_text";
    public static final String A013_error_description = "error_description";
    public static final String A014_contents_info = "contents_info";

    /* Device */
    public static final String D001_log_info_type = "log_info_type";
    public static final String D002_log_time = "log_time";
    public static final String D003_device_info = "device_info";
    public static final String D004_system_info = "system_info";
    public static final String D005_network_info = "network_info";
    public static final String D006_service_mode = "service_mode";
    public static final String D007_stb_upgrade = "stb_upgrade";
    public static final String D008_hdmi_plug_inout = "hdmi_plug_inout";
    public static final String D009_hdmi_hdcp_version = "hdmi_hdcp_version";
    public static final String D010_device_info_hdmi = "device_info_hdmi";
    public static final String D011_device_info_rcu = "device_info_rcu";
    public static final String D012_device_info_rcu_error = "device_info_rcu_error";
    public static final String D013_device_info_bt = "device_info_bt";
    public static final String D014_user_config = "user_config";
    public static final String D015_contents_info = "contents_info";
    public static final String D016_video_info = "video_info";
    public static final String D017_audio_info = "audio_info";
    public static final String D018_error_status_info = "error_status_info";
    public static final String D019_server_check_result = "server_check_result";
    public static final String D020_btv_error = "btv_error";
    public static final String D021_vod_error = "vod_error";
    public static final String D022_signature = "signature";
    public static final String D023_device_debug_info = "device_debug_info";
    public static final String D024_device_postmortem = "device_postmortem";
    public static final String D025_hdmi_debug_info = "hdmi_debug_info";

    /* Common */
    public static final String C001_client_send_time = "client_send_time";
    public static final String C002_mac_address = "mac_address";
    public static final String C003_device_id = "device_id";
    public static final String C004_zip_code = "zip_code";
    public static final String C005_client_ip = "client_ip";
    public static final String C006_device_base_info = "device_base_info";
    public static final String C007_version_info = "version_info";
    public static final String C008_log_info = "log_info";
}