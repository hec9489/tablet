package com.skb.qsm2lib.Values;

/* Trans type */
public class TRANS_TYPE{
    public static final String INIT = "INIT";
    public static final String COLD_BOOTING = "COLD_BOOTING";
    public static final String SLEEP_MODE = "SLEEP_MODE";
    public static final String WAKEUP_MODE = "WAKEUP_MODE";
    public static final String CPU_LIMIT = "CPU_LIMIT";
    public static final String MEMORY_LIMIT = "MEMORY_LIMIT";
    public static final String SERVICE_STATUS_CHANGED = "SERVICE_STATUS_CHANGED";
    public static final String HDMI_STATUS_CHANGED = "HDMI_STATUS_CHANGED";
    public static final String RCU_PAIRING = "RCU_PAIRING";
    public static final String BT_PAIRING = "BT_PAIRING";
    public static final String MEDIA_ERROR = "MEDIA_ERROR";
    public static final String SETTING_CHANGED = "SETTING_CHANGED";
    public static final String VOD_PLAY = "VOD_PLAY";
    public static final String HE_ERROR = "HE_ERROR";
    public static final String UPGRADE_STATUS = "UPGRADE_STATUS";
    public static final String QSM_SIGNATURE = "QSM_SIGNATURE";
    public static final String APPLICATON = "APPLICATON";
    public static final String REQUEST = "REQUEST";
}