package com.skb.qsm2lib.Values;

/* Log info type */
public class LOG_INFO_T{
    public static final String DEVICE = "Q1";
    public static final String APP_NATIVE = "Q2";
    public static final String APP_WEBUI = "Q3";
}