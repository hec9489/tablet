package com.skb.qsm2lib;

import com.skb.qsm2lib.Values.DEVICE_INFO_T;
import com.skb.qsm2lib.QSM2Report;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import android.content.Context;

public class QSM2Transfer {
    private final static String TAG = "QSM2Transfer";
    private LocalSocket mSender = null;
    private OutputStream mOutputStream = null;
    private InputStream mInputStream = null;
    private String LOCAL_SOCKET_NAME = "/dev/socket/qsm_server";
    private String INTERNAL_DIR = "data/data";
    private String PACKAGE_CONFIG_DIR = "btf/config";
    private String QSM_CONFIG_DIR = "btf/qsm/config";
    private String SERVER_LIST_CONF = "server-list.conf";
    private String QSM_INIT_DATA_JSON = "qsm_init_data.json";

    private static QSM2Report.OnReceiveCallBack mCallBack = null;

    //cracker@yiwoosolution.co.kr
    private native int native_init(String mPackageName);
    private native int native_release();
    private native int sendToserver(String dataJSON);
	//private native int navigator_init(String mPackageName);

    private Context mAppContext;

	private int SERVER_LIST_TYPE_STB = 1;
	private int SERVER_LIST_TYPE_DEV = 2;
	private int SERVER_LIST_TYPE_PRD = 3;
	private String QSM_SERVER_ID = "qsm2.0";
	private String ADDRESS_START_ATTR = "address=\"";
	private String ADDRESS_END_ATTR = "\"";
    private String SERVER_LIST_STRING_DEV = "tcp://qsmlog-dev.hanafostv.com";

    static {
        System.loadLibrary("qsm_server");
        //System.loadLibrary("Navigator_client");
    }

    public QSM2Transfer(Context context){
        if(DEVICE_INFO_T.TABLET_BUILD == 1)
        {
            String mPackageName;
            boolean bChange;

			if(context == null) return;
			mAppContext = context;

			mPackageName = mAppContext.getPackageName();
			bChange = getPackageChangeVersion(mAppContext);
			
            assetConfigRead(mAppContext, PACKAGE_CONFIG_DIR, SERVER_LIST_CONF, bChange);

            native_init(mPackageName);
		    //navigator_init(mPackageName);
        }
	}

	private boolean getPackageChangeVersion(Context context) {
        String versionName = "Unknown";
		boolean change = false;

        if (context == null) {
            return change;
        }
        try {
            PackageInfo packageInfo = context.getApplicationContext()
                    .getPackageManager()
                   .getPackageInfo(context.getApplicationContext().getPackageName(), 0 );
            versionName = packageInfo.versionName;
            Log.d(TAG, "PackageVersionName :" + versionName);

            SharedPreferences pref = context.getSharedPreferences("qsm2lib", context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            String pref_version_name = pref.getString("package_version_name", "Unknown");

			if(!versionName.equals(pref_version_name))
			{
                editor.putString("package_version_name", versionName);
                editor.apply();
                change = true;
                Log.d(TAG, "Change package_version_name :" + versionName);
			}
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "PackageVersionName :" + e.getMessage());
        }	
        
        return change;
    }

	private void makeConfigDir(Context context, String dir){
        if(DEVICE_INFO_T.TABLET_BUILD == 1)
        {
            File directory = new File(INTERNAL_DIR + "/" + context.getPackageName() + "/" + dir);
            if(!directory.exists()){
                boolean wasSuccess = directory.mkdirs();
                if(!wasSuccess){
                    Log.d(TAG, "dir " + dir + " was not success");
                }
            }
        }
    }

	private boolean assetConfigRead(Context context, String dir, String file, boolean change){
        if(DEVICE_INFO_T.TABLET_BUILD == 1)
        {
            InputStream is = null;
            AssetManager assetManager = context.getAssets();
            File confFile = new File((INTERNAL_DIR + "/" + context.getPackageName() + "/" + dir), file);

			if(!confFile.exists() || change)
			{
                try{
                    is = assetManager.open((dir + "/" +file), AssetManager.ACCESS_BUFFER);
                    
                    if(is != null)
                    {
                        int size = is.available();
                        byte[] buffer = new byte[size];
                        is.read(buffer);
                        is.close();
                        
                        makeConfigDir(context, dir);
                        FileOutputStream fos = null;
                        try{
							if(change) confFile.delete();

                            fos = new FileOutputStream(confFile);
                            fos.write(buffer);
							fos.close();

							Log.d(TAG, "QSM assetConfigRead success");
							return true;
                        }catch(Exception oe){
                            oe.printStackTrace();
                        }
                    }
                }catch(IOException ie){
                    ie.printStackTrace();
                }
			}
        }
		
		return false;
    }

	private boolean configChange(Context context, String dir, String file, String config){
        File confFile = new File((INTERNAL_DIR + "/" + context.getPackageName() + "/" + dir), file);
		FileOutputStream fos = null;

        makeConfigDir(context, dir);
		if(confFile.exists()) confFile.delete();

        try{
            fos = new FileOutputStream(confFile);
            byte[] buffer = config.getBytes("UTF-8");
            fos.write(buffer);
            fos.close();

			Log.d(TAG, "QSM configChange success");
			return true;
        }catch(Exception oe){
            oe.printStackTrace();
        }

		return false;
	}

	private boolean assetConfigChange(Context context, String dir, String file, String serverID, String serverAddress){
        if(DEVICE_INFO_T.TABLET_BUILD == 1)
        {
            InputStream is = null;
            AssetManager assetManager = context.getAssets();
			
            try{					
                is = assetManager.open((dir + "/" +file), AssetManager.ACCESS_BUFFER);
				BufferedReader reader = new BufferedReader(new InputStreamReader(is));

				String line = "";
				String address = "";
				String newConfig = "";
				int startIndex, endIndex;
				
				while((line=reader.readLine()) != null){
					if(line.contains(serverID)){
						startIndex = line.indexOf(ADDRESS_START_ATTR) + ADDRESS_START_ATTR.length();
						endIndex = line.indexOf(ADDRESS_END_ATTR, startIndex);
						address = line.substring(startIndex, endIndex);

					    line = line.replace(address, serverAddress);
						Log.d(TAG, "QSM assetConfigChange = " + serverAddress);
					}
					newConfig += (line + "\r\n");
				}
				
				is.close();
	            return configChange(context, dir, file, newConfig);
            }
			catch(IOException ie){
				ie.printStackTrace();
			}
        }
		return false;
    }
	
    /* PUBLIC */
    public boolean connect(){
        //cracker@yiwoosolution.co.kr
        if(DEVICE_INFO_T.TABLET_BUILD == 0)
        {
            try {
                mSender = new LocalSocket();
                LocalSocketAddress address = new LocalSocketAddress(LOCAL_SOCKET_NAME,
                        LocalSocketAddress.Namespace.FILESYSTEM);
    
                mSender.connect(address);
                if(mSender.isConnected()){
                    mOutputStream = mSender.getOutputStream();
                }else{
                    return false;
                }
            } catch (IOException ex) {
                Log.wtf("QSM IOEXCEPTION", ex);
                return false;
            }
        }
        Log.d(TAG, "connect : succuss");
        return true;
    }

    public boolean connectForReceive(){
        //cracker@yiwoosolution.co.kr
        if(DEVICE_INFO_T.TABLET_BUILD == 0)
        {
            try {
                mSender = new LocalSocket();
                LocalSocketAddress address = new LocalSocketAddress(LOCAL_SOCKET_NAME,
                        LocalSocketAddress.Namespace.FILESYSTEM);
    
                mSender.connect(address);
                if(mSender.isConnected()){
                    mOutputStream = mSender.getOutputStream();
                    mInputStream = mSender.getInputStream();
                }else{
                    return false;
                }
            } catch (IOException ex) {
                Log.wtf("QSM IOEXCEPTION", ex);
                return false;
            }
        }
        
        Log.d(TAG, "connectForReceive : succuss");
		return true;
    }

    public boolean connectForReceive(QSM2Report.OnReceiveCallBack callback){
        Log.d(TAG, "connectForReceive : succuss");
		mCallBack = callback;
		return true;
    }
	
    public int sendData(byte[] data)
    {
        //cracker@yiwoosolution.co.kr
        if(DEVICE_INFO_T.TABLET_BUILD == 0)
        {
            if(mSender == null || mOutputStream == null){
                Log.d(TAG, "QSM sendData failed. socket is null");
                return -1;
            }
            if(!mSender.isConnected()){
                Log.d(TAG, "QSM sendData - socket is not connected");
                if(!connect()){
                    Log.d(TAG, "QSM sendData - fail to connect");
                    return -1;
                }
            }
    
            try {
                mOutputStream.write(data);
                Log.d(TAG, "QSM : send data to QSM service - byte data size ="+data.length);
				return data.length;
            }catch (IOException ex) {
                Log.wtf("QSM IOEXCEPTION", ex);
    
                // skb qsm log service was restarted. so we have to reconnect
                if(!reconnectAndResend(data)){
                    Log.d(TAG, "QSM sendData - fail to retry");
                    return -1;
                }
            }
        }
		else
		{
            String strJson = new String(data);
            if(sendToserver(strJson) >= 0)
            {
                Log.d(TAG, "sendData length = " + data.length);
                return data.length;
            }
            else
            {
                Log.d(TAG, "sendData error");
                return -1;
            }
		}
		return -1;
    }

    public boolean isConnected(){
        return mSender.isConnected();
    }

    public void disconnect(){
	    //cracker@yiwoosolution.co.kr
        if(DEVICE_INFO_T.TABLET_BUILD == 0)
        {
            if(mSender == null || mOutputStream == null){
                Log.d(TAG, "QSM disconnect failed. socket is null");
                return;
            }
            Log.d(TAG, "QSM disconnected");
            try {
                mOutputStream.close();
                mInputStream.close();
                mSender.close();
                mOutputStream = null;
                mInputStream = null;
                mSender = null;
            }catch (IOException ex) {
                Log.wtf("QSM IOEXCEPTION", ex);
            }
        }
		else
		{
		    native_release();
		}
    }

    public InputStream getInputStream(){
        return mInputStream;
    }

    /* PRIVATE */
    private boolean reconnectAndResend(byte[] data){
        disconnect();
        if(!connect()){
            Log.d(TAG, "QSM sendData - faile to connect");
            return false;
        }
        // retry to send data
        try {
            mOutputStream.write(data);
        }catch (IOException e) {
            Log.wtf("QSM IOEXCEPTION", e);
            return false;
        }
        return true;
    }

    public static void readFromJniCallBack(String strData, int size)
    {
        byte[] data = new byte[10];
        if(mCallBack != null)
       	{
             Log.d(TAG, "QSM readFromJniCallBack: call callback function");

             try {
                 data = strData.getBytes("UTF-8");
             }catch(Exception e){
                 Log.e(TAG, "readFromJniCallBack : error="+e.toString());
             }
			 	
             byte action[] = new byte[10];
             for(int i=1; i<size;i++){
                 action[i-1] = data[i];
             }
             mCallBack.onReceiveData(data[0], action, size-1,null);
        }
    }

    private boolean changeQSMServer(String serverID, String serverAddress)
    {
      Log.d(TAG, "QSM changeQSMServer = " + serverID);

      if(serverAddress != null) return assetConfigChange(mAppContext, PACKAGE_CONFIG_DIR, SERVER_LIST_CONF, serverID, serverAddress);
	  else return assetConfigRead(mAppContext, PACKAGE_CONFIG_DIR, SERVER_LIST_CONF, true);
    }
	
    public boolean changeServer(int serverType)
    {
		Log.d(TAG, "QSM changeServer serverType=" + serverType);

	    if(serverType == SERVER_LIST_TYPE_STB)
	    {
			return changeQSMServer(QSM_SERVER_ID, null);
	    }
		else if(serverType == SERVER_LIST_TYPE_DEV)
		{
			return changeQSMServer(QSM_SERVER_ID, SERVER_LIST_STRING_DEV);
		}
		else if(serverType == SERVER_LIST_TYPE_PRD)
		{
			return changeQSMServer(QSM_SERVER_ID, null);
		}

		return false;
    }
	
}
