package com.skb.btv.smartrcuagent.message;

import java.util.HashMap;

import android.os.Parcel;
import android.os.Parcelable;

public class ResponsePairingConfirm implements Parcelable {
    public String PairingID;
    public String PairingSessionID;
    public String ResultCode;
    public String ResultPhrase;

    public static final Creator<ResponsePairingConfirm> CREATOR = new Creator<ResponsePairingConfirm>() {
        public ResponsePairingConfirm createFromParcel(Parcel in) {
            return new ResponsePairingConfirm(in);
        }

        public ResponsePairingConfirm[] newArray(int size) {
            return new ResponsePairingConfirm[size];
        }
    };

    public ResponsePairingConfirm() {
    }

    public ResponsePairingConfirm(HashMap<String, String> hm) {
        PairingID = hm.get("PairingID");
        PairingSessionID = hm.get("PairingSessionID");
        ResultCode = hm.get("ResultCode");
        ResultPhrase = hm.get("ResultPhrase");
    }

    private ResponsePairingConfirm(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel out, int arg1) {
        out.writeString(PairingID);
        out.writeString(PairingSessionID);
        out.writeString(ResultCode);
        out.writeString(ResultPhrase);
    }

    public void readFromParcel(Parcel in) {
        PairingID = in.readString();
        PairingSessionID = in.readString();
        ResultCode = in.readString();
        ResultPhrase = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "[ResPairingConfirm (" + " PairingID=" + PairingID + ", " + " PairingSessionID=" + PairingSessionID + ", "
                + "ResultCode=" + ResultCode + ", ResultPhrase=" + ResultPhrase + ")]";
    }
}
