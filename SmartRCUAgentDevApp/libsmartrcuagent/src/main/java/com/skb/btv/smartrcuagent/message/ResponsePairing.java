package com.skb.btv.smartrcuagent.message;

import java.util.HashMap;

import android.os.Parcel;
import android.os.Parcelable;

public class ResponsePairing implements Parcelable {
    public String AuthCode;
    public String AuthCodeExpireDuration;
    public String ResultCode;
    public String ResultPhrase;

    public static final Creator<ResponsePairing> CREATOR = new Creator<ResponsePairing>() {
        public ResponsePairing createFromParcel(Parcel in) {
            return new ResponsePairing(in);
        }

        public ResponsePairing[] newArray(int size) {
            return new ResponsePairing[size];
        }
    };

    public ResponsePairing() {

    }

    public ResponsePairing(HashMap<String, String> hm) {
        AuthCode = hm.get("AuthCode");
        AuthCodeExpireDuration = hm.get("AuthCodeExpireDuration");
        ResultCode = hm.get("ResultCode");
        ResultPhrase = hm.get("ResultPhrase");
    }

    private ResponsePairing(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel out, int arg1) {
        out.writeString(AuthCode);
        out.writeString(AuthCodeExpireDuration);
        out.writeString(ResultCode);
        out.writeString(ResultPhrase);
    }

    public void readFromParcel(Parcel in) {
        AuthCode = in.readString();
        AuthCodeExpireDuration = in.readString();
        ResultCode = in.readString();
        ResultPhrase = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "[ResponsePairing (" + "AuthCode=" + AuthCode + ", AuthCodeExpireDuration=" + AuthCodeExpireDuration
                + ", ResultCode=" + ResultCode + ", ResultPhrase=" + ResultPhrase + ")]";
    }

}
