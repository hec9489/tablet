package com.skb.btv.smartrcuagent.message;

import java.util.HashMap;

import android.os.Parcel;
import android.os.Parcelable;

public class PairingItem implements Parcelable {

    public String UserName;
    public String UserID;
    public String UserNumber;
    public String PairingID;
    public String RepresentFlag;
    public String ServiceType;
    public String PairingListDeviceID;
    public String PairingDeviceMac;

    public static final Creator<PairingItem> CREATOR = new Creator<PairingItem>() {
        public PairingItem createFromParcel(Parcel in) {
            return new PairingItem(in);
        }

        public PairingItem[] newArray(int size) {
            return new PairingItem[size];
        }
    };

    public PairingItem() {
    }

    public PairingItem(HashMap<String, String> hm) {
        UserName = hm.get("UserName");
        UserID = hm.get("UserID");
        UserNumber = hm.get("UserNumber");
        PairingID = hm.get("PairingID");
        RepresentFlag = hm.get("RepresentFlag");
        ServiceType = hm.get("ServiceType");
        PairingListDeviceID = hm.get("PairingListDeviceID");
        PairingDeviceMac = hm.get("PairingDeviceMac");
    }

    private PairingItem(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel out, int arg1) {
        out.writeString(UserName);
        out.writeString(UserID);
        out.writeString(UserNumber);
        out.writeString(PairingID);
        out.writeString(RepresentFlag);
        out.writeString(ServiceType);
        out.writeString(PairingListDeviceID);
        out.writeString(PairingDeviceMac);

    }

    public void readFromParcel(Parcel in) {
        UserName = in.readString();
        UserID = in.readString();
        UserNumber = in.readString();
        PairingID = in.readString();
        RepresentFlag = in.readString();
        ServiceType = in.readString();
        PairingListDeviceID = in.readString();
        PairingDeviceMac = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "[PairingItem (" +
                " UserName=" + UserName +
                ", UserID=" + UserID + ", UserNumber=" + UserNumber +
                ", PairingID=" + PairingID +
                ", RepresentFlag=" + RepresentFlag +
                ", ServiceType=" + ServiceType +
                ", PairingListDeviceID=" + PairingListDeviceID +
                ", PairingDeviceMac=" + PairingDeviceMac +
                ")]";
    }
}
