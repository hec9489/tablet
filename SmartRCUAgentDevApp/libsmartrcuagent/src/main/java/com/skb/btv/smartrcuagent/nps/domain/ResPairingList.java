package com.skb.btv.smartrcuagent.nps.domain;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResPairingList {

    public class Header {

        @SerializedName("if_no")
        String if_no;
        @SerializedName("ver")
        String ver;
        @SerializedName("response_format")
        String response_format;
        @SerializedName("result")
        String result;
        @SerializedName("reason")
        String reason;
        @SerializedName("sender")
        String sender;
        @SerializedName("receiver")
        String receiver;

        public String get_if_no() {
            return if_no;
        }

        public String get_ver() {
            return ver;
        }

        public String get_response_format() {
            return response_format;
        }

        public String get_result() {
            return result;
        }

        public String get_reason() {
            return reason;
        }

        public String get_sender() {
            return sender;
        }

        public String get_receiver() {
            return receiver;
        }

        @Override
        public String toString() {
            return "\"header\" : { " + "if_no = " + if_no + "," + "ver = " + ver + ","
                    + "response_format = " + response_format + "," + "result = " + result + ","
                    + "reason = " + reason + "," + "sender = " + sender + ","
                    + "receiver = " + receiver + " }";
        }
    }

    public class Body {
        @SerializedName("pairing_deviceid")
        String pairing_deviceid;
        @SerializedName("pairing_device_type")
        String pairing_device_type;
        @SerializedName("pairing_list")
        List<PairingItem> pairing_list = new ArrayList<PairingItem>();
        @SerializedName("custom_param")
        Map<String, String> custom_param = new HashMap<String, String>();

        public String get_pairing_deviceid() {
            return pairing_deviceid;
        }

        public String get_pairing_device_type() {
            return pairing_device_type;
        }

        public List<PairingItem> get_pairing_list() {
            return pairing_list;
        }

        public Map<String, String> get_custom_param() {
            return custom_param;
        }

        @Override
        public String toString() {
            return "\"body\" : { " + "pairing_deviceid = " + pairing_deviceid + ","
                    + "pairing_device_type = " + pairing_device_type + ","
                    + "pairing_list = " + pairing_list + ","
                    + "custom_param = " + custom_param + " }";
        }
    }

    public class PairingItem {
        @SerializedName("pairing_list_deviceid")
        String pairing_list_deviceid;
        @SerializedName("service_type")
        String service_type;
        @SerializedName("pairingid")
        String pairingid;
        @SerializedName("user_name")
        String user_name;
        @SerializedName("muser_num")
        String muser_num;
        @SerializedName("userid")
        String userid;
        @SerializedName("pairing_device_mac")
        String pairing_device_mac;
        @SerializedName("represent_flag")
        String represent_flag;

        public String get_pairing_list_deviceid() {
            return pairing_list_deviceid;
        }

        public String get_service_type() {
            return service_type;
        }

        public String get_pairingid() {
            return pairingid;
        }

        public String get_user_name() {
            return user_name;
        }

        public String get_muser_num() {
            return muser_num;
        }

        public String get_userid() {
            return userid;
        }

        public String get_pairing_device_mac() {
            return pairing_device_mac;
        }

        public String get_represent_flag() {
            return represent_flag;
        }

        @Override
        public String toString() {
            return "PairingItem [ userid=" + userid + ", user_name=" + user_name + ", muser_num=" + muser_num
                    + ", pairingid=" + pairingid + ", service_type=" + service_type
                    + ", pairing_list_deviceid=" + pairing_list_deviceid + ", represent_flag=" + represent_flag
                    + ", pairing_device_mac=" + pairing_device_mac + "]";
        }
    }

    @SerializedName("header")
    public Header header = new Header();
    @SerializedName("body")
    public Body body = new Body();

    @Override
    public String toString() {
        return "ResPairingList : {" + "\n" + header + "\n" + body + "\n" + "}";
    }
}
