package com.skb.btv.smartrcuagent;

import android.os.Handler.Callback;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;

import com.skb.btv.smartrcuagent.bridge.InputBridge;
import com.skb.btv.smartrcuagent.event.KeyEvent;
import com.skb.btv.smartrcuagent.event.MouseEvent;
import com.skb.btv.smartrcuagent.message.ResponseReceiveMessage;
import com.skb.btv.smartrcuagent.nps.NpsControlThread;
import com.skb.btv.smartrcuagent.util.ALog;

import java.util.HashMap;
import java.util.Iterator;

public class ResponseCallback implements Callback {
    public final static int RESPONSE_KEY_EVENT_RECEIVED = 1;
    public final static int RESPONSE_MOUSE_EVENT_RECEIVED = 2;
    public final static int RESPONSE_MOUSEWHEEL_EVENT_RECEIVED = 3;
    public final static int RESPONSE_NPS_STB_ADJUST_REQUEST = 4;

    private InputBridge inputBridge;
    private SmartRCUAgentListener smartRCUAgentListener;

    public ResponseCallback(InputBridge inputBridge) {
        this.inputBridge = inputBridge;
    }

    public ResponseCallback(SmartRCUAgentListener listener) {
        smartRCUAgentListener = listener;
    }

    public void setInputBridge(InputBridge inputBridge) {
        this.inputBridge = inputBridge;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean handleMessage(Message msg) {
        HashMap<String, String> paramMap = null;
        ALog.d("msg.what: " + msg.what);
        ALog.d("msg.obj: " + msg.obj);
        switch (msg.what) {
            case RESPONSE_KEY_EVENT_RECEIVED:
                ALog.d("RESPONSE_KEY_EVENT_RECEIVED");
                if (msg.obj instanceof KeyEvent) {
                    KeyEvent keyevent = (KeyEvent) msg.obj;
                    if (inputBridge != null) {
                        inputBridge.sendEvent(InputBridge.EV_KEY, keyevent.getCode(),
                                keyevent.getAction());
                    }
                }
                return true;

            case RESPONSE_MOUSE_EVENT_RECEIVED:
                ALog.d("RESPONSE_MOUSE_EVENT_RECEIVED");
                MouseEvent mouseEvent = (MouseEvent) msg.obj;
                if (inputBridge != null) {
                    inputBridge.sendMouseMovement(mouseEvent.getXDelta(),
                            mouseEvent.getYDelta());
                }
                return true;

            case RESPONSE_MOUSEWHEEL_EVENT_RECEIVED:
                ALog.d("RESPONSE_MOUSEWHEEL_EVENT_RECEIVED");
                break;

            case RESPONSE_NPS_STB_ADJUST_REQUEST:
                ALog.d("RESPONSE_NPS_STB_ADJUST_REQUEST");
                Log.d("Pumpkin", "RESPONSE_NPS_STB_ADJUST_REQUEST 1");

                if (msg.obj instanceof NpsObject) {
                    Log.d("Pumpkin", "RESPONSE_NPS_STB_ADJUST_REQUEST 2");
                    NpsObject npsObject = (NpsObject) msg.obj;
                    paramMap = npsObject.getMap();

                    ResponseReceiveMessage request = new ResponseReceiveMessage(paramMap);
                    try {
                        smartRCUAgentListener.onResponseReceiveMessage(request);
                        Log.d("Pumpkin", "RESPONSE_NPS_STB_ADJUST_REQUEST 3");
                    } catch (NullPointerException e) {
                        ALog.d("Catched Null Pointer Exception by listener by listner: " + smartRCUAgentListener);
                        e.printStackTrace();
                        HashMap<String, String> message = new HashMap<String, String>();
                        NpsControlThread.getInstance().sendRequestData("0", "Success", message);
                    } catch (Exception e) {
                        ALog.d("Catched general Exception by listener by listner: " + smartRCUAgentListener);
                        e.printStackTrace();
                        HashMap<String, String> message = new HashMap<String, String>();
                        NpsControlThread.getInstance().sendRequestData("0", "Success", message);
                    }
                }
                break;

            default:
                break;
        }
        return false;
    }
}
