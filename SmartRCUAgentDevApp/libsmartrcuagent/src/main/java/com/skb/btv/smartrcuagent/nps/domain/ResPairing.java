package com.skb.btv.smartrcuagent.nps.domain;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class ResPairing {

    public class Header {

        @SerializedName("if_no")
        String if_no;
        @SerializedName("ver")
        String ver;
        @SerializedName("response_format")
        String response_format;
        @SerializedName("result")
        String result;
        @SerializedName("reason")
        String reason;
        @SerializedName("sender")
        String sender;
        @SerializedName("receiver")
        String receiver;

        public String get_if_no() {
            return if_no;
        }

        public String get_ver() {
            return ver;
        }

        public String get_response_format() {
            return response_format;
        }

        public String get_result() {
            return result;
        }

        public String get_reason() {
            return reason;
        }

        public String get_sender() {
            return sender;
        }

        public String get_receiver() {
            return receiver;
        }

        @Override
        public String toString() {
            return "\"header\" : { " + "if_no = " + if_no + "," + "ver = " + ver + ","
                    + "response_format = " + response_format + "," + "result = " + result + ","
                    + "reason = " + reason + "," + "sender = " + sender + ","
                    + "receiver = " + receiver + " }";
        }
    }

    public class Body {
        @SerializedName("host_deviceid")
        String host_deviceid;
        @SerializedName("authcode")
        String authcode;
        @SerializedName("pairing_sessionid")
        String pairing_sessionid;
        @SerializedName("authcode_valid_time")
        String authcode_valid_time;
        @SerializedName("custom_param")
        Map<String, String> custom_param = new HashMap<String, String>();

        public String get_host_deviceid() {
            return host_deviceid;
        }

        public String get_authcode() {
            return authcode;
        }

        public String get_pairing_sessionid() {
            return pairing_sessionid;
        }

        public String get_authcode_valid_time() {
            return authcode_valid_time;
        }

        public Map<String, String> get_custom_param() {
            return custom_param;
        }

        @Override
        public String toString() {
            return "\"body\" : { " + "host_deviceid = " + host_deviceid + ","
                    + "authcode = " + authcode + "," + "pairing_sessionid = " + pairing_sessionid + ","
                    + "authcode_valid_time = " + authcode_valid_time + ","
                    + "custom_param = " + custom_param + " }";
        }
    }

    @SerializedName("header")
    public Header header = new Header();
    @SerializedName("body")
    public Body body = new Body();

    @Override
    public String toString() {
        return "ResPairing : {" + "\n" + header + "\n" + body + "\n" + "}";
    }

}
