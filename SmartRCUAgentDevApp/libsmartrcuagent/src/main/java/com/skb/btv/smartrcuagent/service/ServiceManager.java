package com.skb.btv.smartrcuagent.service;

import java.util.HashSet;

public class ServiceManager {

	private static ServiceManager mInstance;
	private static HashSet<String> registerServices = new HashSet<String>();

	private ServiceManager() {
	}

	public synchronized static ServiceManager getInstance() {
		if (mInstance == null) {
			mInstance = new ServiceManager();
		}
		return mInstance;
	}

	public boolean register(String name) {
		registerServices.add(name);
		return native_register(name);
	}

	public boolean unregister(String name) {
		registerServices.remove(name);
		return native_unregister(name);
	}

	public static boolean isRegistered(String name) {
		return registerServices.contains(name);
	}

	private native boolean native_register(String name);

	private native boolean native_unregister(String name);

}
