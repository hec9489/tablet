package com.skb.btv.smartrcuagent;

public class DomainStatus {
	public static final String DOMAIN_STATUS_SUCCESS = "success";
	public static final String DOMAIN_STATUS_ERROR = "error";
	public static final String DOMAIN_STATUS_RECOVER = "repair";

	private static String currentStatus = DOMAIN_STATUS_SUCCESS;

	public static void setCurrentDomainStatus(String status) {
		currentStatus = status;
	}

	public static String getCurrentDomainStatus() {
		return currentStatus;
	}

}
