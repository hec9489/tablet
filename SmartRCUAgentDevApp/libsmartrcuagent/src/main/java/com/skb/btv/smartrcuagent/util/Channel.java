package com.skb.btv.smartrcuagent.util;

public class Channel {
    private String sid;
    private String ch;
    private String cname;

    public Channel() {
        super();
    }

    public Channel(String sid, String ch, String cname) {
        super();
        this.sid = sid;
        this.ch = ch;
        this.cname = cname;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getCh() {
        return ch;
    }

    public void setCh(String ch) {
        this.ch = ch;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }
}
