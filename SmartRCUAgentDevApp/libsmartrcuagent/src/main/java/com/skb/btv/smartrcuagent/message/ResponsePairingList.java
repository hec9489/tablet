package com.skb.btv.smartrcuagent.message;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Parcel;
import android.os.Parcelable;

public class ResponsePairingList implements Parcelable {

    public ArrayList<PairingItem> PairingItems = new ArrayList<PairingItem>();
    public String ResultCode;
    public String ResultPhrase;

    public static final Creator<ResponsePairingList> CREATOR = new Creator<ResponsePairingList>() {
        public ResponsePairingList createFromParcel(Parcel in) {
            return new ResponsePairingList(in);
        }

        public ResponsePairingList[] newArray(int size) {
            return new ResponsePairingList[size];
        }
    };

    public ResponsePairingList() {
    }

    public ResponsePairingList(HashMap<String, String> hm) {
        ResultCode = hm.get("ResultCode");
        ResultPhrase = hm.get("ResultPhrase");

        int i = 0;
        while (true) {
            if (hm.containsKey("UserName_" + i)) {
                PairingItem pairingItem = new PairingItem();
                pairingItem.UserName = hm.get("UserName_" + i);
                pairingItem.UserID = hm.get("UserID_" + i);
                pairingItem.UserNumber = hm.get("UserNumber_" + i);
                pairingItem.PairingID = hm.get("PairingID_" + i);
                pairingItem.RepresentFlag = hm.get("RepresentFlag_" + i);
                pairingItem.ServiceType = hm.get("ServiceType_" + i);
                pairingItem.PairingListDeviceID = hm.get("PairingListDeviceID_" + i);
                pairingItem.PairingDeviceMac = hm.get("PairingDeviceMac_" + i);
                PairingItems.add(pairingItem);
            } else {
                break;
            }
            i++;
        }

    }

    private ResponsePairingList(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel out, int arg1) {
        out.writeInt(PairingItems.size());
        out.writeTypedList(PairingItems);
        out.writeString(ResultCode);
        out.writeString(ResultPhrase);

    }

    public void readFromParcel(Parcel in) {
        in.readInt();
        in.readTypedList(PairingItems, PairingItem.CREATOR);
        ResultCode = in.readString();
        ResultPhrase = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("[ResponsePairingList (ResultCode=" + ResultCode + ", ResultPhrase=" + ResultPhrase + ", ");
        if (PairingItems.size() > 0) {
            sb.append(PairingItems.toString() + ", ");
        }
        sb.append(")]");
        return sb.toString();
    }
}
