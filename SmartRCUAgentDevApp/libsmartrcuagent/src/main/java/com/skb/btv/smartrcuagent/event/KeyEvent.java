package com.skb.btv.smartrcuagent.event;

import com.google.gson.Gson;

public class KeyEvent extends Event {
    private int code;
    private int action;

    public KeyEvent(int code, int action) {
        this.type = Type.KeyEvent;
        this.code = code;
        this.action = action;
    }

    public int getCode() {
        return code;
    }

    public int getAction() {
        return action;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
