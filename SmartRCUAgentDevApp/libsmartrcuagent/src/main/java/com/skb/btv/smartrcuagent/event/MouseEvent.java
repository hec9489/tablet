package com.skb.btv.smartrcuagent.event;

public class MouseEvent extends Event {
    private int xDelta;
    private int yDelta;

    public MouseEvent(int xDelta, int yDelta) {
        this.type = Type.MouseEvent;
        this.xDelta = xDelta;
        this.yDelta = yDelta;
    }

    public int getXDelta() {
        return xDelta;
    }

    public int getYDelta() {
        return yDelta;
    }

}
