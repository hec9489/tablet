package com.skb.btv.smartrcuagent.nps.domain;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class ResReceiveMessage {

    public class Header {

        @SerializedName("if_no")
        String if_no;
        @SerializedName("ver")
        String ver;
        @SerializedName("response_format")
        String response_format;
        @SerializedName("result")
        String result;
        @SerializedName("reason")
        String reason;
        @SerializedName("sender")
        String sender;
        @SerializedName("receiver")
        String receiver;

        public String get_if_no() {
            return if_no;
        }

        public String get_ver() {
            return ver;
        }

        public String get_response_format() {
            return response_format;
        }

        public String get_result() {
            return result;
        }

        public String get_reason() {
            return reason;
        }

        public String get_sender() {
            return sender;
        }

        public String get_receiver() {
            return receiver;
        }

        @Override
        public String toString() {
            return "\"header\" : { " + "if_no = " + if_no + "," + "ver = " + ver + ","
                    + "response_format = " + response_format + "," + "result = " + result + ","
                    + "reason = " + reason + "," + "sender = " + sender + ","
                    + "receiver = " + receiver + " }";
        }
    }

    public class Body {
        @SerializedName("send_deviceid")
        String send_deviceid;
        @SerializedName("receive_deviceid")
        String receive_deviceid;
        @SerializedName("pairingid")
        String pairingid;
        @SerializedName("service_type")
        String service_type;
        @SerializedName("message")
        Map<String, String> message = new HashMap<String, String>();
        @SerializedName("abort")
        String abort;
        @SerializedName("ip")
        String ip;
        @SerializedName("port")
        String port;
        @SerializedName("custom_param")
        Map<String, String> custom_param = new HashMap<String, String>();

        public String get_send_deviceid() {
            return send_deviceid;
        }

        public String get_receive_deviceid() {
            return receive_deviceid;
        }

        public String get_pairingid() {
            return pairingid;
        }

        public String get_service_type() {
            return service_type;
        }

        public Map<String, String> get_message() {
            return message;
        }

        public String get_abort() {
            return abort;
        }

        public String get_ip() {
            return ip;
        }

        public String get_port() {
            return port;
        }

        public Map<String, String> get_custom_param() {
            return custom_param;
        }

        @Override
        public String toString() {
            return "\"body\" : { " + "send_deviceid = " + send_deviceid + ","
                    + "receive_deviceid = " + receive_deviceid + "," + "pairingid = " + pairingid + ","
                    + "service_type = " + service_type + "," + "message = " + message + "," + "abort = " + abort + ","
                    + "ip = " + ip + "," + "port = " + port + "," + "custom_param = " + custom_param + " }";
        }
    }

    @SerializedName("header")
    public Header header = new Header();
    @SerializedName("body")
    public Body body = new Body();

    @Override
    public String toString() {
        return "ResReceiveMessage : {" + "\n" + header + "\n" + body + "\n" + "}";
    }
}
