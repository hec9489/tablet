package com.skb.btv.smartrcuagent.nps;

import com.skb.btv.smartrcuagent.util.ALog;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;

public class NpsAddressReader {
    private static final String NPS_ADDRESS_FILE_PATH = "/data/btv_home/config/server-list.conf";
    public static final String DEFAULT_NPS_ADDRESS = "nps.hanafostv.com";
    public static final int DEFAULT_NPS_PORT = 9090;
    private static NpsAddressReader instance;
    private String npsAddress;
    private Integer npsPort;

    private NpsAddressReader() {
        initRealNpsAddressFromFile();
    }

    public synchronized static NpsAddressReader getInstance() {
        if (instance == null) {
            instance = new NpsAddressReader();
        }

        return instance;
    }

    public int getNpsPort() {
        return npsPort;
    }

    public String getNpsAddress() {
        return npsAddress;
    }

    public void setNpsAddress(String npsAddress) {
        this.npsAddress = npsAddress;
    }

    public void setNpsPort(int port) {
        this.npsPort = port;
    }

    public void refresh() {
        initRealNpsAddressFromFile();
    }

    private Reader getSourceReader() throws FileNotFoundException {
        Reader reader = null;
        File file = new File(NPS_ADDRESS_FILE_PATH);
        reader = new FileReader(file);
        return reader;
    }

    private void initRealNpsAddressFromFile() {
        try {
            XmlPullParser xpp = XmlPullParserFactory.newInstance()
                    .newPullParser();

            xpp.setInput(getSourceReader());

            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    if (xpp.getName().equals("server")) {
                        int attrCount = xpp.getAttributeCount();
                        String id = "";
                        String address = "";
                        int port = 9090;
                        for (int i = 0; i < attrCount; i++) {
                            String attrName = xpp.getAttributeName(i);
                            if ("id".equals(attrName)) {
                                id = xpp.getAttributeValue(i);
                            } else if ("address".equals(attrName)) {
                                address = xpp.getAttributeValue(i);
                            } else if ("port".equals(attrName)) {
                                port = Integer.parseInt(xpp
                                        .getAttributeValue(i));
                            }
                        }
                        if ("nps".equals(id)) {
                            npsAddress = address;
                            ALog.d("server-list.conf >> npsAddr: " + npsAddress);
                            npsPort = port;
                            ALog.d("server-list.conf >> npsPort: " + npsPort);
                            break;
                        }

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                }
                xpp.next();
                eventType = xpp.getEventType();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
            setDefaultAddressAndPort();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            setDefaultAddressAndPort();
        } catch (Exception e) {
            e.printStackTrace();
            setDefaultAddressAndPort();
        }
    }

    private void setDefaultAddressAndPort() {
        npsAddress = DEFAULT_NPS_ADDRESS;
        npsPort = DEFAULT_NPS_PORT;
    }
}
