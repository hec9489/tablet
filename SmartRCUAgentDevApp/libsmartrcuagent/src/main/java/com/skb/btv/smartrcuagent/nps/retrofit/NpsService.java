package com.skb.btv.smartrcuagent.nps.retrofit;


import com.skb.btv.smartrcuagent.nps.domain.ResAgreement;
import com.skb.btv.smartrcuagent.nps.domain.ResPairing;
import com.skb.btv.smartrcuagent.nps.domain.ResPairingConfirm;
import com.skb.btv.smartrcuagent.nps.domain.ResPairingList;
import com.skb.btv.smartrcuagent.nps.domain.ResPairingSequence;
import com.skb.btv.smartrcuagent.nps.domain.ResPairingStatus;
import com.skb.btv.smartrcuagent.nps.domain.ResReceiveMessage;
import com.skb.btv.smartrcuagent.nps.domain.ResRepresent;
import com.skb.btv.smartrcuagent.nps.domain.ResSetRepresent;
import com.skb.btv.smartrcuagent.nps.domain.ResUnPairing;
import com.skb.btv.smartrcuagent.nps.domain.ResUpdateAgreement;
import com.skb.btv.smartrcuagent.nps.domain.ResUpdateHostDeviceInfo;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface NpsService {

    @POST("/nps/v5/reqPairing")
    Call<ResPairing> reqPairing(@Body Object object);

    @POST("/nps/v5/reqPairingSequence")
    Call<ResPairingSequence> reqPairingSequence(@Body Object object);

    @POST("/nps/v5/reqPairingConfirm")
    Call<ResPairingConfirm> reqPairingConfirm(@Body Object object);

    @POST("/nps/v5/reqUpdateHostDeviceInfo")
    Call<ResUpdateHostDeviceInfo> reqUpdateHostDeviceInfo(@Body Object object);

    @POST("/nps/v5/reqReceiveMessage")
    Call<ResReceiveMessage> reqReceiveMessage(@Body Object object);

    @POST("/nps/v5/reqPairingList")
    Call<ResPairingList> reqPairingList(@Body Object object);

    @POST("/nps/v5/reqPairingStatus")
    Call<ResPairingStatus> reqPairingStatus(@Body Object object);

    @POST("/nps/v5/reqUnPairing")
    Call<ResUnPairing> reqUnPairing(@Body Object object);

    @POST("/nps/v5/reqUpdateAgreement")
    Call<ResUpdateAgreement> reqUpdateAgreement(@Body Object object);

    @POST("/nps/v5/reqAgreement")
    Call<ResAgreement> reqAgreement(@Body Object object);

    @POST("/nps/v5/reqSetRepresent")
    Call<ResSetRepresent> reqSetRepresent(@Body Object object);

    @POST("/nps/v5/reqRepresent")
    Call<ResRepresent> reqRepresent(@Body Object object);

    @Headers("Content-Type: application/json")
    @POST("/nps/v5/{apiname}")
    Call<ResponseBody> reqApiName(@Path("apiname") String apiName, @Body String object);

    @Headers("accept: application/json; charset=UTF-8, Content-Type: application/json; charset=UTF-8")
    @POST()
    Call<ResponseBody> reqUrl(@Url String url, @Body String object);

    //@Headers("accept: application/json; charset=UTF-8", "Content-Type: application/json; charset=UTF-8")
    @Headers("Content-Type: application/json; charset=UTF-8")
    @POST()
    Call<ResponseBody> reqUrlRequest(@Url String url, @Body RequestBody object);

}
