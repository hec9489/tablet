package com.skb.btv.smartrcuagent;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import com.skb.btv.domain.SmartRcuAgentEnvironment;
import com.skb.btv.smartrcuagent.message.ResponseAgreement;
import com.skb.btv.smartrcuagent.message.ResponsePairing;
import com.skb.btv.smartrcuagent.message.ResponsePairingConfirm;
import com.skb.btv.smartrcuagent.message.ResponsePairingList;
import com.skb.btv.smartrcuagent.message.ResponsePairingSequence;
import com.skb.btv.smartrcuagent.message.ResponseRepresent;
import com.skb.btv.smartrcuagent.message.ResponseSetRepresent;
import com.skb.btv.smartrcuagent.message.ResponseUnPairing;
import com.skb.btv.smartrcuagent.message.ResponseUpdateAgreement;
import com.skb.btv.smartrcuagent.message.ResponseUpdateHostDeviceInfo;
import com.skb.btv.smartrcuagent.nps.NpsAddressReader;
import com.skb.btv.smartrcuagent.nps.NpsControlThread;
import com.skb.btv.smartrcuagent.nps.domain.ResAgreement;
import com.skb.btv.smartrcuagent.nps.domain.ResPairing;
import com.skb.btv.smartrcuagent.nps.domain.ResPairingConfirm;
import com.skb.btv.smartrcuagent.nps.domain.ResPairingList;
import com.skb.btv.smartrcuagent.nps.domain.ResPairingSequence;
import com.skb.btv.smartrcuagent.nps.domain.ResPairingStatus;
import com.skb.btv.smartrcuagent.nps.domain.ResRepresent;
import com.skb.btv.smartrcuagent.nps.domain.ResSetRepresent;
import com.skb.btv.smartrcuagent.nps.domain.ResUnPairing;
import com.skb.btv.smartrcuagent.nps.domain.ResUpdateAgreement;
import com.skb.btv.smartrcuagent.nps.domain.ResUpdateHostDeviceInfo;
import com.skb.btv.smartrcuagent.nps.domain.SKBRemoconSvcBuilder;
import com.skb.btv.smartrcuagent.nps.retrofit.NpsService;
import com.skb.btv.smartrcuagent.nps.retrofit.NpsServiceBuilder;
import com.skb.btv.smartrcuagent.util.AES256Util;
import com.skb.btv.smartrcuagent.util.ALog;
import com.skb.btv.smartrcuagent.util.DnsIpReader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by parkjeongho on 2021-05-25 오후 3:51
 */
public class SmartRCUAgentServiceImpl {
    private String pairingSessionId;
    private long authCodeExpireDuration = 65 * 1000;

    public static Handler mResponseHandler;
    private ResponseCallback responseCallback;

    private static SmartRCUAgentServiceImpl instance;
    private SmartRCUAgentListener smartRCUAgentListener;

    public synchronized static SmartRCUAgentServiceImpl getInstance() {
        if (instance == null) {
            instance = new SmartRCUAgentServiceImpl();
        }
        return instance;
    }

    private SmartRCUAgentServiceImpl() {
        if(!NpsControlThread.getInstance().isRunning()) {
            NpsControlThread.getInstance().npsStart();
        }
    }

    private void initResponseHandler() {
        if (mResponseHandler == null) {
            HandlerThread responseHandlerThread = new HandlerThread("Response looper");
            responseHandlerThread.start();
            responseCallback = new ResponseCallback(smartRCUAgentListener);
            mResponseHandler = new Handler(responseHandlerThread.getLooper(), responseCallback);
        }
    }

    public void setSmartRCUAgentListener(SmartRCUAgentListener listener) {
        smartRCUAgentListener = listener;
        initResponseHandler();
    }

    private void refreshNpsAddress() {
        try {
            String npsAddress = NpsAddressReader.getInstance().getNpsAddress();
            int npsPort = NpsAddressReader.getInstance().getNpsPort();
            String realNpsIpFromDns = npsAddress;
            try {
                realNpsIpFromDns = DnsIpReader.getInstance().getRealIpFromDns(npsAddress);
                NpsAddressReader.getInstance().setNpsAddress(realNpsIpFromDns);

            } catch (UnknownHostException e1) {
                ALog.printStackTrace(e1);
                DomainStatus.setCurrentDomainStatus(DomainStatus.DOMAIN_STATUS_ERROR);
                DomainQueryRetryThread.retryStart(3);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopService() {
        if(NpsControlThread.getInstance().isRunning()) {
            NpsControlThread.getInstance().npsStop();
        }
    }

    public ResponsePairing doReqPairing() {
        ALog.d("doReqPairing()");

        // NpsService npsService =
        // NpsServiceBuilder.buildNpsServiceWithIpHost("1.255.86.154", 8080);
        NpsService npsService = NpsServiceBuilder.buildNpsServiceWithIpHost(
                NpsAddressReader.getInstance().getNpsAddress(), NpsAddressReader.getInstance().getNpsPort());


        ResPairing response = null;
        Call<ResPairing> call = npsService.reqPairing(SKBRemoconSvcBuilder
                .buildReqPairing(SmartRcuAgentEnvironment.getBySenderName(), SmartRcuAgentEnvironment.stbId, null));
        try {
            response = call.execute().body();

            if (response != null) {
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("AuthCode", response.body.get_authcode());
                hm.put("AuthCodeExpireDuration", response.body.get_authcode_valid_time());
                hm.put("ResultCode", response.header.get_result());
                hm.put("ResultPhrase", response.header.get_reason());
                pairingSessionId = response.body.get_pairing_sessionid();
                try {
                    authCodeExpireDuration = Long.parseLong(response.body.get_authcode_valid_time());
                } catch (NumberFormatException e) {
                    authCodeExpireDuration = 65 * 1000;
                }
                return new ResponsePairing(hm);
            }
        } catch (IOException e1) {
            ALog.printStackTrace(e1);
            refreshNpsAddress();
        }

        return new ResponsePairing();
    }

    public void doReqPairingSequence() {

        NpsService npsService = NpsServiceBuilder.buildNpsServiceWithEndPointAndTimeout(
                NpsAddressReader.getInstance().getNpsAddress(), NpsAddressReader.getInstance().getNpsPort(),
                authCodeExpireDuration + 5, TimeUnit.SECONDS);

        Call<ResPairingSequence> call = npsService.reqPairingSequence(SKBRemoconSvcBuilder.buildReqPairingSequence(
                SmartRcuAgentEnvironment.getBySenderName(), SmartRcuAgentEnvironment.stbId, pairingSessionId, null));
        call.enqueue(new Callback<ResPairingSequence>() {

            @Override
            public void onFailure(Call<ResPairingSequence> arg0, Throwable arg1) {
                // TODO Auto-generated method stub
                ALog.d("onFailure ResPairingSequence: " + arg0.toString() + arg1.getMessage());
            }

            @Override
            public void onResponse(Call<ResPairingSequence> arg0, Response<ResPairingSequence> arg1) {
                ResPairingSequence response = arg1.body();
                ALog.d("onResponse ResPairingSequence : " + arg1.raw().code());
                ResponsePairingSequence responsePairingSequence = null;
                if (response != null) {
                    HashMap<String, String> hm = new HashMap<String, String>();
                    hm.put("ResultCode", response.header.get_result());
                    hm.put("ResultPhrase", response.header.get_reason());
                    hm.put("UserName", response.body.get_user_name());
                    hm.put("UserID", response.body.get_user_id());
                    hm.put("UserNumber", response.body.get_muser_num());
                    hm.put("ServiceType", response.body.get_service_type());

                    responsePairingSequence = new ResponsePairingSequence(hm);
                } else {
                    responsePairingSequence = new ResponsePairingSequence();
                }

                smartRCUAgentListener.onResponsePairingSequence(responsePairingSequence);
            }
        });
    }

    public ResponsePairingConfirm doReqPairingConfirm(String PairingProcess, String RestrictedAge,
                                                      String AdultSafetyMode) {

        NpsService npsService = NpsServiceBuilder.buildNpsServiceWithIpHost(
                NpsAddressReader.getInstance().getNpsAddress(), NpsAddressReader.getInstance().getNpsPort());


        HashMap<String, String> deviceinfo = new HashMap<String, String>();
        deviceinfo.put("restricted_age", RestrictedAge);
        deviceinfo.put("adult_safety_mode", AdultSafetyMode);
        deviceinfo.put("stb_src_agent_version", SmartRcuAgentEnvironment.srcAgentVersion);
        deviceinfo.put("stb_patch_version", SmartRcuAgentEnvironment.stbPatchVersion);
        //Encrypt
        String stbMacAddr = AES256Util.encryptAES256(SmartRcuAgentEnvironment.stbMacAddr);
        deviceinfo.put("stb_mac_address", stbMacAddr);
        deviceinfo.put("stb_ui_version", SmartRcuAgentEnvironment.stbUiVesrion);
        deviceinfo.put("kidsbooks_app_version", SmartRcuAgentEnvironment.kidsBookVersion);

        ResPairingConfirm response = null;
        Call<ResPairingConfirm> call = npsService.reqPairingConfirm(
                SKBRemoconSvcBuilder.buildReqPairingConfirm(SmartRcuAgentEnvironment.getBySenderName(),
                        SmartRcuAgentEnvironment.stbId, pairingSessionId, PairingProcess, deviceinfo, null));
        try {
            response = call.execute().body();

            if (response != null) {
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("ResultCode", response.header.get_result());
                hm.put("ResultPhrase", response.header.get_reason());
                hm.put("PairingSessionID", response.body.get_pairing_sessionid());
                hm.put("PairingID", response.body.get_pairingid());
                //hm.put("DeviceInfo", response.body.get_host_deviceinfo());

                // ALog.d("MAC Decrypt : " +
                // AES256Util.decryptAES256(response.body.get_host_deviceid()));
                return new ResponsePairingConfirm(hm);
            }
        } catch (IOException e1) {
            ALog.printStackTrace(e1);
            refreshNpsAddress();
        }
        return new ResponsePairingConfirm();
    }

    public ResponseUpdateHostDeviceInfo doReqUpdateHostDeviceInfo(String RestrictedAge, String AdultSafetyMode) {

//		String responseString = "";
//		String requestString = "{\"body\":{\"host_deviceid\":\"{3D0FB86E-73D5-11EA-B1D9-BF69B691B61A}\",\"host_deviceinfo\":{\"stb_mac_address\":\"IfDhwjue+fIH/liLXSnCRKadRL91g9KHPXzZKUQDFn0=\",\"kidsbooks_app_version\":\"\",\"restricted_age\":\"19\",\"stb_src_agent_version\":\"3.5.10\",\"adult_safety_mode\":\"1|0\",\"stb_patch_version\":\"15.522.46\",\"stb_ui_version\":\"1.9.0\"}},\"header\":{\"if_no\":\"IF-NPS-504\",\"receiver\":\"NPS\",\"response_format\":\"json\",\"sender\":\"STB\",\"sender_name\":\"1.9.0,15.522.46,3.5.10\",\"ver\":\"5.0\"}}";
//		String requestUrl = "/nps/v5/reqUpdateHostDeviceInfo";
//		if(true){
//			try {
//				responseString = doReqUserApi(requestUrl, requestString);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//			ALog.e("doReqUpdateHostDeviceInfo: responseString : " + responseString );
//			return null;
//		}


        NpsService npsService = NpsServiceBuilder.buildNpsServiceWithIpHost(
                NpsAddressReader.getInstance().getNpsAddress(), NpsAddressReader.getInstance().getNpsPort());


        HashMap<String, String> deviceinfo = new HashMap<String, String>();
        deviceinfo.put("restricted_age", RestrictedAge);
        deviceinfo.put("adult_safety_mode", AdultSafetyMode);
        deviceinfo.put("stb_src_agent_version", SmartRcuAgentEnvironment.srcAgentVersion);
        deviceinfo.put("stb_patch_version", SmartRcuAgentEnvironment.stbPatchVersion);
        //Encrypt
        String stbMacAddr = AES256Util.encryptAES256(SmartRcuAgentEnvironment.stbMacAddr);
        deviceinfo.put("stb_mac_address", stbMacAddr);
        deviceinfo.put("stb_ui_version", SmartRcuAgentEnvironment.stbUiVesrion);
        deviceinfo.put("kidsbooks_app_version", SmartRcuAgentEnvironment.kidsBookVersion);

        ResUpdateHostDeviceInfo response = null;
        Call<ResUpdateHostDeviceInfo> call = npsService.reqUpdateHostDeviceInfo(
                SKBRemoconSvcBuilder.buildReqUpdateHostDeviceInfo(SmartRcuAgentEnvironment.getBySenderName(),
                        SmartRcuAgentEnvironment.stbId, deviceinfo, null));
        try {
            response = call.execute().body();

            if (response != null) {
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("ResultCode", response.header.get_result());
                hm.put("ResultPhrase", response.header.get_reason());
                return new ResponseUpdateHostDeviceInfo(hm);
            }
        } catch (IOException e1) {
            ALog.printStackTrace(e1);
            refreshNpsAddress();
        }
        return new ResponseUpdateHostDeviceInfo();
    }

    public ResponsePairingList doReqPairingList() {

        NpsService npsService = NpsServiceBuilder.buildNpsServiceWithIpHost(
                NpsAddressReader.getInstance().getNpsAddress(), NpsAddressReader.getInstance().getNpsPort());

        ResPairingList response = null;
        Call<ResPairingList> call = npsService.reqPairingList(SKBRemoconSvcBuilder.buildReqPairingList(
                SmartRcuAgentEnvironment.getBySenderName(), SmartRcuAgentEnvironment.stbId, "H", "ALL", null));
        try {
            response = call.execute().body();

            if (response != null) {
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("ResultCode", response.header.get_result());
                hm.put("ResultPhrase", response.header.get_reason());

                if (response.body.get_pairing_list() != null) {
                    int count = response.body.get_pairing_list().size();
                    if (count == 0) {
                        ALog.e("Not exist pairing list count = 0 npsStop");
                        if(NpsControlThread.getInstance().isRunning()){
                            NpsControlThread.getInstance().npsStop();
                        }
                        SmartRcuAgentEnvironment.isPaired = false;
                    } else {
                        SmartRcuAgentEnvironment.isPaired = true;
                    }
                    for (int i = 0; i < count; i++) {
                        ResPairingList.PairingItem item = response.body.get_pairing_list().get(i);
                        hm.put("UserName_" + i, item.get_user_name());
                        hm.put("UserID_" + i, item.get_userid());
                        hm.put("UserNumber_" + i, item.get_muser_num());
                        hm.put("PairingID_" + i, item.get_pairingid());
                        hm.put("RepresentFlag_" + i, item.get_represent_flag());
                        hm.put("ServiceType_" + i, item.get_service_type());
                        hm.put("PairingListDeviceID_" + i, item.get_pairing_list_deviceid());
                        hm.put("PairingDeviceMac_" + i, item.get_pairing_device_mac());
                    }
                }else{
                    if("1018".equals(response.header.get_result())){
                        ALog.e("Not exist pairing list npsStop");
                        if(NpsControlThread.getInstance().isRunning()){
                            NpsControlThread.getInstance().npsStop();
                        }
                        SmartRcuAgentEnvironment.isPaired = false;
                    }
                }

                return new ResponsePairingList(hm);
            }
        } catch (IOException e1) {
            ALog.printStackTrace(e1);
            refreshNpsAddress();
        }
        return new ResponsePairingList();
    }

    public void doReqPairingStatus() {
        ALog.d("doReqPairingStatus: start " );
        boolean isPaired = false;
        try {
            //isReqPairingStatus();
            isPaired = isReqPairingStatusNps();
        } catch (IOException e) {
            ALog.printStackTrace(e);
            refreshNpsAddress();
        }

        ALog.d("doReqPairingStatus: end isPaired : " + isPaired );
    }

    public ResponseUnPairing doReqUnPairing(String PairingID, String ServiceType) {

        NpsService npsService = NpsServiceBuilder.buildNpsServiceWithIpHost(
                NpsAddressReader.getInstance().getNpsAddress(), NpsAddressReader.getInstance().getNpsPort());

        ResUnPairing response = null;
        Call<ResUnPairing> call = npsService
                .reqUnPairing(SKBRemoconSvcBuilder.buildReqUnPairing(SmartRcuAgentEnvironment.getBySenderName(),
                        SmartRcuAgentEnvironment.stbId, "H", PairingID, ServiceType, null));
        try {
            response = call.execute().body();

            if (response != null) {
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("ResultCode", response.header.get_result());
                hm.put("ResultPhrase", response.header.get_reason());
                hm.put("PairingID", response.body.get_pairingid());
                hm.put("ServiceType", response.body.get_service_type());

                return new ResponseUnPairing(hm);
            }
        } catch (IOException e1) {
            ALog.printStackTrace(e1);
            refreshNpsAddress();
        }
        return new ResponseUnPairing();
    }

    public ResponseAgreement doReqAgreement(String PairingID, String ServiceType) {

        NpsService npsService = NpsServiceBuilder.buildNpsServiceWithIpHost(
                NpsAddressReader.getInstance().getNpsAddress(), NpsAddressReader.getInstance().getNpsPort());

        ResAgreement response = null;
        Call<ResAgreement> call = npsService
                .reqAgreement(SKBRemoconSvcBuilder.buildReqAgreement(SmartRcuAgentEnvironment.getBySenderName(),
                        SmartRcuAgentEnvironment.stbId, "H", PairingID, ServiceType, null));
        try {
            response = call.execute().body();

            if (response != null) {
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("PairingID", response.body.get_pairingid());
                hm.put("ServiceType", response.body.get_service_type());
                if (response.body.get_agreement() != null) {
                    hm.put("PurchaseList", response.body.get_agreement().get("purchase_list"));
                    hm.put("UnlimitedVod", response.body.get_agreement().get("unlimited_vod"));
                } else {
                    hm.put("PurchaseList", "");
                    hm.put("UnlimitedVod", "");
                }
                hm.put("ResultCode", response.header.get_result());
                hm.put("ResultPhrase", response.header.get_reason());
                return new ResponseAgreement(hm);
            }
        } catch (IOException e1) {
            ALog.printStackTrace(e1);
            refreshNpsAddress();
        }
        return new ResponseAgreement();
    }

    public ResponseSetRepresent doReqSetRepresent(String PairingID, String ServiceType, String RepresentFlag) {
        NpsService npsService = NpsServiceBuilder.buildNpsServiceWithIpHost(
                NpsAddressReader.getInstance().getNpsAddress(), NpsAddressReader.getInstance().getNpsPort());


        ALog.d("doReqSetRepresent() PairingID:"+PairingID + " ServiceType:"+ServiceType+" RepresentFlag:" +RepresentFlag );

        ResSetRepresent response = null;
        Call<ResSetRepresent> call = npsService
                .reqSetRepresent(SKBRemoconSvcBuilder.buildReqSetRepresent(SmartRcuAgentEnvironment.getBySenderName(),
                        SmartRcuAgentEnvironment.stbId, "H", PairingID, ServiceType, null, RepresentFlag));
        try {
            response = call.execute().body();

            if (response != null) {
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("PairingID", response.body.get_pairingid());
                hm.put("ServiceType", response.body.get_service_type());
                hm.put("RepresentFlag", response.body.get_represent_flag());
                hm.put("ResultCode", response.header.get_result());
                hm.put("ResultPhrase", response.header.get_reason());
                return new ResponseSetRepresent(hm);
            }
        } catch (IOException e1) {
            ALog.printStackTrace(e1);
            refreshNpsAddress();
        }
        return new ResponseSetRepresent();
    }

    public ResponseRepresent doReqRepresent(String PairingID, String ServiceType) {
        NpsService npsService = NpsServiceBuilder.buildNpsServiceWithIpHost(
                NpsAddressReader.getInstance().getNpsAddress(), NpsAddressReader.getInstance().getNpsPort());

        ResRepresent response = null;
        Call<ResRepresent> call = npsService
                .reqRepresent(SKBRemoconSvcBuilder.buildReqRepresent(SmartRcuAgentEnvironment.getBySenderName(),
                        SmartRcuAgentEnvironment.stbId, "H", PairingID, ServiceType, null, ""));
        try {
            response = call.execute().body();

            if (response != null) {
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("PairingID", response.body.get_pairingid());
                hm.put("ServiceType", response.body.get_service_type());
                hm.put("RepresentFlag", response.body.get_represent_flag());
                hm.put("ResultCode", response.header.get_result());
                hm.put("ResultPhrase", response.header.get_reason());
                return new ResponseRepresent(hm);
            }
        } catch (IOException e1) {
            ALog.printStackTrace(e1);
            refreshNpsAddress();
        }
        return new ResponseRepresent();
    }

    public ResponseUpdateAgreement doReqUpdateAgreement(String PairingID, String ServiceType, String PurchaseList,
                                                        String UnlimitedVod) {

        NpsService npsService = NpsServiceBuilder.buildNpsServiceWithIpHost(
                NpsAddressReader.getInstance().getNpsAddress(), NpsAddressReader.getInstance().getNpsPort());

        HashMap<String, String> agreement = new HashMap<String, String>();
        if (PurchaseList != null) {
            agreement.put("purchase_list", PurchaseList);
        }
        if (UnlimitedVod != null) {
            agreement.put("unlimited_vod", UnlimitedVod);
        }

        ResUpdateAgreement response = null;
        Call<ResUpdateAgreement> call = npsService.reqUpdateAgreement(
                SKBRemoconSvcBuilder.buildReqUpdateAgreement(SmartRcuAgentEnvironment.getBySenderName(),
                        SmartRcuAgentEnvironment.stbId, "H", PairingID, ServiceType, agreement, null));
        try {
            response = call.execute().body();

            if (response != null) {
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("PairindID", response.body.get_pairingid());
                hm.put("ServiceType", response.body.get_service_type());
                hm.put("ResultCode", response.header.get_result());
                hm.put("ResultPhrase", response.header.get_reason());
                return new ResponseUpdateAgreement(hm);
            }
        } catch (IOException e1) {
            ALog.printStackTrace(e1);
            refreshNpsAddress();
        }
        return new ResponseUpdateAgreement();
    }

    public void doReqReceiveMessage(String receive_result, String result_reason, String PairingID, String CtrlType,
                                    String CtrlValue, String SvcType, String CurChNum, String CurCID, String PlayCtrl, String CurVolume,
                                    String SID) {

        if (SmartRcuAgentEnvironment.isAbort) {
            ALog.d("Abort is true => End Request!!");
            SmartRcuAgentEnvironment.isAbort = false;
            if (NpsControlThread.getInstance().isRunning()) {
                ALog.e("Abort is true => NPS STOP!! npsStop");
                NpsControlThread.getInstance().npsStop();
            }

            return;
        }

        if(NpsControlThread.getInstance().isRunning()) {

            ALog.d("===== doReqReceiveMessage() =====");
            Log.d("TVSmartRCUAgentAPK","PairingID : " + PairingID + ", " + "CtrlType : " + CtrlType + ", "
                    + "CtrlValue : " + CtrlValue + ", "+ "SvcType : " + SvcType + ", "
                    + "CurChNum : " + CurChNum + ", "+ "CurCID : " + CurCID + ", "
                    + "PlayCtrl : " + PlayCtrl + ", "+ "CurVolume : " + CurVolume + ", "+ "SID : " + SID + "\n");
            ALog.d("===== doReqReceiveMessage() =====");

            HashMap<String, String> message = new HashMap<String, String>();
            if (CtrlType == null || CtrlType.equals("")) {
                // Not exist response data
            } else {

                if(CtrlType.equals("SendMsg")){
                    CtrlValue = AES256Util.encryptAES256(CtrlValue);
                }
                message.put("PairingID", PairingID);
                message.put("CtrlType", CtrlType);
                message.put("CtrlValue", CtrlValue);
                message.put("SvcType", SvcType);
                message.put("CurChNum", CurChNum);
                message.put("CurCID", CurCID);
                message.put("PlayCtrl", PlayCtrl);
                message.put("CurVolume", CurVolume);
                message.put("SID", SID);
            }

            NpsControlThread.getInstance().sendRequestData(receive_result, result_reason, message);
        } else {
            ALog.d("===== doReqReceiveMessage() ===== NpsControlThread is not running ");
        }
    }

    public String getDomainStatus() {
        String result = DomainStatus.getCurrentDomainStatus();
        if (DomainStatus.DOMAIN_STATUS_RECOVER.equals(result)) {
            DomainStatus.setCurrentDomainStatus(DomainStatus.DOMAIN_STATUS_SUCCESS);
        }
        ALog.d("domainStatus: " + result);
        return result;
    }

    public boolean isReqPairingStatus() throws IOException {
        if(true)
            return isReqPairingStatusNps();

        NpsService npsService = NpsServiceBuilder.buildNpsServiceWithIpHost(
                NpsAddressReader.getInstance().getNpsAddress(), NpsAddressReader.getInstance().getNpsPort());


        ResPairingStatus response = null;
        Call<ResPairingStatus> call = npsService.reqPairingStatus(SKBRemoconSvcBuilder.buildReqPairingStatus(
                SmartRcuAgentEnvironment.getBySenderName(), SmartRcuAgentEnvironment.stbId, "H", null));

        response = call.execute().body();

        if (response != null) {
            if (response.body.get_pairing_status() != null) {
                if (response.body.get_pairing_status().equals("1")) {
                    SmartRcuAgentEnvironment.isPaired = true;
                    return true;
                }
            }
        }

        SmartRcuAgentEnvironment.isPaired = false;
        return false;
    }

    public boolean isReqPairingStatusNps() throws IOException {
        ALog.d("isReqPairingStatusNps: start " );
        NpsService npsService = NpsServiceBuilder.buildNpsServiceWithIpHost(
                NpsAddressReader.getInstance().getNpsAddress(), NpsAddressReader.getInstance().getNpsPort());


        //String requestString = "{\"body\":{\"pairing_device_type\":\"H\",\"pairing_deviceid\":\"{ADC6DC0C-FBE3-11EA-BD09-5F5CE34E524C}\"},\"header\":{\"if_no\":\"IF-NPS-533\",\"receiver\":\"NPS\",\"response_format\":\"json\",\"sender\":\"STB\",\"sender_name\":\"1.10.0,19.522.42,3.5.10\",\"ver\":\"5.0\"}}";
        //String requestString = "{body:{pairing_device_type:H,pairing_deviceid:" + SmartRcuAgentEnvironment.stbId + "},header:{if_no:IF-NPS-533,receiver:NPS,response_format:json,sender:STB,sender_name:" + SmartRcuAgentEnvironment.getBySenderName() + ",ver:5.0}}";
        String requestString = "{\"body\":{\"pairing_device_type\":\"H\",\"pairing_deviceid\":\"" + SmartRcuAgentEnvironment.stbId + "\"},\"header\":{\"if_no\":\"IF-NPS-533\",\"receiver\":\"NPS\",\"response_format\":\"json\",\"sender\":\"STB\",\"sender_name\":\"" + SmartRcuAgentEnvironment.getBySenderName() + "\",\"ver\":\"5.0\"}}";
        String requestUrl = "/nps/v5/reqPairingStatus";

        ALog.d("isReqPairingStatusNps: requestUrl : " + requestUrl + ", requestString : " + requestString);

        try {
            JSONObject requestJson = new JSONObject(requestString);

            Call<ResponseBody> call = npsService.reqUrlRequest(requestUrl, RequestBody.create(MediaType.parse("application/json"), requestJson.toString()));

            ResponseBody response = call.execute().body();

            if (response != null) {
                String responseString = response.string();
                ALog.d("isReqPairingStatusNps: responseString : " + responseString );
                if (responseString != null) {
                    if (responseString.contains("\"pairing_status\":\"1\"")) {
                        SmartRcuAgentEnvironment.isPaired = true;

                        ALog.d("isReqPairingStatusNps: SmartRcuAgentEnvironment.isPaired : " + SmartRcuAgentEnvironment.isPaired );
                        return true;
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        SmartRcuAgentEnvironment.isPaired = false;
        return false;
    }

    public String doReqUserApi(String requestUrl, String requestString) {
        String resultResponse = "";

        NpsService npsService = NpsServiceBuilder.buildNpsServiceWithIpHost(
                NpsAddressReader.getInstance().getNpsAddress(), NpsAddressReader.getInstance().getNpsPort());

        ALog.i("doReqUserApi: requestUrl : " + requestUrl + ", requestString : " + requestString);

        try {
            JSONObject requestJson = new JSONObject(requestString);
            Call<ResponseBody> call = npsService.reqUrlRequest(requestUrl, RequestBody.create(MediaType.parse("application/json"), requestJson.toString()));

            ResponseBody response = call.execute().body();
            if (response != null) {
                resultResponse = response.string();

            }
        } catch (JSONException e) {
            ALog.e("doReqUserApi: JSONException " );
        } catch (IOException ioException){
            ALog.e("doReqUserApi: IOException ");
        }


        ALog.i("doReqUserApi: resultResponse : " + resultResponse );
        return resultResponse;
    }

    public void doReqUserAsyncApi(String requestUrl, String requestString) {

        NpsService npsService = NpsServiceBuilder.buildNpsServiceWithEndPointAndTimeout(
                NpsAddressReader.getInstance().getNpsAddress(), NpsAddressReader.getInstance().getNpsPort(),
                authCodeExpireDuration + 5, TimeUnit.SECONDS);

        ALog.i("doReqUserAsyncApi: requestUrl : " + requestUrl + ", requestString : " + requestString);

        try {
            JSONObject requestJson = new JSONObject(requestString);
            Call<ResponseBody> call = npsService.reqUrlRequest(requestUrl, RequestBody.create(MediaType.parse("application/json"), requestJson.toString()));

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> arg0, Response<ResponseBody> arg1) {
                    ResponseBody responseBody = arg1.body();
                    String response  = "";

                    try {
                        if(responseBody != null){
                            response = responseBody.string();

                        }
                    } catch (IOException ioException) {
                        ALog.e("doReqUserAsyncApi: IOException ");
                    }

					smartRCUAgentListener.onResponseUserAsyncApi(response);
                }

                @Override
                public void onFailure(Call<ResponseBody> arg0, Throwable arg1) {
                    ALog.d("doReqUserAsyncApi onFailure ResPairingSequence: " + arg0.toString() + arg1.getMessage());
                }
            });
        } catch (JSONException e) {
            ALog.e("doReqUserAsyncApi: JSONException " );
        }

        ALog.i("doReqUserAsyncApi: end" );
    }
}
