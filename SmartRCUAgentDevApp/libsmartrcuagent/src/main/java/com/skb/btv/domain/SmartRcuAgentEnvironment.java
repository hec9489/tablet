package com.skb.btv.domain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.skb.btv.smartrcuagent.util.ALog;
import com.skb.btv.smartrcuagent.util.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;


import android.content.Context;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class SmartRcuAgentEnvironment {
    private static final String FILE_VERSION = "/data/btv_home/config/version.txt";
    private static final String FILE_STBID = "/data/btv_home/config/devid.dat";
    private static final String FILE_MW_PROPERTY = "/data/btv_home/config/tvservice-system-properties.xml";
    private static final String FILE_MAC_ADDRESS = "/data/btv_home/config/mac_addr";

    public static String stbId;
    public static String stbCd;
    public static String childrenSeeLimit;
    public static String adultMenu;
    public static String erosMenu;
    public static String stbMacAddr;
    public static String stbIpAddr;
    public static String stbUiVesrion = "";
    public static String srcAgentVersion = "";
    public static String kidsBookVersion = "";
    public static String senderName = "";
    public static boolean isPrivate;
    public static String stbPatchVersion = "";
    public static String existStbIpAddr = "";
    public static boolean isPaired = false;
    public static boolean isUnpairing = false;
    public static boolean isAbort = false;

    public static String RCU_VERSION = "2.1.2";

    public static void buildByTvSystem(Context context, EnvironmentInfo environmentInfo) {
        //ALog.d("buildByTvSystem: start getFirmwareVersion: " + systemManager.getSystemServiceDeviceInfo().getFirmwareVersion());
        //BtvNetworkInfo networkMap = networkManager.getNetworkServiceEthernet().getNetworkInfo();
        stbMacAddr = environmentInfo.getStbMacAddr();
        stbId = environmentInfo.getStbId();
        stbCd = environmentInfo.getModel();
        childrenSeeLimit = environmentInfo.getChildrenSeeLimit();
        adultMenu = environmentInfo.getAdultMenu();
        erosMenu = environmentInfo.getErosMenu();
        stbIpAddr = environmentInfo.getStbIpAddr();
        srcAgentVersion = Utils.getPackageVersionName(context, context.getPackageName());
        isPrivate = Utils.isSiteLocalAddress(stbIpAddr);
        stbPatchVersion = environmentInfo.getStbPatchVersion();
        stbUiVesrion = environmentInfo.getUiVersion();
		senderName = environmentInfo.getSenderName();

        kidsBookVersion = Utils.getPackageVersionName(context, "com.skb.kidsbooks");


        ALog.d("PROPERTY_STBID = '" + stbId + "'");
        ALog.d("Model = '" + stbCd + "'");
        ALog.d("PROPERTY_CHILDRENSEELIMIT = '" + childrenSeeLimit + "'");
        ALog.d("PROPERTY_ADULTMOVIEMENU = '" + adultMenu + "'");
        ALog.d("PROPERTY_EROSMENU = '" + erosMenu + "'");
        ALog.d("MAC Address = '" + stbMacAddr + "'");
        ALog.d("IP Address = '" + stbIpAddr + "'");
        ALog.d("stbPatchVersion = '" + stbPatchVersion + "'");
        ALog.d("srcAgentVersion = '" + srcAgentVersion + "'");
        ALog.d("kidsBookVersion = '" + kidsBookVersion + "'");
        ALog.d("senderName = '" + senderName + "'");
    }

    public static void checkPublicTestMode() {
        // test code for public IP
        File testFile = new File("/data/btv_home/smart_rcu_public_test.json");
        if (testFile.exists()) {
            try {
                PublicMode publicMode = new Gson().fromJson(new FileReader(
                        testFile), PublicMode.class);
                if (publicMode.run) {
                    ALog.d("ip_test_mode >> change ip to :" + publicMode.ip);
                    stbIpAddr = publicMode.ip;
                    isPrivate = publicMode.isPrivate;
                    if (!isPrivate) {
                        ALog.d("public ip >> must check 8114 port fowarding");
                    }
                }
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public static void checkIpNullTestMode() {
        // test code for ip null
        File testFile = new File("/data/btv_home/smart_rcu_ip_null_test.json");
        if (testFile.exists()) {
            stbIpAddr = null;
        }
    }

    public static void getByTvStbId(EnvironmentInfo environmentInfo) {

        if (environmentInfo == null) {
            return;
        }
        stbId = environmentInfo.getStbId();
    }

    public static String getBySenderName() {
/*
        return SmartRcuAgentEnvironment.stbUiVesrion + "," + SmartRcuAgentEnvironment.stbPatchVersion + ","
                + SmartRcuAgentEnvironment.srcAgentVersion;
*/
        return senderName;
    }

    public static void setSmartRcuAgentEnvironment(Context context, EnvironmentInfo environmentInfo) {
        // network info
        stbMacAddr = getMacAddress();
        stbIpAddr = environmentInfo.getStbIpAddr();

        isPrivate = Utils.isSiteLocalAddress(stbIpAddr);

        // stb property
        stbId = getStbId();
        stbCd = android.os.Build.MODEL;
        stbPatchVersion = getFirmwareVersion();

        getHomeUiProperty();

        // package version
        stbUiVesrion = Utils.getPackageVersionName(context, "com.skb.google.tv");
        if (stbUiVesrion.isEmpty()) {
            stbUiVesrion = Utils.getPackageVersionName(context, "com.skb.tv");
        }
        kidsBookVersion = Utils.getPackageVersionName(context, "com.skb.kidsbooks");
//		if(kidsBookVersion.isEmpty()){
//			kidsBookVersion = Utils.getPackageVersionName(context, "com.skb.zemkids");
//		}
        srcAgentVersion = Utils.getPackageVersionName(context, context.getPackageName());

        ALog.d("setSmartRcuAgentEnvironment: start getFirmwareVersion: " + stbPatchVersion);
        ALog.d("setSmartRcuAgentEnvironment: PROPERTY_STBID = '" + stbId + "'");
        ALog.d("setSmartRcuAgentEnvironment: Model = '" + stbCd + "'");
        ALog.d("setSmartRcuAgentEnvironment: PROPERTY_CHILDRENSEELIMIT = '" + childrenSeeLimit + "'");
        ALog.d("setSmartRcuAgentEnvironment: PROPERTY_ADULTMOVIEMENU = '" + adultMenu + "'");
        ALog.d("setSmartRcuAgentEnvironment: PROPERTY_EROSMENU = '" + erosMenu + "'");
        ALog.d("setSmartRcuAgentEnvironment: MAC Address = '" + stbMacAddr + "'");
        ALog.d("setSmartRcuAgentEnvironment: IP Address = '" + stbIpAddr + "'");
        ALog.d("setSmartRcuAgentEnvironment: stbPatchVersion = '" + stbPatchVersion + "'");
        ALog.d("setSmartRcuAgentEnvironment: kidsBookVersion = '" + kidsBookVersion + "'");
        ALog.d("setSmartRcuAgentEnvironment: srcAgentVersion = '" + srcAgentVersion + "'");
    }

    private static String getFirmwareVersion() {
        ALog.e("getFirmwareVersion start");
        String firmwareVersion = "";

        File file = new File(FILE_VERSION);
        if (file.exists()) {
            try {
                FileInputStream fIn = new FileInputStream(file);
                InputStreamReader isr = new InputStreamReader(fIn);
                BufferedReader buffreader = new BufferedReader(isr);

                firmwareVersion = buffreader.readLine();
                firmwareVersion.trim();
                firmwareVersion = firmwareVersion.replaceAll("(\r|\n|\r\n|\n\r)", "");

                ALog.d("getFirmwareVersion btv_home/config/version.txt : " + firmwareVersion);
                isr.close();
                fIn.close();
            } catch (IOException ioe) {
                ALog.e("getFirmwareVersion IOException error");
            }
        } else {
            ALog.e("getFirmwareVersion file not exists");
        }
        return firmwareVersion;
    }

    private static String getStbId() {
        ALog.e("getStbId start");
        String stbId = "";

        File file = new File(FILE_STBID);
        if (file.exists()) {
            try {
                FileInputStream fIn = new FileInputStream(file);
                InputStreamReader isr = new InputStreamReader(fIn);
                BufferedReader buffreader = new BufferedReader(isr);

                stbId = buffreader.readLine();
                stbId.trim();
                stbId = stbId.replaceAll("(\r|\n|\r\n|\n\r)", "");

                ALog.d("getStbId btv_home/config/devid.dat : " + stbId);
                isr.close();
                fIn.close();
            } catch (IOException ioe) {
                ALog.e("getStbId IOException error");
            }
        } else {
            ALog.e("getStbId file not exists");
        }
        return stbId;
    }

    private static void getHomeUiProperty() {
        ALog.d("getHomeUiProperty : start ");
        boolean isChildrenSeeLimit = false;
        boolean isAdultMenu = false;
        boolean isErosMenu = false;
        boolean isStbId = false;
        boolean isStbMac = false;
        boolean isStbCode = false;
        boolean isStbPatchVersion = false;

        if (Utils.isValidStbId(stbId)) {
            isStbId = true;
        }

        if (Utils.isValidStbMac(stbMacAddr)) {
            isStbMac = true;
        }

        if (stbCd.length() > 0) {
            isStbCode = true;
        }

        if (stbPatchVersion.length() > 0) {
            isStbPatchVersion = true;
        }

        ALog.d("getHomeUiProperty : start isStbId : " + isStbId + ", isStbMac : " + isStbMac + ", isStbCode : " + isStbCode + ", isStbPatchVersion : " + isStbPatchVersion);

        // XML 문서 파싱
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = factory.newDocumentBuilder();
            Document document = documentBuilder.parse(new File(FILE_MW_PROPERTY));
            ALog.d("getHomeUiProperty : start " + FILE_MW_PROPERTY);

            // root 구하기
            Element root = document.getDocumentElement();

            // root의 속성
            NodeList childeren = root.getChildNodes(); // 자식 노드 목록 get
            ALog.d("getHomeUiProperty : childeren.length : " + childeren.getLength());
            for (int i = 0; i < childeren.getLength(); i++) {
                Node node = childeren.item(i);
                //ALog.d("node.getNodeType : " + node.getNodeType());
                if (node.getNodeType() == Node.ELEMENT_NODE) { // 해당 노드의 종류 판정(Element일 때)
                    Element ele = (Element) node;
                    String nodeName = ele.getNodeName();

                    if (nodeName.equalsIgnoreCase("entry")) {

                        if (ele.getAttribute("key").length() > 0) {
                            if (ele.getAttribute("key").equals("CHILDRENSEELIMIT")) {
                                ALog.d("channel attribute key : " + ele.getAttribute("key") + ", ele.getTextContent() : " + ele.getTextContent());

                                childrenSeeLimit = ele.getTextContent(); // CHILDRENSEELIMIT
                                isChildrenSeeLimit = true;
                            } else if (ele.getAttribute("key").equals("ADULTMOVIEMENU")) {
                                ALog.d("channel attribute key : " + ele.getAttribute("key") + ", ele.getTextContent() : " + ele.getTextContent());

                                adultMenu = ele.getTextContent(); // ADULTMOVIEMENU
                                isAdultMenu = true;
                            } else if (ele.getAttribute("key").equals("EROSMENU")) {
                                ALog.d("channel attribute key : " + ele.getAttribute("key") + ", ele.getTextContent() : " + ele.getTextContent());

                                erosMenu = ele.getTextContent(); // EROSMENU
                                isErosMenu = true;
                            } else if (ele.getAttribute("key").equals("STBID")) {
                                ALog.d("channel attribute key : " + ele.getAttribute("key") + ", ele.getTextContent() : " + ele.getTextContent() + ", isStbId : " + isStbId);
                                if (isStbId == false) {
                                    stbId = ele.getTextContent(); // STBID
                                    isStbId = true;
                                }
                            } else if (ele.getAttribute("key").equals("STB_MAC")) {
                                ALog.d("channel attribute key : " + ele.getAttribute("key") + ", ele.getTextContent() : " + ele.getTextContent() + ", isStbMac : " + isStbMac);
                                if (isStbMac == false) {
                                    stbMacAddr = ele.getTextContent(); // STB_MAC
                                    isStbMac = true;
                                }
                            } else if (ele.getAttribute("key").equals("STB_MODEL")) {
                                ALog.d("channel attribute key : " + ele.getAttribute("key") + ", ele.getTextContent() : " + ele.getTextContent() + ", isStbCode : " + isStbCode);
                                if (isStbCode == false) {
                                    stbCd = ele.getTextContent(); // STB_MODEL
                                    isStbCode = true;
                                }
                            } else if (ele.getAttribute("key").equals("STB_SW_VERSION")) {
                                ALog.d("channel attribute key : " + ele.getAttribute("key") + ", ele.getTextContent() : " + ele.getTextContent() + ", isStbPatchVersion : " + isStbPatchVersion);
                                if (isStbPatchVersion == false) {
                                    stbPatchVersion = ele.getTextContent(); // STB_SW_VERSION
                                    isStbPatchVersion = true;
                                }
                            }
                        }
                    }
                }
                if (isChildrenSeeLimit && isAdultMenu && isErosMenu && isStbId && isStbMac && isStbCode && isStbPatchVersion) {
                    ALog.d("getHomeUiProperty : get property all. so break; ");
                    break;
                }
            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getMacAddress() {
        ALog.e("getMacAddress start");
        String macAddress = "";

        File file = new File(FILE_MAC_ADDRESS);
        if (file.exists()) {
            try {
                FileInputStream fIn = new FileInputStream(file);
                InputStreamReader isr = new InputStreamReader(fIn);
                BufferedReader buffreader = new BufferedReader(isr);

                macAddress = buffreader.readLine();
                macAddress.trim();
                macAddress = macAddress.replaceAll("(\r|\n|\r\n|\n\r)", "");

                ALog.d("getMacAddress btv_home/config/mac_addr : " + macAddress);
                isr.close();
                fIn.close();
            } catch (IOException ioe) {
                ALog.e("getMacAddress IOException error");
            }
        } else {
            ALog.e("getMacAddress file not exists");
        }
        return macAddress;
    }

}
