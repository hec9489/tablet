package com.skb.btv.smartrcuagent.nps.domain;

import java.util.HashMap;
import java.util.Map;

public class ReqReceiveMessage {

    public Header header = new Header();
    public Body body = new Body();

    public class Header {
        public String if_no;
        public String ver;
        public String sender_name;
        public String response_format;
        public String sender;
        public String receiver;
    }

    public class Body {
        public String receive_deviceid;
        public String receive_result;
        public String receive_result_reason;
        public Map<String, String> message = new HashMap<String, String>();
        public Map<String, String> custom_param = new HashMap<String, String>();
    }

}
