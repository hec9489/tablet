package com.skb.btv.smartrcuagent;

import java.util.HashMap;

public class NpsObject {
    private HashMap<String, String> map;

    public NpsObject(HashMap<String, String> map) {
        this.map = map;
    }

    public HashMap<String, String> getMap() {
        return map;
    }

    public void setMap(HashMap<String, String> map) {
        this.map = map;
    }
}
