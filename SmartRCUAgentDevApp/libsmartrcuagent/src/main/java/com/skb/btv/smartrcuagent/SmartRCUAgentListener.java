package com.skb.btv.smartrcuagent;

import com.skb.btv.smartrcuagent.message.ResponsePairingSequence;
import com.skb.btv.smartrcuagent.message.ResponseReceiveMessage;

/**
 * Created by parkjeongho on 2021-05-27 오후 3:58
 */
public interface SmartRCUAgentListener {
    void onResponseReceiveMessage(ResponseReceiveMessage response);
    void onResponsePairingSequence(ResponsePairingSequence response);
    void onResponseUserAsyncApi(String response);
}
