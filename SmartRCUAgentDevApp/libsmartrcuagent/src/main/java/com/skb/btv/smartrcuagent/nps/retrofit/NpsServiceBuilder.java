package com.skb.btv.smartrcuagent.nps.retrofit;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class NpsServiceBuilder {
    private static final String NPS_IP = "1.255.86.154";

    public static NpsService buildNpsServiceDefault() {
        return buildNpsServiceWithEndPointAndTimeout(
                NPS_IP, 8080, 65,
                TimeUnit.SECONDS);
    }

    public static NpsService buildNpsServiceWithIpHost(String host, int port) {
        return buildNpsServiceWithEndPointAndTimeout(host, port, 65,
                TimeUnit.SECONDS);
    }

    public static NpsService buildNpsServiceWithEndPointAndTimeout(String host,
                                                                   int port, long timeout, TimeUnit timeUnit) {
        host = checkUrlWithHttp(host);
        if (port != 80) {
            host += ":" + port;
        }


        HttpLoggingInterceptor logTag = new HttpLoggingInterceptor();
        logTag.setLevel(Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(timeout, timeUnit);
        httpClient.readTimeout(timeout, timeUnit);
        httpClient.writeTimeout(timeout, timeUnit);
        httpClient.addInterceptor(logTag);
        httpClient.addInterceptor(new LogInterceptor());
        NpsService npsService = new Retrofit.Builder().baseUrl(host)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build().create(NpsService.class);

        return npsService;
    }

    private static String checkUrlWithHttp(String url) {
        if (!url.startsWith("http://")) {
            url = "http://" + url;
        }
        return url;
    }
}
