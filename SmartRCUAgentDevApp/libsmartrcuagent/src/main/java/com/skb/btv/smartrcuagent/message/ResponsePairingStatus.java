package com.skb.btv.smartrcuagent.message;

import java.util.HashMap;

import android.os.Parcel;
import android.os.Parcelable;

public class ResponsePairingStatus implements Parcelable {
    public String PairingID;
    public String ServiceType;
    public String PairingStatus;
    public String ResultCode;
    public String ResultPhrase;

    public static final Creator<ResponsePairingStatus> CREATOR = new Creator<ResponsePairingStatus>() {
        public ResponsePairingStatus createFromParcel(Parcel in) {
            return new ResponsePairingStatus(in);
        }

        public ResponsePairingStatus[] newArray(int size) {
            return new ResponsePairingStatus[size];
        }
    };

    public ResponsePairingStatus() {
    }

    public ResponsePairingStatus(HashMap<String, String> hm) {
        PairingID = hm.get("PairingID");
        ServiceType = hm.get("ServiceType");
        PairingStatus = hm.get("PairingStatus");
        ResultCode = hm.get("ResultCode");
        ResultPhrase = hm.get("ResultPhrase");
    }

    private ResponsePairingStatus(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    public void readFromParcel(Parcel in) {
        PairingID = in.readString();
        ServiceType = in.readString();
        PairingStatus = in.readString();
        ResultCode = in.readString();
        ResultPhrase = in.readString();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        // TODO Auto-generated method stub
        out.writeString(PairingID);
        out.writeString(ServiceType);
        out.writeString(PairingStatus);
        out.writeString(ResultCode);
        out.writeString(ResultPhrase);
    }

    @Override
    public String toString() {
        return "[ResponsePairingStatus (" + "PairingID=" + PairingID
                + ", ServiceType=" + ServiceType + ", PairingStatus=" + PairingStatus
                + ", ResultCode=" + ResultCode + ", ResultPhrase=" + ResultPhrase + ")]";
    }

}
