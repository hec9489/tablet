package com.skb.btv.smartrcuagent.event;

public class MouseWheelEvent extends Event {
    private int xScrollAmt;
    private int yScrollAmt;

    public MouseWheelEvent(int xScrollAmt, int yScrollAmt) {
        this.type = Type.MouseWheelEvent;
        this.xScrollAmt = xScrollAmt;
        this.yScrollAmt = yScrollAmt;
    }

    public int getXScrollAmt() {
        return xScrollAmt;
    }

    public int getYScrollAmt() {
        return yScrollAmt;
    }

}
