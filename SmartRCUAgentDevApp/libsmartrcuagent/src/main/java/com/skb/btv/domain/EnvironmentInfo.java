package com.skb.btv.domain;

/**
 * Created by parkjeongho on 2021-05-25 오후 4:54
 */
public class EnvironmentInfo {
    private String stbMacAddr;
    private String stbId;
    private String model;
    private String childrenSeeLimit;
    private String adultMenu;
    private String erosMenu;
    private String stbIpAddr;
    private String stbPatchVersion;
    private String uiVersion;
    private String senderName;

    public String getStbMacAddr() {
        return stbMacAddr;
    }

    public void setStbMacAddr(String stbMacAddr) {
        this.stbMacAddr = stbMacAddr;
    }

    public String getStbId() {
        return stbId;
    }

    public void setStbId(String stbId) {
        this.stbId = stbId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
	
    public String getChildrenSeeLimit() {
        return childrenSeeLimit;
    }

    public void setChildrenSeeLimit(String childrenSeeLimit) {
        this.childrenSeeLimit = childrenSeeLimit;
    }

    public String getAdultMenu() {
        return adultMenu;
    }

    public void setAdultMenu(String adultMenu) {
        this.adultMenu = adultMenu;
    }

    public String getErosMenu() {
        return erosMenu;
    }

    public void setErosMenu(String erosMenu) {
        this.erosMenu = erosMenu;
    }

    public String getStbIpAddr() {
        return stbIpAddr;
    }

    public void setStbIpAddr(String stbIpAddr) {
        this.stbIpAddr = stbIpAddr;
    }
	
    public String getStbPatchVersion() {
        return stbPatchVersion;
    }

    public void setStbPatchVersion(String stbPatchVersion) {
        this.stbPatchVersion = stbPatchVersion;
    }

    public String getUiVersion() {
        return uiVersion;
    }

    public void setUiVersion(String uiVersion) {
        this.uiVersion = uiVersion;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }
}
