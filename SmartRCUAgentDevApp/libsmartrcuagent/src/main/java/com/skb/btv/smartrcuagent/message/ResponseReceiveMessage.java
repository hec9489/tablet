package com.skb.btv.smartrcuagent.message;

import java.util.HashMap;

import android.os.Parcel;
import android.os.Parcelable;

public class ResponseReceiveMessage implements Parcelable {

    public String PairingID;
    public String ServiceType;
    public String CtrlType;
    public String CtrlValue;
    public String RCStatusQuery;
    public String PushText;
    public String JsonMessage;
    public String CustomParam;

    public static final Creator<ResponseReceiveMessage> CREATOR = new Creator<ResponseReceiveMessage>() {
        public ResponseReceiveMessage createFromParcel(Parcel in) {
            return new ResponseReceiveMessage(in);
        }

        public ResponseReceiveMessage[] newArray(int size) {
            return new ResponseReceiveMessage[size];
        }
    };

    public ResponseReceiveMessage() {
    }

    public ResponseReceiveMessage(HashMap<String, String> hm) {
        PairingID = hm.get("PairingID");
        ServiceType = hm.get("ServiceType");
        CtrlType = hm.get("CtrlType");
        CtrlValue = hm.get("CtrlValue");
        RCStatusQuery = hm.get("RCStatusQuery");
        PushText = hm.get("PushText");
        JsonMessage = hm.get("JsonMessage");
        CustomParam = hm.get("CustomParam");
    }

    private ResponseReceiveMessage(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel out, int arg1) {
        out.writeString(PairingID);
        out.writeString(ServiceType);
        out.writeString(CtrlType);
        out.writeString(CtrlValue);
        out.writeString(RCStatusQuery);
        out.writeString(PushText);
        out.writeString(JsonMessage);
        out.writeString(CustomParam);
    }

    public void readFromParcel(Parcel in) {
        PairingID = in.readString();
        ServiceType = in.readString();
        CtrlType = in.readString();
        CtrlValue = in.readString();
        RCStatusQuery = in.readString();
        PushText = in.readString();
        JsonMessage = in.readString();
        CustomParam = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
