package com.skb.btv.smartrcuagent.nps.domain;

import java.util.HashMap;
import java.util.Map;

public class ReqUpdateHostDeviceInfo {
    public Header header = new Header();
    public Body body = new Body();

    public class Header {
        public String if_no;
        public String ver;
        public String sender_name;
        public String response_format;
        public String sender;
        public String receiver;
    }

    public class Body {
        public String host_deviceid;
        public Map<String, String> host_deviceinfo = new HashMap<String, String>();
        public Map<String, String> custom_param = new HashMap<String, String>();
    }
}
