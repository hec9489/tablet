package com.skb.btv.smartrcuagent.message;

import java.util.HashMap;

import android.os.Parcel;
import android.os.Parcelable;

public class ResponseUpdateHostDeviceInfo implements Parcelable {
    public String ResultCode;
    public String ResultPhrase;

    public static final Creator<ResponseUpdateHostDeviceInfo> CREATOR = new Creator<ResponseUpdateHostDeviceInfo>() {
        public ResponseUpdateHostDeviceInfo createFromParcel(Parcel in) {
            return new ResponseUpdateHostDeviceInfo(in);
        }

        public ResponseUpdateHostDeviceInfo[] newArray(int size) {
            return new ResponseUpdateHostDeviceInfo[size];
        }
    };

    public ResponseUpdateHostDeviceInfo() {
    }

    public ResponseUpdateHostDeviceInfo(HashMap<String, String> hm) {
        ResultCode = hm.get("ResultCode");
        ResultPhrase = hm.get("ResultPhrase");
    }

    private ResponseUpdateHostDeviceInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel out, int arg1) {
        out.writeString(ResultCode);
        out.writeString(ResultPhrase);
    }

    public void readFromParcel(Parcel in) {
        ResultCode = in.readString();
        ResultPhrase = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "[ResponseUpdateHostDeviceInfo (ResultCode=" + ResultCode + ", ResultPhrase=" + ResultPhrase + ")]";
    }
}
