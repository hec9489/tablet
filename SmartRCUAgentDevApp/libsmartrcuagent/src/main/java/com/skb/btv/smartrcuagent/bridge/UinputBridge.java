/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.skb.btv.smartrcuagent.bridge;

import com.skb.btv.smartrcuagent.util.ALog;

import java.io.IOException;

/**
 * Sends the input event to the linux driver.
 *
 * @author tambard@google.com (Thibault Ambard)
 */
public final class UinputBridge implements InputBridge {

    private final int mFd;

    public UinputBridge(String devname, String uniqueId, int classes) throws IOException {
        ALog.e("UinputBridge devname : " + devname + ", uniqueId : " + uniqueId + ", classes : " + classes);
        int fd = nativeConnect(devname, uniqueId, classes, InputBridge.TOUCH_PRESSURE_SCALE);
        if (fd < 0) {
            throw new IOException(String.format("Could not connect to: %s with classes 0x%X",
                    devname, classes));
        }
        ALog.e("UinputBridge fd : " + fd);
        mFd = fd;
    }

    /**
     * Disconnects and unregisters a file descriptor from uinput.
     */
    public void disconnect() {
        nativeDisconnect(mFd);
    }

    /**
     * Sends a new input event to the uinput layer.
     *
     * @param type  the uinput event type
     * @param code  the uinput event code
     * @param value the uinput event value
     */
    @Override
    public void sendEvent(int type, int code, int value) {
        nativeSendEvent(mFd, type, code, value);
    }

    /**
     * Sends mouse movement event to the uinput layer.
     *
     * @param deltaX the X delta of the movement
     * @param deltaY the Y delta of the movement
     */
    @Override
    public void sendMouseMovement(int deltaX, int deltaY) {
        nativeSendMouseMovement(mFd, deltaX, deltaY);
    }

    @Override
    public void sendTouchEvent(int count, int id[], int action[],
                               int x[], int y[], int pressure[]) {
        nativeSendTouchEvent(mFd, count, id, action, x, y, pressure);
    }

    /**
     * Creates a new uinput device.
     *
     * @param devname            the name to use when registering the new device
     * @param path               the unique device-id
     * @param classes            a bitmask of supported classes
     * @param touchPressureScale devices touch screen's pressure scale factor
     * @return a uinput file descriptor for the new device
     */
    private static native int nativeConnect(String devname, String path,
                                            int classes, int touchPressureScale);

    /**
     * Disconnects and unregisters a file descriptor from uinput.
     *
     * @param fd the file descriptor to unregister.
     */
    private static native void nativeDisconnect(int fd);

    /**
     * Sends a new input event to the uinput layer.
     *
     * @param fd    the file descriptor of the uinput device
     * @param type  the uinput event type
     * @param code  the uinput event code
     * @param value the uinput event value
     */
    private static native void nativeSendEvent(int fd, int type, int code,
                                               int value);

    /**
     * Sends mouse movement event to the uinput layer.
     *
     * @param fd     the file descriptor of the uinput device
     * @param deltaX the X delta of the movement
     * @param deltaY the Y delta of the movement
     */
    private static native void nativeSendMouseMovement(int fd, int deltaX,
                                                       int deltaY);

    /**
     * Sends touch event to the uinput layer.
     *
     * @param fd       the file descriptor of the uinput device
     * @param count    number of pointers in a touch event
     * @param id       ID of each pointer
     * @param action   action for each pointer. (0: up, 1: down)
     * @param x        X coordinates of each pointer
     * @param y        Y coordinates of each pointer
     * @param pressure pressure of each pointer
     */
    private static native void nativeSendTouchEvent(int fd, int count,
                                                    int id[], int action[], int x[], int y[], int pressure[]);
}
