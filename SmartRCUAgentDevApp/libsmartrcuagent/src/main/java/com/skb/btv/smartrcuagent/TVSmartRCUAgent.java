package com.skb.btv.smartrcuagent;

import android.content.Context;
import android.util.Log;

import com.skb.btv.domain.EnvironmentInfo;
import com.skb.btv.domain.SmartRcuAgentEnvironment;
import com.skb.btv.smartrcuagent.message.ResponseAgreement;
import com.skb.btv.smartrcuagent.message.ResponsePairing;
import com.skb.btv.smartrcuagent.message.ResponsePairingConfirm;
import com.skb.btv.smartrcuagent.message.ResponsePairingList;
import com.skb.btv.smartrcuagent.message.ResponsePairingSequence;
import com.skb.btv.smartrcuagent.message.ResponseReceiveMessage;
import com.skb.btv.smartrcuagent.message.ResponseRepresent;
import com.skb.btv.smartrcuagent.message.ResponseSetRepresent;
import com.skb.btv.smartrcuagent.message.ResponseUnPairing;
import com.skb.btv.smartrcuagent.message.ResponseUpdateAgreement;
import com.skb.btv.smartrcuagent.message.ResponseUpdateHostDeviceInfo;

/**
 * Created by parkjeongho on 2021-05-25 오후 3:30
 */
public class TVSmartRCUAgent {
    private final static String TAG = "TVSmartRCUAgentLib";
	private final static String VERSION = "0.9.7"; //20210610
    private Context mContext;
    private SmartRCUAgentServiceImpl mSmartRCUAgentService;

    private SmartRCUAgentListener mSmartRCUAgentListener;

    private static TVSmartRCUAgent instance;

    public synchronized static TVSmartRCUAgent getInstance() {
        if (instance == null) {
            instance = new TVSmartRCUAgent();
        }
        return instance;
    }

    private TVSmartRCUAgent() {
    	Log.e(TAG,"===========================");
    	Log.e(TAG,"TVSmartRCUAgent version : "+ VERSION);
		Log.e(TAG,"===========================");
        mSmartRCUAgentService = SmartRCUAgentServiceImpl.getInstance();
    }
	
    public void startService(Context context, EnvironmentInfo environmentInfo) {
        mContext = context;
        SmartRcuAgentEnvironment.buildByTvSystem(mContext, environmentInfo);
    }
	
    public void stopService() {
        mSmartRCUAgentService.stopService();
    }

    public void setOnListener(SmartRCUAgentListener listener) {
        mSmartRCUAgentListener = listener;
        mSmartRCUAgentService.setSmartRCUAgentListener(mSmartRCUAgentListener);
    }

    public ResponsePairing requestPairing() {
        ResponsePairing response = null;
        if (mSmartRCUAgentService != null) {
            response = mSmartRCUAgentService.doReqPairing();
        }
        return response;
    }

    public void requestPairingSequence() {
        if (mSmartRCUAgentService != null) {
            mSmartRCUAgentService.doReqPairingSequence();
        }
    }

    public void requestPairingStatus() {
        if (mSmartRCUAgentService != null) {
            mSmartRCUAgentService.doReqPairingStatus();
        }
    }

    public ResponsePairingConfirm requestPairingConfirm(String PairingProcess, String RestrictedAge, String AdultSafetyMode) {
        ResponsePairingConfirm response = null;
        if (mSmartRCUAgentService != null) {
            response = mSmartRCUAgentService.doReqPairingConfirm(PairingProcess, RestrictedAge, AdultSafetyMode);
        }
        return response;
    }

    public ResponseUpdateHostDeviceInfo requestUpdateHostDeviceInfo(String RestrictedAge, String AdultSafetyMode) {
        ResponseUpdateHostDeviceInfo response = null;
        if (mSmartRCUAgentService != null) {
            response = mSmartRCUAgentService.doReqUpdateHostDeviceInfo(RestrictedAge, AdultSafetyMode);
        }
        return response;
    }

    public ResponsePairingList requestPairingList() {
        ResponsePairingList response = null;
        if (mSmartRCUAgentService != null) {
            response = mSmartRCUAgentService.doReqPairingList();
        }
        return response;
    }

    public ResponseUnPairing requestUnPairing(String PairingID, String ServiceType) {
        ResponseUnPairing response = null;
        if (mSmartRCUAgentService != null) {
            response = mSmartRCUAgentService
                    .doReqUnPairing(PairingID, ServiceType);
        }
        return response;
    }

    public ResponseUpdateAgreement requestUpdateAgreement(String PairingID, String ServiceType
            , String PurchaseList, String UnlimitedVod) {
        ResponseUpdateAgreement response = null;
        if (mSmartRCUAgentService != null) {
            response = mSmartRCUAgentService
                    .doReqUpdateAgreement(PairingID, ServiceType, PurchaseList, UnlimitedVod);
        }
        return response;
    }

    public ResponseAgreement requestAgreement(String PairingID, String ServiceType) {
        ResponseAgreement response = null;
        if (mSmartRCUAgentService != null) {
            response = mSmartRCUAgentService
                    .doReqAgreement(PairingID, ServiceType);
        }
        return response;
    }

    public ResponseSetRepresent requestSetRepresent(String PairingID, String ServiceType, String RepresentFlag) {
        ResponseSetRepresent response = null;
        if (mSmartRCUAgentService != null) {
            response = mSmartRCUAgentService
                    .doReqSetRepresent(PairingID, ServiceType, RepresentFlag);
        }
        return response;
    }

    public ResponseRepresent requestRepresent(String PairingID, String ServiceType) {
        ResponseRepresent response = null;
        if (mSmartRCUAgentService != null) {
            response = mSmartRCUAgentService
                    .doReqRepresent(PairingID, ServiceType);
        }
        return response;
    }

    public String getDomainStatus() {
        Log.d(TAG, "getDomainStatus()");
        if (mSmartRCUAgentService != null) {
            String domainStatus = mSmartRCUAgentService
                    .getDomainStatus();
            Log.d(TAG, "getDomainStatus() >> return: " + domainStatus);
            return domainStatus;
        } else {
            return "";
        }
    }

    public void requestReceiveMessage(String receive_result, String result_reason, String PairingID,
                                      String CtrlType, String CtrlValue, String SvcType, String CurChNum, String CurCID, String PlayCtrl,
                                      String CurVolume, String SID) {

        if (mSmartRCUAgentService != null) {
            mSmartRCUAgentService.doReqReceiveMessage(receive_result,
                    result_reason, PairingID, CtrlType, CtrlValue, SvcType, CurChNum, CurCID,
                    PlayCtrl, CurVolume, SID);
        }
    }

    public String requestUserApi(String url, String requestJson) {
        Log.d(TAG, "requestUserApi() url : " + url + " requestJson : " + requestJson);
        if (mSmartRCUAgentService != null) {
            String response = mSmartRCUAgentService.doReqUserApi(url, requestJson);
            Log.d(TAG, "requestUserApi() >> return: " + response);
            return response;
        } else {
            return "";
        }
    }


    public void requestUserAsyncApi(String url, String requestJson) {
        Log.d(TAG, "requestUserAsyncApi() url : " + url + " requestJson : " + requestJson);
        if (mSmartRCUAgentService != null) {
            mSmartRCUAgentService.doReqUserAsyncApi(url, requestJson);
            Log.d(TAG, "requestUserAsyncApi() >> return: ");
            return;
        }
    }

}
