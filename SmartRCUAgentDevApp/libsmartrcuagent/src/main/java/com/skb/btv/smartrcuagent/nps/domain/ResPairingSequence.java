package com.skb.btv.smartrcuagent.nps.domain;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class ResPairingSequence {
    public class Header {

        @SerializedName("if_no")
        String if_no;
        @SerializedName("ver")
        String ver;
        @SerializedName("response_format")
        String response_format;
        @SerializedName("result")
        String result;
        @SerializedName("reason")
        String reason;
        @SerializedName("sender")
        String sender;
        @SerializedName("receiver")
        String receiver;

        public String get_if_no() {
            return if_no;
        }

        public String get_ver() {
            return ver;
        }

        public String get_response_format() {
            return response_format;
        }

        public String get_result() {
            return result;
        }

        public String get_reason() {
            return reason;
        }

        public String get_sender() {
            return sender;
        }

        public String get_receiver() {
            return receiver;
        }

        @Override
        public String toString() {
            return "\"header\" : { " + "if_no = " + if_no + "," + "ver = " + ver + ","
                    + "response_format = " + response_format + "," + "result = " + result + ","
                    + "reason = " + reason + "," + "sender = " + sender + ","
                    + "receiver = " + receiver + " }";
        }
    }

    public class Body {
        @SerializedName("host_deviceid")
        String host_deviceid;
        @SerializedName("service_type")
        String service_type;
        @SerializedName("user_name")
        String user_name;
        @SerializedName("muser_num")
        String muser_num;
        @SerializedName("userid")
        String userid;
        @SerializedName("custom_param")
        Map<String, String> custom_param = new HashMap<String, String>();

        public String get_host_deviceid() {
            return host_deviceid;
        }

        public String get_service_type() {
            return service_type;
        }

        public String get_user_name() {
            return user_name;
        }

        public String get_muser_num() {
            return muser_num;
        }

        public String get_user_id() {
            return userid;
        }

        public Map<String, String> get_custom_param() {
            return custom_param;
        }

        @Override
        public String toString() {
            return "\"body\" : { " + "host_deviceid = " + host_deviceid + ","
                    + "service_type = " + service_type + "," + "muser_num = " + muser_num + ","
                    + "user_name = " + user_name + "," + "user_id = " + userid + ","
                    + "custom_param = " + custom_param + " }";
        }
    }

    @SerializedName("header")
    public Header header = new Header();
    @SerializedName("body")
    public Body body = new Body();


    @Override
    public String toString() {
        return "ResPairingSequence : {" + "\n" + header + "\n" + body + "\n" + "}";
    }
}
