package com.skb.btv.smartrcuagent.util;

import android.util.Log;

import static com.skb.btv.domain.SmartRcuAgentEnvironment.RCU_VERSION;

public class ALog {

    public final static String TAG = "TVSmartRCUAgentAPK " + RCU_VERSION;
    // private final static boolean logaable = Log.isLoggable(TAG, Log.DEBUG);
    private final static boolean logaable = true;
    private final static boolean isVervose = true;

    public static void printStackTrace(Exception e) {
        if (logaable) {
            if (isVervose) {
                Log.e(TAG, "error", e);
            } else {
                if (e.getMessage() != null) {
                    Log.e(TAG, "error: " + e.getMessage());
                } else {
                    Log.e(TAG, "error: unknown");
                }
            }

        }

    }

    public static void printStackTrace(Throwable th) {
        if (logaable) {
            if (isVervose) {
                Log.e(TAG, "error", th);
            } else {
                if (th.getMessage() != null) {
                    Log.e(TAG, "error: " + th.getMessage());
                } else {
                    Log.e(TAG, "error: unknown");
                }
            }

        }
    }

    public static void d(String message) {
        if (logaable) {
            if (isVervose) {
                StackTraceElement ste = new Throwable().getStackTrace()[1];
                message += " >> file: " + ste.getFileName() + " >> LINE: "
                        + ste.getLineNumber() + ">> TID: "
                        + Thread.currentThread().getId();
            }
            Log.d(TAG, message);
        }
    }

    public static void v(String message) {
        if (logaable) {
            if (isVervose) {
                StackTraceElement ste = new Throwable().getStackTrace()[1];
                message += " FILE: " + ste.getFileName() + " >> LINE: "
                        + ste.getLineNumber();
            }
            Log.v(TAG, message);
        }
    }

    public static void e(String message) {
        if (logaable) {
            if (isVervose) {
                StackTraceElement ste = new Throwable().getStackTrace()[1];
                message += " file: " + ste.getFileName() + " >> LINE: "
                        + ste.getLineNumber();
            }
            Log.e(TAG, message);
        }
    }

    public static void i(String message) {
        if (logaable) {
            if (isVervose) {
                StackTraceElement ste = new Throwable().getStackTrace()[1];
                message += " file: " + ste.getFileName() + " >> LINE: "
                        + ste.getLineNumber();
            }
            Log.i(TAG, message);
        }
    }

}
