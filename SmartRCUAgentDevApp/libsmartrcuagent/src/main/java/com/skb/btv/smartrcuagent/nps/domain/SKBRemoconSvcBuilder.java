package com.skb.btv.smartrcuagent.nps.domain;

import java.util.Map;

public class SKBRemoconSvcBuilder {

    public static ReqPairing buildReqPairing(String sender_name,
                                             String host_deviceid, Map<String, String> custom) {

        ReqPairing request = new ReqPairing();

        request.header.if_no = "IF-NPS-501";
        request.header.ver = "5.0";
        request.header.sender_name = sender_name;
        request.header.response_format = "json";
        request.header.sender = "STB";
        request.header.receiver = "NPS";

        request.body.host_deviceid = host_deviceid;
        request.body.custom_param = custom;

        return request;
    }

    public static ReqPairingSeqeunce buildReqPairingSequence(String sender_name, String host_deviceid,
                                                             String pairing_sessionid, Map<String, String> custom) {

        ReqPairingSeqeunce request = new ReqPairingSeqeunce();

        request.header.if_no = "IF-NPS-502";
        request.header.ver = "5.0";
        request.header.sender_name = sender_name;
        request.header.response_format = "json";
        request.header.sender = "STB";
        request.header.receiver = "NPS";

        request.body.host_deviceid = host_deviceid;
        request.body.pairing_sessionid = pairing_sessionid;
        request.body.custom_param = custom;

        return request;
    }

    public static ReqPairingConfirm buildReqPairingConfirm(String sender_name, String host_deviceid,
                                                           String pairing_sessionid, String pairing_process, Map<String, String> deviceinfo, Map<String, String> custom) {

        ReqPairingConfirm request = new ReqPairingConfirm();
        request.header.if_no = "IF-NPS-503";
        request.header.ver = "5.0";
        request.header.sender_name = sender_name;
        request.header.response_format = "json";
        request.header.sender = "STB";
        request.header.receiver = "NPS";

        request.body.host_deviceid = host_deviceid;
        request.body.pairing_sessionid = pairing_sessionid;
        request.body.pairing_process = pairing_process;
        request.body.host_deviceinfo = deviceinfo;
        request.body.custom_param = custom;

        return request;
    }

    public static ReqUpdateHostDeviceInfo buildReqUpdateHostDeviceInfo(String sender_name, String host_deviceid,
                                                                       Map<String, String> deviceinfo, Map<String, String> custom) {

        ReqUpdateHostDeviceInfo request = new ReqUpdateHostDeviceInfo();
        request.header.if_no = "IF-NPS-504";
        request.header.ver = "5.0";
        request.header.sender_name = sender_name;
        request.header.response_format = "json";
        request.header.sender = "STB";
        request.header.receiver = "NPS";

        request.body.host_deviceid = host_deviceid;
        request.body.host_deviceinfo = deviceinfo;
        request.body.custom_param = custom;

        return request;
    }

    public static ReqReceiveMessage buildReqReceiveMessage(String sender_name, String receive_deviceid,
                                                           String receive_result, String receive_result_reason, Map<String, String> message, Map<String, String> custom) {

        ReqReceiveMessage request = new ReqReceiveMessage();
        request.header.if_no = "IF-NPS-522";
        request.header.ver = "5.0";
        request.header.sender_name = sender_name;
        request.header.response_format = "json";
        request.header.sender = "STB";
        request.header.receiver = "NPS";

        request.body.receive_deviceid = receive_deviceid;
        request.body.receive_result = receive_result;
        request.body.receive_result_reason = receive_result_reason;
        request.body.message = message;
        request.body.custom_param = custom;

        return request;
    }

    public static ReqPairingList buildReqPairingList(String sender_name, String pairing_deviceid,
                                                     String pairing_device_type, String service_type, Map<String, String> custom) {

        ReqPairingList request = new ReqPairingList();
        request.header.if_no = "IF-NPS-532";
        request.header.ver = "5.0";
        request.header.sender_name = sender_name;
        request.header.response_format = "json";
        request.header.sender = "STB";
        request.header.receiver = "NPS";

        request.body.pairing_deviceid = pairing_deviceid;
        request.body.pairing_device_type = pairing_device_type;
        request.body.retrieve_service_type = service_type;
        request.body.custom_param = custom;

        return request;
    }

    public static ReqPairingStatus buildReqPairingStatus(String sender_name, String pairing_deviceid,
                                                         String pairing_device_type, Map<String, String> custom) {

        ReqPairingStatus request = new ReqPairingStatus();
        request.header.if_no = "IF-NPS-533";
        request.header.ver = "5.0";
        request.header.sender_name = sender_name;
        request.header.response_format = "json";
        request.header.sender = "STB";
        request.header.receiver = "NPS";

        // HostDevice excludes (pairingid/userid/servicetype)
        request.body.pairing_deviceid = pairing_deviceid;
        request.body.pairing_device_type = pairing_device_type;
        request.body.custom_param = custom;

        return request;
    }

    public static ReqUnPairing buildReqUnPairing(String sender_name, String pairing_deviceid,
                                                 String pairing_device_type, String pairingid, String service_type, Map<String, String> custom) {

        ReqUnPairing request = new ReqUnPairing();
        request.header.if_no = "IF-NPS-534";
        request.header.ver = "5.0";
        request.header.sender_name = sender_name;
        request.header.response_format = "json";
        request.header.sender = "STB";
        request.header.receiver = "NPS";

        request.body.pairing_deviceid = pairing_deviceid;
        request.body.pairing_device_type = pairing_device_type;
        request.body.pairingid = pairingid;
        request.body.service_type = service_type;
        request.body.custom_param = custom;

        return request;
    }

    public static ReqUpdateAgreement buildReqUpdateAgreement(String sender_name, String pairing_deviceid,
                                                             String pairing_device_type, String pairingid, String service_type, Map<String, String> agreement, Map<String, String> custom) {

        ReqUpdateAgreement request = new ReqUpdateAgreement();
        request.header.if_no = "IF-NPS-535";
        request.header.ver = "5.0";
        request.header.sender_name = sender_name;
        request.header.response_format = "json";
        request.header.sender = "STB";
        request.header.receiver = "NPS";

        request.body.pairing_deviceid = pairing_deviceid;
        request.body.pairing_device_type = pairing_device_type;
        request.body.pairingid = pairingid;
        request.body.service_type = service_type;
        request.body.agreement = agreement;
        request.body.custom_param = custom;

        return request;
    }

    public static ReqAgreement buildReqAgreement(String sender_name, String pairing_deviceid,
                                                 String pairing_device_type, String pairingid, String service_type, Map<String, String> custom) {

        ReqAgreement request = new ReqAgreement();
        request.header.if_no = "IF-NPS-536";
        request.header.ver = "5.0";
        request.header.sender_name = sender_name;
        request.header.response_format = "json";
        request.header.sender = "STB";
        request.header.receiver = "NPS";

        request.body.pairing_deviceid = pairing_deviceid;
        request.body.pairing_device_type = pairing_device_type;
        request.body.pairingid = pairingid;
        request.body.service_type = service_type;
        request.body.custom_param = custom;

        return request;
    }

    public static ReqSetRepresent buildReqSetRepresent(String sender_name, String pairing_deviceid,
                                                       String pairing_device_type, String pairingid, String service_type, Map<String, String> custom, String represent_flag) {

        ReqSetRepresent request = new ReqSetRepresent();
        request.header.if_no = "IF-NPS-537";
        request.header.ver = "5.0";
        request.header.sender_name = sender_name;
        request.header.response_format = "json";
        request.header.sender = "STB";
        request.header.receiver = "NPS";

        request.body.pairing_deviceid = pairing_deviceid;
        request.body.pairing_device_type = pairing_device_type;
        request.body.pairingid = pairingid;
        request.body.service_type = service_type;
        request.body.represent_flag = represent_flag;
        request.body.custom_param = custom;

        return request;
    }

    public static ReqRepresent buildReqRepresent(String sender_name, String pairing_deviceid,
                                                 String pairing_device_type, String pairingid, String service_type, Map<String, String> custom, String represent_flag) {

        ReqRepresent request = new ReqRepresent();
        request.header.if_no = "IF-NPS-538";
        request.header.ver = "5.0";
        request.header.sender_name = sender_name;
        request.header.response_format = "json";
        request.header.sender = "STB";
        request.header.receiver = "NPS";

        request.body.pairing_deviceid = pairing_deviceid;
        request.body.pairing_device_type = pairing_device_type;
        request.body.pairingid = pairingid;
        request.body.service_type = service_type;
        request.body.represent_flag = represent_flag;
        request.body.custom_param = custom;

        return request;
    }

}
