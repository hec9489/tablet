package com.skb.btv.smartrcuagent.message;

import java.util.HashMap;

import android.os.Parcel;
import android.os.Parcelable;

public class ResponseUpdateAgreement implements Parcelable {

    public String ResultCode;
    public String ResultPhrase;
    public String PairingID;
    public String ServiceType;

    public static final Creator<ResponseUpdateAgreement> CREATOR = new Creator<ResponseUpdateAgreement>() {
        public ResponseUpdateAgreement createFromParcel(Parcel in) {
            return new ResponseUpdateAgreement(in);
        }

        public ResponseUpdateAgreement[] newArray(int size) {
            return new ResponseUpdateAgreement[size];
        }
    };

    public ResponseUpdateAgreement() {
    }

    public ResponseUpdateAgreement(HashMap<String, String> hm) {
        PairingID = hm.get("PairingID");
        ServiceType = hm.get("ServiceType");
        ResultCode = hm.get("ResultCode");
        ResultPhrase = hm.get("ResultPhrase");
    }

    private ResponseUpdateAgreement(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel out, int arg1) {
        out.writeString(PairingID);
        out.writeString(ServiceType);
        out.writeString(ResultCode);
        out.writeString(ResultPhrase);
    }

    public void readFromParcel(Parcel in) {
        PairingID = in.readString();
        ServiceType = in.readString();
        ResultCode = in.readString();
        ResultPhrase = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "[ResponseUpdateAgreement (PairingID=" + PairingID + ", ServiceType=" + ServiceType
                + ", ResultCode=" + ResultCode + ", ResultPhrase=" + ResultPhrase + ")]";
    }

}
