package com.skb.btv.smartrcuagent.message;

import java.util.HashMap;

import android.os.Parcel;
import android.os.Parcelable;

public class ResponseUnPairing implements Parcelable {
    public String ResultCode;
    public String ResultPhrase;

    public static final Creator<ResponseUnPairing> CREATOR = new Creator<ResponseUnPairing>() {
        public ResponseUnPairing createFromParcel(Parcel in) {
            return new ResponseUnPairing(in);
        }

        public ResponseUnPairing[] newArray(int size) {
            return new ResponseUnPairing[size];
        }
    };

    public ResponseUnPairing() {
    }

    public ResponseUnPairing(HashMap<String, String> hm) {
        ResultCode = hm.get("ResultCode");
        ResultPhrase = hm.get("ResultPhrase");
    }

    private ResponseUnPairing(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    public void readFromParcel(Parcel in) {
        ResultCode = in.readString();
        ResultPhrase = in.readString();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        // TODO Auto-generated method stub
        out.writeString(ResultCode);
        out.writeString(ResultPhrase);
    }

    @Override
    public String toString() {
        return "[ResponsePairingStatus (" + "ResultCode=" + ResultCode + ", ResultPhrase=" + ResultPhrase + ")]";
    }
}
