package com.skb.btv.smartrcuagent.event;

public class EventManager {

    private static EventManager mInstance;

    private Listener mListener;

    public static interface Listener {
        public void onEventReceived(Event event);
    }

    private EventManager() {

    }

    public synchronized static EventManager getInstance() {
        if (mInstance == null) {
            mInstance = new EventManager();
        }
        return mInstance;
    }

    public Listener getListener() {
        return mListener;
    }

    public void setListener(Listener listener) {
        mListener = listener;
        native_setListener(listener);
    }

    private native void native_setListener(Listener listener);

}
