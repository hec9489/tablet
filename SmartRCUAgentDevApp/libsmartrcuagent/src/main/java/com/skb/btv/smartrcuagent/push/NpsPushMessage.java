package com.skb.btv.smartrcuagent.push;

public class NpsPushMessage {
	private String if_no;
	private String nps_ip;
	private String nps_port;
	private String stb_id;
	private String stmn_nm;
	private String PairingID;
	
	public String get_if_no() {
		return if_no;
	}
	public void set_if_no(String if_no) {
		this.if_no = if_no;
	}
	public String get_nps_ip() {
		return nps_ip;
	}
	public void set_nps_ip(String nps_ip) {
		this.nps_ip = nps_ip;
	}
	public String get_nps_port() {
		return nps_port;
	}
	public void set_nps_port(String nps_port) {
		this.nps_port = nps_port;
	}
	public String get_stb_id() {
		return stb_id;
	}
	public void set_stb_id(String stb_id) {
		this.stb_id = stb_id;
	}
	public String get_stmn_nm() {
		return stmn_nm;
	}
	public void set_stmn_nm(String stmn_nm) {
		this.stmn_nm = stmn_nm;
	}
	public String get_PairingID() {
		return PairingID;
	}
	public void set_PairingID(String pairingID) {
		PairingID = pairingID;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return  "\n"+ "if_no : " + if_no +"\n" + "nps_ip : " + nps_ip + "\n" 
				+ "nps_port : " + nps_port + "\n" + "stb_id : " + stb_id + "\n"
				+ "stmn_nm : " + stmn_nm + "\n" + "PairingID : " + PairingID + "\n";
	}
	
}
