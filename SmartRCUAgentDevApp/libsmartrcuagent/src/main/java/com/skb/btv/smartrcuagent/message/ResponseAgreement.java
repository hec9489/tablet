package com.skb.btv.smartrcuagent.message;

import java.util.HashMap;

import android.os.Parcel;
import android.os.Parcelable;

public class ResponseAgreement implements Parcelable {
    public String ResultCode;
    public String ResultPhrase;
    public String PairingID;
    public String ServiceType;
    public String PurchaseList;
    public String UnlimitedVod;

    public static final Creator<ResponseAgreement> CREATOR = new Creator<ResponseAgreement>() {
        public ResponseAgreement createFromParcel(Parcel in) {
            return new ResponseAgreement(in);
        }

        public ResponseAgreement[] newArray(int size) {
            return new ResponseAgreement[size];
        }
    };

    public ResponseAgreement() {
    }

    public ResponseAgreement(HashMap<String, String> hm) {
        PairingID = hm.get("PairingID");
        ServiceType = hm.get("ServiceType");
        PurchaseList = hm.get("PurchaseList");
        UnlimitedVod = hm.get("UnlimitedVod");
        ResultCode = hm.get("ResultCode");
        ResultPhrase = hm.get("ResultPhrase");
    }

    private ResponseAgreement(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel out, int arg1) {
        out.writeString(PairingID);
        out.writeString(ServiceType);
        out.writeString(PurchaseList);
        out.writeString(UnlimitedVod);
        out.writeString(ResultCode);
        out.writeString(ResultPhrase);
    }

    public void readFromParcel(Parcel in) {
        PairingID = in.readString();
        ServiceType = in.readString();
        PurchaseList = in.readString();
        UnlimitedVod = in.readString();
        ResultCode = in.readString();
        ResultPhrase = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "[ResponseAgreement (PairingID=" + PairingID + ", ServiceType=" + ServiceType
                + ", PurchaseList=" + PurchaseList + ", UnlimitedVod=" + UnlimitedVod
                + ", ResultCode=" + ResultCode + ", ResultPhrase=" + ResultPhrase + ")]";
    }
}
