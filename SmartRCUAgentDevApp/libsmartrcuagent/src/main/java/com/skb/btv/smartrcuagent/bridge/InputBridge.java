/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.skb.btv.smartrcuagent.bridge;

/**
 * Input bridge interface for handling input events.
 *
 * @author dziekon@google.com (Tomek Dziekonski)
 */
public interface InputBridge {
    // NOTE(mikey): constants are copied from the private (hidden) Android class
    // RawKeyEvent.java, which are themselves simply shadowing values from
    // <linux/input.h>

    // Bitmasks for the `classes` argument of addDevice. These match the values
    // in RawInputEvent, but are unrelated.

    public static final int CLASS_KEYBOARD = 0x00000001;
    public static final int CLASS_ALPHAKEY = 0x00000002;
    public static final int CLASS_TOUCHSCREEN = 0x00000004;
    public static final int CLASS_TRACKBALL = 0x00000008;
    public static final int CLASS_MOUSE = 0x00000010;

    // Event types.

    public static final int EV_SYN = 0x00;
    public static final int EV_KEY = 0x01;
    public static final int EV_REL = 0x02;
    public static final int EV_ABS = 0x03;
    public static final int EV_MSC = 0x04;
    public static final int EV_SW = 0x05;
    public static final int EV_LED = 0x11;
    public static final int EV_SND = 0x12;
    public static final int EV_REP = 0x14;
    public static final int EV_FF = 0x15;
    public static final int EV_PWR = 0x16;
    public static final int EV_FF_STATUS = 0x17;

    // Relative axes (EV_REL) scan codes.

    public static final int REL_X = 0x00;
    public static final int REL_Y = 0x01;
    public static final int REL_Z = 0x02;
    public static final int REL_RX = 0x03;
    public static final int REL_RY = 0x04;
    public static final int REL_RZ = 0x05;
    public static final int REL_HWHEEL = 0x06;
    public static final int REL_DIAL = 0x07;
    public static final int REL_WHEEL = 0x08;
    public static final int REL_MISC = 0x09;
    public static final int REL_MAX = 0x0f;

    // Action by the buttons or touches.

    public static final int ACTION_UP = 0;
    public static final int ACTION_DOWN = 1;


    // Touch pressure scale factor.
    // Defined attouch.pressure.scale field in touch screen idc file
    // TODO(jaewan): Obtain this from the system configuration

    public static final int TOUCH_PRESSURE_SCALE = 80;

    /**
     * Sends a new input event to the uinput layer.
     *
     * @param type  the uinput event type
     * @param code  the uinput event code
     * @param value the uinput event value
     */
    public void sendEvent(int type, int code, int value);

    /**
     * Sends mouse movement event to the uinput layer.
     *
     * @param deltaX the X delta of the movement
     * @param deltaY the Y delta of the movement
     */
    public void sendMouseMovement(int deltaX, int deltaY);

    /**
     * Sends touch event to the uinput layer.
     *
     * @param count    number of pointers in a touch event
     * @param id       ID of each pointer
     * @param action   action for each pointer.
     * @param x        X coordinates of each pointer
     * @param y        Y coordinates of each pointer
     * @param pressure pressure of each pointer
     */
    public void sendTouchEvent(int count, int id[], int action[],
                               int x[], int y[], int pressure[]);
}
