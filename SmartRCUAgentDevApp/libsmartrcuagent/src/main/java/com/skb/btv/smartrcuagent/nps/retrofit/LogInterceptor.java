package com.skb.btv.smartrcuagent.nps.retrofit;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

import static com.skb.btv.domain.SmartRcuAgentEnvironment.RCU_VERSION;

public class LogInterceptor implements Interceptor {
    final static String TAG = "TVSmartRCUAgentAPK " + RCU_VERSION;

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();

        long t1 = System.nanoTime();
        Log.d(TAG, "----------------------------------------------->>");
        Log.d(TAG, String.format("[STB =>> NPS] : Sending request %s ", request.url()));
        Buffer requestBuffer = new Buffer();
        request.body().writeTo(requestBuffer);
        Log.d(TAG, requestBuffer.readUtf8());
        Log.d(TAG, "----------------------------------------------->>");

        Response response = chain.proceed(request);

        long t2 = System.nanoTime();
        Log.d(TAG, "<<-----------------------------------------------");
        Log.d(TAG, String.format("[NPS =>> STB] : Received response for %s in %.1fms%n%s", response.request().url(), (t2 - t1) / 1e6d, response.headers()));

        MediaType contentType = response.body().contentType();
        String content = response.body().string();

        Log.d(TAG, content);
        Log.d(TAG, "<<-----------------------------------------------");
        ResponseBody wrappedBody = ResponseBody.create(contentType, content);
        return response.newBuilder().body(wrappedBody).build();

    }


}
