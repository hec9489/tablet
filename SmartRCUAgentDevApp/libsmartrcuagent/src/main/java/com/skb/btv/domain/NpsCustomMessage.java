package com.skb.btv.domain;

public class NpsCustomMessage {
    private String key;
    private String value;

    public NpsCustomMessage() {
        super();
    }

    public NpsCustomMessage(String key, String value) {
        super();
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
