package com.skb.btv.smartrcuagent.nps;

import android.app.Instrumentation;
import android.os.Message;

import com.skb.btv.smartrcuagent.NpsObject;
import com.skb.btv.smartrcuagent.ResponseCallback;
import com.google.anymote.Key.Code;
import com.skb.btv.smartrcuagent.SmartRCUAgentServiceImpl;
import com.skb.btv.smartrcuagent.nps.domain.ResReceiveMessage;
import com.skb.btv.smartrcuagent.nps.domain.SKBRemoconSvcBuilder;
import com.skb.btv.smartrcuagent.nps.retrofit.NpsService;
import com.skb.btv.smartrcuagent.nps.retrofit.NpsServiceBuilder;
import com.skb.btv.smartrcuagent.util.AES256Util;
import com.skb.btv.smartrcuagent.util.ALog;
import com.skb.btv.domain.SmartRcuAgentEnvironment;

import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NpsControlThread extends Thread {
    private static NpsControlThread mInstance;
    private NpsService npsService;
    private boolean isRunning;
    private int seqCount = 0;
    private int retryCount = 0;

    private String receive_result;
    private String receive_result_reason;
    private String currentIpAddr;
    private Map<String, String> message;

    public NpsControlThread() {

    }

    public synchronized static NpsControlThread getInstance() {
        if (mInstance == null) {
            mInstance = new NpsControlThread();
        }
        return mInstance;
    }

    public synchronized void npsStart() {
        ALog.d("npsStart()");
        npsService = NpsServiceBuilder.buildNpsServiceWithEndPointAndTimeout(
                NpsAddressReader.getInstance().getNpsAddress(), NpsAddressReader.getInstance().getNpsPort(), 245,
                TimeUnit.SECONDS);
        currentIpAddr = NpsAddressReader.getInstance().getNpsAddress();
        receive_result = "0";
        receive_result_reason = "Success";
        message = new HashMap<String, String>();
        isRunning = true;
        retryCount = 0;
        start();
    }

    public synchronized void npsStop() {
        ALog.d("npsStop()");
        if (reqReceiveMessage != null) {
            reqReceiveMessage.cancel();
        }
        retryCount = 0;
        isRunning = false;
        SmartRcuAgentEnvironment.isAbort = false;
        interrupt();
        //SmartRCUAgent.smartRCUAgent.changeNpsAddress();
        mInstance = null;
    }

    public synchronized void unlock() {
        notify();
    }

    public synchronized void sendRequestData(String receive_result, String receive_result_reason,
                                             Map<String, String> message) {

        this.receive_result = receive_result;
        this.receive_result_reason = receive_result_reason;
        this.message = message;

        notify();

    }

    public synchronized boolean isRunning() {
        return isRunning;
    }

    @Override
    public void run() {
        while (isRunning) {
            try {

                if (currentIpAddr != null) {
                    if (!currentIpAddr.equals(NpsAddressReader.getInstance().getNpsAddress())) {
                        ALog.d("NPS Addr Change : " + currentIpAddr + " => "
                                + NpsAddressReader.getInstance().getNpsAddress());

                        npsService = NpsServiceBuilder.buildNpsServiceWithEndPointAndTimeout(
                                NpsAddressReader.getInstance().getNpsAddress(), NpsAddressReader.getInstance().getNpsPort(), 245,
                                TimeUnit.SECONDS);
                        currentIpAddr = NpsAddressReader.getInstance().getNpsAddress();
                    }
                }

                reqReceiveMessage();
            } catch (SocketTimeoutException e) {
                ALog.d("SocketTimeoutException");
                ALog.printStackTrace(e);
            } catch (IOException e) {
                ALog.d("IOException");
                ALog.printStackTrace(e);
            }

            if (SmartRcuAgentEnvironment.isAbort) {
                ALog.e("Abort is true STOP!! npsStop");
                SmartRcuAgentEnvironment.isAbort = false;
                npsStop();
                break;
            }

            ALog.d("Next Event Request!! - isRunning :" + isRunning);
        }
    }

    Call<ResReceiveMessage> reqReceiveMessage = null;

    synchronized void reqReceiveMessage() throws SocketTimeoutException, IOException {

        try {
            seqCount++;
            ALog.d("reqReceiveMessage : " + seqCount);

            reqReceiveMessage = npsService.reqReceiveMessage(
                    SKBRemoconSvcBuilder.buildReqReceiveMessage(SmartRcuAgentEnvironment.getBySenderName(),
                            SmartRcuAgentEnvironment.stbId, receive_result, receive_result_reason, message, null));
            ALog.d("reqReceiveMessage : " + reqReceiveMessage.request().toString());
            reqReceiveMessage.enqueue(new Callback<ResReceiveMessage>() {

                @Override
                public void onFailure(Call<ResReceiveMessage> arg0, Throwable arg1) {
                    ALog.d("onFailure : " + arg1.getMessage());
                    if (arg1.getMessage() == null) {
                        ALog.e("onFailure - Connection Time Out npsStop");
                        npsStop();
                    } else {
                        if (retryCount < 3) {
                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e) {
                                // TODO Auto-generated catch block
                                ALog.d("onFailure - InterruptedException");
                            }
                            unlock();
                            retryCount++;
                            ALog.d("Retry Connect Server : " + retryCount);
                        } else {
                            ALog.e("Retry Connect Server : " + retryCount + " npsStop");
                            npsStop();
                        }
                    }
                }

                @Override
                public void onResponse(Call<ResReceiveMessage> arg0, Response<ResReceiveMessage> arg1) {
                    ResReceiveMessage response = arg1.body();
                    ALog.d("onResponse : " + arg1.raw().code());
                    retryCount = 0;
                    // TODO Abort 가 1일 경우 Message 전달 후 다음번 Request 는 안함
                    if (response != null) {
                        String resultCode = response.header.get_result();
                        if (resultCode != null) {
                            if (resultCode.equals("0000")) {
                                String abort = response.body.get_abort();
                                if (abort != null) {
                                    if (abort.equals("1")) {
                                        ALog.e("Abort == 1  do not request isAbort");
                                        SmartRcuAgentEnvironment.isAbort = true;
                                    }
                                }

                                String ipAddr = response.body.get_ip();
                                String port = "9090";
                                if (response.body.get_port() != null) {
                                    port = response.body.get_port();
                                }

                                if (ipAddr != null) {
                                    if (!ipAddr.equals(currentIpAddr)) {
                                        NpsAddressReader.getInstance().setNpsAddress(ipAddr);
                                        NpsAddressReader.getInstance().setNpsPort(Integer.parseInt(port));
                                    }
                                }

                                String SendDeviceID = response.body.get_send_deviceid();
                                if (SendDeviceID == null || SendDeviceID.equals("")) {
                                    ALog.d("No Message by Server");
                                    HashMap<String, String> emptyMsg = new HashMap<String, String>();
                                    sendRequestData("-1", "Success", emptyMsg);
                                } else {
                                    String PairingID = response.body.get_pairingid();
                                    String ServiceType = response.body.get_service_type();
                                    String CtrlType = response.body.get_message().get("CtrlType");
                                    String CtrlValue = response.body.get_message().get("CtrlValue");
                                    String RCStatusQuery = response.body.get_message().get("RCStatusQuery");
                                    ALog.d("#########################NpsString - CtrlType:" + CtrlType + ", CtrlValue:" + CtrlValue);
                                    final Code code = NpsCtrlConvertUtil.getKeyCodeByNpsString(CtrlType, CtrlValue);
                                    ALog.d("#########################NpsString - code:" + code);
                                    if (code != null) {
                                        //SmartRCUAgent.smartRCUAgent.sendKeyEventWithDownUpAction(code.getNumber());
                                        new Thread(new Runnable() {
                                            public void run() {
                                                ALog.e("#########################NpsString - code.getNumber:" + code.getNumber());
                                                new Instrumentation().sendKeyDownUpSync(code.getNumber());
                                            }
                                        }).start();
                                    } else {

                                        if (CtrlType != null) {
                                            if (CtrlType.equals("SendMsg")) {
                                                CtrlValue = AES256Util.decryptAES256(CtrlValue);
                                            }
                                        }
                                    }

                                    if (CtrlValue == null) { // listener exception mute/power/guide
                                        CtrlValue = "";
                                    }

                                    ALog.d("===== onResponseReceiveMessage =====");
                                    ALog.d("PairingID : " + PairingID + "\n"
                                            + "ServiceType : " + ServiceType + "\n"
                                            + "CtrlType : " + CtrlType + "\n"
                                            + "CtrlValue : " + CtrlValue + "\n"
                                            + "RCStatusQuery : " + RCStatusQuery + "\n");
                                    ALog.d("===== onResponseReceiveMessage =====");

                                    Message msg = SmartRCUAgentServiceImpl.mResponseHandler.obtainMessage(ResponseCallback.RESPONSE_NPS_STB_ADJUST_REQUEST);
                                    HashMap<String, String> hm = new HashMap<String, String>();
                                    hm.put("PairingID", PairingID);
                                    hm.put("ServiceType", ServiceType);
                                    hm.put("CtrlType", CtrlType);
                                    hm.put("CtrlValue", CtrlValue);
                                    hm.put("RCStatusQuery", RCStatusQuery);

                                    JSONObject jsonObject = new JSONObject(response.body.get_message());
                                    String message = jsonObject.toString();
                                    hm.put("JsonMessage", message);

                                    if (response.body.get_custom_param() != null) {
                                        JSONObject jsonObject2 = new JSONObject(response.body.get_custom_param());
                                        String custom_param = jsonObject2.toString();
                                        hm.put("CustomParam", custom_param);
                                    } else {
                                        hm.put("CustomParam", "");
                                    }

                                    msg.obj = new NpsObject(hm);

                                    SmartRCUAgentServiceImpl.mResponseHandler.sendMessage(msg);
                                }

                            } else {
                                //TODO error
                                if (resultCode.equals("1031")) {
                                    //previous message by server do not stop polling
                                } else if (resultCode.equals("1004")) {
                                    if (response.body.get_message().get("CtrlType") != null) {
                                        if (response.body.get_message().get("CtrlType").equals("NotiUnpairing")) {
                                            ALog.d("=====>> NotiUnpairing value is 0");
                                            Message msg = SmartRCUAgentServiceImpl.mResponseHandler.obtainMessage(ResponseCallback.RESPONSE_NPS_STB_ADJUST_REQUEST);
                                            HashMap<String, String> hm = new HashMap<String, String>();
                                            hm.put("PairingID", response.body.get_pairingid());
                                            hm.put("ServiceType", response.body.get_service_type());
                                            hm.put("CtrlType", response.body.get_message().get("CtrlType"));
                                            hm.put("CtrlValue", response.body.get_message().get("CtrlValue"));
                                            hm.put("RCStatusQuery", response.body.get_message().get("RCStatusQuery"));
                                            msg.obj = new NpsObject(hm);

                                            SmartRCUAgentServiceImpl.mResponseHandler.sendMessage(msg);
                                        }
                                    }

                                    ALog.e("result error is Abort 1004 isAbort");
                                    SmartRcuAgentEnvironment.isAbort = true;
                                    unlock();
                                } else {
                                    ALog.e("result error is Abort etc isAbort resultCode : " + resultCode);
                                    SmartRcuAgentEnvironment.isAbort = true;
                                    unlock();
                                }
                            }
                        } else {
                            //TODO error
                            ALog.e("returnCode error is Abort isAbort");
                            SmartRcuAgentEnvironment.isAbort = true;
                            unlock();
                        }
                    } else {
                        //TODO error
                        ALog.e("response error is Abort isAbort");
                        SmartRcuAgentEnvironment.isAbort = true;
                        unlock();
                    }
                }
            });

            ALog.e("reqReceiveMessage call enqueue");
            wait(250000);
            ALog.e("reqReceiveMessage not call callback");
        } catch (InterruptedException e) {
            ALog.e("InterruptedException npsStop");
            npsStop();
        }
        ALog.d("reqReceiveMessage end");
    }

}
