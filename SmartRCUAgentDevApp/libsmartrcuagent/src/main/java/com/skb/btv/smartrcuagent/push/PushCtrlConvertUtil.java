package com.skb.btv.smartrcuagent.push;

import com.google.anymote.Key;
import com.google.anymote.Key.Code;

public class PushCtrlConvertUtil {

	/**
	 * @param ctrlType
	 *            PushServer 에서 받은 이벤트 문자열
	 * @return 키 이벤트 인경우 해당하는 Code 를 아닌 경우 NULL 을 반환
	 */
	//PowerCtrl,Guide,Mute 제외 ON,OFF 값 체크로 인해
	public static Code getKeyCodeByPushString(String ctrlType, String ctrlValue) {
		if ("Down".equals(ctrlType)) {
			return Key.Code.KEYCODE_DPAD_DOWN;
		} else if ("Up".equals(ctrlType)) {
			return Key.Code.KEYCODE_DPAD_UP;
		} else if ("Right".equals(ctrlType)) {
			return Key.Code.KEYCODE_DPAD_RIGHT;
		} else if ("Left".equals(ctrlType)) {
			return Key.Code.KEYCODE_DPAD_LEFT;
		} else if ("Ok".equals(ctrlType)) {
			return Key.Code.KEYCODE_DPAD_CENTER;
		} else if ("OK".equals(ctrlType)) {
			return Key.Code.KEYCODE_DPAD_CENTER;
		}else if ("ButtonCancel".equals(ctrlType)) {
			return Key.Code.KEYCODE_BACK;
		} else if ("CHUp".equals(ctrlType)) {
			return Key.Code.KEYCODE_CHANNEL_UP;
		} else if ("CHDown".equals(ctrlType)) {
			return Key.Code.KEYCODE_CHANNEL_DOWN;
		} else if ("VOLUp".equals(ctrlType)) {
			return Key.Code.KEYCODE_VOLUME_UP;
		} else if ("VOLDown".equals(ctrlType)) {
			return Key.Code.KEYCODE_VOLUME_DOWN;
		} else if ("ButtonRed".equals(ctrlType)) {
			return Key.Code.KEYCODE_PROG_RED;
		} else if ("ButtonGreen".equals(ctrlType)) {
			return Key.Code.KEYCODE_PROG_GREEN;
		} else if ("ButtonYellow".equals(ctrlType)) {
			return Key.Code.KEYCODE_PROG_YELLOW;
		} else if ("ButtonBlue".equals(ctrlType)) {
			return Key.Code.KEYCODE_PROG_BLUE;
		} else if ("ButtonHome".equals(ctrlType)) {
			return Key.Code.KEYCODE_HOME;
		} else if ("ButtonMenu".equals(ctrlType)) {
			return Key.Code.KEYCODE_MENU;
		} else if ("PIP".equals(ctrlType)) {
			return Key.Code.KEYCODE_F8;
		} else if ("Quit".equals(ctrlType)){
			return Key.Code.KEYCODE_F12;
		} else if("PlayCtrl".equals(ctrlType)) {
			if(ctrlValue.equals("REW")){
				return Key.Code.KEYCODE_MEDIA_REWIND;
			}else if(ctrlValue.equals("Stop")){
				return Key.Code.KEYCODE_MEDIA_STOP;
			}else if(ctrlValue.equals("Play")){
				return Key.Code.KEYCODE_MEDIA_PLAY_PAUSE;
			}else if(ctrlValue.equals("FF")){
				return Key.Code.KEYCODE_MEDIA_FAST_FORWARD;
			}else if(ctrlValue.equals("Prev")){
				return Key.Code.KEYCODE_MEDIA_PREVIOUS;
			}else if(ctrlValue.equals("Next")){
				return Key.Code.KEYCODE_MEDIA_NEXT;
			}else if(ctrlValue.equals("Pause")){
				return Key.Code.KEYCODE_MEDIA_PLAY_PAUSE;
			}else{
				return null;
			}
		} else {
			return null;
		}
	}
}
