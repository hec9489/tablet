package com.skb.btv.smartrcuagent.push;

public class PushMessage {
	
	private String text;
	private String ctrl_value;
	private String ctrl_type;
	private String msg_code;
	private String sendr_date;
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getCtrlValue() {
		return ctrl_value;
	}
	public void setCtrlValue(String ctrl_value) {
		this.ctrl_value = ctrl_value;
	}
	public String getCtrlType() {
		return ctrl_type;
	}
	public void setCtrltype(String ctrl_type) {
		this.ctrl_type = ctrl_type;
	}
	public String getMsgCode() {
		return msg_code;
	}
	public void setMsgCode(String msg_code) {
		this.msg_code = msg_code;
	}
	public String getSendrDate() {
		return sendr_date;
	}
	public void setSenderDate(String sendr_date) {
		this.sendr_date = sendr_date;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "PushMessage : " + "text : " + text + " ctrl_value : " + ctrl_value + " ctrl_type : " + ctrl_type
				+ " msg_code : " + msg_code + " sendr_date : " + sendr_date;
	}
	
	
}
