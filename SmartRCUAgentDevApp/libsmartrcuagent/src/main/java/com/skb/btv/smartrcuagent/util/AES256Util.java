package com.skb.btv.smartrcuagent.util;

import org.apache.commons.codec.CharEncoding;
import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES256Util {

    private final static String KEY = "R3WoPEtbkEIhPQqrKl37fQEsfZAYpPMk";
    private final static String IV = "8C7BFE4A1116E5E5";

    private static AlgorithmParameterSpec ivSpec;
    private static SecretKeySpec serectKey;
    private static Cipher encyrptCipher = null;
    private static Cipher decryptCipher = null;

    static {
        initEncryption();
    }

    private static void initEncryption() {

        ALog.i("encryption init");
        byte[] key128Data = null;
        byte[] key256Data = null;

        try {
            key128Data = IV.substring(0, 16).getBytes();
            key256Data = KEY.substring(0, 32).getBytes(CharEncoding.UTF_8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        ivSpec = new IvParameterSpec(key128Data);
        serectKey = new SecretKeySpec(key256Data, "AES");
    }

    public static String encryptAES256(String string) {


        try {
            if (encyrptCipher == null) {
                ALog.d("encyrptCipher == null");
                initEncryption();
                encyrptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                encyrptCipher.init(Cipher.ENCRYPT_MODE, serectKey, ivSpec);
            }
            byte[] encrypted = encyrptCipher.doFinal(string.getBytes(CharEncoding.UTF_8));
            byte[] base64Encoded = Base64.encodeBase64(encrypted);

            String result = new String(base64Encoded, CharEncoding.UTF_8);
            return result;

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }

        return null;

    }

    public static String decryptAES256(String string) {
        try {
            if (decryptCipher == null) {
                ALog.d("decryptCipher == null");
                initEncryption();
                decryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                decryptCipher.init(Cipher.DECRYPT_MODE, serectKey, ivSpec);
            }

            byte[] base64Decoded = Base64.decodeBase64(string.getBytes(CharEncoding.UTF_8));
            byte[] decrypted = decryptCipher.doFinal(base64Decoded);

            String result = new String(decrypted, CharEncoding.UTF_8);
            return result;

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }

        return null;

    }
}
