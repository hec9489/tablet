package com.skb.btv.smartrcuagent.util;

import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class HttpManager {
    public static final int DEFAULT_CONNECTION_TIMEOUT = 30000;

    private static HttpManager instance;

    private HttpManager() {

    }

    public synchronized static HttpManager getInstance() {
        if (instance == null) {
            instance = new HttpManager();
        }
        return instance;
    }

    private String inputStreamToString(InputStream inputStream)
            throws IOException {
        StringBuffer stringBuffer = new StringBuffer();
        byte[] buffer = new byte[4096];

        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            stringBuffer.append(new String(buffer, 0, length));
        }

        return stringBuffer.toString();
    }

    private InputStream getInputStreamFromHttpPost(HttpClient client,
                                                   HttpPost post) throws IOException {
        HttpResponse response = client.execute(post);

        final int statusCode = response.getStatusLine().getStatusCode();

        if (statusCode != HttpStatus.SC_OK) {
            throw new IOException("HttpStatus error. HttpStatusCode: "
                    + statusCode);
        }
        HttpEntity httpEntity = response.getEntity();
        return httpEntity.getContent();
    }

    private HttpClient getDefaultClient(Integer timeout) {
        HttpClient httpclient = new DefaultHttpClient();
        httpclient.getParams().setParameter("http.protocol.expect-continue",
                false);
        httpclient.getParams().setParameter("http.connection.timeout", timeout);
        httpclient.getParams().setParameter("http.socket.timeout", timeout);
        return httpclient;
    }

    private HttpPost getDefaultTextHttpPost(String url,
                                            ArrayList<? extends NameValuePair> params)
            throws UnsupportedEncodingException {
        HttpPost httpPost = new HttpPost(url);
        if (params != null && params.size() > 0) {
            UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,
                    HTTP.UTF_8);
            httpPost.setEntity(ent);
        }
        return httpPost;
    }

    private HttpPost setHttpPostToJson(HttpPost httpPost) {
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        return httpPost;
    }

    public InputStream getInputStreamByPost(String url, Integer timeout,
                                            ArrayList<? extends NameValuePair> params) throws IOException {

        HttpClient client = getDefaultClient(timeout);
        HttpPost post = getDefaultTextHttpPost(url, params);
        InputStream inputStream = getInputStreamFromHttpPost(client, post);
        return inputStream;
    }

    public InputStream getInputStreamByJsonPost(String url, Integer timeout,
                                                ArrayList<? extends NameValuePair> params) throws IOException {

        HttpClient client = getDefaultClient(timeout);
        HttpPost post = getDefaultTextHttpPost(url, params);
        post = setHttpPostToJson(post);
        InputStream inputStream = getInputStreamFromHttpPost(client, post);
        return inputStream;
    }

    public String getStringByPost(String url,
                                  ArrayList<? extends NameValuePair> params, Integer timeout)
            throws IOException {
        InputStream inputStream = getInputStreamByPost(url, timeout, params);
        return inputStreamToString(inputStream);
    }

    public String getStringByPost(String url, Integer timeout,
                                  BasicNameValuePair... params) throws IOException {
        ArrayList<BasicNameValuePair> paramList = new ArrayList<BasicNameValuePair>();
        for (BasicNameValuePair param : params) {
            paramList.add(param);
        }
        return getStringByPost(url, paramList, timeout);
    }

    public Object getObjectFromServerByGson(String url, Integer timeout,
                                            Object param, Class<?> classType) throws IOException {
        final Gson gson = new Gson();
        String paramString = gson.toJson(param);
        ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

        params.add(new BasicNameValuePair("param", paramString));
        InputStream inputStream = getInputStreamByPost(url, timeout, params);
        String resultString = inputStreamToString(inputStream);
        Object result = gson.fromJson(resultString, classType);
        return result;
    }

}
