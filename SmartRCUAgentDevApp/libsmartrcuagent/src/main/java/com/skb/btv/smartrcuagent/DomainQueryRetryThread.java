package com.skb.btv.smartrcuagent;

import com.google.gson.Gson;
import com.skb.btv.smartrcuagent.nps.NpsAddressReader;
import com.skb.btv.smartrcuagent.util.ALog;
import com.skb.btv.smartrcuagent.util.DnsIpReader;

import java.io.IOException;
import java.net.UnknownHostException;

//import retrofit.RetrofitError;

public class DomainQueryRetryThread extends Thread {
	int interval = 3;
	private boolean inturrupt = false;
	Gson gson = new Gson();

	private static DomainQueryRetryThread instance;

	public synchronized static void retryStart(int interval) {
		if (instance != null) {
			if (instance.getInturrupt()) {
				ALog.d("domain query retry aleady running... >> ignore start");
				return;
			}
		}
		instance = new DomainQueryRetryThread(interval);
		instance.start();
	}

	private DomainQueryRetryThread(int interval) {
		this.interval = interval;
		if (this.interval < 3) {
			this.interval = 3;
		}
	}

	public void setInturrupt(boolean inttrupt) {
		ALog.d("retryLogic >> setInturrupt: " + inttrupt);
		this.inturrupt = inttrupt;
	}

	public boolean getInturrupt() {
		return this.inturrupt;
	}

	public boolean changeNpsAddr() {
		ALog.d("changeNpsAddr");
		String ipaddr;
		try {
			NpsAddressReader.getInstance().refresh();
			ipaddr = NpsAddressReader.getInstance().getNpsAddress();
			ipaddr = DnsIpReader.getInstance().getRealIpFromDns(ipaddr);
			NpsAddressReader.getInstance().setNpsAddress(ipaddr);
			boolean isPaired = false;

			try {
				isPaired = SmartRCUAgentServiceImpl.getInstance()
						.isReqPairingStatus();
			} catch (IOException e) {
				// TODO: handle exception
				return false;
			}
			return true;
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
			DomainStatus
					.setCurrentDomainStatus(DomainStatus.DOMAIN_STATUS_ERROR);
			return false;
		}
	}

	@Override
	public void run() {
		ALog.d("start retry logic");
		if (changeNpsAddr()) {
			// do not thing
		} else {
			try {
				for (int count = 0; count < 9; count++) {
					if (!DomainStatus.getCurrentDomainStatus().equals(
							DomainStatus.DOMAIN_STATUS_ERROR)) {
						return;
					}
					if (inturrupt) {
						break;
					}
					int sleepTime = interval;
					if (count >= 0 && count < 3) {
						sleepTime = interval;
					} else if (count >= 3 && count < 6) {
						sleepTime = interval * 2;
					} else if (count >= 6 && count < 9) {
						sleepTime = interval * 4;
					}
					for (int i = 0; i < sleepTime; i++) {
						if (inturrupt) {
							break;
						}
						sleep(1000);
					}
					if (inturrupt) {
						break;
					} else {
						ALog.d("retryLogic >> sleep " + sleepTime
								+ " second, count: " + count);
						try {
							NpsAddressReader.getInstance().refresh();
							String ipAddr = NpsAddressReader.getInstance()
									.getNpsAddress();
							ipAddr = DnsIpReader.getInstance()
									.getRealIpFromDns(ipAddr);
							
							boolean isPaired = false;

							try {
								isPaired = SmartRCUAgentServiceImpl.getInstance()
										.isReqPairingStatus();
							} catch (IOException e) {
								isPaired = false;
							}

							inturrupt = true;
							DomainStatus
									.setCurrentDomainStatus(DomainStatus.DOMAIN_STATUS_RECOVER);
						} catch (UnknownHostException e) {
							if(count >= 8){
								DomainStatus
								.setCurrentDomainStatus(DomainStatus.DOMAIN_STATUS_RECOVER);
								
							}else{
								DomainStatus
								.setCurrentDomainStatus(DomainStatus.DOMAIN_STATUS_ERROR);
							}
							ALog.printStackTrace(e);
						} 
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		super.run();
	}
}
