package com.skb.btv.smartrcuagent.message;

import java.util.HashMap;

import android.os.Parcel;
import android.os.Parcelable;

public class ResponsePairingSequence implements Parcelable {
    public String UserName;
    public String UserID;
    public String UserNumber;
    public String ServiceType;
    public String ResultCode;
    public String ResultPhrase;

    public static final Creator<ResponsePairingSequence> CREATOR = new Creator<ResponsePairingSequence>() {
        public ResponsePairingSequence createFromParcel(Parcel in) {
            return new ResponsePairingSequence(in);
        }

        public ResponsePairingSequence[] newArray(int size) {
            return new ResponsePairingSequence[size];
        }
    };

    public ResponsePairingSequence() {
    }

    public ResponsePairingSequence(HashMap<String, String> hm) {
        UserName = hm.get("UserName");
        UserID = hm.get("UserID");
        UserNumber = hm.get("UserNumber");
        ServiceType = hm.get("ServiceType");
        ResultCode = hm.get("ResultCode");
        ResultPhrase = hm.get("ResultPhrase");
    }

    private ResponsePairingSequence(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void readFromParcel(Parcel in) {
        UserName = in.readString();
        UserID = in.readString();
        UserNumber = in.readString();
        ServiceType = in.readString();
        ResultCode = in.readString();
        ResultPhrase = in.readString();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(UserName);
        out.writeString(UserID);
        out.writeString(UserNumber);
        out.writeString(ServiceType);
        out.writeString(ResultCode);
        out.writeString(ResultPhrase);
    }

    @Override
    public String toString() {
        return "[ResponsePairingSequence (" + "UserName=" + UserName + ", UserID=" + UserID
                + ", UserNumber=" + UserNumber + ", ServiceType=" + ServiceType
                + ", ResultCode=" + ResultCode + ", ResultPhrase=" + ResultPhrase + ")]";
    }


}
