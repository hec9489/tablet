package com.skb.btv.smartrcuagent.message;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

public class ResponseSetRepresent implements Parcelable {
    public String ResultCode;
    public String ResultPhrase;
    public String PairingID;
    public String ServiceType;
    public String RepresentFlag;

    public static final Creator<ResponseSetRepresent> CREATOR = new Creator<ResponseSetRepresent>() {
        public ResponseSetRepresent createFromParcel(Parcel in) {
            return new ResponseSetRepresent(in);
        }

        public ResponseSetRepresent[] newArray(int size) {
            return new ResponseSetRepresent[size];
        }
    };

    public ResponseSetRepresent() {
    }

    public ResponseSetRepresent(HashMap<String, String> hm) {
        PairingID = hm.get("PairingID");
        ServiceType = hm.get("ServiceType");
        RepresentFlag = hm.get("RepresentFlag");
        ResultCode = hm.get("ResultCode");
        ResultPhrase = hm.get("ResultPhrase");
    }

    private ResponseSetRepresent(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel out, int arg1) {
        out.writeString(PairingID);
        out.writeString(ServiceType);
        out.writeString(RepresentFlag);
        out.writeString(ResultCode);
        out.writeString(ResultPhrase);
    }

    public void readFromParcel(Parcel in) {
        PairingID = in.readString();
        ServiceType = in.readString();
        RepresentFlag = in.readString();
        ResultCode = in.readString();
        ResultPhrase = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "[ResponseAgreement (PairingID=" + PairingID + ", ServiceType=" + ServiceType
                + ", RepresentFlag=" + RepresentFlag
                + ", ResultCode=" + ResultCode + ", ResultPhrase=" + ResultPhrase + ")]";
    }
}
