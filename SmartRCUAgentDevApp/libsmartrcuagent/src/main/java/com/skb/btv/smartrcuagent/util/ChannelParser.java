package com.skb.btv.smartrcuagent.util;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ChannelParser {
    public ArrayList<Channel> parse() throws XmlPullParserException,
            IOException {
        ArrayList<Channel> channels = new ArrayList<Channel>();

        XmlPullParser xpp = XmlPullParserFactory.newInstance().newPullParser();
        String filepath = "/data/btv_home/run/channel.xml";
        xpp.setInput(new FileReader(new File(filepath)));

        int eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                String tag = xpp.getName();
                if ("channel".equals(tag)) {
                    Channel channel = new Channel();
                    int count = xpp.getAttributeCount();
                    for (int i = 0; i < count; i++) {
                        String attrName = xpp.getAttributeName(i);
                        if ("sid".equals(attrName)) {
                            channel.setSid(xpp.getAttributeValue(i));
                        } else if ("ch".equals(attrName)) {
                            channel.setCh(xpp.getAttributeValue(i));
                        } else if ("cname".equals(attrName)) {
                            channel.setCname(xpp.getAttributeValue(i));
                        }
                    }
                    channels.add(channel);
                }
            }
            xpp.next();
            eventType = xpp.getEventType();
        }
        return channels;
    }

    public Channel findChannelBySid(String sid, ArrayList<Channel> channels) {
        if (channels == null || sid == null) {
            return null;
        }
        for (Channel ch : channels) {
            if (sid.equals(ch.getSid())) {
                return ch;
            }
        }
        return null;
    }

    public Channel findChannelByChNum(String chNum, ArrayList<Channel> channels) {
        if (channels == null || chNum == null) {
            return null;
        }
        for (Channel ch : channels) {
            if (chNum.equals(ch.getCh())) {
                return ch;
            }
        }
        return null;
    }

    public Channel findChannelByChName(String chName, ArrayList<Channel> channels) {
        if (channels == null || chName == null) {
            return null;
        }
        for (Channel ch : channels) {
            if (chName.equals(ch.getCname())) {
                return ch;
            }
        }
        return null;
    }
}
