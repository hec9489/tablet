package com.skb.btv.smartrcuagent.event;

public abstract class Event {
    Type type;

    public enum Type {
        KeyEvent,
        MouseEvent,
        MouseWheelEvent
    }

    public Type getType() {
        return type;
    }
}
