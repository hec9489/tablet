package com.skb.btv.smartrcuagent.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class DnsIpReader {
    String testIpAddr = "116.126.69.76";

    private DnsIpReader() {
    }

    public static DnsIpReader instance;

    public synchronized static DnsIpReader getInstance() {
        if (instance == null) {
            instance = new DnsIpReader();
        }
        return instance;
    }

    public String getRealIpFromDns(String addr) throws UnknownHostException {
        System.setProperty("networkaddress.cache.ttl", "1");
        System.setProperty("networkaddress.cache.negative. ttl", "1");
        java.security.Security.setProperty("networkaddress .cache.ttl", "0");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            ALog.d("DnsIpReader inturuppted");
        }
        InetAddress address;
        address = InetAddress.getByName(addr);
        String ipAddr = address.getHostAddress();
        ALog.d("real ip addr: " + ipAddr);

        // //=========== Test code ==============
        // if ("116.126.69.76".equals(ipAddr)) {
        // testIpAddr = "116.126.69.74";
        // } else {
        // testIpAddr = "116.126.69.76";
        // }
        // ipAddr = testIpAddr;
        // //============ test code end ============
        return ipAddr;
    }
}
