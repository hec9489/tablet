package com.skb.btv.smartrcuagentdevapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.skb.btv.domain.EnvironmentInfo;
import com.skb.btv.smartrcuagent.SmartRCUAgentListener;
import com.skb.btv.smartrcuagent.TVSmartRCUAgent;
import com.skb.btv.smartrcuagent.message.PairingItem;
import com.skb.btv.smartrcuagent.message.ResponseAgreement;
import com.skb.btv.smartrcuagent.message.ResponsePairing;
import com.skb.btv.smartrcuagent.message.ResponsePairingConfirm;
import com.skb.btv.smartrcuagent.message.ResponsePairingList;
import com.skb.btv.smartrcuagent.message.ResponsePairingSequence;
import com.skb.btv.smartrcuagent.message.ResponseReceiveMessage;
import com.skb.btv.smartrcuagent.message.ResponseRepresent;
import com.skb.btv.smartrcuagent.message.ResponseUnPairing;
import com.skb.btv.smartrcuagent.message.ResponseUpdateAgreement;
import com.skb.btv.smartrcuagent.message.ResponseUpdateHostDeviceInfo;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView textHello;
    private TextView textStatus;
    private Button btnVerify;
    private Button btnVerifyConfirm;
    private Button btnUnPairing;
    private Button btnReqAgreement;
    private Button btnReqUpdateAgreement;
    private Button btnReqRepresent;
    private Button btnRecvMessage;
    private Button btnReqUserApi;
    private Button btnReqUserAsyncApi;
    private ProgressBar progressBar;
    private LinearLayoutCompat[] itemLayout = new LinearLayoutCompat[4];
    private TextView[] itemText = new TextView[4];

    private TVSmartRCUAgent tvSmartRCUAgent;
    private ArrayList<PairingItem> pairingItems = new ArrayList<>();
    private int selectedPairingItemIdx = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textHello = findViewById(R.id.tv_hello);
        textStatus = findViewById(R.id.tv_status);
        progressBar = findViewById(R.id.progress_circular);

        btnVerify = findViewById(R.id.btn_verify);
        btnVerify.setOnClickListener(this);
			
        btnVerifyConfirm = findViewById(R.id.btn_verify_confirm);
        btnVerifyConfirm.setOnClickListener(this);

        btnUnPairing = findViewById(R.id.btn_unpairing);
        btnUnPairing.setOnClickListener(this);

        btnReqAgreement = findViewById(R.id.btn_agreement);
        btnReqAgreement.setOnClickListener(this);

        btnReqUpdateAgreement = findViewById(R.id.btn_update_agreement);
        btnReqUpdateAgreement.setOnClickListener(this);
		
        btnReqRepresent = findViewById(R.id.btn_represent);
        btnReqRepresent.setOnClickListener(this);

        btnRecvMessage = findViewById(R.id.btn_recv_msg);
        btnRecvMessage.setOnClickListener(this);

        btnReqUserApi = findViewById(R.id.btn_user_api);
        btnReqUserApi.setOnClickListener(this);

        btnReqUserAsyncApi = findViewById(R.id.btn_user_async_api);
        btnReqUserAsyncApi.setOnClickListener(this);

		
        itemLayout[0] = findViewById(R.id.ll_item1);
        itemLayout[1] = findViewById(R.id.ll_item2);
        itemLayout[2] = findViewById(R.id.ll_item3);
        itemLayout[3] = findViewById(R.id.ll_item4);
        for(int i=0; i<4; i++) {
            itemLayout[i].setOnClickListener(this);
        }

        itemText[0] = findViewById(R.id.tv_item1);
        itemText[1] = findViewById(R.id.tv_item2);
        itemText[2] = findViewById(R.id.tv_item3);
        itemText[3] = findViewById(R.id.tv_item4);

        findViewById(R.id.btn_verify).setOnClickListener(this);
        findViewById(R.id.btn_pairing_list).setOnClickListener(this);
        findViewById(R.id.btn_pairing_status).setOnClickListener(this);
        findViewById(R.id.btn_domain_status).setOnClickListener(this);
        findViewById(R.id.btn_update_device_info).setOnClickListener(this);

        EnvironmentInfo environmentInfo = new EnvironmentInfo();
        environmentInfo.setStbId("{69AF56BB-BA9A-11EA-AB15-C57A43B26EA7}");
        environmentInfo.setStbPatchVersion("15.522.64");
        environmentInfo.setUiVersion("1.9.0");
        environmentInfo.setStbMacAddr("30:EB:25:10:56:24");
		environmentInfo.setModel(android.os.Build.MODEL);
		environmentInfo.setSenderName("B tv Air 5.2.4,10.524.0000,");

        tvSmartRCUAgent = TVSmartRCUAgent.getInstance();		
        tvSmartRCUAgent.startService(getApplicationContext(), environmentInfo);
		
        tvSmartRCUAgent.setOnListener(new SmartRCUAgentListener() {
            @Override
            public void onResponseReceiveMessage(ResponseReceiveMessage response) {
                textHello.setText("CtrlType => " + response.CtrlType
                        + "\nCtrlValue => " + response.CtrlValue
                        + "\nServiceType => " + response.ServiceType
                        + "\nCustomParam => " + response.CustomParam
                        + "\nJsonMessage => " + response.JsonMessage
                        + "\nPushText => " + response.PushText
                        + "\nRCStatusQuery => " + response.RCStatusQuery
                        + "\nPairingID => " + response.PairingID);

                textStatus.setText("Response Receive Message");
                new ReqReceiveMessageTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }

            @Override
            public void onResponsePairingSequence(ResponsePairingSequence response) {
                textHello.setText("resultCode => " + response.ResultCode
                        + "\nresultPhrase => " + response.ResultPhrase
                        + "\nServiceType => " + response.ServiceType
                        + "\nUserID => " + response.UserID
                        + "\nUserName => " + response.UserName
                        + "\nUserNumber => " + response.UserNumber);

                textStatus.setText("Response Pairing Sequence");
				new ReqPairingConfirmTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }

            @Override
            public void onResponseUserAsyncApi(String response) {
                textHello.setText("response => " + response);

                textStatus.setText("Response User Async Api");
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tvSmartRCUAgent.stopService();
    }

    @Override
    public void onClick(View v) {
        int vid = v.getId();

        switch(vid) {
            case R.id.btn_verify:
                new ReqPairingTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;

            case R.id.btn_verify_confirm:
                new ReqPairingConfirmTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;

            case R.id.btn_pairing_list:
                new ReqPairingListTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;

            case R.id.btn_pairing_status:
                new ReqPairingStatusTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;

            case R.id.btn_domain_status:
                new GetDomainStatusTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;

            case R.id.btn_update_device_info:
                new ReqUpdateHostDeviceInfoTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;

            case R.id.btn_unpairing:
                new ReqUnPairingTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;

            case R.id.btn_agreement:
                new ReqAgreementTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;

			case R.id.btn_update_agreement:
                new ReqUpdateAgreementTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;				

            case R.id.btn_represent:
                new ReqRepresentTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;

            case R.id.btn_recv_msg:
                new ReqReceiveMessageTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;

			case R.id.btn_user_api:
                new ReqUserApiTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
				
			case R.id.btn_user_async_api:
                new ReqUserAsyncApiTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
				
            case R.id.ll_item1:
                if(pairingItems.size() > 0) {
                    if(selectedPairingItemIdx == 0) {
                        selectedPairingItemIdx = -1;
                        itemLayout[0].setBackgroundResource(R.drawable.rect_teal);
                        enableButtons(false);
                    } else {
                        if(selectedPairingItemIdx > -1) {
                            itemLayout[selectedPairingItemIdx].setBackgroundResource(R.drawable.rect_teal);
                        }
                        selectedPairingItemIdx = 0;
                        itemLayout[0].setBackgroundResource(R.drawable.rect_black);
                        enableButtons(true);
                    }
                }
                break;

            case R.id.ll_item2:
                if(pairingItems.size() > 1) {
                    if(selectedPairingItemIdx == 1) {
                        selectedPairingItemIdx = -1;
                        itemLayout[1].setBackgroundResource(R.drawable.rect_teal);
                        enableButtons(false);
                    } else {
                        if(selectedPairingItemIdx > -1) {
                            itemLayout[selectedPairingItemIdx].setBackgroundResource(R.drawable.rect_teal);
                        }
                        selectedPairingItemIdx = 1;
                        itemLayout[1].setBackgroundResource(R.drawable.rect_black);
                        enableButtons(true);
                    }
                }
                break;

            case R.id.ll_item3:
                if(pairingItems.size() > 2) {
                    if(selectedPairingItemIdx == 2) {
                        selectedPairingItemIdx = -1;
                        itemLayout[2].setBackgroundResource(R.drawable.rect_teal);
                        enableButtons(false);
                    } else {
                        if(selectedPairingItemIdx > -1) {
                            itemLayout[selectedPairingItemIdx].setBackgroundResource(R.drawable.rect_teal);
                        }
                        selectedPairingItemIdx = 2;
                        itemLayout[2].setBackgroundResource(R.drawable.rect_black);
                        enableButtons(true);
                    }
                }
                break;

            case R.id.ll_item4:
                if(pairingItems.size() > 3) {
                    if(selectedPairingItemIdx == 0) {
                        selectedPairingItemIdx = -1;
                        itemLayout[3].setBackgroundResource(R.drawable.rect_teal);
                        enableButtons(false);
                    } else {
                        if(selectedPairingItemIdx > -1) {
                            itemLayout[selectedPairingItemIdx].setBackgroundResource(R.drawable.rect_teal);
                        }
                        selectedPairingItemIdx = 3;
                        itemLayout[3].setBackgroundResource(R.drawable.rect_black);
                        enableButtons(true);
                    }
                }
                break;
        }
    }

    private void enableButtons(boolean v) {
        btnUnPairing.setEnabled(v);
        btnReqAgreement.setEnabled(v);
		btnReqUpdateAgreement.setEnabled(v);
        btnReqRepresent.setEnabled(v);
        btnRecvMessage.setEnabled(v);
		btnReqUserApi.setEnabled(v);
		btnReqUserAsyncApi.setEnabled(v);
    }


    /*
        인증절차
        1.모바일 Btv 앱에서 인증번호로 인증하기
          - 닉네임 / 생년월일 / 캐릭터 선택 / 약관확인

        2.셋탑박스에서 인증번호 요청
          - tvSmartRCUAgent.requestPairing() 요청 후 응답이 오면
          - tvSmartRCUAgent.requestPairingSequence() 를 요청한 후
            응답 콜백(onResponsePairingSequence(ResponsePairingSequence response))이 오기를 기다린다.
            
        3.셋탑박스에 뜬 인증번호를 모바일 Btv 앱에서 입력한 후 확인버튼 누름
        
        4.셋탑박스에서
          - 응답 콜백(onResponsePairingSequence(ResponsePairingSequence response)) 이 온다.
          - 응답 콜백이 오면 확인 후 tvSmartRCUAgent.requestPairingConfirm() 호출
          - 인증(연결)완료
    */


    private class ReqPairingListTask extends AsyncTask< Void, Void, ResponsePairingList> {
        protected ResponsePairingList doInBackground(Void...params) {
            ResponsePairingList pairingList = tvSmartRCUAgent.requestPairingList();
            return pairingList;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(ResponsePairingList result) {
            super.onPostExecute(result);
            textHello.setText("resultCode => " + result.ResultCode
                    + "\nresultPhrase => " + result.ResultPhrase
                    + "\nparingItemCount => " + result.PairingItems.size());

            pairingItems.clear();
            for(int i=0; i<4; i++) {
                itemLayout[i].setBackgroundResource(R.drawable.rect_gray);
                itemText[i].setText("연결가능");
            }

            int size = result.PairingItems.size();
            if(size > 0) {
                for(int i=0; i<size; i++) {
                    pairingItems.add(result.PairingItems.get(i));
                    itemLayout[i].setBackgroundResource(R.drawable.rect_teal);
                    itemText[i].setText(result.PairingItems.get(i).UserName);
                }
            }

            textStatus.setText("Request Pairing List");
            progressBar.setVisibility(View.GONE);
			enableButtons(false);
			selectedPairingItemIdx = -1;
        }
    }


    private class ReqPairingTask extends AsyncTask< Void, Void, ResponsePairing> {
        protected ResponsePairing doInBackground(Void...params) {
            ResponsePairing responsePairing = tvSmartRCUAgent.requestPairing();
            return responsePairing;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(ResponsePairing result) {
            super.onPostExecute(result);
            textHello.setText("resultCode => " + result.ResultCode
                    + "\nresultPhrase => " + result.ResultPhrase
                    + "\nAuthCode => " + result.AuthCode
                    + "\nAuthCodeExpireDuration => " + result.AuthCodeExpireDuration);

            textStatus.setText("Request Pairing");
            progressBar.setVisibility(View.GONE);
            btnVerify.setEnabled(false);
            btnVerifyConfirm.setEnabled(false);
            new ReqPairingSequenceTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }


    private class ReqPairingSequenceTask extends AsyncTask< Void, Void, String> {
        protected String doInBackground(Void...params) {
            tvSmartRCUAgent.requestPairingSequence();
            return "Request Pairing Sequence";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            textStatus.setText(result);
            btnVerifyConfirm.setEnabled(false);
            progressBar.setVisibility(View.GONE);
        }
    }


    private class ReqPairingConfirmTask extends AsyncTask< Void, Void, ResponsePairingConfirm> {
        protected ResponsePairingConfirm doInBackground(Void...params) {
            ResponsePairingConfirm responsePairingConfirm = tvSmartRCUAgent.requestPairingConfirm("1", "0", "1|1");
            return responsePairingConfirm;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(ResponsePairingConfirm result) {
            super.onPostExecute(result);
            textHello.setText("resultCode => " + result.ResultCode
                    + "\nresultPhrase => " + result.ResultPhrase
                    + "\nPairingID => " + result.PairingID
                    + "\nPairingSessionID => " + result.PairingSessionID);

            textStatus.setText("Request Pairing Confirm");
            progressBar.setVisibility(View.GONE);
            btnVerify.setEnabled(true);
            btnVerifyConfirm.setEnabled(false);
			new ReqPairingListTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }


    private class ReqPairingStatusTask extends AsyncTask< Void, Void, String> {
        protected String doInBackground(Void...params) {
            tvSmartRCUAgent.requestPairingStatus();
            return "Request Pairing Status";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            textHello.setText("");
            textStatus.setText(result);
            progressBar.setVisibility(View.GONE);
        }
    }


    private class GetDomainStatusTask extends AsyncTask< Void, Void, String> {
        protected String doInBackground(Void...params) {
            String domainStatus = tvSmartRCUAgent.getDomainStatus();
            return domainStatus;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            textHello.setText(result);
            textStatus.setText("Get Domain Status");
            progressBar.setVisibility(View.GONE);
        }
    }


    private class ReqUpdateHostDeviceInfoTask extends AsyncTask< Void, Void, ResponseUpdateHostDeviceInfo> {
        protected ResponseUpdateHostDeviceInfo doInBackground(Void...params) {
            ResponseUpdateHostDeviceInfo responsePairingConfirm = tvSmartRCUAgent.requestUpdateHostDeviceInfo("0", "1|1");
            return responsePairingConfirm;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(ResponseUpdateHostDeviceInfo result) {
            super.onPostExecute(result);
            textHello.setText("resultCode => " + result.ResultCode
                    + "\nresultPhrase => " + result.ResultPhrase);

            textStatus.setText("Request Update Host DeviceInfo");
            progressBar.setVisibility(View.GONE);
        }
    }


    private class ReqUnPairingTask extends AsyncTask< Void, Void, ResponseUnPairing> {
        protected ResponseUnPairing doInBackground(Void...params) {
            String pairingID = pairingItems.get(selectedPairingItemIdx).PairingID;
            String serviceType = pairingItems.get(selectedPairingItemIdx).ServiceType;
            ResponseUnPairing responsePairing = tvSmartRCUAgent.requestUnPairing(pairingID, serviceType);
            return responsePairing;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(ResponseUnPairing result) {
            super.onPostExecute(result);
            textHello.setText("resultCode => " + result.ResultCode
                    + "\nresultPhrase => " + result.ResultPhrase);

            textStatus.setText("Request UnPairing");
            progressBar.setVisibility(View.GONE);
            btnUnPairing.setEnabled(false);
            new ReqPairingListTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }



    private class ReqAgreementTask extends AsyncTask< Void, Void, ResponseAgreement> {
        protected ResponseAgreement doInBackground(Void...params) {
            String pairingID = pairingItems.get(selectedPairingItemIdx).PairingID;
            String serviceType = pairingItems.get(selectedPairingItemIdx).ServiceType;
            ResponseAgreement responsePairingConfirm = tvSmartRCUAgent.requestAgreement(pairingID, serviceType);
            return responsePairingConfirm;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(ResponseAgreement result) {
            super.onPostExecute(result);
            textHello.setText("resultCode => " + result.ResultCode
                    + "\nresultPhrase => " + result.ResultPhrase
                    + "\nPairingID => " + result.PairingID
                    + "\nPurchaseList => " + result.PurchaseList
                    + "\nUnlimitedVod => " + result.UnlimitedVod);

            textStatus.setText("Request Agreement");
            progressBar.setVisibility(View.GONE);
        }
    }

    private class ReqUpdateAgreementTask extends AsyncTask< Void, Void, ResponseUpdateAgreement> {
        protected ResponseUpdateAgreement doInBackground(Void...params) {
            String pairingID = pairingItems.get(selectedPairingItemIdx).PairingID;
            String serviceType = pairingItems.get(selectedPairingItemIdx).ServiceType;
            ResponseUpdateAgreement responseUpdateAgreement = tvSmartRCUAgent.requestUpdateAgreement(pairingID, serviceType, null, null);
            return responseUpdateAgreement;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(ResponseUpdateAgreement result) {
            super.onPostExecute(result);
            textHello.setText("resultCode => " + result.ResultCode
                    + "\nresultPhrase => " + result.ResultPhrase
                    + "\nPairingID => " + result.PairingID
                    + "\nServiceType => " + result.ServiceType);

            textStatus.setText("Request Update Agreement");
            progressBar.setVisibility(View.GONE);
        }
    }

    private class ReqRepresentTask extends AsyncTask< Void, Void, ResponseRepresent> {
        protected ResponseRepresent doInBackground(Void...params) {
            String pairingID = pairingItems.get(selectedPairingItemIdx).PairingID;
            String serviceType = pairingItems.get(selectedPairingItemIdx).ServiceType;
            ResponseRepresent responsePairingConfirm = tvSmartRCUAgent.requestRepresent(pairingID, serviceType);
            return responsePairingConfirm;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(ResponseRepresent result) {
            super.onPostExecute(result);
            textHello.setText("resultCode => " + result.ResultCode
                    + "\nresultPhrase => " + result.ResultPhrase
                    + "\nPairingID => " + result.PairingID
                    + "\nRepresentFlag => " + result.RepresentFlag);

            textStatus.setText("Request Represent");
            progressBar.setVisibility(View.GONE);
        }
    }


    /*
      모바일 Btv 앱에서 리모콘을 조작했을 때 조작 상태를 가져오기 위한 api
      리모콘 조작을 하기 전에 우선 호출해놓는다.
     */
    private class ReqReceiveMessageTask extends AsyncTask< Void, Void, String> {
        protected String doInBackground(Void...params) {
            String pairingID = pairingItems.get(selectedPairingItemIdx).PairingID;

            tvSmartRCUAgent.requestReceiveMessage("0", "Success", pairingID,
                    "CHUp", "", "IPTV", "54", "", "",
                    "17", "178");
            return "Request Receive Message";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            textStatus.setText(result);
            progressBar.setVisibility(View.GONE);
        }
    }

    private class ReqUserApiTask extends AsyncTask< Void, Void, String> {
        protected String doInBackground(Void...params) {
			String user_url = "http://nps.hanafostv.com:9090/nps/v5/reqPairingList ";
			String json_data = "{\"body\":{\"pairing_device_type\":\"H\",\"pairing_deviceid\":\"{69AF56BB-BA9A-11EA-AB15-C57A43B26EA7}\",\"retrieve_service_type\":\"ALL\"},\"header\":{\"if_no\":\"IF-NPS-532\",\"receiver\":\"NPS\",\"response_format\":\"json\",\"sender\":\"STB\",\"sender_name\":\"1.9.0,15.522.64,1.0\",\"ver\":\"5.0\"}}";
            String responseUserApi = tvSmartRCUAgent.requestUserApi(user_url, json_data);
            return responseUserApi;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            textHello.setText("String => " + result);

            textStatus.setText("Request User Api");
            progressBar.setVisibility(View.GONE);
        }
    }


    private class ReqUserAsyncApiTask extends AsyncTask< Void, Void, String> {
        protected String doInBackground(Void...params) {
			String user_url = "http://nps.hanafostv.com:9090/nps/v5/reqPairingList ";
			String json_data = "{\"body\":{\"pairing_device_type\":\"H\",\"pairing_deviceid\":\"{69AF56BB-BA9A-11EA-AB15-C57A43B26EA7}\",\"retrieve_service_type\":\"ALL\"},\"header\":{\"if_no\":\"IF-NPS-532\",\"receiver\":\"NPS\",\"response_format\":\"json\",\"sender\":\"STB\",\"sender_name\":\"1.9.0,15.522.64,1.0\",\"ver\":\"5.0\"}}";
            tvSmartRCUAgent.requestUserAsyncApi(user_url, json_data);
            return "Request User Async Api";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            textStatus.setText(result);
            progressBar.setVisibility(View.GONE);
        }
    }


}
